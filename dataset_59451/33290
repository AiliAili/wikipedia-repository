{{featured article}}

{{Taxobox
| name = Rodrigues parrot
| status = EX
| status_system = IUCN3.1
| status_ref = <ref>{{IUCN|id=22728851 |title=''Necropsittacus rodricanus'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
| extinct = soon after 1761
| image = Necropsittacus.jpg
| image_width = 250px
| image_caption = [[Subfossil]] skull and limb bones, 1879
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[bird|Aves]]
| ordo = [[Psittaciformes]]
| familia = [[Psittaculidae]]
| subfamilia = [[Psittaculinae]]
| tribus = [[Psittaculini]]
| genus = '''''Necropsittacus'''''
| genus_authority = [[Alphonse Milne-Edwards|Milne-Edwards]], 1874
| species = '''''N. rodricanus'''''
| binomial = ''Necropsittacus rodricanus''
| binomial_authority = (Milne-Edwards, 1867)
| synonyms =
* ''Psittacus rodricanus'' <small>Milne-Edwards, 1867</small>
* ''Psittacus rodericanus'' <small>Milne-Edwards, 1873</small>
* ''Necropsittacus rodericanus'' <small>Newton, 1875</small>
|range_map=LocationRodrigues.PNG
|range_map_width=250px
|range_map_caption=Location of [[Rodrigues]]}}

The '''Rodrigues parrot''' (''Necropsittacus rodricanus'') is an [[extinct]] species of [[parrot]] that was [[endemism in birds|endemic]] to the [[Mascarene]] island of [[Rodrigues]] in the [[Indian Ocean]], east of [[Madagascar]]. It is unclear to which other species it is most closely related, but it is classified as a member of the [[Tribe (biology)|tribe]] [[Psittaculini]], along with other Mascarene parrots. The Rodrigues parrot bore similarities to the [[broad-billed parrot]] of [[Mauritius]], and may have been related. Two additional species have been assigned to its [[genus]] (''N. francicus'' and ''N. borbonicus''), based on descriptions of parrots from the other Mascarene islands, but their identities and validity have been debated.

The Rodrigues parrot was green, and had a proportionally large head and beak along with a long tail. Its exact size is unknown, but it may have been around {{convert|50|cm|in|abbr=on}} long. It was the largest parrot on Rodrigues, and it had the largest head of any Mascarene parrot. It may have looked similar to the [[great-billed parrot]]. By the time it was discovered, it frequented and nested on islets off southern Rodrigues, where introduced rats were absent, and fed on the seeds of the ''[[Fernelia buxifolia]]'' shrub. The species is known from [[subfossil]] bones and from mentions in contemporary accounts. It was last mentioned in 1761, and probably became extinct soon after, perhaps due to a combination of predation by rats, [[deforestation]], and hunting by humans.

==Taxonomy==
[[File:Rodrigues Parrot.jpg|thumb|left|upright|[[Holotype]] beak, 1873]]
Birds thought to be the Rodrigues parrot were first mentioned by [[François Leguat]] in his 1708 memoir, ''A New Voyage to the East Indies''. Leguat was the leader of a group of nine [[French Huguenot]] refugees who colonised [[Rodrigues]] between 1691 and 1693 after they were [[wikt:maroon#Verb|marooned]] there. Subsequent accounts were by Julien Tafforet, who was marooned on the island in 1726, in his ''Relation de l'Île Rodrigue'', and then by the French mathematician [[Alexandre Pingré]], who travelled to Rodrigues to view the [[1761 transit of Venus]].<ref name="Lost Land"/><ref name="Leguat">{{cite book | last= Leguat | first= F. | editor= Pasfield Oliver, S.  | editor-link= Samuel Pasfield Oliver | year= 1891 | title= The voyage of François Leguat of Bresse, to Rodriguez, Mauritius, Java, and the Cape of Good Hope | volume= Volume 1 | publisher= Hakluyt Society | location= London | url=https://archive.org/stream/voyagefranoisle01missgoog#page/n200/mode/1up | pages=84–85}}</ref><ref name="Cheke1987">{{Cite book| last1 = Cheke | first1 = A. S. | editor1-last = Diamond| editor1-first = A. W.| doi = 10.1017/CBO9780511735769.003 | chapter = An ecological history of the Mascarene Islands, with particular reference to extinctions and introductions of land vertebrates | title = Studies of Mascarene Island Birds | pages = 5–89 | year = 1987 | isbn = 978-0-521-11331-1| location = Cambridge | publisher = Cambridge University Press }}</ref>

The Rodrigues parrot was [[scientifically described]] and named as ''[[Psittacus]] rodricanus'' in 1867 by the French ornithologist [[Alphonse Milne-Edwards]], based on a [[subfossil]] partial beak.<ref>{{Cite journal | last = Milne-Edwards | first = A. | title = Une Psittacien fossile de l'Île Rodrigue | journal = Annales des Sciences Naturelles, Zoologie | series=Series 5 | volume = 8 | pages = 145–156 | language = French | year = 1867 | url = http://www.biodiversitylibrary.org/item/92541#page/153/mode/1up}}</ref> The [[Specific name (zoology)|specific name]] refers to Rodrigues, which is itself named after the discoverer of the island, the Portuguese navigator [[Diogo Rodrigues]].<ref name="Mascarene Parrots"/> Milne-Edwards corrected the spelling of the specific name to ''rodericanus'' in 1873 (in a compilation of his articles about extinct birds), a spelling which was used in the literature henceforward, but it was changed back to ''rodricanus'' by the [[IOC World Bird List]] in 2014.<ref name="Milne-EdwardsMemoir">{{cite book
| last1 = Milne-Edwards
| first1 = A.
|year= 1866–1873
| pages =23–34
| language = French
|title= Recherches sur la faune ornithologique éteinte des iles Mascareignes et de Madagascar
|publisher=G. Masson
|url=https://archive.org/stream/recherchessurlaf01miln#page/22/mode/2up
|location=Paris}}</ref><ref name="IOC">{{cite web|url=http://www.worldbirdnames.org/bow/parrots/|publisher=IOC World Bird List|title=Parrots & cockatoos|author=Gill, F.|author2=Donsker, D.|accessdate=February 11, 2017|year=2014}}</ref> Milne-Edwards moved the species to its own [[genus]] ''Necropsittacus'' in 1874; the name is derived from the Greek words ''necros'', which means dead, and ''psittakos'', parrot, in reference to the bird being extinct.
<ref name="Mascarene Parrots"/><ref>{{cite journal  | last = Milne-Edwards | first =  A. | title = Recherches sur la faune ancienne des Iles Mascareignes| journal = Annales des Sciences Naturelles, Zoologie | series =  Series 5 | volume = 19 | language =  French | pages =  1–31 | year =1874 | url=http://biodiversitylibrary.org/page/33089117 }}</ref>

The current whereabouts of the [[holotype]] beak are unknown. It may be specimen UMZC 575, a [[rostrum (anatomy)|rostrum]] that was sent from Milne-Edwards to the English zoologist [[Alfred Newton]] after 1880, which matches the drawing and description in Milne-Edwards' paper, but this cannot be confirmed.<ref name="Mascarene Parrots">{{Cite journal|last=Hume|first=J. P. | pages = 4–34|year=2007 |url=http://julianhume.co.uk/wp-content/uploads/2010/07/Hume-Mascarene-Parrots.pdf |title=Reappraisal of the parrots (Aves: Psittacidae) from the Mascarene Islands, with comments on their ecology, morphology, and affinities |journal=[[Zootaxa]] |volume=1513}}</ref> In 1893 the ornithologists [[Edward Newton]] (brother of Alfred) and [[Hans Gadow]] described more fossils of the Rodrigues parrot, including a skull and limb bones.<ref name="Newton & Gadow"/> Remains of the species are scarce, and subfossils have been discovered in caves on the [[Plaine Corail]] and in [[Caverne Tortue]].<ref name="Synopsis Hume">{{cite journal | url= http://verlag.nhm-wien.ac.at/buecher/2013_SAPE_Proceedings/15_Hume.pdf | title= A synopsis of the pre-human avifauna of the Mascarene Islands | last=Hume | first=J. P. |editor=Göhlich, U. B. |editor2=Kroh, A. | journal=Proceedings of the 8th International Meeting of Society of Avian Paleontology and Evolution | year=2013 | pages=195–237}}</ref>

In the footnotes to his 1873 compilation, Milne-Edwards correlated the subfossil species with parrots mentioned by Leguat.<ref name="Milne-EdwardsMemoir"/> In 1875, A. Newton analysed Tafforet's then newly rediscovered account, and identified a description of the Rodrigues parrot therein.<ref>{{cite journal  | last = Newton  | first =  A.   | title = Additional evidence as to the original fauna of Rodriguez | journal = Proceedings of the Zoological Society of London  | series =   | volume =  | edition = | pages = 39–43  | url = http://www.biodiversitylibrary.org/page/28501177#page/77/mode/1up  | year = 1875}}</ref> In a footnote in an 1891 edition of Leguat's memoir, the English writer [[Samuel Pasfield Oliver]] doubted that the parrots mentioned were the Rodrigues parrot, due to their smaller size, and suggested they may have been [[Newton's parakeet]] (''Psittacula exsul'').<ref name="Leguat"/> As Leguat mentioned both green and blue parrots in the same sentence, the English [[palaeontologist]] [[Julian Hume]] suggested in 2007 that these could either be interpreted as references to both the Rodrigues parrot and Newton's parakeet, or as two [[colour morphs]] of the latter.<ref name="Mascarene Parrots"/>

===Evolution===
Many [[endemic]] Mascarene birds, including the [[dodo]] (''Raphus cucullatus''), are derived from South Asian ancestors, and Hume has proposed that this may be the case for all the parrots there as well. Sea levels were lower during the [[Pleistocene]], so it was possible for species to colonise some of the then less isolated islands.<ref name="Lost Land"/> Although most extinct parrot species of the Mascarenes are poorly known, subfossil remains show that they shared features such as enlarged heads and jaws, reduced pectoral bones, and robust leg bones.<ref name="Mascarene Parrots"/> Newton and Gadow found the Rodrigues parrot to be closely related to the [[broad-billed parrot]] (''Lophopsittacus mauritianus'') due to their large jaws and other [[osteological]] features, but were unable to determine whether they both belonged in the same genus, since a head-crest was only known from the latter.<ref name="Newton & Gadow">{{cite journal| doi = 10.1111/j.1469-7998.1893.tb00001.x| last1 = Newton | first1 = E.| last2 = Gadow | first2 = H.| year = 1893| title = IX. On additional bones of the Dodo and other extinct birds of Mauritius obtained by Mr. Theodore Sauzier| journal = The Transactions of the Zoological Society of London| volume = 13| issue = 7| pages = 281–302| pmid = | url = http://www.biodiversitylibrary.org/page/31083700#page/379/mode/1up| ref = harv
}}</ref> The British ornithologist Graham S. Cowles instead found their skulls too dissimilar for them to be close relatives in 1987.<ref name="Cowles87">{{Cite book | doi = 10.1017/CBO9780511735769.004| editor1-last = Diamond| editor1-first = A. W.| title = Studies of Mascarene Island Birds| chapter = The fossil record| pages = 90–100| year = 1987| location = Cambridge | last1 = Cowles | first1 = G. S.| isbn = 978-0-511-73576-9}}</ref>

Hume has suggested that the Mascarene parrots have a common origin in the [[Evolutionary radiation|radiation]] of the [[Psittaculini]] [[Tribe (biology)|tribe]], basing this theory on [[morphology (biology)|morphological]] features and the fact that ''[[Psittacula]]'' parrots have managed to colonise many isolated islands in the Indian Ocean.<ref name="Mascarene Parrots"/> The Psittaculini may have invaded the area several times, as many of the species were so specialised that they may have evolved significantly on [[hotspot island]]s before the Mascarenes emerged from the sea.<ref name="Lost Land">{{cite book
| last1 = Cheke
| first1 = A. S.
| first2 = J. P.
| last2 = Hume
| year = 2008
| title = Lost Land of the Dodo: an Ecological History of Mauritius, Réunion & Rodrigues
| pages = 49–73, 181
| publisher = T. & A. D. Poyser
| location = New Haven and London
| isbn = 978-0-7136-6544-4}}</ref> A 2011 genetic study by Kundu and colleagues instead found that the [[Mascarene parrot]] (''Mascarinus mascarin'') of [[Réunion]] was most closely related to the [[lesser vasa parrot]] (''Coracopsis nigra'') from [[Madagascar]] and nearby islands, and therefore unrelated to the ''Psittacula'' parrots, disputing the theory of their common origin.<ref>{{Cite journal | last1 = Kundu | first1 = S. | last2 = Jones | first2 = C. G. | last3 = Prys-Jones | first3 = R. P. | last4 = Groombridge | first4 = J. J. | title = The evolution of the Indian Ocean parrots (Psittaciformes): Extinction, adaptive radiation and eustacy | doi = 10.1016/j.ympev.2011.09.025 | journal = Molecular Phylogenetics and Evolution | volume = 62 | issue = 1 | pages = 296–305 | year = 2011| pmid =  22019932| pmc = }}</ref>

===Hypothetical extinct relatives===
[[File:Necropsittacus borbonicus.png|thumb|left|1907 illustration by [[Henrik Grönvold]], showing the colouration of the [[hypothetical species]] ''N. borbonicus'' combined with the body-plan of the Rodrigues parrot]]
The British zoologist [[Walther Rothschild]] assigned two [[Hypothetical species|hypothetical]] parrot species from the other Mascarene Islands to the ''Necropsittacus'' genus; ''N. francicus'' in 1905 and ''N. borbonicus'' (named for Bourbon, the original name of Réunion) in 1907. ''N. borbonicus'' was based on a single account by the French traveller [[Sieur Dubois]], who mentioned "green parrots of the same size [presumably as the [[Réunion parakeet]] (''Psittacula eques eques'')] with head, upper parts of the wings, and tail the colour of fire" on Réunion. Rothschild considered it to belong to ''Necropsittacus'' since Dubois compared it with ''Psittacula'' species, which are related. Rothschild gave the original description of ''N. francicus'' as "head and tail fiery red, rest of body and tail green", and stated it was based on descriptions from voyages to [[Mauritius]] in the 17th and early 18th century.<ref name="Mascarene Parrots"/><ref name="Ornis">{{cite journal  | last =  Rothschild | first =  W.| title =  On extinct and vanishing birds | journal = Ornis (Proceedings of the 4th International Ornithological Congress, London)| series =   | volume = 14 | edition =    | pages =  191–217 | url = http://www.biodiversitylibrary.org/item/106225#page/207/mode/1up  | year = 1905}}</ref><ref name="Rothschild">{{Cite book  | last = Rothschild  | first = W.  | title = Extinct Birds  | publisher = Hutchinson & Co  | year = 1907  | location = London  | pages = 61–62  | url = https://archive.org/stream/extinctbirdsatte00roth#page/60/mode/2up}}</ref><ref name="Fuller Extinct"/>

The two assigned ''Necropsittacus'' species have since become the source of much taxonomic confusion, and their identities have been debated. ''N. borbonicus'' later received [[common names]] such as Réunion red and green parakeet or Réunion parrot, and ''N. francicus'' has been called the Mauritian parrot. The Japanese ornithologist [[Masauji Hachisuka]] recognised ''N. borbonicus'' in 1953, and published a restoration of it with the colouration described by Dubois and the body-plan of the Rodrigues parrot. He did not find the naming of ''N. francicus'' to have been necessary, but expressed hope more evidence would be found. In 1967, the American ornithologist [[James Greenway]] suggested that ''N. borbonicus'' may have been an escaped pet [[lory]] seen by Dubois, since 16th century Dutch paintings show the somewhat similar East Indian [[chattering lory]] (''Lorius garrulus''), presumably in captivity. However, Greenway was unable to find any references that matched those Rothschild had given for ''N. francicus''.<ref name="Mascarene Parrots"/><ref name="Extinct Birds"/><ref name =Greenway>{{cite book
 | last = Greenway
 | first = J. C.
 | title = Extinct and Vanishing Birds of the World
 | publisher = American Committee for International Wild Life Protection 13
 | series = 
 | volume =
 | edition = 
 | location = New York
 | year = 1967
 | page = 127
 | doi = 
 | isbn = 978-0-486-21869-4
 | mr = 
 | zbl =  }}</ref>

In 1987, the British ecologist Anthony S. Cheke found the described colour-pattern of ''N. borbonicus'' remiscent of ''Psittacula'' parrots, but considered ''N. francicus'' to be based on confused reports.<ref name="Cheke1987"/> In 2001 the British writer [[Errol Fuller]] suggested Dubois' account of ''N. borbonicus'' could either have referred to an otherwise unrecorded species or have been misleading, and found ''N. francicus'' to be "one of the most dubious of all hypothetical species".<ref name="Fuller Extinct"/> In 2007, Hume suggested that Rothschild had associated ''N. borbonicus'' with the Rodrigues parrot because he had mistakenly incorporated Dubois' account into his description of the latter; he stated the Rodrigues parrot also had red plumage (though it was all-green), and had been mentioned by Dubois (who never visited Rodrigues). Rothschild also attributed the sighting of ''N. francicus'' to Dubois, repeating the colour-pattern he had described earlier for the Rodrigues parrot, and this lead Hume to conclude that the name ''N. francicus'' was based solely on "the muddled imagination of Lord Rothschild". Hume added that if Dubois' description of ''N. borbonicus'' was based on a parrot endemic to Réunion, it may have been derived from the [[Alexandrine parakeet]] (''Psittacula eupatria''), which has a similar colouration, apart from the red tail.<ref name="Mascarene Parrots"/><ref name="Ornis"/>

==Description==
[[File:Tanygnathus megalorynchos -two in captivity-8a.jpg|thumb|The Rodrigues parrot may have resembled the [[great-billed parrot]]]]
The Rodrigues parrot was described as being the largest parrot species on the island, with a big head and a long tail. Its plumage was described as being of uniform green colouration.<ref name="Extinct Birds"/> Its skull was flat and depressed compared to those of most other parrots, but similar to the genus ''[[Ara (genus)|Ara]]''. The skull was {{convert|50|mm|in|abbr=on}} long without the beak, {{convert|38|mm|in|abbr=on}} wide, and {{convert|24|mm|in|abbr=on}} deep. The [[coracoid]] (part of the shoulder) was {{convert|35|mm|in|abbr=on}} long, the [[humerus]] (upper-arm bone) {{convert|53|mm|in|abbr=on}}, the [[ulna]] (lower-arm bone) {{convert|57|mm|in|abbr=on}}, the [[femur]] (thigh-bone) {{convert|49|mm|in|abbr=on}}, the [[tibia]] (lower-leg bone) {{convert|63|mm|in|abbr=on}}, and the [[metatarsus]] (foot bone) {{convert|22|mm|in|abbr=on}}.<ref name="Rodriguez Birds">{{cite journal| doi = 10.1098/rstl.1879.0043| last1 = Günther | first1 = A.| last2 = Newton | first2 = E.| year = 1879| title = The extinct birds of Rodriguez| journal = [[Philosophical Transactions of the Royal Society of London]]| volume = 168| pages = 423–437| url = https://archive.org/stream/philtrans07832595/07832595#page/n0/mode/2up| pmc = | ref = harv
}}</ref> Its exact body length is unknown, but it may have been around {{convert|50|cm|in|abbr=on}}, comparable to the size of a large [[cockatoo]].<ref name="Fuller Extinct">{{cite book
  | last = Fuller
  | first = E.
  | year = 2001
  | title = Extinct Birds
  | edition = revised
  | publisher = Comstock
  | location = New York
  |pages = 232–233
  | isbn = 978-0-8014-3954-4
  | ref = harv
  }}</ref> Its tibia was 32% smaller than that of a female broad-billed parrot, yet the pectoral bones were of similar size, and proportionally its head was the largest of any Mascarene species of parrot.<ref name="Mascarene Parrots"/>

The skeleton of the Rodrigues parrot had similarities with the ''[[Tanygnathus]]'' and ''Psittacula'' parrot genera. The pectoral and [[pelvic]] bones were similar in size to those of the [[New Zealand kaka]] (''Nestor meridionalis''), and it may have looked similar to the [[great-billed parrot]] (''Tanygnathus megalorynchos'') in life, but with a larger head and tail. It differed from other Mascarene parrots in several skeletal features, including having nostrils that faced upwards instead of forwards. No features of the skull suggest it had a crest like the broad-billed parrot, and there is not enough fossil evidence to determine whether it had pronounced [[sexual dimorphism]].<ref name="Mascarene Parrots"/> There are intermediate specimens between the longest and shortest examples of the known skeletal elements, which indicates there were no distinct size groups.<ref name="Rodriguez Birds"/>

==Behaviour and ecology==
[[File:Necropsittacus rodericanus.jpg|thumb|upright|Restoration of two Rodrigues parrots]]
Tafforet's 1726 description is the only detailed account of the Rodrigues parrot in life:
{{Quotation|The largest are larger than a pigeon, and have a tail very long, the head large as well as the beak. They mostly come on the islets which are to the south of the island, where they eat a small black seed, which produces a small shrub whose leaves have the smell of the orange tree, and come to the mainland to drink water&nbsp;... they have their plumage green.<ref name="Mascarene Parrots"/>}}
Tafforet also mentioned that the parrots ate the seeds of the ''[[Fernelia buxifolia]]'' shrub ("bois de buis"), which is endangered today, but was common all over Rodrigues and nearby islets during his visit. Due to a large population of introduced rats on Rodrigues, the parrots, the [[Rodrigues starling]] (''Necropsar rodericanus''), and the [[Rodrigues pigeon]] (''Nesoenas rodericanus''), frequented and nested on offshore islets, where the rats were absent.<ref name="Mascarene Parrots"/>

Many other of the endemic species of Rodrigues became extinct after the arrival of humans, so the [[ecosystem]] of the island is heavily damaged. Before humans arrived, forests covered the island entirely, but very little remains today due to [[deforestation]]. The Rodrigues parrot lived alongside other recently extinct birds such as the [[Rodrigues solitaire]] (''Pezophaps solitaria''), the [[Rodrigues rail]] (''Erythromachus leguati''), Newton's parakeet, the Rodrigues starling, the [[Rodrigues owl]] (''Mascarenotus murivorus''), the [[Rodrigues night heron]] (''Nycticorax megacephalus''), and the Rodrigues pigeon. Extinct reptiles include the [[domed Rodrigues giant tortoise]]  (''Cylindraspis peltastes''), the [[saddle-backed Rodrigues giant tortoise]] (''Cylindraspis vosmaeri''), and the [[Rodrigues day gecko]] (''Phelsuma edwardnewtoni'').<ref name="Lost Land"/>

==Extinction==
[[File:Rodrigues.jpg|thumb|[[Leguat]]'s map of pristine Rodrigues; his settlement is in the northeast|alt=Map of Rodrigues, decorated with solitaires]]
Of the eight or so parrot species endemic to the Mascarenes, only the [[echo parakeet]] (''Psittacula eques echo'') of Mauritius has survived. The others were likely all made extinct by a combination of excessive hunting and deforestation by humans. Like mainland Rodrigues, the offshore islets were eventually infested by rats as well, and this is believed to be the reason for the demise of the Rodrigues parrot and other birds there.<ref name="Mascarene Parrots"/> The rats probably preyed on their eggs and chicks.<ref name="Extinct Birds">{{cite book
| last1 = Hume
| first1 = J. P.
| first2 = M.
| last2 = Walters
|pages= 178–179
|year= 2012
|location= London
|title= Extinct Birds
|publisher= A & C Black
|isbn=978-1-4081-5725-1}}</ref> Pingré indicated that local species were popular [[Game (hunting)|game]], and found that the Rodrigues parrot was rare:
{{Quotation|The perruche [Newton's parakeet] seemed to me much more delicate [than the flying-fox]. I would not have missed any game from France if this one had been commoner in Rodrigues; but it begins to become rare. There are even fewer perroquets [Rodrigues parrots], although there were once a big enough quantity according to François Leguat; indeed a little islet south of Rodrigues still retains the name Isle of Parrots [Isle Pierrot].<ref name="Mascarene Parrots"/>}}
Pingré also reported that the island was becoming deforested by [[tortoise]] hunters who set fires to clear vegetation. Along with direct hunting of the parrots, this likely led to a reduction in the population of Rodrigues parrots. Pingré's 1761 account is the last known mention of the species, and it probably became extinct soon after.<ref name="Mascarene Parrots"/>

==References==
{{reflist|30em}}

==External links==
*{{Commons category-inline|Necropsittacus rodricanus}}
*{{Wikispecies-inline|Psittaculini}}

{{Psittaculini}}
{{Birds}}
{{portalbar|Birds|Animals|Biology|Mauritius|Madagascar|Extinction|Paleontology}}
{{taxonbar}}

[[Category:Extinct birds of Indian Ocean islands]]
[[Category:Necropsittacus]]
[[Category:Bird extinctions since 1500]]
[[Category:Birds described in 1867]]
[[Category:Parrots of Africa]]
[[Category:Fauna of Rodrigues]]