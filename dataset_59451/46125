{{Infobox person
|name = John Allen
|image =
|image_size =
|caption =
|birth_name =
|birth_date = 1823
|birth_place = Near [[Syracuse, New York|Syracuse]], [[New York (state)|New York]], United States
|death_date = 1870
|death_place = [[Perth, New York|West Perth]], New York
|death_cause =
|resting_place =
|resting_place_coordinates =
|residence =
|nationality = American
|other_names = E.E. Van Allen
|known_for = Saloon keeper and underworld figure known as the "Wickedest Man in New York".
|education =
|alma_mater =
|employer =
|occupation = Saloon keeper
|home_town =
|title =
|salary =
|networth =
|height =
|weight =
|term =
|predecessor =
|successor =
|party =
|boards =
|religion =
|spouse =
|partner =
|children =
|parents =
|relations =
|signature =
|website =
|footnotes =
}}

'''John Allen''' (1823–1870) was an [[United States|American]] [[saloon keeper]] and underworld figure in [[New York City]] during the early-to mid-19th century. A former religious student, Allen was considered one of the most notorious criminals in the city and was known as the "Wickedest Man in New York". A public crusade against him, headed by lawyer and journalist [[Oliver Dyer]], resulted in a reform movement known as the "Water Street revival".

The campaign, in which Allen and other notorious underworld figures had been "reformed" by religious leaders, was later revealed to be a fraud following [[expose (journalism)|exclusive exposes]] by the ''[[New York Times]]'' and the ''[[New York World]]'' forcing Allen to leave the city.

==Early life and criminal career==
Born to a prominent and well-to-do religious family in [[upstate New York]] near [[Syracuse, New York|Syracuse]], two of his brothers became [[Presbyterian]] preachers while a third became a [[Baptist]] minister. The rest of his brothers, however, settled in [[New York City]] where they became "professional burglars and footpads", most especially [[Theodore Allen (saloon keeper)|Theodore Allen]] who became one of the city's earliest underworld figures. Allen was attending the [[Union Theological Seminary in the City of New York|Union Theological Seminary]] when, around 1850, he left the institution to join his brothers in New York.<ref name="Martin">Martin, Edward Winslow. ''The Secrets of the Great City: A Work Descriptive of the Virtues and the Vices, the Mysteries, Miseries and Crimes of New York City''. Philadelphia: Jones Brothers & Co., 1868. (pg. 322-344)</ref><ref name="Asbury">[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 50-56) ISBN 1-56025-275-8</ref> He lived with his brothers for about a year, who tutored him in burglary, and did ''"quite well by his own account"'' but was eventually cast out by his brothers after confessing to being a [[police informant]].<ref name="Asbury2">[[Herbert Asbury|Asbury, Herbert]]. ''All Around the Town: Murder, Scandal, Riot and Mayhem in Old New York''. New York: Alfred A. Knoff, 1929. (pg. 123-131) ISBN 1-56025-521-8</ref>

It was during this time that he married a "lush worker" known as Little Susie and the two moved to the waterfront district of the infamous Fourth Ward in 1855. While Susie continued her trade of "rolling drunks", Allen was employed as a runner for a [[Shanghaiing|crimp house]]. His particular job was to lure sailor, or any passersby, into the establishment where they would be drugged and [[Shanghaiing|shanghaied]] for outgoing vessels short on crew. Allen himself was eventually suffered a similar fate two years later when, while drinking with his employer one night, was drugged, robbed and woke up hours later ''"in the [[forecastle]] of a ship bound for [[South America]]"''. Allen made his way back to New York six months later, and soon after his return, his former employer had been found ''"beaten to death with an iron belaying-pin"''. There was no evidence connecting Allen to the murder, but he was considered a suspect by police and decided to seek different means of employment.<ref name="Asbury2"/>

He and Susie moved to the district around [[Sixth Avenue (Manhattan)|Sixth Avenue]] and [[13th Street (Manhattan)|Thirteenth Street]], in what would later become the "[[Tenderloin, Manhattan|Tenderloin district]]", and began working for procuress [[Hester Jane Haskins]]. The husband and wife were among ''"respectable-looking young men and women"'' employed by Haskins to travel throughout [[New England]] to lure young women to New York with the promises of work. Once these women arrived, they were abducted and forced to work in [[brothels]]. When Haskins began kidnapping young girls from more prominent families, Allen and Susie decided to leave her organization. Haskins was arrested only a year later.<ref name="Asbury2"/>

==Rise to fame in the New York underworld==
Returning to the waterfront, Allen and his wife opened a dance hall on Water Street. The dance hall also operated as a [[brothel]] occupied by twenty young women ''"who wore long black bodices of satin, scarlet skirts and stockings, and red topped boots with bells affixed to the ankles"''.<ref name="Morris">Morris, Lloyd R. ''Incredible New York: High Life and Low Life of the Last Hundred Years''. New York: Random House, 1951. (pg. 226)</ref> One of the girls who worked at Allen's establishment was supposedly the daughter of a [[Lieutenant-Governor]] in [[New England]]. She had originally come to New York to find her fortune and fell into the hands of [[procuring (prostitution)|procurers]] and [[forced prostitution|forced her into prostitution]].<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/>

In time, Allen's resort became one of the principal hangouts for gangsters and other criminals of the Fourth Ward. By 1860, he had amassed a personal fortune of over $100,000. His resort became one of the earliest dance halls, and later the model for many of the city's most infamous dive bars, saloons and other resorts during the late 19th century and up until the turn of the 20th century. Among them were ''the Haymarket'', ''McGurk's Suicide Hall'', ''Paresis Hall'' and [[Billy McGlory]]'s ''Armory Hall''.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/> It was reported that, every evening, ''"several hundred partake of the rude fun, among them are boys and girls below twelve years of age. The atmosphere reeks with blasphemy. The women are driven to their work by imprecation, and often by blows, from their task master."''<ref name="Morris"/>

{{Rquote|right|
''There is rest for the weary,<br />There is rest for you,<br />On the other side of Jordan,<br>In the sweet fields of Eden,<br />Where the Tree of Life is blooming,<br />There is rest for you.''|
There is Rest for the Weary}}

Although involved in theft, procuring and possibly murder, Allen remained a devoutly religious man long after leaving the ministry. He opened his resort every afternoon at 1:00 pm, however he gathered his employees, including prostitutes, bartenders and musicians alike, and held a [[prayer meeting]] in a bar room in the back of the hall three days a week at noon. In each cubicle where Allen's women brought men, a Bible and other religious literature was available. On gala nights, these were often given away as souvenirs by Allen himself. Allen subscribed to almost every religious paper and magazine published in the United States during this time<ref name="Morris"/> as well as his favorite newspapers the ''[[New York Observer]]'' and ''[[The Independent]]''. He scattered these about the dance hall and bar room of the resort while every table and bench had ''The Little Wanderers' Friend'', then a popular hymnbook. It was in this spirit that Allen would lead his employees and patrons in a sing-song, most often, ''"There is Rest for the Weary"''.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/>

==Water Street revival==
Allen's resort received considerable coverage by newspapers and magazines, particularly in light of the colorful atmosphere and his eccentric manner, the most prominent of these being ''Packard's Monthly'' journalist [[Oliver Dyer]] who first referred to him as the ''"Wickedest Man in New York"''. Allen's activities also led to his being targeted by reformers and evangelical clergymen seeking to rid the city of vice and crime. The most prominent of these was Reverend A.C. Arnold, founder of the Howard Mission, who visited Allen's resort to persuade him to allow an ordained preacher to conduct his prayer meetings.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/><ref name="Morris"/>

On May 25, 1868, Arnold led a group of six clergymen and a number of devout laymen to Allen's dance hall. When they approached Allen, they found he was so drunk that he was unable to object when they held a prayer meeting lasting from midnight until around 4:00 am. The incident was covered extensively by the press causing regular curiosity seekers and ministers to visit the dance hall for several months. The unwanted attention drove Allen's regular customers away and he began to lose money. Arnold and other preachers continued to hold prayer meetings at the dance hall, usually whenever they were able to be given consent by an intoxicated Allen, and began to call upon him to close down the dance hall.<ref name="Morris"/> Finally, at midnight on August 29, 1868, Allen's dance hall closed for the first time in seventeen years. The following morning, a notice was posted on the door.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/>

{{cquote|''This Dance House Is Closed: No gentlemen admitted unless accompanied by their wives, who wish to employ magdalenes as servants.''}}

One day after the close of Allen's resort, A.C. Arnold publicly announced that Allen had been converted and reformed, and that he had forsaken his former occupation. Revival meetings were held in the resort several days later and, the following Sunday, Allen attended church services at the Howard Mission where its congregation prayed for him at Arnold's request.
Allen's appearance at the mission gained attention by the press as well as the daily meetings at Allen's establishment which continued for a month.<ref name="Morris"/> It was also during this time that the ministers had approached Allen's rivals, most notably [[Tommy Hadden]], [[Kit Burns]] and Bill Slocum, to hold similar meetings in their establishments.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/>

On September 11, a prayer meeting was held in Hadden's Water Street boarding house with his consent although none were held in his more infamous Cherry Street resort. Meetings were also held in Bill Slocum's gin mill, also on Water Street, and Kit Burns "rat pit" held in his liquor store. Their establishments were also overrun by preachers and, while none of the men would attend services at the Howard Mission, they did allowed themselves to be mentioned in the congregation's prayers.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/>

This campaign, later to become known as the "Water Street revival", was declared in a public statement issued by many of the city's prominent religious leaders which explained its purpose claiming that Allen, Burns, Hadden and Slocum had freely allowed the use of their establishments for religious purposes because they had reformed and had renounced their lives of crime. An extensive investigation by the ''[[New York Times]]'' showed that the preachers, and certain financial backers, had paid Allen $350 for the use of his dance hall for a month. As part of their agreement, Allen had also agreed to sing hymns, prayer meeting and to claim that he had given his dance hall free of charge ''"because of his love of the preachers"''. These denouncements by the ''New York Times'' and the ''New York World'' caused serious damage to the preachers campaign as its large congregations began to desert the cause due to the perceived dishonesty by religious leaders. The "Water Street revival" eventually faded from public attention and was abandoned.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/><ref name="Morris"/>

==Aftermath and final years==
While his competitors soon returned to their criminal ways, Allen never recovered from the Water Street revival. His underworld reputation was irrevocably damaged, his former criminal clientele regarding him as ''"loose and unsound"'', and most refrained from attending the dance hall. Although he still retained his women and musicians, he was forced to close the dance hall within a few months.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/><ref name="Morris"/>

His last public appearance was in late 1868 when he and his wife were arraigned in the [[The Tombs|Tombs Police Court]], along with several of their girls, and charged with robbing a sailor of $15. One of the girls, Margaret Ware, was immediately held for trial while Allen himself was bound over $300 (or $500) bail for appearance in General Sessions. Appearing before Judge Joseph Dowling, Allen claimed that his arrest had been caused by Oliver Dyer and that the charges were a ''"put up job"''.<ref name="Martin"/><ref name="Asbury"/><ref name="Asbury2"/> The arresting officer, Captain [[Thomas Woolsey Thorne]], accused Allen of running a ''"disorderly house"''. Allen denied this charge and insisted that is establishment had been in use for the past several days for religious meetings. Allen was released and the others not already sentenced were discharged.<ref>{{Citation
| title = John Allen At The Tombs.; The "Wickedest Man in New-York" Returns to His Former Business--Descent Upon His Brothel by the Police--Arrest of all the Inmates of the House
|journal=[[The New York Times]]
|date= October 18, 1868 | accessdate = 2009-08-25
| url = https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9D05E6DB1E3CE13BBC4052DFB6678383679FDE
| format=PDF
}}</ref> He died in [[Perth, New York|West Perth]], [[Fulton County, New York]] two years later.<ref>{{Citation
| title = Death of the "Wickedest Man in New-York."
|journal=The New York Times
|date= October 4, 1870 | accessdate = 2009-08-25
| url = https://query.nytimes.com/mem/archive-free/pdf?res=9A0DE4DD1F3CE13BBC4C53DFB667838B669FDE
| format=PDF
}}</ref>

==References==
{{Reflist}}

==Further reading==
*Browne, Junius Henri. ''The Great Metropolis: A Mirror of New York''. Hartford: American Publishing Company, 1869.
*Ellington, George. ''The Women of New York: or, Social life in the Great City''. New York: New York Book Company, 1870.

{{DEFAULTSORT:Allen, John}}
[[Category:1823 births]]
[[Category:1870 deaths]]
[[Category:Criminals from New York City]]
[[Category:People from Manhattan]]
[[Category:Saloonkeepers]]