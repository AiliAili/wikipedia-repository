{{multiple issues|
{{cleanup-reorganize|date=January 2013}}
{{notability|date=January 2013}}
{{orphan|date=January 2013}}
{{ref improve|date=January 2013}}
}}
==Applications of Planning History and Theory to Current Planning Topics==
Planners often look to the past to shape the future; without referencing case studies (past failures and successes) it is difficult to know what will produce a great city. Brownfield remediation and the need for affordable housing to produce economic development are two issues supported by a number of planning theories and case studies.  

==Brownfield remediation and economic development==
[[File:Brownfield overview.pdf|thumbnail|default|Cost Benefits of Brownfield Remediation, in .pdf form]]

A Brownfield as defined by the EPA is “a real property, the expansion, redevelopment, or reuse of which may be complicated by the presence or potential presence of a hazardous substance, pollutant, or contaminant.” Brownfield remediation is being considered more and more often as a viable way to revitalize and spur economic development in communities. It is often dismissed by developers as being too expensive, but a number of studies actually show that remediation has a great number of public benefits as well as economic and environmental gains.

==Benefits==
Some pros for Brownfield redevelopment include:<ref name=Portland>{{cite web|title=Brownfield/Greenfield Development Cost Comparison Study: EXECUTIVE SUMMARY|url=http://www.portofportland.com/PDFPOP/Trade_Trans_Studies_Brnfld_Stdy_Exec_Smry.pdf|publisher=[[Port of Portland (Oregon)|Port of Portland]]|accessdate=8 December 2012|date=December 2004}}</ref> 
*Contributes to smart growth practices by reclaiming underused space and establishing new growth in areas with existing infrastructure.
*Particularly in mill renovations, re-use can improve water quality and make riverfronts more accessible to pedestrians and for recreational purposes. 
*Preserves historic, cultural, or social icons important to community identity.
*Can support a variety of businesses, interests, and needs of the community. Often are great for mixed use developments, which provide high density housing helping to prevent sprawl, conserves natural resources, agricultural land, and forests by concentrating development.
*Improves environmental health through remediation of degraded and contaminated buildings and land.
*Reduces auto dependency by concentrating development to cultivate healthier communities while mitigating greenhouse gas emissions. 
*Reuse converts areas that are a drain on taxes and municipal services into financial assets through improved property values, higher property taxes, and often new sources of revenue. 
*Produces employment opportunities for local workers (which can cut commuting behavior). 
*Surrounding property owners tend to reinvest, making their properties more valuable and typically resulting in a higher tax yield for the community. 
*The environmental remediation of Brownfields leads to environmental improvements to adjacent waterways.

==History==
The term [[Brownfield]] was coined on June 28, 1992, at a U.S. congressional field hearing hosted by the Northeast Midwest Congressional Coalition, and the first detailed policy analysis of the issue was convened by the Cuyahoga County Planning Commission. The next year, the EPA selected [[Cuyahoga County]] as its first Brownfield pilot project.<ref name=Portland />

There are a number of laws and incentives that have helped to promote the re-use of Brownfield sites. Among the most notable are:<ref name=Portland />

*Brownfields Tax Incentive (1994): environmental cleanup costs are fully deductible in the year they are sustained, rather than capitalized and spread over time. Improvements to the program in 2006 expanded the tax incentive to include petroleum cleanup.
*Brownfield Revitalization and Environmental Restoration Act (2002): provides grants for inventorying, characterizing, assessing, remediating, and conducting planning related to Brownfield sites. Defines a "Brownfield site,” and also exempts from liability under CERCLA some property owners that may have had land contaminated by nearby property possessed by other owners.

==Associated costs==
Associated costs with brownfield remediation include:<ref name=Portland />
*Site assessment
*Assessment costs can range from $20,000 to upwards of $500,000 depending on many factors like lot size and pollution level.
*Environmental remediation
*Environmental remediation insurance
*Developers can invest in cost-cap or pollution legal liability
*Financing premiums
*Lenders are less likely to invest so acquiring financing is often difficult. 
*Legal fees
*There are generally more legal consideration with Brownfields so developers may need to pay higher filing fees at the very least. 
*Extended development period
*For all the testing and financing remediations typically take more time and developers lose money on other projects that they can’t give their time to.

==Case Study==
[[File:Cost differential between Brownfield and Greenfield development in four case studies.png|thumb|Cost differential between Brownfield and Greenfield development in four case studies]]
[[File:Public benefits differential between Brownfield and Greenfield development.png|thumb|Public benefits differential between Brownfield and Greenfield development]]

As part of their discussion on industrial land, the [[Port of Portland (Oregon)|Port of Portland]], [[Portland Development Commission]], METRO and the Portland Bureau of Planning sponsored a Brownfield/Greenfield Development Cost Comparison Study with the goal of providing a better understanding of costs and issues associated with industrial development of Greenfield sites and the redevelopment of Brownfield sites. The study also compares [[Brownfield land|Brownfield]] and [[Greenfield land]] development costs. Using case-studies, the project compared costs associated with specific industrial projects between Brownfield sites and Greenfield sites. Four types of industrial development projects were identified: general manufacturing, high tech, warehouse and distribution, and industrial park.<ref name=Portland />

As shown, the cost of remediation in these case studies negates the savings in infrastructure costs. However, there are many benefits that are not exhibited in this analysis. Brownfield redevelopment poses a number of public benefits not accrued by Greenfield development such as: increased local income tax revenues, public land conservation and other environmental policy goals, the social benefits associated with contaminated site remediation and economic revitalization, and the enhancement of surrounding property values. The following is a monetary breakdown of the public benefits differential between Brownfield and Greenfield development.<ref name=Portland />

A great example of how Brownfield Remediation has been extremely successful is the Whitin Machine Works mill renovation in Whitinsvill, MA. A more detailed breakdown of this project, the Portland study, and an overview of the costs and benefits associated with remediation can be found in the imbedded file "Cost Benefits of Brownfield Remediation."

==Affordable Housing==
[[File:Hampden County employment data.pdf|thumbnail|Unemployment Data in Hampden County as it Relates to Affordable Housing]]
There are a number of ethical concerns involved with providing affordable housing. Such as:
*Good homes are generally located near good jobs, and without affordable housing units located near economic centers, the socially disadvantaged cannot hope to secure these jobs.
*Since socially disadvantaged often do not have cars and public transport is more common in city centers, they are even more limited in what jobs they can hold.
*The very poor who are forced to live where there is affordable housing and may not have access to transportation, may in turn not have access to town meetings where they could vote to make changes that would help them. 

The imbedded PDF, "Hampden County employment data," breaks down unemployment levels in 2000, 2005, and 2010 across Hampden County, MA. When compared to figures showing where the extremely affordable housing (homes valued at less than $50,000), is located, it becomes apparent that since not every town has affordable housing, the very poor are limited to areas with low income homes which may take them further away from jobs, and result in higher unemployment figures.

==References==
{{Reflist}}

[[Category:Environmental design]]