{{About|the novel by [[Benjamin Disraeli]]|the non-fiction book about Shirley Ardell Mason's treatment for [[dissociative identity disorder]]|Sybil (book)}}
{{Infobox book
| name          = Sybil, or The Two Nations
| title_orig    =
| translator    =
| image         = Sybil.jpg
| caption = First edition title page
| author        = [[Benjamin Disraeli]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| subject       =
| genre         = [[Fiction]]
| publisher     =
| pub_date      = 1845
| english_pub_date =
| media_type    =
| pages         =
| isbn          =
| oclc          =
| preceded_by   =
| followed_by   =
}}
'''''Sybil, or The Two Nations''''' is an 1845 novel by [[Benjamin Disraeli]]. Published in the same year as [[Friedrich Engels]]'s ''[[The Condition of the Working Class in England in 1844]]'', ''Sybil'' traces the plight of the [[working class]]es of England. Disraeli was interested in dealing with the horrific conditions in which the majority of England's working classes lived — or, what is generally called the [[Condition of England question]].

The book is a [[roman à thèse]], or a novel with a thesis — which was meant to create a furor over the squalor that was plaguing England's working class cities.

Disraeli's novel was made into a silent film called ''[[Sybil (1921 film)|Sybil]]'' in 1921, starring [[Evelyn Brent]] and [[Cowley Wright]].

Disraeli's interest in this subject stemmed from his interest in the [[Chartist movement]], a working-class political reformist movement that sought universal male suffrage and other parliamentary reforms. ([[Thomas Carlyle]] sums up the movement in his 1839 book "Chartism."<ref>https://en.wikisource.org/wiki/Chartism</ref>) Chartism failed as a parliamentary movement (three petitions to Parliament were rejected); however, five of the "Six Points" of Chartism would become a reality within a century of the group's formation.

Chartism demanded:
# [[Universal suffrage]] for men
# [[Secret Ballot]]
# Removal of property requirements for Parliament
# Salaries for [[Member of Parliament|Members of Parliament]] (MPs)
# Equal [[Electoral districts]]
# Annually elected [[Parliament of the United Kingdom|Parliament]]

==Characters==
*Sybil Gerard
*Charles Egremont
*Lord Marney
*Lord Henry Sydney
*Lord de Mowbray
*Rigby
*Taper
*Tadpole
*Lady St. Julians
*Marchioness of Deloraine
*Baptist Hatton
*Aubrey St. Lys
*Sidonia
*Devilsdust
*Dandy Mick
*Walter Gerard (Sybil's father)
*Stephen Morley
*Mr. Mountchesney

==See also==
{{portal|Novels|Victorian era}}
*[[One Nation Conservatism]]
*[[Coningsby (novel)]]
*[[Tancred (novel)]]
*[[The Difference Engine]], a [[steampunk]] novel containing alternate versions of several characters from ''Sybil''. It also features Disraeli himself as a character.

==External links==
{{Wikiquote}}
{{Wikisource|Sybil}}
{{Gutenberg|no=3760|name=Sybil, or The Two Nations}}
* {{librivox book | title=Sybil | author=Benjamin DISRAELI}}

==Bibliography==

===Editions===
There is no critical edition of Disraeli's novels. Most editions use the text of Longmans Collected Edition (1870–71).<ref name="Penguin Classics">{{cite book|last1=Disraeli|first1=Benjamin|editor1-last=Braun|editor1-first=Thom|title=Sybil|date=1987|publisher=Penguin|isbn=0-14-043134-9|pages=17}}</ref>
*Disraeli, Benjamin ''Sybil''. (Harmondsworth: [[Penguin Classics|Penguin]], 1987) [ISBN 0-14-043134-9]. Edited with an introduction by [[Rab Butler]] and notes by Thom Braun.
*Disraeli, Benjamin ''Sybil''. (Oxford: [[Oxford World's Classics|Oxford University Press]], 1998) [ISBN 0-19-283693-5]. Edited with an introduction and notes by Sheila Smith.

===Works of criticism===
*Braun, Thom ''Disraeli The Novelist''. (London: George Allen & Unwin, 1981) [ISBN 0 04 809017 4].

==References==
{{reflist}}

{{Benjamin Disraeli}}

[[Category:1845 novels]]
[[Category:British novels adapted into films]]
[[Category:Novels by Benjamin Disraeli]]
[[Category:British political novels]]
[[Category:Victorian novels]]


{{1840s-novel-stub}}