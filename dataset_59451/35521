{{good article}}
{{Use dmy dates|date=May 2015}}
{{Infobox Grand Prix race report
| Type = F1
| Country = Canada
| Grand Prix = Canadian
| Details ref = 
| Image = Circuit Gilles Villeneuve.svg
| Caption = Layout of the Circuit Gilles Villeneuve
| date = 7 June
| year = 2015
| Official name = Formula 1 [[Canadian Grand Prix|Grand Prix du Canada]] 2015<ref name="formal">{{cite web|url=http://www.formula1.com/content/fom-website/en/championship/races/2015/Canada.html|title=Formula 1 Grand Prix du Canada 2015|work=Formula1.com|publisher=[[Formula One Group|Formula One Administration]]|accessdate=24 May 2015}}</ref>
| Race_No = 7
| Season_No = 19
| location = [[Circuit Gilles Villeneuve]]<br />[[Montreal|Montreal, Quebec]], [[Canada]]
| Course = Street circuit
| Course_mi = 2.710
| Course_km = 4.361
| Distance_laps = 70
| Distance_mi = 189.700
| Distance_km = 305.270
| Weather = Sunny<br/>{{convert|20-21|C|F}} air temperature<br/>{{convert|39|C|F}} track temperature<br/>{{convert|4|m/s|ft/s|abbr=on}} wind from the north<ref name="Motorsport Total">{{cite web|title=GP Kanada in Montreal / Rennen|url=http://www.motorsport-total.com/f1/ergeb/2015/07/71.shtml|website=motorsport-total.com|accessdate=20 March 2016|language=German|trans_title=Canada GP in Montreal / Race}}</ref>
| Attendance = 
| Pole_Driver = [[Lewis Hamilton]]
| Pole_Team = [[Mercedes-Benz in Formula One|Mercedes]]
| Pole_Time = 1:14.393
| Pole_Country = United Kingdom
| Fast_Driver = [[Kimi Räikkönen]]
| Fast_Team = [[Scuderia Ferrari|Ferrari]]
| Fast_Time = 1:16.987
| Fast_Lap = 42
| Fast_Country = FIN
| First_Driver = [[Lewis Hamilton]]
| First_Team = [[Mercedes-Benz in Formula One|Mercedes]]
| First_Country = GBR
| Second_Driver = [[Nico Rosberg]]
| Second_Team = [[Mercedes-Benz in Formula One|Mercedes]]
| Second_Country = GER
| Third_Driver = [[Valtteri Bottas]]
| Third_Team = [[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| Third_Country = FIN
| Lapchart = {{F1Laps2015|CAN}}
}}

The '''2015 Canadian Grand Prix''', formally known as the '''Formula 1 Grand Prix du Canada 2015''', was a [[Formula One]] [[auto racing|motor race]] held on 7 June 2015 at the [[Circuit Gilles Villeneuve]] in [[Montreal|Montreal, Quebec]], Canada.<ref name="formal"/> The race was the seventh round of the [[2015 Formula One season|2015 season]], and marked the 52nd running of the [[Canadian Grand Prix]]. [[Daniel Ricciardo]] was the defending race winner, having won his first ever grand prix [[2014 Canadian Grand Prix|the year before]].

[[Lewis Hamilton]] of the [[Mercedes-Benz in Formula One|Mercedes]] team won the race from pole position, leading for all but one lap of the race. He extended his championship lead over teammate [[Nico Rosberg]], who finished second, to 17 points.

==Report==

===Background===
The pit-stop decision that seemingly cost [[Lewis Hamilton]] a victory at the [[2015 Monaco Grand Prix|previous race in Monaco]]<ref>{{cite web|last1=Coulthard|first1=David|authorlink=David Coulthard|title=Lewis Hamilton's trust in Mercedes will be dented by Monaco shocker|url=http://www.telegraph.co.uk/sport/motorsport/formulaone/lewishamilton/11628553/Lewis-Hamiltons-trust-in-Mercedes-will-be-shattered-by-Monaco-shocker.html|publisher=The Telegraph|accessdate=7 June 2015|date=25 May 2015}}</ref> was still a talking point when the paddock arrived in Canada. In Thursday's press conference however, Hamilton insisted that he "couldn't care less" about the incident, saying: "I can't do anything about the past so there's honestly no point in thinking about it, it is about trying to shape the future."<ref>{{cite web|title=Hamilton 'couldn't care less' about Monaco|url=http://www.planetf1.com/driver/10635/53539/Hamilton-couldnt-care-less-about-Monaco|website=planetf1.com|date=4 June 2015|deadurl=yes|archiveurl=https://web.archive.org/web/20150612162906/http://www.planetf1.com/driver/10635/53539/Hamilton-couldnt-care-less-about-Monaco|archivedate=12 June 2015|accessdate=21 January 2016}}</ref>

[[Max Verstappen]] received a five-place grid penalty for the Canadian Grand Prix after causing a collision with [[Romain Grosjean]] in Monaco.<ref name="Monaco penalty">{{cite web|title=Max penalised but blames Grosjean|url=http://www.planetf1.com/driver/55667788/51267/Max-penalised-but-blames-Grosjean|website=planetf1.com|date=24 May 2015|deadurl=yes|archiveurl=https://web.archive.org/web/20150706215428/http://www.planetf1.com/driver/55667788/51267/Max-penalised-but-blames-Grosjean|archivedate=6 July 2015|accessdate=21 January 2016}}</ref> During the final practice session, [[Scuderia Toro Rosso|Toro Rosso]] had to equip his car with the fifth internal combustion engine of the season. As he exceeded the limit of four units per power unit element per season, he received an additional ten-place grid penalty.<ref name="Verstappen penalty">{{cite web|title=FIA Stewards Decision&nbsp;– Document No. 20|url=http://www.fia.com/file/29003/download?token=RM_tDbUG|archive-url=https://web.archive.org/web/20150606213333/http://www.fia.com/file/29003/download?token=RM_tDbUG|work=FIA.com|publisher=[[Fédération Internationale de l'Automobile]]|date=6 June 2015|accessdate=6 June 2015|archive-date=6 June 2015}}</ref><ref>{{cite web|last1=Collantine|first1=Keith|title=Verstappen to receive second grid penalty|url=http://www.f1fanatic.co.uk/2015/06/06/verstappen-to-receive-second-grid-penalty/|publisher=F1Fanatic|accessdate=7 June 2015|date=6 June 2015}}</ref>

Minor changes were made to the last right-left chicane of the track. Prior to the first session of the weekend, the stewards placed a bollard in the run-off area to keep the drivers from re-entering the track too early. Ahead of the third practice session on Saturday, an additional orange-coloured kerb was added on the apex of turn 14. The stewards stated that "any driver who fails to negotiate turn 14 by using the track, and who makes contact with any part of the new kerb element, will not be required to keep to the left of the red and white polystyrene block, but must re-join the track safely."<ref>{{cite web|last1=Collantine|first1=Keith|title=New rule to stop drivers cutting final chicane|url=http://www.f1fanatic.co.uk/2015/06/06/new-rule-to-stop-drivers-cutting-final-chicane/|publisher=F1Fanatic|accessdate=7 June 2015|date=6 June 2015}}</ref>

Going into the race, Hamilton was leading the [[List of Formula One World Drivers' Champions|World Drivers' Championship]] by ten points from teammate [[Nico Rosberg]], who had won the two previous rounds in [[2015 Spanish Grand Prix|Spain]] and Monaco. [[Sebastian Vettel]] was third, a further 18 points behind Rosberg.<ref>{{cite web|title=2015 Driver Standings|url=http://www.formula1.com/content/fom-website/en/championship/results/2015-driver-standings.html|publisher=FIA|accessdate=4 August 2015|archiveurl=https://web.archive.org/web/20150604222543/http://www.formula1.com/content/fom-website/en/championship/results/2015-driver-standings.html|archivedate=4 June 2015}}</ref> In the [[List of Formula One World Constructors' Champions|Constructors' Championship]], [[Mercedes-Benz in Formula One|Mercedes]] was leading [[Scuderia Ferrari|Ferrari]] by 84 points, with [[Williams Grand Prix Engineering|Williams]] down in third.<ref>{{cite web|title=2015 Constructor Standings|url=http://www.formula1.com/content/fom-website/en/championship/results/2015-constructor-standings.html|publisher=FIA|accessdate=4 August 2015|archiveurl=https://web.archive.org/web/20150604221012/http://www.formula1.com/content/fom-website/en/championship/results/2015-constructor-standings.html|archivedate=4 June 2015}}</ref>

===Free practice===
Per the regulations for the 2015 season, three practice sessions were held, two 1.5-hour sessions on Friday and another one-hour session before qualifying on Saturday.<ref name=pracreg>{{cite web|title=Practice and qualifying|url=http://www.formula1.com/content/fom-website/en/championship/inside-f1/rules-regs/Practice_qualifying_and_race_start_procedure.html|website=formula1.com|publisher=FOM|accessdate=25 October 2015|archiveurl=https://web.archive.org/web/20150905082403/http://www.formula1.com/content/fom-website/en/championship/inside-f1/rules-regs/Practice_qualifying_and_race_start_procedure.html|archivedate=5 September 2015}}</ref> During first practice on Friday morning, Lewis Hamilton posted the fastest time&nbsp;– over a second quicker than the fastest time set in first practice in 2014&nbsp;– and four-tenths of a second ahead of his teammate Nico Rosberg in second. Both [[Lotus F1|Lotus]] and [[Force India]] showed good pace with Romain Grosjean and [[Nico Hülkenberg]] being third and fourth fastest respectively, though over 1.5 seconds behind Hamilton. Incidents during the session included Hamilton spinning out when his brakes locked up and [[Carlos Sainz Jr.]] stopping at the end of pit lane when he set out to post a timed lap towards the end of the session.<ref>{{cite web|last1=Collantine|first1=Keith|title=Hamilton leads the way despite spin|url=http://www.f1fanatic.co.uk/2015/06/05/hamilton-leads-the-way-despite-spin/|publisher=F1Fanatic|accessdate=7 June 2015|date=5 June 2015}}</ref>

In second practice, Hamilton was again quickest but was also caught out again. He went off at the turn ten hairpin and crashed his car while he was doing a run on intermediate tyres in wet conditions&nbsp;– a downpour of rain affected the second half of the session. Most teams sent their drivers out only in the dry first half, setting times on the super-soft tyre compound. Ferrari looked to be significantly closer to Mercedes, with Sebastian Vettel and [[Kimi Räikkönen]] finishing second and third respectively, less than 0.4 seconds behind Hamilton. Lotus confirmed their good pace<ref>{{cite web|last1=Collantine|first1=Keith|title=Lotus and Ferrari stand out in rain-hit practice|url=http://www.f1fanatic.co.uk/2015/06/06/lotus-and-ferrari-stand-out-in-rain-hit-practice/|publisher=F1Fanatic|accessdate=7 June 2015|date=6 June 2015}}</ref> from the first session when [[Pastor Maldonado]] was fifth fastest, behind Rosberg.<ref>{{cite web|last1=Collantine|first1=Keith|title=Hamilton quickest again&nbsp;– then crashes in the rain|url=http://www.f1fanatic.co.uk/2015/06/05/hamilton-quickest-again-then-crashes-in-the-rain/|publisher=F1Fanatic|accessdate=7 June 2015|date=5 June 2015}}</ref>

The third practice session on Saturday morning was disrupted by two red flag periods. Twenty minutes into practice, [[Felipe Nasr]] lost control of his car when he weaved from side to side on track to increase his tyre temperatures while his [[drag reduction system]] (DRS) was open on the first part of the Casino straight, and crashed into the barrier at the inside of the track. After the red flag was lifted, only twelve minutes of the session remained and all drivers took to the track to try out the super-soft compound. This did not last long however, as [[Jenson Button]] had to park his car at turn seven, reporting a problem with his power unit. This brought out the red flag once again, effectively ending the session. Nico Rosberg was fastest, half a second ahead of Räikkönen, while Hamilton drove just nine laps, ending the practice with the slowest time set.<ref>{{cite web|last1=Collantine|first1=Keith|title=Rosberg on top as Nasr crash disrupts practice|url=http://www.f1fanatic.co.uk/2015/06/06/rosberg-on-top-as-nasr-crash-disrupts-practice/|publisher=F1Fanatic|accessdate=7 June 2015|date=6 June 2015}}</ref>

===Qualifying===
[[File:Lotus duo in pit exit.jpg|thumb|During Q3, both [[Lotus F1|Lotus]] drivers exited the pit lane simultaneously.]]
Qualifying consisted of three parts, 18, 15 and 12 minutes in length respectively, with five drivers eliminated from competing after each of the first two sessions.<ref name=pracreg /> The 45 minutes of qualifying were split into parts of eighteen, fifteen and twelve minutes of running respectively. While [[Sauber]] were able to place Felipe Nasr in a new car in time for qualifying following his accident in practice, Jenson Button's problems could not be sorted out and he missed the session, meaning that his participation in the race would be left to a decision by the race stewards.

During the first part of qualifying (Q1), Mercedes was able to refrain from using the faster super-soft tyres, nevertheless setting times below 1:16. Fastest in the session was Romain Grosjean, who was narrowly quicker than the Mercedes drivers, but on the softer tyre compound. With Button not participating, four drivers were left to be eliminated. The two [[Marussia F1|Manor]] drivers once more did not make the cut and took 18th and 19th on the grid, with [[Roberto Merhi]] out-qualifying his teammate [[Will Stevens]] for the first time in a qualifying that both drivers participated in. Joining them on the sidelines were two more prominent drivers: both Sebastian Vettel and [[Felipe Massa]] suffered from problems with their power units, finishing 16th and 17th respectively.

Lewis Hamilton was quickest in Q2, just 0.012 seconds ahead of his teammate Nico Rosberg. The Mercedes power unit proved its superiority on the high-speed Montreal track, with all remaining Mercedes-powered cars making it into Q3, leaving Kimi Räikkönen and the two [[Red Bull Racing|Red Bull]] drivers to take the remaining three places. This was the first time in 2015 that Force India was able to get both cars into Q3 in qualifying, having last done so at the [[2014 German Grand Prix]].

As the top ten took to the track for the final part of qualifying, Nico Rosberg was unable to beat his time set in Q2 with his first fast lap. Hamilton however improved on his time, being more than three-tenths of a second faster than Rosberg. When both drivers were unable to improve on their times with their second timed laps, Hamilton took the 44th [[pole position]] of his career. Row two was taken up by the two Finns, Räikkönen and [[Valtteri Bottas]], who narrowly beat the two Lotuses of Romain Grosjean and Pastor Maldonado. Both Lotus drivers had emerged from their pit boxes simultaneously for their final timed laps, entering the track side-by-side. The two Red Bulls finished eighth and ninth, splitting the two Force India cars of Nico Hülkenberg and [[Sergio Pérez]].<ref name=guardianquali>{{cite web|last1=Richards|first1=Giles|title=Lewis Hamilton eclipses Nico Rosberg to claim Canadian Grand Prix pole|url=https://www.theguardian.com/sport/2015/jun/06/lewis-hamilton-nico-rosberg-canadian-grand-prix-pole|publisher=The Guardian|accessdate=7 June 2015|date=6 June 2015}}</ref><ref>{{cite web|last1=Collantine|first1=Keith|title=Hamilton beats Rosberg to pole as Vettel falls in Q1|url=http://www.f1fanatic.co.uk/2015/06/06/hamilton-beats-rosberg-to-pole-as-vettel-falls-in-q1/|publisher=F1Fanatic|accessdate=7 June 2015|date=6 June 2015}}</ref>

====Post-qualifying====
In an interview following qualifying, Lewis Hamilton expressed that it was "very special" to get another pole in Canada, since Canada had been the scene of his first race win in [[2007 Canadian Grand Prix|2007]].<ref name=guardianquali /> Sebastian Vettel lamented his qualifying performance, telling German TV that a small part of the car was broken but that he was confident to be able to make up places during the race, pointing to Kimi Räikkönen's fast pace in qualifying.<ref>{{cite web|last1=Brümmer|first1=Elmar|title=Lahmgelegt von Elektro-Teufeln|url=http://www.sueddeutsche.de/sport/sebastian-vettel-in-montreal-lahmgelegt-von-elektro-teufeln-1.2507416|publisher=Süddeutsche Zeitung|accessdate=7 June 2015|language=German|date=7 June 2015|trans_title=Crippled by electrical gremlins}}</ref>

Since he did not post a lap time within 107% of the fastest time during Q1, Jenson Button was required to apply to the race stewards to be allowed to the start the race; they granted him permission to do so.<ref name="Button starts">{{cite web|title=FIA Stewards Decision&nbsp;– Document No. 26|url=http://www.fia.com/file/29020/download?token=kr_wYnRE|archive-url=https://web.archive.org/web/20150606213748/http://www.fia.com/file/29020/download?token=kr_wYnRE|work=FIA.com|publisher=[[Fédération Internationale de l'Automobile]]|date=6 June 2015|accessdate=6 June 2015|archive-date=6 June 2015}}</ref> After qualifying, [[McLaren]] had to fit his car with a fifth heat motor generator unit (MGU-H) and a fifth turbo charger for the season. As this exceeded the limit of four units per power unit element, Button was later handed ten-place and five-place grid penalties. As he was already due to start from the back of the grid, the penalty was substituted with a drive-through penalty, to be served within the first three laps of the race.<ref name="Button penalties">{{cite web|title=FIA Stewards Decision&nbsp;– Document No. 36|url=http://www.fia.com/file/29070/download?token=X_iTFagj|archive-url=https://web.archive.org/web/20150607172618/http://www.fia.com/file/29070/download?token=X_iTFagj|work=FIA.com|publisher=[[Fédération Internationale de l'Automobile]]|date=7 June 2015|accessdate=7 June 2015|archive-date=7 June 2015}}</ref>

Sebastian Vettel was handed a five-place grid penalty for overtaking Manor's Roberto Merhi under red flags during the third free practice session.<ref name="Vettel penalty">{{cite web|title=FIA Stewards Decision&nbsp;– Document No. 27|url=http://www.fia.com/file/29017/download?token=ZjfT-4ag|archive-url=https://web.archive.org/web/20150606214322/http://www.fia.com/file/29017/download?token=ZjfT-4ag|work=FIA.com|publisher=[[Fédération Internationale de l'Automobile]]|date=6 June 2015|accessdate=6 June 2015|archive-date=6 June 2015}}</ref> After Max Verstappen qualified twelfth for the race, he was unable to be placed the full fifteen places back on the grid from both his penalties. As grid penalties were no longer carried over to subsequent races in 2015, he was instead handed a ten-second penalty, to be served with his first pit stop during the race.<ref>{{cite web|last1=Collantine|first1=Keith|title=Verstappen to serve ten-second time penalty in race|url=http://www.f1fanatic.co.uk/2015/06/06/verstappen-to-serve-ten-second-time-penalty-in-race/|publisher=F1Fanatic|accessdate=7 June 2015|date=6 June 2015}}</ref>

===Race===
[[File:2015 Canadian GP opening lap.jpg|thumb|The start of the race]]
At the start of the race, all drivers at the front of the grid got away without incident and with no changes in position. On the outside of turn three, Nico Hülkenberg was able to get around Pastor Maldonado for sixth position. Sebastian Vettel and Felipe Massa, who both started from the rear end of the grid, made up places early in the race, running in twelfth and thirteenth by lap 9, when Vettel came into the pit lane for his first stop. The pit stop did not go well, as his car was stationary for about six seconds. Massa in turn went past [[Marcus Ericsson]] for eleventh two laps later in a wheel-to-wheel manoeuvre through turns one and two. Meanwhile, at the front, Lewis Hamilton had built a sufficient gap to second-placed Rosberg to deny Rosberg the chance to use DRS and try to overtake.

By lap 14, Massa had passed [[2014 Canadian Grand Prix|2014 race]] winner [[Daniel Ricciardo]] to move up into tenth position. The first pit stops at the front began by lap 18, when Maldonado made a pit stop from seventh. While Massa passed [[Daniil Kvyat]] for seventh place on lap 21, Sebastian Vettel was stuck in 16th behind the McLaren of [[Fernando Alonso]], who was told to save fuel, a problem that also afflicted his teammate Jenson Button. By lap 23 however, Vettel had gone past Alonso and the two Toro Rosso cars of Verstappen and Sainz to move up into 13th.

[[File:Ericsson leading Massa and Vettel.jpg|thumb|Both [[Felipe Massa]] (left) and [[Sebastian Vettel]] (right) made their way through the field after a poor qualifying.]]
On lap 28, Kimi Räikkönen was the first of the top runners to pit, coming out in fourth, but spun at the hairpin in a repeat of an incident from 2014. The incident cost him twelve seconds and allowed Valtteri Bottas to stay ahead of him in third after he made a pit stop on lap 30. Hamilton made a pit stop on the same lap with Rosberg following suit two laps later, both without incident. At this point, Rosberg was about 1.5 seconds behind his teammate. Another four laps later, Sebastian Vettel came in for his second and final stop of the race. Massa came in for his only stop on lap 38, putting on the super-soft tyres, and dropped from sixth to ninth. Räikkönen made a pit stop again on lap 42, staying in fourth position ahead of the Lotus of Romain Grosjean.

On lap 46, Vettel went past Hülkenberg for eighth at the final chicane. There was no contact between the two drivers, but the Force India car spun in order to avoid the wall on the outside of the final corner. Two laps later, Fernando Alonso was the first driver to retire from the race, his third consecutive retirement. On lap 52, Romain Grosjean tried to lap Will Stevens, cutting his rear left tyre in the process. The incident also brought him a five-second time penalty. The other Lotus of Pastor Maldonado also lost a position when Vettel moved past him for fifth on lap 56. Jenson Button joined his teammate in retirement two laps later, being called into the pit lane by his team. Felipe Massa went up into sixth overtaking Maldonado on lap 64. For the remaining laps, Hamilton managed to control the lead and went on to win in Montreal for the fourth time, finishing 2.2 seconds ahead of Rosberg, with Bottas in third a further 38 seconds behind.<ref>{{cite web|last1=Ostlere|first1=Lawrence|title=F1: Canadian GP&nbsp;– as it happened|url=https://www.theguardian.com/sport/live/2015/jun/07/f1-canadian-gp-live-lap-by-lap|publisher=The Guardian|accessdate=8 June 2015|date=7 June 2015}}</ref>

===Post-race===
[[File:Hamilton Canada 2015.jpg|thumb|[[Lewis Hamilton]] won the race for [[Mercedes-Benz in Formula One|Mercedes]].]]
During the podium interview, conducted by [[Ted Kravitz]], a reporter for [[Sky Sports F1]], Lewis Hamilton expressed delight at his victory, saying that he needed the victory after the disappointment of Monaco. While he stated that the car suffered from [[Understeer and oversteer|understeer]] throughout the race, he felt that he "always had it under control", saying: "I had a bit of time in my pocket to be able to pull it out when I needed to, so it was never too serious." In turn, Nico Rosberg felt that his qualifying performance on Saturday "[made] that big difference" and had cost him the chance at victory.<ref>{{cite web|title=2015 Canadian Grand Prix - Sunday Race Press Conference|url=http://www.fia.com/news/2015-canadian-grand-prix-sunday-race-press-conference-0|publisher=FIA|accessdate=10 June 2015|date=7 June 2015}}</ref>

Following the race, Kimi Räikkönen apologised to his team over the radio for his spin,<ref>{{cite web|last1=Collantine|first1=Keith|title=Raikkonen baffled by repeat of hairpin spin|url=http://www.f1fanatic.co.uk/2015/06/07/raikkonen-baffled-by-repeat-of-hairpin-spin/|publisher=F1Fanatic|accessdate=8 June 2015|date=7 June 2015}}</ref> which many felt cost him a podium finish.<ref>{{cite web|last1=Richards|first1=Giles|title=Five things we learned from the F1 Canadian Grand Prix|url=https://www.theguardian.com/sport/blog/2015/jun/08/five-things-canadian-grand-prix|publisher=The Guardian|accessdate=10 June 2015|date=8 June 2015}}</ref><ref>{{cite web|title=F1 Canadian Grand Prix: Raikkonen perplexed by 'stupid' podium-costing spin|url=http://www.crash.net/f1/news/219741/1/raikkonen-perplexed-by-stupid-podiumcosting-spin.html|website=crash.net|accessdate=10 June 2015|date=8 June 2015}}</ref> In post-race interviews, he explained that an issue with the torque map, triggered by the pit stop, had caused him to spin out at the hairpin.<ref>{{cite web|last1=Barretto|first1=Lawrence|title=Canadian GP: Kimi Raikkonen blames Ferrari torque map for spin|url=http://www.autosport.com/news/report.php/id/119377|website=autosport.com|accessdate=8 June 2015|date=7 June 2015}}</ref> After finishing sixth from 15th on the grid, Felipe Massa was happy with a "good race", but lamented his problems in qualifying as he felt a podium finish would have been in reach, should he have started further up the order.<ref>{{cite web|title=F1 Canadian Grand Prix: Massa happy despite podium miss|url=http://www.crash.net/f1/news/219779/1/massa-happy-despite-podium-miss.html|website=crash.net|accessdate=10 June 2015|date=9 June 2015}}</ref>

For causing the collision with Will Stevens, Romain Grosjean was given two penalty points on his [[FIA Super Licence]] and said after the race: "I thought I was past the Manor, but it was soon clear that I wasn't. It was my fault entirely and I apologise for it. You never stop learning as a driver."<ref>{{cite web|last1=Edmondson|first1=Laurence|title=Romain Grosjean takes the blame and two penalty points for clash with Will Stevens|url=http://www.espn.co.uk/f1/story/_/id/13033158/romain-grosjean-takes-blame-two-penalty-points-clash-stevens|website=espn.co.uk|accessdate=10 June 2015|date=8 June 2015}}</ref> Meanwhile, Sebastian Vettel insisted that "it was fairly clear [with Hülkenberg] [...] When I got close enough I went around the outside, braking later and still managing to get the corner. I was clearly ahead and then I saw him opening the brakes into the last part of the chicane, so I reacted and jumped the second part because if I started on the track, we would have crashed."<ref>{{cite web|title=F1 Canadian Grand Prix: Charging Vettel hints at what could have been|url=http://www.crash.net/f1/news/219737/1/charging-vettel-hints-at-what-could-have-been.html|website=crash.net|accessdate=10 June 2015|date=8 June 2015}}</ref> Nico Hülkenberg in turn declared that Vettel "didn't leave me anywhere to go so to avoid contact I kept braking, lost the rear and spun". He did however voice satisfaction with his result, finishing eighth and taking four championship points.<ref>{{cite web|title=F1 Canadian Grand Prix: Hulkenberg: Vettel left me nowhere to go|url=http://www.crash.net/f1/news/219754/1/hulkenberg-vettel-left-me-nowhere-to-go.html|website=crash.net|accessdate=10 June 2015|date=8 June 2015}}</ref>

==Classification==

===Qualifying===
{| class="wikitable" style="font-size: 85%;"
|-
!rowspan="2"|{{Tooltip|Pos.|Qualifying position}}
!rowspan="2"|{{Tooltip|Car<br>no.|Number}}
!rowspan="2"|Driver
!rowspan="2"|Constructor
!colspan="3"|{{nowrap|Qualifying times}}
!rowspan="2"|{{Tooltip|Final<br>grid|Final grid position}}
|-
!Q1
!Q2
!Q3
|-
! 1
| align="center" |44
| {{flagicon|GBR}} [[Lewis Hamilton]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| 1:15.895
| '''1:14.661'''
| '''1:14.393'''
| 1
|-
! 2
| align="center" |6
| {{flagicon|GER}} [[Nico Rosberg]]
| [[Mercedes-Benz in Formula One|Mercedes]]
| 1:15.893
| 1:14.673
| 1:14.702
| 2
|-
! 3
| align="center" |7
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| [[Scuderia Ferrari|Ferrari]]
| 1:16.259
| 1:15.348
| 1:15.014
| 3
|-
! 4
| align="center" |77
| {{flagicon|FIN}} [[Valtteri Bottas]]
| [[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:16.552
| 1:15.506
| 1:15.102
| 4
|-
! 5
| align="center" |8
| {{flagicon|FRA}} [[Romain Grosjean]]
| [[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| '''1:15.833'''
| 1:15.187
| 1:15.194
| 5
|-
! 6
| align="center" |13
| {{nowrap|{{flagicon|VEN}} [[Pastor Maldonado]]}}
| [[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:16.098
| 1:15.622
| 1:15.329
| 6
|-
! 7
| align="center" |27
| {{flagicon|GER}} [[Nico Hülkenberg]]
| {{nowrap|[[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]}}
| 1:16.186
| 1:15.706
| 1:15.614
| 7
|-
! 8
| align="center" |26
| {{flagicon|RUS}} [[Daniil Kvyat]]
| [[Red Bull Racing]]-[[Renault in Formula One|Renault]]
| 1:16.415
| 1:15.891
| 1:16.079
| 8
|-
! 9
| align="center" |3
| {{flagicon|AUS}} [[Daniel Ricciardo]]
| [[Red Bull Racing]]-[[Renault in Formula One|Renault]]
| 1:16.410
| 1:16.006
| 1:16.114
| 9
|-
! 10
| align="center" |11
| {{flagicon|MEX}} [[Sergio Pérez]]
| {{nowrap|[[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]}}
| 1:16.827
| 1:15.974
| 1:16.338
| 10
|-
! 11
| align="center" |55
| {{flagicon|ESP}} [[Carlos Sainz Jr.]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Renault in Formula One|Renault]]
| 1:16.611
| 1:16.042
|
| 11
|-
! 12
| align="center" |33
| {{flagicon|NED}} [[Max Verstappen]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Renault in Formula One|Renault]]
| 1:16.361
| 1:16.245
|
| 19{{ref|1|1}}
|-
! 13
| align="center" |9
| {{flagicon|SWE}} [[Marcus Ericsson]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 1:16.796
| 1:16.262
|
| 12
|-
! 14
| align="center" |14
| {{flagicon|ESP}} [[Fernando Alonso]]
| [[McLaren]]-[[Honda in Formula One|Honda]]
| 1:17.012
| 1:16.276
|
| 13
|-
! 15
| align="center" |12
| {{flagicon|BRA}} [[Felipe Nasr]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| 1:16.968
| 1:16.620
|
| 14
|-
! 16
| align="center" |5
| {{flagicon|GER}} [[Sebastian Vettel]]
| [[Scuderia Ferrari|Ferrari]]
| 1:17.344
|
| 
| 18{{ref|2|2}}
|-
! 17
| align="center" |19
| {{flagicon|BRA}} [[Felipe Massa]]
| [[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| 1:17.886
|
|
| 15
|-
! 18
| align="center" |98
| {{flagicon|ESP}} [[Roberto Merhi]]
| [[Marussia F1|Marussia]]-[[Scuderia Ferrari|Ferrari]]
| 1:19.133
|
|
| 16
|-
! 19
| align="center" |28
| {{flagicon|GBR}} [[Will Stevens]]
| [[Marussia F1|Marussia]]-[[Scuderia Ferrari|Ferrari]]
| 1:19.157
|
|
| 17
|-
! colspan=8 | 107% time: 1:21.141
|-
! —
| align="center" |22
| {{flagicon|GBR}} [[Jenson Button]]
| [[McLaren]]-[[Honda in Formula One|Honda]]
| no time
|
|
| 20{{ref|3|3}}
|-
! colspan=8 | Source:<ref>{{cite web|title=2015 Canadian Grand Prix – Qualifying Official Classification|url=http://www.fia.com/file/29024/download?token=-qDMX-AD|archive-url=https://web.archive.org/web/20150607130951/http://www.fia.com/file/29023/download?token=4b0rn_rw|work=FIA.com|publisher=[[Fédération Internationale de l'Automobile]]|date=6 June 2015|accessdate=7 June 2015|archive-date=7 June 2015}}</ref><ref>{{cite web|title=2015 Canadian Grand Prix – Official Starting Grid|url=http://www.fia.com/file/29071/download?token=JGnnUo1L|archive-url=https://web.archive.org/web/20150607174208/http://www.fia.com/file/29071/download?token=JGnnUo1L|publisher=[[Fédération Internationale de l'Automobile]]|date=7 June 2015|accessdate=7 June 2015|archive-date=7 June 2015}}</ref>
|}

;Notes
*{{note|1|1}} – Max Verstappen received a five-place grid penalty for causing an avoidable collision during the previous race and a ten-place grid penalty for exceeding the allowed internal combustion engine allocation.<ref name="Monaco penalty"/><ref name="Verstappen penalty"/><br>
*{{note|2|2}} – Sebastian Vettel received a five-place grid penalty for overtaking under red flags during the third free practice session.<ref name="Vettel penalty"/><br>
*{{note|3|3}} – Jenson Button received permission from the stewards to start the race despite not setting a qualifying time.<ref name="Button starts"/> He later received ten- and five-place grid penalties for exceeding the allowed allocation of two of his power unit components.<ref name="Button penalties"/>

===Race===
[[File:Ericsson Canada 2015.jpg|thumb|[[Marcus Ericsson]] stopped on the track on the way back to the pit lane after the end of the race.]]
{|class="wikitable" style="font-size: 85%;"
!{{Tooltip|Pos.|Position}}
!{{Tooltip|No.|Car number}}
!Driver
!Constructor
!Laps
!Time/Retired
!Grid
!Points
|-
! 1
| align="center" | 44
| {{flagicon|GBR}} '''[[Lewis Hamilton]]'''
| '''[[Mercedes-Benz in Formula One|Mercedes]]'''
| align="center" | 70
| 1:31:53.145
| align="center" | 1
| align="center" |  '''25'''
|-
! 2
| align="center" | 6
| {{flagicon|GER}} '''[[Nico Rosberg]]'''
| '''[[Mercedes-Benz in Formula One|Mercedes]]'''
| align="center" | 70
| +2.285
| align="center" | 2
| align="center" |  '''18'''
|-
! 3
| align="center" | 77
| {{flagicon|FIN}} '''[[Valtteri Bottas]]'''
| '''[[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| align="center" | 70
| +40.666
| align="center" | 4
| align="center" | '''15'''
|-
! 4
| align="center" | 7
| {{flagicon|FIN}} '''[[Kimi Räikkönen]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| align="center" | 70
| +45.625
| align="center" | 3
| align="center" |  '''12'''
|-
! 5
| align="center" | 5
| {{flagicon|GER}} '''[[Sebastian Vettel]]'''
| '''[[Scuderia Ferrari|Ferrari]]'''
| align="center" | 70
| +49.903
| align="center" | 18
| align="center" | '''10'''
|-
! 6
| align="center" | 19
| {{flagicon|BRA}} '''[[Felipe Massa]]'''
| '''[[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| align="center" | 70
| +56.381
| align="center" | 15
| align="center" | '''8'''
|-
! 7
| align="center" | 13
| {{flagicon|VEN}} '''[[Pastor Maldonado]]'''
| '''[[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| align="center" | 70
| +1:06.664
| align="center" | 6
| align="center" | '''6'''
|-
! 8
| align="center" | 27
| {{flagicon|GER}} '''[[Nico Hülkenberg]]'''
| '''[[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| align="center" | 69
| +1 Lap
| align="center" | 7
| align="center" | '''4'''
|-
! 9
| align="center" | 26
| {{flagicon|RUS}} '''[[Daniil Kvyat]]'''
| '''[[Red Bull Racing]]-[[Renault in Formula One|Renault]]'''
| align="center" | 69
| +1 Lap
| align="center" | 8
| align="center" | '''2'''
|-
! 10
| align="center" | 8
| {{flagicon|FRA}} '''[[Romain Grosjean]]'''
| '''[[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]'''
| align="center" | 69
| +1 Lap
| align="center" | 5
| align="center" | '''1'''
|-
! 11
| align="center" | 11
| {{flagicon|MEX}} [[Sergio Pérez]]
| [[Force India]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="center" | 69
| +1 Lap
| align="center" | 10
| 
|-
! 12
| align="center" | 55
| {{flagicon|ESP}} [[Carlos Sainz Jr.]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Renault in Formula One|Renault]]
| align="center" | 69
| +1 Lap
| align="center" | 11
| 
|-
! 13
| align="center" | 3
| {{flagicon|AUS}} [[Daniel Ricciardo]]
| [[Red Bull Racing]]-[[Renault in Formula One|Renault]]
| align="center" | 69
| +1 Lap
| align="center" | 9
|
|-
! 14
| align="center" | 9
| {{flagicon|SWE}} [[Marcus Ericsson]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| align="center" | 69
| +1 Lap
| align="center" | 12
| 
|-
! 15
| align="center" | 33
| {{flagicon|NED}} [[Max Verstappen]]
| [[Scuderia Toro Rosso|Toro Rosso]]-[[Renault in Formula One|Renault]]
| align="center" | 69
| +1 Lap
| align="center" | 19
|
|-
! 16
| align="center" | 12
| {{flagicon|BRA}} [[Felipe Nasr]]
| [[Sauber]]-[[Scuderia Ferrari|Ferrari]]
| align="center" | 68
| +2 Laps
| align="center" | 14
| 
|-
! 17
| align="center" | 28
| {{flagicon|GBR}} [[Will Stevens]]
| [[Marussia F1|Marussia]]-[[Scuderia Ferrari|Ferrari]]
| align="center" | 66
| +4 Laps
| align="center" | 17
|
|-
! Ret
| align="center" | 98
| {{flagicon|ESP}} [[Roberto Merhi]]
| [[Marussia F1|Marussia]]-[[Scuderia Ferrari|Ferrari]]
| align="center" | 57
| Driveshaft
| align="center" | 16
|
|-
! Ret
| align="center" | 22
| {{flagicon|GBR}} [[Jenson Button]]
| [[McLaren]]-[[Honda in Formula One|Honda]]
| align="center" | 54
| Exhaust
| align="center" | 20
| 
|-
! Ret
| align="center" | 14
| {{flagicon|ESP}} [[Fernando Alonso]]
| [[McLaren]]-[[Honda in Formula One|Honda]]
| align="center" | 44
| Exhaust
| align="center" | 13
|
|-
! colspan=8 | Source:<ref name="Motorsport Total"/><ref>{{cite web|url=http://www.formula1.com/content/fom-website/en/championship/results/2015-race-results/2015-canada-results/race.html|title=Formula 1 Grand Prix Du Canada 2015 – Race results|work=Formula1.com|publisher=[[Formula One Group|Formula One Administration]]|date=7 June 2015|accessdate=7 June 2015}}</ref>
|}

===Championship standings after the race===
{{col-start}}
{{col-2}}
;Drivers' Championship standings
{|class="wikitable" style="font-size: 85%;"
|-
!
!{{Tooltip|Pos.|Position}}
!Driver
!Points
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 1
| {{flagicon|GBR}} [[Lewis Hamilton]]
| align="left" | 151
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 2
| {{flagicon|GER}} [[Nico Rosberg]]
| align="left" | 134
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 3
| {{nowrap|{{flagicon|GER}} [[Sebastian Vettel]]}}
| align="left" | 108
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 4
| {{flagicon|FIN}} [[Kimi Räikkönen]]
| align="left" | 72
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 5
| {{flagicon|FIN}} [[Valtteri Bottas]]
| align="left" | 57
|-
|}
{{col-2}}
;Constructors' Championship standings
{|class="wikitable" style="font-size: 85%;"
|-
!
!{{Tooltip|Pos.|Position}}
!Constructor
!Points
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 1
| {{flagicon|DEU}} [[Mercedes-Benz in Formula One|Mercedes]]
| align="left" | 285
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 2
| {{flagicon|ITA}} [[Scuderia Ferrari|Ferrari]]
| align="left" | 180
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 3
| {{flagicon|GBR}} [[Williams Grand Prix Engineering|Williams]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="left" | 104
|-
|align="left"| [[File:1rightarrow blue.svg|10px]]
| align="center" | 4
| {{nowrap|{{flagicon|AUT}} [[Red Bull Racing]]-[[Renault in Formula One|Renault]]}}
| align="left" | 54
|-
|align="left"| [[File:1uparrow green.svg|10px]] 2
| align="center" | 5
| {{flagicon|GBR}} [[Lotus F1|Lotus]]-[[Mercedes AMG High Performance Powertrains|Mercedes]]
| align="left" | 23
|-
|}
{{col-end}}
* <small>'''Note''': Only the top five positions are included for both sets of standings.</small>

==References==
{{Reflist|30em}}

==External links==
* [https://www.formula1.com/content/fom-website/en/championship/races/2015/Canada.html Official event website on Formula1.com]

{{Commons category|2015 Canadian Grand Prix}}

{{F1 race report
| Name_of_race = [[Canadian Grand Prix]]
| Year_of_race = 2015
| Previous_race_in_season = [[2015 Monaco Grand Prix]]
| Next_race_in_season = [[2015 Austrian Grand Prix]]
| Previous_year's_race = [[2014 Canadian Grand Prix]]
| Next_year's_race = [[2016 Canadian Grand Prix]]
}}
{{F1GP 10-19}}

[[Category:Canadian Grand Prix]]
[[Category:2015 Formula One races|Canadian]]
[[Category:2015 in Canadian motorsport|Grand Prix]]
[[Category:June 2015 sports events]]
[[Category:21st century in Montreal]]
[[Category:2015 in Quebec]]