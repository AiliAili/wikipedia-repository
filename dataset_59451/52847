{{Infobox journal
| title = Small Business Economics
| cover =
| editor = [[Zoltan Acs|Z.J. Acs]],; [[David B. Audretsch|D.B. Audretsch]]
| discipline = [[Entrepreneurship]]
| former_names =
| abbreviation = Small Bus. Econ.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Quarterly
| history = 1989-present
| openaccess =
| license =
| impact = 1.795
| impact-year = 2015
| website = http://www.springer.com/business+%26+management/business+for+professionals/journal/11187
| link1 = http://www.springerlink.com/openurl.asp?genre=issue&issn=0921-898X&issue=current
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR = 0921898X
| OCLC = 20141957 
| LCCN = 89659272 
| CODEN = SBECEX
| ISSN = 0921-898X
| eISSN = 1573-0913
}}
'''''Small Business Economics''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by [[Springer Science+Business Media]] covering research into [[entrepreneurship]] from different disciplines, including [[economics]], [[finance]], [[management]], [[psychology]], and [[sociology]]. The [[editors-in-chief]] are Z.J. Acs ([[George Mason University]]) and D.B. Audretsch ([[Indiana University Bloomington]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|2|
* [[Academic OneFile]]
* [[Cengage]]
* [[Current Contents]]/Social & Behavioral Sciences
* [[EconLit]]
* [[Expanded Academic ASAP]]
* [[International Bibliography of Book Reviews]]
* [[International Bibliography of Periodical Literature]]
* [[International Bibliography of the Social Sciences]]
* [[ProQuest]]
* [[RePEc]]
* [[Scopus]]
* [[Social Science Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014[[impact factor]] of 1.795, ranking it in the top 20% of 245 journals in the category "Economics".<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Economics |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-12-13 |work=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/business+%26+management/business+for+professionals/journal/11187}}

[[Category:Business and management journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1989]]
[[Category:English-language journals]]