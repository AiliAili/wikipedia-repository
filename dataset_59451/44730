{{other uses|JAMA (disambiguation)}}
{{Infobox journal
| title = JAMA
| former_names= Transactions of the American Medical Association; Councilor's Bulletin; Bulletin of the American Medical Association; Journal of the American Medical Association
| cover   = [[File:JAMA,_The_Journal_of_the_American_Medical_Association,_May_26,_1993,_cover,_Jean_Metzinger,_Soldier_at_a_Game_of_Chess.jpg|200px|alt=Cover]]
| caption = Cover, May 26, 1993: [[Jean Metzinger]], ''[[Soldier at a Game of Chess]]''
| editor  = [[Howard C. Bauchner]]
| discipline = [[Medicine]]
| abbreviation = JAMA
| publisher  = [[American Medical Association]]
| country  = United States
| frequency  = 48/year
| history  = 1883–present
| openaccess = 
| license  = 
| impact  = 37.684
| impact-year = 2015
| website  = http://jama.jamanetwork.com/
| link1   = http://jama.jamanetwork.com/issue.aspx
| link1-name = Online access
| link2   = http://jama.jamanetwork.com/issues.aspx
| link2-name = Online archive
| JSTOR   = 
| OCLC   = 1124917 
| LCCN   = 82643544
| CODEN   = JAMAAP
| ISSN  = 0098-7484
| ISSN2 = 0002-9955
| ISSN2label = Until 1960:
| eISSN  = 1538-3598
}}

'''''JAMA: The Journal of the American Medical Association''''' is a [[peer-reviewed]] [[medical journal]] published 48 times a year by the [[American Medical Association]]. It publishes original research, reviews, and editorials covering all aspects of the [[Biomedicine|biomedical sciences]]. The journal was established in 1883 with [[Nathan Smith Davis]] as the founding editor. The journal's current [[editor-in-chief]] is [[Howard Bauchner]] of [[Boston University]], who succeeded [[Catherine DeAngelis]] on July 1, 2011.<ref>[http://chronicle.com/blogs/ticker/new-editor-in-chief-named-at-journal-of-the-american-medical-association/31246?sid=at&utm_source=at&utm_medium=en "New Editor in Chief Named at "Journal of the American Medical Association'" ''Chronicle of Higher Education,'' March 10, 2011]</ref>

== History ==
The journal was established in 1883 by the [[American Medical Association]] and superseded the ''Transactions of the American Medical Association''.<ref name=UWEB/> The ''Councilor's Bulletin'' was renamed the ''Bulletin of the American Medical Association'' which was later absorbed by the ''Journal of the American Medical Association''.<ref name=CASSI/> In 1960 the journal obtained its current title, ''JAMA: The Journal of the American Medical Association''.<ref name=MEDLINE/><ref name=LC>{{cite web |url=http://lccn.loc.gov/82643544 |title= ''JAMA: The Journal of the American Medical Association'' |work=Library of Congress Catalog |publisher=[[Library of Congress]] |accessdate=2014-12-27}}</ref> The journal is commonly referred to as ''JAMA''.

== Continuing medical education ==
''Continuing Education Opportunities for Physicians'' was a semiannual journal section providing lists for regional or national levels of [[continuing medical education]] (CME). ''JAMA'' had provided this information since 1937. Prior to 1955, the list was produced either quarterly or semiannually. Between 1955 and 1981, the list was available annually, as the number of CME offerings increased from 1,000 (1955) to 8,500 (1981). The JAMA website states that [[webinar]]s are available for CME.<ref name=CME1987>
{{Cite journal
 | title = Continuing Education Opportunities for Physicians 
 | journal = JAMA 
 | volume = 257 
 | issue = 1 
 | pages = 97–121 
 | publisher = American Medical Association 
 | date = January 2, 1987 
 | url = http://jama.ama-assn.org/content/257/1/97.full.pdf+html 
 | doi = 10.1001/jama.1987.03390010101048 
 | id = 
 | accessdate = 2010-12-18}}</ref>

== Publication of article by Barack Obama==
On 11 July 2016 ''JAMA'' published an article by [[Barack Obama]] titled "United States Health Care Reform: Progress to Date and Next Steps",<ref>{{cite journal|last1=Obama|first1=Barack|title=United States Health Care Reform - Progress to Date and Next Steps|journal=JAMA|date=July 11, 2016|volume=316|issue=5|pages=525–532|doi=10.1001/jama.2016.9797|url=http://jamanetwork.com/journals/jama/fullarticle/2533698|accessdate=23 January 2017}}</ref> which was the first academic paper published by a sitting [[U.S. President]]. The article was not subject to blind peer-review and argued for specific policies which future presidents could pursue in order to improve national health care reform implementation.<ref>#ObamaJAMA: Obama Just Became the First Sitting President to Publish an Academic Paper, Kelly Dickerson, July 13, 2016, Mic.com, https://mic.com/articles/148595/obamajama-obama-academic-paper-made-history#.zNIXflcV4</ref>

== Policy shift ==
After the controversial firing of an editor-in-chief, [[George D. Lundberg]], a process was put in place to ensure editorial freedom. A seven-member journal oversight committee was created to evaluate the editor-in-chief and to help ensure editorial independence. Since its inception, the committee has met at least once a year. Presently, ''JAMA'' states that article content should be attributed to authors and not the publisher.<ref name="sciencenow.sciencemag">{{cite news |url= http://news.sciencemag.org/1999/01/jama-editor-gets-boot |first= Constance |last= Holden |title= JAMA Editor Gets the Boot |work= Science Now |date= 15 January 1999 |publisher= [[Science (journal)|Science]]}}</ref><ref>{{cite journal |first= Jerome P. |last= Kassirer  |date= 27 May 1999 |title= Editorial Independence |journal= [[The New England Journal of Medicine]] |volume= 340 |issue= 21 |pages= 1671–2 |doi= 10.1056/NEJM199905273402109}}</ref><ref>[http://pubs.ama-assn.org/misc/conditions.dtl JAMA & Archives Conditions of Use] {{webarchive |url=https://web.archive.org/web/20071212144203/http://pubs.ama-assn.org/misc/conditions.dtl |date=December 12, 2007 }}</ref><ref>{{cite journal |url= http://jama.jamanetwork.com/article.aspx?articleid=190346 |author= Signatories of the Editorial Governance Plan |title= Editorial Governance for JAMA |date= 16 June 1999 |volume= 281 |issue= 26 |pages= 2240–2 |doi= 10.1001/jama.281.23.2240 |journal=JAMA}}</ref>

===Artwork===
From 1964 to 2013, the journal used images of artwork on its cover and published essays commenting on this artwork.<ref name="Levine">{{cite web | author = Levine, Jefferey M. | title = JAMA removes cover art, and why that matters | date = 6 November 2013 | work = KevinMD.com | url = http://www.kevinmd.com/blog/2013/11/jama-removes-cover-art-matters.html}}</ref> According to former editor George Lundberg, this practice was designed to link the humanities and medicine.<ref>{{cite journal | author = Showalter E | year = 1999 | title = Commentary: An inconclusive study | journal = BMJ | volume = 319 | issue = 7225 | pages = 1603–1605 | doi = 10.1136/bmj.319.7225.1603 | pmid=10600956 | pmc=28304}}</ref>  In 2013, a redesign moved the art feature to an inside page, replacing the cover with a table of contents.<ref name="Levine" /> The purpose of the redesign was to standardize the appearance of all journals in the [[List of American Medical Association journals|JAMA network]].<ref>{{cite journal | vauthors = Henry R, Bauchner H | year = 2013 | title = JAMA gets a new look! | journal = JAMA | volume = 310 | issue = 1 | page = 39 | doi = 10.1001/jama.2013.7053 | url = http://media.jamanetwork.com/news-item/also-appearing-in-this-issue-of-jama-14/}}</ref>

== Previous editors ==
The following persons have been editor-in-chief:<ref>{{cite journal | author = American Medical Association | year = 2015 | title = JAMA Masthead | volume = 313 | issue = 14 | pages = 1397–1398 | doi = 10.1001/jama.2014.11680 | url = http://jama.jamanetwork.com/article.aspx?articleid=2247150 | journal=JAMA}}</ref>
{{columns-list|colwidth=30em|
* [[Nathan S. Davis]] (1883–1888)
* John B. Hamilton (1889, 1893–1898)
* John H. Hollister (1889–1891)
* James C. Culbertson (1891–1893)
* Truman W. Miller (1899)
* George H. Simmons (1899–1924)
* [[Morris Fishbein]] (1924–1949)
* Austin Smith (1949–1958)
* Johnson F. Hammond (1958–1959)
* John H. Talbott (1959–1969)
* Hugh H. Hussey (1970–1973)
* Robert H. Moser (1973–1975)
* William R. Barclay (1975–1982)
* [[George D. Lundberg]] (1982–1999)
* [[Catherine D. DeAngelis]] (2000–2011)
}}

==Abstracting and indexing==
This journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic OneFile]]<ref name=UWEB/>
* [[Academic Search]]<ref name=UWEB/>
* [[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-27}}</ref>
* [[Biological Abstracts]]<ref name=UWEB/>
* [[CAB Abstracts]]<ref name= CABAB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title= Serials cited |work= [[CAB Abstracts]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-27}}</ref>
* [[Chemical Abstracts]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-27}}</ref>
* [[CINAHL]]<ref name=CINAHL>{{cite web |url=http://www.ebscohost.com/titleLists/ccf-coverage.htm |title=CINAHL Complete Database Coverage List |publisher=[[EBSCO Information Services]] |work=[[CINAHL]] |accessdate=2014-12-27}}</ref>
* [[Current Index to Statistics]]<ref name=UWEB/>
* [[Current Contents]]/Clinical Medicine<ref name=ISI/>
* Current Contents/Life Sciences<ref name=ISI/>
* [[Elsevier BIOBASE]]<ref name=UWEB>{{cite web |title= ''JAMA: The Journal of the American Medical Association'' |url= http://www.ulrichsweb.serialssolutions.com/title/1419685242719/41747 |work= [[Ulrichsweb]] |publisher= [[ProQuest]] |accessdate=2014-12-27 |subscription= yes}}</ref>
* [[Embase]]<ref name=UWEB/> 
* [[Global Health]]<ref name= CABGH>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/global-health/ |title= Serials cited |work= [[Global Health]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-27}}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/7501160 |title= ''JAMA'' |work= [[United States National Library of Medicine|NLM]] Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2014-12-27}}</ref>
* [[PsychINFO]]<ref>{{cite web |url=http://www.apa.org/pubs/databases/psycinfo/coverage.aspx |publisher=[[American Psychological Association]] |title=PsychINFO Journal Coverage |accessdate=2014-12-27}}</ref>
* [[Science Citation Index]]<ref name=ISI/>
* [[Scopus]]<ref name=UWEB/>
* [[Tropical Diseases Bulletin]]<ref name= CABTDB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/tropical-diseases-bulletin/ |title= Serials cited |work= [[Tropical Diseases Bulletin]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-27}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 37.684, ranking it 3rd out of 153 journals in the category "Medicine, General & Internal".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Medicine, General & Internal |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of American Medical Association journals]]

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://jama.ama-assn.org/}}
* [http://pubs.ama-assn.org/ American Medical Association Archives]
* [http://onlinebooks.library.upenn.edu/webbin/serial?id=jama Free copies of volumes 1-80 (1883-1923)], from the Internet Archive and HathiTrust

[[Category:American Medical Association academic journals]]
[[Category:Publications established in 1883]]
[[Category:English-language journals]]
[[Category:General medical journals]]
[[Category:Weekly journals]]
[[Category:1883 establishments in the United States]]