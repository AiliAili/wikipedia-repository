{{EngvarB|date=November 2015}}
{{Use dmy dates|date=November 2015}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name = Castle Richmond
| title_orig = 
| translator = 
| image = File:Castle Richmond.jpg
| caption = First edition title page
| author = [[Anthony Trollope]]
| illustrator = 
| cover_artist = 
| country = England
| language = English
| series = 
| genre = 
| publisher = [[Chapman and Hall]]
| release_date = 10 May 1860
| english_release_date =
| media_type = Print
| pages = 
| isbn = 
| preceded_by = [[The Bertrams]]
| followed_by = [[Framley Parsonage]]
}}
'''''Castle Richmond''''' is the third of five novels set in Ireland by [[Anthony Trollope]].  ''Castle Richmond'' was written between 4 August 1859 and 31 March 1860, and was published in three volumes on 10 May 1860.<ref>Richard Mullen with James Munson, ''The Penguin Companion to Trollope'' (London: Penguin Books, 1996), p. 62.</ref>  It was his tenth novel.  Trollope signed the contract for the novel on 2 August 1859.  He received £600, £200 more than the payment for his previous novel, ''The Bertrams,'' reflecting his growing popular success.<ref>Richard Mullen with James Munson, ''The Penguin Companion to Trollope'' (London: Penguin Books, 1996), p. 63.</ref>

''Castle Richmond'' is set in southwestern Ireland at beginning of the [[Great Famine (Ireland)|Irish famine]].  Castle Richmond is situated on the banks of the [[Blackwater River (Kerry)|Blackwater River]] in [[County Cork]].<ref>Anthony Trollope, ''Castle Richmond'' (New York: Dover Publications, 1984), p. 2.</ref>  Trollope's work in Ireland from 1841 to 1859 had given him an extensive knowledge of the island, and Richard Mullen has written that "All the principal strands of his life were formed in Ireland."<ref>Richard Mullen with James Munson, ''The Penguin Companion to Trollope'' (London: Penguin Books, 1996), p. 233.</ref>

The unusually (for Trollope) complicated plot features the competition of two Protestant cousins of English origin, Owen Fitzgerald and Herbert Fitzgerald, for the hand of Clara Desmond, the noble but impoverished daughter of the widowed Countess of Desmond, providing the novel's principal dramatic interest.  ''Castle Richmond'' was the first of several novels by Trollope in which [[bigamy]] played an important role.

The [[Great Famine (Ireland)|Irish famine]] and efforts by authorities to mitigate its effects are the subject of many scenes and the object of abundant commentary throughout.  The famine also occasions more explicit religious commentary than is typical in novels by Trollope.

The critical reception to the novel was limited but generally favourable.  However, ''Castle Richmond'' did not sell particularly well.<ref>Richard Mullen with James Munson, ''The Penguin Companion to Trollope'' (London: Penguin Books, 1996), p. 67.</ref>

==Sources==
{{reflist}}

==External links==
* [http://www.anthonytrollope.com/ Anthony Trollope] – Comprehensive summaries of all of Trollope's plots and characters as well as information on all things Trollopian.
* {{gutenberg |no=5897 |title=Castle Richmond}}
* {{librivox book | title=Castle Richmond | author=Anthony Trollope}}

{{Anthony Trollope}}

{{DEFAULTSORT:Castle Richmond}}
[[Category:1860 novels]]
[[Category:Novels by Anthony Trollope]]
[[Category:Novels set in County Cork]]
[[Category:Great Famine (Ireland)]]
[[Category:Chapman & Hall books]]