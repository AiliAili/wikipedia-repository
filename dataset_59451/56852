{{Infobox journal
| title         = European Journal of Immunology
| cover         = [[File:European Journal of Immunology.gif]]
| editor        = [[Eddy Liew]] (chair), [[Cate Livingstone]] (managing ed.)
| discipline    = [[Allergy]]. [[immunology]]
| language      = [[English language|English]]
| abbreviation  = Eur. J. Immunol.
| publisher     = [[Wiley-VCH]]
| country       = [[Germany]]
| frequency     = 12/year
| history       = 1971–present
| openaccess    = Online Open
| license       = 
| impact        = 5.179
| impact-year   = 2009
| website       = http://www3.interscience.wiley.com/journal/25061/home
| link1         = http://www.efis.org/
| link1-name    = European Federation of Immunological Societies
| link2         = 
| link2-name    = 
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 01568458
| LCCN          = 
| CODEN         = 
| ISSN          = 0014-2980
| eISSN         = 1521-4141
| boxwidth      = 
}}

The '''''European Journal of Immunology''''' is an academic journal of the [[European Federation of Immunological Societies]] covering  basic immunology research, with a primary focus on [[antigen]] processing, cellular [[immune response]], immunity to [[infection]], [[Immunotherapy|immunomodulation]], [[leukocyte]] signalling, [[clinical immunology]], [[innate immunity]], molecular immunology, and related new technology.

The journal's editor is Francesco Annunziato who is a Chairman of the Executive Committee.<ref>{{Cite journal|title = European Journal of Immunology|url = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1521-4141|doi = 10.1002/(issn)1521-4141}}</ref>

Professionals in the fields of immunology, biochemistry, infection, oncology, hematology, cell biology, rheumatology, endocrinology and molecular biology make up the journal's readership.<ref>{{Cite web|title = Wiley-VCH - European Journal of Immunology|url = http://www.wiley-vch.de/publish/en/journals/alphabeticIndex/2040/|website = www.wiley-vch.de|access-date = 2016-02-11}}</ref>

==References==
{{Reflist}}

[[Category:Publications established in 1971]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Academic journals associated with international learned and professional societies of Europe]]


{{medical-journal-stub}}