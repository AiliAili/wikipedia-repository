{{refimprove|date=June 2011}}
{{Infobox film
| name           = 5 Girls
| image          = 5_Girls_Poster.jpg
| image_size     =
| caption        =
| director       = Maria Finitzo
| producer       = Maria Finitzo<br />David E. Simpson <br /> Gordon Quinn (Executive Producer)<br />Jerry Blumenthal (Co-Executive Producer)
| writer         =
| narrator       =
| starring       =
| music          =
| cinematography = Dana Kupper
| editing        = David E. Simpson
| distributor    = [[Kartemquin Films]]
| released       = {{Film date|2001}}
| runtime        = 120 min
| country        = United States
| language       = English
| budget         =
| gross          =
}}
'''''5 Girls''''' is a documentary released in 2001 by [[Kartemquin Films]] for [[P.O.V.|PBS's P.O.V.]] series. The film follows five strong young women between the ages of 13 and 17. Unlike the myriad reports, books and "specials" that focus on young women as passive and powerless, ''5 Girls'' explores the ways these girls discover the resources necessary to successfully navigate the rocky waters of adolescence. It focuses on the positive ways girls learn to adapt to challenge in their lives by understanding and exercising choices, by believing in their strength when others do not and by resisting powerful cultural messages, which urge them to be silent.

Directed by Maria Finitzo, ''5 Girls'' made its television premiere on PBS's P.O.V. on October 2, 2001.

At the time of the film's release, The New York Times praised ''5 Girls'' for its "intimacy and candor".  Reminiscent of [[Michael_Apted|Michael Apted's]] classic [[Up_series|Up! series]], the film unfolds into a bold "sociological portrait" showing the transformation of each girl into a woman.<ref>[https://movies.nytimes.com/movie/264016/5-Girls/overview?scp=3&sq=Finitzo&st=cse/ "5 Girls (2001)".]  Southern, Nathan.  The New York Times.  Retrieved 26 Dec. 2011.</ref><ref>[https://www.nytimes.com/2001/10/01/arts/television-review-five-girls-in-search-of-the-best-in-themselves.html?scp=4&sq=Finitzo&st=cse/  "5 Girls-Television Review".]  Wertheimer, Ron.  The New York Times.  1 Oct. 2001.  Retrieved 26 Dec. 2011.</ref>  In 2002, ''5 Girls'' was awarded the Henry Hampton Award for Excellence in Film & Digital Media from the Council on Foundations.  The film also took home The Silver Award from the Chicago Film & Television Competition.<ref>[http://webapp.sundance.org/docsource/author/Maria%20Finitzo/ "Sundance DocSource."]  Sundance Institute. Retrieved 26 Jan. 2011.</ref>

In September 2007, Kartemquin Films released ''5 Girls'' on DVD.       

==References==
{{reflist}}

==External links==
* [http://www.kartemquin.com/films/5-girls/ "5 Girls" Official Site]
* [http://kartemquin.com/ Kartemquin Films]
* {{imdb title|id=0328808|title=5 Girls}}
* [http://www.pbs.org/pov/5girls/ "5 Girls - P.O.V."]

{{Kartemquin Films}}

[[Category:American documentary films]]
[[Category:American films]]
[[Category:2001 films]]
[[Category:POV (TV series) films]]
[[Category:Feminist films]]
[[Category:Documentary films about women]]
[[Category:2000s documentary films]]
[[Category:Documentary films about adolescence]]
[[Category:Kartemquin Films films]]