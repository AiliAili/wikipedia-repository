{{Use dmy dates|date=March 2011}}
{{Infobox military person
|name=Hans-Joachim Birkner
|birth_date=22 October 1921
|death_date={{death date and age|1944|12|14|1921|10|22|df=y}}
|image=Hans-Joachim Birkner (soldier).jpg
|birth_place=[[Schönwalde, Saxony-Anhalt|Schönwalde]], Germany
|death_place=[[Kraków]], Poland
|allegiance={{flag|Nazi Germany}}
|branch={{Luftwaffe}}
|serviceyears=1940–44
|rank=[[Leutnant]]
|unit=[[JG 52]]
|battles=World War II
*[[Eastern Front (World War II)|Eastern Front]]
|awards=[[Knight's Cross of the Iron Cross]]
|laterwork=}}

'''Hans-Joachim Birkner''' (22 October 1921 – 14 December 1944) was a [[List of German World War II jet aces|Luftwaffe fighter ace]] and recipient of the [[Knight's Cross of the Iron Cross]] ({{lang-de|Ritterkreuz des Eisernen Kreuzes}}) during World War II. The Knight's Cross of the Iron Cross was awarded to recognise extreme battlefield bravery or successful military leadership. Birkner was credited with 117 aerial victories—that is, 117 aerial combat encounters resulting in the destruction of the enemy aircraft.

==Career==
Birkner was born on 22 October 1921 at [[Schönwalde, Saxony-Anhalt|Schönwalde]] ([[East Prussia]]). ''[[Feldwebel]]'' Birkner was posted to 9./[[Jagdgeschwader 52]] (JG 52—52nd Fighter Wing) on the Eastern Front in summer 1943 and recorded his first victory, a [[P-39 Airacobra]], on 1 October.<ref group="Note">For an explanation of ''Luftwaffe'' unit designations see [[Luftwaffe Organisation|Organisation of the Luftwaffe during World War II]].</ref> By the end of the year he had claimed 24 victories in total. Over the next few months Birkner often flew as ''Rottenflieger'' ([[wing man]]) to the high scoring aces [[Günther Rall]] and [[Erich Hartmann]], claiming any of his victories whilst flying with them. On 15 January 1944 Birkner claimed five P-39 fighters shot down. In April, he claimed a further 29 victories, including six in one day on 19 April. In May he claimed 17 victories, including five on 30 May. In June he claimed a [[United States Army Air Forces]] (USAAF) [[P-51 Mustang]] escort fighter over Rumania on 24 June for his 92nd victory.

Birkner was awarded the [[Knight's Cross of the Iron Cross]] on 27 July 1944 for 98 victories. On 1 October 1944, Birkner was appointed ''[[Staffelkapitän]]'' (squadron leader) 9./JG 52. In October Birkner claimed another 18 victories, including his 100th victory on 14 October. He was the 95th ''Luftwaffe'' pilot to achieve the century mark.<ref>Obermaier 1989, p. 243.</ref>

Birkner was killed on 14 December 1944 when he suffered engine failure landing at [[Kraków|Krakau]] in a [[Messerschmitt Bf 109]] G-14.<ref>Weal 2003, p. 81.</ref>

Hans-Joachim Birkner was credited with 117 victories in 284 missions, and some 121 combats. All his victories were recorded over the Eastern Front. Included in his total are at least 15 [[Ilyushin Il-2|Ilyushin Il-2 Sturmovik]]s.<ref>Weal 2003, p. 80.</ref>

==Awards==
* [[Iron Cross]] (1939) 2nd and 1st Class
* [[Ehrenpokal der Luftwaffe]] on 24 April 1944 as ''[[Fahnenjunker]]''-''[[Feldwebel]]'' and pilot<ref>Patzwall 2008, p. 52.</ref>
* [[German Cross]] in Gold on 20 March 1944 as ''Fahnenjunker''-''Feldwebel'' in the 9./''Jagdgeschwader'' 52<ref>Patzwall & Scherzer 2001, p. 43.</ref>
* [[Knight's Cross of the Iron Cross]] on 27 July 1944 as ''Fahnenjunker''-''Feldwebel'' and pilot in the 9./''Jagdgeschwader'' 52<ref>Fellgiebel 2000, p. 133.</ref>{{refn|According to Scherzer as pilot in the III./''Jagdgeschwader'' 52.<ref>Scherzer 2007, p. 222.</ref>|group="Note"}}

==Notes==
{{Reflist|group="Note"}}

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
* {{Cite book
  |last=Weal
  |first=John
  |year=2001
  |title=Bf 109 Aces of the Russian Front
  |url=https://books.google.com/books?id=IzJUlCpm4zMC&printsec=frontcover
  |location=Oxford, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-84176-084-1
}}
* {{Cite book
  |last=Weal
  |first=John
  |year=2004
  |title=Jagdgeschwader 52: The Experten (Aviation Elite Units)
  |location=London, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-84176-786-4
}}
{{Refend}}

==External links==
*{{cite web | title=Aces of the Luftwaffe|work=Hans-Joachim Birkner| url=http://www.luftwaffe.cz/birkner.html|accessdate=20 February 2008}}
*{{cite web | title=Ritterkreuzträger 1939–45|work=Hans-Joachim Birkner| url=http://www.ritterkreuztraeger-1939-45.de/Luftwaffe/B/Bi/Birkner-Hans-Joachim.htm|accessdate=23 November 2011}}

{{Knight's Cross recipients of JG 52}}
{{Top German World War II Aces}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Birkner, Hans-Joachim}}
[[Category:1921 births]]
[[Category:1944 deaths]]
[[Category:People from East Prussia]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:German military personnel killed in World War II]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]