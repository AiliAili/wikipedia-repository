{{Use dmy dates|date=April 2012}}
{{good article}}
{{Infobox football biography
| image          = Jim Baxter in Scotland shirt.png
| caption        = Baxter during his time at [[Nottingham Forest]]
| fullname       = James Curran Baxter
| birth_date     = {{birth date|1939|9|29|df=yes}}
| birth_place    = [[Hill of Beath]], Scotland
| death_date     = {{Death date and age|2001|4|14|1939|9|29|df=y}}
| death_place    = [[Glasgow]], [[Scotland]]<ref>[http://news.bbc.co.uk/sport1/hi/scotland/1179640.stm "Rangers legend Baxter dies"] BBC Sport website (14 April 2001)</ref> 
| position       = [[Wing half|Left-half]]
| years1         = 1957–1960
| years2         = 1960–1965
| years3         = 1965–1967
| years4         = 1967
| years5         = 1967–1969
| years6         = 1969–1970
| clubs1         = [[Raith Rovers F.C.|Raith Rovers]]
| clubs2         = [[Rangers F.C.|Rangers]]
| clubs3         = [[Sunderland A.F.C.|Sunderland]]
| clubs4         = → [[Vancouver Royals|Vancouver Royal Canadians]] (loan)<ref>[http://www.nasljerseys.com/Players/B/Baxter.Jim.htm NASL profile]</ref>
| clubs5         = [[Nottingham Forest F.C.|Nottingham Forest]]
| clubs6         = [[Rangers F.C.|Rangers]]
| caps1          = 62
| goals1         = 3
| caps2          = 136
| goals2         = 18
| caps3          = 87
| goals3         = 10
| caps4          = 12
| goals4         = 2
| caps5          = 48
| goals5         = 3
| caps6          = 14
| goals6         = 1
| totalcaps      = 359
| totalgoals     = 37
| nationalyears1 = 1960–1967
| nationalteam1  = [[Scotland national football team|Scotland]]
| nationalcaps1  = 34
| nationalgoals1 = 3
}}
'''James Curran Baxter''' (29 September 1939 – 14 April 2001) was a Scottish professional [[Association football|footballer]] who played as a [[wing half|left half]]. He is generally regarded<ref name="BlackCatalogue" /> as one of the country's greatest ever players. He was born, educated and started his career in [[Fife]], but his peak playing years were in the early 1960s with the [[Glasgow]] club [[Rangers F.C.|Rangers]], whom he helped to win ten trophies between 1960 and 1965, and where he became known as "Slim Jim". However, he started drinking heavily during a four-month layoff caused by a leg fracture in December 1964, his fitness suffered, and he was [[Transfer (football)|transferred]] to [[Sunderland A.F.C.|Sunderland]] in summer 1965. In two and a half years at Sunderland he played 98 games and scored 12 goals, becoming known for drinking himself unconscious the night before a match and playing well the next day. At the end of 1967 Sunderland transferred him to [[Nottingham Forest F.C.|Nottingham Forest]], who gave him a [[Free transfer (football)|free transfer]] back to Rangers in 1969 after 50 games. After a further year with Rangers Baxter retired from football in 1970, at the age of 31.

From 1961 to 1967, he was a leading member of a strong Scottish international team that lost only once to [[England national football team|England]], in 1965, shortly after he recovered from the leg fracture. He thought his best international performance was a 2–1 win against England in 1963, when he scored both goals after Scotland were reduced to 10 players – left back [[Eric Caldow]] had his leg broken in a tackle with [[Bobby Smith (footballer, born 1933)|Bobby Smith]]. In the 1967 match against England, who had won the [[1966 FIFA World Cup|1966 World Cup]], he taunted the opposition by ball juggling while waiting for his team-mates to find good positions. Although he was given most of the credit for the 3–2 win, some commentators wished he had made an effort to run up a bigger score.

In his prime, Baxter was known for his ability to raise a team's morale, his good tactical vision, precise passing and ability to send opponents the wrong way – and for being a joker on the pitch. He also broke with Glasgow tradition by becoming friendly with several members of their major [[Old Firm|Glasgow rivals]], [[Celtic F.C.|Celtic]].

Although he gained a reputation as a womaniser when he moved to Glasgow, he married in 1965 and had two sons. The marriage broke up in 1981, and in 1983 he formed another relationship that lasted the rest of his life. After retiring from football he became manager of a [[Public house|pub]], and his continued heavy drinking damaged his liver so badly that he needed two [[Organ transplant|transplant]]s at the age of 55, after which he swore off [[Alcoholic beverage|alcohol]]. Baxter was also addicted to gambling, and is estimated to have lost between £250,000 and £500,000. After he died of [[pancreatic cancer]] in 2001, his funeral was held in [[Glasgow Cathedral]] and his ashes were buried at Rangers' [[Ibrox Stadium]]. In 2003, a statue was erected in his honour at his hometown, [[Hill of Beath]].

==Early life==
Baxter was born in [[Hill of Beath]], [[Fife]], on 29 September 1939 and was educated and started his career there. After leaving school he spent eight months as an apprentice [[Cabinet making|cabinet maker]], and then worked as a coal miner.<ref name="GuardianObit" />

His former headmaster James Carmichael took an interest in ex-pupils and encouraged Baxter to join local football team Halbeath Juveniles instead of one of the glamour clubs.<ref name="IndependentObit" /> Baxter went on to play for the Fife junior team, [[Crossgates Primrose J.F.C.|Crossgates Primrose]].<ref name="GuardianObit" /> Baxter joined [[Raith Rovers F.C.|Raith Rovers]] as a part-timer in 1957.<ref name="GuardianObit" /> He later said of his time with the two Fife clubs, "I would never have made it in today's circumstances. I needed bastards like Carmichael, Buckard, Ferrier, [[Bert Herdman|Herdman]] and [[Willie McNaught|McNaught]]. Young players like I was would simply tell them to get stuffed and take their talent elsewhere. I owe them."<ref name="IndependentObit" />

==Club career==

===Raith Rovers===
Baxter joined [[Raith Rovers F.C.|Raith Rovers]] as a part-timer in 1957.<ref name="GuardianObit" /> In his time at Raith he orchestrated a 3-2 win over Rangers at Ibrox. [[Scot Symon]] decided he wanted to sign him for Rangers from that performance.

Jim Baxter in an interview on his career was asked if joining the senior ranks was the beginning of his football education. Baxter found the idea of being given any sort of football education laughable before then commenting on [[Willie McNaught]]. Describing McNaught as, ‘a smashing guy’, Baxter explained how instead of thoughtlessly charging forward with the ball, McNaught suggested more considered football – change from running 30 yards to produce a five yard pass to running five yards and producing a 30 yard pass. This, Baxter then stated, was his football education with Willie McNaught the provider.<ref name = "qoswillie">[http://www.qosfc.com/new_newsview.aspx?newsid=1215 "Willie McNaught (and Ken)" www.qosfc.com 26 April 2012]</ref>

===Rangers===
In June 1960,<ref name="UEFABaxter">{{cite web | title=On This Day: 29 September | publisher=[[UEFA]]| url=http://www.uefa.com/uefa/history/OnThisDay/index,newsid=216506.html | accessdate=2009-01-05 | archiveurl=https://web.archive.org/web/20081209071201/http://www.uefa.com/uefa/history/onthisday/index,newsid=216506.html | archivedate=9 December 2008}}</ref> he joined the [[Glasgow]] team [[Rangers F.C.|Rangers]] for a [[transfer fee]] of £17,500, a Scottish record at the time.<ref name="GuardianObit" /><ref name="SundayMirror15Apr2001"/><!--
http://www.uefa.com/uefa/history/OnThisDay/index,newsid=216506.html not on Internet Archive on 24 Sep 2010, try again in Mar 2011
-->

His first two seasons at Ibrox were coupled with two years' [[National service]] as a [[Black Watch]] soldier.

{{Annotated image | float=right | caption=Baxter played his first game for [[Rangers F.C.|Rangers]] at [[inside left]],<ref name="GuardianObit" /> but he soon established himself as a [[left half]]<ref name="SundayMirror15Apr2001" />
| image=2-3-5 (pyramid).svg | width=190 | image-width=200 | height=280 
| annotations=
{{Annotation|45|63|Inside<br/>&nbsp;&nbsp;left&nbsp;&nbsp;&nbsp;|font-weight=bold|background-color=#ffffcc}}
{{Annotation|25|146|Left half|font-weight=bold|background-color=#ffffcc}}
}}
Baxter played for Rangers from 1960 to 1965, mainly as an attacking [[left half]]. During this period the team won the [[Scottish Football League Premier Division|Scottish League]] Championship in 1961, 1963 and 1964,<ref name="SundayMirror15Apr2001" /> and the [[Scottish Cup]] in 1964.<ref name="GuardianObit" /> Rangers fans remember him as "Slim Jim", and in 18 "[[Old Firm]]" games against local rivals [[Celtic F.C.|Celtic]] – 10 Scottish League, five [[Scottish League Cup]] and three Scottish Cup matches – he was only twice on the losing side.<ref name="RangersHOFBaxter" /> His first Rangers game  was in August 1960 at [[inside left]] against [[Partick Thistle F.C.|Partick Thistle]] in the Scottish League Cup. He scored his first goal for the club in November 1960, against [[Clyde F.C.|Clyde]],<ref name="GuardianObit" /> and in the same month scored an early goal in Rangers' 8–0 win over the German team [[Borussia Mönchengladbach]].<ref name="SundayMirror15Apr2001" /> In [[1961 European Cup Winners' Cup Final|1961]] Baxter played in the Rangers team that contested the first ever [[European Cup Winners' Cup]] Final, a [[two-legged tie]] that they lost 4–1 on aggregate to [[ACF Fiorentina|Fiorentina]].<ref name="RSSSF ECWC">{{cite web|url=http://www.rsssf.com/ec/ec196061.html#cwc |title=Cup Winners' Cup 1960-61 |publisher=[[Rec.Sport.Soccer Statistics Foundation|RSSSF]] |accessdate=2 June 2009}}</ref>
 
In December in the [[European Cup 1964–65]] Baxter played brilliantly to set up a 2–0 win for Rangers in an away game against [[SK Rapid Wien|Rapid Vienna]]. With the game won rather than play out time Baxter's confidence over extended into arrogance to make fun of his opponents with the ball. In the last minute he went to 'nutmeg' an opponent who was having none of it and broke Baxter's leg with this challenge.<ref>[[European Cup 1964–65]]</ref> This was to be a watershed for Baxter. During the four months in which he was unable to play he began drinking to his detriment. This ultimately was to curtail his playing career and his life.<ref name="IndependentObit" />

Just after Baxter's return to the first team, manager [[Scot Symon]] sold him in May 1965.

===Sunderland===
Baxter joined [[Sunderland A.F.C.|Sunderland]] for a [[transfer fee]] of £72,500, the highest ever paid to a Scottish club at the time.<ref name="GuardianObit" /><ref name="RangersHOFBaxter" /><ref name="IndependentSelfconfidentMaster" /> Baxter played 98 games for Sunderland in England's First Division (then the top tier), scoring 12 goals.<ref name="BlackCatalogue">{{cite book 
| title=Black Catalogue: We are Sunderland | editor=Ken Gambles | publisher=PDG Books Ltd | year=2005 | isbn=1-905519-02-8 
| author=Robson, Barry | chapter=The One and Only, Slim Jim | pages=1–4 | url=https://books.google.com/?id=7t2rTXPLs-oC&pg=PA3&dq=jim+baxter+football+rangers#PPA1,M1 | accessdate=2012-07-07 
}}</ref><ref name="GuardianObit" />

===Nottingham Forest===
In December 1967, Sunderland sold Baxter to English First Division club [[Nottingham Forest F.C.|Nottingham Forest]] for £100,000. While there he became friends with cricketer [[Gary Sobers]]. After playing 50 games for Forest, in 1969, Baxter was given a [[Free transfer (football)|free transfer]].

===Return to Rangers===
Baxter moved back to Rangers.<ref name="GuardianObit" /> His return to Rangers was brief, as he retired from football in 1970, aged only 31.<ref name="RangersHOFBaxter" /> By the end of his career with Rangers he had made 254 appearances for the club, including victories that led to three Scottish League Championships, three Scottish Cups and four [[Scottish League Cup]]s.<ref name="RangersHOFBaxter" />

{{clear}}

==International career==
In the 1960s Baxter gained 34 [[Cap (sport)|cap]]s as a member of strong [[Scotland national football team|Scottish teams]], which included [[Billy McNeill]], [[Pat Crerand]],<ref name="SundayMirror15Apr2001" /> [[John White (footballer born 1937)|John White]], [[Dave Mackay]], [[Denis Law]]<ref name="IndependentObit" /> and [[John Greig]].<ref>{{cite web | url=http://www.rangers.premiumtv.co.uk/page/halloffame/0,,5~1561270,00.html | accessdate=2009-01-05 | title=John Greig
}}</ref> In his international appearances he scored three goals, and Scotland won 21, drew 3 and lost 10 of these matches.<ref name="LondonHeartsBaxter" /> He made his international debut in November 1961, when Scotland beat [[Northern Ireland national football team|Northern Ireland]].<ref name="GuardianObit" /><ref name="LondonHeartsBaxter" /> Earlier in 1961 Scotland lost 9–3 to England at [[Wembley Stadium (1923)|Wembley]], and April 1962 Baxter and Crerand played brilliantly, helping Scotland to gain some revenge with a 2–0 win.<ref name="IndependentObit" /><ref>{{cite book | title=The Essential Shankly: Revealing the Kop Legend Who Launched a Thousand Quips | author=Keith, John | publisher=Robson | year=2001 | isbn=1-86105-465-3| chapter=Funny Gags and Short Jabs | page=161 | url=https://books.google.com/?id=5ZdDpQI2W8EC&pg=PA161&dq=%22jim+baxter%22+%22world+cup%22#PPA161,M1 | accessdate=2009-01-07 
}}</ref>

According to many commentators, his greatest performances were against England in 1963 and 1967.<ref name="GuardianObit" /><ref name="UEFABaxter" /><ref name="SundayMirror15Apr2001" /> Baxter regarded his performance in 1963 as the better of the two.<ref>{{cite video | people = Jim Baxter | title = The Jim Baxter Story | medium = DVD | publisher = John Williams Productions - JW129 |date = 2001 }}</ref> In the 1963 game Scotland were reduced to 10 men when their [[left back]] [[Eric Caldow]]'s leg was broken in a tackle – [[Substitute (football)|substitution]]s were not allowed in those days.<ref name="GuardianObit" />  Baxter, supported by Mackay, White and Law,<ref name="IndependentObit" /> led Scotland to a 2–1 win, scoring both of the goals,  the first being Baxter's first-ever [[Penalty kick (association football)|penalty kick]], for an English foul on [[Willie Henderson]].<ref>{{cite news | title=Flower of Scotland stirs the passions | author=Shaw, Phil | date=14 October 1999 | publisher=''[[The Independent]]'' | url=http://www.independent.co.uk/sport/football/scottish/flower-of-scotland-stirs-the-passions-742823.html | accessdate=2009-01-05 | location=London}}</ref> [[Bobby Moore]] thought this was the best team Scotland ever fielded.<ref name="Powell2002" />

The following year Scotland, again inspired by Baxter and Law, beat England 1–0, and only poor finishing prevented them from scoring a bigger win. In 1966, sixteen months after his leg had been broken, Baxter was no longer able to inspire his team-mates, and Scotland lost 4–3 to England.<ref name="IndependentObit" />

In the 1967 [[British Home Championship]], Baxter produced a dominating but controversial performance for Scotland, tantalising England, who had won the [[FIFA World Cup|World Cup]] in 1966,<ref name="IndependentObit" /> by playing "[[keepie uppie]]" (ball juggling) while waiting for team-mates to get into good positions.<ref name="Holt1990SportAndBritish">{{cite book 
| title=Sport and the British: A Modern History | author=Holt, Richard | publisher=Oxford University Press| year=1990 
| isbn=0-19-285229-9 | chapter=Englishness and Britishness | page=260 | url=https://books.google.com/?id=vtChCoG6veMC&pg=PA260&dq=jim+baxter+football+rangers#PPP12,M1 | accessdate=2009-01-05 
}}</ref> Some commentators accepted that humiliating the opposition was a valid objective, while others regarded it as childish and thought Scotland should have won a more convincing victory than the actual 3–2 scoreline.<ref name="SportAndNationalIdentity">{{cite book | title=Sport and National Identity in the Post-war World | editor=Smith, Adrian |editor2=Porter, Dilwyn | publisher=Routledge 
| year= 2004 | isbn=0-415-28300-0 | chapter=Cry for Us, Argentina: Sport and national identity in late twentieth-century Scotland | author=Ronald Kowalski | page=74 
| url=https://books.google.com/books?id=yZfYh7n2qTMC&pg=PA74&dq=%22Sport+and+National+Identity+in+the+Post-war+World%22+jim+baxter 
| accessdate=2009-01-05 }}</ref><ref>{{cite book | title=The Association Game: A History of British Football | author=Taylor, Matthew | publisher=Pearson Education | year=2008 
| isbn=0-582-50596-8 | chapter=Glory and Decline, 1961-1985 | pages=296–297 | url=https://books.google.com/books?id=J5oqBHq0XrEC&pg=PA296&dq=jim+baxter+football+rangers#PPP8,M1 | accessdate=2009-01-05 
}}</ref> Team-mate Denis Law expressed opinions on both sides of this debate, saying that Baxter was "the best player on the park" and the main reason for the Scots' victory,<ref name="DailyTelgraphObit" /> but complaining that Baxter's lack of urgency had prevented Scotland from thoroughly avenging the 9–3 defeat in April 1961.<ref name="IndependentObit">{{cite news | title=Jim Baxter | date=16 April 2001 | publisher=''[[The Independent]]'' | author=Dalyell, Tam | url=http://www.independent.co.uk/news/obituaries/jim-baxter-729021.html | accessdate=2009-01-05 | location=London}}</ref><ref name="SportAndNationalIdentity" /> [[Alex Ferguson]] said Baxter's performance "could have been set to music".<ref name="IndependentObit" /> In this game Baxter also conspired with [[Billy Bremner]] against [[Alan Ball, Jr.|Alan Ball]], sending Bremner a "50-50" pass, which allowed Bremner to "hit Ball like a train" when Ball contested possession.( Was it not Nobby Stiles ? )<ref name="GuardianObit" /> As Scotland were the first team to beat England after the [[1966 FIFA World Cup]], the [[Tartan Army]] proclaimed themselves "[[Unofficial Football World Championships|unofficial world champions]]".<ref>{{cite news | url=https://www.theguardian.com/football/2005/jun/08/theknowledge.sport | accessdate=2009-01-05 | title=Zimbabwe: Kings of the world |author1=Dart, James |author2=Ahmed, Zohaib | date=8 June 2005 | publisher=[[The Guardian]] | location=London}}</ref>

In October 1963, which may have been Baxter's best year overall, he played in the "rest of the world" in a match against England to celebrate the [[centenary]] of [[The Football Association]].<ref name="GuardianObit" /> He came on to the field in the second half, and his performance won the admiration of [[Ferenc Puskás]].<ref name="IndependentSelfconfidentMaster">{{cite news| url=http://www.independent.co.uk/sport/football/scottish/baxter-a-selfconfident-master-of-sublime-skills-681499.html | accessdate=2009-01-05 | title=Baxter a self-confident master of sublime skills | author=Ken Jones | date=16 April 2001 | publisher=[[The Independent]] | location=London}}</ref> However England won the match 2–1.<ref>{{cite web | url=http://www.thefa.com/England/SeniorTeam/Archive/matchstats.html?m=373 | archive-url=https://archive.is/20071029051823/http://www.thefa.com/England/SeniorTeam/Archive/matchstats.html?m=373 | dead-url=yes | archive-date=2007-10-29 | accessdate=2009-01-05 | title=England v Rest of World, 23 October 1963 }} </ref>

Scotland did not qualify for the final stage of the [[FIFA World Cup]] during Baxter's playing career. Scottish public opinion at the time blamed lack of commitment by the "Anglos", Scottish-born players who spent little or none of their playing careers in Scotland.<ref>{{cite book | title=The Association Game: A History of British Football | author=Taylor, Matthew | publisher=Pearson Education | year=2008 | isbn=0-582-50596-8 | chapter=Glory and Decline, 1961-1985 | page=100 | url=https://books.google.com/books?id=J5oqBHq0XrEC&pg=PA296&dq=jim+baxter+football+rangers#PPP8,M1 | accessdate=2009-01-05 }}</ref> However at the time beating England was more important to the Scots.<ref name="Powell2002">{{cite book | title=Bobby Moore: The Life and Times of a Sporting Hero | author=Powell, Jeff | publisher=Robson Books Ltd | edition=2 | year=2002 | isbn=1-86105-511-0 | url=https://books.google.com/?id=Cw3BkpXDOWQC&pg=PA121&dq=%22scotland%22+%22world+cup%22#PPA121,M1 | accessdate=2009-01-07 | chapter=Bloody Foreigners | pages=121=122 }}</ref><ref>{{cite book | title=Power, Corruption and Pies: Volume 2 | publisher=WSC Books Limited | year=2006 | isbn=0-9540134-8-4 | chapter=Working from Home | author=Gall, Ken | pages=154–155 | url=https://books.google.com/books?id=HuTdMMRxFucC&pg=PA155&dq=%22jim+baxter%22+%22world+cup%22#PPA155,M1 | accessdate=2009-01-07 }}</ref> In 1960–61, when Baxter played in all the qualifying matches for the [[1962 FIFA World Cup|1962 World Cup]], they finished first equal in their qualifying group but lost the play-off against [[Czechoslovakia national football team|Czechoslovakia]],<ref name="LondonHeartsBaxter">{{cite web | url=http://www.londonhearts.com/scotland/players/jamescurranbaxter.html | accessdate=2009-01-07 | title=Jim Baxter - Scotland Football Record from 09 Nov 1960  to  22 Nov 1967  | publisher=London Hearts Supporters Club}}</ref> who were runners-up to Brazil in the Final.<ref>{{cite web | url=http://www.fifa.com/worldcup/archive/edition=21/index.html | accessdate=2009-01-07 | title=1962 FIFA World Cup Chile | publisher=[[FIFA]] }}</ref> Four years later Baxter played in only two of the qualifying games,( however one of these games was against Italy where he made John Greig's winning goal after starting the move by taking the ball from Scotland's keeper )   <ref name="LondonHeartsBaxter" /> before breaking his leg in a club game in Vienna.<ref name="IndependentObit" /> Scotland finished second in their qualifying group, behind [[Italian national football team|Italy]].<ref>{{cite web | url=http://www.planetworldcup.com/CUPS/1966/wc66qualification.html | accessdate=2009-01-07 | title=World Cup 1966: The Qualification Rounds }}</ref> In 1968–69 he was not selected to play in any of the qualifiers for the [[1970 FIFA World Cup|1970 World Cup]].<ref name="LondonHeartsBaxter" />

===International goals===
:''Scores and results list Scotland's goal tally first.''
{| class="wikitable"
! # !! Date !! Venue !! Opponent !! Score !! Result !! Competition
|-
| 1. || 2 May 1962 || [[Hampden Park]], [[Glasgow]] || {{fb|URU}} || '''1'''–3 || 2–3 || [[Exhibition game|Friendly]]
|-
| 2.
| rowspan=2 | 6 April 1963 || rowspan=2 | [[Wembley Stadium (1923)|Wembley Stadium]], [[London]] || rowspan=2 | {{fb|ENG}} || '''1'''–0 || rowspan=2 | 2–1 || rowspan=2 | [[1963 British Home Championship]]
|-
| 3.
| '''2'''–0
|}

==Career statistics==

===International===
<ref>{{NFT player|pid=18038}}</ref>
{| class="wikitable" style="text-align:center"
! colspan=3 | [[Scotland national football team|Scotland national team]]
|-
!Year!!Apps!!Goals
|-
|1960||1||0
|-
|1961||7||0
|-
|1962||4||1
|-
|1963||7||2
|-
|1964||5||0
|-
|1965||3||0
|-
|1966||4||0
|-
|1967||3||0
|-
!Total||34||3
|}

==Personal life and retirement==
After moving to [[Rangers F.C.|Rangers]], Baxter became a notorious womaniser. In his words, "One day, I was a [[Raith Rovers F.C.|Raith Rovers]] player who couldnae pull the birds at the [[Cowdenbeath]] Palais. The next day I was in [[Glasgow]] and the girls were throwing themselves at me. It was certainly a change and I wasn't letting it go by." However, in 1965 he married Jean Ferguson, a hairdresser, and the couple brought up two sons Alan and Steven. His marriage to Jean broke down in 1981 and the two divorced. Jean married golfer William McCondichie three years later.<ref name="SundayMirror15Apr2001" /> In 1983 Baxter formed a relationship with Norma Morton, and the couple remained together until his death in 2001.<ref name="Daily Telegraph2001TrueBraveheart">{{cite news | url=http://www.telegraph.co.uk/sport/football/leagues/scottishpremier/3003687/Last-lament-for-true-braveheart.html | accessdate=2009-01-05 | title=Last lament for true braveheart | author=Cramb, Auslan | publisher=[[Daily Telegraph]] | location=London | date=2001-04-20}}</ref>

Baxter was free of the [[Sectarianism in Glasgow|sectarianism]] that marked the rivalry between [[Glasgow]]'s two leading teams. His close friends included the Celtic players [[Billy McNeill]], [[Pat Crerand]] and [[Mike Jackson (footballer)|Mike Jackson]], in defiance of the unwritten rule that rivals did not associate.<ref name="IndependentObit" /><ref>{{cite news | url=http://www.timesonline.co.uk/tol/sport/football/scotland/article2652410.ece | accessdate=2009-01-05 | title=Firm pals who broke the Glasgow mould | publisher=[[The Sunday Times]] | author=Baillie, Rodger 
 | location=London | date=2007-10-14}}</ref>

Like some other British football stars of the late 20th century, Baxter drank to excess,<ref name="SundayMirror15Apr2001" /><ref name="MudSweatBeers">{{cite book 
| title=Mud, Sweat and Beers: A Cultural History of Sport and Alcohol |author1=Collins, Tony |author2=Vamplew, Wray | publisher=Berg Publishers | year=2002 | isbn=1-85973-558-4 | chapter=A Little of What Does You Good?| page=104 | url=https://books.google.com/?id=IdZJaM5oUgcC&dq=jim+baxter+football+rangers | accessdate=2009-01-05 }}</ref> and at one point was said to be consuming three bottles of [[Bacardi]] a day.<ref name="SundayMirror15Apr2001" /> Scotland team-mate [[Dave Mackay (footballer born 1934)|Dave Mackay]] unsuccessfully advised him to train harder and live more sensibly.<ref name="GuardianObit" /> Baxter often got falling-down drunk the night before a match, but this did not seem to hamper his play, and team managers took little notice of his drinking.<ref name="BlackCatalogue" /> After retiring from football Baxter became a [[pub]] licensee, an unsuitable career for a problem drinker.<ref name="MudSweatBeers" /> At the age of 55 he needed two [[liver]] [[organ transplant|transplants]] in four days,<ref name="GuardianObit" /><ref name="MudSweatBeers" /> and promised to quit drinking.<ref name="SundayMirror15Apr2001" />

His other lifelong addiction was gambling, at which he lost £500,000 by his own estimate and £250,000 by third-party estimates.<ref name="GuardianObit">{{cite news | url=https://www.theguardian.com/news/2001/apr/16/guardianobituaries.football | accessdate=2009-01-05 | title=Jim Baxter | author=Glenn, Patrick  | date=16 April 2001 | publisher=''[[The Guardian]]'' | location=London}}</ref><ref name="SundayMirror15Apr2001" /> Later in his life, when asked if earning the huge incomes of footballers in later decades would have made a difference, he replied, "Aye, I would have gambled £50,000 a week on the horses instead of £100." <ref name="BBCStatue" />

In February 2001, Baxter was diagnosed as suffering from [[cancer]] of the [[pancreas]], and he died at his home on Glasgow's [[Glasgow#South Side|South Side]] on 14 April 2001, with his partner Norma and his sons Alan and Steven at his bedside.<ref name="SundayMirror15Apr2001" /> His funeral was held in [[Glasgow Cathedral]], where a reading was given by [[Gordon Brown]], a long-time fan of [[Raith Rovers F.C.]], where Baxter began his career.<ref name="Daily Telegraph2001TrueBraveheart" />

==Legacy==
[[File:Jim Baxter statue Hill Of Beath.jpg|thumb|Statue in Baxter's honour in his home town, [[Hill of Beath]] in [[Fife]] – with the ball on his famous left foot]]
Baxter was noted for accurate passes, for sending opponents the wrong way with a swivel of his hips, and for inspiring team-mates with his confident approach.<ref name="Daily Telegraph2001TrueBraveheart" /> [[Manchester United F.C.|Manchester United]] manager [[Alex Ferguson]] described Baxter as "arguably the best player to play in Scottish football"<ref name="DailyTelgraphObit">{{cite news | url=http://www.telegraph.co.uk/sport/football/3003288/Baxter-best-to-play-in-Scottish-football.html | accessdate=2009-01-05 
| title=Baxter 'best to play in Scottish football' | author=Gray, William | date=15 Apr 2001 | publisher=''[[Daily Telegraph]]'' | location=London
}}</ref> and "the greatest player I ever played with&nbsp;... He had touch, balance, vision and just this wonderful aura&nbsp;..." Rangers manager [[Willie Waddell]] said, "Jim was the finest [[left half]] ever produced by Rangers." [[Jimmy Johnstone]], who played for Ranger's great rivals Celtic, said shortly after Baxter's death, "He was a great man and a genius on the ball."<ref name="SundayMirror15Apr2001">{{cite news | newspaper=Sunday Mirror | date=15 April 2001 | author=McGill, Craig | title=Slim Jim Baxter 1939 - 2001: Death of a Legend | url=http://findarticles.com/p/articles/mi_qn4161/is_20010415/ai_n14526017 | accessdate=2009-01-05 }} {{Dead link|date=April 2012|bot=H3llBot}}</ref> Allegedly [[Pelé]] once said that Baxter should have been a [[Brazil national football team|Brazilian]].<ref name="BlackCatalogue" /> After Baxter's performance in the 1963 "rest of the world" vs England match, [[Ferenc Puskás]] asked, "Where has this fellow been hiding?"<ref name="IndependentSelfconfidentMaster" />  Shortly before he died [[George Best]] named Baxter as one of the best eleven players he had played with or against in an interview with [[FourFourTwo]] magazine.<ref>{{cite web |url=http://www.fourfourtwo.com/features/george-best-perfect-xi |title=George Best: Perfect XI |last=Massarella |first=Louis |date=December 2005 |website=Four Four Two |access-date=8 August 2015}}</ref>

Baxter is a member of the Rangers supporters' Hall of Fame,<ref name="RangersHOFBaxter" /> and one of the first 50 added to the Scottish Sporting Hall of Fame when it was created in 2005.<ref>{{cite news | url=http://news.bbc.co.uk/2/hi/uk_news/scotland/2530435.stm | accessdate=2009-01-05 | title=Scotland honours sporting legends | publisher=[[BBC]] | date=30 November 2002}}</ref> In 2004, he was also inducted into the [[Scottish Football Hall of Fame]].<ref>{{cite web | url=http://www.scottishhealthawards.com/www.sfmhalloffame.com/inductees_2004.html | title=Scottish Football Hall of Fame - 2004 inductees}}</ref>  In the December 1999 issue of [[World Soccer (magazine)|World Soccer magazine]] he was voted by a readers' poll into a list of the [[World Soccer (magazine)#The Greatest Players of the 20th century|100 greatest players of the 20th century]].<ref>http://www.englandfootballonline.com/TeamHons/HonsWldSocPlyrsCent.html</ref> He played most of his best football in his early twenties, before the leg fracture against [[Rapid Vienna]] in December 1964 and the start of his heavy drinking that made his nickname "Slim Jim" less appropriate.<ref name="IndependentObit" /><ref>{{cite web | url = http://sport.scotsman.com/top-stories/Flawed-legend--and-greater.2251226.jp | accessdate=2009-01-05 | title=Flawed legend - and greater for it | author=Pattullo, Alan | publisher=[[The Scotsman]] }}</ref>

Baxter attracted attention by his stylish play, controlling the game with "unhurried artistry" and refusing to conform with the "efficient" style that dominated English football or the energetic, physical style that was typical of Rangers at the time.<ref name="Holt1990SportAndBritish" /> He described his approach as "treating the ball like a woman. Give it a cuddle, caress it a wee bit, take your time, and you'll get the required response".<ref>{{cite book | title=Football Talk: The Language & Folklore of the World's Greatest Game | author=Seddon, Peter | publisher=Robson | year=2004 | isbn=1-86105-683-4 | chapter=Nothing but a Bag of Wind? | page=49 | url=https://books.google.com/books?id=7N3iYWotTPYC&pg=PA49&dq=jim+baxter+football+rangers#PPP7,M1 | accessdate=2009-01-05 }}</ref> Although Rangers insisted that players tuck their shirts completely into their shorts, Baxter always let part of his dangle over his left hip.<ref name="RangersHOFBaxter">{{cite web | url=http://www.rangers.premiumtv.co.uk/page/halloffame/0,,5~1561270,00.html | accessdate=2009-04-15 | title=Jim Baxter }}</ref> He was also noted as a joker on the pitch.<ref name="IndependentObit" /> After the [[replay (sports)|replay]] of the [[Scottish Cup]] Final against Celtic in 1963 he stuffed the match ball up his shirt and later gave it to a new member of the team. The [[Scottish Football Association|SFA]] insisted that the ball should be returned, and Rangers sent them a ball, but possibly not the match ball.<ref>{{cite web | url=http://www.dailyrecord.co.uk/football/2008/12/22/night-slim-jim-had-a-ball-86908-20989029/ | accessdate=2009-01-05 | title=Night Slim Jim Had A Ball | date=22 December 2008 | publisher=[[Daily Record (Scotland)|Daily Record]]
}}</ref>

The [[Tartan Army]] unsuccessfully attempted to get the new [[Wembley Stadium]] footbridge named after him,<ref>{{cite book 
| title=The Unofficial Football World Championships: An Alternative Soccer History | author=Brown, Paul | publisher=Tonto Books | year=2006| isbn=0-9552183-1-4 | chapter=Classic Matches | page=74 | url=https://books.google.com/?id=XktTup3O-fQC&pg=PA74&dq=jim+baxter+football+rangers#PPA5,M1 | accessdate=2009-01-05 
}}</ref><ref>{{cite web | url=http://www.lda.gov.uk/server/show/ConWebDoc.1071 | accessdate=2008-01-06 | title=Battle of the bridge commences | publisher=London Development Agency }}</ref> and on 24 February 2005 [[Scottish National Party]] [[Member of Parliament|MP]] [[Pete Wishart]] presented an [[Early Day Motion]] in the [[House of Commons of the United Kingdom|House of Commons]] supporting this campaign.<ref>{{cite web | url=http://www.currybet.net/cbet_blog/2005/03/baxter_vs_moore.php | accessdate=2009-01-05 | title=Baxter vs Moore - Costing you money in Parliament }}</ref><ref>{{cite web | url=http://www.westminster.snp.org/index.php?option=com_content&task=view&id=1378&Itemid=40 | accessdate=2009-01-05 | title=SNP MP's motion backs Jim Baxter bridge at Wembley | publisher=[[Scottish National Party]] }}</ref>

In 2003, a statue of Baxter was erected in his birthplace, [[Hill of Beath]] in [[Fife]], after a campaign that raised £80,000.<ref name="BlackCatalogue" /><ref name="BBCStatue">{{cite news | url=http://news.bbc.co.uk/1/hi/scotland/2962041.stm | accessdate=2009-01-05 | title=Statue to football legend unveiled | publisher=[[BBC]] | date=20 April 2003 }}</ref>

==Honours==
===Club===
;Rangers
*[[UEFA Cup Winners' Cup]] (Runners-up): [[1960–61 European Cup Winners' Cup|1960–61]]
*[[List of Scottish football champions#Scottish League First Division (1893–1975)|Scottish League First Division]]: [[1960–61 Scottish First Division|1960–61]], [[1962–63 Scottish First Division|1962–63]], [[1963–64 Scottish First Division|1963–64]]
*[[Scottish Cup]]: [[1961–62 Scottish Cup|1961–62]], [[1962–63 Scottish Cup|1962–63]], [[1963–64 Scottish Cup|1963–64]]
*[[Scottish League Cup]]: [[1960–61 Scottish League Cup|1960–61]], [[1961–62 Scottish League Cup|1961–62]], [[1963–64 Scottish League Cup|1963–64]], [[1964–65 Scottish League Cup|1964–65]]

===Individual===
*[[Scottish Football Hall of Fame]] (inaugural inductee)
*[[Scottish Sports Hall of Fame#Football|Scottish Sports Hall of Fame]] (inaugural inductee)
*[[Ballon d'Or (1956–2009)|Ballon d'Or]]: [[Ballon d'Or 1965|1965]] (13th place)<ref>{{cite web | url = http://www.rsssf.com/miscellaneous/europa-poy65.html | title = European Footballer of the Year ("Ballon d'Or") 1965 | publisher = RSSSF | date = 22 June 2005 | accessdate = 24 October 2016 | first = Pierrend | last = José Luis}}</ref>

==References==
{{reflist|30em}}

==External links==
*{{SFA profile}}
*[http://www.glasgowguide.co.uk/images_jbf1.html  Jim Baxter's Funeral Images @ Glasgow Guide]
*[http://www.neilbrown.newcastlefans.com/player/jimbaxter.html Jim Baxter's career stats]

{{Rangers F.C. Hall of Fame}}
{{Scottish Football Hall of Fame}}
{{Raith Rovers F.C. Hall of Fame}}

{{Authority control}}

{{DEFAULTSORT:Baxter, Jim}}
[[Category:Deaths from pancreatic cancer]]
[[Category:Nottingham Forest F.C. players]]
[[Category:People from Hill of Beath]]
[[Category:Raith Rovers F.C. players]]
[[Category:Rangers F.C. players]]
[[Category:Scottish Football League players]]
[[Category:Scottish footballers]]
[[Category:Scotland international footballers]]
[[Category:Scottish Sports Hall of Fame inductees]]
[[Category:Sunderland A.F.C. players]]
[[Category:English Football League players]]
[[Category:Black Watch soldiers]]
[[Category:1939 births]]
[[Category:2001 deaths]]
[[Category:Scottish Football Hall of Fame inductees]]
[[Category:Scottish Football League representative players]]
[[Category:Sportspeople from Fife]]
[[Category:Scotland under-23 international footballers]]
[[Category:Crossgates Primrose J.F.C. players]]
[[Category:Scottish expatriate footballers]]
[[Category:Vancouver Royals players]]
[[Category:United Soccer Association players]]
[[Category:Expatriate soccer players in Canada]]
[[Category:Association football wing halves]]