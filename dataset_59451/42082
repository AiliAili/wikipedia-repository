<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Trident
 | image=Ramphos Trident Amphib trike.jpg
 | caption=Ramphos Trident
}}{{Infobox Aircraft Type
 | type=[[Ultralight trike]]
 | national origin=[[Italy]]
 | manufacturer=[[Ramphos]]
 | designer=
 | first flight=
 | introduced=1998
 | retired=
 | status=In production (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!-- [[US$]] [[Euro|€]] [[Pound sterling|£]] (date) -->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Ramphos Trident''' is an [[Italy|Italian]] amphibious [[ultralight trike]], designed and produced by [[Ramphos]] of [[Fontanafredda]]. The aircraft is supplied as a kit for [[Homebuilt aircraft|amateur construction]] or as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 220. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
The Trident was designed to comply with the [[Fédération Aéronautique Internationale]] [[microlight]] category, including the category's maximum gross weight of {{convert|450|kg|lb|0|abbr=on}}. The Trident features a [[strut-braced]] [[hang glider]]-style [[high-wing]], weight-shift controls, a two-seats-in-[[tandem]] open cockpit with a rigid boat hull, retractable [[tricycle landing gear]] and a single engine in [[pusher configuration]].<ref name="WDLA11" />

The aircraft is made from bolted-together [[aluminum]] tubing, with its double surface wing covered in [[Dacron]] sailcloth and its boat hull made from either [[fibreglass]] or [[carbon fibre]] and [[Kevlar]]. Its {{convert|10.5|m|ft|1|abbr=on}} span Hazard wing has struts and uses an "A" frame weight-shift control bar. The powerplant is a twin cylinder, liquid-cooled, [[two-stroke]], [[dual-ignition]] {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] engine or a four-cylinder, air and liquid-cooled, [[four-stroke]], [[dual-ignition]] {{convert|80|hp|kW|0|abbr=on}} [[Rotax 912UL]] engine or a {{convert|78|hp|kW|0|abbr=on}} converted [[Smart Car]] four stroke [[turbocharged]] engine. All engines are fitted with a clutch that stops the propeller from turning when the engine is at idle to permit water handling. The boat hull features a water [[rudder]].<ref name="WDLA11" />

Starting in 2005 the frame and wing portion of the aircraft was taken from the [[Skyrider Sonic]] ultralight trike, built by [[Skyrider Flugschule]].<ref name="WDLA11" />
<!-- ==Operational history== -->

==Variants==
;Hydro
:Initial [[flying boat]] model that lacks wheeled landing gear. Introduced in 1998 and in production in 2013.<ref name="WDLA11" />
;Trident
:Amphibious model with fibreglass boat hull, in production in 2013.<ref name="WDLA11" />
;C
:Amphibious model with carbon fibre/Kevlar boat hull and [[lexan]] windows in the bottom of the hull to allow visibility downwards. In production in 2013.<ref name="WDLA11" />
<!-- ==Aircraft on display== -->

==Specifications (Trident) ==
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=10.5
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=15
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=200
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|fuel capacity= <!-- {{convert|XX|l}} or {{convert|XX|u.s.gal}} -->
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912UL]]
|eng1 type=four cylinder, liquid and air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=60
|eng1 hp=

|prop blade number=2
|prop name=composite
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=Y

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{Commons category}}
*{{Official website|http://www.ramphos.com/}}

[[Category:Italian sport aircraft 1990–1999]]
[[Category:Italian ultralight aircraft 1990–1999]]
[[Category:Single-engined pusher aircraft]]
[[Category:Ultralight trikes]]
[[Category:Homebuilt aircraft]]