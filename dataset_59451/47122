{{cleanup|reason=Reads like a class assignment, contains much irrelevant stuff (e.g. section on imaging techniques)|date=April 2015}}
'''Cognitive genomics''' (or neurative genomics) is the sub-field of [[genomics]] pertaining to cognitive function in which the [[gene]]s and non-coding sequences of an [[organism|organism's]] [[genome]] related to the health and activity of the [[brain]] are studied. By applying [[comparative genomics]], the genomes of multiple species are compared in order to identify genetic and [[phenotype|phenotypical]] differences between species. Observed phenotypical characteristics related to the neurological function include [[behavior]], [[personality type|personality]], [[neuroanatomy]], and [[neuropathology]]. The theory behind cognitive genomics is based on elements of [[genetics]], [[evolutionary biology]], [[molecular biology]], [[cognitive psychology]], [[behavioral psychology]], and [[neurophysiology]].

[[Intelligence]] is the most extensively studied behavioral [[trait (biology)|trait]].<ref>Plomin, R. & Spinath, F.M. (2004). “Intelligence: Genetics, Genes, and Genomics.” Journal of Personality and Social Psychology, 86(1): 112-129.</ref> In humans, approximately 70% of all genes are expressed in the brain.<ref name="Hariri">Hariri, A.R., & Weinberger, D.R. (2003). Imaging Genomics.</ref> Genetic variation accounts for 40% of phenotypical variation.<ref>Asherson, P. (2005). “Intelligence: Genetics, Genes, and Genomics.” Psychiatry, 4:12.</ref> Approaches in cognitive genomics have been used to investigate the genetic causes for many mental and [[neurodegeneration|neurodegenerative]] disorders including [[Down syndrome]], [[Major Depressive Disorder]], [[autism]], and [[Alzheimer's disease]].

== Cognitive genomics testing ==

=== Approaches ===

==== Evo-geno ====
The most commonly used approach to genome-investigation is evolutionary genomics biology, or evo-geno, in which the genomes of two species which share a common ancestor are compared.<ref name="Geschwind">Geschwind, D.H., & Konopka, G. (2010). “Human Brain Evolution: Harnessing the Genomics (R)evolution to Link Genes, Cognition, and Behavior”. Neuron, 68(2): 231-244</ref> A common example of evo-geno is comparative cognitive genomics testing between humans and [[chimpanzee]]s which shared an ancestor 6-7 million years ago.<ref name="pnas">Caceres. “Elevated gene expression levels distinguish human from non-human primate brains”. http://www.pnas.org/content/100/22/13030.full</ref> Patterns in local [[gene expression]] and [[gene splicing]] are examined to determine genomic differentiation. Comparative transcriptomic analyses conducted on primate brains to measure gene expression levels have shown significant differences between human and chimpanzee genomes.<ref name="Geschwind" /> The evo-geno approach was also used to verify the theory that humans and non-human primates share similar expression levels in energy metabolism-related genes which have implications for aging and neurodegenerative disease.<ref name="Geschwind" />

==== Evo-devo ====
Evolutionary development biology (evo-devo) approach compares cognitive and neuroanatomic development patterns between sets of species. Studies of human [[fetal|fetus]] brains reveal that almost a third of expressed genes are regionally differentiated, far more than non-human species.<ref name="Geschwind" /> This finding could potentially explain variations in cognitive development between individuals. Neuroanatomical evo-devo studies have connected higher brain order to [[brain lateralization]] which, though present in other species, is highly ordered in humans.

==== Evo-pheno and evo-patho ====
Evolutionary phenotype biology (evo-pheno) approach examines phenotype expression between species. Evolutionary pathology biology (evo-patho) approach investigates disease prevalence between species.

=== Imaging genomics ===

==== Candidate gene selection ====
In genomics, a gene being imaged and analyzed is referred to as a candidate gene. The ideal candidate genes for comparative genomic testing are genes that harbor well-defined functional polymorphisms with known effects on neuroanatomical and/or cognitive function.<ref name="Hariri" /> However, genes with either identified single-nucleotide polymorphisms or allele variations with potential functional implications on neuroanatomical systems suffice.<ref name="Hariri" /> The weaker the connection between the gene and the phenotype, the more difficult it is to establish causality through testing.<ref name="Hariri" />

==== Controlling for non-genetic factors ====
Non-genetic factors such as age, illness, injury, or substance abuse can have significant effects on gene expression and phenotypic variance.<ref name="Hariri" /> The identification and contribution of genetic variation to specific phenotypes can only be performed when other potential contributing factors can be matched across genotype groups.<ref name="Hariri" /> In the case of neuroimaging during task performance such as in fMRI, groups are matched by performance level. Non-genetic factors have a particularly large potential effect on cognitive development. In the case of autism, non-genetic factors account for 62% of disease risk.<ref name="bare_url_b">Digitale, E. (2011). “Non-genetic factors play surprisingly large role in determining autism.” Stanford School of Medicine, Stanford University. http://med.stanford.edu/ism/2011/july/autism.html</ref>

==== Task selection ====
In order to study the connection between a [[candidate gene]] and a proposed phenotype, a subject is often given a task to perform that elicits the behavioral phenotype while undergoing some form of [[neuroimaging]]. Many behavioral tasks used for genomic studies are modified versions of classic behavioral and [[neuropsychology|neuropsychological]] tests designed to investigate neural systems critical to particular behaviors.<ref name="Hariri" />

== Species used in comparative cognitive genomics ==

=== Humans ===
In 2003, the [[Human Genome Project]] produced the first complete human genome.<ref>U.S. Department of Energy, Office of Science, Office of Biological and Environmental Research, Human Genome Project. “About the Human Genome Project.”</ref> Despite the project’s success, very little is known about cognitive gene expression.<ref name="Interviewwith">Interview with Todd Preuss, PhD, Yerkes National Primate Research Center</ref> Prior to 2003, any evidence concerning human brain connectivity was based on [[post-mortem]] observations.<ref name="bare_url">Behrens. “Non-Invasive Mapping of Connections Between Human Thalamus and Cortex Using Diffusion Imagery.” http://cs.unc.edu/Research/MIDAG/defmreps/styner_www/public/DTI_tutorial/7%20Nat%20Neurosci%202003%20Behrens.pdf</ref> Due to ethical concerns, no invasive [[in vivo]] genomics studies have been performed on live humans.

=== Non-human primates ===
As the closest genetic relatives to humans, [[non-human primates]] are the most preferable genomics imaging subjects. In most cases, primates are imaged while under [[anesthesia]].<ref name="Interviewwith" /> Due to the high cost of raising and maintaining primate populations, genomic testing on non-human primates is typically performed at primate research facilities.

==== Chimpanzees ====
Chimpanzees (''Pan troglodytes'') are the closest genetic relatives to human, sharing 93.6% genetic similarity.<ref>Cohen, J. (2007). “Relative Differences: The Myth of the 1%”. Science, 29(316): 1836.</ref> It is believed that humans and chimpanzees shared a common genetic ancestor around 7 million years ago.<ref name="Interviewwith" /> The movement to sequence the chimpanzee genome began in 1998 and was given high priority by the US National Institutes of Health (NIH).<ref>Olson, M.V. & Varki, A. (2003). “Sequencing the Chimpanzee Genome: Insights Into Human Evolution and Disease.” Nature Reviews, 4: 20-28.</ref>

Currently, human and chimpanzees have the only sequenced genomes in the extended family of primates.<ref>Goodman, M. (2005). “Moving primate genomics beyond the chimpanzee genome.” Trend in Genetics, 21(9): 511-517.</ref> Some comparisons of autosomal intergenic non-repetitive DNA segments suggest as little as 1.24% genetic difference between humans and chimpanzees along certain sections.<ref>Chen, F.C. & Li, W.H. (2001). “Genomic Divergences between Humans and Other Hominoids and the Effective Population Size of the Common Ancestor of Humans and Chimpanzees.”</ref> Despite the genetic similarity, 80% of proteins between the two species are different which understates the clear phenotypical differences.<ref>Glazko. “Eighty percent of proteins are different between humans and chimpanzees.” https://homes.bio.psu.edu/people/faculty/nei/lab/2005-glazko-etal.pdf</ref>

==== Rhesus macaques ====
[[Rhesus macaque]]s (''Macaca mulatta'') exhibit a 93% genetic similarity to humans approximately.<ref>Tomlin, R. (2007). “DNA Sequence of Rhesus Macaque Has Evolutionary, Medical Implications”. http://www.bcm.edu/news/item.cfm?newsID=853</ref> They are often used as an out-group in human/chimpanzee genomic studies.<ref name="Interviewwith" /> Humans and rhesus macaques shared a common ancestor an estimated 25 million years ago.<ref name="pnas" />

==== Apes ====
Orangutans (''Pongo pygmaeus'') and gorillas (''Gorilla gorilla'') have been used in genomics testing but are not common subjects due to cost.<ref name="Interviewwith" />

== Imaging techniques ==

In the past decade, significantly more attention has been given to imaging genomics. Prior to 2003, only 10 peer-reviewed papers on imaging genomics were published compared to the 14 published in 2006 alone.<ref>Glahn, D.C., Paus, T., & Thompson, P.M. (2007). “Imaging Genomics: Mapping the Influence of Genetics on Brain Structure and Function.” Human Brain Mapping, 28:461–463.</ref>

=== fMRI ===
Functional magnetic resonance imaging ([[fMRI]]) is a widely available imaging technique that measures changes in blood flow in imaged tissue. It’s commonly used in neurophysiological studies to determine [[neuron]] activation.<ref name="Interviewwith" /> Unlike a standard [[MRI]], fMRI can be used to actively measure subject neuronal activity as the patient performs tasks. This technique isn’t used on conscious non-human primates due to the inability to restrain the primates during imaging.<ref name="Interviewwith" /> However, fMRI illustrates regional brain activity when performed on primates that performed tasks over an extended period of time and were then anesthetized.

=== DTI ===
[[Diffusion tensor imaging]] is an imaging technique that measures the diffusion of water across a membrane. In neurophysiological studies, it is used to determine neuronal connections along [[gray matter]] tracts.<ref name="bare_url" /> DTI is particularly effective in neural imaging due to the high degree of directional organization in neural tissue, specifically the connections between the [[cerebral cortex]] and the [[thalamus]].<ref name="bare_url" /> Due to its non-invasive nature, it is the most preferable imaging technique for use with non-human primates.<ref name="Interviewwith" />

=== FDG-PET ===
[[Positron emission tomography]] (PET) is a common diagnostic imaging technique where a patient is injected with a radioactive dye. Fluorodeoxyglucose (FDG) is a sugar derivative. In fluorodeoxyglucose positron emission tomography (FDG-PET), it is injected into the patient where it tends to accumulate around areas of the cortex that are, or have been recently, active.<ref name="Interviewwith" />

== Neurobehavioral and cognitive disorders ==

Despite what is sometimes reported, most behavioral or pathological phenotypes are not due to a single [[gene mutation]] but rather a complex genetic basis.<ref name="bare_url_a">McGuffin (2001). “Toward Behavioral Genomics”.</ref> However, there are some exceptions to this rule such as [[Huntington's disease]] which is caused by a single specific genetic disorder.<ref name="bare_url_a" /> The occurrence of neurobehavioral disorders is influenced by a number of factors, genetic and non-genetic.

=== Down syndrome ===
Down syndrome is a genetic syndrome marked by [[intellectual disability]] and distinct cranio-facial features and occurs in approximately 1 in 800 live births.<ref name="Fisch">Fisch, G. (2003). Genetics and Genomics of Neurobehavioral Disorders.</ref> Experts believe the genetic cause for the syndrome is a lack of genes in the [[21st chromosome]].<ref name="Fisch" /> However, the gene or genes responsible for the cognitive phenotype have yet to be discovered.

=== Fragile-X syndrome ===
[[Fragile-X syndrome]] is caused by a mutation of the FRAXA gene located in the [[X chromosome]].<ref name="Fisch" /> The syndrome is marked by intellectual disability (moderate in males, mild in females), language deficiency, and some [[autistic spectrum]] behaviors.<ref name="Fisch" />

=== Alzheimer’s disease ===
Alzheimer’s disease is a neurodegenerative disorder that causes age-correlated progressive cognitive decline.<ref name="Fisch" /> [[Animal models|animal model]] using mice have investigated the pathophysiology and suggest possible treatments such as [[immunization]] with [[amyloid beta]] and peripheral administration of [[antibodies]] against amyloid beta.<ref name="Fisch" /> Studies have linked Alzheimer’s with gene alterations causing SAMP8 [[protein]] abnormalities.<ref>Butterfield, D.A. & Poon, H.F. (2005). “The senescence-accelerated prone mouse (SAMP8): A model of age-related cognitive decline with relevance to alterations of the gene expression and protein abnormalities in Alzheimer’s disease.” Experimental Gerontology, 40:774–783.</ref>

=== Autism ===
Autism is a pervasive developmental disorder characterized by abnormal social development, inability to empathize and effectively communicate, and restricted patterns of interest.<ref name="Fisch" /> A possible neuroanatomical cause is the presence of tubers in the temporal lobe.<ref name="Fisch" /> As mentioned previously, non-genetic factors account for 62% of autism development risk.<ref name="bare_url_b" /> Autism is a human-specific disorder. As such, the genetic cause has been implicated to highly ordered brain lateralization exhibited by humans.<ref>Geschwind, D.H., & Konopka, G. (2010). “Human Brain Evolution: Harnessing the Genomics (R)evolution to Link Genes, Cognition, and Behavior”. Neuron, 68(2): 231-244.</ref> Two genes have been linked to autism and autism spectrum disorders (ASD): c3orf58 (a.k.a. Deleted In Autism-1 or DIA1) and cXorf36 (a.k.a.Deleted in Autism-1 Related or DIA1R).<ref>Aziz, A. (2010). “Characterization of the Deleted in Autism 1 Protein Family: Implications for Studying Cognitive Disorders.”</ref>

=== Major depressive disorder ===
Major depressive disorder is a common [[mood disorder]] believed to be caused by irregular neural uptake of [[serotonin]]. While the genetic cause is unknown, genomic studies of post-mortem MDD brains have discovered abnormalities in the [[fibroblast growth factor]] system which supports the theory of growth factors playing an important role in mood disorders.<ref>Niculescu, A.B. (2005). “Genomics Studies of Mood Disorders – the Brain as a Muscle.” Genome Biology, 6: 215.</ref>

=== Others ===
Other neurodegenerative disorders include [[Rett syndrome]], [[Prader–Willi syndrome]], [[Angelman syndrome]], and [[Williams-Beuren syndrome]].

== See also ==
*[[Genomics]]
*[[Neurogenetics]]
*[[Comparative genomics]]
*[[Genetics]]
*[[Evolutionary biology]]
*[[Molecular biology]]
*[[Cognitive psychology]]
*[[Behavioral psychology]]
*[[Neuroanatomy]]

==References==
{{reflist|2}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

== External links ==
*http://www.yerkes.emory.edu/
*http://www.neuroscience.ox.ac.uk/

{{Genomics}}

[[Category:Genomics]]