{{Infobox company
| name             = J & J Ultralights
| logo             = J & J Ultralights Logo 2003.png
| caption          = 
| type             = [[Privately held company]]
| traded_as        = 
| genre            = <!-- Only used with media and publishing companies -->
| fate             = Out of business before 2005
| predecessor      = 
| successor        = 
| foundation       = 1980s
| founder          = 
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| location_city    = [[Live Oak, Florida]]
| location_country = [[United States]]
| location         = 
| locations        = 
| area_served      = 
| key_people       = Owners: Roger and Ronavin Johnston
| industry         = [[Aerospace]]
| products         = [[Ultralight trike]]s
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = <!-- {{URL|www.example.com}} -->
| footnotes        = 
| intl             = 
}}

'''J & J Ultralights''' was an [[United States|American]] aircraft manufacturer that was based at Wings ’N Sunset Airport in [[Live Oak, Florida]]. The company specialized in the design and manufacture of [[ultralight trike]]s, including [[Amphibious aircraft|amphibious]] models. J & J Ultralights first produced aircraft in the 1980s, but was out of business before 2005. The company's designs were later produced by [[Leading Edge Air Foils]] and [[Kemmeries Aviation]].<ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, page C-24. Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref><ref name="KitplanesFeb2000">Downey, Julia: ''2000 Trike and 'Chute Directory'', Kitplanes, Volume 17, Number 2, February 2000, page 46. Kitplanes Acquisition Company. ISSN 0891-1851</ref><ref name="KitplanesFeb2005">Downey, Julia: ''2005 Trikes 'Chutes and Rotorcraft Directory'', Kitplanes, Volume 22, Number 2, February 2005, page 48. Belvoir Publications. ISSN 0891-1851</ref><ref name="WDLA04">Bertrand, Noel; Rene Coulon; et al: ''World Directory of Leisure Aviation 2003-04'', page 102. Pagefast Ltd, Lancaster OK, 2003. ISSN 1368-485X</ref><ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 178. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref><ref name="Home">{{cite web|url = http://www.jjultralight.com/menu/mainmenu.htm|title = Home|accessdate = 3 July 2014|last = J & J Ultralights|date = n.d. |archiveurl = https://web.archive.org/web/20030202125029/http://www.jjultralight.com/menu/mainmenu.htm |archivedate = 2 February 2003}}</ref>

The company was founded by Roger and Ronavin Johnston. Operations encompassed aircraft design and manufacture, sales, support and [[flight training]].<ref name="Home" />

The company began producing the [[J & J Ultralights Tukan|Tukan]] trike in the 1980s and in 1995 purchased the rights to the [[J & J Ultralights Seawing|Seawing]] amphibious trike. By 2003 the company had 200 aircraft flying and another 150 [[homebuilt aircraft|kits]] under construction by customers.<ref name="About">{{cite web|url = http://www.jjultralight.com/menu/about%20J&J.htm|title = Where, When, What and Why|accessdate = 3 July 2014|last = J & J Ultralights|date = n.d. |archiveurl = https://web.archive.org/web/20030202160942/http://www.jjultralight.com/menu/about%20J&J.htm |archivedate = 2 February 2003}}</ref>

== Aircraft ==

{| class="wikitable" 
|-
|+ Aircraft built by J & J Ultralights
|- 
! Model name
! First flight
! Number built
! Type
|-
|align=left| [[J & J Ultralights Tukan]]
|align=center| 1980s
|align=center| 30 (February 2000)
|align=left| ultralight trike
|-
|align=left| [[J & J Ultralights Seawing]]
|align=center| 1995
|align=center| 2 (February 2005)
|align=left| amphibious [[ultralight trike]]
|}

==References==
{{Reflist|30em}}

==External links==
* [http://www.jjultralight.com/ Former location of company website]
* [https://web.archive.org/web/*/http://www.jjultralight.com/ Company website archives] on [[Archive.org]]

{{DEFAULTSORT:J and J Ultralights}}
[[Category:Defunct aircraft manufacturers of the United States]]
[[Category:Ultralight trikes]]
[[Category:Homebuilt aircraft]]