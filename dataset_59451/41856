{{about|the fruit}}
{{about||the species called "wild cherry" in the British Isles|Prunus avium|other uses|Wild Cherry (disambiguation){{!}}Wild Cherry}}
{{redirect|Cherry tree|other uses of "Cherry tree" or "Cherrytree"|Cherry tree (disambiguation)}}
[[File:Cherry Stella444.jpg|thumb|right|''Prunus avium'', sweet cherry, also called wild cherry]]
A '''cherry''' is the [[fruit]] of many plants of the genus ''[[Prunus]]'', and is a fleshy [[drupe]] (stone fruit).

The cherry fruits of commerce usually are obtained from a limited number of species such as [[cultivar]]s of the sweet cherry, ''[[Prunus avium]]''. The name 'cherry' also refers to the cherry tree, and is sometimes applied to [[almond]]s and visually similar flowering trees in the genus ''Prunus'', as in "ornamental cherry", "[[cherry blossom]]", etc. '''Wild cherry''' may refer to any of the cherry species growing outside of cultivation, although ''Prunus avium'' is often referred to specifically by the name "wild cherry" in the British Isles.

== Botany ==
[[File:Black Che.jpg|thumb|''Prunus cerasus'']]
Many cherries are members of the subgenus '''''Cerasus''''', which is distinguished by having the [[flower]]s in small [[corymb]]s of several together (not singly, nor in [[raceme]]s), and by having smooth fruit with only a weak groove along one side, or no groove. The subgenus is [[native plant|native]] to the [[temperateness|temperate]] regions of the [[Northern Hemisphere]], with two species in America, three in Europe, and the remainder in Asia. Other cherry fruits are members of [[Prunus subg. Padus|subgenus ''Padus'']]. 

Most eating cherries are derived from either ''[[Prunus avium]]'', the '''sweet cherry''' (also called the '''wild cherry'''), or from ''[[Prunus cerasus]]'', the '''sour cherry'''.

== History ==

=== Etymology and antiquity ===
The English word cherry derives from French ''cerise'', Spanish ''cereza'', all originating from the Latin ''cerasum'',<ref>{{cite web|url=http://www.etymonline.com/index.php?term=cherry|title=Cherry|publisher=Online Etymology Dictionary, Douglas Harper|date=2017|accessdate=13 February 2017}}</ref> referring to an ancient Greek region near [[Giresun]], Turkey, from which cherries were first thought to be exported to Europe.<ref>{{cite book|url=https://books.google.ca/books?id=V1-Cg_DD0L4C&pg=PA334#v=onepage&q&f=false|title=A History of the Vegetable Kingdom, Page 334|author=Rhind W|publisher=Oxford University|date=1841}}</ref>

The indigenous range of the [[Prunus avium|sweet cherry]] extends through most of Europe, western Asia, and parts of northern Africa, and the fruit has been consumed throughout its range since prehistoric times. A cultivated cherry is recorded as having been brought to Rome by [[Lucullus|Lucius Licinius Lucullus]] from northeastern [[Anatolia]], also known as the [[Pontus]] region, in 72 BC.<ref>{{Cite CE1913|wstitle=Pontus}}</ref>

Cherries were introduced into England at [[Teynham]], near [[Sittingbourne]] in [[Kent]], by order of [[Henry VIII of England|Henry VIII]], who had tasted them in [[Flanders]].<ref>The curious [[antiquary]] [[John Aubrey]] (1626–1697) noted in his memoranda: "Cherries were first brought into Kent tempore H. viii, who being in Flanders, and likeing the Cherries, ordered his Gardener, brought them hence, and propagated them in England." {{ cite book |author=Oliver Lawson Dick, ed. |title=''Aubrey's Brief Lives. Edited from the Original Manuscripts'' |year=1949 |page=xxxv}}</ref><ref>"All the cherry gardens and orchards of Kent are said to have been stocked with the Flemish cherry from a plantation of 105 acres in Teynham, made with foreign cherries, pippins [ [[wikt:pippin|pippin apples]] ], and golden rennets goldreinette apples, done by the [[fruiterer]] of Henry VIII." ([http://www.kent-opc.org/Parishes/Teynham.html Kent On-line: Teynham Parish])</ref><ref>The [http://www.civicheraldry.co.uk/kent_ob.html civic coat of arms of Sittingbourne] with the crest of a "cherry tree fructed proper" and motto "known by their fruits" were only granted on July 28, 1949, however.</ref>

Cherries arrived in North America early in the settlement of Brooklyn, New York (then called "New Netherland") when the region was under Dutch sovereignty. Trades people leased or purchased land to plant orchards and produce gardens, "Certificate of Corielis van Tienlioven that he had found 12 apple, 40 peach, 73 cherry trees, 26 sage plants.., behind the house sold by Anthony Jansen from Salee [Morocco, Africa] to Barent Dirksen [Dutchmen],... ANNO 18th of June 1639." from the "Register of the Provincial Secretary 1638–1682" Pg. 179 <ref>http://www.newnetherlandinstitute.org/files/6514/0151/8811/Volume_I_-_Register_of_the_Provincial_Secretary_1638-1642.pdf</ref>

== Cultivation ==
The cultivated forms are of the species [[Prunus avium|sweet cherry]] (''P. avium'') to which most cherry [[cultivar]]s belong, and the [[sour cherry]] (''P. cerasus''), which is used mainly for cooking. Both species originate in Europe and western Asia; they do not [[Pollination#Mechanics|cross-pollinate]]. Some other species, although having edible fruit, are not grown extensively for consumption, except in northern regions where the two main species will not grow. Irrigation, spraying, labor, and their propensity to damage from rain and hail make cherries relatively expensive. Nonetheless, demand is high for the fruit. In commercial production, cherries are harvested by using a mechanized 'shaker'.<ref>{{cite web|author=Chainpure |url=http://www.chainpure.com/2009/06/wow-its-cherry-harvesting.html |title=Soul to Brain: Wow! Its Cherry Harvesting |publisher=Chainpure.com |date=2009-06-23 |accessdate=2011-11-26}}</ref> Hand picking is also widely used to harvest the fruit to avoid damage to both fruit and trees.

Common rootstocks include Mazzard, Mahaleb, Colt, and Gisela Series, a dwarfing rootstock that produces trees significantly smaller than others, only 8 to 10 feet (2.5 to 3 meters) tall.<ref name="ingels"/> Sour cherries require no [[pollenizer]], while few sweet varieties are self-fertile.<ref name="ingels"/>

=== Growing season ===
{{refimprove|section|date=June 2016}}
[[File:Cherry trees in Tehran..jpg|thumb|280px|right|Ripe cherries of [[Tehran]] in the middle of June.]]

Like most temperate-latitude trees, cherry seeds require exposure to cold to germinate (an adaptation which prevents germination during the autumn, which would then result in the seedling being killed by winter temperatures). The [[cherry pit|pits]] are planted in the autumn (after first being chilled) and seedlings emerge in the spring.<ref name=ucd/> A cherry tree will take three to four years in the field to produce its first crop of fruit, and seven years to attain full maturity.<ref name="ucd">{{cite web | url=http://fruitandnuteducation.ucdavis.edu/fruitnutproduction/Cherry/ | title=Cherry | publisher=Department of Plant Sciences, University of California at Davis | work=Fruit and Nut Information Center | date=2016 | accessdate=28 June 2016}}</ref> Because of the cold-weather requirement, none of the ''Prunus'' genus can grow in tropical climates.

Cherries have a short growing season and can grow in most [[temperateness|temperate]] latitudes.<ref name=ucd/> Cherries blossom in April (in the Northern Hemisphere) and the peak season for the cherry harvest is in the summer. In [[southern Europe]] in June, in [[North America]] in June, in [[England]] in mid-July, and in southern [[British Columbia]] ([[Canada]]) in June to mid-August. In many parts of North America, they are among the first tree fruits to flower and ripen in mid-Spring.

In the Southern Hemisphere, cherries are usually at their peak in late December and are widely associated with [[Christmas]]. 'Kordia' is an early variety which ripens during the beginning of December, 'Lapins peak' ripens near the end of December, and 'Sweethearts' finish slightly later.

=== Pests and diseases ===
[[File:Italienische Süßkirschen.JPG|thumb|150px|Cherries]]
Generally, the cherry can be a difficult fruit tree to grow and keep alive.<ref name="ingels">{{cite book | last = Ingels| first= Chuck, et. al. | title=The Home Orchard:  Growing Your Own Deciduous Fruit and Nut Trees | pages=27–8 | year=2007 | publisher=University of California Agriculture and Natural Resources}}</ref>  In Europe, the first visible pest in the growing season soon after blossom (in April in western Europe) usually is the [[Myzus cerasi|black cherry aphid]] ("cherry blackfly", ''Myzus cerasi''), which causes leaves at the tips of branches to curl, with the blackfly colonies exuding a sticky secretion which promotes fungal growth on the leaves and fruit. At the fruiting stage in June/July (Europe), the [[Rhagoletis cerasi|cherry fruit fly]] (''Rhagoletis cingulata'' and ''Rhagoletis cerasi'') lays its eggs in the immature fruit, whereafter its larvae feed on the cherry flesh and exit through a small hole (about 1 mm diameter), which in turn is the entry point for fungal infection of the cherry fruit after rainfall.<ref>{{cite web|url=http://www.plantwise.org/KnowledgeBank/Datasheet.aspx?dsid=47051|title=cherry fruit fly (Rhagoletis cingulata)|work=plantwise.org}}</ref> In addition, cherry trees are susceptible to bacterial [[canker]], [[cytospora]] canker, [[Monilinia fructicola|brown rot of the fruit]], [[root rot]] from overly wet soil, crown rot, and several viruses.<ref name="ingels"/>

== Cultivars ==
The following cultivars have gained the [[Royal Horticultural Society]]'s [[Award of Garden Merit]]:
{|
|- valign=top
|
{| class="wikitable sortable"
|-
! Name !! Height !! Spread !! Ref.
|-
| Accolade || 8m || 8m || <ref name="autogenerated1">{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1530 |title=RHS Plant Selector Prunus 'Accolade' (d) AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Amanogawa || 8m || 4m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=5318 |title=RHS Plant Selector Prunus 'Amanogawa' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Autumnalis (''P.'' × ''subhirtella'') || 8m || 8m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1562 |title=RHS Plant Selector Prunus × subhirtella 'Autumnalis' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Autumnalis Rosea (''P.'' × ''subhirtella'') || 8m || 4m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=5464 |title=RHS Plant Selector Prunus × subhirtella 'Autumnalis Rosea' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Avium Grandiflora ''see'' Plena ||  ||  ||
|-
| Colorata (''P. padus'') || 12m || 8m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1545 |title=RHS Plant Selector Prunus padus 'Colorata' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Grandiflora ''see'' Plena ||  ||
|-
| [[Prunus 'Kanzan'|Kanzan]] || 12m || 12m+ || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1539 |title=RHS Plant Selector Prunus 'Kanzan' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Kiku-shidare-zakura || 4m || 4m|| <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=5920 |title=RHS Plant Selector Prunus 'Kiku-shidare-zakura' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Kursar || 8m || 8m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1540 |title=RHS Plant Selector Prunus 'Kursar' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Morello (''P. cerasus'') || 4m || 4m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=4499 |title=RHS Plant Selector Prunus cerasus 'Morello' (C) AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Okamé (''P.'' × ''incam'') || 12m || 8m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1544 |title=RHS Plant Selector Prunus × incam 'Okamé' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Pandora || 12m || 8m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1547 |title=RHS Plant Selector Prunus 'Pandora' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Pendula Rosea || 4m || 4m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=3474 |title=RHS Plant Selector Prunus pendula 'Pendula Rosea' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|}
|
{| class="wikitable sortable"
|-
! Name !! Height !! Spread !! Ref.
|-
| Pendula Rubra || 4m || 4m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1548 |title=RHS Plant Selector Prunus pendula 'Pendula Rubra' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Pink Perfection || 8m || 8m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1549 |title=RHS Plant Selector Prunus 'Pink Perfection' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Plena (Grandiflora) || 12m || 8m+ || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1531 |title=RHS Plant Selector Prunus avium 'Plena' (d) AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Praecox (''P. incisa'') || 8m || 8m || <ref name="autogenerated2">{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=4629 |title=RHS Plant Selector Prunus avium 'Stella' (F) AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| ''Prunus avium'' (wild cherry) || 12m+ || 8m+ || <ref name="autogenerated1"/>
|-
| ''Prunus'' × ''cistena'' || 1.5m || 1.5m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1561 |title=RHS Plant Selector Prunus × cistena AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| ''Prunus sargentii'' (Sargent's cherry) || 12m+ || 8m+ || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1550 |title=RHS Plant Selector Prunus sargentii AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| ''Prunus serrula'' (Tibetan cherry) || 12m || 8m+ || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1551 |title=RHS Plant Selector Prunus serrula AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Shirofugen || 8m || 8m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1552 |title=RHS Plant Selector Prunus 'Shirofugen' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Shirotai || 8m || 8m || <ref>{{cite web|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1553 |title=RHS Plant Selector Prunus 'Shirotae' AGM / RHS Gardening |publisher=Apps.rhs.org.uk |accessdate=2012-11-11}}</ref>
|-
| Shōgetsu || 8m || 8m || <ref>{{cite web|title=RHS Plant Selector – ''Prunus'' 'Shogetsu'|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1554|accessdate=29 May 2013}}</ref>
|-
| Spire || 12m || 8m || <ref>{{cite web|title=RHS Plant Selector – ''Prunus'' 'Spire'|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1556|accessdate=29 May 2013}}</ref>
|-
| Stella || 4m || 4m || <ref name="autogenerated2"/>
|-
| Ukon || 8m || 8m+ || <ref>{{cite web|title=RHS Plant Selector – ''Prunus'' 'Ukon'|url=http://apps.rhs.org.uk/plantselector/plant?plantid=1559|accessdate=29 May 2013}}</ref>
|}
|}

See [[cherry blossom]] and ''[[Prunus]]'' for ornamental trees.

{| class="wikitable sortable" style="float:right"
|+ Top (sweet) cherry producing nations in 2014 (tonnes)
!Rank
!Country
! data-sort-type="number" | Production
|-
| style="text-align:right" | 1 || {{TUR}} || style="text-align:right" | 445,556
|-
| style="text-align:right" | 2 || {{USA}} || style="text-align:right" | 329,852
|-
| style="text-align:right" | 3 || {{IRN}} || style="text-align:right" | 172,000
|-
| style="text-align:right" | 4 || {{ESP}} || style="text-align:right" | 118,220
|-
| style="text-align:right" | 5 || {{ITA}} || style="text-align:right" | 110,766
|-
|-bgcolor="#CCCCCC"
! !! style="text-align:left" | '''World'''!! style="text-align:right" | 2,245,826
|-
!colspan=3| <small>''Source: [[FAO|UN Food & Agriculture Organization]]'' <ref name="FAO Cherries">{{cite web|url=http://www.fao.org/faostat/en/#data/QC|publisher= [[FAO|UN Food & Agriculture Organization]], FAOSTAT, Statistics Division|title=Crops/Regions/Production of (Sweet) Cherries by Countries (from pick lists)|year=2014|accessdate=10 February 2017}}</ref>
|}

== Production ==

In 2014, world production of sweet cherries was 2.25 million [[tonne]]s, with [[Turkey]] producing 20% of this total. Other major producers of sweet cherries were the United States and [[Iran]]. World production of sour cherries in 2014 was 1.36 million tonnes, led by [[Russia]], [[Ukraine]] and Turkey.

=== Middle East ===

Major commercial cherry orchards in West Asia are in [[Turkey]] (mainly [[Anatolia]]), [[Iran]], Syria, [[Uzbekistan]], [[Lebanon]] (Bekaa Valley), and [[Israel]] (Golan Heights, Gush Eztion and Northern Galilee).

=== Europe ===
Major commercial cherry orchards in Europe are in [[Turkey]], [[Italy]], [[Spain]] and other Mediterranean regions, and to a smaller extent in the [[Baltic States]] and southern [[Scandinavia]].

{| class="wikitable sortable" style="float:right"
|+ Top sour cherry producing nations in 2014 (tonnes)
!Rank
!Country
! data-sort-type="number" | Production
|-
| style="text-align:right" | 1 || {{RUS}} || style="text-align:right" | 198,000
|-
| style="text-align:right" | 2 || {{UKR}} || style="text-align:right" | 182,880
|-
| style="text-align:right" | 3 || {{TUR}} || style="text-align:right" | 182,577
|-
| style="text-align:right" | 4 || {{POL}} || style="text-align:right" | 176,545
|-
| style="text-align:right" | 5 || {{USA}} || style="text-align:right" | 137,983
|-
|-bgcolor="#CCCCCC"
! !! style="text-align:left" | '''World''' !! style="text-align:right" | 1,362,231
|-
!colspan=3| <small>''Source: [[FAO|UN Food & Agriculture Organization]]'' <ref name="FAO Cherries"/>
|}

In [[France]] since the 1920s, the first cherries of the season come in April/May from the region of [[Céret]] ([[Pyrénées-Orientales]]),<ref>{{fr}} Fabricio Cardenas, [http://vieuxpapierspo.blogspot.fr/2014/08/premieres-cerises-de-ceret-et-dailleurs.html Vieux papiers des Pyrénées-Orientales, ''Premières cerises de Céret et d'ailleurs''], August 24, 2014</ref> where the local producers send, as a tradition since 1932, the first crate of cherries to the [[President of France|president of the Republic]].<ref>{{fr}} Fabricio Cardenas, [http://vieuxpapierspo.blogspot.fr/2014/06/des-cerises-de-ceret-pour-le-president.html Vieux papiers des Pyrénées-Orientales, ''Des cerises de Céret pour le président de la République en 1932''], June 1st 2014</ref>

=== North America ===
[[File:Washington USA Rainier cherries.jpg|thumb|Rainier cherries from the state of Washington, USA]]
In the United States, most sweet cherries are grown in [[Washington (state)|Washington]], [[California]], [[Oregon]], [[Wisconsin]], and [[Michigan]].<ref name="NASS">{{cite report|url=http://usda.mannlib.cornell.edu/usda/current/CherProd/CherProd-06-23-2011.pdf|publisher=National Agricultural Statistics Service, USDA|accessdate=2011-10-06|title=Cherry Production|issn=1948-9072|date=June 23, 2011}}</ref> Important sweet cherry cultivars include [[Bing cherry|Bing]], [[Ulster cherry|Ulster]], [[Rainier cherry|Rainier]], Brooks, Tulare, King, and Sweetheart.<ref>{{cite web|url=http://www.nwcherries.com/nwcherries/varieties|title=Cherry Varieties|publisher=|accessdate=24 October 2014}}</ref> Both Oregon and Michigan provide light-colored 'Royal Ann' ('Napoleon'; alternately 'Queen Anne') cherries for the [[maraschino cherry]] process. Most sour (also called tart) cherries are grown in Michigan, followed by [[Utah]], [[New York (state)|New York]], and Washington.<ref name="NASS"/> Sour cherries include 'Nanking' and [[Evans cherry|'Evans']]. [[Traverse City, Michigan]] claims to be the "Cherry Capital of the World", hosting a [[National Cherry Festival]] and making the world's largest [[cherry pie]]. The specific region of northern Michigan known for tart cherry production is referred to as the "Traverse Bay" region.

Native and non-native sweet cherries grow well in Canada's [[provinces]] of [[Ontario]] and [[British Columbia]] where an annual cherry fiesta has been celebrated for seven consecutive decades in the [[Okanagan Valley]] town of [[Osoyoos]].<ref>{{cite web|url=http://osoyoosfestivalsociety.ca/wp/cherry-fiesta/|title=Cherry Fiesta 2106|publisher=Osoyoos Festival Society|date=2016|accessdate=21 November 2016}}</ref> In addition to the Okanagan, other British Columbia cherry growing regions are the [[Similkameen Valley]] and [[Kootenays|Kootenay Valley]], all three regions together producing 5.5 million kg annually or 60% of total Canadian output.<ref>{{cite web|url=http://www.agf.gov.bc.ca/aboutind/products/plant/cherries.htm|title=Cherries|publisher=BC Ministry of Agriculture|date=2013|accessdate=28 June 2014}}</ref> Sweet cherry varieties in British Columbia include Rainier, Van, Chelan, Lapin, Sweetheart, Skeena, Staccato, Christalina and Bing.

=== Australia ===
In Australia, cherries are grown in all the states except for the Northern Territory.  The major producing regions are located in the temperate areas within New South Wales, Victoria, South Australia and Tasmania.  Western Australia has limited production in the elevated parts in the southwest of the state.  Key production areas include [[Young, New South Wales|Young]], [[Orange, New South Wales|Orange]] and [[Bathurst, New South Wales|Bathurst]] in [[New South Wales]], [[Wandin]], the Goulburn and Murray valley areas in [[Victoria (Australia)|Victoria]], the [[Adelaide Hills]] region in [[South Australia]], and the Huon and Derwent Valleys in [[Tasmania]].

Key commercial varieties in order of seasonality include 'Empress', 'Merchant', 'Supreme', 'Ron's seedling', 'Chelan', 'Ulster', 'Van', 'Bing', 'Stella', 'Nordwunder', 'Lapins', 'Simone', 'Regina', 'Kordia' and 'Sweetheart'.  New varieties are being introduced, including the late season 'Staccato' and early season 'Sequoia'.  The Australian Cherry Breeding program is developing a series of new varieties which are under testing evaluation.<ref>{{cite web|title=ANNUAL INDUSTRY REPORT 08 • 09|publisher=Horticulture Australia Limited (HAL)|url=http://www.horticulture.com.au/admin/assets/library/annual_reports/pdfs/PDF_File_78.pdf}}</ref>

The New South Wales town of [[Young, New South Wales|Young]] is called the "Cherry Capital of Australia" and hosts the National Cherry Festival.

== Nutritional value ==
{{stack begin}}
{{nutritional value | name= Cherries, sour, red, raw
| kJ=209
| protein=1 g
| fat=0.3 g
| carbs=12.2 g
| fiber=1.6 g
| sugars=8.5 g
| calcium_mg=16
| iron_mg=0.32
| magnesium_mg=9
| phosphorus_mg=15
| potassium_mg=173
| sodium_mg=3
| zinc_mg=0.1
| manganese_mg=0.112
| vitC_mg=10
| thiamin_mg=0.03
| riboflavin_mg=0.04
| niacin_mg=0.4
| pantothenic_mg=0.143
| vitB6_mg=0.044
| folate_ug=8
| choline_mg=6.1
| vitA_ug=64
| betacarotene_ug=770
| lutein_ug=85
| vitK_ug=2.1
| source_usda = 1
| note=[http://ndb.nal.usda.gov/ndb/search/list?qlookup=09063&format=Full Link to USDA Database entry]
}}
{{stack end}}
{{stack begin}}
{{nutritional value | name=Cherries, sweet, red, raw
| kJ=263
| protein=1.1 g
| fat=0.2 g
| carbs=16 g
| fiber=2.1 g
| sugars=12.8 g
| calcium_mg=13
| iron_mg=0.36
| magnesium_mg=11
| phosphorus_mg=21
| potassium_mg=222
| sodium_mg=0
| zinc_mg=0.07
| manganese_mg=0.07
| vitC_mg=7
| thiamin_mg=0.027
| riboflavin_mg=0.033
| niacin_mg=0.154
| pantothenic_mg=0.199
| vitB6_mg=0.049
| folate_ug=4
| choline_mg=6.1
| vitA_ug=3
| betacarotene_ug=38
| lutein_ug=85
| vitK_ug=2.1
| source_usda = 1
| note=[http://ndb.nal.usda.gov/ndb/search/list?qlookup=09070&format=Full Link to USDA Database entry]
}}
{{stack end}}
As raw fruit, sweet cherries provide little nutrient content per 100 g serving (nutrient table). [[Dietary fiber]] and [[vitamin C]] are present in moderate content while other [[vitamin]]s and [[dietary mineral]]s each supply less than 10% of the [[Daily Value]] (DV) per serving, respectively (table).<ref>{{cite web |url=http://nutritiondata.self.com/facts/fruits-and-fruit-juices/1867/2 |title=Nutrition facts, cherries, sweet, raw, 100 g |author=<!--Staff writer(s); no by-line.--> |work=US Department of Agriculture National Nutrient Database, Standard Reference 21 |publisher=Nutritiondata.com |accessdate=19 February 2013}}</ref>

Compared to sweet cherries, raw [[Prunus cerasus|sour cherries]] contain slightly higher content per 100 g of vitamin C (12% DV) and vitamin A (8% DV) (table).<ref>{{cite web |url=http://nutritiondata.self.com/facts/fruits-and-fruit-juices/1861/2 |title=Nutrition facts, cherries, sour, red, raw, 100 g |author=<!--Staff writer(s); no by-line.--> |work=US Department of Agriculture National Nutrient Database, Standard Reference 21 |publisher=Nutritiondata.com |accessdate=19 February 2013}}</ref>

== Other uses ==
Cherry wood is valued for its rich color and straight grain in manufacturing fine furniture, particularly desks, tables and chairs.<ref>{{cite web|url=https://www.ontario.ca/page/types-ontario-wood|title=Types of Ontario wood: Black cherry|publisher=Queen's Printer for Ontario, Canada|date=2016|accessdate=25 December 2016}}</ref><ref>{{cite web|url=https://extension.usu.edu/files/publications/factsheet/HI_12.pdf|title=Selecting wood furniture|publisher=Utah State University|date=1987|accessdate=25 December 2016}}</ref>

== Species ==
The list below contains many ''[[Prunus]]'' species that bear the common name cherry, but they are not necessarily members of the subgenus ''Cerasus'', or bear edible fruit. For a complete list of species, see ''[[Prunus]]''. Some common names listed here have historically been used for more than one species, e.g. "rock cherry" is used as an alternative common name for both ''P. prostrata'' and ''P. mahaleb'' and "wild cherry" is used for several species.
* ''[[Prunus apetala]]'' (Siebold & Zucc.) Franch. & Sav. – clove cherry
* ''[[Prunus avium]]'' (L.) L. – sweet cherry, wild cherry, mazzard or gean
* ''[[Prunus campanulata]]'' Maxim. – Taiwan cherry, Formosan cherry or bell-flowered cherry
* ''[[Prunus canescens]]'' Bois. – grey-leaf cherry
* ''[[Prunus caroliniana]]'' Aiton – Carolina laurel cherry or laurel cherry
* ''[[Prunus cerasoides]]'' D. Don. – wild Himalayan cherry
* ''[[Prunus cerasus]]'' L. – sour cherry
* ''[[Prunus cistena]]'' Koehne – purple-leaf sand cherry
* ''[[Prunus cornuta]]'' (Wall. ex Royle) Steud. – Himalayan bird cherry
* ''[[Prunus cuthbertii]]'' Small – Cuthbert cherry
* ''[[Prunus cyclamina]]'' Koehne – cyclamen cherry or Chinese flowering cherry
* ''[[Prunus dawyckensis]]'' Sealy – Dawyck cherry
* ''[[Prunus dielsiana]]'' C.K. Schneid. – tailed-leaf cherry
* ''[[Prunus emarginata]]'' (Douglas ex Hook.) Walp. – Oregon cherry or bitter cherry
* ''[[Prunus eminens]]'' Beck – {{lang-de|mittlere Weichsel}} (semisour cherry)
* ''[[Prunus fruticosa]]'' Pall. – European dwarf cherry, dwarf cherry, Mongolian cherry or steppe cherry
* ''[[Prunus gondouinii]]'' (Poit. & Turpin) Rehder – duke cherry
* ''[[Prunus grayana]]'' Maxim. – Japanese bird cherry or Gray's bird cherry
* ''[[Prunus humilis]]'' Bunge – Chinese plum-cherry or humble bush cherry
* ''[[Prunus ilicifolia]]'' (Nutt. ex Hook. & Arn.) Walp. – hollyleaf cherry, evergreen cherry, holly-leaved cherry or islay
* ''[[Prunus incisa]]'' Thunb. – Fuji cherry
* ''[[Prunus jamasakura]]'' Siebold ex Koidz. – Japanese mountain cherry or Japanese hill cherry
* ''[[Prunus japonica]]'' Thunb. – Korean cherry
* ''[[Prunus laurocerasus]]'' L. – cherry laurel
* ''[[Prunus lyonii]]'' (Eastw.) Sarg. – Catalina Island cherry
* ''[[Prunus maackii]]'' Rupr. – Manchurian cherry or Amur chokecherry
* ''[[Prunus mahaleb]]'' L. – Saint Lucie cherry, rock cherry, perfumed cherry or mahaleb cherry
* ''[[Prunus maximowiczii]]'' Rupr. – Miyama cherry or Korean cherry
* ''[[Prunus mume]]'' (Siebold & Zucc.) –  Chinese plum or Japanese apricot
* ''[[Prunus myrtifolia]]'' (L.) Urb. – West Indian cherry
* ''[[Prunus nepaulensis]]'' (Ser.) Steud. – Nepal bird cherry
* ''[[Prunus nipponica]]'' Matsum. – Takane cherry, peak cherry or Japanese alpine cherry
* ''[[Prunus occidentalis]]'' Sw. – western cherry laurel
* ''[[Prunus padus]]'' L. – bird cherry or European bird cherry
* ''[[Prunus pensylvanica]]'' L.f. – pin cherry, fire cherry, or wild red cherry
* ''[[Prunus pleuradenia]]'' Griseb. – Antilles cherry
* ''[[Prunus prostrata]]'' Labill. – mountain cherry, rock cherry, spreading cherry or prostrate cherry
* ''[[Prunus pseudocerasus]]'' Lindl. – Chinese sour cherry or false cherry
* ''[[Prunus pumila]]'' L. – sand cherry
* ''[[Prunus rufa]]'' Wall ex Hook.f. – Himalayan cherry
* ''[[Prunus salicifolia]]'' Kunth. (=''P. serotina'') – capulin, Singapore cherry or tropic cherry
* ''[[Prunus sargentii]]'' Rehder – Sargent's cherry<!-- or Ezo Mountain cherry: This name is dubious. See [[Talk:Prunus sargentii]] -->
* ''[[Prunus serotina]]'' Ehrh. – black cherry, wild cherry
* ''[[Prunus serrula]]'' Franch. – paperbark cherry, birch bark cherry or Tibetan cherry
* ''[[Prunus serrulata]]'' Lindl. – Japanese cherry, hill cherry, Oriental cherry or East Asian cherry
* ''[[Prunus speciosa]]'' (Koidz.) Ingram – Oshima cherry
* ''[[Prunus ssiori]]'' Schmidt- Hokkaido bird cherry
* ''[[Prunus stipulacea]]'' Maxim.
* ''[[Prunus subhirtella]]'' Miq. – Higan cherry or spring cherry
* ''[[Prunus takesimensis]]'' Nakai – Takeshima flowering cherry
* ''[[Prunus tomentosa]]'' Thunb. – Nanking cherry, Manchu cherry, downy cherry, Shanghai cherry, Ando cherry, mountain cherry, Chinese dwarf cherry, Chinese bush cherry
* ''[[Prunus verecunda]]'' (Koidz.) Koehne – Korean mountain cherry
* ''[[Prunus virginiana]]'' L. – chokecherry
* ''[[Prunus x yedoensis]]'' Matsum. – Yoshino cherry or Tokyo cherry

== See also ==
* [[Cherry pitter]]
* [[Dried cherry]]
* [[List of Award of Garden Merit flowering cherries]]

== References ==
{{reflist|30em}}

== External links ==
{{commons category|Cherries}}
* {{Cite AmCyc|wstitle=Cherry |short=x}}

{{Cherries}}
{{Woodworking}}

{{Authority control}}

[[Category:Cherries| ]]
[[Category:Medicinal plants of Africa]]
[[Category:Medicinal plants of Asia]]
[[Category:Medicinal plants of Europe]]
[[Category:Plants of temperate climates]]
[[Category:Prunus]]
[[Category:Symbols of Utah]]