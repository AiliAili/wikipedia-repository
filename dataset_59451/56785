{{Infobox Journal
| title=Epidemiology
| cover=
| discipline=[[Public health]]
| abbreviation=
| publisher=[[Lippincott, Williams & Wilkins]]
| country=
|frequency=Bimonthly
| history=
| openaccess=
| website= http://journals.lww.com/epidem/pages/default.aspx
| ISSN=1044-3983
}}

'''''Epidemiology''''' is a bi-monthly, peer-reviewed journal for [[epidemiology|epidemiologic]] research, published by [[Lippincott Williams & Wilkins]]. 

The journal publishes original research from all fields of epidemiology, as well as review articles, meta-analyses, novel hypotheses, descriptions and applications of new methods and discussions of research theory and public health policy. It is the official journal of the International Society for Environmental Epidemiology (ISEE).  In 2011, Epidemiology had an impact factor of 5.6, ranking 4th among 157 journals in the field of public, environmental and occupational health.<ref>{{cite web|title=Journal Citation Reports|publisher=Thomson Reuters 2011|accessdate=25 September 2011}}</ref>

Epidemiology was founded by [[Kenneth Rothman (epidemiologist)|Ken Rothman]] in 1990.<ref>[http://www.bu.edu/sph/profile/kenneth-rothman/ Kenneth Rothman]</ref> [[Allen Wilcox]] has been Editor-in-Chief since 2001.<ref>[http://www.niehs.nih.gov/research/atniehs/labs/epi/pi/reproductive/ Reproductive Epidemiology Group]</ref> Its editorial offices are in [[Durham, North Carolina]].

==References==
{{reflist}}

[[Category:Epidemiology journals]]
[[Category:Lippincott Williams & Wilkins academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1990]]