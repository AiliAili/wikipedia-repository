{{Use dmy dates|date=February 2014}}
{{Use British English|date=February 2014}}
{{Orphan|date=July 2012}}

{{Infobox military person
| name = Peter Stanley James
| honorific_suffix = DFC, AE
| image = WCdr PSJ DFC.jpg
| image_size = 220
| caption =
| birth_name = Peter Stanley James
| birth_date = {{Birth date|1917|02|24|df=y}}
| birth_place = [[Wellingborough]], England
| death_date = {{Death date and age|1999|01|11|1917|02|24|df=y}}
| death_place = [[Irchester]], England
| allegiance = [[United Kingdom]]
| branch = [[Royal Air Force Volunteer Reserve]]
| battles=
{{plainlist|
*Second World War
*:[[European theatre of World War II|European theatre]]
}}
}}

[[Wing commander (rank)|Wing Commander]] '''Peter Stanley James''' [[Distinguished Flying Cross (United Kingdom)|DFC]], [[Air Efficiency Award|AE]], [[Royal Air Force Volunteer Reserve|RAF VR]] (24 February 1917 &ndash; 11 January 1999), was a [[Aviator|pilot]] in the [[Royal Air Force Volunteer Reserve]] during the Second World War, flying in [[RAF Bomber Command]] with [[No. 35 Squadron RAF|35 Squadron]], [[No. 78 Squadron RAF|78 Squadron]] and [[No. 148 Squadron RAF|148 Squadron]].

James was captain of [[Handley Page Halifax]] L9500 (TL-H) during a daylight raid against the German battleship {{ship|German battleship|Scharnhorst||2}}, flew in all three [[Bombing of Cologne in World War II|thousand bomber raids]] and was one of the first pilots to take the [[Handley Page Halifax]] into battle.

James was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] in 1941 and died in 1999.

==Early life and career==
James was born in [[Wellingborough]], [[Northamptonshire]] the son of Peter Octavius James and Mabel Whitton James.

He lived with his family in Wollaston, [[Northamptonshire]] and attended [[Wellingborough School]] from 1928 to 1933 before joining Nicholson Sons and Daniel Ltd., a local tannery based in Little Irchester, in late 1933. [[File:Nicholson Sons & Daniels Ltd. - Football Team 1934.jpg|thumb|Nicholson Sons & Daniels Ltd., football team, 1934 (James is 3rd from the left on the back row)]]

On 14 April 1938, James joined the [[Royal Air Force Volunteer Reserve|RAF VR]] as a [[Sergeant]] (pupil) pilot and began ab-initio flying training at [[Sywell Aerodrome]] in [[Northamptonshire]] – flying the [[de Havilland Tiger Moth]], [[Hawker Hart]], [[Hawker Hind]] and [[Hawker Audax]] aircraft. Upon graduation, James was posted to [[List of Royal Air Force & Defence Schools|No. 2 Service Flying Training School]] at [[RAF Brize Norton]] in [[Oxfordshire]], for multi-engine training on the [[Airspeed Oxford]], being awarded his [[Aircrew brevet|RAF Flying Badge]] on 20 June 1940.
In August 1940, James joined [[List of Royal Air Force & Defence Schools|No. 10 Operational Training Unit]] at [[RAF Abingdon]], [[Oxfordshire]] where he began [[Medium bomber]] conversion training, flying the twin engine [[Armstrong Whitworth Whitley]].
[[File:PSJames Graduation Sywell 1938.jpg|thumb|Sywell graduation 1938]]

Upon successful completion of his training, James was promoted to [[Pilot Officer]] and posted to [[No. 78 Squadron RAF|No. 78 Squadron]], an operational unit, flying the [[Armstrong Whitworth Whitley]], based at [[RAF Dishforth]], [[Yorkshire]].

Between 27 October 1940 and 11 February 1941, James took part in 15 operations over occupied territory.
These included trips to [[Hamm]] and [[Duisburg]] in the Ruhr, the ports of Lorient and Wilhelmshaven, and the city of Bremen, when, on the return leg, after 11 hours flying and with the loop aerial and port exactor unserviceable, James ordered his crew to bail out – all landing safely in [[South Molton]], Devon.
James also took part in [[Bombing of Mannheim in World War II|Operation Abigail Rachel]] in which 200 bombers attacked the city of Mannheim in retaliation for the [[Luftwaffe]] attacks on Southampton and Coventry.
An additional raid to Italy on 8/9 November 1940 was aborted when a [[Junkers Ju 88]] of the [[Luftwaffe]] dropped a stick of bombs along the length of the flare path as four [[Armstrong Whitworth Whitley]]s, including James's aircraft, were waiting to take off at [[RAF Honington]], Suffolk.

During his time with [[No. 78 Squadron RAF|No. 78 Squadron]] James flew with Wing Commander (later Air Commodore) [[John Whitworth|John Nicholas Haworth Whitworth, CB, DSO, DFC and Bar]], who was to become the station commander at [[RAF Scampton]] during [[Operation Chastise]], [[No. 617 Squadron RAF|617 Squadron's]] raid on the [[Edersee Dam|Edersee]], [[Möhne Reservoir|Möhne]] and [[Sorpe Dam|Sorpe]] dams.

On 5 March 1941, James was posted to [[No. 35 Squadron RAF|No. 35 Squadron]], based at [[RAF Linton-on-Ouse]], Yorkshire and
began immediate conversion onto the [[Handley Page Halifax]], flying circuits and landings in the prototype aircraft, serial number L7244.
During his time with [[No. 35 Squadron RAF|No. 35 Squadron]], James took part in 16 operations over occupied territory, including the first three [[Handley Page Halifax]] operations of the war, the bombing of the [[Leuna|Leuna oil plant]] south of Merseburg and a daylight raid on the German battleship {{ship|German battleship|Scharnhorst||2}} in port at [[La Rochelle]]. James's posting to [[No. 35 Squadron RAF|No. 35 Squadron]] also included an attachment to No. 2 Beam Approach Training Flight (B.A.T.F) based at [[RAF Driffield]], Yorkshire.
[[File:A Handley-Page Halifax B.II s.I (35Sqn) prepares for start, Linton.jpg|thumb|A Handley-Page Halifax B.II s.I (35 Sqn) prepares for start, Linton-on-Ouse]]

On 11 August 1941, James was posted to No. 28 (Heavy) Conversion Flight at [[RAF Leconfield]], Yorkshire where he became an instructor operating under the command of Squadron Leader (later Group Captain) [[James Brian Tait|James Brian ‘Willie’ Tait DSO and three bars, DFC and bar]].

On 12 February 1942, James was promoted to acting [[Flying Officer]] and took up an instructors post with the newly formed No. 35 Squadron (Heavy) Conversion Flight.
During his time with No. 35 Conversion Flight, James again flew with Wing Commander [[John Whitworth]], in addition to Squadron Leader [[James Brian Tait|James Tait]].
Amongst the pupils under his tutorage was Pilot Officer Donald P. McIntyre of the [[Royal Canadian Air Force]].
On 27 April 1942, whilst flying Halifax W1048 TL-S, McIntyre and his crew took part on a raid against the German battleship {{ship|German battleship|Tirpitz||2}}. During the bombing run, his aircraft was hit by anti-aircraft (AA) fire and despite an extensive fire which engulfed the starboard wing, skilfully managed to land the stricken aircraft onto the frozen [[Hoklingen|lake Hoklingen]], Norway.
W1048 was salvaged on 30 June 1973 by an RAF sub aqua team and members of the Draugen Diving Club and is on permanent display in the Bomber Hall of the [[Royal Air Force Museum London|Royal Air Force Museum, Hendon]].[[File:Halifax RAF Museum.jpg|thumb|caption|Handley Page Halifax W1048 TL-S]]

After a brief spell as Officer Commanding of No. 35 Conversion Flight, James was promoted to Squadron Leader and in April 1942 moved to become Office Commanding No. 78 Conversion Flight based at RAF Croft, Yorkshire. Whilst on attachment to this unit, James flew Halifax 2, serial no. R9434, with Wing Commander (later Air Vice-Marshall) E J Corbally on board to RAF Colerne, Wiltshire, to demonstrate the aircraft to members of the Air Ministry and War Chiefs of Staff.

On 30 May 1942, James flew Halifax L9624 with Pilot Officer Mitchener and a crew composed of No. 78 Conversion Flight students, taking part in the first [[Bombing of Cologne in World War II|thousand-bomber raid to Cologne]].
The same crew accompanied James on the second thousand-bomber raid to Essen on 1 June 1942 and on 25 June 1942 James flew his last operation of the war, the third thousand-bomber raid to Bremen.

Following further instructional postings at No. 10 flying instructors school in Reading and as Flight Commander at No. 6 Elementary Flying Training School (E.F.T.S.) at Sywell, James was posted to the headquarters of number 50 ‘Training’ group, the Air staff where he held the position of ‘Air 2’.
Other instructional positions followed before a posting to the directing staff at No. 2 Officers Advanced Training school in Malta, flying the [[Douglas C-47 Skytrain]], [[Vickers Wellington]] and [[Avro Anson]].
In August 1945, James joined number [[No. 148 Squadron RAF|148 squadron]] at [[Foggia Airfield Complex|Foggia]], Italy, as Officer Commanding ‘B’ flight flying the [[Consolidated B-24 Liberator]] where he took part in Air Trooping sorties, and Bulls eye exercises, analysing the effectiveness of radar vectored fighter intercepts onto large bomber formations.
[[File:No. 2 Officers Advanced Training school.jpg|thumb|caption|Junior Commanders Course, 18 May 1944, College Hall, RAF Cranwell]]

==''Scharnhorst''==
On 24 July 1941, James took part in a daylight raid on the German battleship the {{ship|German battleship|Scharnhorst||2}}, in dock at [[La Rochelle]]. Fifteen aircraft in total were detailed for the attack with nine from [[No. 35 Squadron RAF|No. 35 Squadron]] and 6 from [[No. 76 Squadron RAF|No. 76 Squadron]].

Report taken from [[No. 35 Squadron RAF|No. 35 Squadron]] Operational log.<ref>{{cite web |url=http://www.archieraf.co.uk/archie/l9512tlustory1941.html|title=Archie a Pilot in Bomber Command|author=Druce, Linzee |publisher=Linzee Druce |date= 2001–2008 |work=Daylight Bombing Raid on German Battleship Scharnhorst at La Rochelle 24th July 1941 |accessdate=11 February 2012 }}</ref>

Halifax L9500 – H

F/O James
Sgt Scott
Sgt Sewell
F/Sgt Rogers
Sgt Cox
Sgt Sachs
Sgt McQuigg

{{Quotation|"Took off from Stanton Harcourt at time stated. Joined up with leader and proceeded to [[La Rochelle]]. Excellent weather. Heavy [[Anti-aircraft warfare|flak]] encountered, and about 30 enemy aircraft on reaching the target area. Although this aircraft suffered heavier enemy aircraft attacks than any other in the formation, the Captain, with (a) high display of skill and coolness went on to successfully penetrate the defences and attacked the target with precision. It was not possible in the circumstances to observe the results of the attack, but photographs were obtained. In all, this aircraft had twenty encounters with enemy aircraft, and with (an) exampled display of coolness and skill in the face of such odds, Sgt Sachs the tail gunner, not only successfully defended his aircraft, but succeeded in shooting down one ‘confirmed’ and two ‘probable’ enemy aircraft and damaged several others. This aircraft kept in close formation throughout with Halifax L9501-Y, Captained by F/O Owen, and returned safely to England landing at [[RAF Weston Zoyland|Weston]] Zoyland at time stated."}}
<gallery>
Image:1941 Rotol visit.jpg|James talks to employees at a Rotol Airscrew factory in the Midlands following the raid against the ''Scharnhorst'', 1941
Image:James visits the Rotol Airscrew factory, Midlands - 1941.jpg|James inspects a Rotol airscrew during a visit to the Midlands factory, 1941
Image:Local newspaper cutting - Rotol factory visit,1941.jpg|Local newspaper cutting following Rotol visit, 1941
</gallery>

==Distinguished Flying Cross==
After the operation against the {{ship|German battleship|Scharnhorst||2}}, James was awarded the Distinguished Flying Cross in recognition not just of the raid, but for his professionalism and example set during other operations.
The announcement for the decoration was published in the London Gazette on 24 October 1941:<ref>{{London Gazette |issue=35322 |date=21 October 1941 |startpage=6189}}</ref>

'Acting Flying Officer Peter Stanley James (83276), Royal Air Force Volunteer Reserve, No. 35 Squadron.
(Operational flying hours – 198. No. of sorties – 35).

"This officer has displayed outstanding keenness in operations against the enemy. As captain of aircraft, his work has been of the highest standard, whilst he always remains cheerful and confident however severe the opposition. On the occasion of a daylight attack on the Scharnhorst on the 24th July, 1941, Flying Officer James skilfully and coolly penetrated the defences and attacked the objective with precision in spite of heavy enemy fighter opposition. In all, his aircraft had 20 encounters with fighter aircraft and it was largely his skilful manoeuvring which enabled the rear gunner to beat off the attacks with the loss to (the) enemy of one and probably two other aircraft. Flying Officer James has set an example of the highest order."

==Thousand-bomber raids==
Extracts taken from the book ‘The Thousand Plan: The Story of the First Thousand Bomber Raid on Cologne’ by [[Ralph Barker]]<ref>{{cite book | last=Barker | first=Ralph | year=1966 | title=The Thousand Plan: The Story of the First Thousand Bomber Raid on Cologne | publisher=[[The Reprint Society]] | isbn=9781853102004  | pages= 107 & 205 }}</ref>

{{Quotation|"Almost every pilot at [[RAF Croft|Croft]] had converted on to Halifaxes under a Squadron Leader named Peter James, the man who was working overtime to get everyone fit for the raid. James was 6 feet 21/2 inches tall, with very long legs, and feet that splayed out. His dark hair, close-cropped at the temples, clung easily to his scalp, he had a frank open face and an engaging smile, and he was one of those fortunate men who could maintain the aura of seniority and command and at the same time pass as one of the boys.
Conversion flying was hard work, involving continual circuits and landings, four hours at a time, with four different pilots taking their turn. Then when the morning session was over the aircraft were serviced and fuelled ready for night flying. This was how Peter James spent his operational rest. Few men were more relaxed and even-tempered, yet there came a time when he was biting his lip until it bled."}}

{{Quotation|"At [[RAF Croft|Croft]], high on the Yorkshire moors, Bob Plutte, Paddy Todd's pilot, took off at exactly midnight. Twenty minutes earlier, the tall instructor Peter James had taken off in a Halifax of the conversion flight. It had been an uncomfortable experience. In the middle of his take-off run, when he was trundling along the runway at 90 miles an hour, the blackout curtain above him had started to flutter and the hatch over the cockpit had begun to lift. James shouted at his second pilot to grab it but it was too late. Before anyone could do anything the hatch had blown open and locked itself fully back in the up-right position. The draught, and the noise of the engines and airstream, was terrific. But for the moment James had to concentrate on somehow completing the take-off. He was committed to it and it was too late to throttle back now.
Once airborne he urged the crew to try to close the hatch, but they couldn’t get up into the airstream to do it. They tried to lasso it, and the flight engineer got a rope round it, but it refused to budge. Eventually James decided to leave it; if they pulled at it any more it might snap at the hinges and blow back against the tail. That could do enough damage to end their ambitions of going to Cologne or anywhere else.
To the surprise of his scratch crew, Peter James made no attempt to turn back. They were briefed to fly at 15,000 feet, and the cold was so intense when they climbed up through the icing that James had to hold the control column with his elbows, his hands were so numb. It was colder than it had been in the early Whitleys, when they had no heaters at all and hoar frost had formed inside the plane. But this was one party James didn’t intend to miss.
In common with many other pilots in the last wave, he mistook the burning city 150 miles ahead for the rising moon and altered course accordingly, unable to believe it could be Cologne. And shortly afterwards he met further misfortune; the port outer header tank blew up and he had to feather the engine. He continued to steer for Cologne for a time, but the odds were now too much against him and he was reluctantly forced to turn back."}}

==Operations and remarks==
<ref>{{cite book | last=Middlebrook | first=Martin |author2=Chris Everitt |date=May 1985 | title=The Bomber Command War Diaries: An Operational Reference Book | publisher=[[Viking]] | isbn=0-670-80137-2 | pages= 99–272 }}</ref><ref>{{cite book | last=James | first=Peter S. | year=1939 | title=Pilots Flying Log Book: Peter Stanley James 83276 }}</ref>
{| class="wikitable sortable"
|-
! '''Date''' !! '''Aircraft''' !! '''Serial''' !! '''Target''' !! '''Duration (hrs)''' !! '''Remarks'''
|-
| 29/10/40 || Whitley 5 || T4165 || Ostend || 9.40 || Not Available
|-
| 05/11/40 || Whitley 5 || T4165 || Hamburg || 8.10 || Severe icing conditions
|-
| 13/11/40 || Whitley 5 || T4165 || Turin || 8.20 || Severe icing, turned back. 10/10th cloud from 4,000&nbsp;ft to 16,000&nbsp;ft.
|-
| 17/11/40 || Whitley 5 || T4167 || Hamburg || 10.40 || Heavy flak encountered. Bombs hung up.
|-
| 04/12/40 || Whitley 5 || T4165 || Düsseldorf || 8.00 || Bombed from 4,000&nbsp;ft. Poor weather - 10/10th clouds.
|-
| 07/12/40 || Whitley 5 || T4165 || Manheim || 6.25 || Good weather. Bombed from 14,000&nbsp;ft.
|-
| 16/12/40 || Whitley 5 || P4937 || Duisburg || 8.30 || Considerable light flak encountered.
|-
| 19/12/40 || Whitley 5 || T4167 || Lorient || 7.00 || 10/10th cloud. Bombed from 5,000&nbsp;ft.
|-
| 28/12/40 || Whitley 5 || P4937 || Hamburg || 6.50 || Heavy flak encountered. Bombed from 13,000&nbsp;ft.
|-
| 01/01/41 || Whitley 5 || P4937 || Bremen || 2.35 || Turned back. Engines iced up, high temperatures.
|-
| 03/02/41 || Whitley 5 || P4937 || Bremen || 10.45 || Bombed from 13,000&nbsp;ft. Baled crew out after 11 hours flying. Wireless and port airscrew unserviceable.
|-
| 15/01/41 || Whitley 5 || T4166 || Wilhelmshaven || 6.15 || Bombed from 13,000&nbsp;ft. Good weather, clear visibility. Heavy & light flak. Searchlight activity.
|-
| 06/02/41 || Whitley 5 || T4209 || Dunkirk || 4.40 || Invasion barges. Bombed from 12,000&nbsp;ft. 10/10th cloud. Fairly heavy flak.
|-
| 11/02/41 || Whitley 5 || T4209 || Bremen || 7.30 || Bombed from 10,000&nbsp;ft. 10/10th cloud and fog over all the country. Landed at Leuchars on instruments at 3rd attempt. Very accurate flak fire.
|-
| 10/03/41 || Halifax 1 || L9496 || Le Havre || 4.08 || First Halifax operation of the war.
|-
| 12/03/41 || Halifax 1 || L9496 || Hamburg || 1.10 || Front & rear turrets unserviceable due to hydraulic failure. Turned back.
|-
| 13/03/41 || Halifax 1 || L9496 || Hamburg || 8.00 || Bombed from 15,000&nbsp;ft. Intense heavy & light flak. Large concentrations of searchlights.
|-
| 12/06/41 || Halifax 1 || L9500 || Huls || 6.00 || 3 runs made over target. Attacked by night fighter. Rear gunner fired three bursts & believed to have scored hits.
|-
| 15/06/41 || Halifax 1 || L9500 || Hanover || 6.30 || Took 4,000&nbsp;lb bomb. Bombed from 12,000&nbsp;ft. Rear gunner shot down a [[Junkers Ju 88|Ju 88]] night fighter over the target. The first time a Halifax has taken this size of bomb.
|-
| 17/06/41 || Halifax 1 || L9500 || Hanover || 6.00 || Took 4,000&nbsp;lb bomb. 10/10th cloud all the way, unable to find target, dropped bomb on Vectha night fighter airfield from 11,000&nbsp;ft.
|-
| 20/06/41 || Halifax 1 || L9500 || Kiel || 5.30 || 10/10th cloud at target. Bombed from 13,000&nbsp;ft. Heavy flak encountered after delivery of attack.
|-
| 05/07/41 || Halifax 1 || L9500 || Magdeburg || 6.30 || Bombed Minden from 10,000&nbsp;ft. Light flak and a few searchlights encountered. Several holes from flak on the way back. Landed at Marham to inspect damage, nothing serious so returned to base next day. Flew without airspeed indicator for 5 hours due to it icing up. Broke cloud within 5 minutes of the beacon in Suffolk.
|-
| 07/07/41 || Halifax 1 || L9500 || Frankfurt || 6.30 || Bombed from 11,000&nbsp;ft. Saw 6 Ju 88 night fighters over France.
|-
| 08/07/41 || Halifax 1 || L9500 || Leuna || 6.00 || Bombed Weissenfels from 11,000&nbsp;ft, time too short to reach target. Slight heavy flak and a few searchlights encountered both inaccurate. Hit by flak North East of Ruhr on return journey.
|-
| 14/07/41 || Halifax 1 || L9500 || Hanover || 5.30 || Took 4,000&nbsp;lb bomb. Weather very good. Heavy flak and searchlight activity encountered.
|-
| 19/07/41 || Halifax 1 || L9500 || Hanover || 5.45 || Took 4,000&nbsp;lb bomb. Bombed from 13,000&nbsp;ft. Weather fairly good. Heavy flak and intense searchlight activity encountered. Some holes from flak.
|-
| 21/07/41 || Halifax 1 || L9500 || Manheim || 6.45 || Took 4,000&nbsp;lb bomb. Bombed from 13,000&nbsp;ft. Weather very good but slight ground haze. Slight heavy flak and searchlight activity encountered.
|-
| 24/07/41 || Halifax 1 || L9500 || La Rochelle || 7.00 || Daylight raid on the ''Scharnhorst''. No fighter cover, stayed in formation throughout with F/O Owen. Gunners shot down 4 ME 109's. We had 20 encounters with fighters and estimated around 30 of them in all. Very heavy Flak encountered. Bombed from 15,000&nbsp;ft on a clear afternoon. The grey haze from flak bursts extended from 14,000&nbsp;ft to 16,000&nbsp;ft. Landed at Weston Zoyland with F/O Owen to inspect for damage, tail wheel tyre flat from flak or fighter fire. Seven hours flying in formation all the way.
|-
| 02/08/41 || Halifax 1 || L9500 || Berlin || 7.00 || Bombed at 18,000&nbsp;ft but got to 20,000&nbsp;ft. Both flak and searchlight activity were intense. No cloud over target but ground haze.
|-
| 05/08/41 || Halifax 1 || L9500 || Karlsruhe || 6.45 || Bombed from 14,000&nbsp;ft. Met Ju 88 over target but no attack. Very good visibility. Slight flak encountered in the target area.
|-
| 30/05/42 || Halifax 2 || L9624 || Cologne || 4.00 || Flew at 13,000&nbsp;ft with the cockpit hatch open. Had to feather the port outer engine due to a glycol leak in the radiator and returned on three engines.
|-
| 01/06/42 || Halifax 2 || L9624 || Essen || 4.35 || Attacked by J 88 from port and starboard quarters, tracer seen from 600 / 700 yards but no hits.
|-
| 25/06/42 || Halifax 2 || L9624 || Bremen || 6.15 || Not Available
|}

==Memoria==
Aspects of James operational career are on display in two UK museums, including a photograph and fragments of Whitley P4937 at the South Molton and District museum, Devon and uniform and other artefacts at the Sywell Aviation Museum in Northamptonshire. [[File:AviationMuseum.JPG|thumb|caption|Sywell Aviation Museum]]

==Legacy==
On 21 May 1946 James was appointed Officer Commanding [[Mick Mannock|No. 378 (Mannock) Squadron]], [[Air Training Corps]], a post he held until 15 March 1949 when he was appointed Officer Commanding Northamptonshire and Huntingdonshire Wing Air Training Corps. James was awarded the [[Cadet Forces Medal]] in November 1958.
During his time as [[wing commander (rank)|wing commander]], James was involved in the acquisition of land for several of the counties Air Training Corps Squadrons including 1101 Squadron, Kettering.
James held this post for the following 17 years until the amalgamation of the Northamptonshire wing with the Leicestershire Wing in 1966 when he stepped down, becoming chairman of No. 378 (Mannock) Squadron civilian committee.
[[File:Wing Parade - RAF Wittering.jpg|thumb|James inspecting cadets at the Northamptonshire Wing Parade, RAF Wittering.]]

==References==
{{reflist}}
* {{cite book | last=Barker | first=Ralph | year=1966 | title=''The Thousand Plan: The Story of the First Thousand Bomber Raid on Cologne'' | publisher=[[The Reprint Society]] | isbn=9781853102004  | pages= 107 & 205 }}
* {{cite book | last=Middlebrook | first=Martin |author2=Chris Everitt |date=May 1985 | title=''The Bomber Command War Diaries: An Operational Reference Book'' | publisher=[[Viking]] | isbn=0-670-80137-2 | pages= 99–272 }}
* {{cite web |url=http://www.archieraf.co.uk/archie/l9512tlustory1941.html/ |title=''Archie a Pilot in Bomber Command'' |author=Druce, Linzee |publisher=Linzee Druce |date= 2001–2008 |work=Daylight Bombing Raid on German Battleship Scharnhorst at La Rochelle 24th July 1941 |accessdate=11 February 2012 }}
* {{London Gazette |issue=35322 |date=21 October 1941 |startpage=6189}}

==External links==
{{Commonscat|Peter Stanley James}}

{{DEFAULTSORT:James, Peter Stanley}}
[[Category:1917 births]]
[[Category:1999 deaths]]
[[Category:British aviators]]
[[Category:Royal Air Force officers]]
[[Category:British World War II pilots]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]