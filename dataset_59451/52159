{{Infobox publisher
| name         = Latin American Literary Review/Press
| image        = 
| caption      =
| parent       = 
| status       = 
| traded_as    =
| predecessor  =
| founded      = 1972
| founder      = Yvette Miller
| successor    = Debra Castillo
| country      = United States
| headquarters = [[Ithaca]], [[New York (state)|New York]]
| distribution = 
| keypeople    = 
| publications = Magazines, books
| topics       = [[Latin American literature]]
| genre        = 
| imprints     = 
| revenue      = 
| owner        =
| numemployees =
| url          = http://larlp.net
}}

The '''Latin American Literary Review/Press''', affiliated with the Department of Comparative Literature in [[Cornell University]],  [[Ithaca]], [[New York (state)|New York]], is a [[non-profit organization]].  The founding [[editor-in-chief]] was Yvette E. Miller.;<ref name=unesco/><ref name=council/><ref name=asu/> she has been succeeded by Debra A. Castillo.<ref name="Debra A. Castillo ">{{cite web|url=http://complit.cornell.edu/debra-ann-castillo |title= Debra Ann Castillo |date= |accessdate=March 11, 2017}}</ref>

Miller developed two entities: the ''Latin American Literary Review'', a [[literary magazine]], and the Latin American Literary Review Press, which published English translations of [[Latin American literature]].<ref name=unesco>{{cite web|title=Latin American Literary Review Press (United States of America)|url=http://portal.unesco.org/culture/en/ev.php-URL_ID=3989&URL_DO=DO_TOPIC&URL_SECTION=201.html|publisher= United Nations Educational, Scientific and Cultural Organization|quote=The goal of the Latin American Literary Review Press has been to make available to the general public translations of Latin American literature.}}</ref><ref name=council>{{cite web|author=Leslie Schwartz|title=Latin American Literature Hits The Big Time|url=http://www.clmp.org/about/newswire_archive_01_05.html|publisher=The Council of Literary Magazines and Press|date=May 15, 2001}}</ref><ref name=asu>{{cite web|author=ASU|title=Latin American Literary Review Press (pp.26-37)|url=http://www.asu.edu/brp/BRP%20catalog%202011%20all%5B1%5D.pdf|publisher=Arizona State University|quote=Latin American Literary Review Press was founded in 1980 to familiarize readers outside of the field with Latin American literature.}}</ref>

The ''Latin American Literary Review'' was established in 1972 and is published biannually.  As of 2017 it has moved to an online platform with Ubiquity Press.<ref name="LALRonline">{{cite web|url=http://larlp.net |title=Latin American Literary Review |date= |accessdate=March 11, 2017}}</ref> It contains feature essays, creative work, new translations of important texts, and reviews of recent literary works from [[Latin America]]. It publishes articles in English, [[Spanish language|Spanish]], and [[Portuguese language|Portuguese]].<ref name="LALRblog">{{cite web|url=https://lalronline.wordpress.com |title=Latin American Literary Review |date= |accessdate=March 11, 2017}}</ref><ref name="LALRfacebook">{{cite web|url=https://www.facebook.com/Latin-American-Literary-Review-346081595780931/  |title= Latin American Literary Review |date= |accessdate=March 11, 2017}}</ref>

The Latin American Literary Review Press was founded in 1980 and has published creative writing and [[literary criticism]] that has been translated into English. The press was created with the principal objective of familiarizing readers outside the field with Latin American literature.<ref name=unesco/><ref name=council/><ref name=asu/><ref>{{cite web|url=http://www.lalrp.org/a_press.html |title=About the press |publisher=Latin American Literary Review Press |date=May 1, 2012|accessdate=March 3, 2014}}</ref> Since Miller's death, the Press has been inactive.

==Notable publications==
*''[[Leyendas de Guatemala|Legends of Guatemala]]'' - by [[Miguel Ángel Asturias]] (ISBN 978-1-891270-53-6)
*''Bubbeh'' - by [[Sabina Berman]] (ISBN 978-0-935480-93-1)<ref>{{cite web|author=Michael Lowenthal|title=''Bubbeh''|url=https://www.nytimes.com/books/98/06/14/bib/980614.rv101134.html|publisher=The New York Times|date=June 14, 1998}}</ref>
*''My Heart Flooded with Water'' - selected poems by [[Alfonsina Storni]] (ISBN 978-1-891270-51-2)
*''Bazaar of the Idiots'' - by [[Gustavo Álvarez Gardeazábal]] (ISBN 978-0-935480-48-1)
*''[[Yo-Yo Boing!]]'' - by [[Giannina Braschi]] (ISBN 978-0-935480-97-9)
*''[[Memorias del subdesarrollo|Memories of Underdevelopment]]'' - by [[Edmundo Desnoes]] (ISBN 978-1-891270-18-5)
*''[[Nazarín]]'' - by [[Benito Pérez Galdós]] (ISBN 978-0-935480-75-7)
*''Patient'' - by [[Ana María Shua]] (ISBN 978-0-935480-90-0)
*''[[La Brecha|Breakthrough]]'' - by [[Mercedes Valdivieso]] (ISBN 978-0-935480-33-7)

==See also==
{{Portal|Hispanic and Latino Americans}}
*[[Bilingual Review Press]]
*[[The Bilingual Review]]
*[[Hispanic and Latino Americans]]

== References ==
{{reflist}}

==External links==
* {{Official website|http://www.lalrp.org}}

{{DEFAULTSORT:Latin American Literary Review Press}}
[[Category:Book publishing companies based in Pennsylvania]]
[[Category:Media in Pittsburgh]]
[[Category:Publishing companies established in 1972]]
[[Category:Hispanic and Latino-American literature]]
[[Category:Linguistics journals]]
[[Category:American literary magazines]]
[[Category:Latin American literature]]
[[Category:Literary publishing companies]]