{{Multiple issues|
{{Orphan|date=November 2016}}
{{notability|Products|date=September 2016}}
}}

{{Infobox software
| name = BeeSafe
| logo = [[File:BeeSafe_logo.png|250px]]
| developer = BeeSafe
| latest release version = 
| latest release date = 
| programming language = 
| operating system = [[iOS]] <br/> [[Android (operating system)|Android]]
| genre = [[Personal safety app]]
| license = [[Proprietary software|Proprietary]]
| website = {{URL|http://www.beesafe.me/}}
}}

'''BeeSafe''' is a [[Personal safety app|personal safety mobile app]] launched in 2015 as a [[Slovakia|Slovak]] [[Startup company|startup]]. It is a [[Location-based service|location-based]] [[Next-generation network|next generation]] security service that notifies family members and friends in case the user of the app gets in danger.<ref name="Crunchbase">{{cite news|url=https://www.crunchbase.com/organization/beesafe-s-r-o#/entity|title=BeeSafe, A next generation safety platform, Calls for help automatically|last=|first=|date=|publisher=|newspaper=|location=|pages=|language=English|coauthors=|accessdate=31 August 2016}}</ref> The app has received numerous awards.<ref name="ViaBona" /><ref name="F6S">{{cite news|url=https://www.f6s.com/beesafe|title=What is Beesafe, Features of BeeSafe|last=|first=|date=|publisher=|newspaper=FS6|location=|pages=|language=English|coauthors=|accessdate=31 August 2016}}</ref> The app has more than 700 downloads and 250 active logins from more than 60 countries worldwide.<ref name="Startitup">{{cite news|url=http://www.startitup.sk/slovenska-appka-beesafe-sa-postara-otvoju-bezpecnost/|title=Slovak app BeeSafe will take care of your safety|last=Matušková|first=Ráchel|date=28 July 2016|publisher=|newspaper=STARTITUP|location=|pages=|language=Slovak|coauthors=|accessdate=31 August 2016}}</ref>

== History ==
BeeSafe was founded on March 20, 2015 by [[Peter Stražovec]] and [[Michal Kačerík]].<ref name="Founders">{{cite news|url=http://www.up.co/communities/slovakia/kosice/startup-weekend/5533|title=Founders of BeeSafe|last=|first=|date=|publisher=|newspaper=|location=Košice|language=English|coauthors=|website=|accessdate=31 August 2016}}</ref><ref name="Crunchbase" /> The project was a winner of [[Žilina]]’s Startup Weekend 2013<ref name="Startitup" />and a StartupAwards.SK 2015 finalist. Later on, the app was released in the [[Android (operating system)|Android]] and [[iOS]] marketplace.<ref name="Piestany">{{cite news|url=http://www.startupers.sk/piestany-idu-znizovat-kriminalitu-v-spolupraci-so-slovenskym-startupom-beesafe/|title=The city of Piešťany plans to decrease the criminality levels in cooperation with a Slovak startup BeeSafe|last=Mikula|first=Andrej|date=24 November 2015|publisher=|newspaper=Startupers|location=|language=Slovak|coauthors=|website=|accessdate=31 August 2016}}</ref>  The whole BeeSafe project was in The Spot booster and incubator in [[Bratislava]] for three months.
<ref name="Video">{{cite news|url=https://www.youtube.com/watch?v=RBnxk7ksSew|title=Interview about the BeeSafe startup|last=Jankovičová|first=Veronika|date=12 Jan 2016|publisher=|newspaper=CEEscape|location=Bratislava|pages=|language=English|coauthors=|accessdate=31 August 2016}}</ref>

BeeSafe entered into an agreement with the city of [[Piešťany]] in November 2015 to increase the security of its citizen by connecting the mobile app with the police platform. It is the first city that started using the BeeSafe platform. Further on, the application tries to help people in other [[Slovakia|Slovak]] cities. The cities can see the users only if they are in danger.<ref name="Webnoviny">{{cite news|url=http://www.webnoviny.sk/slovensko/clanok/1014387-ludi-v-piestanoch-ma-chranit-mobilna-aplikacia-beesafe/|title=People in Piešťany are going to stay protected by mobile app BeeSafe|last=SITA|first=|date=21 November 2015|publisher=|newspaper=Webnoviny|location=Slovakia|language=Slovak|coauthors=|website=|accessdate=31 August 2016}}</ref>

== Features ==
BeeSafe is a next generation corporate and personal emergency app. 
It is not a tracking application, neither [[panic button]] application. The [[user (computing)|user]] doesn’t have to press any panic button, the process of help call is fully automated.
BeeSafe app includes a silent alarm system. The system can trigger an alarm and notify user’s family, friends, security service and the police, once the user is in a dangerous situation. It is available for both Android or Apple smartphones or other smart devices.<ref name="Crunchbase" /> b

== Awards ==
BeeSafe app received the Via Bona award, it is a winner of a Slovak startup and has other nominations too.<ref name="ViaBona">{{cite news|url=http://www.nadaciapontis.sk/article/75-nominees-competing-for-the-via-bona-slovakia-award/1763|title=Via Bona Awards nominations|last=|first=|date=23 January 2016|publisher=|newspaper=|location=Bratislava|language=English|coauthors=|website=|accessdate=31 August 2016}}</ref>

== References ==
{{reflist|30em}}

== External links ==

* [http://www.beesafe.me/ BeeSafe Homepage]

[[Category:Mobile applications]]
[[Category:Android (operating system) software]]
[[Category:IOS (Apple)]]