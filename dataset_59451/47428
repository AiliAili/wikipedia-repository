{{Infobox artist
| name             = Destiny Deacon
| image            = 
| image_size       = 
| alt              = 
| caption          = 
| birth_name       = <!-- only use if different than name -->
| birth_date       = {{Birth year and age|1957}}
| birth_place      = [[Maryborough, Queensland]], [[Australia]]
| death_date       = 
| death_place      =
| nationality      = Australian
| education        = 
| alma_mater       = 
| known_for        = [[Photography]], [[Film|Video]], [[art installation|Installation]], [[Performance art|Performance]]
| notable_works    = 
| style            = 
| movement         = 
| spouse           = 
| awards           = 
| elected          =
| patrons          = 
}}

{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
'''Destiny Deacon''' is an Australian photographer born in 1957 in [[Maryborough, Queensland]] of the [[K'ua K'ua]] and [[Darnley Island (Queensland)|Erub/Mer]] peoples.<ref>{{cite web|title=Destiny Deacon|url=http://nga.gov.au/retake/artists/00000003.htm|publisher=National Gallery of Australia|accessdate=27 January 2015}}</ref> She has exhibited photographs and films across Australia and also internationally, focusing on politics and exposing the disparagement around [[Indigenous Australian culture]]s.<ref name="MCA"/>

==Early life==

Destiny Deacon relocated to [[Port Melbourne]],<ref>{{cite web|title=Arts Calls with Tracey Moffatt|url=http://www.abc.net.au/arts/stories/s4122902.htm|publisher=Australian Broadcast Corporation|accessdate=15 April 2015}}</ref> Victoria in 1959 with her mother [[Eleanor Harding]] then married to Destiny's father wharf labourer and unionist Warren Deacon.  Soon after, Deacon's parents separated and she and her siblings were raised by her mother with the help of a close indigenous community.<ref name=daao>{{cite web|title=Destiny Deacon Biography|url=http://www.daao.org.au/bio/destiny-deacon/biography/?|publisher=Design and Art Australia Online|accessdate=11 April 2015}}</ref> Deacon's interest in photography started at a very early age.<ref name="Summer Series 6">{{cite web|title=Summer Series 6|url=http://www.abc.net.au/tv/messagestick/stories/s1542817.htm|publisher=Australian Broadcasting Corporation|accessdate=11 April 2015}}</ref> However instead of pursuing photography Deacon decided to attend university and study politics, a field that her mother had been very interested and active within, with her involved with the [[United Council of Aboriginal Women]]. After attending the [[University of Melbourne]] and completing a Bachelor of Arts program in politics and obtaining a Diploma in teaching from [[La Trobe University]], Deacon moved on to first become a history teacher across various community and secondary schools around Victoria, and then to a tutor and lecturer in Australian Writing and Culture, and Aboriginal and Torres Strait Islander Cultural Production at [[Melbourne University]].<ref name=daao/> It wasn't until 1990<ref name=daao/> after a stint on [[community radio]] for [[3CR (Melbourne)|3CR]] <ref>{{cite book|title=Half Light: Portraits of Black Australia|last2=Jonestitle|first2=Jonathan|date=2008|publisher=Art Gallery of New South Wales|page=64|last1=Perkins|first1=Hettie}}</ref> that she decided to move into professional photography after holding an exhibition with a few friends.<ref name="Summer Series 6"/>

==Artistic development==
Before her foray into professional photography Deacon became involved with indigenous activist [[Charles Perkins (Aboriginal activist)|Charles Perkins]] working from Canberra as a staff trainer, her strong interest in politics led her to become one of his 'Angels', this passion was the beginning of her artistic endeavors.<ref name=MCA>{{cite web|title=Destiny Deacon MCA|url=https://www.mca.com.au/media/uploads/file/2012/01/11/Destiny_Deacon_Resource_Kit_Dec04.pdf|publisher=Museum of Contemporary Art Australia|accessdate=31 March 2015}}</ref>

Growing up Deacon and her family lived in various Melbourne inner suburbs including [[Housing Commission of Victoria|commission housing]], which while often tough opened her eyes to a whole other world.<ref name=MCA/>

Using what she had learnt about politics through Perkins, the 'Angels' and her upbringing Deacon started taking photographs of her culture using her trademark 'black dollies' and other [[kitsch]] items as props to expose racism and Australia's' sordid past.<ref name="abc.net.au">{{cite web|title=Summer Series 6|url=http://www.abc.net.au/tv/messagestick/stories/s1542817.htm|publisher=Australian Broadcast Corporation Australia|accessdate=31 March 2015}}</ref>

==Aesthetics==
Deacon said in an interview published in the Sydney Biennale 2000 "Photography is white people's invention. Lots of things seem really technical, for example the camera, the darkroom.. I've started taking the kind of pictures I do because I can't paint..and then I discovered it was a good way of expressing some feelings that lurk inside"<ref>{{cite book|title=Sydney Biennale 2000|date=2000|publisher=Sydney Biennale LTD|first=Nick|last=Waterlow|page=46}}</ref>

Deacon works across a spectrum of different mediums including [[photography]], [[Filmmaking|video]], [[art installation|installation]] and [[Performance art|performance]], but her most prominent is her use of dolls to convey her message about the racism that exists within Australia.<ref name="MCA" /> Deacon's photography polarizes popular Anglo culture against Indigenous existence creating satirical images, using Aboriginal imagery, found items, family members, and friends in very strange scenarios.<ref>{{cite web|title=Native Peoples and Photography|url=http://www.oxfordreference.com/view/10.1093/acref/9780198662716.001.0001/acref-9780198662716-e-1080?rskey=LUztiz&result=6|publisher=Oxford Reference|accessdate=31 March 2015}}</ref> ''Where's Mickey?'' (2003) shows the large difference between how Indigenous people are perceived by the white Australian population and the reality of her family and friends lives.<ref name=MCA/> Deacon has said about her work that the "Humour cuts deep. I like to think that there's a laugh and a tear in each."<ref name=MCA/>

In ''Oz'' (1998) series<ref>{{Cite web|url=http://www.artgallery.nsw.gov.au/collection/works/?group_accession=418.2003.1-15|title=Works from the collective title Oz|last=Deacon|first=Destiny|date=1998|website=AGNSW collection record|publisher=Art Gallery of New South Wales|access-date=6 April 2016}}</ref> Deacon incorporates [[Koori]] kitsch dolls and shows the construction of identity is an old game that she can play too.<ref name=":1">{{Cite web|url=http://www.artgallery.nsw.gov.au/collection/works/418.2003.2/|title=Under the spell of the poppies, from the series Oz|last=Deacon|first=Destiny|date=1998|website=AGNSW collection record|publisher=Art Gallery of New South Wales|others=Art Gallery of New South Wales Photography Collection Handbook (2007)|access-date=6 April 2016}}</ref> Using ''[[The Wonderful Wizard of Oz|The wizard of Oz]]'' as a starting point for her re-presentation of Aboriginal culture and identity, she recognises the fictionalising of history, identity and nationhood in Australia’s past – a reminder that things are not always as they appear, nor what we have been made to believe; that history is written much similar to a story.<ref name=":1" />

==Work and exhibitions==
Deacon's first show, ‘Pitcha Mi Koori’, was a part of the [[Melbourne Fringe Festival]], and in 1991, her work was included in ‘Aboriginal Women’s Exhibition’, at the [[Art Gallery of New South Wales]] in Sydney with her first solo exhibition, ''Caste Offs'', held in 1993 at the [[Australian Centre for Photography]] in Sydney.
Deacon's work began to be included in group exhibitions in 1994, including ''Blakness: Blak City Culture!'' at the [[Australian Centre for Contemporary Art]] in Melbourne, ''True Colours: Aboriginal and Torres Strait Islander Artists Raise the Flag'' at Bluecoat Gallery, Liverpool; [[South London Gallery]], London; City Gallery, Leicester and Australia. 
Welcome to My Koori World (video) was shown at the [[Museum of Modern Art]] in New York in a show ''An Eccentric Orbit: Video Art in Australia'' which was also picked up by ABC TV for the ''Blackout'' series.
In 1998 Deacon explored her mother's life by photographing her family in the [[Torres Strait Islands]] after her death two years previous, documenting it in a show titled ''Postcards from Mummy'' this journey "allowed her to come to come to terms with the loss of her mother and the importance of history, memory and place to identify."<ref>{{cite book|title=Look: Contemporary Australian Photography since 1980|last=Marsh|first=Anne|page=32|date=2010|publisher=Macmillan Art Publishing}}</ref>
''Walk & don’t look blak'' was Deacon's first large retrospective held at the [[Museum of Contemporary Art, Sydney]] in 2004 encompassing the past 14 years of her work and practice. From there it toured the [[Ian Potter Museum of Art]] at [[Melbourne University]], the Adam Art Gallery and the [[Wellington City Gallery]] in Wellington, New Zealand, the Tjibao Cultural Centre in [[Noumea]], New Caledonia and the [[Tokyo Metropolitan Museum of Photography]] in Japan. 
For ''2004: Australian Culture Now'' at The [[Australian Centre for the Moving Image]] in Melbourne, Deacon was commissioned to make a film for the programme ''[[Neighbours]]'' ''(the remix)''.<ref name=daao/>

Deacon's work has also been featured in numerous local and international exhibitions such as [[Temporary exhibitions at the Art Gallery of New South Wales|Perspecta]] (1993, 1999), [[Havana Biennial]] (1994), Johannesburg Biennale (1995), Brisbane’s [[Queensland Art Gallery|Asia-Pacific Triennial of Contemporary Art]] (1996), [[Melbourne International Biennial 1999|Melbourne International Biennial]] (1999), [[Biennale of Sydney]] (2000), Yokohama Triennale (2001), Das Lied von der Erde (2001) and [[Documenta]] 11 (2002).<ref>{{Cite web|url=http://www.artgallery.nsw.gov.au/collection/works/135.2002/|title=Me and Virginia's doll|last=Deacon|first=Destiny|date=1995|website=AGNSW collection record|publisher=Art Gallery of New South Wales|access-date=5 April 2016}}</ref>

==References==
{{reflist}}

== External links ==
[http://www.artgallery.nsw.gov.au/collection/artists/deacon-destiny/ Destiny Deacon] at the [[Art Gallery of New South Wales]]{{DEFAULTSORT:Deacon, Destiny}}

{{Authority control}}
{{DEFAULTSORT:Deacon, Destiny}}
[[Category:1957 births]]
[[Category:Living people]]
[[Category:20th-century Australian artists]]
[[Category:20th-century women artists]]
[[Category:21st-century Australian artists]]
[[Category:21st-century women artists]]
[[Category:Australian Aboriginal artists]]
[[Category:Australian contemporary artists]]
[[Category:Australian photographers]]
[[Category:Australian women photographers]]
[[Category:La Trobe University alumni]]
[[Category:People from Maryborough, Queensland]]
[[Category:Photographers from Melbourne]]
[[Category:University of Melbourne alumni]]