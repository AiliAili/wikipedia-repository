{{Use dmy dates|date=December 2011}}
{{Use British English|date=December 2011}}
{{Infobox single
| Name           = Rumour Has It
| Artist         = [[Adele]]
| Cover          = Rumour Has It artwork.jpg
| Album          = [[21 (Adele album)|21]]
| Released       = 5 November 2011
| Recorded       = 2009–10
| Genre          = [[jazz]], [[Pop music|Pop]]
| Length         = 3:43
| Label          = [[XL Recordings|XL]]
| Writer         = Adele Adkins, [[Ryan Tedder]]
| Producer       = Ryan Tedder
| Last single    = "[[Set Fire to the Rain]]"<br />(2011)
| Next single    = "[[Turning Tables]]"<br />(2011)
| This single    = "'''Rumour Has It'''"<br />(2011)
}}

"'''Rumour Has It'''" is a song by British singer [[Adele]]. The song was written by Adele and [[Ryan Tedder]] while the production was handled by Tedder. Adele has said the song was not inspired by the media but it was aimed at her friends who believed things they heard about her. It was the fourth single from ''[[21 (Adele album)|21]]'' in the United States.

The song received critical acclaim from critics, who praised Adele's voice and the song's "[[catchiness]]". Even without having been released as a single, the song charted on the [[Billboard Hot 100|''Billboard'' Hot 100]] at number 16 and topped the [[Adult album alternative|Triple A]] chart. In the [[Netherlands]], the record was released as the fourth single from ''21'' after "[[Rolling in the Deep]]", "[[Set Fire to the Rain]]" and "[[Someone like You (Adele song)|Someone like You]]". The track received a Double Platinum certification by the [[Recording Industry Association of America|RIAA]] for sales exceeding 2 million units in the US. It has been covered by [[Jeremih]] and by [[Characters of Glee|''Glee'' cast]] during the episode "[[Mash Off]]" with another Adele song "[[Someone like You (Adele song)|Someone like You]]", and also by [[Katharine McPhee]] during the episode "[[The Cost of Art]]" from the TV series ''[[Smash (TV series)|Smash]]''. Adele added the song to the [[set list]] on her second worldwide tour [[Adele Live]].

==Background==
"Rumour Has It" was written by Adele and [[Ryan Tedder]] and produced by Tedder.<ref name="Cre" /> Talking about the collaboration with Tedder on "Rumour Has It", Adele revealed: "You can really tell when you hear a Ryan Tedder song, which I liked, but I wanted to come out with something that would surprise everyone when it was us two put together, so we came out with this sort of bluesy-pop stomping song really."<ref name="Tedder song" /> Adele explained that the song was not inspired by the media, but was aimed at her own friends, who frequently spread rumours about her break-up with her boyfriend: "People might think it's about blogs and magazines and papers, but it's not. It's about my own friends believing stuff that they hear about me, which is pretty mortifying really."<ref name="Tedder song" /><ref name="adinterview" /> Adele said that "Rumour Has It" and "[[Rolling in the Deep]]" were lyrically opposite of "[[Someone like You (Adele song)|Someone Like You]]". She said that she wrote "[[Someone like You (Adele song)|Someone Like You]]" after she was tired of "being such a bitch" with "[[Rolling in the Deep]]" or "Rumour Has It."<ref name="mtv1658345" />

On 28 October 2011, during an interview with ''Billboard'', Columbia Records revealed that "Rumour Has It" would be released as the third US single from the album and serviced to pop and adult contemporary radio.<ref name="singlerelease" /> However, the release of the song was scrapped and "[[Set Fire to the Rain]]" was released instead on 21 November 2011.<ref name="setrelease" /><ref name="setrelease2" /> Columbia explained: "Our research found more programmer preference for '[[Set Fire to the Rain]]'. [...] Both 'Rumour' and 'Fire' came back strong, but 'Fire' was a bit stronger. [...] It's a better plan to go with 'Fire' over 'Rumour' at pop and adult radio."<ref name="setrelease" /> After the [[2012 Grammy Awards]] "Rumour Has It" was then released as the fourth single.<ref name="Billboard" /><ref>{{cite web|last=Hampp|first=Andrew|title =Adele's Manager Talks Grammys, Tour, 'Rumour Has It' & 'Glee'|work=Billboard|publisher=Prometheus Global Media|date=14 February 2012|url=http://www.billboard.com/articles/news/505765/adeles-manager-talks-grammys-tour-rumour-has-it-glee|accessdate=15 February 2012}}</ref>

==Composition==
{{Listen
  | filename = Adele - Rumour Has It.ogg
  | title = "Rumour Has It"
  | description = A 22-second sample of "Rumour Has It", described by Adele as a "bluesy [[pop music|pop]] stomping song".<ref name="Tedder song" /><ref name="telegraph826968" />
  | pos = left
}}
"Rumour Has It" was described by [[The A.V. Club]] as "a soul-strutter" while [[AllMusic]] as being a "blues-inflected" song <ref name="allmusic" /> while Adele described as a "bluesy pop stomping song".<ref name="Tedder song" /><ref name="telegraph826968" />  Noel Murray of ''[[The A.V. Club]]'' found a "persuasive backbeat", comparing the song to Adele's earlier material during her first studio album ''[[19 (Adele album)|19]]'' (2008) due to their similar instrumentation consisting of piano and guitars.<ref name="AV Club" /> Ian Wade of [[BBC Online]] wrote that the song "channels the avenging [[Rock and roll|rock'n'roll]] soul of [[Wanda Jackson]]."<ref name="bbc review" /> Tom Townshend of [[MSN Music]] found a "similar primal stomp" to "[[Rolling in the Deep]]" (2010) which made a "pacey, hypnotic, [[funk]], interrupted by sublime symphonic blues."<ref name="MSN" /> According to the sheet music published by [[Universal Music Publishing Group]] at the website Musicnotes.com, "Rumour Has It" is written in the key of [[D Minor]].<ref name="sheet" /> It is set in a [[time signature]] of common time with an up-tempo beat of 126 beats per minute.<ref name="sheet" /> Adele's vocal ranges from the note of [[D (musical note)|D<sub>3</sub>]] to the note of [[D Minor|B4]].<ref name="sheet" />

==Reception==

===Critical reception===
The song received critical acclaim. While reviewing ''21'', [[Allmusic]]'s Matt Collar concluded that "Rumour Has It" and "He Won't Go" are "terrifically catchy, booty-shaking numbers, and exactly the kind of songs you want and expect from Adele."<ref name="allmusic" /> Noel Murray of ''The A.V. Club'' wrote that the song was "overpowering",<ref name="AV Club" /> while Leah Greenblatt of ''[[Entertainment Weekly]]'' praised Adele's voice comparing it with [[The Shangri-Las]].<ref name="ew" /> Chris Parkin of ''[[NME]]'' called it "a swamp song so perfectly shadowy [[David Lynch]] might be fond of it."<ref name="NME" /> Jon Caramanica of ''[[The New York Times]]'' found "hollow counterpoint vocals, and a daringly morbid bridge that jerks away from the song's rhythm, before once again acceding to it."<ref name="N-Y Times" /> Matthew Cole of [[Slant Magazine|''Slant'' Magazine]] concluded that when Adele sings an adventurous arrangement like the "spooky [[Tin Pan Alley]] blues" on "Rumour Has It", she "sounds every bit the phenomenon the press has made her out to be."<ref name="slant" /> Joanne Dorken of [[MTV UK]] said that the song is a "bluesy/soul anthem [which] shows a more sassy side to Adele. With its banging drums, infectious beats and cheeky lyrics, you can't help but tap your feet to this up-tempo number from Miss. Adkin."<ref name="mtvuk" /> [[PopMatters]]{{'}} Jer Fairall called "Rumour Has It" a "booming [[John Barry (composer)|John Barry]] homage."<ref name="Matters" /> The writers of ''Rolling Stone'' placed the song at number twenty-nine on their list of "50 Best Singles of 2011".<ref name="RSS 2011">{{cite web|url=http://www.rollingstone.com/music/lists/50-best-singles-of-2011-20111207/adele-rumour-has-it-20111206|title=50 Best Singles of 2011|author=Staff|date= 8 December 2011|work=Rolling Stone|publisher=Wenner Media LLC|accessdate=9 December 2011}}</ref>

A writer of ''[[URB (magazine)|URB]]'' compared Adele's voice with a "'[19]40s, piano-vixen lounge singer."<ref name="urb" /> John Murphy of [[musicOMH]] wrote that "Rumour Has It has more sass, a brilliant blues/soul anthem with more fantastic drumming and a cute lyrical twist at the end."<ref name="OMH" /> While reviewing ''21'', Gary McGinley of No Ripcord called the song a "potential single" from the album,<ref name="ripcord" /> while Ian Wade of BBC Online called it a "literally banging" song.<ref name="bbc review" /> Jim Farber of ''[[Daily News (New York)|Daily News]]'' wrote that Adele "mined a bayou stomp" the catchy "Rumour Has It."<ref name="NYDN" /> Allison Stewart of ''[[The Washington Post]]'' wrote that "'Rumour Has It' is set in a fictional universe where [[Dusty Springfield]] fronts the [[The Ronettes|Ronettes]]."<ref name="Washington Post" /> [[Sputnikmusic]]'s Joseph Viney chose the song as the best on the album, praising its "pounding drums, sweet vocal harmonies and a tale of love both won and lost with some alacrity."<ref name="Sputnik" />

===Chart performance===
"Rumour Has It" has charted on various formats, including the [[Rock Songs]] chart peaking at number 28 and the Triple A chart, where it reached the top position for one week in August 2011.<ref name="singlerelease" /> It has also charted on the [[Billboard Hot 100|''Billboard'' Hot 100]] at number 96 for the week ending 3 August 2011.<ref name="bill100530" /> As of March 2012, "Rumour Has It" sold over 1,000,000 copies in the United States according to [[Nielsen SoundScan]],<ref name="setrelease" /> and should be noted that the song sold 500,000 copies before its single release. The track already has a history on the singles chart. "[[Rumour]]" reached No. 16 on the Hot 100 in December, fueled largely by the "[[Glee (TV series)|Glee]]" cast's exposure of the song. The same week, the TV troupe bowed at No. 11 with its mash-up of the song and Adele's "[[Someone Like You (Adele song)|Someone Like You]]".

==Live performances and cover versions==
Adele added the song to the [[set list]] on her second worldwide tour Adele Live.<ref name="digital" /> She also performed the song during the [[iTunes Festival#2011|2011 iTunes Festival]] in London.<ref name="digia328882" /> American singer [[Jeremih]] covered the song during ''Billboard''{{'}}s Mashup Mondays.<ref name="bill1005241852" /> During an interview he said: "I know you probably haven't heard this type of sound from my voice, so I just wanted to touch it. And see what I could do with it. [...] The bridge is my favorite part. It showcased my vocals entirely, being so bare, just the keys and guitar. At first I was going to switch from keys to strings, but that was just too much."<ref name="bill1005241852" /> Jessica Letkemann of ''Billboard'' praised Jeremih's [[tenor]] and his "dusky-yet-feminine voice" saying that he was "centered on stripping the song's instrumentation down."<ref name="bill1005241852" /> During the cover, Jeremih made references to [[Vanilla Ice]], [[David Bowie]] and [[Freddie Mercury]].<ref name="bill1005241852" />

[[Amber Riley]], [[Naya Rivera]] and [[Heather Morris]] sang a mash-up of "[[Someone Like You (Adele song)|Someone Like You]]" and "Rumour Has It" during ''[[Glee (TV series)|Glee]]''{{'}}s episode "[[Mash Off]]" which aired on 15 November 2011.<ref name="rollglee"/><ref name="eglee"/> However, the cover was posted online on 10 November.<ref>{{cite web|url=http://www.digitalspy.co.uk/ustv/s57/glee/news/a350279/glees-amber-riley-naya-rivera-perform-adele-mashup-watch-video.html|title='Glee's Amber Riley, Naya Rivera perform Adele mashup – Watch video|publisher=Digital Spy. Hachette Filipacchi Médias|first=Tara|last=Fowler|date=10 November 2011|accessdate=14 November 2011}}</ref> Jenna Mullins of E! Online praised the cover saying that it will "knock your socks right off"<ref>{{cite web|url=http://uk.eonline.com/news/watch_with_kristin/watch_now_glees_epic_300th_musical/274383|title=Watch Now: Glee's Epic 300th Musical Performance! Plus—Can You Spot the Brittana Moment?|publisher=E! Online|first=Jenna|last=Mullins|date=10 November 2011|accessdate=14 November 2011}}</ref> and a writer for ''OK!'' described it as "AMAZE-ing".<ref>{{cite web|url=http://www.ok.co.uk/celebrity-news/view/42015/Glee-s-Santana-serenades-Brittany-with-an-Adele-Rumour-Has-It-and-Someone-Like-You-mash-up/|title=Glee's Santana serenades Brittany with an Adele Rumour Has It and Someone Like You mash-up|work=OK!|publisher=Northern & Shell|date=11 November 2011|accessdate=14 November 2011}}</ref> Erica Futterman of ''Rolling Stone'' noted that the cover was "one of the greatest things the show has done [so far]."<ref>{{cite web|url=http://www.rollingstone.com/movies/news/glee-recap-adele-makes-mash-off-a-resounding-success-20111116|title='Glee' Recap: Adele Makes 'Mash Off' a Resounding Success|first=Erica|last=Futterman|work=Rolling Stone|publisher=Wenner Media|date=16 November 2011|accessdate=23 November 2011}}</ref> Similarly, ''Billboard''{{'}}s Raye Votta commented that the cover was "arguably the best performance 'Glee' has done since "[[Don't Stop Believin'#Glee cover|Don't Stop Believin']]"."<ref>{{cite web|url=http://www.billboard.com/articles/news/465103/glee-tugs-at-heartstrings-with-adele-mashup-falls-flat-elsewhere|title='Glee' Tugs At Heartstrings With Adele Mashup, Falls Flat Elsewhere|work=Billboard|publisher=Prometheus Global Media|location=New York|first=Rae|last=Votta|page=2|date=16 November 2011|accessdate=23 November 2011}}</ref> Their version of the song peaked at number 11 on the ''Billboard'' Hot 100 while selling 160,000 digital downloads in its first week and became the fifth highest digital sales week by a ''Glee'' Cast single.<ref>{{cite news|url=http://www.billboard.com/articles/columns/chart-beat/464972/weekly-chart-notes-glee-cast-lady-gaga-brantley-gilbert|title=Weekly Chart Notes: 'Glee' Cast, Lady Gaga, Brantley Gilbert|last=Trust|first=Gary|date=25 November 2011|accessdate=26 November 2011|work=Billboard|publisher=Prometheus Global Media}}</ref><ref>{{Cite news|url=http://www.billboard.com/articles/news/464987/rihannas-love-still-leads-hot-100-katy-perry-back-in-top-10|title=Rihanna's 'Love' Still Leads Hot 100, Katy Perry Back in Top 10|date= 23 November 2011|first=Keith|last=Caulfield|work=Billboard|publisher=Prometheus Global Media|accessdate=26 November 2011}}</ref> As of March 2015, it remains the ninth best-selling ''Glee'' Cast recording in the show's history, having sold 413,000 copies in the United States.<ref>{{cite web |url=http://www.billboard.com/articles/columns/pop-shop/6509278/glee-cast-10-best-selling-downloads |title='Glee' Cast's 10 Best-Selling Downloads |work=[[Billboard (magazine)|Billboard]] |publisher=[[Prometheus Global Media]] |date=20 March 2015 |accessdate=20 March 2015}}</ref> In Canada, the song debuted on the [[Canadian Hot 100]] at number 12, selling 14,000 downloads.<ref name="Can">{{cite web|url=http://en-ca.nielsen.com/content/dam/nielsen/en_ca/documents/pdf/newsletters/billboard_canadian-update/Nielsen%20Music%20Canadian%20Update.pdf|title=Nielsen Music: Canadian Update (November 25, 2011)|date=25 November 2011|work=Billboard|publisher=Prometheus Global Media|accessdate=24 November 2011}}</ref> The mash-up also peaked at number 35 on the [[UK Singles Chart]],<ref>{{cite web|url=http://www.theofficialcharts.com/archive-chart/_/1/2011-11-26/|title=2011 Top 40 Official UK Singles Archive|publisher=The Official Charts Company|date=26 November 2011|accessdate=23 February 2012}}</ref> number 28 in Australia<ref>{{cite web|url=http://www.australian-charts.com/showitem.asp?interpret=Glee+Cast&titel=Rumor+Has+It+%2F+Someone+Like+You&cat=s|title=Australian-charts – Glee Cast – Rumour Has It/Someone Like You|publisher=Australian Recording Industry Association. Hung Medien|accessdate=23 February 2012}}</ref> and number 19 on the Irish Singles Chart.<ref>{{cite web|url=http://www.chart-track.co.uk/index.jsp?c=p%2Fmusicvideo%2Fmusic%2Farchive%2Findex_test.jsp&ct=240001&arch=t&lyr=2011&year=2011&week=47|title=Top 50 Singles, Week Ending 24 November 2011|publisher=Irish Recorded Music Association|accessdate=23 February 2012}}</ref>

The song was also covered by [[Katharine McPhee]] on the [[TV series]] "[[Smash (TV series)|Smash]]", during the episode [[The Cost of Art]].<ref>{{cite web|url=http://ryanseacrest.com/2012/02/13/exclusive-watch-katharine-mcphee-sing-adeles-rumor-has-it-video/|title=EXCLUSIVE: Katharine McPhee Rocks Adele's ‘Rumor Has It’ On ‘Smash’ [VIDEO<nowiki>]</nowiki>|first=Michael|last=Murray|work=RyanSeacrest.com|date=13 February 2012|accessdate=19 April 2012}}</ref> On the episode, after practicing her dance moves, McPhee's character Karen and her fellow "Marilyn" actors take the stage to rock out to the song.<ref>{{cite web|url=http://www.huffingtonpost.com/maggie-furlong/smash-review-episode-4_b_1295138.html|title='Smash' Review: A New Plea To Watch The Show's Best Episode Yet|first=Maddie|last=Furlong|work=The Huffington Post|date=23 February 2012|accessdate=19 April 2012}}</ref>

American [[emo]] band [[The Promise Ring]] performed a version of the song in September 2012 for ''[[The A.V. Club]]''{{'s}} A.V. Undercover series.<ref name="undercover">{{cite web|title=The Promise Ring covers Adele|url=http://www.avclub.com/articles/the-promise-ring-covers-adele,70725/|accessdate=3 April 2013}}</ref>

The ''[[X Factor Indonesia]]'' winner [[Fatin Shidqia|Fatin Shidqia Lubis]] performed this song on Mentor's choice episode on 22 February 2013.<ref>{{cite web|url=https://www.youtube.com/watch?v=jQi7Kk5bJx0 |title=Fatin Shidqia – Rumor Has It (Adele) – GALA SHOW 1 – X Factor Indonesia (22 Feb 2013)|publisher=youtube.com|date=22 February 2013|accessdate=8 September 2013}}</ref>

The alternative metal band [[Mushroomhead]] included a rendition of the song on their album ''[[The Righteous & the Butterfly]]''.

[[The Voice Australia]] contestant Lexi Clark performed this song for the first live final of [[The Voice (Australia season 5)|season 5]] on the 13 June 2016

[[The King's Offspring]] sampled the song on their [[God, Girls, & Glazed Donuts]] album.<ref>{{cite web|url=http://www.jesusfreakhideout.com/indiemusic/GodGirlsandGlazedDonuts.asp|title=The King's Offspring, "God, Girls and Glazed Donuts" Review|accessdate=2016-12-23}}</ref>

==Credits and personnel==
*Adele – songwriter, backing vocals, lead vocals
*Ryan Tedder – songwriter, producer, bass, electric guitar, piano, B-3 organ, drums, music programming, musical & string arrangements, recording engineer 
*Jerrod "Skins" Bettis – drums, acoustic guitar
*Tom Elmhirst – audio mixing 
*Dan Perry – audio mixing assistant, additional vocals

==Charts and certifications==
{{col-start}}
{{col-2}}

===Weekly charts===
{|class="wikitable sortable"
|-
!Chart (2011–12)
!Peak<br>position
|-
|Australia ([[ARIA Charts|ARIA]])<ref>{{cite web|url=http://pandora.nla.gov.au/pan/23790/20120116-1555/Issue1135.pdf|title=ARIA Report: Issue 1135 (Week Commencing 28 November 2011)|publisher=[[Australian Recording Industry Association]]|accessdate=28 November 2011|format=PDF|page=4}}</ref>
| style="text-align:center;"|69
|-
{{singlechart|Austria|26|artist=Adele|song=Rumour Has It}}
|-
{{singlechart|Flanders|46|artist=Adele|song=Rumour Has It|accessdate=30 December 2011}}
|-
{{singlechart|Wallonia Tip|7|artist=Adele|song=Rumour Has It|accessdate=24 April 2012}}
|-
{{singlechart|Canada|16|artist=Adele|artistid=810846|accessdate=8 March 2012}}
|-
{{singlechart|France|172|artist=Adele|song=Rumour Has It|accessdate=27 February 2012}}
|-
{{singlechart|Germany|68|artist=Adele|song=Rumour Has It|accessdate=20 April 2012}}
|-
{{singlechart|Hungary|36|year=2012|week=19|artist=Adele|song=Rumour Has It|accessdate=16 May 2012}}
|-
|Ireland ([[Irish Singles Chart|IRMA]])<ref>{{cite web|url=http://irish-charts.com/showinterpret.asp?interpret=Adele|title=Discography Adele|publisher=Irish Charts Portal. Hung Medien (Steffen Hung)|last=Hung|first=Steffen}}</ref>
|align=center|71
|-
{{singlechart|Israelairplay|7|artist=Adele|song=Rumor Has It|year=2011|week=50}}
|-
|Italy ([[FIMI]])<ref>{{cite web|url=http://www.fimi.it/classifiche#/category:digital/id:1023|title=Classifica settimanale WK 15 (dal 2012-04-09 al 2012-04-15)|publisher=[[FIMI]]|accessdate=3 December 2011}}</ref>
| style="text-align:center;"|23
|-
| Mexico ([[Billboard (magazine)|''Billboard'' Mexican Airplay]])<ref>{{cite web|archiveurl=http://www.webcitation.org/67u9hAKT0?url=http://www.billboard.biz/bbbiz/charts/internationalcharts/mexico|url=http://www.billboard.biz/bbbiz/charts/internationalcharts/mexico|title=Mexico Airplay|date=2012-06-02|archivedate=2012-05-24|publisher=Billboard}}</ref>
| style="text-align:center;"|31
|-
{{singlechart|Dutch40|21|artist=Adele|song=Rumour Has It|accessdate=13 October 2011}}
|-
{{singlechart|Dutch100|52|artist=Adele|song=Rumour Has It|accessdate=13 October 2011}}
|-
| Poland ([[Polish Music Charts|Polish Airplay New]])<ref>{{cite web|url=http://bestsellery.zpav.pl/airplays/pozostale/archiwum.php?year=2012&typ=nowosci&idlisty=663#title|title=Listy bestsellerów, wyróżnienia :: Związek Producentów Audio-Video|publisher=[[Polish Music Charts|Polish Airplay New]]|accessdate=30 March 2017}}</ref>
| align="center" | 1
|-
|{{singlechart|Scotland|71|date=2011-12-10|accessdate=20 May 2012}}
|-
|South Korea International Chart ([[Gaon Music Chart|Gaon]])<ref name="korea">[http://www.gaonchart.co.kr/main/section/search/list.gaon?Search_str=Rumor+Has+It:: 가온차트와 함께하세요 ::]. Gaonchart.co.kr. Retrieved on 2012-03-30.</ref>
| style="text-align:center;"|1
|-
|{{singlechart|UK|85|date=2011-12-10|accessdate=20 May 2012}}
|-
|{{singlechart|Billboardhot100|16|artist=Adele|song=Rumour Has It|artistid={{BillboardID|Adele}}|accessdate=20 May 2012}}
|-
|{{singlechart|Billboardadultcontemporary|4|artist=Adele|song=Rumour Has It|artistid={{BillboardID|Adele}}|accessdate=20 May 2012}}
|-
|{{singlechart|Billboardadultpopsongs|2|artist=Adele|song=Rumour Has It|artistid={{BillboardID|Adele}}|accessdate=20 May 2012}}
|-
|{{singlechart|Billboardrocksongs|27|artist=Adele|song=Rumour Has It|artistid={{BillboardID|Adele}}|accessdate=24 October 2014}}
|-
|{{singlechart|Billboardpopsongs|8|artist=Adele|song=Rumour Has It|artistid={{BillboardID|Adele}}|accessdate=20 May 2012}}
|-
|}
{{col-2}}

===Certifications===
{{Certification Table Top}}
{{certification Table Entry|type=single|region=Australia|artist=Adele|title=Rumour Has It|award=Gold|certyear=2012|relyear=2011|digital=true}}
{{certification Table Entry|type=single|region=Canada|artist=Adele|title=Rumour Has It|award=Platinum|certyear=2012|relyear=2011|certmonth=5|digital=true|number=2|accessdate=1 November 2013}}
{{certification Table Entry|type=single|region=Italy|artist=Adele|salesamount=25,000|title=Rumour Has It|award=Gold|certyear=2012|relyear=2011|certref=<ref>{{cite web | url = http://www.fimi.it/temp/cert_GFK_download_242012.pdf | publisher = [[Federation of the Italian Music Industry]] | language = Italian | format = PDF | accessdate = 26 January 2012 | title = Certificazione Singoli Digitali dalla settimana 1 del 2009 alla settimana 24 del 2012}}</ref>}}
{{Certification Table Entry|region=United Kingdom|title=Rumour Has It|artist=Adele|award=Silver|type=single|certyear=2017|relyear=2011|accessdate=31 March 2017}}
{{Certification Table Entry|region=United States|title=Rumour Has It|artist=Adele|award=Platinum|number=2|type=single|digital=true|certyear=2012|relyear=2011|accessdate = 6 December 2012}}
{{Certification Table Bottom|streaming=true}}

===Year-end charts===

{| class="wikitable"
|-
!scope="col"|Chart (2012)
!scope="col"|Position
|-
|US [[Billboard Hot 100|''Billboard'' Hot 100]]<ref>{{cite web|url=http://www.billboard.com/charts/year-end/2012/hot-100-songs?begin=61&order=position|title=Best of 2012 – Hot 100 Songs|publisher=Billboard.com|accessdate=2012-12-14|date=}}</ref>
| style="text-align:center;"|64
|}
{{col-end}}

==Release history==
{| class="wikitable"
|-
! scope="row"| Region
! scope="row"| Date
! scope="row"| Format
|-
|United Kingdom<ref name="BBCRadio2">{{cite web|url=http://www.bbc.co.uk/radio2/music-events/playlist/|title=BBC – Radio 2 – Playlist – Week Commencing: 5 November 2011|publisher=[[BBC Radio 2]]|date=5 November 2011|archiveurl=http://www.webcitation.org/62ttlTgv1?url=http://www.bbc.co.uk/radio2/music-events/playlist/|archivedate=2 November 2011}}</ref>
|5 November 2011
|[[BBC Radio 2|Mainstream radio]]
|}

==References==
{{reflist|3|refs=
<ref name="Billboard">{{cite web|url=http://www.billboard.com/articles/news/506161/adeles-next-21-single-rumour-has-it|title=Adele's Next '21' Single: 'Rumor Has It'|work=[[Billboard (magazine)|Billboard]]|publisher=Prometheus Global Media|first=Marc|last=Schneider|date=13 February 2012|accessdate=13 February 2012}}</ref>
<ref name="Cre">{{cite AV media notes |title=21 |others=Adele |date=2011 |type=liner notes |publisher=[[XL Records]], [[Columbia Records]]}}</ref>
<ref name="adinterview">{{cite web|url=http://www.adele.tv/trackbytrack/archive/|first=Adele|last=Adkins|year=2011|title=Adele ''21'' Track by Track Interview|publisher=Adele.tv. [[XL Recordings]]|accessdate=29 October 2011}}</ref>
<ref name="Tedder song">{{cite web|url=http://www.digitalspy.ca/music/news/a298869/adele-aimed-to-surprise-with-tedder-song.html |title= Adele aimed to "surprise" with Tedder song|last1=Levine |first1= Nick|date=19 January 2011|publisher=Digital Spy. Hachette Filipacchi Ltd|accessdate=24 April 2011}}</ref>
<ref name="bill100530">{{cite web|url=http://www.billboard.com/articles/news/468222/britney-spears-bounds-into-hot-100s-top-10-lmfao-still-no-1|title=Britney Spears Bounds Into Hot 100's Top 10, LMFAO Still No. 1 |work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|date=3 August 2011|accessdate=3 August 2011}}</ref>
<ref name="singlerelease">{{cite web|url=http://www.billboard.com/articles/columns/chart-beat/465444/weekly-chart-notes-adele-justin-bieber-jason-aldean|title=Weekly Chart Notes: Adele, Justin Bieber, Jason Aldean|first=Gary|last=Trust|work=Billboard|publisher=Prometheus Global Media|date=28 October 2011|accessdate=29 October 2011}}</ref>
<ref name="bill1005241852">{{cite web|url=http://www.billboard.com/articles/columns/mashup-mondays/470384/jeremih-strips-adeles-rumour-down|title=Jeremih Strips Adele's 'Rumour' Down|work=Billboard|publisher=Prometheus Global Media|first=Jessica|last=Letkemann|location=New York|date=20 June 2011|accessdate=29 October 2011}}</ref>
<ref name="allmusic">{{cite web|url=http://www.allmusic.com/album/r2078324/review|title=''21'' – Adele Review|publisher=[[Allmusic]]. [[Rovi Corporation]]|first=Matt|last=Collar|accessdate=29 October 2011}}</ref>
<ref name="rollglee">{{cite news|url=http://www.rollingstone.com/movies/news/glee-mixes-toby-keith-and-adele-for-300th-musical-performance-20111027|title='Glee' to Feature Toby Keith, Adele in New Episodes|first=Mikael|last=Wood|work=[[Rolling Stone]]|publisher=[[Jann Wenner|Wenner Media]]|date=27 October 2011|accessdate=29 October 2011}}</ref>
<ref name="eglee">{{cite news|url=http://uk.eonline.com/news/watch_with_kristin/watch_now_first_look_darren_criss/271981|title=Watch Now: First Look at Darren Criss Singing Katy Perry on Glee!|publisher=[[E! Online]]. [[NBC Universal]]|first=Jenna|last=Mullins|date=27 October 2011|accessdate=29 October 2011}}</ref>
<!--
<ref name="canrock">{{cite web|url=http://canadianrockalt.blogspot.com/2011/06/active-rock-june-21-2011.html|title=Canadian Active Rock & Alt Rock Chart Archive: Active Rock – June 21, 2011|publisher=[[America's Music Charts]]|date=21 June 2011|accessdate=29 October 2011}}</ref>-->
<ref name="AV Club">{{cite news|url=http://www.avclub.com/articles/adele-21,52181/|title=Adele: ''21'' Review|last=Murray|first=Noel|work=[[The A.V. Club]]|publisher=[[The Onion]]|location=Chicago|date=22 February 2011 |accessdate=29 October 2011}}</ref>
<ref name="telegraph826968">{{cite news|url=http://www.telegraph.co.uk/culture/culturevideo/musicvideo/8269688/Adele-Rumour-Has-It-is-about-my-own-friends.html|title=Adele: 'Rumour Has It' is about my own friends|work=[[The Daily Telegraph]]|publisher=Telegraph Media Group|date=19 January 2011|accessdate=29 October 2011}}</ref>
<ref name="ew">{{cite web|url=http://www.ew.com/ew/article/0,,20464873,00.html|title=Review: Adele, ''21''|publisher=[[Time Inc.|Time]] division of [[Time Warner]]|work=[[Entertainment Weekly]]|author=Greenblatt, Leah|date=9 February 2011|accessdate=29 October 2011}}</ref>
<ref name="NME">{{cite web|url=http://www.nme.com/reviews/adele/11814 |title=Adele&nbsp;– Album Review: Adele&nbsp;– ''21'' (XL) -Album Reviews|work=[[NME]]|publisher=[[IPC Media]]. [[Time Inc.]]|author=Parkin, Chris|date=24 January 2011|accessdate=29 October 2011}}</ref>
<ref name="N-Y Times">{{Cite news|url=https://www.nytimes.com/2011/03/01/arts/music/01choice.html|title=Adele:''21''|work=[[The New York Times]]|publisher=[[New York Times Company]]|first=Jon|last=Caramanica|date=11 February 2011|accessdate=29 October 2011}}</ref>
<ref name="slant">{{cite web|url=http://www.slantmagazine.com/music/review/adele-21/2403|title=Adele: ''21''&nbsp;– Music Review |publisher=[[Slant Magazine|''Slant'' Magazine]]|date=20 February 2011|author=Cole, Matthew|accessdate=29 October 2011}}</ref>
<ref name="urb">{{Cite news|url=http://www.urb.com/2011/02/25/adele-21/|title=Adele: ''21''| work=[[URB (magazine)|URB]]|date=25 February 2011|accessdate=29 October 2011}}</ref>
<ref name="OMH">{{cite web|url=http://www.musicomh.com/albums/adele-2_0111.htm |title= Adele: ''21'' Review|last1=Murphy |first1=John|date= 24 January|publisher=[[MusicOMH]]|accessdate=29 October 2011}}</ref>
<ref name="ripcord">{{Cite news|url=http://www.noripcord.com/reviews/music/adele/21|title=Adele ''21''|publisher=No Ripcord|first=Gary|last=McGinley|date=25 January 2011|accessdate=29 October 2011}}</ref>
<ref name="NYDN">{{cite news|url=http://www.nydailynews.com/entertainment/music/2011/02/22/2011-02-22_adele_21_review_perfect_album_floats_beyond_countries_and_time.html?r=entertainment |title=Adele ''21'' review: Perfect album floats beyond countries and time|work=[[Daily News (New York)|Daily News]]|publisher=[[Mortimer Zuckerman]]|author=Farber, Jim|date=22 February 2011|accessdate=29 October 2011}}</ref>
<ref name="bbc review">{{cite news|url=http://www.bbc.co.uk/music/reviews/fnj9 |title=Review: Adele&nbsp;– ''21''|publisher=BBC Online. [[British Broadcasting Corporation]]|date=17 January 2011 |accessdate=29 October 2011}}</ref>
<ref name="Washington Post">{{cite news|url=http://blog.washingtonpost.com/clicktrack/2011/02/album_review_adele_21.html |title=Adele, ''21'': Album review |work=[[The Washington Post]]|publisher=The Washington Post Company|author=Stewart, Allison|date=22 February 2011|accessdate=29 October 2011}}</ref>
<ref name="Sputnik">{{Cite news|url=http://www.sputnikmusic.com/review/41520/Adele-21/|title=Album review: Adele&nbsp;– 21|publisher=[[Sputnikmusic]]|first=Joseph|last=Viney|date=29 January 2011|accessdate=29 October 2011}}</ref>
<ref name="MSN">{{cite web|url=http://music.uk.msn.com/xclusives/adele/article.aspx?cp-documentid=155818465|title=Album review: Adele&nbsp;– 21|publisher=[[MSN Music]]. [[Microsoft]]|first=Tom|last=Townshend|date=11 January 2011|accessdate=29 October 2011}}</ref>
<ref name="mtvuk">{{cite web|url=http://www.mtv.co.uk/news/adele/254508-adele-21-track-by-track-review|title=Adele '21' – Track By Track Review|first=Joanne|last=Dorken|publisher=[[MTV UK]]. [[MTV Networks]]|date=21 January 2011|accessdate=29 October 2011}}</ref>
<ref name="mtv1658345">{{cite news|url=http://www.mtv.com/news/articles/1658345/adele-21.jhtml|title=Adele Says 21 Has People Thinking 'I'm Sort Of A Manic-Depressive'|first=James|last=Montgomery|date=18 February 2011|publisher=[[MTV News]]. [[MTV Networks]]|accessdate=29 October 2011}}</ref>
<ref name="digital">{{cite news|url=http://www.digitalspy.co.uk/music/news/a341387/adele-halts-london-hammersmith-gig-after-fan-faints.html|title=Adele halts London Hammersmith gig after fan faints|first=Lewis|last=Corner|date=20 September 2011|publisher=[[Digital Spy]]. [[Hachette Filipacchi Médias]]|accessdate=29 October 2011}}</ref>
<ref name="digia328882">{{cite news|url=http://www.digitalspy.co.uk/music/news/a328882/adele-covers-bonnie-raitts-i-cant-make-you-love-me-video.html|title=Adele covers Bonnie Raitt's 'I Can't Make You Love Me': Video|first=Lewis|last=Corner|date=8 July 2011|publisher=Digital Spy. Hachette Filipacchi Médias|accessdate=29 October 2011}}</ref>
<ref name="Matters">{{cite web|url= http://www.popmatters.com/pm/review/137326-adele-21/|title=Adele 21|last=Fairall|first=Jer|publisher=[[Pop Matters]]|date=23 February 2011|accessdate=29 October 2011}}</ref>
<ref name="sheet">{{cite web|url=http://www.musicnotes.com/sheetmusic/mtdVPE.asp?ppn=MN0091336|title=Adele – Rumour Has It – Sheet Music (Digital Download)|publisher=Musicnotes.com. [[Universal Music Publishing Group]]|accessdate=29 October 2011}}</ref>
<ref name="setrelease">{{cite web|url=http://www.billboard.com/articles/columns/chart-beat/465227/adeles-set-fire-to-the-rain-is-next-single-from-21|title=Adele's 'Set Fire to the Rain' Is Next Single From '21'|work=Billboard|publisher=Prometheus Global Media|location=New York|first=Gary|last=Trust|date=9 November 2011|accessdate=14 November 2011}}</ref>
<ref name="setrelease2">{{cite web|url=http://gfa.radioandrecords.com/publishGFA/GFANextPage.asp?sDate=11/21/2011&Format=1|title=Going For Adds: CHR/Top 40 – 21 November 2011|work=[[R&R (magazine)|R&R magazine]]|accessdate=14 November 2011}}</ref>

<!--add references above this line-->
}}

==External links==
* {{MetroLyrics song|adele|rumour-has-it}}<!-- Licensed lyrics provider -->

{{Adele}}

[[Category:2011 singles]]
[[Category:Adele songs]]
[[Category:Songs written by Ryan Tedder]]
[[Category:Songs written by Adele]]
[[Category:XL Recordings singles]]