{{good article}}
{{Infobox hurricane season
|Basin=Atl
|Year=1863
|Track=1863 Atlantic hurricane season summary map.png
|First storm formed=May 24, 1863
|Last storm dissipated=September 30, 1863
|Strongest storm name=[[#Hurricane One|One]], [[#Hurricane Two|Two]], [[#Hurricane Three|Three]], and [[#Hurricane Four|Four]]
|Strongest storm winds=90
|Strongest storm pressure=
|Total storms=9 official, 1 unofficial
|Total hurricanes=5 official, 1 unofficial
|Total intense=0
|Fatalities=200
|Damages=
|five seasons=[[1861 Atlantic hurricane season|1861]], [[1862 Atlantic hurricane season|1862]], '''1863''', [[1864 Atlantic hurricane season|1864]], [[1865 Atlantic hurricane season|1865]]}}

The '''1863 Atlantic hurricane season''' featured five [[Landfall (meteorology)|landfalling]] [[tropical cyclone]]s. In the absence of modern satellite and other remote-sensing technologies, only [[Tropical cyclone|storms]] that affected populated land areas or encountered ships at sea were recorded, so the actual total could be higher. An undercount bias of zero to six tropical cyclones per year between 1851 and 1885 has been estimated.<ref>{{cite book |title=Hurricanes and Typhoons: Past, Present and Future |chapter=The Atlantic hurricane database re-analysis project: Documentation for the 1851–1910 alterations and additions to the HURDAT database |last=Christopher W. Landsea|year=2004 |publisher=Columbia University Press|location=New York |isbn=0-231-12388-4 |pages=177–221}}</ref> There were seven recorded hurricanes and no major hurricanes, which are Category&nbsp;3 or higher on the modern day [[Saffir–Simpson scale]].<ref name="ACE">{{cite report|work=[[Hurricane Research Division]]; [[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=[[National Oceanic and Atmospheric Administration]]|date=2016|title=Atlantic basin Comparison of Original and Revised HURDAT|accessdate=January 9, 2017|url=http://www.aoml.noaa.gov/hrd/hurdat/comparison_table.html|location=Miami, Florida}}</ref> Of the known 1863 cyclones, seven were first documented in 1995 by José Fernández-Partagás and Henry Diaz,<ref name="partagas-1">{{cite book|author=José Fernández-Partagás and Henry F. Diaz|title=A Reconstruction of Historical Tropical Cyclone Frequency in the Atlantic from Documentary and other Historical Sources: Year 1870|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1858-1864/1863.pdf
|publisher=Climate Diagnostics Center, National Oceanic and Atmospheric Administration|location=Boulder, Colorado|year=1995a|accessdate=2011-10-14}}</ref> while the ninth tropical storm was first documented in 2003.<ref name="meta"/> These changes were largely adopted by the [[National Oceanic and Atmospheric Administration]]'s [[Atlantic hurricane reanalysis]] in their updates to the [[HURDAT|Atlantic hurricane database]] (HURDAT), with some adjustments.

Although it is not officially listed in HURDAT, Hurricane Amanda, named after [[USS Amanda (1856)|a ship run aground by the storm]], developed in the Gulf of Mexico on May&nbsp;24. First documented in 2013 by Michael Chenoweth and Cary Mock, the system capsized several other ships and caused damage along the coast of the [[Florida Panhandle]]. The cyclone made [[Landfall (meteorology)|landfall]] near [[Apalachicola, Florida]] exceptionally early in the season, on May 28. Amanda holds the distinction of being the only known hurricane landfall in the United States in the month of May since HURDAT records began in 1851. On land and at sea, the cyclone left at least 110&nbsp;fatalities. Few other storms were notable. In August, the third official storm capsized the American brig ''Bainbridge'' off [[Hatteras, North Carolina]], drowning 80&nbsp;people. The seventh official cyclone caused 10&nbsp;deaths near [[Tampico]], [[Tamaulipas]], after the ship ''J.K.L.'' sunk.

==Systems==
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/05/1863 till:01/11/1863
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/05/1863

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_(TD)_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:24/05/1863 till:29/05/1863 color:C2 text:"Amanda (C2)"
  from:08/08/1863 till:09/08/1863 color:C2 text:"One (C2)"
  from:18/08/1863 till:19/08/1863 color:C2 text:"Two (C2)"
  from:19/08/1863 till:23/08/1863 color:C2 text:"Three (C2)"
  from:27/08/1863 till:28/08/1863 color:C2 text:"Four (C2)"
  from:09/09/1863 till:16/09/1863 color:C1 text:"Five (C1)"
  from:16/09/1863 till:19/09/1863 color:TS text:"Six (TS)"
  from:18/09/1863 till:19/09/1863 color:TS text:"Seven (TS)"
  from:26/09/1863 till:27/09/1863 color:TS text:"Eight (TS)"
  from:29/09/1863 till:30/09/1863 color:TS text:"Nine (TS)"

  bar:Month width:15 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/05/1863 till:01/06/1863 text:May
  from:01/06/1863 till:01/07/1863 text:June
  from:01/07/1863 till:01/08/1863 text:July
  from:01/08/1863 till:01/09/1863 text:August
  from:01/09/1863 till:01/10/1863 text:September
  from:01/10/1863 till:01/11/1863 text:October

TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir–Simpson Hurricane Scale]])"

</timeline>
</center>

===Hurricane One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic hurricane 1 track.png
|Formed=August 8
|Dissipated=August 9
|1-min winds=90
}}
A Category&nbsp;2 hurricane was first encountered by the ship ''Francis B. Cutting'' about {{convert|630|mi|km|abbr=on}} south-southeast of [[Cape Race]], [[Newfoundland and Labrador|Newfoundland]], on August&nbsp;8.<ref name="partagas-1"/>{{Atlantic hurricane best track}} With winds estimated at 105&nbsp;mph (165&nbsp;km/h), the storm weakened to a Category&nbsp;1 hurricane several hours later as it tracked northeastward. The cyclone was last noted late on August&nbsp;9.{{Atlantic hurricane best track}}
{{Clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic hurricane 2 track.png
|Formed=August 18
|Dissipated=August 19
|1-min winds=90
}}
The ship ''American Congress'' first encountered this storm on August&nbsp;18,<ref name="partagas-1"/> about {{convert|320|mi|km|abbr=on}} south-southeast of [[Sable Island]].{{Atlantic hurricane best track}} Reports from ''American Congress'' and other ships in the cyclone's path suggest that the storm was a Category&nbsp;2 hurricane that moved east-northeastward offshore [[Atlantic Canada]] between August&nbsp;18 and August&nbsp;19. The hurricane caused the loss of the ship ''B.R. Millam'', whose crew transferred to the ''Thebes'', while the ''Herzogin'' lost several masts and sails.<ref name="partagas-1"/>
{{Clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic hurricane 3 track.png
|Formed=August 19
|Dissipated=August 23
|1-min winds=90
|Pressure=975
}}
A hurricane was first seen by the ship ''Addie Barnes'' on August&nbsp;19 in the western Atlantic Ocean, about midway between the southeastern [[Bahamas]] and [[Bermuda]]. It headed northwestward, causing heavy rains and damage to the [[Outer Banks]] of [[North Carolina]], but remained offshore.<ref name="partagas-1"/> It turned northeastward and made landfall near [[Dartmouth, Nova Scotia]], before transitioning into an [[extratropical cyclone]].{{Atlantic hurricane best track}} Several vessels were struck by the hurricane.<ref name="partagas-1"/> The American brig ''Bainbridge'' capsized in the storm off [[Hatteras, North Carolina|Hatteras]] early on August&nbsp;21 with the loss of 80&nbsp;lives.<ref name ="toll">{{cite report|author=Edward N. Rappaport and José Fernández-Partagás|title=The Deadliest Atlantic Tropical Cyclones, 1492–1996: Cyclones with 25+ deaths|publisher=National Oceanic and Atmospheric Administration|work=National Hurricane Center|year=1996|accessdate=January 9, 2017|url=http://www.nhc.noaa.gov/pastdeadlyapp1.shtml?}}</ref> The sole survivor was picked up by the ''South Boston'' on the evening of August&nbsp;22. The ship ''American Congress'' encountered this hurricane on August&nbsp;22 off [[Georges Bank]]. On August&nbsp;23, the ''Minor'' was wrecked on the south side of [[St. Paul Island (Nova Scotia)|St Paul Island]], off the northeastern tip of Nova Scotia. Two ships, including the ''Ashburton'' recorded a barometric pressure of {{convert|975|mbar|inHg|abbr=on}}, the lowest in relation to the storm.<ref name="partagas-1"/>
{{Clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic hurricane 4 track.png
|Formed=August 27
|Dissipated=August 28
|1-min winds=90
}}
This hurricane is known from two ship reports. The steamship ''Dolphin'', sailing from [[Key West, Florida|Key West]] to [[New York City]], encountered a hurricane on the night of August&nbsp;27 and for 18&nbsp;hours thereafter.<ref name="partagas-1"/> Wind reports from the ship suggested that the storm was a Category&nbsp;2 hurricane winds of 105&nbsp;mph (165&nbsp;km/h).{{Atlantic hurricane best track}} The brig ''Camilla'' was struck about {{convert|200|mi|km|abbr=on}} from [[Sandy Hook]] in [[New Jersey]] on August&nbsp;28 and forced to return to port for repairs.<ref name="partagas-1"/> The storm was last noted later that day.{{Atlantic hurricane best track}}
{{Clear}}

===Hurricane Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic hurricane 5 track.png
|Formed=September 9
|Dissipated=September 16
|1-min winds=70
}}
A tropical storm was initially encountered near the [[Lesser Antilles]] on September&nbsp;9 by the ship ''Frank W.''. Later that day, the ship ''Mary Ann'' was dismasted.<ref name="partagas-1"/> It is estimated that the cyclone intensified into a Category&nbsp;1 hurricane around 12:00&nbsp;UTC on September&nbsp;9, with winds reaching 80&nbsp;mph (130&nbsp;km/h). The system moved north-northwestward or nort hward for several days and closely approached Bermuda late on September&nbsp;11.{{Atlantic hurricane best track}} Around that time, some ships to the southeast of the island were damaged during the storm and put into Bermuda as a result.<ref name="partagas-1"/> By early on September&nbsp;13, the hurricane was beginning to move in a more northeasterly direction.{{Atlantic hurricane best track}} The bark ''Machae'' was dismasted on September&nbsp;14.<ref name="partagas-1"/> The cyclone weakened to a tropical storm early the following day.{{Atlantic hurricane best track}} On September&nbsp;16, the ''Glad Tiding'' last observed the storm about halfway between Newfoundland and [[Ireland]].<ref name="partagas-1"/>
{{Clear}}

===Tropical Storm Six===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic tropical storm 6 track.png
|Formed=September 16
|Dissipated=September 19
|1-min winds=60
}}
A tropical storm formed near South Florida on September&nbsp;16. Later that day, the sloop ''Eliza'' was dismantled at Matanilla Reef, about {{convert|50|mi|km|abbr=on}} north of [[Grand Bahama]].<ref name="partagas-1"/> Moving north-northeastward, the storm began approaching the [[Carolinas]] on September&nbsp;17. The cyclone intensified and peaked with winds of 70&nbsp;mph (110&nbsp;km/h) that same day. Around 13:00&nbsp;UTC on September&nbsp;18, the system made landfall in [[Emerald Isle, North Carolina]]. Thereafter, the storm tracked rapidly north-northeastward and lost tropical characteristics near the [[Connecticut]]&ndash;[[Massachusetts]]&ndash;[[New York (state)|New York]] state lines early on September&nbsp;19.{{Atlantic hurricane best track}}

In [[South Carolina]], strong winds and large waves impacted the [[Charleston, South Carolina|Charleston]] area. A number of homes were destroyed, forcing some occupants to ride out the storm completely exposed to the weather. Waves overtopped the levees, flooding army camps along the coast.<ref>{{cite news|url=https://www.newspapers.com/clip/8258376/the_wheeling_daily_intelligencer/|title=From Charleston|date=September 24, 1863|newspaper=[[The Intelligencer and Wheeling News Register]]|page=3|accessdate=January 13, 2017|location=New York City, New York|via=[[Newspapers.com]]}} {{open access}}</ref> On September&nbsp;18, two schooners were capsized in the Lower [[Potomac River]]. Crops were also destroyed in the area, while a railroad bridge was carried away. A ship was demasted off [[Cove Point Light|Cove Point]] in [[Chesapeake Bay]] on September&nbsp;18.<ref name="partagas-1"/> Heavy rainfall in [[Pennsylvania]] resulted in flooding along the [[Delaware River]] and [[Lehigh Canal]], especially in [[Easton, Pennsylvania|Easton]]. In [[Jim Thorpe, Pennsylvania|Jim Thorpe]], then known as Mauch Chunk, three bridges washed away, while a dam was destroyed.<ref>{{cite news|url=https://www.newspapers.com/clip/8258494/the_baltimore_sun/|title=The Equinoctial Storm&ndash;A Flood on the Lehigh and Delaware Rivers|date=September 19, 1863|newspaper=[[The Baltimore Sun]]|page=1|accessdate=January 13, 2017|location=Easton, Pennsylvania|via=[[Newspapers.com]]}} {{open access}}</ref> In New York City, gale force winds were observed at harbor.<ref name="partagas-1"/>

{{Clear}}

===Tropical Storm Seven===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic tropical storm 7 track.png
|Formed=September 18
|Dissipated=September 19
|1-min winds=50
}}
On September&nbsp;18, a heavy northern gale wrecked the ship ''Smoker'' on the bar at [[Tampico, Tamaulipas]], in [[Mexico]]. On September&nbsp;19, two ships were capsized, the ''John Howell'' and the ''J.K.L''. After the latter sunk, 10&nbsp;people drowned, including the captain. No specific locations are known for these shipwrecks so no complete track for this storm is known, but it was active in the western Gulf of Mexico beginning on September&nbsp;18.<ref name="partagas-1"/> The storm made landfall early on September&nbsp;19 in a rural area of [[Tamaulipas]] to the north of Tampico.{{Atlantic hurricane best track}} Based on John Kaplan and Mark DeMaria's inland decay model created in 1995, it is estimated that the cyclone dissipated several hours later.<ref name="partagas-1"/>
{{Clear}}

===Tropical Storm Eight===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic tropical storm 8 track.png
|Formed=September 26
|Dissipated=September 27
|1-min winds=50
}}
Three ships reported encountering a tropical storm on September&nbsp;26 in the western Atlantic, beginning with the ''Horace E. Bell'' about {{convert|320|mi|km|abbr=on}} west-southwest of Bermuda.<ref name="partagas-1"/>{{Atlantic hurricane best track}} Data from these ships indicated that the storm peaked with winds of 60&nbsp;mph (95&nbsp;km/h). The storm moved rapidly north-northwestward and was last noted offshore the [[Mid-Atlantic states|Mid-Atlantic]] early on September&nbsp;27.{{Atlantic hurricane best track}}
{{Clear}}

===Tropical Storm Nine===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1863 Atlantic tropical storm 9 track.png
|Formed=September 29
|Dissipated=September 30
|1-min winds=60
|Pressure=999
}}
A tropical storm formed offshore southeast [[Texas]] on at 00:00&nbsp;UTC on September&nbsp;29,{{Atlantic hurricane best track}} though the system exhibited some non-tropical characteristics.<ref name="meta">{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_master.html|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|author=Christopher W. Landsea|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|accessdate=January 8, 2016|location=Miami, Florida|display-authors=etal}}</ref> Moving northeastward, the cyclone made landfall near [[Galveston, Texas]], about twelve hours later with winds of 70&nbsp;mph (110&nbsp;km/h).{{Atlantic hurricane best track}} Around that time, a barometric pressure of {{convert|999|mbar|inHg|abbr=on}} was observed in Houston, the lowest pressure in relation to the storm.<ref name="meta"/> At 12:00&nbsp;UTC, the cyclone transitioned into an extratropical cyclone over southwestern [[Louisiana]]. The remnants moved north-northeastward until dissipating over southern [[Mississippi]] on October&nbsp;1.{{Atlantic hurricane best track}}

In Texas, strong winds and tree damage occurred at [[Sabine Pass]], where the schooner ''Manhasett'' was driven ashore. The ''Manhasett'', a [[Union (American Civil War)|Union]] ship, was then captured by the [[Confederate States Army|Confederates]]. In Louisiana, heavy rainfall at the [[Atchafalaya Basin]] over the course of two and a half days forced Confederate troops to remain at Morgan’s Ferry. Rainfall from the storm in [[New Orleans]] ended a drought in the city.<ref name="rola">{{cite book|url=http://www.wpc.ncep.noaa.gov/research/lahur.pdf|title=Louisiana Hurricane History|author=David M. Roth|publisher=National Weather Service, Southern Region Headquarters|date=January 13, 2010|accessdate=January 7, 2017}}</ref>
{{Clear}}

===Other systems===
[[File:Amanda 1863 path.png|thumb|right|Proposed path of Hurricane "Amanda"]]
In addition, a tropical system developed in the Gulf of Mexico on May&nbsp;24, based on analysis from Michael Chenoweth and Cary Mock in 2013. Given the name Hurricane Amanda after a Union ship it washed ashore, the cyclone is estimated to have intensified into a hurricane on May&nbsp;27. Amanda moved northward and made landfall to the west of [[Apalachicola, Florida]], on May&nbsp;28. Early that day, the ''[[USS Amanda (1856)|USS Amanda]]'' observed a barometric pressure of {{convert|975|mbar|inHg|abbr=on}}, the lowest in association with the cyclone. The storm weakened while moving inland, before accelerating ahead of a [[cold front]] and becoming an extratropical cyclone over [[Kentucky]] late on May&nbsp;29. An extratropical low absorbed the remnants of the storm over [[Quebec]] on May&nbsp;31. According to Chenoweth, Amanda is the only documented United States landfalling hurricane in the month of May since HURDAT records began in 1851.<ref name="Hurricane Amanda Report"/>

Amanda caused heavy damage in the northeast [[Gulf of Mexico]] and the [[Florida Panhandle]]. In addition to sinking the ''USS Amanda'', several other ships encountered the storm or were also capsized. At least 38&nbsp;deaths occurred at sea. In [[St. Marks, Florida]], strong winds destroyed homes and fences,<ref name="Hurricane Amanda Report"/> as well as the salt works, ruining about 40,000&nbsp;bushels of salt.<ref name="fwo">{{cite news|url=https://www.newspapers.com/clip/8258637/fayetteville_weekly_observer/|title=Terrible Storm|date=June 8, 1863|newspaper=Fayetteville Weekly Observer|page=3|accessdate=January 13, 2017|via=[[Newspapers.com]]}} {{open access}}</ref> Storm surge inundated crops and the railroad tracks. A total of 40&nbsp;people and 48&nbsp;mules and oxen drowned.<ref name="Hurricane Amanda Report"/><ref name="fwo"/> An additional 32&nbsp;people drowned at Dickerson Bay and Goose Creek.<ref name="Hurricane Amanda Report"/> Some coastal forts were damaged, while tents and equipment used by Confederate troops were lost.<ref name="fwo"/> In [[Tallahassee, Florida|Tallahassee]], heavy rainfall and severe gales were reported,<ref name="Hurricane Amanda Report"/> damaging homes and other properties.<ref name="fwo"/> The hurricane caused upward of 110&nbsp;fatalities.<ref name="Hurricane Amanda Report">{{cite journal|url=http://journals.ametsoc.org/doi/pdf/10.1175/BAMS-D-12-00171.1|author=Michael Chenoweth and Cary J. Mock |title = Hurricane "Amanda": Rediscovery of a Forgotten U.S. Civil War Florida Hurricane |journal = Bull. Am. Meteorol. Soc. |volume = 94 |issue = 11 |pages = 1735–42 |date = 2013 |doi = 10.1175/BAMS-D-12-00171.1 |bibcode = 2013BAMS...94.1735C|accessdate=January 9, 2017}}</ref>

On September&nbsp;11, the ship ''North American'' reported a 'hurricane from SW' at a position that would indicate a storm centre just east of Newfoundland. No evidence of a tropical origin for this cyclone has been found.<ref name="partagas-1"/>

== See also ==
{{Portal|Tropical cyclones}}
*[[List of tropical cyclones]]
*[[List of Atlantic hurricane seasons]]
*[[Tropical cyclone observation]]
*[[Atlantic hurricane reanalysis project]]

==References==
{{Reflist|2}}

{{TC Decades|Year=1860|basin=Atlantic|type=hurricane}}

[[Category:1860–1869 Atlantic hurricane seasons| ]]
[[Category:Articles which contain graphical timelines]]