{{Orphan|date=June 2013}}

'''Comprehension of idioms''' is the act of processing and understanding [[idioms]]. Idioms are a common type of [[figure of speech]]. Based on common [[linguistic]] definitions, an idiom is a combination of words that contains a [[meaning (linguistic)|meaning]] that cannot be understood based on the [[literal language|literal]] [[definition]] of the individual words.<ref name="Liu">Liu, D. (2008). Idioms: Description, comprehension, acquisition, and pedagogy. New York, NY: Routledge.</ref> An example of an idiom is ''hit the sack'', which means ''to go to bed''. It can be used in a sentence like the following: ''I'm so exhausted that I'm going to hit the sack now''.

Traditionally, idiom comprehension was thought to require a distinct processing mode other than [[literal language]] comprehension.  Subsequent research suggested that the comprehension of idioms could be explained in the context of general models of comprehension.  Contemporary researchers have also posited that different modes of processing are required for distinct types of idioms. Factors, such as idiom familiarity, [[Transparency (linguistic)|transparency]], and [[Context (language use)|context]] are found to influence idiom comprehension. 

Recent [[neurolinguistic]] research has found, using various techniques, several [[neural substrates]] that are associated with idiom comprehension, such as the left [[temporal lobe]] and [[prefrontal cortex]].

== History ==

[[Psycholinguistic]] research in idiom comprehension began in the 1970s.  Early research on [[figurative language]] typically assumed that comprehending figurative and literal language involved different kinds of processes. The non-compositional models of idiom comprehension were constructed based on this assumption. 

[[File:Noncompositional models for idiom comprehension.png|thumb|Noncompositional Models.]]

=== Non-compositional Models ===

Non-compositional models in general assume that idioms are stored like long words in memory.<ref name="Caillies">Caillies, S., & Butcher, K. (2007). Processing of idiomatic expressions: Evidence for a new hybrid view. ''Metaphor and Symbol'', 22, 79-108.</ref> For example, the entire phrase ''kick the bucket'' is represented by the meaning ''to die''. The words kick and bucket separately do not contribute to the idiomatic meaning. Since individual words do not matter in an idiomatic [[expression (language)|expression]], they are thought to be processed together as one entity during comprehension.  In contrast, processing a literal expression requires the meaning of each word to be derived and then perceived in relation to the other words in the phrase. Non-compositional models include the ''literal first hypothesis'', ''lexical representation hypothesis'', and the ''direct access hypothesis''. 

'''The Literal First Hypothesis''' 

*The literal first hypothesis suggests that an idiom is first processed literally upon encounter.<ref>Swinney, D. A., & Culter, A. (1979). The access and processing of idiomatic expression. ''Journal of Verbal Learning and Verbal Behaviour, 18'', 523-534</ref> Idioms are often [[ambiguity|ambiguous]] since they may be processed in two ways: ''literally'' and ''idiomatically''. For example, ''kick the bucket'' can be interpreted as to ''strike the pail with one’s foot''. The literal first hypothesis proposes that, only after an appropriate literal meaning is not found, a mental idiom list is assessed to retrieve the idiomatic meaning. This hypothesis posits that there are separate storage for normal words and idioms, and that literal processing and figurative processing require different modes. Researchers claimed that this hypothesis was supported when people reported that they made an idiomatic interpretation first after they have read a set of idiom-containing sentences, but a literal interpretation first if they have seen a set of literal but ambiguous sentences. 

'''The Lexical Representation Hypothesis'''

*Contrary to the previous model, the lexical representation hypothesis posits that idioms are stored along with normal words in memory.<ref>Bobrow, S. A., & Bell, S. M. (1973). On catching on to idiomatic expressions. ''Memory & Cognitio, 1'', 343-346</ref> Idioms are processed both literally and figuratively simultaneously. However, the figurative meaning (lexical representation) of the idiom is accessed first since the word string is stored and processed as one entry. Literal processing consumes more time since it requires the understanding of each word individually. Research conducted to support this hypothesis found that people recognized [[grammatical]] idioms as meaningful expressions more quickly than non-idiomatic but grammatical phrases when they were presented out of context.

'''The Direct Access Hypothesis'''

*The direct access hypothesis proposes that literal processing does not necessary occur when an idiom is encountered. It is possible that people completely bypass such mode when faced with a highly familiar idiom or have sufficient context to infer an idiomatic interpretation. In these cases, the idiomatic meaning is directly accessed.<ref name="Gibbs">Gibbs, R. W. (1980). Spilling the beans on understanding and memory for idioms in context. ''Memory and Cognition, 8'', 149-156</ref> This hypothesis is based on the findings that demonstrated that people processed idiomatic sentences quicker than literal ones, and tended to interpret idiomatic expressions figuratively even when they are used in a literal sense.

=== Criticisms of Non-compositional Models ===

There are several criticisms for the non-compositional models. ''First'', studies demonstrated that idiom expressions are not processed more slowly than literal expression. In fact, it is often the opposite, which is in contrary to the prediction of literal first hypothesis.  ''Second'', idioms have been found to be more than just “frozen phrases” or long words. For example, some idioms can be transformed to some extent and  still be recognized and understood. For example, ''spill the beans'' can be used as “the beans were spilt by Mary”. This is possible because ''spill the bean'' can be mapped on the meaning “reveal the secret”, i.e. ''spill'' (reveal) and ''beans'' (secret). Such idiom shows that the internal structure of the word string matters during comprehension. The meaning of some idioms, like ''play with fire'', can be also inferred from literal interpretation of their components (i.e. ''to do something dangerous''). These findings reveal that idioms are not a [[homogeneous]], distinct group and thus may not involve different processing strategies from those for literal expressions.<ref name="Titone">Titone, D. A., & Connine, C. M. (1999). On the compositional and noncompositional nature of idiomatic expressions. ''Journal of Pragmatics, 31'', 1655-1674.</ref>

=== Compositional Models ===

Compositional models assume that idiom comprehension uses normal language processing.<ref name="Caillies"/> When an idiomatic expression is encountered, it is processed incrementally like a normal [[expression (language)|expression]]. Components of an idiomatic word string contribute to a [[literal and figurative language|figurative]] meaning in either a literal or [[metaphor]]ical way. Compositional models include the ''configurational hypothesis'' and the ''conceptual metaphor hypothesis''. 

[[File:Compositional models of idiom comprehension.png|thumb|upright=0.70|left|Compositional models.]]

'''The Configurational Hypothesis'''

*The configurational hypothesis rose in response to the former hypotheses. It was based on a finding that demonstrated that people did not make an idiomatic interpretation once the first part of an idiom is encountered (against the prediction of the ''direct access hypothesis'') or at the end of the idiom (against the prediction of the ''lexical representation hypothesis''). Instead, the configurational hypothesis posits that a sufficient portion of an idiomatic word string must be processed literally before the idiom is identified.<ref>Cacciari, C., & Tabossi, P. (1988). The comprehension of idioms. ''Journal of Memory and Language, 27'', 668-683</ref> The point where most people recognize the string as an idiom is the ''idiom key''. Once the key is encountered, the words in the rest of the string will not be processed literally. The idiom key is the most important part of the idiom. If it is replaced by other words, the idiomatic meaning cannot be activated.

'''The Conceptual Metaphor Hypothesis'''

*The conceptual metaphor hypothesis proposes that [[metaphors]] are fundamental to human thought and they influence the comprehension of many aspects of language, including idioms.<ref>Gibbs, R. W., Bogdanovich, J. M., Sykes, J. R., & Barr, D. J. (1997). Metaphor in idiom comprehension. ''Journal of Memory and Language, 37'', 141–154</ref> An example of a [[conceptual metaphor]] is “love is a journey”. This metaphor is embedded in the idiom-containing sentences like ''their marriage is on the rocks'' and ''our relationship is at a cross-road''. This hypothesis suggests that conceptual metaphors facilitate the understanding of idiomatic expressions. Individual words in the idiom can metaphorically contribute to its figurative meaning. In fact, research demonstrated that conceptual metaphors are activated during idiom comprehension. The words associated with the metaphor (e.g., journey) were more quickly identified as meaningful after the presentation of the idiom. {{citation needed|date=March 2015}}

=== Criticisms of Compositional Models ===

The prediction of the configurational hypothesis was not supported by research findings. Researchers found that even after a word string is recognized as an idiom, its literal meaning is still activated. Another criticism of the compositional models concerns the role of familiarity in idiom comprehension. As discovered by research on non-compositional models, idiomatic expressions are processed faster than non-idiomatic expressions. This is likely due to people’s familiarity of idioms. This suggests that when highly familiar idioms are encountered, people may not need to engage in literal processing or utilize conceptual metaphor to infer their meanings.<ref name=Titone/>

== Recent Models ==

[[File:Dual idiom representation model.png|thumb|upright=0.50|right|The dual idiom representation model.]]
Research in idiom comprehension continues today. Non-compositional and compositional models both inform contemporary researchers. One of the recent models includes the dual idiom representation model.

'''The Dual Idiom Representation Model'''

*The dual idiom representation model suggests that idiomatic expressions are simultaneously “long words” and compositional phrases.<ref name= Titone/> Thus, idiom comprehension involves both direct memory [[recall (memory)|retrieval]] and normal language processing.  Idioms behave noncompositionally to the degree they are familiar. When highly familiar idioms encountered, their idiomatic meanings can be directly activated. In addition, idioms can behave compositionally if they are decomposable. Decomposability of an idiom refers to the extent to which the literal meaning of the individual words in the word string participate in the overall figurative meaning of the idiom. As suggested earlier, the idiom ''play with fire'' is decomposable while ''kick the bucket'' is nondecomposable. Comprehension is facilitated in a decomposable idiom since there are commonality between the results of its literal analysis and its idiomatic meaning. Research shows that familiarity had the larger affect on the speed of idiom comprehension than decomposability. Decomposability nonetheless facilitates processing the meaningfulness of the idiomatic expression.<ref>Libben, M. R., & Titone, D. A. (2008). The multidetermined nature of idiom processing. ''Memory & Cognition, 36'', 1103-1121.</ref>

== Factors that affect Idiom Comprehension ==

Many factors have an effect on the comprehension of idioms. They can influence the processing speed and understandability of the idioms. Some of these factors include the familiarity, [[Transparency (linguistic)|transparency]], and [[Context (language use)|context]] of use of the idioms.<ref name="Liu"/>

=== Familiarity ===

Idiom familiarity is typically defined as how frequently an idiom is encountered in a language community. Subjective ratings of idiom familiarity are usually obtained from members of the language community. An example of a more familiar English idiom is ''pain in the neck'', while a less familiar idiom is ''paddle his own canoe''. Research demonstrated that familiar idioms are processed quicker and more accurately than unfamiliar ones.<ref>Schweigert, W. A. (1986). The comprehension of familiar and less familiar idioms. ''Journal of Psycholinguistic Research, 15'', 33-45.</ref> As the dual idiom representation model suggests, highly familiar idioms may lead to direct memory retrieval. Processing unfamiliar idioms, on the other head, requires [[context (language use)|contextual information]] and [[commonsense]] knowledge.

=== Transparency ===

Idiom transparency can refer to the “literalness” of an idiom; how easy it is to understand an idiom based on the words it contains. Idioms can be sorted depending on their degree of [[Transparency (linguistic)|transparency]]. Three categories of idioms have been identified: ''decomposable'', ''abnormally decomposable'', and ''nondecomposable''.<ref name="Gibbs" /> Decomposable idioms are composed of words that literally contribute to their overall figurative meaning, e.g. ''pop the question''. ''Pop'' refers to ''sudden'' and ''the question'' refers to ''marriage proposal''. Abnormally decomposable idioms contain words that are associated with the overall figurative meaning of the idiom but in a metaphorical way, e.g. ''spill the beans''. ''Spill'' mapping on to ''reveal'' and ''beans'' metaphorically representing ''secret''. Nondecomposable idioms are made of words that do not reflect their idiomatic meaning, e.g. ''kick the bucket''. People are found to respond to both types of decomposable idioms faster than nondecomposable ones.

=== Context of use ===

Research in children highlighted the important effects of [[Context (language use)|context]] on idiom comprehension. It was found that children understand idiomatic expression more accurately when they are shown in informative contexts than when they are presented in isolation.<ref>Levorato, M. C., Roch, M., & Nesi, B. (2007). A longitudinal study of idiom and text comprehension. ''Journal of Child Language'', 34, 473-494.</ref> When they are encountered out of context, children tended to interpret idioms literally. The ability to use contextual information in language processing has also been found to influence children’s performance in idiom comprehension. Adults, however, are more affected by the familiarity of the idiom.

== Neurolinguistic Research ==

Researchers have used various means to explore the brain regions that are associated with idiom comprehension. The investigations involve examining [[brain damage]] patients, and conducting [[rTMS|transcranial magnetic stimulation (rTMS)]] and [[fMRI|functional magnetic resonance imaging (fMRI)]] studies. Brain damages in the patients were mostly caused by strokes or traumatic accidents. [[rTMS]] causes temporary cerebral lesion, and thereby impairs the target area’s functioning. [[fMRI]] measures brain activity by detecting changes in blood flow. The areas that have been studied include the [[right hemisphere]], [[left hemisphere]], and [[prefrontal cortex]].

=== Right Hemisphere ===

Since [[aphasic]] patients often preserve their ability to use familiar phrases (i.e. [[idioms]] and [[proverb]]s), researchers hypothesized that they may be stored and processed in different brain regions than novel phrases. It was found that patients who experienced right-brain damage (RBD) showed more impairment in idiom comprehension than patients with left-brain damage (LBD). RBD patients however performed better in comprehending novel sentence compared to patients with LBD.<ref>Van Lancker, D. R., & Kempler, D. (1987). Comprehension of familiar phrases by left-but not by right-hemisphere damaged patients. ''Brain and Language, 32'', 265-277.</ref> This suggests that the [[right hemisphere]] is involved in the comprehension of idioms. However, later research has suggested that RBD patients’ difficulties in idiom comprehension, as demonstrated by sentence-to-picture matching task, may be due to deficits in their [[visuospatial]] abilities, rather than impairment in [[linguistic]] processing. The right hemisphere shows dominance in visuospatial processing.<ref>Papagno, C., Curti, R., Rizzo, S., Crippa, F., & Colombo, M. R. (2006). Is the right hemisphere involved in idiom comprehension? A neuropsychological study. ''Neuropsychology, 20'', 598-606.</ref>

[[File:Gray's Anatomy plate 517 brain.png|thumb|left|Gray's Neuroanatomy.]]

=== Left Hemisphere ===

Other researchers have hypothesized that comprehension of nondecomposable idioms mainly involved [[Lateralization of brain function|left hemispheric]] activity. When healthy adults were studied, left [[temporal lobe|temporal]] [[rTMS]] was found to influence the reaction time and accuracy of both idiom comprehension and literal sentence processing. In particular, it increased processing time and reduced accuracy in understanding.  When rTMS was applied to the left [[frontal lobe|frontal]], right [[frontal lobe|frontal]] and right [[temporal lobe|temporal]], there were no effects on idiom comprehension.<ref>Papagno, C., Oliveri, M., Romero, L. (2002). Neural correlates of idiom comprehension. ''Cortex, 38'', 895-898.</ref> Recent research with healthy adults using fMRI also found that, when processing idioms that were not literally interpretable (i.e.,  ''raining cats and dogs''), the [[Broca’s area]] in the left prefronto-temporal network was activated. However, when literally interpretable idioms (i.e., ''break the ice'') were processed, the left [[medial frontal gyrus]] (MFQ) and [[superior frontal gyrus]] (SFG) were activated.<ref>Hillert, D. G., & Buracas, G. T. (2009). The neural substrates of spoken idiom comprehension. ''Language and Cognitive Processes, 24'', 1370-1391.</ref>

=== Prefrontal Cortex ===

[[Prefrontal cortex]] (PFC) is suggested to be important for idiom comprehension. It may play a role in selecting the appropriate interpretation and suppressing the incorrect ones when an idiomatic expression is encountered. Research using [[fMRI]] indicated that the left [[temporal cortex]], left superior medial frontal gyrus, and left [[inferior frontal gyrus]] were activated when idiomatic phrases were presented.<ref>Lauro, L. J. R., Tettamanti, M., Cappa, S. F., & Papagno, C. (2008). Idiom comprehension: A prefrontal task? ''Cerebral Cortex, 18'', 162-170.</ref> In addition, the right [[superior temporal gyrus|superior]] and [[middle temporal gyrus|middle temporal gyri]], the right [[Poles of cerebral hemispheres|temporal pole]], and the right [[inferior frontal gyrus]] were activated as well. In contrast, the left [[Inferior parietal lobule|inferior parietal lobe]] and the right [[supramarginal gyrus]] were activated when literal sentences were presented.

== Future Research ==

Future research in this field should continue to investigate which brain regions are associated with idiom processing, in order to resolve the ongoing debate on [[lateralization of brain function|hemispheric specialization]] in [[figurative language]] comprehension.<ref>Kasparian, K. (2013) Hemispheric differences in figurative language processing: Contributions of neuroimaging methods and challenges in reconciling current empirical findings. ''Journal of Neurolinguistics, 26,'' 1-12.</ref> A recent review suggested that the familiarity affect which brain region is activated during the comprehension of figurative language. Less familiar expressions are suggested to be processed in the [[right hemisphere]], while more familiar ones are processed predominately in the [[left hemisphere]]. More brain imaging research needs to be conducted to test this hypothesis.  

Additionally, future research could use [[magnetoencephalography|magnetoencephalography (MEG)]] to explore the temporal dynamics of idiom comprehension. Recent research using MEG has found that, when idioms containing action verbs (i.e. ''kick the bucket'') were processed, [[motor cortex|motor regions]] of the brain were activated. The anterior fronto-temporal cortex, a region previously found to distinguish between literal and figurative processing, was activated at the same time as well. This suggests that literal and figurative meanings are processed in parallel to some extent, which gives support to the configurational hypothesis.<ref>Boulenger, V., Shtyrov, Y., & Pulvermuller, F. (2012). When do you grasp the idea? MEG evidence for instantaneous idiom understanding. ''NeuroImage, 59,'' 3502-3513.</ref> More work with MEG should be done to verify these findings.

== See also ==

*[[Idioms]]
*[[List of English language idioms]]
*[[Metaphors]]
*[[Conceptual metaphor]]
*[[Figure of speech]]
*[[Literal and figurative language]]
*[[Linguistics]]
*[[Psycholinguistics]]
*[[Neurolinguistics]]
*[[Cognitive Neuroscience]]

== References ==

{{reflist}}



[[Category:Idioms]]