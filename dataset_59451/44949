[[Image:Sonarbuoy loaded on aircraft.jpg|thumb|right|180px|Sonobuoy being loaded onto an USN P-3C Orion aircraft]]
A '''sonobuoy''' (a [[portmanteau]] of [[sonar]] and [[buoy]]) is a relatively small buoy (typically {{convert|5|in|cm|disp=or|order=flip|abbr=on}}, in diameter and {{convert|3|ft|cm|abbr=on|disp=or|order=flip}} long) expendable sonar system that is dropped/ejected from [[aircraft]] or ships conducting [[anti-submarine warfare]] or [[underwater acoustics|underwater acoustic]] research.

==Theory of operation==

The buoys are ejected from aircraft in canisters and deploy upon water impact.  An inflatable surface float with a [[radio transmitter]] remains on the surface for communication with the aircraft, while one or more [[hydrophone]] sensors and stabilizing equipment descend below the surface to a selected depth that is variable, depending on environmental conditions and the search pattern.  The buoy relays [[acoustics|acoustic]] information from its hydrophone(s) via [[UHF]]/[[VHF]] radio to operators on board the aircraft.

==History==
[[Image:Airlaunched sonobuoy 179.jpg|thumb|left|right|P-3 Orion paradropping a sonobuoy.]]
[[Image:SSQ-47B sonobuoy.jpg|thumb|right|AN/SSQ-47B active pinger ranging sonar sonobuoy (frequency #4) and shipping container (octagonal form aids stacking.)]]
With the technological improvement of the [[submarine]] in modern warfare, the need for an effective tracking system was born. Sound Navigation And Ranging ([[Sonar|SONAR]]) was originally developed by the British&mdash;who called it ASDIC&mdash;in the waning days of [[World War I]]. At the time the only way to detect submarines was by listening for them (passive sonar), or visually by chance when they were on the surface recharging their [[battery (electricity)|battery]] banks. Air patrols (the British mostly used small [[airship]]s which had the advantage of long endurance) could spot surfaced submarines and occasionally, when conditions were right, even submerged ones as the diving depth of submarines of the era was so limited. If contact was made, they would follow the submarine while summoning surface ships by radio to attack it. 

Sonar saw extremely limited use and was mostly tested in the [[Atlantic Ocean]] with few naval officers seeing any merit in the system. With the end of World War I came the end to serious development of sonar in the United States, a fact that was to be fatal in the early days of [[World War II]]. However, considerable development of ASDIC took place in the United Kingdom, including integration with a plotting table and weapon.

While the United Kingdom pursued the development of sonar during the interwar period, the [[United States Coast and Geodetic Survey]] during the 1920s developed the [[radio acoustic ranging]] method of fixing the position of [[survey ship]]s during [[hydrographic survey]] operations by detonating a small explosive at the location of the ship, recording the time it took for the sound of the explosion to reach distant [[hydrophone]]s mounted at shore stations or aboard manned station ships, and radioing the time of receipt of the sound to the ship, allowing the crew to make precise position fixes by using [[triangulation]]. In 1931, the Coast and Geodetic Survey proposed the replacement of manned station ships with "radio-sonobuoys", and placed the new buoys in service beginning in July 1936. These buoys weighed 700 pounds (317.5 kg), could be deployed or recovered by Coast and Geodetic Survey ships in five minutes, and were equipped with subsurface hydrophones, batteries, and radio transmitters that automatically sent a radio signal when their hydrophones detected the sound of a ranging explosion. These "radio-sonobuoys" were the ancestors of the sonobuoys that began to appear in the 1940s.<ref name=theberge20091202>[https://www.hydro-international.com/content/article/system-without-fixed-points Theberge, Alfred E., "System Without Fixed Points: Development of the Radio-Acoustic Ranging Navigation Technique (Part 1)," hydro-international.com, December 2, 2009.]</ref><ref name=popmech/><ref name=HollerJan2014>[http://www.navairdevcen.org/PDF/THE EVOLUTION OF THE SONOBUOY.pdf Holler, Roger A., "The Evolution of the Sonobuoy From World War II to The Cold War," ''U.S. Navy Journal of Underwater Acoustics'', January 2014, p. 323.]</ref><ref>[http://celebrating200years.noaa.gov/breakthroughs/hydro_survey/welcome.html#methods celebrating200years.noaa.gov Top Tens: Breakthroughs: Hydrographic Survey Techniques: Acoustic Survey Methods: Radio Acoustic Ranging]</ref><ref name=popmech>[https://books.google.com/books?id=e9wDAAAAMBAJ&pg=PA828&lpg=PA828&dq=radio+acoustic+ranging&source=bl&ots=EV8p-JgNc-&sig=nQRMebdKPf1B0VQ0ACD69GsrlGM&hl=en&sa=X&ved=0ahUKEwiQ3Ju7y83LAhWDs4MKHSCEDjQ4ChDoAQgbMAA#v=onepage&q=radio%20acoustic%20ranging&f=false Anonymous, "Ocean's Depth Measured By Radio Robot," ''Popular Mechanics'', December 1938, pp. 828-830.]</ref>

The damage inflicted upon the Allies by German [[U-boat]]s during World War II made the need for sonar a priority. With millions of tons of shipping being sunk in the Atlantic,<ref>{{cite book |title= The Right of the Line: The Royal Air Force in the European War, 1939–1945|last= Terraine|first= John|authorlink= John Terraine|year= 1985|publisher= [[Hodder & Stoughton]]|location= London|isbn= 978-0-340-26644-1|oclc= 13125337}}</ref> there was a need to locate submarines so that they could be sunk or prevented from attacking. Sonar was installed on a number of ships along with [[tadar]] and [[high-frequency direction finding]] ("Huff-Duff") to detect surfaced submarines. While sonar was a primitive system, it was constantly improved. 

{{main|Battle of the Atlantic}}

Modern anti-submarine warfare methods evolved from the techniques devised for the movement of convoys and battle groups through hostile waters during World War II . It was imperative that submarines be detected and neutralized long before the task group came within range of an attack. Aircraft-based submarine detection was the obvious solution.  The maturity of radio communication and sonar technology made it possible to combine a sonar transducer, batteries, a radio transmitter and whip antenna, within a self-contained air-deployed floating (sono)buoy.  

Early sonobuoys had limited range, limited battery life and were overwhelmed by the noise of the ocean. They first appeared during world war II, in which they first were used in July 1942 by [[RAF_Coastal_Command_during_World_War_II#Sensors|RAF Coastal Command]] under the code name 'High Tea', the first squadron to use them operationally being [[No. 210 Squadron RAF]], operating [[Short Sunderland|Sunderlands]].  They were also limited by the use of human ears to discriminate man-made noises from the oceanic background. However, they demonstrated that the technology was viable. With the development of better hydrophones, the [[transistor]] and miniaturization, and the realization that very low frequency [[Infrasound|sound]] was important, more effective acoustic sensors followed. The sonobuoy went from being an imposing six feet tall, two feet diameter sensor to the compact suite of electronics it is today.

The advancement in sonobuoy technology aided the development of aircraft such as the [[P-2 Neptune]], [[S-2 Tracker]], [[S-3 Viking|S-3B Viking]] and [[P-3 Orion]] for anti-submarine warfare.

==Operation==
[[Image:Sonobuoy.JPG|thumb|right|Sonobuoy deployment procedures after impacting water.]]

Sonobuoys are classified into three categories: active, passive and special purpose. 

*'''Active sonobuoys''' emit sound energy (pings) into the water and listen for the returning echo before transmitting information&mdash;usually range and bearing&mdash;via UHF/VHF radio to a receiving ship or aircraft. The original active sonobuoys pinged continuously after deployment for a predetermined period of time. Later, Command Activated Sonobuoy System (CASS) sonobuoys allowed the aircraft to trigger pings (or buoy scuttling) via a radio link. This evolved into DICASS (Directional CASS) in which the return echo contained bearing as well as range data.
*'''Passive sonobuoys''' emit nothing into the water, but rather listen, waiting for sound waves (for instance, power plant, propeller or door-closing and other noises) from ships or submarines, or other acoustic signals of interest such as an aircraft's black box pinger, to reach the hydrophone. The sound is then transmitted via UHF/VHF radio to a receiving ship or aircraft.
*'''Special purpose sonobuoys''' relay various types of oceanographic data to a ship, aircraft, or satellite.  There are three types of special-purpose sonobuoys in use today.  These sonobuoys are not designed for use in submarine detection or localization.
**BT&mdash;The bathythermobuoy (BT) relay [[bathythermograph]]ic and/or [[salinity]] readings at various depths.
**SAR&mdash;The [[search and rescue]] (SAR) buoy is designed to operate as a floating [[Radio frequency]] beacon. As such, it is used to assist in marking the location of an aircraft crash site, a sunken ship, or survivors at sea.
**ATAC/DLC&mdash;Air transportable communication (ATAC) and down-link communication (DLC) buoys, such as the [[Underwater telephone|UQC]], or "gertrude", are intended for use as a means of communication between an aircraft and a submarine, or between a ship and a submarine.

This information is analyzed by computers, acoustic operators and [[TACCO]]s to interpret the sonobuoy information. 

Active and/or passive sonobuoys may be laid in large fields or barriers for initial detection. Active buoys may then be used for precise location. Passive buoys may also be deployed on the surface in patterns to allow relatively precise location by [[triangulation]]. Multiple aircraft or ships monitor the pattern either passively listening or actively transmitting to drive the submarine into the sonar net.  Sometimes the pattern takes the shape of a grid or other array formation and complex [[beamforming]] [[signal processing]] is used to transcend the capabilities of single, or limited numbers of, hydrophones.

==References==
{{Reflist}}

==External links==
{{Commons category|Sonobuoys}}
*[http://www.sonobuoytechsystems.com/products.htm Sparton Sonobuoys]
*[http://www.ultra-ms.com/ Ultra Electronics - Maritime Systems ]
*[http://www.ultra-ussi.com/ Ultra Electronics - USSI ]
*[http://www.ultra-sonar.com/ Ultra Electronics - Sonar Systems]
*[https://www.ultra-fei.com/index.php?page=receivers Ultra Electronics - Flightline Systems]
*[http://www.sonobuoytechsystems.com/ Sonobuoy TechSystems ]
*[http://www.fas.org/programs/ssp/man/uswpns/air/asw/air_antisubwar.html Early sonobuoy development via NDRC]
*[http://www.maritime.org/doc/sonar/chap16.htm#pg291 WWII Radio sonobuoy in Naval Sonar Ch. 16]
*[http://www.maritime.org/doc/sonar/chap16.htm#pg298 AN/CRT-1A Sonobuoy]
*[http://www.maritime.org/doc/sonar/fig16-10.htm Illustration of AN/CRT-1A Sonobuoy]
*[http://www.flightglobal.com/pdfarchive/view/1977/1977%20-%203595.html ''Mini-Jezebel - A giant reduction in Sonobuoy size''] - a 1977 ''[[Flight International]]'' advertisement for the Mini-Jezebel sonobuoy

{{hydroacoustics}}

[[Category:Anti-submarine warfare]]
[[Category:Sonar]]