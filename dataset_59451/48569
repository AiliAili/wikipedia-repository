{{Infobox writer <!-- for more information see [[:Template:Infobox writer/doc]] -->
| name         = Dennis Hopeless
| image        = Dennis Hopeless C2E2 2012.jpg
| imagesize    = 
| caption      = Hopeless at the 2012 [[Chicago Comic & Entertainment Expo]]
| pseudonym    = 
| birth_date   = 
| birth_place  = [[Kansas City, Missouri]]
| occupation   = Comic Book Writer
| nationality  = American
| period       = 2007–present
| genre        =
| notableworks = ''LoveSICK''<br />''X-Men: Season One''<br />''[[Avengers Arena]]''<br />''[[Spider-Woman (Jessica Drew)|Spider-Woman Vols. 5 & 6]]''<br />''[[Cable and X-Force]]''
| movement     = 
| influences   = 
| influenced   = 
| signature    = 
| website      = {{URL|http://dennishopeless.com/}}
}}

'''Dennis Hopeless''' is an American [[comics writer]] from Kansas City, Missouri who has written for [[Marvel Comics]], [[Image Comics]], [[Dark Horse Comics]], [[Boom! Studios]], [[Arcana Studio]], and [[Oni Press]].<ref>{{cite web|url=http://comicbookdb.com/creator_chron.php?ID=11940|website=Comic Book DB|title=Dennis Hopeless|accessdate=April 26, 2016}}</ref>

Hopeless has written multiple series starring teenage [[superhero]]es and has said that he "tend[s] to write about the challenge of growing up."<ref>{{cite web|url=http://www.newsarama.com/8209-x-men-season-one-aims-for-new-readers-with-old-characters.html|website=Newsarama.com|title=X-MEN: SEASON ONE Aims for New Readers with Old Characters|accessdate=April 26, 2016}}</ref> He's been praised by critics for including a female point-of-view in his comics.<ref>{{cite web|url=http://popcultureblog.dallasnews.com/2012/09/comics-who-is-dennis-hopeless.html/|website=Dallas Morning News|title=Comics: Who Is Dennis Hopeless?|accessdate=April 26, 2016}}</ref> After finding success with two [[Creator ownership|creator-owned]] comics, Hopeless began writing for Marvel in 2011. In 2015, Hopeless wrote two limited series as part of Marvel's ''[[Secret Wars (2015 comic book)|Secret Wars]]'' event. The following year, Hopeless began scripting Marvel's relaunched ongoing series [[All-New X-Men]]. That same year, he resumed his writing duties on Marvel's [[Spider-Woman]] comic, the sixth volume of the character's ongoing series. The series earned Hopeless some of the best reviews of his career, as he and his creative team received praise from feminist critics for showing the realities of motherhood.<ref>{{cite web|url=https://www.themarysue.com/spider-woman-no-4-interview/|website=The Mary Sue|title=Marvel’s Dennis Hopeless and Nick Lowe Talk Writing a Single Mom Superhero in Spider-Woman|accessdate=April 10, 2017}}</ref> In 2017, Marvel began publishing a [[Jean Grey]] ongoing series, the character's first, with Hopeless and artist Victor Ibanez at the helm.<ref>{{cite web|url=http://comicsalliance.com/jean-grey-hopeless-ibanez/|website=Comics Alliance|title=‘Jean Grey’ Is On The Way From Dennis Hopeless And Victor Ibanez|accessdate=April 10, 2017}}</ref>

==Career==
Dennis Hopeless worked in a comic store in the mid-2000s while trying to break into the [[comics]] industry.<ref>{{cite web|url=http://herocomplex.latimes.com/comics/dennis-hopeless-introduces-house-of-m-to-secret-wars-battleworld/|website=Hero Complex|title=Dennis Hopeless introduces ‘House of M’ to Secret Wars’ Battleworld|accessdate=April 26, 2016}}</ref> In 2007, he created ''GearHead'' (with penciller Kevin Mellon), a four-issue comic about a female auto mechanic searching for her lost brother. The series was published by [[Arcana Studio]].<ref>{{cite web|url=http://comicvine.gamespot.com/gearhead-1/4000-106809/|website=Comic Vine|title=Gearhead #1 (Issue)|accessdate=April 26, 2016}}</ref> His next notable work didn't hit shelves until 2011 when Hopeless reunited with artist Kevin Mellon to create ''LoveSTRUCK'', a supernatural [[graphic novel]] inspired in part by [[Frank Herbert|Frank Herbert's]] ''[[Dune (novel)|Dune]]'' and [[Garth Ennis|Garth Ennis's]] ''[[Preacher (comics)|Preacher]]'' that was published by [[Image Comics]].<ref>{{cite web|url=http://www.westfieldcomics.com/blog/tag/dennis-hopeless/|website=Westfield Comics Blog|title=Interview: Dennis Hopeless & Kevin Mellon on Image's LoveStruck|accessdate=April 26, 2016}}</ref><ref>{{cite web|url=http://www.comicbookresources.com/?page=article&id=33949|website=Comic Book Resources|title=Hopeless Hits the Bullseye with LoveStruck|accessdate=April 26, 2016}}</ref> That same year, Hopeless began working with [[Marvel Comics]], writing the second volume of [[Legion of Monsters]] (with penciler Juan Doe). The series starred the titular Legion and monster-hunter [[Elsa Bloodstone]].<ref>{{cite web|url=http://www.comicbookresources.com/?page=user_review&id=4008|website=Comic Book Resources|title=Review: Legion of Monsters #1|accessdate=April 26, 2016}}</ref>

While ''Legion of Monsters'' was still being published, Marvel hired Hopeless (along with pencillers [[Jamie McKelvie]] and [[Mike Norton]]) to create a graphic novel called ''X-Men: Season One'' as part of a series of four graphic novels focusing on the origins of some of the company's biggest characters.<ref>{{cite web|url=http://comicvine.gamespot.com/articles/why-marvels-season-one-graphic-novels-may-not-be-s/1100-143554/|website=Comic Vine|title=Why Marvel's 'Season One' Graphic Novels May Not Be Such A Great Idea|accessdate=April 26, 2016}}</ref> While some publications were hesitant to embrace the ''Season One'' concept, Hopeless's novel garnered generally positive reviews,<ref>{{cite web|url=http://www.multiversitycomics.com/reviews/review-x-men-season-one/|website=Comic Vine|title=Review: X-Men: Season One|accessdate=April 26, 2016}}</ref><ref>{{cite web|url=http://www.teenreads.com/reviews/x-men-season-one|website=Teenreads|title=X-Men: Season One|accessdate=April 26, 2016}}</ref> with [[Comic Book Resources]] calling it, "easy to understand, fun to read and still pretty wide in scope."<ref>{{cite web|url=http://www.comicbookresources.com/?page=user_review&id=4556|website=Comic Book Resources|title=Review: X-Men: Season One|accessdate=April 26, 2016}}</ref>

Hopeless's next work for Marvel was 2013's ''[[Avengers Arena]]'', an 18-issue comic series in which the villain [[Arcade (Marvel Comics)|Arcade]] kidnaps 16 teenage superheroes and forces them to fight each other to the death.<ref>{{cite web|url=http://comicvine.gamespot.com/avengers-arena/4050-54622/|website=Comic Vine|title=Review: X-Men: Season One|accessdate=April 26, 2016}}</ref> The series starred characters from [[Avengers Academy]], the [[Runaways (comics)|Runaways]] and Hopeless's newly created Braddock Academy and featured covers referencing ''[[Lord of the Flies]]'', ''[[The Hunger Games (film series)|The Hunger Games]]'' series, the game show ''[[Survivor (U.S. TV series)|Survivor]]'', and the Japanese film ''[[Battle Royale (film)|Battle Royale]]''. The comic won Hopeless the 2013 [[Harvey Awards|Harvey Award]] for Most Promising New Talent. That same year, Hopeless wrote ''[[Cable and X-Force]]'' (with artist [[Salvador Larroca]]), a 19-issue series that ran concurrently and eventually crossed over with [[Sam Humphries]] and [[Ron Garney]]'s ''[[Uncanny X-Force|Uncanny X-Force Vol. 2]]''.<ref>{{cite web|url=http://comicvine.gamespot.com/cable-and-x-force/4050-54644/|website=Comic Vine|title=Cable and X-Force|accessdate=April 26, 2016}}</ref> Also in 2013, Hopeless co-wrote a 4-issue series called ''The Answer!'' with [[Eisner Award]] winning creator [[Mike Nolan]] for [[Dark Horse Comics]].<ref>{{cite web|url=http://www.comicbookresources.com/?page=user_review&id=5594|website=Comic Book Resources|title=Review: The Answer #1|accessdate=April 26, 2016}}</ref> In 2014, Hopeless and artist Kevin Walker authored ''[[Avengers Undercover]]'', a direct follow-up series to ''[[Avengers Arena]]'' with many of the same characters.<ref>{{cite web|url=http://comicvine.gamespot.com/avengers-undercover/4050-72271/|website=Comic Vine|title=Avengers Undercover|accessdate=April 26, 2016}}</ref>

Hopeless's next project was the limited series ''All-New Captain America: Fear Him'' (with co-writer [[Rick Remender]]). Part of Marvel's [[Infinite Comics]] series, it starred [[Falcon (comics)|Sam Wilson]] in his new role as [[List of incarnations of Captain America|Captain America]].<ref>{{cite web|url=http://comicvine.gamespot.com/allnew-captain-america-fear-him-infinite-comic/4050-77871/|website=Comic Vine|title=All-New Captain America: Fear Him|accessdate=April 26, 2016}}</ref> The following year, Hopeless began working on the fifth volume of Marvel's ''[[Spider-Woman (Jessica Drew)|Spider-Woman]]'' comic with penciler [[Greg Land]] and ''Big Thunder Mountain Railroad'' (with artist Tigh Walker), an all-ages western adventure comic based on the classic [[The Walt Disney Company|Disney]] theme park [[Big Thunder Mountain Railroad|attraction]].<ref>{{cite web|url=http://comicvine.gamespot.com/spider-woman-1/4000-470434/|website=Comic Vine|title=Spider-Woman #1 (Issue)|accessdate=April 27, 2016}}</ref><ref>{{cite web|url=http://comicvine.gamespot.com/big-thunder-mountain-railroad-1/4000-483343/|website=Comic Vine|title=Big Thunder Mountain Railroad #1 (Issue)|accessdate=April 27, 2016}}</ref> That summer, Marvel began their ''[[Secret Wars (2015 comic book)|Secret Wars]]'' crossover event and Hopeless wrote two books in the storyline, ''[[Inferno (Marvel Comics)|Inferno]]'' (with Javier Garron) and ''[[House of M]]'' (with Mark Failla), both based on both based on previous Marvel events.<ref>{{cite web|url=http://comicvine.gamespot.com/inferno-1/4000-489901/|website=Comic Vine|title=Inferno #1 (Issue)|accessdate=April 27, 2016}}</ref><ref>{{cite web|url=http://comicvine.gamespot.com/house-of-m-1/4000-497944/|website=Comic Vine|title=House of M #1 (Issue)|accessdate=April 27, 2016}}</ref>

Upon the conclusion of the Secret Wars event, Marvel relaunched their ''[[Spider-Woman (Jessica Drew)|Spider-Woman]]'' comic in January 2016 with Hopeless now joined by artist [[Javier Rodríguez (artist)|Javier Rodríguez]] who had worked with Hopeless on the previous volume after Land's departure.<ref>{{cite web|url=http://comicvine.gamespot.com/spider-woman-1/4000-506182/|website=Comic Vine|title=Spider-Woman #1 (Issue)|accessdate=April 27, 2016}}</ref> The relaunched series centered on Spider-Woman, Jessica Drew's newly announced pregnancy and impending motherhood. Critics praised the relatable, fun storytelling of the comic with IGN calling it, "laid back at times and outlandish at others" while scoring it an 8.6 out of 10.<ref>{{cite web|url=http://www.ign.com/articles/2015/11/16/spider-woman-1-review-3|website=IGN|title=Spider-Woman #1 Review|accessdate=April 27, 2016}}</ref> The following month, Hopeless served as writer for another relaunched series, the second volume of [[All-New X-Men]], a comic starring the time-displaced original [[X-Men]] now traveling the country with three young [[X-Mansion|Jean Grey School]] students.<ref>{{cite web|url=http://comicvine.gamespot.com/all-new-x-men-1/4000-507175/|website=Comic Vine|title=All-New X-Men #1|accessdate=April 27, 2016}}</ref>

In late 2016, Hopeless wrote the main story in a [[One-shot (comics)|one-shot]] licensed comic called "WWE: Then, Now, Forever", which was published by [[Boom! Studios]] with Dan Mora  providing the art.<ref>{{cite web|url=http://comicvine.gamespot.com/wwe-then-now-forever-1/4000-557407/|website=Comic Vine|title=WWE: Then. Now. Forever. #1|accessdate=January 23, 2017}}</ref> The comic became a ongoing series simply called [[WWE]] the following year with Hopeless continuing to write the main story and [[Daniel Acuña]] replacing Mora on pencils.<ref>{{cite web|url=http://comicvine.gamespot.com/wwe-1-untitled-the-new-days-optimistic-odyssey-par/4000-576683/|website=Comic Vine|title=WWE #1|accessdate=January 23, 2017}}</ref>

Both the All-New X-Men and Spider-Woman ongoing series came to an end in May of 2017 with both series receiving fairly positive reviews throughout their run.<ref>{{cite web|url=http://comicbookroundup.com/comic-books/writer-artist/dennis-hopeless/|website=Comic Book Roundup|title=Dennis Hopeless Comic Reviews|accessdate=April 10, 2017}}</ref> The final issue of Spider-Woman, in particular, garnered a great deal of positive sentiment, with [[The A.V. Club]]'s Oliver Sava calling the series "one of Marvel’s most consistently entertaining, fun-loving titles" in his review of the issue.<ref>{{cite web|url=http://comicbookroundup.com/comic-books/reviews/marvel-comics/spider-woman-(2015)/17|website=Comic Book Roundup|title=Spider-Woman #17|accessdate=April 10, 2017}}</ref><ref>{{cite web|url=http://www.avclub.com/live/marvel-just-doesnt-know-what-do-its-group-young-ho-252885/entry/326|website=The A.V. Club|title=Spider-Woman thrives thanks to its superheroine’s relatable, everyday problems|accessdate=April 10, 2017}}</ref> Continuing his work at Marvel, Hopeless was tapped to take over writing duties for the [[Doctor Strange]] ongoing beginning with issue #21 as well as scripting a new [[Jean Grey]] series, the character's first ongoing solo book and part of Marvel's ResurrXion revamp.<ref>{{cite web|url=http://www.cbr.com/doctor-strange-jason-aaron-chris-bachalo-dennis-hopeless-niko-henrichon-marvel-may-solicitation/|website=Comic Book Resources|title=Hopeless & Henrichon Take Over Doctor Strange From Aaron & Bachalo|accessdate=April 10, 2017}}</ref><ref>{{cite web|url=http://www.newsarama.com/32188-jean-grey-s-first-ever-ongoing-is-coming-and-so-is-the-phoenix.html|website=Newsarama|title=JEAN GREY's First-Ever Ongoing Is Coming (And So Is THE PHOENIX)|accessdate=April 10, 2017}}</ref> Hopeless's first Dr. Strange issue and the first issue of the Jean Grey series will both have a cover date of July, 2017.

==Personal==
Dennis married wife Jessie Hopeless in 2004 when both were 23.<ref>{{cite web|url=http://www.cbr.com/hopeless-talks-spider-woman-and-balancing-crime-fighting-with-motherhood/|website=Comic Book Resources|title=Hopeless Talks “Spider-Woman” and Balancing Crime Fighting with Motherhood|accessdate=April 10, 2017}}</ref> Jessie is a tattoo artist for Exile Tattoo in Kansas City, Missouri where the couple reside.<ref>{{cite web|url=http://www.exiletattoo.net/jessiegallery.html|website=Exile Tattoo|title=Artwork and Tattoos by Jessie Hopeless|accessdate=April 10, 2017}}</ref> In 2014, Jessie gave birth to twin boys, an event which Hopeless credits with inspiring the plot of his run on [[Spider-Woman]]. The couple collaborated professionally in 2016, scripting the tenth issue of the ongoing [[Invader_Zim#Comic_book_series|Invader Zim]] comic for [[Oni Press]]. The issue received relatively positive reviews with IGN's Jesse Schedeen praising the comic's adherence to the look and feel of the original cartoon series.<ref>{{cite web|url=http://comicbookroundup.com/comic-books/reviews/oni-press/invader-zim/10|website=Comic Book Roundup|title=Invader Zim #10|accessdate=April 10, 2017}}</ref><ref>{{cite web|url=http://www.ign.com/articles/2016/06/09/comic-book-reviews-for-june-8-2016?page=3|website=IGM|title=Comic Book Reviews For June 8, 2016|accessdate=April 10, 2017}}</ref>

==Awards==
*2013 - [[Harvey Award]]—Most Promising New Talent (for [[Avengers Arena]])<ref>{{cite web|url=http://www.harveyawards.org/previous-awards-nominees/2013-harvey-awards/|website= The Harvey Awards|title=2013 Harvey Awards|accessdate=April 27, 2016}}</ref>
*2016 - [[GLAAD Media Award]]—Outstanding Comic Book (Nominee) (for [[All-New X-Men]])<ref>{{cite web|url=https://www.glaad.org/mediaawards/nominees?field_nominee_language_value=english&field_nominee_category_tid=39779|website= GLAAD|title=And the #glaadawards nominees are...|accessdate=April 10, 2017}}</ref>


==Bibliography==

=== Arcana Studio ===

*''GearHead'' (limited series) (April 2007 — May 2008) (with Kevin Mellon, Ed Herrera, Alexey Strakhov and Jessie Hopeless)
**''GearHead'' (tpb, 120 pages, 2008, ISBN 978-0976309598) collects: ''GearHead'' 1-4

=== Image Comics ===

*''[[Hack/Slash|Hack/Slash: Trailers #2]]'', story: "Too Cute" (with Kyle R. Strahm, Buster Moody and Chris Crank, November 2010)
*''[[LoveSTRUCK]]'' (graphic novel) (with Kevin Mellon and Erik Jones, October 2011, ISBN 978-1607064473)

=== Dark Horse Comics ===

*''The Answer!'' (limited series) (January 2013 — April 2013) (co-written with [[Mike Norton]])
**''The Answer! Volume 1'' (tpb, 120 pages, 2013, ISBN 978-1616551971) collects: ''The Answer!'' 1-4

=== Marvel Comics ===

*''[[Legion of Monsters]]'' vol. 2 #1-4 (with Juan Doe and Will Quintana, December 2011 — March 2012)
**''Legion of Monsters'' (tpb, 96 pages, April 2012, ISBN 978-0785140573) collects: ''Legion of Monsters'' 1-4
*''X-Men: Season One'' (graphic novel) (with [[Jamie McKelvie]], [[Mike Norton]], [[Matt Wilson (comics artist)|Matt Wilson]] and Clayton Cowles, May 2012, ISBN 978-0785156451)
*''[[Marvel NOW!|Marvel NOW! Point One]]'' #1 story "Forge in: Crazy Enough" (with Gabriel Hernandez Walta, David Curiel and Joe Sabino, December 2012)
*''[[Avengers Arena]]'' #1-18 (with [[Kev Walker]], Alessandro Vitti, [[Riccardo Burchielli]], Frank Martin Jr. and Jean-Francois Beaulieu, February 2013 — N2014), Collected in:
**''Volume 1: Kill or Die'' (tpb, 144 pages, May 2013, ISBN 978-0785166573), collects: ''Avengers Arena'' 1-6
**''Volume 2: Game On'' (tpb, 144 pages, September 2013, ISBN 978-0785166580), collects: ''Avengers Arena'' 7-12
**''Volume 3: Boss Level'' (tpb, 136 pages, February 2014, ISBN 978-0785189282), collects: ''Avengers Arena'' 13-18
*''[[Cable and X-Force]]'' #1-19 (with [[Salvador Larroca]], Gerardo Sandoval, [[Cullen Bunn]], Frank G. D'Armata, Angel Unzueta and Rachelle Rosenberg, February 2013 — March 2014), Collected in:
**''Volume 1: Wanted'' (tpb, 136 pages, May 2013, ISBN 978-0785166900), collects: ''Cable and X-Force'' 1-5
**''Volume 2: Dead or Alive'' (tpb, 96 pages, November 2013, ISBN 978-0785166917), collects: ''Cable and X-Force'' 6-9
**''Volume 3: This Won't End Well'' (tpb, 112 pages, February 2014, ISBN 978-0785188827), collects: ''Cable and X-Force'' 10-14
**''Volume 4: This Won't End Well'' (tpb, 152 pages, April 2014, ISBN 978-0785189466), collects: ''Cable and X-Force'' 15-19, ''[[Uncanny X-Force]]'' 16-17
*''[[Avengers Undercover]]'' #1-10 (with [[Kev Walker]], [[Timothy Green II]], Tigh Walker and Jean-Francois Beaulieu, May 2014 — November 2014), Collected in:
**''Volume 1: Descent'' (tpb, 112 pages, September 2014, ISBN 978-0785189404), collects: ''Avengers Undercover'' 1-5
**''Volume 2: Going Native'' (tpb, 112 pages, November 2013, ISBN 978-0785189411), collects: ''Avengers Undercover'' 6-10
*''All-New Captain America: Fear Him'' [[Infinite Comics|Infinite Comic]] #1-6 (with [[Rick Remender]], Mast and Geoffo, October 2014 — November 2014), Collected in:
**''Volume 1: Descent'' (tpb, 136 pages, June 2015, ISBN 978-0785192589)
*''[[Spider-Woman (Jessica Drew)|Spider-Woman Vol. 5]]'' #1-9 (with [[Greg Land]], Javier Rodriguez, W. Scott Forbes, Jay Leisten, Natacha Bustos and Muntsa Vicente, May 2014 — November 2014), Collected in:
**''Volume 1: Spider-Verse'' (tpb, 112 pages, June 2015, ISBN 978-0785154587), collects: ''Spider-Woman Vol. 5'' 1-4
**''Volume 2: New Duds'' (tpb, 136 pages, February 2016, ISBN 978-0785154594), collects: ''Spider-Woman Vol. 5'' 5-10
*''[[Big Thunder Mountain Railroad]]'' #1-5 (with Tigh Walker, Felix Ruiz, Guillermo Mogorron, and Jean-Francois Beaulieu, May 2015 — October 205), Collected in:
**''Big Thunder Mountain Railroad'' (hc, 128 pages, October 2015, ISBN 978-0785197010), collects: ''Big Thunder Mountain Railroad'' 1-5
*''[[Inferno (Marvel Comics)#Secret Wars|Inferno]]'' #1-5 (with Javier Garron, Chris Sotomayor and Joe Sabino, July 2015 — November 2015), Collected in:
**''Inferno: Warzones'' (tpb, 112 pages, December 2015, ISBN 978-0785198734), collects: ''Inferno'' 1-5
*''[[House of M#Later miniseries|House of M]]'' #1-4 (with Javier Garron, Chris Sotomayor and Joe Sabino, October 2015 — December 2015), Collected in:
**''House of M: Warzones'' (tpb, 120 pages, February 2016, ISBN 978-0785198727), collects: ''House of M'' 1-4, ''House of M (2005)'' 1
*''[[The Amazing Spider-Man|Amazing Spider-Man Vol. 4]]'' #1 story "What to Expect" (with Javier Rodriguez and Álvaro López, December 2015), Collected in:
**'' Vol. 1: Worldwide'' (tpb, 144 pages, April 2016, ISBN 978-0785199427), collects: ''Amazing Spider-Man Vol. 4'' 1-5
*''[[Spider-Woman (Jessica Drew)|Spider-Woman Vol. 6]]'' #1-17 (with Javier Rodriguez, [[Veronica Fish]], [[Joëlle Jones]], Álvaro López and Rachelle Rosenberg, January 2016 — March 2017), Collected in:
**''Shifting Gears Volume 1: Baby Talk'' (tpb, 120 pages, June 2016, ISBN 978-0785196228), collects: ''Spider-Woman Vol. 6'' 1-5
**''Spider-Women'' (tpb, 136 pages, July 2016, ISBN 978-1302900939), collects: ''Spider-Woman Vol. 6'' 6-7, ''[[Silk (comics)|Silk]]'' 7-8, ''[[Spider-Gwen]]'' 7-8
**''Shifting Gears Volume 2: Civil War II'' (tpb, 112 pages, December 2016, ISBN 978-0785196235), collects: ''Spider-Woman Vol.6'' 8-12
**''Shifting Gears Vol. 3: Scare Tactics'' (tpb, 112 pages, June 2017, ISBN 978-1302903305), collects: ''Spider-Woman Vol.6'' 13-17
*''[[All-New X-Men|All-New X-Men Vol. 2]]'' #1-19 (with [[Mark Bagley]], Paco Diaz, Nolan Woodard, Andrew Hennessy and Rachelle Rosenberg, February 2016 — March 2017), Collected in:
**''Inevitable Vol. 1: Ghost of the Cyclops'' (tpb, 136 pages, May 2016, ISBN 978-0785196303), collects: ''All-New X-Men'' 1-6
**''Inevitable Vol. 2: Apocalypse Wars'' (tpb, 136 pages, October 2016, ISBN 978-0785196310), collects: ''All-New X-Men'' 7-12
**''Inevitable Vol. 3: Hell Hath So Much Fury'' (tpb, 112 pages, January 2016, ISBN 978-1302902919), collects: ''All-New X-Men'' 13-16
**''Inevitable Vol. 4: IvX'' (tpb, 112 pages, September 2017, ISBN 978-1302903305), collects: ''All-New X-Men'' 17-19, Annual 1

==References==
{{reflist}}

{{Authority control}}

{{DEFAULTSORT:Hopeless, Dennis}}
[[Category:Living people]]
[[Category:Year of birth missing (living people)]]
[[Category:American comics writers]]