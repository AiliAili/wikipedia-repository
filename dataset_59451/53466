{{Infobox company
|  name   = Crowe Horwath LLP
|  logo   = Crowe Horwath logo.svg
|  type   = [[Limited Liability Partnership]]
|  key_people     = Jim Powers, [[Chief executive officer|CEO]]
|  industry       = [[Professional Services]]
|  services       = [[Audit]]<br />[[Tax]]<br />Advisory<br />Risk<br />Performance
|  revenue        = {{profit}}[[United States dollar|US$]]686.6 million (2014)
|  num_employees  = 3,000 (April 2015)
|  homepage       = [http://www.crowehorwath.com/ www.crowehorwath.com]
|  foundation     = [[South Bend]], [[Indiana]], [[United States|U.S.]] (1942)
|  location       = [[Oak Brook, Illinois|Oak Brook]], [[Illinois]], [[United States|U.S.]]
}}

'''Crowe Horwath LLP ''' is one of the largest public accounting,{{Dead link|date=March 2017}} consulting, and technology firms in the U.S. Crowe is an independent member of [[Crowe Horwath International]].

==History==
===Founding===
Crowe Chizek, was established in 1942 in South Bend, Ind., by Fred P. Crowe Sr. and Cletus F. Chizek. Previously, Crowe had worked in public accounting for many years and also served as the St. Joseph County auditor for eight years. Chizek was head of the accounting department at the University of Notre Dame and also worked part-time in public accounting. Following the death of Fred P.Crowe Sr. in 1952, Cletus F. Chizek reorganized the firm as Crowe, Chizek and Company and two firm personnel, M. Mendel Piser and Fred P. Crowe Jr., became partners. The first legal partnership of Crowe Chizek and Company was formulated in 1962 with six founding partners: Cletus F. Chizek, M. Mendel Piser, Fred P. Crowe Jr., Robert W. Green, Joseph A. Bauters and John J. Pairitz.<ref>{{cite web|url=http://www.accountingtoday.com/news/2722-1.html |title=AccountingToday |publisher=AccountingToday }}</ref>

===Name change to Crowe Horwath===
The firm changed its name to Crowe Horwath in 2008 to strengthen its alignment with Horwath International and in 2009 Horwath International changed its name to Crowe Horwath International. The new name combined the international awareness of the Horwath brand with the United States awareness of Crowe Horwath LLP (Crowe), the network’s largest member firm in the U.S.

Historically, Crowe is one of the fastest growing firms with an average annual growth rate of 17 percent over the past 40 years.<ref>{{cite web|url=http://www.crowehorwath.com/news-room/firm-history.aspx |title=Crowe Firm History|publisher=Crowe Horwath|date=May 2013}}</ref>

==Services==
[[Audit]], [[Tax]], Advisory, Risk, and Performance.<ref>{{Cite web|title = About: Crowe Horwath LLP|url = http://www.crowehorwath.com/about/|website = |accessdate = 2015-09-27|publisher = Crowe Horwath}}</ref>

==Industry Recognition==
*The 2015 Accounting Today Top 100 Ranks Crowe in the Top 10 in the U.S.{{Dead link|date=March 2017}}
*2015 PAR Top 100 Ranks Crowe in the Top 10 in the U.S. <ref>{{cite web|url=http://insidepublicaccounting.com/wp-content/uploads/2015/07/IPA-100_2015_WEB.pdf|title=2015 PAR Top 100 Ranks Crowe in the Top 10 in the U.S.}}</ref>
*2015 101 National Best and Brightest Companies to Work For<ref>{{cite web|url=http://101bestandbrightest.com/events/2016-best-and-brightest-companies-to-work-for-in-the-nation/winners/?winyear=121|title=2015 101 National Best and Brightest Companies to Work For}}</ref>
*2016 Best Places to Work in Kentucky<ref>{{cite web|url=http://www.bestplacestoworkky.com/UserFiles/File/2016/2016%20BPWK%20-%20Winners%20Listing_Web.pdf|title=2016 Best Places to Work in Kentucky}}</ref>
*2016 Best Places to Work in Vermont<ref>{{cite web|url=http://events.vermontbiz.com/best-places-to-work/|title=2016 Best Places to Work in Vermont}}</ref>
*2015 Best Places to Work in New Jersey<ref>{{cite web|url=http://www.njbiz.com/article/20150501/NJBIZ01/150509985/njbiz-ranks-best-places-to-work-in-nj-for-2015| title=NJBIZ }}</ref>
*2015 West Michigan 101 Best and Brightest Companies to Work For<ref>{{cite web|url=http://101bestandbrightest.com/events/west-michigans-2015-101-best-and-brightest-companies-to-work-for/winners/?winyear=58| title=Best and Brightest }}</ref>
*2015 Ohio's Best Employers<ref>{{cite web|url=http://www.bestemployersoh.com/index.php?option=com_content&task=view&id=54| title=Best Employers in Ohio }}</ref>
*2015 Atlanta's Best and Brightest Companies to Work For<ref>{{cite web|url=http://101bestandbrightest.com/events/atlantas-2015-best-and-brightest-companies-to-work-for/winners/| title=Best and Brightest}}</ref>
*2015 Best Places to Work in Illinois<ref>{{cite web|url=http://www.bestplacestoworkil.com/index.php?option=com_content&task=view&id=55| title=Best Places to Work in Illinois}}</ref>
*2014 Best companies to work for in Texas<ref>{{cite web|url=http://www.bestcompaniestx.com/index.php?option=com_content&task=view&id=48&Itemid=1| title=Best Companies to Work for in Texas }}</ref>

==References==
<references/>

==External links==
* [http://www.crowehorwath.com/ Crowe Horwath] - Main site
* [http://www.crowehorwathinternational.com Crowe Horwath International] - The [[Swiss Verein]] of which Crowe is part.
* {{OpenCorp|Crowe Horwath}}

[[Category:Accounting firms of the United States]]