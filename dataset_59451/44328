{{EngvarB|date=May 2014}}
{{Infobox airport
| name        = Sardar Vallabhbhai Patel International Airport<br>સરદાર વલ્લભભાઈ પટેલ આંતરરાષ્ટ્રીય એરપોર્ટ<br>सरदार वल्लभभाई पटेल अंतरराष्ट्रीय हवाई अड्डा
| image       = Terminal 2 from inside.jpg
| image-width = 250
| caption     = Interior of Terminal 2
| IATA        = AMD
| ICAO        = VAAH
| type        = Public
| owner-oper  = [[Airports Authority of India]]
| city-served =
<div>
* [[Ahmedabad]]
* [[Gandhinagar]]
</div>
| location    = [[Ahmedabad]], [[Gujarat]], India
| hub = 
| elevation-f = 189
| elevation-m = 58
| coordinates = {{Coord|23|04|38|N|072|38|05|E|type:airport|display=inline,title}}
| pushpin_map = India Gujarat#India
| pushpin_label = '''AMD'''
| website     = [http://www.aai.aero/allAirports/ahmedabad_generalinfo.jsp Official website]
| r1-number   = 05/23
| r1-length-f = 11,811
| r1-length-m = 3,599
| r1-surface  = Concrete/[[Asphalt]]
|stat-year= 2016
|stat1-header= Passenger movements
|stat1-data= 7,229,118
|stat2-header= Aircraft movements
|stat2-data= 49,776
|stat3-header= Cargo tonnage
|stat3-data= 72,402
| footnotes = Source: [[Airport Authority of India|AAI]]<ref name="201617passengers">{{cite report |url=http://www.aai.aero/traffic_news/Jan2k16annex3.pdf |title=Traffic News for the month of January 2016: Annexure III |page=3 |date=9 March 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=January 2016: 630,142  passengers; January 2015: 502,186 passengers}}
*{{cite report |url=http://www.aai.aero/traffic_news/Feb2k16annex3.pdf |title=Traffic News for the month of February 2016: Annexure III |page=3 |date=8 April 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=February 2016: 593,513 passengers; February 2015: 459,366 passengers}}
*{{cite report |url=http://www.aai.aero/traffic_news/Mar2k16annex3.pdf |title=Traffic News for the month of March 2016: Annexure III |page=3 |date=22 May 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=March 2016: 593,647 passengers; March 2015: 452,809 passengers}}
*{{cite report |url=http://www.aai.aero/traffic_news/Dec2k16annex3.pdf |title=Traffic News for the month of December 2016: Annexure III |page=4 |date=30 January 2017 |website=Airports Authority of India |access-date=1 February 2017 |quote=April–December 2016: 5,411,816 passengers; April–December 2015: 4,662,809 passengers}}</ref>
<ref name="201617move">{{cite report |url=http://www.aai.aero/traffic_news/Jan2k16annex2.pdf |title=Traffic News for the month of January 2016: Annexure II |page=3 |date=9 March 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=January 2016: 4,266 aircraft movements; January 2015: 3,350 aircraft movements}}
*{{cite report |url=http://www.aai.aero/traffic_news/Feb2k16annex2.pdf |title=Traffic News for the month of February 2016: Annexure II |page=3 |date=8 April 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=February 2016: 4,030 aircraft movements; February 2015: 3,131 aircraft movements}}
*{{cite report |url=http://www.aai.aero/traffic_news/Mar2k16annex2.pdf |title=Traffic News for the month of March 2016: Annexure II |page=3 |date=22 May 2016 |website=Airports Authority of India |access-date=4 April 2016 |quote=March 2016: 4,307 aircraft movements; March 2015: 3,651 aircraft movements}}
*{{cite report |url=http://www.aai.aero/traffic_news/Dec2k16annex2.pdf |title=Traffic News for the month of December 2016: Annexure II |page=3 |date=30 January 2017 |website=Airports Authority of India |access-date=1 February 2017 |quote=April–December 2016: 37,173 aircraft movements; April–December 2015: 34,592 aircraft movements}}</ref>
<ref>{{cite report |url=http://www.aai.aero/traffic_news/Jan2k16annex4.pdf |title=Traffic News for the month of January 2016: Annexure IV |page=3 |date=9 March 2016 |work=Airports Authority of India |access-date=4 April 2016 |quote=January 2016: 5,520 tonnes}}
*{{cite report |url=http://www.aai.aero/traffic_news/Feb2k16annex4.pdf |title=Traffic News for the month of February 2016: Annexure IV |page=3 |date=8 April 2015 |work=Airports Authority of India |access-date=4 April 2016 |quote=February 2016: 5,269 tonnes}}
*{{cite report |url=http://www.aai.aero/traffic_news/Mar2k16annex4.pdf |title=Traffic News for the month of March 2016: Annexure IV |page=3 |date=22 May 2015 |work=Airports Authority of India |access-date=4 April 2016 |quote=March 2016: 5,494 tonnes}}
*{{cite report |url=http://www.aai.aero/traffic_news/Dec2k16annex4.pdf |title=Traffic News for the month of December 2016: Annexure IV |page=4 |date=30 January 2017 |work=Airports Authority of India |access-date=1 February 2017 |quote=April–December 2016: 56,119 tonnes}}</ref>
|focus_city =
<div>
* [[Air India]]
* [[GoAir]]
* [[IndiGo]]
* [[Jet Airways]]
* [[SpiceJet]]
</div>
}}

'''Sardar Vallabhbhai Patel International Airport''' (SVPIA) {{Airport codes|AMD|VAAH}} is an international airport serving the cities of [[Ahmedabad]] and [[Gandhinagar]] in [[Gujarat]], [[India]]. The airport is located in [[Hansol]], {{convert|9|km|mi|abbr=on}} north of central [[Ahmedabad]]. It is named after [[Sardar Vallabhbhai Patel]], the first [[Deputy Prime Minister of India|Deputy Prime Minister]] of India.

In 2016, it handled over 7.2 million passengers and about 140 aircraft movements a day making it the [[List of busiest airports in India by passenger traffic|eighth busiest airport]] in terms of passenger traffic in India. The airport also serves as a focus city for [[GoAir]] and [[Jet Airways]]. In 2015, the government started the procedure for the privatization of the airport. The airport is set to be replaced by [[Dholera International Airport]] due to expansion constraints.

==History==
The airport was set up in 1937, while international operations began in 1992. It was categorised as an [[International airport]] on 23 May 2000.<ref name="newterminal">{{Cite web|title=Airports International - New Terminal in Ahmedabad |url=http://www.aai.aero/misc/newterminal.pdf |work=[[Airports Authority of India]]| archivedate=22 July 2013 |year=2010 |accessdate=24 March 2015|archiveurl=https://web.archive.org/web/20130722185229/http://www.aai.aero/misc/newterminal.pdf}}</ref> In 2010, the new terminal 2 was inaugurated for handling international passengers. A {{convert|18|ft|m}} tall statue of [[Sardar Vallabhbhai Patel]] was also inaugurated at the airport.<ref name="statue">{{cite news|title= Praful Patel inaugurates Sardar Patel’s statue at Ahmedabad airport |url=http://www.dnaindia.com/india/report-praful-patel-inaugurates-sardar-patel-s-statue-at-ahmedabad-airport-1492728 |date=10 January 2011|work=[[Daily News and Analysis]] |location=Ahmedabad |accessdate=Mar 24, 2015}}</ref><ref name="newbuilding">{{cite news|title=New building to be used as international terminal: AAI |url=http://timesofindia.indiatimes.com/city/ahmedabad/New-building-to-be-used-as-international-terminal-AAI/articleshow/6476900.cms |date=1 September 2010 |work=[[Times of India]] |location=Ahmedabad |accessdate=Mar 24, 2015}}</ref> In 2015, the AAI invited proposals for privatisation of Ahmedabad, Chennai, Kolkata and Jaipur airports.<ref name="eightbid">{{cite news|title=Eight firms line up to bid for airport privatisation projects |url=http://indianexpress.com/article/business/business-others/eight-firms-line-up-to-bid-for-airport-privatisation-projects/ |first=Sharmishtha |last=Mukherjee |date=12 February 2015 |work=[[Indian Express]] |location=New Delhi |accessdate=Mar 24, 2015}}</ref>

A 700kWp rooftop solar plant was commissioned at the airport on 21 March 2017.<ref>{{cite web|title=Airport rooftop solar array joins power grid - Times of India|url=http://timesofindia.indiatimes.com/city/ahmedabad/airport-rooftop-solar-array-joins-power-grid/articleshow/57760836.cms|website=The Times of India|accessdate=24 March 2017}}</ref>

==Structure==
The airport currently consists of four terminals: domestic, international, an additional terminal for secondary traffic and a cargo terminal as well. The airport has 45 parking bays and both the international and domestic terminals have four [[Jet bridge|aero-bridges]] each. The new terminal has been modelled based on [[Singapore Changi Airport]].<ref name="Changi">{{Cite news|title=Fly out of Changi, in apnu Amdavad|url=http://articles.timesofindia.indiatimes.com/2009-06-28/ahmedabad/28203593_1_international-terminal-ahmedabad-airport-passenger-traffic|publisher=The Times of India | date=28 June 2009}}</ref>

The new terminal has a half kilometre long [[moving walkway]], which connects the two terminals.<ref name="express_india">{{cite web|title=New terminal soon at A'bad international airport|url=http://www.expressindia.com/latest-news/New-terminal-soon-at-Abad-international-airport/387679/|publisher=expressindia.com|accessdate=1 February 2011}}</ref> Airports Authority of India (AAI) will construct a new technical block which will enhance the flight handling capacity and provide better control of flights.<ref>{{cite web|title=Rs 90 crore for new air traffic control block at Sardar Vallabhbhai Patel International Airport|url=http://www.dnaindia.com/india/report_rs90-crore-for-new-air-traffic-control-block-at-ahmedabad-airport_1323876|publisher=DNA Ahmedabad Edition}}</ref>

===Runway===
The airport has a single runway that is {{convert|3599|m|ft}} long.<ref name="runwayrepair">{{cite news|title=Runway repair at Ahmedabad airport to hit summer travel |url=http://timesofindia.indiatimes.com/city/ahmedabad/Runway-repair-at-Ahmedabad-airport-to-hit-summer-travel/articleshow/7746074.cms |first=Ankur |last=Jain |date=20 March 2011 |work=[[Times of India]] |location=Ahmedabad |accessdate=Mar 24, 2015}}</ref>

===Air traffic control tower===
As part of the airport modernisation process, the AAI announced that it would construct a new [[Air traffic control]] (ATC) building that would include a new [[Airport tower]] {{convert|65|m|ft}} in height.<ref name="ATCautomated">{{cite news|title= Air traffic control at Ahmedabad airport to be automated |url=http://www.dnaindia.com/india/report-air-traffic-control-at-ahmedabad-airport-to-be-automated-1495588 |first=Satish |last=Jha |date=17 January 2011 |work=[[DNA (newspaper)]] |location=Ahmedabad |accessdate=Mar 24, 2015}}</ref>

==Terminals==

===Terminal 1===
Terminal 1 has 32 [[Airport check-in|check-in]] counters and has an area of {{convert|45000|m2|sqft|abbr=on}}.

===Terminal 2===
T2 was inaugurated on 4 July 2010 and opened for use on 15 September 2010. The terminal won the award for the best Steel Structure at the 2009 edition of the National Structural Steel Design and Construction Awards.<ref name="T2award">{{cite news|title= New Ahmedabad airport terminal wins award |url=http://www.dnaindia.com/india/report-new-ahmedabad-airport-terminal-wins-award-1501588 |first=Satish |last=Jha |date=1 February 2011 |work=[[Daily News and Analysis]] |location=Ahmedabad |accessdate=Mar 24, 2015}}</ref> The terminal has four [[aerobridges]] and 32 check-in counters. With the total floor area of approximately 41,000 sq. meters, this terminal will be able to accommodate around 1,600 passengers at any given time. The new 51,975 sq m apron area can cater for the parking of 9 [[Airbus A320|A-321]] and 4 [[ATR-72]] type of aircraft.

===Cargo Terminal===

The airport handled 51,637 tonnes of cargo, inclusive of [[Gold]] and [[Silver]] in 2013-14. 60% of the cargo comes from Domestic sources.<ref name="dedicatedcargo">{{cite news|title=Ahmedabad airport to get dedicated cargo terminal |url=http://timesofindia.indiatimes.com/city/ahmedabad/Ahmedabad-airport-to-get-dedicated-cargo-terminal/articleshow/38480698.cms |first=Piyush |last=Mishra |date=16 July 2014 |work=[[Times of India]] |location=Ahmedabad |accessdate=Mar 24, 2015}}</ref> In 2009, {{convert|3685|sqm|sqft}} of land was leased for a period of seven years out by the AAI to Gujarat Agro Industries Corporation to set up a center for perishable cargo. However, due to a government policy that prevented third party operations at airports run by the AAI, the CPC was not in use until July 2014, when the minister of state for civil aviation announced that the government had issued a [[No Objection Certificate]] for commencement of operations.<ref name="perishablesoon">{{cite news|title=Ahmedabad airport’s perishable cargo centre to begin services soon |url=http://timesofindia.indiatimes.com/india/Ahmedabad-airports-perishable-cargo-centre-to-begin-services-soon/articleshow/38430232.cms |first=Piyush |last=Mishra |date=15 July 2014 |work=[[Times of India]] |location=Ahmedabad |accessdate=Mar 24, 2015}}</ref> In 2014, it was announced that the airport would be getting a dedicated cargo terminal which is expected to come up at Terminal 3.<ref name="dedicatedcargo" />

==Airlines and destinations==
[[File:SpiceJet Boeing 737-900ER Vyas-3.jpg|thumb|A [[SpiceJet]] [[Boeing 737-900ER]] on the runway]]
[[File:GoAir Airbus A320 Vyas.jpg|thumb|An arriving [[GoAir]] [[Airbus A320]]]]
[[File:IndiGo Airbus A320 Vyas-4.jpg|thumb|An [[IndiGo]] [[Airbus A320]] taxiing]]
[[File:Emirates Airbus A330-200 Vyas-1.jpg|thumb|An [[Emirates (airline)|Emirates]] [[Airbus A330-200]] arriving from [[Dubai International Airport|Dubai–International]]]]

===Passenger===
{{Airport-dest-list
|3rdcoltitle = Terminal
|[[Air Arabia]] | [[Sharjah International Airport|Sharjah]] | 2
|[[Air India]] | [[Chennai International Airport|Chennai]], [[Indira Gandhi International Airport|Delhi]], [[Kuwait International Airport|Kuwait]], [[Heathrow Airport|London–Heathrow]], [[Chhatrapati Shivaji International Airport|Mumbai]], [[Newark Liberty International Airport|Newark]] | 2
|[[Emirates (airline)|Emirates]] | [[Dubai International Airport|Dubai–International]] | 2
|{{nowrap|[[Etihad Airways]]}} | [[Abu Dhabi International Airport|Abu Dhabi]] | 2
|[[flydubai]] | [[Dubai International Airport|Dubai–International]] | 2
|[[GoAir]] | [[Chennai International Airport|Chennai]], [[Indira Gandhi International Airport|Delhi]], [[Dabolim Airport|Goa]] , [[Lokpriya Gopinath Bordoloi International Airport|Guwahati]], [[Cochin International Airport|Kochi]], [[Sanganer International Airport|Jaipur]] , [[Netaji Subhash Chandra Bose International Airport|Kolkata]] , [[Chhatrapati Shivaji International Airport|Mumbai]], [[Pune International Airport|Pune]] | 1
|[[IndiGo]] | [[Kempegowda International Airport|Bangalore]], [[Chennai International Airport|Chennai]], [[Indira Gandhi International Airport|Delhi]], [[Dabolim Airport|Goa]], [[Lokpriya Gopinath Bordoloi International Airport|Guwahati]], [[Rajiv Gandhi International Airport|Hyderabad]], [[Sanganer International Airport|Jaipur]], [[Netaji Subhash Chandra Bose International Airport|Kolkata]], [[Cochin International Airport|Kochi]], [[Calicut International Airport|Kozhikode]], [[Chaudhary Charan Singh International Airport|Lucknow]], [[Chhatrapati Shivaji International Airport|Mumbai]],  [[Pune International Airport|Pune]], [[Trivandrum Airport|Trivandrum]], [[Visakhapatnam Airport|Visakhapatnam]] | 1
|[[Jet Airways]] | [[Kempegowda International Airport|Bangalore]], [[Raja Bhoj Airport|Bhopal]], [[Chennai International Airport|Chennai]], [[Indira Gandhi International Airport|Delhi]], [[Lokpriya Gopinath Bordoloi International Airport|Guwahati]], [[Rajiv Gandhi International Airport|Hyderabad]], [[Devi Ahilyabai Holkar Airport|Indore]], [[Chaudhary Charan Singh International Airport|Lucknow]], [[Chhatrapati Shivaji International Airport|Mumbai]], [[Pune International Airport|Pune]] | 1
|{{nowrap|[[Kuwait Airways]]}} | [[Kuwait International Airport|Kuwait]] | 2
|{{nowrap|[[Qatar Airways]]}} | [[Hamad International Airport|Doha]]| 2
|{{nowrap|[[Singapore Airlines]]}} | [[Changi Airport|Singapore]] | 2
|[[SpiceJet]] | [[Kempegowda International Airport|Bangalore]], [[Chennai International Airport|Chennai]], [[Coimbatore International Airport|Coimbatore]], [[Indira Gandhi International Airport|Delhi]], [[Rajiv Gandhi International Airport|Hyderabad]], [[Sanganer International Airport|Jaipur]], [[Cochin International Airport|Kochi]], [[Chhatrapati Shivaji International Airport|Mumbai]], [[Pune Airport|Pune]], [[Dabolim Airport|Goa]] | 1
|[[SpiceJet]] | [[Dubai International Airport|Dubai–International]], [[Muscat International Airport|Muscat]] | 2
|{{nowrap|[[Ventura AirConnect]]}} | [[Kandla Airport|Kandla]], [[Porbandar Airport|Porbandar]], [[Rajkot Airport|Rajkot]], [[Surat Airport|Surat]] |1
|[[Vistara]] | [[Indira Gandhi International Airport|Delhi]] | 1 
}}

===Cargo===
{{Airport-dest-list
|[[Blue Dart Aviation]] | [[Kempegowda International Airport|Bangalore]], [[Chennai International Airport|Chennai]], [[Indira Gandhi International Airport|Delhi]], [[Rajiv Gandhi International Airport|Hyderabad]], [[Netaji Subhash Chandra Bose International Airport|Kolkata]], [[Lucknow Airport|Lucknow]], [[Chhatrapati Shivaji International Airport|Mumbai]]
|[[Emirates SkyCargo]] | [[Al Maktoum International Airport|Dubai–Al Maktoum]]
|[[Ethiopian Airlines Cargo]] | [[Addis Ababa Bole International Airport|Addis Ababa]], [[King Khalid International Airport|Riyadh]]
|[[Etihad Cargo]] | [[Abu Dhabi International Airport|Abu Dhabi]]
|[[Qatar Airways Cargo]] | [[Hamad International Airport|Doha]]<ref name="QRcargo">{{cite news|title=Qatar Airways Cargo to launch freighter service to Ahmedabad on Feb 3  |url=http://www.gulf-times.com/eco.-bus.%20news/256/details/423910/-qatar-airways-cargo-to-launch-freighter-service-to-ahmedabad-on-feb-3 |date=19 January 2015 |work=Gulf Times |accessdate=Mar 24, 2015}}</ref>
}}

==Connectivity==

The [[Ahmedabad Municipal Transport Service]] (AMTS) runs bus services to the airport.<ref name="freqseek">{{cite news|title=Frequent Flyers Seek Transportation at A'bad Airport: BRTS Can Be a Good Option, Say Some Passengers |url=http://dnasyndication.com/dna/City-Ahmedabad/dna_english_news_and_features/Frequent-flyers-seek-transportation-at-A%E2%80%99bad-airport/DNAHM72314 |first=Megha |last=Bhatt |date=18 May 2014 |work=[[Daily News and Analysis]]|location=Ahmedabad |accessdate=Mar 24, 2015}}</ref> For the convenience of travelers, there is a prepaid taxi booth at the airport premises. There are also plans to extend the [[Ahmedabad BRTS]] and a direct [[Metro-Link Express for Gandhinagar and Ahmedabad|metro rail]] linking the airport with the city center.

[http://oneway.cab/ OneWay.Cab] also provides connectivity<ref>{{Cite web|url=http://economictimes.indiatimes.com/small-biz/money/oneway-cab-raises-rs-3-crore-from-ian/articleshow/52738893.cms|title=OneWay.Cab raises Rs 3 crore from IAN - The Economic Times|access-date=2016-09-13}}</ref> to Ahmedabad Airport from Vadodara,<ref>{{Cite web|url=http://oneway.cab/Ahmedabad/Ahmedabad-To-Vadodara-Taxi.html|title=Ahmedabad to Vadodara One Way Taxi Services|website=oneway.cab|access-date=2016-09-13}}</ref> Surat, Rajkot,<ref>{{Cite web|url=http://oneway.cab/Ahmedabad/Ahmedabad-To-Rajkot-Taxi.html|title=Ahmedabad to Rajkot One Way Taxi Services|website=oneway.cab|access-date=2016-09-13}}</ref> Udaipur<ref>{{Cite web|url=http://oneway.cab/Ahmedabad/Ahmedabad-To-Udaipur-Taxi.html|title=Ahmedabad to Udaipur One Way Taxi Services|website=oneway.cab|access-date=2016-09-13}}</ref> & Anand.<ref>{{Cite web|url=http://oneway.cab/Ahmedabad/Ahmedabad-To-Anand-Taxi.html|title=Ahmedabad to Anand One Way Taxi Services|website=oneway.cab|access-date=2016-09-13}}</ref>

==Accidents and incidents==
*[[Indian Airlines Flight 113]] operating from [[Mumbai]] to Ahmedabad crashed on its final approach to the airport on 19 October 1988, killing 130 people including all 6 crew members. The flight was cleared for a visual approach into a foggy airport, when it struck trees and a high-tension pylon at a distance of 5&nbsp;km from Runway 23, before crashing into a field and bursting into flames.
*[[Jet Airways]] Flight 2510, coming in from [[Indore]] collapsed on the runway while landing at the airport on 22 July 2010. There were 57 passengers and four crew members on board the ATR flight. Some passengers received minor injuries as the nose wheel reportedly collapsed due to a tyre burst.<ref>{{cite web|title=Plane's nose wheel collapses, passengers safe|url=http://ibnlive.in.com/news/planes-nose-wheel-collapses-passengers-safe/127298-3.html?from=tn|publisher=IBNLive.com|accessdate=1 February 2011}}</ref>

==See also==
{{Portal|Gujarat|Ahmedabad|Aviation}}

* [[Dholera International Airport]]
* [[Airports in India]]
* [[List of busiest airports in India by passenger traffic]]

==References==
{{reflist|30em}}

==External links==
{{Commons-inline}}
* [http://www.aai.aero/allAirports/ahmedabad_generalinfo.jsp Sardar Vallabhbhai Patel International Airport] at the [[Airports Authority of India]]
*{{WAD|VAAH}}

<!--Templates-->
{{Airports in India}}
{{Ahmedabad topics}}
{{Use dmy dates|date=May 2014}}

[[Category:Airports in Gujarat]]
[[Category:Transport in Ahmedabad]]