{{Use mdy dates|date=February 2014}}
{{Multiple issues|
{{Expert-subject|1=Spain|date=February 2014|reason=most sources are in Spanish and may be partisan in nature}}
{{Outdated|date=March 2015}}
{{Refimprove|date=February 2014}}}}
{{Republicanism sidebar}}
There has existed in the [[Spain|Kingdom of Spain]] a persistent trend of [[Republicanism|republican]] thought, especially throughout the 19th, 20th, and 21st centuries, that has manifested itself in diverse political parties and movements over the entire course of the [[history of Spain]]. While these movements have shared the objective of establishing a republic in Spain, during these three centuries there have surged distinct schools of thought on the form republicans would want to give to the Spanish [[Sovereign state|State]]: [[unitary state|unitary]] (centralized) or [[federal state|federal]].

Despite the country's long-lasting schools of republican movements, the government of Spain has been organized as a republic during only two very short periods in its history, which totaled less than 10 years of republican government in the entirety of Spanish history. The [[First Spanish Republic]] lasted from February 1873 to December 1874, and the [[Second Spanish Republic]] lasted from April 1931 to April 1939.

Currently there are movements and political parties throughout the entire political spectrum that advocate for a Third Spanish Republic, including almost all of the Spanish [[left-wing politics|left]], as well as liberal, right-winged, and nationalist parties.

==History==
===Origins, the First Republic, and the Bourbon Restoration===
The roots of Spanish [[republicanism]] arose out of liberal thought in the wake of the [[French Revolution]]. The first manifestations of republicanism occurred during the [[Peninsular War]], in which Spain and nearby regions fought for independence from [[Napoleon]], 1808–1814. During the reign of [[Ferdinand VII of Spain|Ferdinand VII]] (1813–1833) there were several liberalist military [[pronunciamiento]]s, but it was not until the reign of [[Isabella II of Spain|Isabella II]] (1833–1868) that the first clearly republican and anti-monarchist movements appeared.[[File:La Republica Española En El Mundo revista La Flaca, 28 de marzo de 1873.JPG|220px|thumbnail|right|The republics of the world, France, the United States, and Switzerland, among others, praise the First Spanish Republic, while the monarchies of the world repudiate it.]]

The [[Glorious Revolution (Spain)|Glorious Revolution of 1868]] overthrew Isabella II, but the Cortes, the Spanish parliament revived by the elections of 1869, voted in favor of a monarchy. A search for a new king was made from amongst several European royal courts. Their selection was the Italian prince [[Amadeo I of Spain|Amadeo I]] de [[House of Savoy|Saboya]], but in the midst of a country that was profoundly instable, enveloped in diverse wars (the [[Third Carlist War]], due to the aspirations to the throne of the Bourbon branch of [[Carlism|carlists]], and the [[Ten Years' War]] in Cuba, among others) and including the opposition of republicans and a large part of the [[aristocracy]], the [[Catholic Church]], and the Spanish people, King Amadeo abdicated on February 11, 1873.

On that same day in 1873, the Cortes proclaimed the [[First Spanish Republic]]. However, the Republic fell victim to the same instabilities provoked by the ongoing wars and the division amongst republicans. The majority of republicans were [[federal republicanism|federalists]], and they therefore supported the formation of a [[federation|federal]] democratic republic, but there was also a [[unitary state|unitary]] republic school. What is more, within the federalists there was an intransigent [[confederation|pro-confederation]] sector that was infuriated and later quashed by the [[Cantonal Revolution]] of 1873. The complicated political situation is demonstrated by the fact that in just eleven months there were four presidents of the Republic: [[Francesc Pi i Margall]], [[Estanislao Figueras]], [[Nicolás Salmerón]], and [[Emilio Castelar]] (the only non-federalist president). On January 3, 1874, General [[Manuel Pavía]] led a coup d'état that established a conservative unitary republican dictatorship under the command of [[Francisco Serrano y Domínguez|General Francisco Serrano y Domínguez]]. The dictatorship was in turn ousted by [[pronunciamiento]] on December 29, 1874, in which Brigadier General [[Arsenio Martínez Campos]] declared the [[Restoration (Spain)|Bourbon Restoration]] and [[Alfonso XII of Spain|Alfonso XII]] ascended to the throne.

Following the Restoration, diverse republican parties appeared once again, for example Castelar's ''Partido Demócrata''—later the ''Partido Demócrata Posibilista'' (PDP) – and [[Cristino Martos]]'s ''Partido Progresista Demócrata''. Nonetheless, these parties, immersed in a system of inequal [[suffrage#census suffrage|censitary suffrage]] between 1878 and 1890, were unable to compete with the large dynastic parties: the [[Liberal-Conservative Party (Spain)|Liberal-Conservative Party]] of [[Antonio Cánovas del Castillo]] and [[Liberal Party (Spain, 1880)|Liberal Party]] of [[Práxedes Mateo Sagasta]]. Later Francisco Pi formed the ''Partido Republicano Democrático Federal'' (PRDF), [[Manuel Ruiz Zorrilla]] and José María Esquerdo created ''Partido Republicano Progresista'' (PRP), and Nicolás Salmerón established the ''Partido Republicano Centralista'' (PRC). These parties contributed a diverse set of independent republican deputies to the Spanish parliament. Factions of the PDP and the PRP branched off and fused to form the ''Partido Republicano Nacional''. In 1898 the ''Fusión Republicana'' was formed, and in 1903 the creation of the [[Republican Union Party (Spain)|Republican Union Party]] attempted to represent and fuse all streams of republican thought. However, two parties split from the Republican Union: [[Alejandro Lerroux]]'s [[Radical Republican Party|''Partido Republicano Radical'']] and [[Vicente Blasco Ibáñez|Vicente Blasco]]'s ''Partido de Unión Republicana Autonomista''. In that time the [[Catalonia|Catalan]] ''Centre Nacionalista Republicà'' (CNR) appeared. Following the acts of "[[Tragic Week (Catalonia)|Tragic Week]]" in [[Barcelona]] in 1909, republican parties and the [[Spanish Socialist Workers' Party]] ("PSOE" in Spanish) joined together to form the ''Conjunción Republicano-Socialista'', at the same time as the Catalan sectors of the Republican Union, the CNR, and the PRDF formed the [[Republican Nationalist Federal Union]]. Later [[Melquíades Álvarez (politician)|Melquiades Álvarez]] split from the ''Conjunción Republicano-Socialista'' to form the [[Reformist Party (Spain)|Reformist Party]].

===Primo de Rivera, the Second Republic, and the Franco regime===
After 1917, the Restoration regime entered a state of crisis, which finally resulted in the ''coup d'état'' of [[Miguel Primo de Rivera]], Captain-General of Catalonia. Primo de Rivera established a [[Restoration (Spain)#Primo de Rivera's dictatorship (1923–1930)|dictatorship]] with the approval of the King [[Alfonso XIII of Spain|Alfonso XIII]]. But the crisis of this dictatorship lead to the resignation of Primo de Rivera in 1930 and made the fall of the monarchy inevitable. On April 14, 1931, two days after a round of municipal elections in which republicans won a [[landslide victory]], Alfonso XIII was sent into exile and the [[Second Spanish Republic]] was proclaimed.

The Second Republic adopted the form of a [[unitary state|unitary republic]], allowing the formation of autonomous regions, a status adopted by Catalonia and the [[Basque Country (autonomous community)|Basque Country]]. The Republic quickly had to confront political polarization of the era, at the same time that [[totalitarianism|totalitarian dictatorships]] were rising in power in Europe. The first President of the Second Republic was [[Niceto Alcalá Zamora]] of the [[Liberal Republican Right]]. Later [[Manuel Azaña]], of [[Republican Action (Spain)|Republican Action]] (later the [[Republican Left (Spain)|Republican Left]]) in coalition with the PSOE, was elected president after the political left's victories in the June 1931 elections. Azaña's government attempted to bring many reforms, such as the Agrarian Reform Law, therefore the Azaña administration is known as the ''Bienio Reformista'' ("Two-Year Reformists" in English). It was also in 1931 that, for the first time in Spanish history, [[universal suffrage]] was established, granting to women the right to vote.
[[File:Flag of Spain (1931 - 1939).svg|thumbnail|[[Flag of the Second Spanish Republic]]]]

That in 1932 there had already been a failed coup led by [[José Sanjurjo|General José Sanjurjo]] shows the political instability of the time. In the general elections of 1933, [[José María Gil-Robles]]'s coalition the [[Spanish Confederation of Autonomous Right-wing Groups|Confederación Española de Derechas Autónomas]] won a parliamentary majority, followed by Lerroux's ''Partido Republicano Radical''. The CEDA, which united diverse conservative and Christian Democrat parties, rejected the presidency of Alcalá Zamora, instead electing Lerroux, who appointed to his cabinet of ministers several members of the CEDA. The integration of the CEDA in the government was one of the motivations for the Spanish Revolution of 1934, in which sectors of the PSOE, the [[Communist Party of Spain]] (''Partido Comunista de España'' or "PCE"), and the trade unions the ''[[Unión General de Trabajadores]]'' (UGT) and the ''[[Confederación Nacional del Trabajo]]'' (CNT) lead a [[general strike]]. The anti-government strike occurred at the same time that the [[Republican Left of Catalonia|Esquerra Republicana de Catalunya]] ("ERC")'s [[Lluís Companys]], President of the [[Generalitat of Catalonia]], proclaimed the Catalan State within the Spanish Federal Republic. The violent repression of the Revolution, especially in Asturais, the suppression of Catalan autonomy, and the detention of numerous political personages motived the formation of the Spanish [[Popular Front (Spain)|Popular Front]] by the PSOE, the UGT, the PCE, the ERC, the ''Partido Obrero de Unificación Marxista'' (in Spanish "POUM"), the Republican Left, and the Republican Union, amongst other. The Popular Front came out victorious in the general elections of 1936, returning Manuel Azaña to the presidency after dismissing Alcalá Zamora.

On July 17, 1936, there was a failed military [[pronunciamiento]] in Morocco that managed to take control of a good part of the territory, which provoked the outbreak of the [[Spanish Civil War]]. While the republican regime was abandoned by the other European democracies and only received military support from the [[Soviet Union]], the conservative rebels were supported by [[Nazi Germany]] and [[Kingdom of Italy#Fascist regime (1922–1943)|fascist Italy]], whose support was pivotal in the final victory of the nationalist uprising. The triumphant [[Francisco Franco|General Francisco Franco]] established a brutal, ironclad dictatorship that lasted until his death in [[Spanish transition to democracy|1975]]. As well, [[Emilio Mola]], leader of the uprising against the Second Republic, attempted to establish a "republican dictatorship,"<ref>{{cite web|url=http://ecodiario.eleconomista.es/espana/noticias/808961/10/08/Franco-Mola-y-Queipo-de-Llano-ante-los-tribunales.html|title=Franco, Mola y Queipo de Llano, ante los tribunales – 17/10/08 – 808961 – EcoDiario|language=Spanish}}</ref> but in 1947 Franco established his authoritarian dictatorship as a [[regency]] for the monarchy, and in 1969 he named [[Juan Carlos I|Juan Carlos de Borbón]], grandson of the ousted Alfonso XIII, as his successor and the next king. Juan Carlos ascended to the throne upon the dictator's death in 1975.

=== Exile and Holocaust ===
A [[Spanish Republican government in exile]] was established in [[Paris]] in April 1939. Thousands of Republicans fled the country to France as well. Many of them were captured after [[Battle of France|France was occupied]] by [[Nazi Germany]] in 1940; some 7,000 died in concentration camps, especially [[Mauthausen-Gusen concentration camp|Mauthausen-Gusen]], during the [[Holocaust]].<ref name="Pike, David Wingeate 2000">Pike, David Wingeate. Spaniards in the Holocaust: Mauthausen, the horror on the Danube;  Editorial: Routledge Chapman & Hall ISBN 978-0-415-22780-3. London, 2000.</ref> The Republican government in exile moved to [[Mexico City]] in 1940, returning to Paris in 1946.

===After the transition to democracy===
The anti-Franco opposition failed in their attempts to provoke the dictators downfall, and after his death they started a process of negotiation with the government that led to the [[Spanish transition to democracy]], in which Spain chose a democratic [[parliamentary monarchy]] as the form of government, with the support of groups that had previously advocated a republic, like the PSOE and the PCE. In 1977, after the first democratic general elections since the Francoist dictatorship, the [[Spanish Republican government in exile|Spanish Republican government-in-exile]], maintained since the end of the Civil War, dissolved itself and officially recognized the post-Franco democratic regime.<ref>{{cite web|title=Izquierda Republicana|url=http://www.izqrepublicana.es/documentacion/declaracionrepublicajunio1977b.pdf|language=Spanish}}</ref> Republicanism in Spain since 1976, principally during the Transition, was represented by the ''Acción Republicana Democrática Española'' (ARDE), the Republican Left, and the Spanish Republican Party. However, the Communist Party (PCE) and its coalition the United Left have resumed advocating for a Third Spanish Republic. There are also other regional parties advocating republicanism.

==Republican political parties after the transition==
There are many republican political parties on the national level. However, since the [[Spanish transition to democracy]], only the [[United Left (Spain)|United Left]], and one of its component parties before its formation, the [[Communist Party of Spain]], had representation in Parliament until 2011. In that year, the United Left was joined by [[EQUO]] as the two political parties with parliamentary representation that advocated a republic. The [[Spanish Socialist Worker's Party]] maintains a position of limited intervention in the republic-vs.-monarchy debate, providing some support to the monarchy while at the same time some members of its base identify as republicans.[[File:Sevillarepublica.jpg|thumbnail|right|A republican demonstration, with participation from members of the Communist Party and others, in Seville on April 14, 2006.]]
*The '''United Left''' ("''Izquierda Unida''" or "IU" in Spanish) is the third-largest party in Spain by number of votes. It is a [[coalition]] of left-winged parties, amongst which is the '''Communist Party of Spain''' ("''Partido Comunista de España''" or "PCE" in Spanish). The IU says its [[mission statement|mission]] is "to transform gradually the capitalist economic, social, and political system into a [[democratic socialism|democratic socialist]] system, founded on the principles of [[justice]], [[social equality]], [[solidarity]], respect of [[nature]], and organized in conformity with a federal and republican [[rule according to higher law|"state of rights"]].<ref>{{cite web|title=Izquierda Unida|url=http://www1.izquierda-unida.es/sites/default/files/Estatutos%20IX%20Asamblea.pdf|language=Spanish}}</ref> The United Left and the PCE advocate the establishment of a Third Spanish Republic.<ref>{{cite web|title=Izquierda Unida|url=http://www1.izquierda-unida.es/noticia.php?id=4966|language=Spanish}}</ref><ref>{{cite web|title=Público.es – Lara y Anguita encabezan la lucha por la III República|url=http://www.publico.es/espana/218552/lara/anguita/encabeza/lucha/iii/republica|language=Spanish}}</ref>
*'''Equo''' is a Spanish [[green politics|green]] party that, through its integration into the [[Community of Valencia|Valencian]] '''''Coalició Compromís''''' ("Compromise Coalition" in English), attained representation in the [[Congress of Deputies (Spain)|Congress of Deputies]] after the [[Spanish general election, 2011|2011 elections]] as the "''Compromís-Q''". Equo advocates for a "federal, secular, and republican state".<ref>{{cite web|title=Equo website|url=http://programa.equova.org/?page_id=100|language=Spanish}}</ref>
*The '''Spanish Socialist Workers' Party''' ("''Partido Socialista Obrero Español''" or "PSOE" in Spanish) is the principal left-winged Spanish political party and the one that has spent the most years in parliamentary majority since the Transition (1982–1996, 2004–2011). Despite its republican tradition, the party does not currently define itself as republican,<ref>{{cite web|title=PSOE|url=http://www.psoe.es/download.do?id=515572|language=Spanish}}</ref> and in fact, in recent years the monarchy and its role have been praised by the PSOE.<ref>{{cite web|url=http://www.elmundo.es/elmundo/2005/10/31/espana/1130725446.html|title=Rodríguez Zapatero y Rajoy elogian el 'digno' papel de la monarquía y reiteran su 'lealtad' a la Corona – EL MUNDO|language=Spanish}}</ref><ref>{{cite web|url=http://www.elmundo.es/papel/2008/11/01/espana/2534000.html|title=Zapatero defiende el "cumplimiento ejemplar del papel constitucional" de Doña Sofía / EL MUNDO|language=Spanish}}</ref><ref>{{cite web|url=http://www.psoe.es/ambito/saladeprensa/docs/index.do?action=View&id=160759|title=BLANCO: 'Los alcaldes socialistas que incumplen la Ley de Banderas son una minoría, saben que deben cumplirla y lo harán' – PSOE|language=Spanish}}</ref> However, the [[youth wing]] of the PSOE, the '''[[Socialist Youth of Spain]]''', does identify its ideology as republican,<ref>{{cite web|url=http://www.jse.org/es/nuestras-ideas|title=Nuestras ideas – JSE|language=Spanish}}</ref> and in its resolutions of the 37th Congress (2004–2008), the PSOE declared itself in support of a "civic republicanism".<ref>[http://www.psoe.es/download.do?id=138757 PSOE Resolutiones for the 37th Congress of Deputies] (2004–2008). Page 101 says: "''Para los socialistas, la defensa y la regulación de derechos arranca de la idea misma del republicanismo cívico que propugnamos.''"</ref> The mentions of republicanism disappeared in the resolutions of the 38th Congress due to internal conflict over this position.

==Public opinion==
Spain's government-run Centro de Investigaciones Sociológicas ("CIS," in English: Center for Sociological Research) has not conducted any surveys in which respondents were asked their preference of the system of government, monarchy or republic. However, the CIS does publish surveys on the "value" respondents place on the monarchy, and the agency has occasionally published questions regarding the current monarch, observing a progressive decline in support for the monarchy.<ref>{{cite web|title=Escolar.net: La monarquía se desgasta en España|url=http://www.escolar.net/MT/archives/001062.html|language=Spanish}}</ref> In fact, although the monarchy has normally been one of the most valued institutions, studies have shown that the monarchy has experienced serious loss in public confidence, more than any other government institution, especially among youths aged 18 to 24, who have voted against it repeatedly in CIS studies since 2006.<ref>{{cite web|title=Público|url=http://files.publico.es/estaticos/pdf/ficheros/pdf/14032010.pdf?r=18|language=Spanish}}</ref><ref>{{cite web|title=El Confidencial|language=Spanish|url=http://www.elconfidencial.com/espana/2011/11/12/nuevo-borron-en-una-monarquia-que-suspenden-los-ciudadanos-87528/}}</ref> For the first time ever in 2011, a majority of the population said they did not support the current monarchy.<ref>{{cite web|title=El País|language=Spanish|url=http://politica.elpais.com/politica/2011/10/26/actualidad/1319626481_119745.html}}</ref> A study published on June 24, 2004, even yield a result of 55% of Spaniards agreeing (''"más bien de acuerdo"'') with the statement that "the Monarchy has overstayed its welcome."<ref>{{cite web|url=http://www.elmundo.es/2004/06/25/espana/1655493.html|title=Más de la mitad de los españoles dice que la Monarquía es algo "superado desde hace tiempo" / EL MUNDO|language=Spanish}}</ref><ref group="note">The statement "the Monarchy is something that has long overstayed its welcome", is roughly translated. The actually Spanish wording used is ''"la Monarquía es algo superado hace tiempo"''.</ref>
Spanish newspapers also sporadically publish surveys and opinion polls with questions related to the monarchy and of the survey respondents political affiliation as monarchists or republican, among other options, with results generably in favor of the monarchy until the year 2013:
{| class="wikitable sortable"
|- bgcolor=lightgrey
! width="5%"| Date
! width="15%"| Survey
! width="10%"| Republican (%)
! width="17%" | More republican than monarchist (%)
! width="15%" | Indifferent, "Don't know," & "No response" (%)
! width="10%"| ''Juancarlist''<ref group="note">''Juancarlism'' (variously written with and without the capital "J") is the support not of the institution of the monarchy itself, but rather for the previous monarch, [[Juan Carlos I of Spain|King Juan Carlos I]]. There was no consensus amongst "Juancarlists" on what to do upon Juan Carlos's abdication or death, but some Juancarlists supported the abolition of the monarchy after Juan Carlos, while others believed that the ascension of the King's heir, [[Felipe, Prince of Asturias|Prince Felipe]] should have been put to a plebiscite. Juan Carlos had a particularly strong following because of his work in the [[Spanish transition to democracy]] from dictatorship in the 1970s, however polls showed that this support was waning towards the end of his reign.</ref> (%)
! width="17%" | More monarchist than republican (%)
! width="10%" | Monarchist (%)
|-
| 07/10/1996
| [http://hemeroteca.lavanguardia.es/preview/1996/10/07/pagina-11/33923187/pdf.html Instituto Opina] para [[La Vanguardia]]
| 9.7%
| 6.2%
| 37.2%
| -
| 11.2%
| 35.7%
|-
| 11/1996
| [http://blogs.elpais.com/metroscopia/2011/12/monarquia-republica.html Metroscopia]
| 13%
| -
| 21%
| -
| -
| 66%
|-
| 1997
| [http://dev.alternativatec.es/metroscopia/images/docs/pulso.pdf Metroscopia]
| 15%
| -
| 20%
| -
| -
| 65%
|-
| 1998
| [http://dev.alternativatec.es/metroscopia/images/docs/pulso.pdf Metroscopia]
| 11%
| -
| 17%
| -
| -
| 72%
|-
| 20/11/2000
| [http://www.elmundo.es/2000/11/20/documentos/20N0023.html Sigma Dos] para [[El Mundo (España)|El Mundo]]
| 15.9%
| -
| 41.1%
| -
| -
| 43%
|-
| 20/11/2005
| [http://www.elmundo.es/elmundo/2005/11/20/espana/1132456951.html Sigma Dos] para [[El Mundo (España)|El Mundo]]
| 23.5%
| -
| More than 38%
| -
| -
| 38%
|-
| 28/09/2006
| [http://www.angus-reid.com/polls/view/13464 Instituto Opina] para [[Cadena Ser]]
| 25%
| -
| 10%
| -
| -
| 65%
|-
| 06/10/2007
| [http://www.elperiodicodearagon.com/noticias/noticia.asp?pkid=355346 Gabinete de Estudios Sociales y Opinión Pública (Gesop)] para Época
| 24.8%
| -
| -
| -
| -
| 50.6%
|-
| 11/10/2007
| [http://www.20minutos.es/noticia/289702/0/monarquia/republica/encuesta/ Metroscopia] para la Fundación Toledo
| 22%
| -
| 9%<ref>http://www.elpais.com/graficos/espana/Intencion/voto/elpepuesp/20111218elpepunac_1/Ges/</ref>
| -
| -
| 69%
|-
| 05/01/2008
| [http://www.elmundo.es/elmundo/2008/01/05/espana/1199505756.html Sigma Dos] para [[El Mundo (España)|El Mundo]]
| 12.8%
| -
| 39.9%
| 14.6%
| -
| 28.5%
|-
| 15/08/2008
| [http://www.elmundo.es/elmundo/2008/08/15/espana/1218765103.html Sigma Dos] para [[El Mundo (España)|El Mundo]]
| 16.2%
| -
| 57.9%
| 7%
| -
| 15.7%
|-
| 06/12/2009
| [http://www.elpais.com/articulo/espana/Constitucion/necesita/cambios/elpepiesp/20091206elpepinac_2/Tes Metroscopia] para [[El País]]
| 25%
| -
| -
| -
| -
| 66%
|-
| 31/10/2010
| [http://www.lavanguardia.es/ciudadanos/noticias/20101031/54063139223/un-57-de-los-espanoles-prefiere-la-monarquia-a-la-republica-segun-un-sondeo.html Análisis Sociológicos Económicos y Políticos (ASEP)]
| 26%
| -
| -
| -
| -
| 57%
|-
| 07/11/2010<ref>http://www.elpais.com/articulo/espana/Zapatero/logra/agitar/votante/PSOE/elpepuesp/20101107elpepinac_6/Tes</ref>
| [http://www.elpais.com/graficos/espana/Intencion/voto/elpepuesp/20111218elpepunac_1/Ges/ Metroscopia] para [[El País]]
| 35%
| -
| 8%
| -
| -
| 57%
|-
| 14/04/2011
| [http://www.elpais.com/elpaismedia/diario/media/201104/14/espana/20110414elpepinac_1_Pes_PDF.doc Metroscopia] para [[El País]]
| 39%
| -
| 10%
| -
| -
| 48%
|-
| 20/06/2011
| [http://www.lasextanoticias.com/videos/ver/urdangarin_no_destruye_la_monarquia/536153 Invymark] para [[La Sexta]]
| 36.8%
| -
| 21.1%
| -
| -
| 42.1%
|-
| 12/12/2011
| [http://www.lasextanoticias.com/videos/ver/urdangarin_no_destruye_la_monarquia/536153 Invymark] para [[La Sexta]]
| 37%
| -
| 3.7%
| -
| -
| 59.3%
|-
| 18/12/2011
| [http://www.elpais.com/graficos/espana/Intencion/voto/elpepuesp/20111218elpepunac_1/Ges/ Metroscopia] para [[El País]]
| 37%
| -
| 14%
| -
| -
| 49%
|-
| 02/01/2012
| [http://rsocial.elmundo.orbyt.es/epaper/xml_epaper/El%20Mundo/02_01_2012/pla_562_Madrid/xml_arts/art_7927638.xml?SHARE=6C23C0F29C6C4F158F7CA6264B486305C89B4D4078EBFA5C005BBF2F7FF9ED9568B4DB13AE56F5538DE86934CCF6D3981B0BEACE41903DA386CE2B4A9684D102E78B8F29EF0EE91C6390E16C17CD2F82 Sigma Dos] para [[El Mundo (España)|El Mundo]]
| 33%
| -
| -
| -
| -
| 60%
|-
| 22/04/2012
| [http://www.larazon.es/noticia/9578-refrendo-a-la-monarquia NC Report] para La Razón
| 35.5%
| -
| 15.9%
| -
| -
| 48.5%
|-
| 23/04/2012
| [http://www.lasextanoticias.com/videos/ver/debe_abdicar_en_el_principe/590123 Invymark] para [[La Sexta]]
| 34%
| -
| 8.1%
| -
| -
| 57.9%
|-
| 03/01/2013
| [http://2.bp.blogspot.com/-jJ8OkwtcEQA/UOUFOGXdsuI/AAAAAAAABAw/SfvEYCvzApY/s1600/1+SONDEO+EL+MUNDO+SIGMA+DOS+%28y+V%29+Radiografia+de+la+Corona.jpg Sigma Dos] para [[El Mundo (España)|El Mundo]]
| 41%
| -
| 5.2%
| -
| -
| 53.8%
|-
| 14/04/2013
| [http://www.larazon.es/detalle_normal/noticias/1864974/el-70-5-de-los-votantes-del-psoe-apoya-la-mon#.UdbdWTu-2So] para La Razón
| -
| -
| -
| -
| -
| 63.5%
|-
| 14/04/2013
|[http://www.publico.es/especial/plebiscito-virtual/?src=lmFp&pos=1 Sondeo del diario Público] <ref group="note">Opinion poll conducted by the daily newspaper ''[[Público (Spain)|Público]]'' in April, May and June 2013. Most of the participants in the survey were from the social network [[Twitter]]. In June, the number of participants exceeded 122,000; it also it asked for an assessment of key members of the Royal family. The data also did a breakdown of the number of voters according to their gender, with a higher percentage, although a lower number, of the women's vote in favor of the Republic. [http://www.publico.es/especial/plebiscito-virtual/?src=lmFp&pos=1 See the updated survey].</ref>
| 87.57%
| -
| -
| -
| -
| 12.43%
|-
|06/06/2014
|[http://www.lasexta.com/noticias/nacional/poblacion-prefiere-monarquia-parlamentaria-frente-segun-barometro-lasexta_2014060600128.html] para [[La Sexta]]
|36.3%
| -
|10.6%
| -
| -
|53.1%
|-
|06/06/2014
|[http://elpais.com/elpais/2014/06/06/media/1402074496_679180.html] para [[El País]]
|36%
| -
| -
| - 
| - 
|49%
|-
|07/06/2014
|[http://www.antena3.com/noticias/espana/dos-cada-tres-cree-que-abdicacion-rey-llegado-momento-oportuno_2014060700052.html] para [[Antena 3 (Spain)|antena 3]]
|35.5%
| -
| -
| -
| -
|60.03%
|-
|07/06/2014
|[http://www.elmundo.es/album/espana/2014/06/08/5394aa55268e3ecd2b8b4578.html el mundo] para [[El Mundo (España)|El Mundo]]
|35.6%
| -
| 8.6%
| -
| -
|55.7%
|-
|23/06/2014
|[http://www.larazon.es/detalle_normal/noticias/6730410/espana+relevo-en-el-trono-de-espana/la-valoracion-de-la-monarquia-crece-dos-puntos-en-una-semana#.Ttt1218VBiDKiRJ] para [[La Razón (España)|La Razón]]
|28.3%
| -
|14%
| -
| -
|57.6%
|-
|14/06/2015
|[http://www.elmundo.es/grafico/espana/2015/06/14/557de46d268e3e7c258b457a.html el mundo] para [[El Mundo (España)|El Mundo]]
|33.7%
| -
|4.8%
| -
| -
|61.5%
|}

After 2005, surveys have measured a larger support for republicanism amongst Spanish youth, with more 18- to 29-year-olds identifying themselves as republicans than those identifying as monarchists, according to '''El Mundo'''.<ref>{{cite web|url=http://www.elmundo.es/papel/2005/11/20/espana/1891310.html|title=Los españoles dan un notable a la Monarquía pero un 38% de jóvenes prefiere la República / EL MUNDO|language=Spanish}}</ref> Despite this, some surveys show a public favor of the monarchy, and according to an August 2008 ''El Mundo'' poll, 47.9% of Spaniards would have liked if they had been able to democratically elect the current King Juan Carlos, and 42.3% of respondents think that the succession of the current heir [[Felipe, Prince of Asturias|Prince Felipe]] should be put to a [[referendum|plebiscite]].<ref>{{cite web|url=http://www.elmundo.es/papel/2008/08/15/espana/2474796.html|title='Indiferentes' ante la Corona o la República / EL MUNDO|language=Spanish}}</ref> According to the newspaper [[Público (Spain)|''Público'']]'s "Publicscopio" section in December 2009, 61% survey respondents are in favor of amending the [[Spanish Constitution of 1978|Spanish Constitution]] to allow the Spanish people to decide between a monarchy and a republic,<ref>{{cite web|url= http://www.publico.es/espana/275803/reforma/constitucion/gana/adeptos/ultimo/ano|title= Público.es – La reforma de la Constitución gana adeptos en el último año|language=Spanish}}</ref> a number that increased by 3% compared to the data collected the year before by the same newspaper.<ref>{{cite web|url=http://www.publico.es/espana/180932/dos-ideas-de-espana-frente-a-la-constitucion|title=Público.es – Dos ideas de España frente a la Constitución|language=Spanish}}</ref> According to a 2012 survey by [[Gallup (company)|Gallup]], 54% of Spaniards were in favor of a referendum to choose the form of government (monarchy or republic), and support was always found to be even higher when surveying younger age groups (support was 73.1% amongst 18- to 24-year-olds, but only 34.5% for those above 65 years). Support for such a referendum is also higher amongst the more educated groups of the population, voters in left-wing political parties, and between members of the upper and upper-middle classes. In 2013, as a result of the accusation of [[Infanta Cristina, Duchess of Palma de Mallorca|Princess Cristina]] in the [[Nóos case|''Nóos'' scandal]], republican support has begun to increase greater than ever before.

When Juan Carlos announced his abdication on 2 June 2014, thousands of protesters took to the squares of several Spanish towns and cities demanding a referendum on whether the monarchy should continue.<ref>{{Cite news |url=http://www.bbc.com/news/world-europe-27672997 |title=Spanish cabinet to discuss King Juan Carlos's abdication |author= |publisher=[[BBC News]] |date=3 June 2014 |accessdate=3 June 2014}}</ref>

==See also==
*[[History of Spain]]
*[[Politics of Spain]]
*[[Republicanism]]
*[[First Spanish Republic]]
*[[Second Spanish Republic]]
*[[Spanish Civil War]]
*[[Alliance of European Republican Movements]]

==Notes==
<references group="note" />

==References==
{{Reflist|3}}

==External links==
{{commons category|Republicanism in Spain}}
*[http://www.redrepublicana.es/ Red Inter Civico Republicana], a Spanish republican movement.
*[http://www.aerm.org/ Alliance of European Republican Movements], the umbrella organization of the RICP.
[[Category:Republicanism in Spain| ]]
[[Category:Politics of Spain]]
[[Category:Political movements in Spain]]
[[Category:Republicanism by country|Spain]]
[[Category:Republicanism in Europe|Spain]]