{|{{Infobox Aircraft Begin
 | name=DB-1
 | image=Ant-36side.jpg
 | caption=Side view of the precursor, the first ANT-25.
}}{{Infobox Aircraft Type
 | type=Long-range bomber
 | national origin=[[Soviet Union]]
 | manufacturer=[[Tupolev]]
 | designer=
 | first flight=1934
 | introduced=
 | retired=1937
 | status=
 | primary user=[[Soviet Air Forces]]
 | number built=18
 | developed from=[[Tupolev ANT-25]]
 | variants with their own articles=
}}
|}
The '''Tupolev DB-1''' was a [[Soviet Union|Soviet]] long-range bomber developed in the 1930s. It was developed from the [[Tupolev ANT-25]] distance record-breaking aircraft. Development was prolonged and it was recognized as obsolete by the time it was in production. Only eighteen were built and all were withdrawn from service in 1937.

==Development==
The possibilities of exchanging some of the fuel of the ANT-25 for bombs and/or cameras was recognized early in its development, and the [[Soviet Air Forces|VVS]] issued a requirement for an aircraft to carry {{convert|1000|kg|abbr=on}} over an operational radius of {{convert|2000|km|abbr=on}} at a speed of {{convert|200|km/h|abbr=on}}. The Tupolev [[OKB]] had prepared a design and built a mockup by August 1933, using the internal designation of ANT-36, and the VVS approved both. Series production of a first batch of 24, out of a planned total of 50, was initiated in the new Factory No. 18 at [[Voronezh]] in 1934, but only a total of 18 were built before the program was canceled.<ref name=go>Gordon, p. 66</ref>

The airframe, engine and crew compartments were retained from the ANT-25 almost unchanged, although the co-pilot's and navigator's positions were each given one {{convert|7.62|mm|1|abbr=on}} [[Degtyaryov machine gun|DA]] [[machine gun]] for defense. The DB-1 (long-range bomber model 1), as it was designated in VVS service, was given a smooth skin and a bomb bay was built in the wing center section that carried ten {{convert|100|kg|abbr=on}} FAB-100 bombs nose-up. An AFA-14 camera was mounted in the rear cockpit, but other cameras could be carried instead of the bombs.<ref name=g6>Gunston, p. 86</ref>

==Operational history==
The DB-1's first flight was in 1934,<ref name=g6/> but the first production aircraft was not tested until the autumn of 1935. It was rejected by the VVS because of poor manufacturing quality. Only ten aircraft were placed into service by the VVS which equipped one regiment based near the factory at Voronezh. All of these were retired by the VVS in 1937.<ref name=go/>

==Variants==
The remainder of these aircraft were used for a variety of tasks. One was fitted with the first flight-cleared Charomskii AN-1 [[diesel engine]] for flight testing. The change in the [[Center of gravity of an aircraft|center of gravity]] caused many problems and the undercarriage had to be made non-retractable. Two others were selected in 1938 for distance record attempts with all-female crews, but an [[Ilyushin DB-3]] was ultimately chosen instead.<ref>Gunston, pp. 86–87</ref>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (variant) ==

<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->

{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop

|ref=Gordon, OKB Tupolev, A History of the Design Bureau and its Aircraft

|crew=three
|capacity= <!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main=13.4 m 
|length alt=43.96 ft
|span main=34 m
|span alt=111.54 ft
|height main=
|height alt=
|area main=88.2 m<sup>2</sup>
|area alt=949.376 sqft
|airfoil=
|empty weight main= 
|empty weight alt= 
|loaded weight main= 7806 kg
|loaded weight alt=17,209.3 lb 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|max takeoff weight more=
|more general=

|engine (prop)=[[Mikulin AM-34]]R
|type of prop= liquid-cooled V-12 
|number of props=1
|power main=611.5 kW 
|power alt=820 hp
|power original=
|power more=

|max speed main=200 kp/h 
|max speed alt=124.27 mph
|max speed more= 
|cruise speed main= 
|cruise speed alt=
|cruise speed more 
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main=4000 km 
|range alt=2485.5 mi
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main=3000 m 
|ceiling alt=9843 ft 
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|guns=2 x {{convert|7.62|mm|1|abbr=on}} [[Degtyaryov machine gun|DA]] machine guns 
|bombs= up to ten {{convert|100|kg|abbr=on}} FAB-100 bombs 
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
*[[Tupolev ANT-25]]
|similar aircraft=
*[[Vickers Wellesley]]
|lists=<!-- related lists -->
}}

==References==
{{reflist}}
{{refbegin}}
* {{cite book|last=Gordon|first=Yefim|author2=Rigamant, Vladimir |title=OKB Tupolev: A History of the Design Bureau and its Aircraft|publisher=Midland Publishing|location=Hinckley, England|year=2005|isbn=1-85780-214-4}}
* {{cite book|last=Gunston|first=Bill|title=Tupolev Aircraft since 1922|publisher=Naval Institute Press|location=Annapolis, MD|year=1995|isbn=1-55750-882-8}}
{{refend}}

<!-- ==External links== -->

{{Tupolev aircraft}}

[[Category:Soviet bomber aircraft 1930–1939]]
[[Category:Tupolev aircraft|DB-1]]
[[Category:Abandoned military aircraft projects of the Soviet Union]]