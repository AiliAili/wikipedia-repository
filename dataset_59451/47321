{{Infobox officeholder
| name         = President Constantine W. Curris
| image        = Pic_for_Constantine_W._Curris_article.jpg
| caption      =
| order        =
| title        = President of<br />[[Murray State University]]
| term_start   = 1973
| term_end     = 1983
| predecessor  = [http://pogue.murraystate.edu/ar/RG/RG003-11-01.htm/ Harry Sparks]
| successor    = [http://www.humanics.org/atf/cf/%7BE02C99B2-B9B8-4887-9A15-C9E973FD5616%7D/kala_m_stroup_web_bio.pdf/ Kala Stroup]
| title2       = President of the<br />[[University of Northern Iowa]]
| term_start2  = 1983
| term_end2    = 1995
| predecessor2 = [http://www.library.uni.edu/collections/special-collections/biographical-sketches/john-j-kamerick/ John Joseph Kamerick]
| successor2   = [http://www.library.uni.edu/collections/special-collections/biographical-sketches/robert-d-koob/ Robert D. Koob]
| title3       = President of<br />[[Clemson University]]
| term_start3  = 1995
| term_end3    = 1999
| predecessor3 = [http://media.clemson.edu/library/special_collections/findingaids/archives/Series0102Prince.html/ Phillip Hunter Prince]
| successor3   = [[James Frazier Barker]]
| title4       = President of the [[American Association of State Colleges and Universities]]
| term_start4  = 1999
| term_end4    = 2008
| predecessor4 = James B. Appleberry
| successor4   = [http://www.aascu.org/presidents-bio/ Muriel A. Howard]
| birth_date   = {{birth date and age|1940|11|13}}
| birth_place  = Lexington, Kentucky, United States
| death_date   = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| death_place  =
| alma_mater   = [[University of Kentucky]], [[University of Illinois]]
| residence    = [[Lexington, Kentucky]]
| profession   = Higher Education Administrator
| religion     =
| spouse       = Jo Hern Curris
| children     = Robert Alexander and Elena Diane
| signature    =
| website      = http://agbsearch.com/consultants/constantine-curris
| footnotes    =
}}
'''Constantine W. "Deno" Curris''' is an American educator who, at 32, became the youngest university president in Kentucky history.<ref>[https://news.google.com/newspapers?nid=266&dat=19630621&id=KP8uAAAAIBAJ&sjid=TNwFAAAAIBAJ&pg=3040,6231703/ Kentucky New Era, June 21, 1983, pg5]</ref> He held that office at three universities for a total of 26 years, after which he became president of the [[American Association of State Colleges and Universities]] an organization of more than 400 colleges and universities.   The New York Times said that Curris “is a strong advocate for higher education and its students and a proponent of the qualitative strengthening of public higher education institutions in order to meet public needs and expectations in the 21st century.”<ref name="nytimes">{{cite web|url=https://www.nytimes.com/ref/college/collegespecial2/coll_asscu_currisbio.html |title=Biography of Constantine Curris |publisher=Nytimes.com |date=2003-09-22 |accessdate=2012-07-04}}</ref>

==Education and Honors==
Constantine William Curris was born in [[Lexington, Kentucky]], on November 13, 1940 and graduated from the [[University of Kentucky]] in 1962 with a B.A. in [[Political Science]].  He obtained an M.A. in Political Science and Public Administration from the [[University of Illinois]] in 1965<ref name="las.illinois" />   and an Ed.D. in Higher Education from the [[University of Kentucky]] in 1967.<ref name="ukalumni">{{cite web|url=http://www.ukalumni.net/s/1052/index-no-right.aspx?sid=1052&gid=1&pgid=677 |title=Constantine W. Curris |publisher=Ukalumni.net |date= |accessdate=2012-07-04}}</ref> He received the Alumni Achievement Award from the College of Arts and Science at the University of Illinois.<ref>{{cite web|url=http://www.las.illinois.edu/alumni/awards/achievement/ |title=Award Recipients " Award Programs " Alumni Association " Alumni & Friends " College of Liberal Arts & Sciences " University of Illinois |publisher=Las.illinois.edu |date= |accessdate=2012-07-04}}</ref>  He is a member of the University of Kentucky Arts & Sciences Hall of Fame,<ref>{{cite web|url=http://alumni-friends.as.uky.edu/alumni-hall-fame-scholarship-recognition |title=Alumni Hall of Fame & Scholarship Recognition |publisher=Alumni-friends.as.uky.edu |date= |accessdate=2012-07-04}}</ref> the University of Kentucky College of Education Hall of Fame, and he was inducted into the University of Kentucky Hall of Distinguished Alumni on May 19, 2000.  
<ref name="ukalumni" />

==Early career==
Curris began his work in higher education in 1965 as vice president and dean of the faculty at [[Midway College]] in [[Kentucky]]. In 1968 he became director of academic programs for the [http://wvde.state.wv.us/boe/ West Virginia Board of Education]. From 1969 through 1971 he was dean of student personnel programs at [[Marshall University]] in [[West Virginia]], and for the following two years was the vice president and dean of the faculty at the [[West Virginia Institute of Technology]].<ref name="library">{{cite web|url=http://www.library.uni.edu/collections/special-collections/biographical-sketches/constantine-william-curris |title=Constantine William Curris |publisher=Library.uni.edu |date=1998-03-18 |accessdate=2012-07-04}}</ref>

==Middle career==
In 1973, the 32-year-old Curris was selected as the president of [[Murray State University]], a position he held until 1983. During this decade, Murray State moved from being a former teachers college that had acquired the status of a University in 1966 toward being a larger regional university with faculty hired through national search committees, a full range of student and faculty services, and the beginnings of a national reputation. Curris’s tenure there was controversial.<ref>http://www.aaup.org/NR/rdonlyres/3B7E9A11-F939-4FEA-88D4-DD787080A4BF/0/Murray.pdf</ref><ref>Powell, Bill,  (Feb. 18, 1981) “President Curris of Murray …”, Louisville Courier Journal, Page 1.</ref> “After Curris's contract was not continued at Murray State in 1983, he was hired as president of UNI ([[University of Northern Iowa]]). That same year, the Murray State University Board of Regents named the school's new [http://www.murraystate.edu/campus/CurrisCenter.aspx/ student center for Curris].”<ref name="library" /> 
Curris was president and professor of public policy at the University of Northern Iowa, 1983-1995. During these years, Curris began his active involvement with the [[American Association of State Colleges and Universities]] (AASCU).  One of the buildings constructed at UNI during Curris’s tenure  was subsequently named the [http://www.library.uni.edu/collections/special-collections/building-histories/curris-business-building/ Curris Business Building] by the [http://www.regents.iowa.gov/ Iowa State Board of Regents]. 
<ref name="library" />
From 1995 to 1999, he served as president and professor of public policy of [[Clemson University]] in Clemson, S.C. 
<ref name="bare_url">{{cite web|url=http://chronicle.com/article/President-of-State-Colleges/41695/ |title=President of State-Colleges Association, Constantine W. Curris, to Retire - Government - The Chronicle of Higher Education |publisher=Chronicle.com |date=2008-09-26 |accessdate=2012-07-04}}</ref>

==Later career==
Curris became widely known as a leader in American higher education; he was a featured participant in a 1997 national video teleconference on “[http://www.worldcat.org/title/new-public-university-how-do-we-compete-in-a-changing-environment/oclc/036980835/ The New Public University: How Do We Compete in a Changing Environment?]” Curris was named president of [[AASCU]] in 1999 and served until 2008.  David L. Warren, president of the [[National Association of Independent Colleges and Universities]] said of Curris: "He brings a broad and sophisticated grasp of the issues, and a highly refined judgment about the national political environment. "<ref name="las.illinois">{{cite web|url=http://www.las.illinois.edu/alumni/magazine/articles/2001/curris/ |title=Dr. Constantine Curris |publisher=Las.illinois.edu |date= |accessdate=2012-07-04}}</ref> He has also been an occasional contributor to ''[[The Chronicle of Higher Education]]'', including articles about the public purposes of public colleges, in support of a unit-record system of collecting student data, and about getting college students to the polls. 
<ref name="bare_url" />

==Boards and commissions==
Other professional experiences for Curris include appointments to the [http://www.southerngrowth.com/pubs/AnniversaryCommentaries/CCurris.pdf/ 1998 Commission on the Future of the South], the [http://www.aplu.org/NetCommunity/Document.Doc?id=183/ Kellogg Commission on the Future of State and Land-Grant Universities], the [[Education Commission of the States]], the Iowa Board of Economic Development, the [[South Carolina Research Authority]], The Sigma Chi Foundation, and the chairmanships of American Humanics and the Iowa Task Force on Teacher Education and Certification.<ref name="nytimes" />

==Retirement==
Since his retirement from AASCU in 2008 Curris has been a consultant in academic searches and is currently with AGB  Search.<ref>[http://agbsearch.com/consultants/constantine-curris Constantine Curris | AGB Search<!-- Bot generated title -->]</ref>
In 2009 he was appointed to the Murray State University Board of Regents and now serves as chairman of that body.<ref>{{cite web |url=http://www.murraystate.edu/boardofregents |publisher=Murray State University |title=Board of Regents}}</ref>

Curris is married to Jo Hern Curris, a tax attorney.  They live in Lexington, Kentucky and are the parents of two adult children: Robert Alexander and Elena Diane.<ref name="nytimes" />

==References==
{{Reflist}}

{{Clemson University presidents}}

{{DEFAULTSORT:Curris, Constantine W.}}
[[Category:1940 births]]
[[Category:Living people]]
[[Category:American educators]]
[[Category:Presidents of Murray State University]]
[[Category:Presidents of Clemson University]]