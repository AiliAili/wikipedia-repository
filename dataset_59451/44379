{{Infobox airport
| name         = Trier-Föhren Airport
| nativename   = {{lang|de|Flugplatz Trier-Föhren}} 
| image        =
| IATA         = <!--none, if you add one, please include a reference-->
| ICAO         = EDRT
| pushpin_map            = Germany Rhineland-Palatinate
| pushpin_map_caption    = Location in Rhineland-Palatinate, Germany
| pushpin_label          = Trier-F
| pushpin_label_position = right
| type         = Public
| owner        =
| operator     = [[Bundeswehr]] / Flugplatz GmbH Trier
| city-served  = [[Trier]], [[Germany]]
| location     = [[Föhren]]
| elevation-f  = 665
| elevation-m  = 203
| coordinates  = {{Coord|49|51|48|N|006|47|17|E|region:DE_type:airport}}
| website      = [http://www.flugplatz-trier.de/ www.Flugplatz-Trier.de]
| metric-rwy   = y
| r1-number    = 05/23
| r1-length-m  = 1,200<sup>†</sup>
| r1-length-f  = 3,937<sup>†</sup>
| r1-surface   = Concrete
| footnotes    = <sup>†</sup>{{convert|1130|m|0|abbr=on}}: takeoff on 05/landing on 23.
Sources: AIRPORTS.DE,<ref name="airports.de">[http://www.airports.de/airport.php?ICAO=EDRT Trier - Föhren - Flugplatz (EDRT)] at AIRPORTS.DE</ref> [[DAFIF]]<ref name="WAD">[http://www.worldaerodata.com/wad.cgi?airport=EDRT Airport information for EDRT] from [[DAFIF]] (effective October 2006)</ref>
}}

'''Trier-Föhren Airport''' ({{lang-de|Flugplatz Trier-Föhren}}) {{airport codes||EDRT<ref>{{GCM|EDRT|Trier-Föhren Airport (IATA: none, ICAO: EDRT)}}</ref>}} is an [[airport]] serving [[Trier]], a city in [[Rhineland-Palatinate]], [[Germany]]. It located in [[Föhren]], {{convert|8|NM|0|lk=in}} northeast of [[Trier]]<ref name="airports.de" /> and approximately {{convert|340|mi|0}} southwest of [[Berlin]]. The airport supports [[general aviation]], with no commercial airline service available.  Charter services and a full selection of amenities are available.

The current airport is a recent facility, opened in 1977, built about 18&nbsp;km from the site of the historic [[Trier Air Base|Trier Airfield]] built in the early 20th Century.<ref>[http://creyete.com/camera/1458-Flugplatz-Trier-F%C3%B6hren---EDRT Flugplatz Trier-Föhren - EDRT]</ref>

==History==
Trier-Föhren Airport was built as a new airport to replace Trier Airfield in the 1970s.  It is  next to the village of Föhren. A French army garrison formerly at Trier Airfield also moved to Föhren until it was disbanded in the 1990s. Since then, the "Industriepark Region Trier" was developed on its former site.<ref>[http://www.i-r-t.de/ Industriepark Region Trier]</ref>

==Facilities==
The airport resides at an [[elevation]] of {{convert|665|ft|0}} above [[mean sea level]]. It has one [[runway]] designated 05/23 with a [[concrete]] surface measuring {{convert|1200|x|30|m|0}}.<ref name="airports.de" /><ref name="WAD" />

==See also==
{{Portal|Germany|Aviation}}
* [[Transport in Germany]]
* [[List of airports in Germany]]

==References==
{{reflist}}

==External links==
* [http://www.flugplatz-trier.de/ Official website]

<!--Navigation box-->
{{DEFAULTSORT:Trier-Fohren Airport}}
[[Category:Airports in Germany]]