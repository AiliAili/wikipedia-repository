{{Use dmy dates|date=December 2013}}
{{Use Australian English|date=December 2013}}
{{Infobox Australian place | type = town
| name     = Rheola
| state    = vic
| image    = 
| caption  = Rheola Hall
| lga      = Shire of Loddon
| use_lga_map = yes
| postcode = 3517
| est      = 
| pop      = 316
| pop_year = {{CensusAU|2011}}
| pop_footnotes=<ref name="abs">{{Census 2011 AUS | id = SSC21140| name = Rheola (State Suburb) | accessdate = 7 December 2014 | quick = on}}</ref>
| elevation= 216
| latd  = 36 |latm =39 |lats  =06
| longd = 143 |longm =41 |longs =54
| maxtemp  = 
| mintemp  = 
| rainfall = 
| stategov = [[Electoral district of Ripon|Ripon]]
| fedgov   = [[Division of Murray|Murray]]
| dist1    = 200
| dir1     = NW
| location1= [[Melbourne]]
| dist2    = 60
| dir2     = WNW
| location2= [[Bendigo, Victoria|Bendigo]]
| dist3    = 29
| dir3     = S
| location3= [[Wedderburn, Victoria|Wedderburn]]
}}

'''Rheola''' is a locality in central [[Victoria (Australia)|Victoria]], Australia. At the {{CensusAU|2011}}, Rheola and the surrounding area had a population of 316.<ref name="abs"/> Originally called the 'Berlin Rush' or 'Berlin field' after [[gold nugget]]s were found. It is part of Victoria's 'Golden Triangle,'<ref name = "v">{{Cite web| last = Lodden Shire Council  | title = www.loddon.vic.gov.au | url = http://www.loddon.vic.gov.au/ | accessdate =13 August 2012 | postscript = <!--None--> }}</ref> which contains some of the world's richest gold fields.  Some of the gold nuggets found in these fields include the [[Welcome Stranger]], [[Hand of Faith]], and the Blanche Barkly. The Viscount Canterbury, Viscountess Canterbury and Precious were all found in the gold field at Rheola.<ref name = "b">{{Cite web| last = National Library of Victoria | title = Catalogue | url = http://catalogue.nla.gov.au/Search/Home?lookfor=Rheola&type=all&limit%5B%5D=&submit=Find&limit%5B%5D=format%3APicture | accessdate =14 August 2012}}</ref>

Nearby locations are the towns of Inglewood (birthplace of Sir [[Reginald Ansett]]), Moliagul (birthplace of [[Royal Flying Doctor Service of Australia|The Flying Doctors]],  Reverend [[John Flynn (minister)|John Flynn]]) and Melville Caves located in the [[Kooyoora State Park]].

The district is known for its annual Easter Monday Charity Carnival which began in 1871.<ref name = "t">{{Cite web| last = Bendigo Tourism | title = Whats On This Month | url = http://www.bendigotourism.com/whats-on/whats-on-this-month/event/652-rheola-charity-carnival | accessdate =13 August 2012}}</ref>

== Location ==
Rheola is within the Loddon Shire, located 60 kilometers west of Bendigo.  Nearby districts include: [[Kingower, Victoria|Kingower]], [[Arnold, Victoria|Arnold]], Llanelly, Murphys Creek, [[Moliagul, Victoria|Moliagul]], [[Logan, Victoria|Logan]], Wehla and the  [[Kooyoora State Park]].  Nearby towns include: [[Inglewood, Victoria|Inglewood]], [[Wedderburn, Victoria|Wedderburn]], [[St Arnaud, Victoria|St Arnaud]] and [[Dunolly, Victoria|Dunolly]].

== History ==
Rheola, like its neighbouring districts, was founded as gold mining village during the gold rush in the second half of the 19th century.  Originally named 'Berlin' (earlier called Byr Lyn or Bervie) the mining village was based near Kangderaar Creek which runs though the area.

The Berlin Post Office was established on 15 February 1869 but was changed to the Rheola Post Office in November 1876 when the town changed its name to Rheola after the town in [[Wales]].<ref name = "g">{{Cite web| last = Gold Fever Prospecting  | title = History & Methods | url = http://www.goldfeverprospecting.com/augoprhime.html }}</ref>  The Post Office closed on 19 June 1974.<ref name = "a">{{Cite web| last = Premier Postal History  | title = Post Office List | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=vic&country= | accessdate =12 August 2012 | postscript = <!--None--> }}</ref>

The 'Berlin Rush' beginnings, August 1868 when Alexander Clelland sank a shallow shaft outside John Catto's Paddock and found a 60 oz nugget at the bottom. The Government rewarded him 100 pounds for the discovery of what he called "Bervie" Gold field. This name was spelled incorrectly and became Berlin in the official register.<ref name = "h">{{Cite web| last = Department of Planning and Community Development  | title = Kerong Region Sites | url =  http://www.dpcd.vic.gov.au/__data/assets/pdf_file/0018/44541/KorongRegion.pdf }}</ref> The Rheola area became famous for its beds of large nuggets scattered through the gullies.<ref name="g" />

Some of the gold nuggets found during the 19th century include: The Needful found in 1869, Rum Ton found in 1870, Viscount Canterbury found in 1871, Precious found in 1871, Viscountess Canterbury and the Crescent found in 1872. John Catto's Paddock was the location of both the Precious and the Viscount Canterbury.<ref name = "b" />  The Precious was Victoria’ fourth largest nugget, weighted at 1,717 ounces.<ref name = "n">{{Cite web| last = Department of Planning and Community Development  | title = Korong Historical Notes | url = http://www.dpcd.vic.gov.au/__data/assets/pdf_file/0004/44635/Korong1.pdf }}</ref>

== Easter Monday Charity Carnival ==
The Rheola Charity Carnival began in 1871.  Events at the Charity Carnival include the wood chopping, vintage tractor pull, small stationary engines display, classic cars and ute display, sheep dog trials, clay target shooting, horse riding event, sheaf tossing, "Miss Rheola" Carnival Girl and the Rheola Gift footrace.  Money raised during the event goes to local hospitals in Inglewood and Dunolly as well as helping to fund the maintenance of the Hall and grounds where the carnival is held.<ref>2012 Rheola Charity Carnival Poster</ref> The event has in recent years had the slogan "Real Country Entertainment since 1871" a prior slogan was "All roads lead to Rheola".

==References==
{{Reflist}}

==External links==
* [http://parkweb.vic.gov.au/explore/parks/kooyoora-state-park Kooyoora State Park]
* [http://www.loddon.vic.gov.au/calendar/calendar.asp?h=-1 Loddon Shire Events Calendar]
* [http://maps.google.com.au/maps?hl=en&safe=active&q=google+maps+rheola&um=1&ie=UTF-8&hq=&hnear=0x6ad09820589cdb1f:0x40579a430a09750,Rheola+VIC&gl=au&sa=X&ei=x0EvUPfyNYmtiAfelIGwCA&ved=0CAkQ8gEwAA Rheola on Google Maps]