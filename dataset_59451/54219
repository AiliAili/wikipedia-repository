{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{| class="infobox bordered" cellpadding="4" style="font-size: 90%; width: 25em;"
|- style="text-align:center;"
|colspan=2|
|-  style="text-align:center; background:blue;"
|colspan=2| <span style="color:White;">'''Forestville Hockey Club'''
|-  style="text-align:center; background:#eee;"
|| '''League''' || Hockey SA Premier League
|- style="text-align:center;"
|| '''Founded''' || 1905
|- style="text-align:center;"
|| '''Team Colours''' || Blue and white||
|- style="text-align:center;"
|| '''Nickname''' || Panthers||
|}

'''Forestville Hockey Club''' is a [[hockey]] club in [[Adelaide]], South Australia.

==History==

Forestville Hockey Club was formed in 1905 by Harry Cowham in an effort to get people living in [[Unley, South Australia]], interested in playing hockey. Forestville competed in that year against: St John's, Sturt, West Torrens, Rosemount, East Torrens, Bankville and [[North Adelaide]]. Forestville is the oldest of the South Australian clubs and it is believed to be the oldest playing in Australia. The club's first 'pitch' was in a paddock near Leah St. Forestville, not far from its present Goodwood Oval location. Forestville play a majority of their home games at the [[State Hockey Centre (South Australia)|State Hockey Centre]]. The venue has been home to Forestville games since 1992.

==Australian representatives==

* [[Trevor Smith (field hockey)|Trevor Smith]] (1976–1984)
* [[Terry Smith (field hockey)|Terry Smith]] (1980–1985)
* [[Roger Smith (field hockey)|Roger Smith]] (1988)
* [[Paul Lewis (field hockey)|Paul Lewis]]  (1992–1996)
* [[Tom Wickham]] (2013–present)

==Achievements==

South Australian Hockey Association men's A grade [[best and fairest]] winners:

* 1935 Harry Cowham
* 1943 Robert Ritchie
* 1964 Geoff Sharples
* 1967 Geoff Sharples
* 1989 Roger Smith
* 1990 Roger Smith
* 1992 Roger Smith

==Panther Press==

Forestville publishes a monthly newsletter, "The Panther Press".

==See also==
* [[List of sporting clubs in Adelaide]]
* [[List of South Australian hockey clubs]]

==External links==
*[http://www.fhc.com.au/ Forestville Hockey Club website]
*[http://www.hockeysa.com.au/ Hockey SA website]
*[http://www.evolvingsolutions.com.au/fhc/images/pp_current_newsletter.pdf Panther Press]
*[http://www.evolvingsolutions.com.au/fhc/index.php?page=history Forestville History Association]

[[Category:Australian field hockey clubs]]
[[Category:1905 establishments in Australia]]
[[Category:Sports clubs established in 1905]]
[[Category:Sporting clubs in Adelaide]]