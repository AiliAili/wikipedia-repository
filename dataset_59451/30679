{{Taxobox
| image = 2014-10-21 Dendrocollybia racemosa (Pers.) R.H. Petersen & Redhead 472423.jpg
| image_width = 234px
| image_caption =
| regnum = [[Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Agaricales]]
| familia = [[Tricholomataceae]]
| genus = '''''Dendrocollybia'''''
| genus_authority = [[R.H.Petersen]] & Redhead (2001)
| type_species = '''''Dendrocollybia racemosa'''''
| type_species_authority = ([[Christian Hendrik Persoon|Pers.]]) R.H.Petersen & Redhead (2001)
| synonyms_ref = <ref name="urlMycoBank: Dendrocollybia racemosa"/>
| synonyms = *''Agaricus racemosus'' <small>Pers.</small>
*''Collybia racemosa'' <small>(Pers.) [[Lucien Quélet|Quél.]]</small>
*''Microcollybia racemosa'' <small>(Pers.) Lennox<ref name=Lennox1979/></small>
*''Mycena racemosa'' <small>(Pers.) [[Samuel Frederick Gray|Gray]]<ref name=Gray1821/></small>
}}

'''''Dendrocollybia''''' is a fungal [[genus]] in the family [[Tricholomataceae]] of the order [[Agaricales]]. It is a [[monotypic]] genus, containing the single species '''''Dendrocollybia racemosa''''', [[common name|commonly]] known as the '''branched Collybia''' or the '''branched shanklet'''. The somewhat rare species is found in the [[Northern Hemisphere]], including the [[Pacific Northwest]] region of western North America, and Europe, where it is included in several [[Regional Red List]]s. It usually grows on the decaying [[basidiocarp|fruit bodies]] of other [[agaric]]s—such as ''[[Lactarius]]'' and ''[[Russula]]''—although the [[host (biology)|host]] mushrooms may be decayed to the point of being difficult to recognize.

''Dendrocollybia racemosa'' fruit bodies have small pale grayish-white or grayish-brown [[pileus (mycology)|caps]] up to {{convert|1|cm|in|1|abbr=on}} wide, and thin [[stipe (mycology)|stems]] up to {{convert|6|cm|in|1|abbr=on}} long. The species is characterized by its unusual stem, which is covered with short lateral branches. The branches often produce spherical slimeheads of translucent [[conidium|conidiophores]] on their swollen tips. The conidiophores produce conidia (asexual [[spore]]s) by [[mitosis]]. Because the fungus can rely on either [[sexual reproduction|sexual]] or [[asexual reproduction|asexual]] modes of reproduction, fruit bodies sometimes have reduced or even missing caps. The unusual stems originate from black pea-sized structures called [[sclerotium|sclerotia]]. The [[teleomorph, anamorph and holomorph|anamorphic]] form of the fungus, known as ''[[Tilachlidiopsis]] racemosa'', is missing the sexual stage of its [[biological life cycle|life cycle]]. It can reproduce at relatively low temperatures, an adaptation believed to improve its ability to grow quickly and fruit on decomposing mushrooms.

==Taxonomy and phylogeny==
{{cladogram|align=left|title=
| clade=
{{clade
|style=font-size:75%;line-height:75%
| label1=
| 1={{clade
     |1={{clade
         |1={{clade
             |1={{clade
                |1=''[[Collybia tuberosa]]''
                |2=''[[Collybia cookei]]''
                }}
             |2=''[[Collybia cirrhata]]''
             }}
         |2=''[[Clitocybe dealbata]]''
         }}
     |2={{clade
        |1={{clade
            |1='''''Dendrocollybia racemosa'''''
            |2=''[[Clitocybe connata]]''
            }}
        |2=''[[Hypsizygus ulmarius]]''
        |3=''[[Lepista nuda]]''
        }}
     }}
}}
| caption=Phylogeny and relationships of ''D.&nbsp;racemosa'' and closely related fungi based on [[ribosomal DNA]] sequences<ref name="Hughes2001"/>
}}
The genus ''Dendrocollybia'' was first described in 2001, to accommodate the species previously known as ''Collybia racemosa''. Before then, the so-named [[taxon]] was considered to be one of four species of ''[[Collybia]]'', a genus which had itself been redefined and reduced in 1997, when most of its species were transferred to ''[[Gymnopus]]'' and ''[[Rhodocollybia]]''.<ref name=Antonin1997/> ''C.&nbsp;racemosa'' was originally described and named ''Agaricus racemosus'' by [[Christian Hendrik Persoon]] in 1797,<ref name="Persoon1794"/> and [[sanctioned name|sanctioned]] under that name by [[Elias Magnus Fries]] in 1821. In his ''[[Systema Mycologicum]]'', Fries [[classification (biology)|classified]] it in his "tribe" ''Collybia'' along with all other similar small, white-spored species with a convex cap and a fragile stem.<ref name="Fries1821"/> In 1873 [[Lucien Quélet]] raised Fries' tribe ''Collybia'' to generic rank.<ref name="Quelet1873"/> [[Samuel Frederick Gray]] called the species ''Mycena racemosa'' in his 1821 ''Natural Arrangement of British Plants'';<ref name=Gray1821/> both this name and Joanne Lennox's 1979 ''Microcollybia racemosa'' are considered [[synonym (biology)|synonyms]].<ref name="urlMycoBank: Dendrocollybia racemosa"/>

[[Rolf Singer]]'s fourth edition (1986) of his comprehensive ''Agaricales in Modern Taxonomy'' included ''Collybia racemosa'' in [[section (biology)|section]] ''Collybia'', in addition to the three species that currently comprise the genus ''Collybia'': ''[[Collybia tuberosa|C.&nbsp;tuberosa]]'', ''[[Collybia cirrhata|C.&nbsp;cirrhata]]'' and ''[[Collybia cookei|C.&nbsp;cookei]]''.<ref name=Singer1986/> A [[phylogenetic]] analysis of the [[internal transcribed spacer]] sequences of [[ribosomal DNA]] by Karen Hughes and colleagues showed that ''C.&nbsp;tuberosa'', ''C.&nbsp;cirrhata'' and ''C.&nbsp;cookei'' form a [[monophyletic]] group within a larger ''Lyophyllum''–''Tricholoma''–''Collybia'' [[clade]] that includes several species of ''[[Lyophyllum]]'', ''[[Tricholoma]]'', ''[[Lepista]]'', ''[[Hypsizygus]]'' and the species ''C.&nbsp;racemosa''. Hughes and colleagues could not identify a clade that included all four species of ''Collybia''. [[Restriction fragment length polymorphism]] analysis of the ribosomal DNA from the four species corroborated the results obtained from phylogenetic analysis. Based on these results, as well as differences in characteristics such as the presence of unique stem projections, fruit body [[pigment]]ation, and [[Chemical tests in mushroom identification|macrochemical reactions]], they circumscribed the new genus ''Dendrocollybia'' to contain ''C.&nbsp;racemosa''.<ref name="Hughes2001"/>

The fungus is [[common name|commonly]] known as the branched Collybia,<ref name="Arora1986"/> or the branched shanklet;<ref name="BMS-English Common Names"/> Samuel Gray referred to it as the "racemelike high-stool".<ref name=Gray1821/> The genus name ''Dendrocollybia'' is a combination of the [[Ancient Greek]] words ''dendro''-, meaning "tree", and ''collybia'', meaning "small coin". The [[specific name (botany)|specific epithet]] ''racemosa'' is from the [[Latin]] word ''racemus''—"a cluster of grapes".<ref name=Haubrich2003/>

==Description==
[[image:Dendrocollybia 512.jpg|thumb|From [[Christian Gottfried Daniel Nees von Esenbeck|Esenbeck]]'s ''Das System der Pilze und Schwämme'' (1816)]]
The [[Pileus (mycology)|cap]] of ''Dendrocollybia racemosa'' is typically between {{convert|3|to|10|mm|in|1|abbr=on}} in diameter, and depending on its stage of development, may be conic to convex, or in maturity, somewhat flattened with a slight rounded central elevation (an [[umbo (mycology)|umbo]]). The cap surface is dry and opaque, with a silky texture; its color in the center is [[wikt:fuscous|fuscous]] (a dusky brownish-gray color), but the color fades uniformly towards the margin. The margin is usually curved toward the [[lamellae (mycology)|gills]] initially; as the fruit body matures the edge may roll out somewhat, but it also has a tendency to fray or split with age. There may be shallow grooves on the cap that correspond to the position of the gills underneath, which may give the cap edge a [[wikt:crenate|crenate]] (scalloped) appearance. The [[trama (mycology)|flesh]] is very thin (less than 1&nbsp;mm thick)<ref name="urlCalifornia Fungi: Collybia racemosa"/> and fragile, lacking in color, and has no distinctive odor or taste.<ref name="Smith1937"/> The gills are relatively broad, narrowly attached to the stem ([[wikt:adnexed|adnexed]]), spaced closely together, and colored gray to grayish-[[tan (color)|tan]], somewhat darker than the cap.<ref name="Bas1995"/> There are additional gills, called ''lamellulae'', that do not extend all the way to the stem; they are interspersed between the gills and arranged in up to three series (tiers) of equal length.<ref name="urlCalifornia Fungi: Collybia racemosa"/> Occasionally, the fungus produces stems with aborted caps, or with the caps missing entirely.<ref name="Arora1986"/>
{{Rquote|left|Stem resembles the raceme of the currant-bush, from whence the berries have been plucked; branches terminated by hyaline beads which disappear.|[[Samuel Frederick Gray|S.F. Gray]]|<ref name=Gray1821/>}}
The [[Stipe (mycology)|stem]] is {{convert|4|to|6|cm|in|1|abbr=on}} long by 1&nbsp;mm thick, roughly equal in width throughout, and tapers to a long "root" which terminates in a dull black, roughly spherical [[sclerotium]].<ref name="Smith1937"/> The stem may be buried deeply in its [[substrate (biology)|substrate]].<ref name="urlCalifornia Fungi: Collybia racemosa"/> The stem surface is roughly the same color as the cap, with a fine whitish powder on the upper surface. In the lower portion, the stem is brownish, and has fine grooves that run lengthwise up and down the surface.<ref name="Bas1995"/> The lower half is covered with irregularly arranged short branch-like protuberances at right angles to the stem that measure 2–3 by 0.5&nbsp;mm. These projections are cylindrical and tapering, with ends that are covered with a slime head of [[conidium|conidia]] (fungal spores produced [[asexual reproduction|asexually]]). ''D.&nbsp;racemosa'' is the only mushroom species known that forms conidia on side branches of the stem.<ref name="Castellano2003"/> The sclerotium from which the stem arises is watery grayish and homogeneous in cross section (not divided into internal chambers), with a thin dull black outer coat, and measures {{convert|3|to|6|mm|in|abbr=on}} in diameter.<ref name="Smith1937"/> American mycologist [[Alexander H. Smith]] cautioned that novice collectors will typically miss the sclerotium the first time they find the species.<ref name="Smith1975"/> The [[edible mushroom|edibility]] of ''D.&nbsp;racemosa'' is unknown,<ref name="Smith1975"/> but as [[David Arora]] says, the fruit bodies are "much too puny and rare to be of value."<ref name="Arora1986"/>

===Microscopic characteristics===
The [[spore]]s are narrowly [[wikt:ellipsoid|ellipsoid]] to [[wikt:ovoid|ovoid]], thin-walled, [[hyaline]] (translucent), with dimensions of 4–5.5 by 2–3&nbsp;[[micrometre|µm]]. When [[staining|stained]] with [[Melzer's reagent]], the spores turn a light blue color. The [[basidia]] (the spore-bearing cells) are four-spored, measure 16–20 by 3.5–4&nbsp;µm, and taper gradually towards the base. [[Cystidia]] are not differentiated in this species. The cap surface is made of a [[pileipellis|cuticle]] of radial, somewhat [[wikt:agglutinate|agglutinated]], rather coarse [[hypha]]e that differ chiefly in size from the underlying tissue—initially 1–3&nbsp;µm in diameter, becoming 5–7&nbsp;µm wide in the underlying tissue. The hyphae are [[clamp connection|clamped]], and encrusted with shallow irregularly shaped masses that are most conspicuous in the surface cells. The gill tissue is made of hyphae that project downward from the cap and arranged in a subparallel fashion, meaning that the hyphae are mostly parallel to one another and are slightly intertwined. The hyphae are clamped, with a narrow, branched compact subhymenium (a narrow zone of small, short hyphae immediately beneath the hymenium) composed of hyphae 2–3&nbsp;µm in diameter. The [[conidia]] are 8.5–12 by 4–5&nbsp;µm, peanut-shaped, non-[[amyloid (mycology)|amyloid]] (not changing color when stained with Melzer's reagent), clamped, and produced by fragmentation of the coarse [[mycelium]].<ref name="Lennox1979"/> [[Clamp connection]]s are present in the hyphae.<ref name="Smith1975"/> Asexual spores are 10.0–15.5 by 3–4&nbsp;µm, ellipsoid to [[wikt:oblong|oblong]], non-amyloid, and contain [[granule (cell biology)|granular]] contents.<ref name="urlCalifornia Fungi: Collybia racemosa"/> The grayish color of the fruit bodies is caused by encrusted pigments (crystalline aggregates of pigment molecules, possibly [[melanin]]) that occur throughout the tissue of the stem and cap, including the gills; these pigments are absent in ''Collybia'' species.<ref name="Hughes2001"/>
[[image:Dendrocollybia racemosa 65827.jpg|thumb|upright|The lateral projections of the stem form conidia.]]

===Similar species===
In contrast to the three species of ''Collybia'',<ref name="Hughes2001"/> ''D.&nbsp;racemosa'' shows negligible reactivity to common [[chemical tests in mushroom identification|chemical tests used in mushroom identification]], including [[aniline]], [[1-Naphthol|alpha-napthol]], [[guaiacol]], [[sulfoformol]], [[phenol]], and [[phenol-aniline]].<ref name="Lennox1979"/>

The cortex (outer tissue layer) of the sclerotium can be used as a diagnostic character to distinguish between ''D.&nbsp;racemosa'' and small white specimens of ''Collybia''. The hyphae of the cortex of ''D.&nbsp;racemosa'' are "markedly angular", in comparison with ''C.&nbsp;cookei'' (rounded hyphae) and ''C.&nbsp;tuberosa'' (elongated hyphae).<ref name=Komorowska2000/> The cortical layer in ''D.&nbsp;racemosa''  has an arrangement that is known as ''textura epidermoidea''—with the hyphae arranged like a jigsaw puzzle. Heavy deposits of dark reddish-brown pigment are evident throughout the cortical tissue in or on the walls and the tips of hyphae.<ref name="Hughes2001"/> The remaining ''Collybia'' species, ''C.&nbsp;cirrhata'', does not form sclerotia.<ref name=Komorowska2000/>

===Anamorph form===
The anamorphic or [[fungi imperfecti|imperfect fungi]] are those that seem to lack a sexual stage in their [[biological life cycle|life cycle]], and typically reproduce by the process of [[mitosis]] in [[conidium|conidia]]. In some cases, the sexual stage—or [[teleomorph, anamorph and holomorph|teleomorph]] stage—is later identified, and a teleomorph-anamorph relationship is established between the species. The [[International Code of Botanical Nomenclature]] permits the recognition of two (or more) names for one and the same organism, one based on the teleomorph, the other(s) restricted to the anamorph. ''[[Tilachlidiopsis]] racemosa'' (formerly known as ''Sclerostilbum septentrionale'', described by [[Alfred Povah]] in 1932)<ref name="Povah1932"/> was shown to be the anamorphic form of ''Dendrocollybia racemosa''.<ref name="Watling1977"/> The [[synnemata]] (reproductive structures made of compact groups of erect [[conidiophore]]s) produced by ''T.&nbsp;racemosa'' always grow on the stem of ''Dendrocollybia racemosa''. The anamorph has an unusually low optimum growth temperature, between {{convert|12|and|18|°C|°F}}, within a larger growth range of {{convert|3|and|22|°C|°F}}. It is thought this is an adaptation that allows the [[mycelia|mycelium]] to grow quickly and enhance its chances of fruiting on agaric mushrooms, which are generally short-lived.<ref name="Stalpers1991"/>

==Habitat, distribution, and ecology==
[[image:Russula crassotunicata 30048.jpg|thumb|right|''Russula crassotunicata'' has been verified as a host for ''Dendrocollybia''.]]
''Dendrocollybia racemosa'' is a [[saprobic]] species, meaning it derives nutrients by breaking down dead or dying tissue. Its fruit bodies grow on the well-decayed remains of [[agaric]]s, often suspected to be ''[[Lactarius]]'' or ''[[Russula]]'',<ref name="Hughes2001"/><ref name="Arora1986"/> although the hosts' identities are often unclear due to an advanced state of decay. A 2006 study used [[Molecular phylogenetics|molecular]] analysis to confirm ''[[Russula crassotunicata]]'' as a host for ''D.&nbsp;racemosa''. This ''Russula'' has a long and persistent decay period, and, in the [[Pacific Northwest]] region of the United States where the study was conducted, provides a "nearly year-round substrate for mycosaprobic species".<ref name=Machnicki2006/> ''Dendrocollybia'' is one of four agaric genera obligately associated with growth on the fruit bodies of other fungi, the others being ''[[Squamanita]]'', ''[[Asterophora]]'', and ''Collybia''.<ref name="Hughes2001"/> ''Dendrocollybia'' is also found less commonly in deep coniferous [[forest floor|duff]], in groups or small clusters. The fungus can form sclerotia in the mummified host fruit bodies, and may also develop directly from their sclerotia in soil.<ref name="Bills2004"/> The fungus is widely distributed in [[temperate]] regions of the [[Northern Hemisphere]],<ref name="Kirk2008"/><ref name="Noordeloos1995"/> but rarely collected "probably due to its small size, camouflage color, and tendency to be immersed in its substrate."<ref name=Lennox1979/> In North America, where the distribution is restricted to the Pacific Northwest,<ref name="urlDendrocollybia racemosa (MushroomExpert.Com)"/> fruit bodies are found in the late summer to autumn, often after a heavy fruiting period for other mushrooms is over.<ref name=Smith1975/> In Europe, it is known from the United Kingdom, [[Scandinavia]],<ref name=OregonBiodiversity/> and Belgium.<ref name="Hughes2001"/> ''Dendrocollybia racemosa'' is in the Danish,<ref name="urlNERI - The Danish Red Data Book"/> Norwegian,<ref name=urlNorwegianRedList/> and British [[Regional Red List|Red List]]s.<ref name=BMSRedList/>

The saprobic behaviors of ''Collybia'' and ''Dendrocollybia'' are slightly different. In the autumn, fruit bodies of ''C.&nbsp;cirrhata'', ''C.&nbsp;cookei'' and ''C.&nbsp;tuberosa'', can be found on blackened, leathery, mummified fruit bodies of their hosts. Sometimes, these species appear to be growing in the soil (or from their sclerotium in soil or [[moss]]), but usually not in huge clusters. In these cases it is assumed that the  hosts are remnants of fruit bodies from a previous season. In all observed cases of ''D.&nbsp;racemosa'', however, the hosts have not been readily observed, suggesting that rapid digestion of the host (rather than mummification) may have taken place. Hughes and colleagues suggest that this may indicate the presence of a different enzymatic system, and a differing ability to compete with other fungi or bacteria.<ref name="Hughes2001"/>

==See also==
{{portal|Fungi}}
*[[List of Tricholomataceae genera]]

==References==
{{Reflist|colwidth=30em|refs=

<ref name="Antonin1997">{{cite journal |vauthors=Antonín V, Halling RE, Noordeloos ME |year=1997 |title=Generic concepts within the groups of ''Marasmius'' and ''Collybia'' sensu lato |journal=Mycotaxon |volume=63 |pages=359–68 |url=http://www.cybertruffle.org.uk/cyberliber/59575/0063/0359.htm}}</ref>

<ref name="Arora1986">{{cite book |author=Arora D. |title=Mushrooms Demystified: A Comprehensive Guide to the Fleshy Fungi |publisher=Ten Speed Press |location=Berkeley, California |year=1986 |page=213 |isbn=0-89815-169-4 |url=https://books.google.com/books?id=86tM01VsFG0C&pg=PA213}}</ref>

<ref name="Bas1995">{{cite book |vauthors=Bas C, Kuyper Th W, Noordeloos ME, Vellinga EC, van Os J |title=Flora Agaricina Neerlandica |volume=3 |publisher=CRC Press |location=Boca Raton, Florida |year=1995 |pages=109–10 |isbn=90-5410-616-6}}</ref>

<ref name="Bills2004">{{cite book |vauthors=Bills GF, Mueller GM, Foster MS |title=Biodiversity of Fungi: Inventory and Monitoring Methods |publisher=Elsevier Academic Press |location=Amsterdam |year=2004 |page=371 |isbn=0-12-509551-1 |url=https://books.google.com/?id=Du2bi4tgcVcC&lpg=PA371}}</ref>

<ref name="BMS-English Common Names">{{cite web |url=http://www.fungi4schools.org/Reprints/ENGLISH_NAMES.pdf |title=Recommended English Names for Fungi in the UK |format=PDF |publisher=[[British Mycological Society]]}}</ref>

<ref name=BMSRedList>{{cite report |title=The Red Data List of Threatened British Fungi |url=http://www.fieldmycology.net/Download/RDL_of_Threatened_British_Fungi.pdf |format=PDF |author=Evans S. |year=2009 |publisher=British Mycological Society |page=}}</ref>

<ref name="Castellano2003">{{cite report |title=Handbook to additional fungal species of special concern in the Northwest Forest Plan (Gen. Tech Rep. PNW-GTR-572) |vauthors=Castellano MA, Cazares E, Fondrick B, Dreisbach T |year=2003 |publisher=U.S. Department of Agriculture, Forest Service, Pacific Northwest Research Station |location=Portland, Oregon |pages=S3–S51 |url=http://www.fs.fed.us/pnw/publications/pnw_gtr572/pnw_gtr572c.pdf |format=PDF}}</ref>

<ref name="Fries1821">{{cite book |title=Systema Mycologicum |volume=1 |author=Fries EM. |year=1821 |publisher=Ex Officina Berlingiana |location=Lundin, Sweden |page=134 |url=http://www.biodiversitylibrary.org/item/25498#198 |language=Latin}}</ref>

<ref name="Gray1821">{{cite book |title=A Natural Arrangement of British Plants |author=Gray SF. |year=1821 |publisher=Baldwin, Cradock, and Joy |location=London, UK |page=620 |url=https://books.google.com/books?id=YuIYAAAAYAAJ&pg=PA620}}</ref>

<ref name=Haubrich2003>{{cite book |author=Haubrich WS. |title=Medical Meanings: A Glossary of Word Origins, |edition=2nd |publisher=American College of Physicians |location=Philadelphia, Pennsylvania |year=2003 |page=201 |isbn=1-930513-49-6 |url=https://books.google.com/books?id=8xCRO8ORO0EC&pg=PA201}}</ref>

<ref name="Hughes2001">{{cite journal |vauthors=Hughes KW, Petersen RH, Johnson JE, ((Moncalvo J-E)), Vilgalys R, Redhead SA, Thomas T, McGhee LL |year=2001 |title=Infragenic phylogeny of ''Collybia'' s. str. based on sequences of ribosomal ITS and LSU regions |journal=Mycological Research |volume=105 |issue=2 |pages=164–72 |doi=10.1017/S0953756200003415}}</ref>

<ref name="Komorowska2000">{{cite journal |author=Komorovska H. |year=2000 |title=A new diagnostic character for the genus ''Collybia'' (Agaricales) |journal=Mycotaxon |volume=75 |pages=343–6 |url=http://www.cybertruffle.org.uk/cyberliber/59575/0075/0343.htm}}</ref>

<ref name="Kirk2008">{{cite book |vauthors=Kirk PM, Cannon PF, Minter DW, Stalpers JA |title=Dictionary of the Fungi |edition=10th |publisher=CAB International |location=Wallingford, UK |year=2008 |page=198 |isbn=978-0-85199-826-8}}</ref>

<ref name="Lennox1979">{{cite journal |author=Lennox JW. |year=1979 |title=Collybioid genera in the Pacific Northwest |journal=[[Mycotaxon]] |volume=9 |issue=1 |pages=117–231 |url=http://www.cybertruffle.org.uk/cyberliber/59575/0009/001/0117.htm}}</ref>

<ref name=Machnicki2006>{{cite journal |title=''Russula crassotunicata'' identified as a host for ''Dendrocollybia racemosa'' |journal=Pacific Northwest Fungi |vauthors=Machniki N, Wright LL, Allen A, Robertson CP, Meyer C, Birkebak JM, Ammirati JF |year=2006 |volume=1 |issue=9 |pages=1–7 |url=http://openjournals.wsu.edu/index.php/pnwfungi/article/download/1024/665 |format=PDF |doi=10.2509/pnwf.2006.001.009}} {{open access}}</ref>

<ref name=Noordeloos1995>{{cite book |title=Flora Agaricina Neerlandica: Vol. 3 (Tricholomataceae II) |year=1995 |chapter=''Collybia'' |author=Noordeloos ME. |pages=106–23 |veditors=Bas C, Kuyper TW, Noordells ME, Vellinga EC |publisher=A.A. Balkema |location=Rotterdam, The Netherlands |isbn=978-90-5410-617-3}}</ref>

<ref name=OregonBiodiversity>{{cite report |title=Rare, Threatened and Endangered Species of Oregon |url=http://orbic.pdx.edu/documents/2010-rte-book.pdf |format=PDF |author=Oregon Biodiversity Information Center |year=2010 |page=91 |publisher=Institute for Natural Resources, Portland State University |location=Portland, Oregon}}</ref>

<ref name="Persoon1794">{{cite journal |author=Persoon CH. |year=1794 |title=Dispositio methodica fungorum |trans_title=Methodical arrangement of the fungi |journal=Neues Magazin für die Botanik, Römer |volume=1 |location=Zürich, Switzerland |pages=81–128 |language=Latin}}</ref>

<ref name="Povah1932">{{cite journal |author=Povah AHW. |year=1932 |title=New fungi from Isle Royale |journal=Mycologia |volume=24 |issue=2 |pages=240–4 |doi=10.2307/3753686 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0024/002/0240.htm}}</ref>

<ref name="Quelet1873">{{cite journal |author=Quélet L. |year=1873 |title=Les champignons du Jura et des Vosges. IIe Partie |trans_title=Mushrooms of the Jura and the Vosges. 2nd Part |journal=Mémoires de la [[Société d'Émulation de Montbéliard]] |volume=5 |series=II |pages=333–427 |language=French}}</ref>

<ref name=Singer1986>{{cite book |author=Singer R. |title=The Agaricales in Modern Taxonomy |edition=4th |publisher=Koeltz Scientific Books |location=Königstein im Taunus, Germany |year=1986 |page=318 |isbn=3-87429-254-1}}</ref>

<ref name="Smith1937">{{cite journal |doi=10.2307/2481096 |author=Smith AH. |year=1937 |title=Notes on agarics from the Western United States |journal=Bulletin of the Torrey Botanical Club |volume=64 |issue=7 |pages=477–87 |jstor=2481096 }}</ref>

<ref name="Smith1975">{{cite book |author=Smith AH. |title=A Field Guide to Western Mushrooms |publisher=University of Michigan Press |location=Ann Arbor, Michigan |year=1975 |pages=108–9 |isbn=0-472-85599-9}}</ref>

<ref name="Stalpers1991">{{cite journal |vauthors=Stalpers JA, Seifert KA, Samson RA |year=1991 |title=A revision of the genera ''Antromycopsis'', ''Sclerostilbum'', and ''Tilachlidiopsis'' (Hyphomycetes) |journal=Canadian Journal of Botany |volume=69 |issue=1 |pages=6–15 |url=http://rparticle.web-p.cisti.nrc.ca/rparticle/AbstractTemplateServlet?calyLang=eng&journal=cjb&volume=69&year=0&issue=1&msno=b91-002 |doi=10.1139/b91-002 }}</ref>

<ref name="urlCalifornia Fungi: Collybia racemosa">{{cite web |url=http://www.mykoweb.com/CAF/species/Dendocollybia_racemosa.html |title=''Collybia racemosa'' |vauthors=Wood M, Stevens F |work=California Fungi |accessdate=2012-07-05}}</ref>

<ref name="urlDendrocollybia racemosa (MushroomExpert.Com)">{{cite web |url=http://www.mushroomexpert.com/dendrocollybia_racemosa.html |title=''Dendrocollybia racemosa'' |author=Kuo M. |work=MushroomExpert.Com |date=March 2005 |accessdate=2010-05-12}}</ref>

<ref name="urlMycoBank: Dendrocollybia racemosa">{{cite web |url=http://www.mycobank.org/Biolomics.aspx?Table=Mycobank&MycoBankNr_=466518 |title=''Dendrocollybia racemosa'' (Pers.) R.H. Petersen & Redhead 2001 |publisher=[[MycoBank]]. International Mycological Association |accessdate=2012-07-05}}</ref>

<ref name="urlNERI - The Danish Red Data Book">{{cite web |url=http://www2.dmu.dk/1_Om_DMU/2_Tvaer-funk/3_fdc_bio/projekter/redlist/data_en.asp?ID=5656&gruppeID=90 |title=NERI&nbsp;– The Danish Red Data Book&nbsp;– ''Dendrocollybia racemosa'' (Pers.) R.H. Petersen & Redhead |vauthors=Heilmann-Clausen J |publisher=National Environmental Research Institute, Denmark |accessdate=2010-05-12}}</ref>

<ref name=urlNorwegianRedList>{{cite web |url=http://www.nhm.uio.no/botanisk/sopp/redgroup.htm |title=Red List of Threatened Fungi in Norway |publisher=The Herbarium, The Natural History Museums and Botanical Garden, University of Oslo |work=Norsk Rødliste 2006 |accessdate=2010-10-26}}</ref>

<ref name="Watling1977">{{cite journal |vauthors=Watling R, Kendrick B |year=1977 |title=Dimorphism in ''Collybia racemosa'' |journal=Michigan Botanist |volume=16 |issue=2 |pages=65–72}}</ref>

}}

==External links==
{{Commons category|Dendrocollybia racemosa}}
*{{IndexFungorum|28459}}

{{taxonbar}}
{{featured article}}

[[Category:Fungi of Europe]]
[[Category:Fungi of North America]]
[[Category:Inedible fungi]]
[[Category:Monotypic Agaricales genera]]
[[Category:Tricholomataceae]]