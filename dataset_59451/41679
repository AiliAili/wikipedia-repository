{{infobox military unit
|unit_name= Imperial Camel Corps Brigade
|image=File:Bog85 2800.jpg
|image_size = 300px
|caption=A posed photograph of Australian, British, New Zealand and Indian Camel Corps troopers
|dates=January 1916 – May 1919
|country={{flag|British Empire}}
|allegiance=
|branch= Army
|type=[[Brigade]]
|role= [[Camel cavalry|Camel-mounted Infantry]]
|size=4,150 men and 4,800 camels
|command_structure=[[Egyptian Expeditionary Force]]
|garrison=
|garrison_label=
|nickname=
|patron=
|motto=
|colors=
|colors_label=
|march=
|mascot=
|equipment=[[camel]]
|equipment_label=
|battles=[[First World War]]
* [[Senussi Campaign]]
* [[Sinai and Palestine Campaign]]
* [[Arab Revolt]]{{#tag:ref|A full list of the corps' battles and engagements is in the aftermath section.|group=nb}}
|anniversaries=
|decorations=
|battle_honours=
|disbanded=
|notable_commanders=[[Clement Leslie Smith]] [[Victoria Cross|VC]]
}}

The '''Imperial Camel Corps Brigade''' (ICCB) was a [[Camel cavalry|camel-mounted infantry]] [[brigade]] that the [[British Empire]] raised in December 1916 during the [[First World War]] for service in the [[Middle East]].

From a small beginning the unit eventually grew to a brigade of four [[battalion]]s, one battalion each from Great Britain and New Zealand and two battalions from Australia. Support troops included a [[mountain artillery]] battery, a [[machine gun]] squadron, [[Royal Engineers]], a [[field ambulance]], and an administrative train.

The ICC became part of the [[Egyptian Expeditionary Force]] (EEF) and fought in several battles and engagements, in the [[Senussi Campaign]], the [[Sinai and Palestine Campaign]], and in the [[Arab Revolt]]. The brigade suffered 246 men killed. The ICC was disbanded in May 1919 after the end of the war.

==Formation==

===Background===
[[File:Australin camel company2.jpg|thumb|Australian camel company]]

The advantages of camels in a desert environment are well known, and the [[British Army]] had raised the [[Somaliland Camel Corps]] in 1912.<ref>Clayton, p.205</ref> However the British Army forces serving in Egypt at the start of the First World War did not possess their own camel formation. The first units of what became the Imperial Camel Corps were four [[Company (military unit)|company]]-sized formations that conducted long-range patrols around the [[Suez Canal]] and the [[Sinai Desert]]. The companies were raised in Egypt in January 1916 from Australians returning from the failed [[Gallipoli Campaign]]. The Indian [[princely state]] of [[Bikaner]] supplied the first camels as the [[Bikaner Camel Corps]] already used camels. These camels were later only used as draught animals and the lighter Egyptian camel became the mount chosen for carrying troops. The camels could cover an average distance of {{convert|3|mi}} an hour, or {{convert|6|mi}} an hour trotting, while carrying a soldier, his equipment, and supplies.<ref name=camel>{{cite web | publisher = History Group of the New Zealand Ministry for Culture and Heritage | title = Cameliers and camels at war | work = New Zealand History online | accessdate=16 January 2012 | date = 30 August 2009 | url = http://www.nzhistory.net.nz/war/camel-corps }}</ref>

The camel companies consisted of a small headquarters and four [[section (military)|sections]], each of seven groups of four men. The establishment of a company was 130 men, all armed with [[Lee–Enfield]]s, the standard British [[bolt action]] rifle of the time.<ref name=camel/> However the move from patrol to a more active combat role in August 1916 led to a re-organisation. Each company added a [[machine-gun]] section of fifteen men with three [[Lewis gun]]s; the company headquarters also received extra staff. All this increased company strength to 184 men. The four companies were expected to operate as independent units that travelled by camel but then dismounted to fight as infantrymen. Following the practise of cavalry and [[mounted infantry]] units, one man of each group of four held the camels when the team was in action, which reduced a team's firepower by a quarter. However it was soon discovered that camels were not as nervous as horses when faced with artillery and rifle fire, and one man would look after twelve to sixteen camels once the troopers had dismounted.<ref name=camel/>

In March 1916 six new companies were raised from British [[yeomanry]] regiments. Then in June another four Australian companies were raised from reinforcements intended for the [[Australian Light Horse]] regiments. Reinforcements from New Zealand intended for the [[New Zealand Mounted Rifles Brigade]] formed two companies, one created in August and the second in November.<ref name=formation>{{cite web|accessdate=17 January 2012|publisher = History Group of the New Zealand Ministry for Culture and Heritage| work = New Zealand History online|title=Formation and expansion|url=http://www.nzhistory.net.nz/war/camel-corps/formation}}</ref>

===Brigade===
[[File:Hong Kong and Singapore Mountain Battery ICC.jpg|thumb|The Hong Kong and Singapore (Mountain) Battery]]

The Imperial Camel Brigade was formed on 19 December 1916,<ref name="Falls252">Falls 1930 Vol. 1 p. 252.</ref><ref name=brigade/> under the command of [[Brigadier General]] [[Clement Leslie Smith]] [[Victoria Cross|VC]].<ref>{{cite web|accessdate=17 January 2012|publisher=[[Australian War Memorial]]|title=J06062 |url=http://cas.awm.gov.au/item/J06062}}</ref> The brigade originally comprised three battalions, 1st (Australian), 2nd (British), and 3rd (Australian), plus supporting units.{{#tag:ref|The second battalion was also known as the 2nd (Imperial) Battalion.<ref name=brigade/>|group=nb}} Each of the battalions had an authorised strength of 770 men and 922 camels. A battalion comprised four companies and a headquarters. The 4th (ANZAC) Battalion was raised in May 1917, but instead of increasing the brigade fighting strength, it was decided one battalion would always be resting and refitting, while three battalions served at the front.<ref name=brigade/><ref name=awarm/>

To complete the brigade structure and supply added firepower, the brigade received some other units: the 265th (Camel) Machine Gun Squadron, with eight [[Vickers machine gun]]s, and the Hong Kong and Singapore (Mountain) Battery, armed with six [[BL 2.75 inch Mountain Gun]]s. Despite their title, the battery was formed by men from the [[British Indian Army]].<ref>{{cite web|accessdate=24 January 2012|publisher = History Group of the New Zealand Ministry for Culture and Heritage| work = New Zealand History online|title=Camel artillery|url=http://www.nzhistory.net.nz/media/photo/camel-artillery-ready-fire}}</ref> The brigade also had its own [[Royal Engineers]] (the 10th (Camel) Field Troop), a [[Royal Signals|signal section]], the Australian (Camel) [[Field Ambulance]], and the 97th Australian Dental Unit, which with only four men was the brigade's smallest unit. The brigade included the ICC Mobile Veterinary Section, and the brigade's logistic units were the ICC Brigade Ammunition Column and the ICC Brigade Train, which carried enough supplies for five days. The total brigade strength was around 4,150 men and 4,800 camels.<ref name=brigade/><ref name=hong>{{cite web|accessdate=17 January 2012|publisher=New Zealand history|title=Camel artillery ready to fire|url=http://www.nzhistory.net.nz/media/photo/camel-artillery-ready-fire}}</ref>

==Operational history==

===1916===

==== Battalions ====
[[File:Camel corps at Magdhaba.jpg|thumb|Imperial Camel Corps at the [[Battle of Magdhaba]]]]
In March 1916, after two months of training, the first camel patrols left their depot at [[Abassi, Egypt|Abassi]] on the outskirts of Cairo to patrol the [[Libyan Desert]]. In 1915 the [[Senussi]] had attacked British and Egyptian outposts along the [[Suez canal]] and the Mediterranean coast. The resulting [[Senussi Campaign]] was largely over by then, but the patrols were to show the Senussi that the British were watching them, and to protect the border areas.<ref name=formation/>

Around the same time long-range patrols, each of about thirty men, went into the south and south-east of the Sinai desert to detect any Ottoman incursion into the area. When the patrols discovered Ottoman outposts, the brigade organized a company-strength raid against the outposts. The ICC undertook similar patrols in the north to protect the rail and water lines, which were vital for any British attack.<ref name=companies/>

==== Brigade ====
The [[Egyptian Expeditionary Force]] (EEF) went over to the offensive in the [[Sinai Desert]] in August, winning the [[Battle of Romani]]. In support of these operations in December the brigade moved into the Sinai; their first large battle came during the [[Battle of Magdhaba]] on 23 December, two days after the brigade was formed.<ref name="Falls252"/><ref name=companies>{{cite web|accessdate=17 January 2011|publisher = History Group of the New Zealand Ministry for Culture and Heritage| work = New Zealand History online|title=New Zealand companies|url=http://www.nzhistory.net.nz/war/camel-corps/nz-companies}}</ref>

===1917===

On 9 January 1917 the ICC was involved in another victory during the [[Battle of Rafa]], which forced the Ottomans to withdraw the Sinai outposts towards [[Gaza City|Gaza]]. This also reduced the need for independent camel patrols across the Sinai; in May the EEF consolidated the now-surplus companies into a new unit, the 4th (ANZAC) Battalion.<ref name=companies/>

The intensity of operations grew and the ICC were next involved in the capture of the Turkish force at [[Raid on Bir el Hassana|Bir el Hassana]],<ref group=nb>The 2nd Battalion of the ICC, together with the Hongkong and Singapore Mountain Battery marched some 30 miles from El Arish, surprising the Turkish forces at Bir el Hassana, who surrendered without resistance. Some local Bedouin fired on the British, who suffered one casualty, a soldier who was shot in the ankle. Because he could no longer ride, the British evacuated him by aeroplane, in the first recorded case of [[Aeromedical Evacuation|aeromedical evacuation]].</ref> the defeats during the [[First Battle of Gaza]] in March, and the [[Second Battle of Gaza]] in April and  a raid on the Sana [[redoubt]] in August. They then had a break to refit. Subsequently, they participated in the victories in the [[Battle of Beersheba (1917)|Battle of Beersheba]], the [[Third Battle of Gaza]], and at  [[Battle of Mughar Ridge]] during October and November. By the end of the year the advance had crossed the Sinai and entered Palestine.<ref name=companies/>

===1918===

[[File:Camels on pontoon.jpg|thumb|ICC troops crossing the [[River Jordan]] to attack [[Amman]] April 1918]]
Early in 1918, the ICC moved to the area of the [[Jordan Valley (Middle East)|Jordan valley]] and took part in the [[First Transjordan attack on Amman (1918)|attack]]  in March and April. The [[First Battle of Amman (1918)|First Battle of Amman]] was unsuccessful; after three days of battle the British were unable to break through the Ottoman defences around the city and had to withdraw. The 4th (Anzac) Battalion did succeed in capturing Hill 3039 overlooking the city and managed to hold out for twenty-four hours in the face of artillery and infantry attacks, until ordered to withdraw.<ref name=companies/>

During the [[Second Transjordan attack on Shunet Nimrin and Es Salt]], the camel brigade were assigned the western defence of the Jordan River ford at Umm esh Shert defending the left flank of the [[4th Light Horse Brigade]]. The camel brigade was unable to support the light horsemen, which were attacked on the left flank and forced to withdraw.<ref>Falls 1930 Vol. 2 pp. 368, 375</ref>

When the EEF advanced out of the Sinai and into Palestine, the change in terrain led to the disbandment of the ICC. In June 1918, the Australian troops were used to form the [[14th Light Horse Regiment (Australia)|14th]] and [[15th Light Horse Regiment (Australia)|15th Light Horse Regiments]]. The New Zealand troops formed the 2nd New Zealand Machine Gun Squadron. All three units became part of the [[5th Light Horse Brigade]]. The six British companies remained part of the ICC for a while longer. Two of them fought with [[T.E. Lawrence]] in the [[Arab Revolt]], and in July 1918 carried out operations sabotaging the [[Hejaz]] railway line. However, no reinforcements were assigned and the six remaining companies were reduced in strength to two before they were eventually disbanded in May 1919.<ref name=end/>

==Aftermath==
[[File:Statues in Victoria Embankment Gardens - geograph.org.uk - 1729996.jpg|thumb|[[Imperial Camel Corps Memorial]] in London]]
Over two years of service cost the ICC 240 deaths: 106 British, 84 Australians, 41 New Zealanders, and nine men from India.<ref name=end/><ref name=london/> A [[Imperial Camel Corps Memorial|memorial to the Imperial Camel Corps]] was unveiled on the 22 July 1921, on the [[Thames Embankment]] in London. On one side it is inscribed with the names of all the members of the corps who died during the war, while on the front is the sentiment;<blockquote> ''To the Glorious and Immortal Memory of the Officers, N.C.O's and Men of the Imperial Camel Corps&nbsp;– British, Australian, New Zealand, Indian&nbsp;– who fell in action or died of wounds and disease in Egypt, Sinai, and Palestine, 1916, 1917, 1918.''<ref name=end>{{cite web|publisher=New Zealand History|title=End|url=http://www.nzhistory.net.nz/war/camel-corps/end|accessdate=16 January 2012}}</ref></blockquote>

The monument also lists all the battles and engagements fought by the corps;
* 1916: [[Battle of Romani|Romani]], [[Senussi Campaign|Baharia]], [[Senussi Campaign|Mazar]], [[Senussi Campaign|Dakhla]], [[Senussi Campaign|Maghara]], [[Battle of Magdhaba|El. Arish]], [[Battle of Magdhaba|Maghdaba]]
* 1917: [[Battle of Rafa|Rafa]], [[Raid on Bir el Hassana|Hassana]], [[First Battle of Gaza|Gaza 1]], [[Second Battle of Gaza|Gaza 2]], [[Second Battle of Gaza|Sana Redoubt]], [[Battle of Beersheba (1917)|Beersheba]], Bir Khu Weilfe, Hill 265
* 1918: [[First Transjordan attack on Amman (1918)|Amman]], [[Occupation of the Jordan Valley (1918)|Jordan Valley]], [[Arab Revolt|Mudawar (Hedjaz)]]<ref name=london>{{cite web|accessdate=17 January 2012|publisher=London Remembers|title=Statue: Imperial Camel Corps|url=http://www.londonremembers.com/memorials/imperial-camel-corps}}</ref>

==Order of battle==
<small>The strength of the brigade/corps in the field was around 3,380 men and 3,880 camels, with one battalion resting.</small>
* Brigade Headquarters (40 men)
* 1st (Australian) Battalion (770 men)
* 2nd (British) Camel Battalion (770 men)
* 3rd (Australian) Camel Battalion (770 men)
* 4th (ANZAC) Camel Battalion (770 men)
* Hong Kong and Singapore (Mountain) Battery (255 men)
* 265th (Camel) Machine Gun Squadron (115 men)
* 10th (Camel) Field Troop, Royal Engineers (71 men)
* Signal Section, ICC Brigade (30 men)
* Australian (Camel) Field Ambulance (185 men)
* 97th Australian Dental Unit (4 men)
* ICC Mobile Veterinary Section (42 men)
* ICC Brigade Ammunition Column  (75 men)
* ICC Brigade Train (245 men) <ref name=brigade>{{cite web|accessdate=16 January 2012|publisher = History Group of the New Zealand Ministry for Culture and Heritage| work = New Zealand History online|title=Organisation|url=http://www.nzhistory.net.nz/war/camel-corps/organisation}}</ref><ref name=awarm>{{cite web|accessdate=16 January 2012|publisher=[[Australian War Memorial]]|title=Imperial Camel Corps |work=First World War, 1914–1918 units|url=http://www.awm.gov.au/units/unit_13624.asp}}</ref>

==Notes==
{{reflist|group=nb}}

==References==
{{Reflist|2}}

==Works cited==
* {{cite book|title=Khaki and Blue: Military and Police in British Colonial Africa|volume=Monographs in International Studies, Volume 51|series=Africa series|first1=Anthony|last1=Clayton|first2=David|last2=Killingray|publisher=Ohio University Center for International Studies|year=1989|isbn=978-0-89680-147-9}}
* {{cite book |title=Military Operations Egypt & Palestine from the Outbreak of War with Germany to June 1917 |last=Falls |first=Cyril |series=Official History of the Great War Based on Official Documents by Direction of the Historical Section of the Committee of Imperial Defence |author2=G. MacMunn |year=1930 |volume=1 |publisher=HM Stationery Office |location=London |oclc=610273484}}

==Further reading==
;War diaries
* AWM4/11/1/1: February 1917 unit diary
* AWM4/11/10/1: 1st Company, ICC.
* AWM4/11/11/1: 2nd Company, ICC.
* AWM4/11/2/1-11/2/16: HQ Imperial Camel Brigade.
* AWM4/11/6/1-11/6/4: 1st Battalion, ICC.
* AWM4/11/8/1-11/8/5: 3rd Battalion, ICC.
* AWM4/11/9/1-11/9/5: 4th Battalion, ICC.

;Books
* {{cite book|first=G.F.|last=Langley|title=Sand, Sweat and Camels: The Australian Companies of the Imperial Camel Corps|publisher=Lowden Publishing  |year=1976|isbn=978-0-909706-51-7}}
* {{cite book|title=Imperial Camel Corps|first=Geoffrey|last=Inchbald|publisher=Johnson|year=1970|isbn=0-85307-094-6}}
* {{cite book|year=1962|title=Seven Pillars of Wisdom|first=T.E.|last=Lawrence|authorlink=T. E. Lawrence|publisher=Penguin Books |isbn=0-14-001696-1}}

==External links==
* [http://www.iwm.org.uk/collections/item/object/100022717 Imperial War Museum information film clip]
* [http://www.chakoten.dk/imperial_camel_corps.html Danish Military History Society article (in Danish)]
* [https://www.google.com/maps/views/view/106069055152830469003/c0c95c531cc3c1e2/ 360 Panorama of Imperial Camel Corps Memorial]

[[Category:Military units and formations of the British Army in World War I]]
[[Category:Military units and formations established in 1916]]
[[Category:Military units and formations disestablished in 1919]]
[[Category:Camel cavalry]]
[[Category:ANZAC units and formations]]