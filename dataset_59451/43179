{|{{Infobox aircraft begin
  |name = Marinens Flyvebaatfabrikk M.F.2
  |image =
  |caption = M.F.2
}}{{Infobox aircraft type
  |type = Maritime reconnaissance, bomber
  |manufacturer = [[Marinens Flyvebaatfabrikk]]<ref name=henriksen/>
  |designer = [[Halfdan Gyth Dehli]]<ref name=henriksen/>
  |first flight =
  |introduced = [[1916 in aviation|1916]]
  |retired = 2 October 1924
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Norwegian Navy Air Service]]
  |more users =
  |produced = 1916&ndash;1918
  |number built = 3
  |unit cost =
  |developed from = [[Marinens Flyvebaatfabrikk M.F.1|M.F.1]]
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Marinens Flyvebaatfabrikk M.F.2''' was a two-seat military [[biplane]] [[floatplane]] produced by [[Marinens Flyvebaatfabrikk]] in 1915 and 1916. It was designed by naval captain [[Halfdan Gyth Dehli]], based on former models by French designer [[Maurice Farman]] and improvements by [[Einar Sem-Jacobsen]] of the [[Norwegian Army Air Service]]'s aircraft factory [[Kjeller Flyfabrikk]].<ref name=henriksen/>

==Background==
The M.F.2 was the second type of aircraft manufactured by Marinens Flyvebaatfabrikk, and was financed by the extraordinary appropriations to the [[Norwegian Armed Forces]] after the outbreak of the [[World War I|First World War]].<ref name=hafsten20>Hafsten 2003: 20</ref>

==Production==
Two examples of the M.F.2 were delivered from the Marinens Flyvebaatfabrikk in the summer of 1916, with the numerals F.10 and F.12. A third example, F.10 (II), was delivered in March 1918, replacing the original F.10, which had crashed at [[Karljohansvern]] naval base on 21 April 1917 after a total of 22 hours flying time.<ref name=hafsten20/><ref name=hafsten209>Hafsten 2003: 209</ref>

==Service==
The M.F.2s were employed for neutrality protection patrols, in addition to being used for student pilots taking their [[Pilot licensing and certification|certificates]]. The M.F.2s were based at the [[Royal Norwegian Navy]]'s main naval base at Karljohansvern in [[Horten]] until July 1918, when F.10 (II) and F.12 were redeployed to the newly established naval station at [[Kristiansand]].<ref>Hafsten 2003: 20&ndash;21</ref> The M.F.2s were part of the First Aerial Group prior to the move to Kristiansand.<ref>Hafsten 2003: 21</ref> At Kristiansand the two M.F.2s served together with three [[Sopwith Baby]] seaplane fighters, and were housed in two aircraft sheds.<ref>Hafsten 2003: 38</ref>

The M.F.2 type served with the Royal Norwegian Navy Air Service until 2 October 1924, when F.12 was written off after 95 hours and 30 minutes of flying time. F.10 (II) had been wrecked during take off from Kristiansand naval air station on 22 July 1919.<ref name=hafsten209/>

The engine of the M.F.2 was a 150 [[horsepower|hp]] [[Sunbeam Motor Car Company|Sunbeam]] [[V8 engine|V8]]. The aircraft was equipped with a radio transmitter. It was armed with a fixed [[Madsen machine gun]] and could carry a bomb load of 2 x 50&nbsp;kg, in the form of two small [[Aerial bomb|bombs]] or mines.<ref name=henriksen>Henriksen 1994: 64&ndash;66</ref><ref name=hafsten20/> The M.F.2 was the first aircraft operated by the [[Royal Norwegian Navy Air Service]] that had permanent armament, or could carry a bomb load. The preceding [[Etrich Taube|Rumpler Etrich Taube]], [[Farman MF.7|Maurice Farman MF.7]] and Marinens Flyvebaatfabrikk M.F.1 types had only been able to carry [[carbine]]s and pistols.<ref>Hafsten 2003: 207&ndash;209</ref>

The wings of the M.F.2 were shorter than those of the M.F.1, and the tail section of the M.F.2 was completely different from its predecessor and included a new type of altitude and side rudder.<ref name=hafsten20/>

==Specifications==
{{aircraft specifications
|jet or prop?=prop
|plane or copter?=plane
|include 'armament' field?=yes
|include 'capacity' field?=no
|switch order of units?=no
<!-- please include units. if something doesn't apply, leave it blank. -->
|ref=''Marinens Flygevåpen 1912&ndash;1944.''<ref name=hafsten209/>
|crew=Two
|capacity=
|length main=9.73 m
|length alt=31 ft 11 in
|span main=15.6 metres
|span alt=51 ft 2 in
|height main=3.59 metres
|height alt=11 ft 9¼ in
|area main=
|area alt=
|empty weight main=1,050 kg
|empty weight alt=2,310 lb
|loaded weight main=1,390 kg
|loaded weight alt=3,058 lb
|useful load main=
|useful load alt=
|max takeoff weight main=
|max takeoff weight alt=
|engine (prop)=[[Sunbeam 150hp]]
|type of prop=
|number of props=1
|power main=150&ndash;160 hp
|power alt=112–119 kW
|max speed main=110 km/h
|max speed alt=59 knots, 68 mph
|cruise speed main=100 km/h
|cruise speed alt=54 knots, 62 mph
|range main=350 km
|range alt=189 [[nautical mile|nmi]], 217 mi
|ceiling main=
|ceiling alt=
|climb rate main=1.2 m/s
|climb rate alt=233 ft/m
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|armament=
* 1 × [[Madsen machine gun]]
* 2 × [[Aerial bomb|bombs]] or mines
}}

==References==
{{Reflist}}

==Bibliography==
* {{Cite book|last=Hafsten |first=Bjørn |first2=Tom |last2=Arheim |title=Marinens Flygevåpen 1912&ndash;1944 |year=2003 |publisher=TankeStreken AS |location=Oslo |isbn=82-993535-1-3 |language=Norwegian }}
* {{Cite book|title=Fra opptakt til nederlag |series=Luftforsvarets historie |volume=1 |first=Vera |last=Henriksen |authorlink=Vera Henriksen |year=1994 |language=Norwegian |publisher=Aschehoug |location=Oslo |isbn=82-03-22068--1}}

{{Marinens Flyvebaatfabrikk aircraft}}

{{Use dmy dates|date=September 2010}}

[[Category:Norwegian military reconnaissance aircraft 1910–1919]]
[[Category:Marinens Flyvebaatfabrikk aircraft|MF02]]
[[Category:Single-engined pusher aircraft]]
[[Category:Floatplanes]]
[[Category:Biplanes]]