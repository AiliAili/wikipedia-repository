[[File:IMGC Heraklion.jpg|thumb|right| Members of the IMGS attending a session of the 24th International Mouse Genome Conference, held near Heraklion, Crete]]
The '''International Mammalian Genome Society''' ('''IMGS''') is a [[Learned society|professional scientific organization]] that promotes and coordinates the [[genetics|genetic]] and [[genomic]] study of mammals. It has a [[scientific journal]], ''[[Mammalian Genome]]'', and organizes an annual international [[Academic conference|meeting]], the International Mammalian Genome Conference (IMGC).

== History and governance ==
[[File:Fatmouse.jpg|thumb|right|The gene affected by the ''[[ob/ob mouse|obese]]'' mutation (left) was identified by [[Jeffrey M. Friedman]], a founding member of the IMGS.<ref name="pmid1794042"/>]]The society was formed in 1991 from informal discussions within the mouse genetics scientific community. It had 48 founding members, including [[Gail R. Martin]], [[Eric Lander]], [[Tsui Lap-chee]] and [[Shirley M. Tilghman]].<ref name="pmid1794042"/> It has three stated goals:<ref name="pmid1794042">{{cite journal |title=The International Mammalian Genome Society |journal=Mamm. Genome |volume=1 |issue=1 |pages=2–4 |year=1991 |pmid=1794042 |doi= 10.1007/BF00350841|url=}}</ref> 
* To facilitate the creation of [[databases]] of genetic information,
* Organize meetings for mammalian geneticists to share expertise and supervise the organization of genetic data (for example, into [[genetic map]]s and [[reference genome]]s), and
* To coordinate the mapping and sequencing of [[model organism]]s with [[Human Genome Project|similar efforts]] focusing on the [[human genome]].

The society has served as an organizing body for a number of initiatives in mouse genetics. It coordinated the formation of the [[International Mouse Mutagenesis Consortium]], an effort to assign a function to every gene in the mammalian genome,<ref name="pmid11233449">{{cite journal |author=Nadeau JH |title=Sequence interpretation. Functional annotation of mouse genome sequences |journal=Science |volume=291 |issue=5507 |pages=1251–5 |date=February 2001 |pmid=11233449 |doi= 10.1126/science.1058244|url= |name-list-format=vanc |author2=Balling R |author3=Barsh G |display-authors=3 |last4=Beier |first4=D |last5=Brown |first5=SD |last6=Bucan |first6=M |last7=Camper |first7=S |last8=Carlson |first8=G |last9=Copeland |first9=N}}</ref><ref name="pmid11431680">{{cite journal |author=Jackson IJ |title=Mouse mutagenesis on target |journal=Nat. Genet. |volume=28 |issue=3 |pages=198–200 |date=July 2001 |pmid=11431680 |doi=10.1038/90017 |url=}}</ref> oversaw activities of chromosome committees and advised on [[biological database]] developments.<ref name="pmid1351872"/> Membership of the International Mammalian Genome Society is open to all people interested in mammalian genetics. Members pay yearly dues, for which they receive [[voting rights]] and access to ''Mammalian Genome''.<ref name="bylaws"/> The society is governed by a secretariat of three presidents (vice, current and past-president) and elected officers.<ref name="pmid1351872"/><ref name="officers">{{Cite web | title =IMGS Officers | work = | publisher = International Mammalian Genome Society | date = December 1, 2010 | url = http://www.imgs.org/?run=home.officers | doi = | accessdate =December 291, 2010}}</ref> Each officer is elected by a ballot of members for a period of two years;<ref name="bylaws">{{Cite web | title =Bylaws of the International Mammalian Genome Society | work = | publisher = International Mammalian Genome Society | date = December 1, 2010 | url = http://www.imgs.org/?run=home.bylaws | doi = | accessdate =December 291, 2010}}</ref> the presidents serve for a consecutive six-year period, two in each position.<ref name="bylaws"/> The current president (2015–2016) is [[Teresa Gunn]], associate professor at the [[McLaughlin Research Institute]].<ref name="officers"/> From 2009 the [[graduate student]] or [[post-doctoral fellow]] who wins the Verne Chapman Young Scientist Award at the annual meeting also joins the secretariat for the following two years.<ref name="officers"/><ref name="pmid20496149"/> Secretariat elections are organized by a Nomination and Election Committee consisting of six active society members.<ref name="bylaws"/> The society also maintains an administrative office at the [[University of North Carolina at Chapel Hill]].

== Publications ==
{{main|Mammalian Genome}}
[[File:2000px Jan Klein by Dorothea Bylica Academic bulletin Academy Sciences Czech Republic.JPEG|thumb|right|[[Jan Klein]], a founding editor of ''Mammalian Genome'']]
The IMGS has an official [[peer review]]ed journal, ''Mammalian Genome'', which was launched with the society in 1991 but published and managed by [[Springer Science+Business Media|Springer]].<ref name="pmid1794042" /><ref name="bylaws"/><ref name="MG"/> Three of the founding members of the society, [[Lee M. Silver]], [[Jan Klein]] and [[Joseph H. Nadeau]], served as the journal's first editors.<ref name="MG">{{cite journal |author= |title=Mammalian Genome |journal=Mamm. Genome |volume=1 |issue=1 |pages=1–1 |year=1991 |pmid=|doi= 10.1007/BF00350840|url=http://www.springerlink.com/content/0938-8990/1/1/ |last1= Silver |first1= Lee M. |last2= Nadeau |first2= Joe |last3= Klein |first3= Jan}}</ref>  ''Mammalian Genome'' currently accepts both original and review articles on "experimental, theoretical, and technical aspects of genomics and genetics in mouse, human, and other species." <ref name=description>{{cite web |url=http://www.springer.com/life+sci/cell+biology/journal/335 |title=Mammalian Genome: Description |accessdate=30 October 2010|publisher=Springer }}</ref> Any changes in editors and [[editorial board]] members are by mutual agreement of the IMGC Secretariat, Springer and the remaining editors.<ref name="bylaws"/>

==Annual meeting==
The IMGS holds an annual meeting, the International Mouse Genome Conference (IMGC), that is attended by scientists from around the world. Prior to the formation of the society the mouse genetics community held a number of annual workshops, which the society adopted at the 4th workshop in [[Lunteren]], Netherlands.<ref name="pmid1351872"/> The location for the meeting has rotated between Europe, the USA, and Japan. Recent IMGCs have hosted satellite events, including student symposia, [[bioinformatic]] workshops and mentoring breakfasts.<ref name="pmid20496149"/> In addition to the scientific program, the IMGC hosts the [[annual general meeting|annual business meeting]] of the society, the annual secretariat meeting and often includes cultural or social events characteristic of host city or country.<ref name="bylaws"/><ref name="pmid20496149"/><ref name="pmid18949515"/>

In 1997, at the 11th Annual meeting, the IMGS inaugurated its first Verne Chapman Memorial Lecture. The annual seminar was named in honor of [[Verne M. Chapman]], a former Director of Scientific Affairs at [[Roswell Park Cancer Institute]] and a founding member of the society.<ref name="pmid9545489"/> A number of awards are also presented at each meeting, including the Verne Chapman Young Scientist Award.<ref name="pmid20496149"/><ref name="pmid19936831"/><ref name="pmid17436036"/>

From 1999 meeting, the conference program and [[Abstract (summary)|abstracts]] are published online.<ref name="calender"/>

{| class="wikitable sortable"
|-
! Year !! Venue !! Country !! Verne Chapman Lecturer !! Citation
|-
| 2015 || [[Yokohama]] || Japan || [[Svante Pääbo]] || 
|-
| 2014 || [[Bar Harbor]] || USA|| [[Bruce Beutler]] || <ref name="calender">{{Cite web  | title =IMGS Meetings  | work =  | publisher = International Mammalian Genome Society | date = December 1, 2010 | url = http://www.imgs.org/?run=home.history | doi =  | accessdate =August 20, 2014}}</ref>
|-
| 2013 || [[Salamanca]] || Spain || [[Nancy Jenkins]] || <ref name="calender"/>
|-
| 2012 || [[St. Pete Beach]] || USA|| [[Eva Eicher]] || <ref name="calender"/>
|-
| 2011 || [[Washington, D.C.]] || USA || [[William Dove]] ||<ref name="calender"/>
|-
| 2010 || [[Heraklion]] || Greece || [[Steve D. M. Brown]] ||<ref name="calender"/>
|-
| 2009 || [[La Jolla, CA|La Jolla]] || USA  || [[Christopher Goodnow]] || <ref name="pmid20496149">{{cite journal |vauthors=Eisener-Dorman AF, Didion JP, Santos C, Calaway JD |title=The 23rd International Mammalian Genome Conference meeting report |journal=Mamm. Genome |volume=21 |issue=5–6 |pages=217–223 |date=June 2010 |pmid=20496149 |pmc=2890982 |doi=10.1007/s00335-010-9265-7 |url=}}</ref>
|-
| 2008|| [[Prague]] || Czech Republic  || [[Philip Avner]] ||<ref name="pmid19936831">{{cite journal |vauthors=Quwailid MM, Parsons MJ, Denny P |title=Report on the 22nd International Mammalian Genome Conference |journal=Mamm Genome |volume= |issue= |pages= |date=November 2009 |pmid=19936831 |doi=10.1007/s00335-009-9233-2 |url=}}</ref>
|-
| 2007 || [[Kyoto]] || Japan  || [[Hiroaki Kitano]] ||<ref name="pmid18949515">{{cite journal |vauthors=Amos-Landgraf J, Schalkwyk LC |title=The 21st International Mammalian Genome Conference meeting report |journal=Mamm. Genome |volume=19 |issue=9 |pages=618–622 |date=September 2008 |pmid=18949515 |doi=10.1007/s00335-008-9147-4 |url=}}</ref>
|-
| 2006 || [[Charleston, SC|Charleston]] || USA  || [[James E. Womack|James Womack]] || <ref name="pmid17436036">{{cite journal |vauthors=Hurd EA, Martin DM |title=The 20th International Mammalian Genome Conference meeting report |journal=Mamm. Genome |volume=18 |issue=3 |pages=145–153 |date=March 2007 |pmid=17436036 |doi=10.1007/s00335-007-9005-9 |url=}}</ref>
|-
| 2005 || [[Strasbourg]] || France  || [[Yoshihide Hayashizaki]] || <ref name="pmid16688525">{{cite journal |vauthors=Solomon NM, Dackor J, Camper SA |title=19th International Mouse Genome Conference |journal=Mamm. Genome |volume=17 |issue=5 |pages=355–362 |date=May 2006 |pmid=16688525 |doi=10.1007/s00335-005-1900-3 |url=}}</ref>
|-
| 2004 || [[Seattle]] || USA  || 	[[Richard Palmiter]] || <ref name="pmid16151691">{{cite journal |author=Lossie AC |title=18th international mouse genome conference |journal=Mamm. Genome |volume=16 |issue=7 |pages=471–5 |date=July 2005 |pmid=16151691 |doi=10.1007/s00335-005-0026-y |url= |name-list-format=vanc |author2=Meehan TP |author3=Castillo A |display-authors=3 |last4=Zheng |first4=Lihua |last5=Weiser |first5=Keith C. |last6=Strivens |first6=Mark A. |last7=Justice |first7=Monica J.}}</ref>
|- 
| 2003 || [[Braunschweig]] || Germany  || [[Kenneth Paigen]] || <ref name="pmid15366370">{{cite journal |vauthors=Smyth I, Van Agtmael T, Jackson IJ |title=17th International Mouse Genome Conference |journal=Mamm. Genome |volume=15 |issue=7 |pages=509–14 |date=July 2004 |pmid=15366370 |doi= 10.1007/s00335-004-4001-9|url=}}</ref>
|-
| 2002 || [[San Antonio, TX|San Antonio]] || USA  || [[Miriam Meisler]] ||<ref name="pmid14629109">{{cite journal |author=Loftus SK |title=Meeting report: 16th International Mouse Genome Conference |journal=Mamm. Genome |volume=14 |issue=9 |pages=593–600 |date=September 2003 |pmid=14629109 |doi= 10.1007/s00335-003-4003-z|url=}}</ref>
|-
| 2001 || [[Edinburgh]] || UK  || [[Jean-Louis Guenet]] || <ref name="pmid12016509">{{cite journal |vauthors=Cabin DE, Olson LE, Reeves RH |title=Meeting Report: 15th International Mouse Genome Conference |journal=Mamm. Genome |volume=13 |issue=5 |pages=229–33 |date=May 2002 |pmid=12016509 |doi=10.1007/s0033502-4001-2 |url=}}</ref>
|-
| 2000 || [[Narita, Chiba|Narita]] || Japan  || [[Mary F. Lyon]] ||<ref name="pmid11309847">{{cite journal |vauthors=Strunk KE, Roberts RB |title=The 14th annual International Mammalian Genome Society Conference: a glimpse into the future of murine functional genomics |journal=Genesis |volume=29 |issue=4 |pages=153–155 |date=April 2001 |pmid=11309847 |doi= 10.1002/gene.1018|url=}}</ref>
|-
| 1999 || [[Philadelphia]] || USA  || [[Janet Rossant]] || <ref name="pmid10928795">{{cite journal |vauthors=Sprunger L, Hunter K |title=The 13th Annual International Mammalian Genome Society Conference: a meeting report |journal=Mamm. Genome |volume=11 |issue=6 |pages=413–6 |date=June 2000 |pmid=10928795 |doi= 10.1007/s003350010079|url=}}</ref>
|-
| 1998 || [[Garmisch-Partenkirchen]] || Germany || [[Oliver Smithies]] ||<ref name="calender"/>
|-
| 1997 || [[St. Pete Beach]] || USA  || [[Harold Varmus]] || <ref name="pmid9545489">{{cite journal |vauthors=Davis AP, Justice MJ |title=Meeting report: 11th International Mouse Genome Conference |journal=Mamm. Genome |volume=9 |issue=5 |pages=345–8 |date=May 1998 |pmid=9545489 |doi= 10.1007/s003359900767|url=}}</ref>
|-
| 1996 || [[Paris]] || France  || n/a || <ref name="pmid9195987">{{cite journal |vauthors=Camper SA, Meisler MH |title=Meeting report: 10th International Mouse Genome Conference |journal=Mamm. Genome |volume=8 |issue=7 |pages=461–3 |date=July 1997 |pmid=9195987 |doi= 10.1007/s003359900476|url=}}</ref>
|-
| 1995 || [[Ann Arbor]] || USA || n/a ||<ref name="calender"/>
|-
| 1994 || [[London]] || UK || n/a ||<ref name="calender"/>
|-
| 1993 || [[Hamanako]] || Japan || n/a ||<ref name="calender"/>
|-
| 1992 || [[Buffalo NY]] || USA || n/a ||<ref name="calender"/>
|-
| 1991 || [[Lunteren]] || Netherlands  || n/a || <ref name="pmid1351872">{{cite journal |author=Brown SD |title=The Mouse Genome Project and human genetics. A report from the 5th International Mouse Genome Mapping Workshop, Lunteren, Holland |journal=Genomics |volume=13 |issue=2 |pages=490–2 |date=June 1992 |pmid=1351872 |doi= 10.1016/0888-7543(92)90283-X|url=}}</ref>
|-
| 1990 || [[Annapolis]] || USA || n/a || <ref name="pmid1686841">{{cite journal |title=Fourth International Workshop on Mouse Genome Mapping. November 4–8, 1990, Annapolis, Maryland. Abstracts |journal=Mamm. Genome |volume=1 Spec No |issue= |pages=S516–32 |year=1991 |pmid=1686841 |doi=10.1007/BF00656505 |url=}}</ref> 
|-
| 1989 || [[Oxford]] || UK || n/a ||<ref name="pmid1686841" /> 
|-
| 1988 || [[Bar Harbor]] || USA || n/a ||<ref name="calender"/>
|-
| 1987 || Paris || France || n/a ||<ref name="calender"/>
|}

==See also==
*[[List of genetics research organizations]]
*[[Human Genome Organisation]]
*[[Genetics Society of America]]

==References==
{{reflist|2}}

==External links==
* [http://imgs.org/ International Mammalian Genome Society] website.
* [http://imgconference.com/ International Mouse Genome Conference 2012] website.
* ''[http://www.springerlink.com/content/0938-8990 Mammalian Genome]'' website.
* [http://www.facebook.com/mammalian.genome Official IMGS Facebook Page]
{{Authority control}}

[[Category:Biology societies]]
[[Category:Organizations established in 1991]]
[[Category:Genetics organizations]]
[[Category:Mammalogy]]