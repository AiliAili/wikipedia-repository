{{Infobox Journal
| cover	=	PacSci.gif
| discipline	=	[[natural sciences]]
| abbreviation	=	''Pac Sci''
| website	=	http://www.uhpress.hawaii.edu/journals/ps/
| publisher	=	[[University of Hawaii Press]]
| country	=	[[United States|USA]]
| history	=	1947 to present
| ISSN	=	0030-8870
| eISSN	=	1534-6188
| impact = 0.924
| impact-year = 2014
| OCLC = 45666040
}}
'''''Pacific Science''''' is an international, multidisciplinary, academic journal devoted to the biological and physical sciences of the Pacific basin, focusing especially on [[biogeography]], [[ecology]], [[evolution]], [[geology]] and [[volcanology]], [[oceanography]], [[palaeontology]], and [[systematics]]. It has been published by the [[University of Hawaii Press|University of Hawai{{okina}}i Press]] since 1947, and serves as the official journal of the [[Pacific Science Association]].

Volume 1 lists [[A. Grove Day]] as the editor in chief of a general editorial board for the [[University of Hawaii]], where the editorship has remained. Leonard D. Tuthill of the Dept. of Zoology and Entomology served as editor of vols. 2-7 (1948–53); William A. Gosline of the Dept. of Zoology edited vols. 8-10 (1954–56) and vols. 22-25 (1968-71); and [[O. A. Bushnell]] of the Dept. of Microbiology edited vols. 11-21 (1957–67). The longest-serving editor was [[E. Alison Kay]] of the Dept. of General Science, then the Dept. of Zoology (from 1982), who edited vols. 26-54 (1972-2000), stepping down only after she retired. Gerald D. Carr of the Dept. of Botany edited vols. 55-58 (2001–04) and was succeeded by his departmental colleague, Curtis C. Daehler, from vol. 59 (2005).

The journal appears quarterly in January, April, July, and October. Its first electronic edition appeared in 2001 on [[Project MUSE]], which continues to host archives of vols. 55 (2001) through 61 (2007). The most current electronic edition is available on [[BioOne]], which also hosts archives going back to vol. 59 (2005).

Back issues of ''Pacific Science'' are archived online in the University of Hawaii at Mānoa's [[ScholarSpace]] [[institutional repository]].

==External links==
* [http://www.uhpress.hawaii.edu/journals/ps/ Publisher homepage]
* [http://www.pacificscience.org/ Pacific Science Association]
* [http://muse.jhu.edu/journals/psc/ MUSE homepage]
* [http://www.bioone.org/perlserv/?request=get-archive&issn=1534-6188 BioOne homepage]
* [http://scholarspace.manoa.hawaii.edu/handle/10125/364 Institutional repository]
* [http://www.pacificscience.wordpress.com/ Guide to online archives]
* [http://scholarspace.manoa.hawaii.edu/ ScholarSpace]

[[Category:English-language journals]]
[[Category:Publications established in 1947]]
[[Category:University of Hawaii Press academic journals]]
[[Category:Asian studies journals]]
[[Category:1947 establishments in Hawaii]]
[[Category:Oceania studies journals]]