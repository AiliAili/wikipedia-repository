<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
|name= Reginald Hoidge
|image= Lieut R T C Hoidge, by William Orpen.jpg
|caption=Lieut R. T. C. Hoidge ([[William Orpen]], 1917)
|birth_date=28 July 1894
|death_date=1 March 1963 (aged 68)
|birth_place=[[Toronto|Toronto, Ontario]], [[Canada]]
|death_place=[[New York City, New York]], [[United States]]
|nickname= Georgie
|allegiance={{flag|Canada|1868}} [[circa|c.]] 1914–1916<br />{{Flag|United Kingdom}} 1916–1920
|branch={{army|Canada}}<br/>{{army|UK}}<br/>{{air force|UK}} (attached)
|serviceyears=
|rank=[[First Lieutenant]]<br /> (held temporary rank as [[Captain (British Army and Royal Marines)|Captain]] for a short period)
|unit=[[Royal Regiment of Canadian Artillery|Canadian Royal Garrison Artillery]]<br />[[Royal Flying Corps]]
*[[No. 1 Squadron RFC]]
*[[No. 56 Squadron RFC]]
|commands=
|battles=[[First World War]]
|awards=[[Military Cross]] [[Medal bar|& Bar]]
|relations=
|laterwork=
}}

'''Reginald Theodore Carlos Hoidge''' [[Military Cross|MC]] [[Medal bar|& Bar]] (28 July 1894 &ndash; 1 March 1963) was a [[Canada|Canadian]] [[First World War]] [[flying ace]], officially credited with 28 victories.<ref name="theaerodrome.com">{{cite web|url=http://www.theaerodrome.com/aces/canada/hoidge.php|title=Reginald Hoidge|publisher=aerodrome.com|accessdate=21 August 2009}}</ref>  He served initially in the [[Royal Regiment of Canadian Artillery|Canadian Royal Garrison Artillery]] before transferring to the [[British Army]] to be attached to the [[Royal Flying Corps]], and then the new [[Royal Air Force]] on its creation in 1918.

==Early life==
Hoidge was born in [[Toronto|Toronto, Ontario]], [[Canada]]. He was the son of John R. and Lovida Hoidge.<ref>{{cite web|url=http://automatedgenealogy.com/census11/View.jsp?id=49398|title=Toronto, p. 8|work=Transcription of 1911 Census of Canada|publisher=Automated Genealogy—Indices of Canadian Censuses}}</ref> He originally served with the Canadian Royal Garrison Artillery.<ref name="theaerodrome.com"/>

==Aerial service in combat==
Hoidge transferred to the British Army, taking a commission in the (British) [[Royal Garrison Artillery]] ([[Special Reserve (militia)|Special Reserve]]), and was attached to the Royal Flying Corps, as a [[second lieutenant]] on 15 November 1916.<ref>{{LondonGazette|issue=29855|startpage=12062|date=8 December 1916|accessdate=21 August 2009}}</ref> He was posted to [[No. 56 Squadron RAF|No. 56 Squadron]] to fly a [[Royal Aircraft Factory SE 5]] fighter in 1917. He flew this aircraft for all his victories.<ref name="theaerodrome.com"/>

His first victory was over an [[Albatros D.III]] on 5 May 1917. He sent it down out of control over Montigny, France. The first of three victories on 24 May made him an ace. In an evening hour's roving battle, he sent an [[Albatros D.II]] down out of control for score number five, set another German plane on fire for number six, and finished up the day by driving down another D.III as victim seven.<ref name="theaerodrome.com"/>

He was promoted to temporary [[first lieutenant|lieutenant]] on 1 June 1917,<ref>{{LondonGazette|issue=30185|supp=yes|startpage=7104|date=13 July 1917|accessdate=21 August 2009}}</ref> and later confirmed in that rank from 11 July 1917.<ref>{{LondonGazette|issue=30368|supp=yes|startpage=11457|date=2 November 1917|accessdate=21 August 2009}}</ref> He was awarded the [[Military Cross]] on 18 July, the citation read:

{{quote|2nd Lt. Reginald Theodore Carlos Hoidge, R.G.A. (Spec. Res.) and R.F.C.

For conspicuous gallantry and devotion. On many occasions he has attacked and destroyed or driven down hostile machines, and has taken part in twenty-four offensive patrols. In all combats his bravery and skill have been most marked.<ref>{{LondonGazette|issue=30188|supp=yes|startpage=7232|date=18 July 1917|accessdate=21 August 2009}}</ref>}}

On 22 August, he was appointed a [[Flight commander (position)|flight commander]], with the rank of temporary [[captain (British Army and Royal Marines)|captain]].<ref>{{LondonGazette|issue=30366|supp=yes|startpage=11416|date=2 November 1917|accessdate=21 August 2009}}</ref>

Hoidge continued downing enemy aircraft until 31 October 1917, when his total stood at 27. During this stretch of success, his most memorable battle was one in which he did not score. He was one of the seven aces involved in [[Werner Voss]]'s last stand on 23 September, when Voss in his [[Fokker F.I]] fought all the British fliers to a standstill, damaging all the attacking SE 5s.<ref>{{cite web|url=http://www.theaerodrome.com/aces/germany/voss.php|title=Werner Voss|publisher=aerodrome.com|accessdate=24 August 2009}}</ref> Hoidge's final total included 23 successes over enemy fighters and only five over opposing two-seater reconnaissance planes.<ref name="theaerodrome.com"/>
His 28-claim tally comprised eight destroyed (including a shared victory), and 20 'out of control' victories (including two shared).<ref>Over the Trenches; Shores, 1990, page 197</ref>

==Later life==
Hoidge was returned to England for a year's duty as an instructor. He was awarded a [[medal bar|Bar]] to his MC on 18 March 1918, the citation read:

{{quote|2nd Lt. (T. /Lt.) Reginald Theodore Carlos Hoidge, M.C., R.G.A., Spec. Res. and R.F.C.

For conspicuous gallantry and devotion to duty in shooting down fourteen enemy aircraft in three and a half months. After attacking a large formation of enemy aircraft, owing to engine trouble he was driven down to six hundred feet at least five miles from our lines, but managed to recross the lines at a height of five hundred feet, and so saved his machine.<ref>{{LondonGazette|issue=30583|supp=yes|startpage=3418|date=18 July 1917|accessdate=21 August 2009}}</ref>}}

He returned to the front as a flight commander in his old unit and scored a final victory on 29 October 1918.<ref name="theaerodrome.com"/> He relinquished his commission on 1 April 1920, and was permitted to retain the rank of lieutenant.<ref>{{LondonGazette|issue=32197|supp=yes|startpage=539|date=18 January 1921|accessdate=21 August 2009}}</ref> He died in New York City on 1 March 1963.<ref name="theaerodrome.com"/>

==References==

===Notes===
{{reflist}}

===Citations===
* ''British and Empire aces of World War I''.Christopher Shores, Mark Rolfe. Osprey Publishing, 2001. ISBN 1-84176-377-2, ISBN 978-1-84176-377-4.

==External links==
{{refbegin}}
{{cite web |url=http://www.theaerodrome.com/aces/canada/index.php |title=WWI Aces of Canada |accessdate=2008-06-14 |work= |date= }}
{{refend}}

{{wwi-air}}

<!-- ==External links== -->

{{DEFAULTSORT:Hoidge, Reginald Theodore Carlos}}
[[Category:Canadian aviators]]
[[Category:Canadian World War I flying aces]]
[[Category:Royal Flying Corps officers]]
[[Category:People from Toronto]]
[[Category:1894 births]]
[[Category:1963 deaths]]
[[Category:Royal Artillery officers]]
[[Category:Recipients of the Military Cross]]