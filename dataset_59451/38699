{{Use dmy dates|date=February 2011}}
{{Infobox military person
|name= George Howard Brett
|birth_date= {{birth date|df=yes|1886|2|7}}
|death_date= {{death date and age|df=yes|1963|12|2|1886|2|7}}
|birth_place= [[Cleveland, Ohio]]
|death_place= [[Orlando, Florida]]
|placeofburial= [[Winter Park, Florida]]
|placeofburial_label= Place of burial
|image=AWM 011744 george brett.jpg
|caption=Lieutenant General George Brett in Australia on 18 March 1942, the day after his appointment as Deputy Supreme Commander, Southwest Pacific Area (SWPA) and Commander of Allied Air Forces, SWPA.
|nickname=
|allegiance= {{flag|United States of America|1908}}
|branch= [[File:US Army Air Corps Hap Arnold Wings.svg|20px]] [[United States Army Air Forces]]
|serviceyears= 1910&ndash;1946
|rank=[[File:US-O9 insignia.svg|45px]] [[Lieutenant General (United States)|Lieutenant General]]
|commands= [[19th Composite Wing]]<br>[[US Army Air Corps]]<br>[[Caribbean Defense Command]]
|unit=
|battles= [[Banana Wars]]<br>[[World War I]]<br/>[[World War II]]
*[[Western Desert Campaign]]
*[[Burma Campaign]]
*[[Dutch East Indies campaign]]
*[[Battle of the Coral Sea]]
*[[New Guinea campaign]]
|awards= [[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]] (2)<br/>[[Silver Star]]<br/>[[Distinguished Flying Cross (U.S.)|Distinguished Flying Cross]]<br/>[[Order of Orange-Nassau|Order of Orange-Nassau (Netherlands)]]<br/>[[Knight Commander of the Order of the Bath|Knight Commander of the Order of the Bath (United Kingdom)]]<ref>[http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&d=EP19430710.2.23.12&l=mi&e=-------10--1----0-- "Honour for Gen. Brett"]
''[[Evening Post (New Zealand)|Evening Post]]'', Volume CXXXVI, Issue 9, 10 July 1943, Page 5, from ''Papers Past'', [[National Library of New Zealand]].</ref>
|laterwork=
}}

'''George Howard Brett''' (7 February 1886 – 2 December 1963) was a [[United States Army Air Forces]] General during [[World War II]]. An [[Early Birds of Aviation|Early Bird of Aviation]], Brett served as a staff officer in [[World War I]]. In 1941, following the outbreak of [[Pacific War|war with Japan]], Brett was appointed Deputy Commander of a short-lived major [[Allies of World War II|Allied]] command, the [[American-British-Dutch-Australian Command]] (ABDACOM), which oversaw Allied forces in [[South East Asia]] and the [[South West Pacific]]. In early 1942, he was put in charge of United States Army Forces in [[Australia]], until the arrival of [[Douglas MacArthur]]. Brett then commanded all Allied Air Forces in the [[South West Pacific Area (command)|Southwest Pacific Area]]. In November 1942, he was appointed commander of the US Caribbean Defense Command and remained in this post for the rest of the war.

==Early life==
George Howard Brett was born in [[Cleveland, Ohio]] on 7 February 1886, the second of five children of [[William Howard Brett]], a notable [[librarian]], and his wife Alice née Allen.<ref>{{harvnb|Cox|2006|p=5}}</ref> George's older brother Morgan graduated with the [[United States Military Academy]] at West Point class of 1906, and served for many years as an [[Ordnance Corps (United States Army)|ordnance]] officer, retiring in 1932 as a [[Colonel (United States)|colonel]]. The family was unable to secure a second West Point appointment, so George Brett graduated from the [[Virginia Military Institute]] in 1909 and was commissioned as a [[Second Lieutenant#United States|Second Lieutenant]] in the [[Philippine Scouts]] on 22 March 1910.<ref>{{harvnb|Cox|2006|p=7}}</ref> While in the Philippines he transferred to the [[United States Cavalry|US Cavalry]] on 10 August 1911, joining the [[2d Stryker Cavalry Regiment|2nd Cavalry]].<ref name="Fogerty">{{harvnb|Fogerty|1953}}</ref>

Brett returned to the United States in May 1912 and was first stationed at [[Fort Bliss]]. In December 1913, he moved to [[Fort Ethan Allen]] where he became friends with a fellow lieutenant of the 2nd
Cavalry, [[Frank M. Andrews]], who was engaged to the daughter of [[Brigadier general (United States)|Brigadier General]] James Allen. While serving as one of Andrews' [[Wedding ceremony participants#Groomsmen|groomsmen]], Brett met Mary Devol, one of the [[Wedding ceremony participants#Bridesmaids|bridesmaids]], and the daughter of another Army officer, [[Major general (United States)|Major General]] [[Carroll Augustine Devol|Carroll A. Devol]]. Brett married Mary Devol in [[Denver]] on 1 March 1916.<ref>{{harvnb|Cox|2006|p=8}}</ref>  Influenced by Allen and Andrews, Brett transferred to the [[Aviation Section, U.S. Signal Corps]] on 2 September 1916. He attended aviation school and on graduation in 1916 was assigned to the office of the [[Signal Corps (United States Army)|Chief Signal Officer]] in  [[Washington, D.C.]] where he was promoted to [[first lieutenant#United States|first lieutenant]] on 1 July 1916 and [[Captain (United States uniformed services)|captain]] on 15 May 1917.<ref name="Fogerty"/>

==World War I==
Brett departed for the [[Western Front (World War I)|Western Front]] in November 1917 but suffered a case of [[appendicitis]], resulting in the loss of his flight status. After making a partial recovery, he served in France as senior materiel officer under Brigadier General [[Billy Mitchell]],<ref name="Cox_p9"/> attaining the temporary rank of [[Major (United States)|major]] on 7 June 1918.<ref name="Fogerty"/> After briefly returned to the United States to serve in Office of the Director of Military Aeronautics in [[Washington, D.C.]] from 1 August to 23 September 1918,<ref name="Cox_p9">{{harvnb|Cox|2006|pp=8&ndash;9}}</ref> Brett went to England to command the [[United States Army Air Service]] Camp at [[Codford]].<ref name="Fogerty"/>

==Between the wars==
Brett was posted to [[Kelly Field Annex|Kelly Field, Texas]], in December 1918, where he commanded the Aviation General Supply Depot until February 1919, when he became the maintenance and supply officer at the Air Service Flying School. He commanded the Air Service depot in [[Morrison, Virginia]] for a month in October 1919 before being
assigned to the office of the Director of the Air Service in Washington, DC, where his rank of major became permanent in 1920. That year he took command of [[Crissy Field]]. His first son, the future [[United States Air Force]] Lieutenant General [[Devol "Rock" Brett]], was born at nearby [[Letterman Army Hospital]] at the [[Presidio of San Francisco]] in 1923.<ref>{{harvnb|Cox|2006|pp=9&ndash;10}}</ref>

From 1924 to 1927 Brett was stationed at the intermediate depot at [[Fairfield, Ohio]], where he was the officer
in charge of the field service section. Starting in June 1927 he attended the [[Air Corps Tactical School]] at [[Langley Field, Virginia]], after which he was selected for the two-year [[Command and General Staff School]] at [[Fort Leavenworth, Kansas]].<ref>{{harvnb|Cox|2006|p=12}}</ref> He commanded [[Selfridge Field, Michigan]] for time before returned to Fort Leavenworth as an Air Corps instructor from 1933 to 1935. After 16 years as a major, he was finally promoted to [[Lieutenant colonel (United States)|lieutenant colonel]] and was selected to attend the [[United States Army War College|Army War College]]. On graduation, he became commander of the [[19th Air Division (United States)|19th Wing]], then stationed in the [[Panama Canal Zone]], with the temporary rank of brigadier general. While he was stationed there, his eldest daughter Dora married his aide, the future [[General (United States)|general]], [[Bernard A. Schriever]].<ref>{{harvnb|Cox|2006|pp=17&ndash;18}}</ref>

On his return from Panama, Brett reverted to his permanent rank of lieutenant colonel. He was briefly stationed in [[Menlo Park, California]], before moving to [[Langley, Virginia]], where he became chief of staff to his old friend Frank Andrews, now the commander of [[United States Army Air Corps#GHQ Air Force|GHQ Air Force]]. In February 1939 Brett moved to [[Wright-Patterson Air Force Base|Wright Field]] as assistant to the chief of the [[United States Army Air Corps]], also serving as commandant of the Air Corps Engineering School and the chief of the Materiel Division. Once again he held the rank of brigadier general before being promoted to major general on 1 October 1940.<ref name="Fogerty"/><ref name="Cox_p20">{{harvnb|Cox|2006|pp=19&ndash;20}}</ref>

==World War II==

===Middle East===
When his immediate superior, [[Major general (United States)|Major General]] [[Henry H. Arnold|Henry H. "Hap" Arnold]], was temporarily transferred to the Army General Staff in November 1939, Brett acted as Chief of the Air Corps. In May 1941, he formally became Chief of the Air Corps for a four-year term, but the June 1941 reorganisation that made Arnold the Chief of [[United States Army Air Forces]] made the post of Chief of the Air Corps somewhat redundant.<ref name="Cox_p20"/> Brett was sent to the United Kingdom to determine how the Army Air Forces could better support [[Royal Air Force]] [[Lend-Lease]] requirements. His recommendation that American labor and facilities be established in the United Kingdom to handle the repair, assembly, and equipping of American aircraft created a stir on both sides of the Atlantic, and was ultimately disapproved by Arnold on the grounds that the personnel and equipment were not available. Brett next paid a visit to the Middle East where his outspoken criticism of arrangements there antagonised his hosts to the extent that the British Ambassador to Egypt, [[Miles Lampson, 1st Baron Killearn|Sir Miles Lampson]], complained about him to the [[Secretary of State for Foreign and Commonwealth Affairs|Foreign Secretary]], Sir [[Anthony Eden]]. [[Air Marshal]] Sir [[Arthur Tedder]] noted that "the charms of General Brett’s company were beginning to pall. After a talk with him on the afternoon of 25 September I wondered in my journal how he and all the American visitors
could lay down the law about things of which they knew next to nothing."  As a result, Brett was ordered to return to the United States in December.<ref>{{harvnb|Cox|2006|pp=22&ndash;24}}</ref>

===East Indies===
[[File:ABDACOM Conference.jpg|thumb|right|ABDA COMMAND meeting with General Wavell for the first time. Seated around the table, from left: Admirals Layton, Helfrich, and Hart, General ter Poorten, Colonel Kengen, Royal Netherlands Army (at head of table), and Generals Wavell, Brett, and Brereton.]]
The outbreak of war between the United States and Japan in December 1941 changed things and Brett received new orders. He first flew to [[Yangon|Rangoon]] and then, in the company of the [[Archibald Wavell, 1st Earl Wavell|Sir Archibald Wavell]], the British [[Commander-in-Chief, India]], on to [[Chongqing|Chungking]] where the two met with [[Generalissimo]] [[Chiang Kai-shek]]. They obtained a promise of Chinese troops to assist in the defence of [[Burma]]. Near Rangoon, Wavell's and Brett’s aircraft was attacked by Japanese aircraft and they were forced to make an emergency landing at a friendly aerodrome in Burma. The area was then bombed by the Japanese, but neither general was harmed.<ref>{{harvnb|Cox|2006|pp=28&ndash;29}}</ref> Brett was appointed Deputy Supreme Commander of the [[American-British-Dutch-Australian Command]] (ABDA), under Wavell, on 1 January 1942, and was promoted to [[Lieutenant general (United States)|lieutenant general]] on 7 January 1942.<ref name="Fogerty"/> He arrived in [[Darwin, Northern Territory|Darwin]] on 28 December 1941. In January he moved to [[Lembang]] in [[West Java]], where Wavell established his headquarters.<ref>{{harvnb|Cox|2006|pp=30&ndash;32}}</ref> The rapid advance of Japanese forces through South East Asia had soon split the Allied-controlled area in two. Brett departed Java for Australia on 23 February 1942, reaching [[Melbourne]] the next day, where he resumed command of US Army Forces in Australia. ABDA was formally dissolved on 25 February.<ref>{{harvnb|Milner|1957|p=9}}</ref>

===Australia===
Already, [[General (United States)|General]] [[Douglas MacArthur]] had also been ordered to Australia. Brett received warning from the [[Chief of Staff of the United States Army|Chief of Staff]], General [[George Marshall]] that MacArthur would call on him to send a flight of long-range bombers to [[Mindanao]]. The only aircraft that Brett could find were [[B-17]]s of the [[19th Airlift Wing|19th Bombardment Group]] which had seen hard service in the [[Battle of the Philippines (1941–42)|Philippines]] and the [[Dutch East Indies campaign]]s. Brett approached [[Vice admiral (United States)|Vice Admiral]] [[Herbert F. Leary]], the commander of naval forces in the Anzac Area, to ask for a loan of some of twelve newly arrived navy B-17s. Leary refused. Brett therefore sent four of the 19th Bombardment Group's old planes. Only one, a B-17 with no brakes piloted by Lieutenant [[Harl Pease]], made it to Mindanao; two turned back with engine trouble, while a fourth ditched in the sea, its crew managing to escape. MacArthur was incensed and sent a message to Marshall. A message from [[Washington, D.C.]] persuaded Leary to release four new B-17s to Brett and these aircraft reached Mindanao on 16 March 1942 and managed to bring MacArthur and his party to Australia. Despite the lack of brakes, Pease also made it back, carrying sixteen refugees.<ref>{{harvnb|Brett|1947|pp=139&ndash;140}}</ref>

It fell to Brett to telephone [[Prime Minister of Australia|Prime Minister]] [[John Curtin]] and inform him of MacArthur's arrival. Although Curtin was unaware of MacArthur's impending arrival, and had expected that Brett would command American forces in Australia, he was persuaded to issue a recommendation that MacArthur be made [[Commander-in-chief|Supreme Commander]] [[South West Pacific Area]].<ref>{{harvnb|Milner|1957|pp=17&ndash;18}}</ref> While Brett considered that he was on "very friendly terms" with Curtin,<ref>{{harvnb|Brett|1947|p=140}}</ref> Brett felt that Curtin was "more interested in keeping the party line on wages, hours and working conditions than in the threat posed by the Japanese."<ref>{{harvnb|Brett|1947|p=26}}</ref> General [[George Kenney]] later recalled that {{quote|I think he [Brett] made his initial mistake in sort of spurning this [[Australian Labor Party|Labor]] government crowd and taking up with the conservative crowd, who had been ousted by the Labor Party and who were not going to get back into power. But Brett figured they were, so he accepted entertainment from them and entertained them in return and became quite close to them. They, in turn, kidded him along and told him they knew he was going to be the commanding general of all the Allied Forces in Australia... Yes, and he believed it, which was too bad.<ref>{{harvnb|Cox|2006|p=58}}</ref>}}

The April 1942 reorganisation that established the [[South West Pacific Area (command)|Southwest Pacific Area]] reduced the United States Army Forces in Australia to a supply and administrative organisation that would soon be renamed the [[Services of Supply]]. Brett instead became commander of Allied Air Forces, Southwest Pacific Area, with his headquarters in [[Melbourne]].<ref>{{harvnb|Milner|1957|pp=19&ndash;22}}</ref> One of MacArthur's first orders to Brett was for a bombing mission to the Philippines, an order which was delivered personally by MacArthur's chief of staff, Major General [[Richard K. Sutherland]]. Brett protested that his planes were worn out, his men were tired, losses might be high, and the Philippines were lost anyway. Sutherland told him the MacArthur wanted the mission carried out. Brett delegated it to Brigadier General [[Ralph Royce]], who led the mission in person. Brett awarded Royce the [[Distinguished Service Cross (United States)|Distinguished Service Cross]].<ref>{{harvnb|Brett|1947|pp=144&ndash;145}}</ref> Henceforth, communications with Sutherland would be handled by Brett's chief of staff, [[Air Vice Marshal]] [[William Bostock]]. MacArthur personally wrote a [[reprimand]] to Brett. Further disagreements between MacArthur and Brett followed.<ref>{{harvnb|Rogers|1990|pp=276&ndash;277}}</ref> On 6 July 1942 Marshall radioed MacArthur to offer him Major General [[George Kenney]] or Brigadier General [[Jimmy Doolittle]] as a replacement for Brett. MacArthur selected Kenney. Brett returned to the United States in his B-17, "[[The Swoose]]", on 4 August 1942. The day before, General MacArthur awarded him the [[Silver Star]] "for gallantry in action in air reconnaissance in the combat zone, Southwest Pacific Area, during the months of May, June and July 1942."<ref>{{harvnb|Cox|2006|pp=60&ndash;61}}</ref>

===Panama===
After a time with no command, Brett was appointed commander of the US Caribbean Defense Command and the US Army's [[Panama Canal Department]] in succession to Lieutenant General Frank M. Andrews in November 1942.<ref>{{harvnb|Cox|2006|p=70}}</ref> In 1945, the [[Inspector General]], Lieutenant General [[Daniel Isom Sultan]], investigated a series of allegations against Brett regarding the misuse of Army funds and property. He reported to the General Marshall that most of the charges were distortions of mission-related events and expenditures, that the remaining allegations had no basis in fact, and that no further action be taken.<ref>{{harvnb|Cox|2006|p=75}}</ref> Brett requested voluntary retirement and retired on 30 April 1945 with the rank of major general, only to be immediately recalled to active duty the next day as a temporary lieutenant general and as Commanding General of the Caribbean Defense Command and Panama Canal Department. On 10 October 1945, Brett handed over command to Lieutenant General [[Willis D. Crittenberger]]. For his service in Panama, Brett was awarded a second Distinguished Service Medal. The citation noted "his broad grasp of military strategy and superior knowledge of air and ground tactics" and that "he succeeded admirably in impressing the republics of Central and South America with the importance and necessity of hemispheric solidarity, imbued them with American ideals, coordinated their use of arms and equipment and indoctrinated them with American training methods &mdash; all of which fostered continued improvement in the relations between all America republics."<ref name="Cox_p76"/> After spending time as a patient in [[Brooke Army Medical Center|Brooke General Hospital]], he reverted to retired status on 10 May 1946 but was later advanced to the grade of lieutenant general on the [[United States Air Force]] retired list by an Act of Congress on 29 June 1948.<ref name="Cox_p76">{{harvnb|Cox|2006|pp=76&ndash;78}}</ref>

==Post-war==
[[File:B-17D-BO 40-3097.jpg|thumb|left|B-17D BO AAF Ser. No. 40-3097 ''The Swoose'' in 1944]]
The [[B-17 Flying Fortress|B-17D]], "[[The Swoose]]", which Brett used extensively for his personal transport during World War II, and which he often piloted, is today the oldest, intact, surviving B-17 Flying Fortress and the only "D" model still in existence. It was transferred from the [[Smithsonian Institution|Smithsonian]] [[National Air and Space Museum]] to the [[National Museum of the United States Air Force]] on 15 July 2008.<ref>{{Citation |first= Sarah |last= Parke |title= The Swoose comes home to roost at the National Museum of the U.S. Air Force |url= http://www.nationalmuseum.af.mil/news/story.asp?storyID=123106540|publisher=National Museum of the United States Air Force |accessdate = 25 February 2009}}</ref>

Brett served on several committees and Air Force boards, including Flying Pay Board, the Air
Force Association Board, and the President's Service Academy Board between 1949 and 1950. When his son Rock Brett was deployed in the [[Korean War]], George Brett took in his daughter-in-law and grandchildren and cared for them. Brett lived in [[Winter Park, Florida]] until his death at age 77. He died of [[cancer]] on 2 December 1963 at the hospital at [[Orlando Air Force Base]]. Survived by his wife, children, and grandchildren, he was buried in [[Winter Park, Florida]].<ref>{{harvnb|Cox|2006|pp=78&ndash;79}}</ref>
{{clear}}

==Dates of rank==
{| class="wikitable"
! Insignia !! Rank !! Component !! Date
|-
|<!--second lieutenants wore no pin insignia in 1910--> No insignia in 1910
|[[US Second Lieutenant|Second Lieutenant]] || [[Philippine Scouts]] || March 22, 1910
|-
|<!--second lieutenants wore no pin insignia in 1911--> No insignia in 1911
|[[US Second Lieutenant|Second Lieutenant]] || [[Cavalry]] || August 10, 1911
|-
|[[File:US-O2 insignia.svg|13px|center]]
| &nbsp;[[US First Lieutenant|First Lieutenant]] || Cavalry || July 1, 1916
|-
|[[File:US-O2 insignia.svg|13px|center]]
| &nbsp;[[US First Lieutenant|First Lieutenant]] || [[Signal Corps]] || September 2, 1916
|-
|[[File:US-O3 insignia.svg|33px|center]]
| &nbsp;[[Captain (U.S. Army)|Captain]] || Signal Corps || May 15, 1917
|-
|[[File:US-O4 insignia.svg|40px|center]]
| &nbsp;[[Major (United States)|Major]] || [[National Army (USA)|National Army]] || June 7, 1918
|-
|[[File:US-O4 insignia.svg|40px|center]]
| &nbsp;[[Major (United States)|Major]] || Air Service || July 1, 1920
|-
|[[File:US-O5 insignia.svg|40px|center]]
| &nbsp;[[Lieutenant colonel (United States)|Lieutenant Colonel]] || [[United States Army Air Corps|Army Air Corps]] || October 1, 1934
|-
|[[File:US-O7 insignia.svg|33px|center]]
| &nbsp;[[Brigadier general (United States)|Brigadier General]] || Temporary || August 20, 1936
|-
|[[File:US-O6 insignia.svg|60px|center]]
| &nbsp;[[Colonel (United States)|Colonel]] || Temporary || September 10, 1938
|-
|[[File:US-O6 insignia.svg|60px|center]]
| &nbsp;[[Colonel (United States)|Colonel]] || Army Air Corps || January 1, 1939
|-
|[[File:US-O7 insignia.svg|33px|center]]
| &nbsp;[[Brigadier general (United States)|Brigadier General]] || Army Air Corps || January 31, 1939
|-
|[[File:US-O6 insignia.svg|60px|center]]
| &nbsp;[[Colonel (United States)|Colonel]] || Army Air Corps || January 1, 1939
|-
|[[File:US-O8 insignia.svg|66px|center]]
| &nbsp;[[Major general (United States)|Major General]] || [[Army of the United States]] || October 1, 1940
|-
|[[File:US-O8 insignia.svg|66px|center]]
| &nbsp;[[Major general (United States)|Major General]] || Army Air Corps || May 31, 1941
|-
|[[File:US-O9 insignia.svg|100px|center]]
| &nbsp;[[Lieutenant general (United States)|Lieutenant General]] || Army of the United States || January 7, 1942
|-
|[[File:US-O8 insignia.svg|66px|center]]
| &nbsp;[[Major general (United States)|Major General]] || Retired || April 30, 1945
|-
|[[File:US-O9 insignia.svg|100px|center]]
| &nbsp;[[Lieutenant general (United States)|Lieutenant General]] || United States Air Force (Retired)|| June 10, 1948
|} <ref>Official Register of Commissioned Officers of the United States Army, 1946.  pg. 813.</ref>

==Notes==
{{Reflist|30em}}

==References==
*{{Cite journal
 | last = Brett
 | first = George H.
 | authorlink = George Brett (military)
 | title = The MacArthur I knew
 | journal = True Magazine
 | date = October 1947
 | ref = harv
}}
*{{Cite book
 | last = Cox
 | first = Douglas A.
 | year = 2006
 | url = http://www.maxwell.af.mil/au/aul/aupress/books/cox/cox.pdf.
 | title = Airpower Leadership on the Front Line Lt Gen George H. Brett and Combat Command
 | place = [[Maxwell Air Force Base]], [[Alabama]]
 | publisher = [[Air University (United States)|Air University]]
 | accessdate = 14 July 2012
 | ref = harv
}}
*{{Cite book
 | first = Dr Robert O.
 | last = Fogerty
 | title = Biographical data on Air Force General Officers
 | year = 1953
 | place = [[Maxwell Air Force Base]], [[Alabama]]
 | publisher = [[Air University (United States)|Air University]]
 | url = http://www.afhra.af.mil/shared/media/document/AFD-090601-134.pdf
 | accessdate =  14 July 2012
 | ref = harv
}}
*{{Cite book
 | first = Samuel
 | last = Milner
 | url = http://www.history.army.mil/html/books/005/5-4/CMH_Pub_5-4.pdf
 | accessdate =  14 July 2012
 | title = Victory in Papua
 | publisher = [[United States Department of the Army]]
 | year = 1957
 | isbn = 1-4102-0386-7
 | ref = harv
}}
*{{Cite book
 |last = Rogers
 |first = Paul P.
 |title = MacArthur and Sutherland: The Good Years
 |publisher = Praeger
 |year = 1990
 |location = New York City
 |isbn = 0-275-92918-3
 | ref = harv
}}

==Further reading==
*{{Cite book
 | last = Ancell
 | first = R. Manning
 | last2 = Miller
 | first2 = Christine
 | title = The Biographical Dictionary of World War II Generals and Flag Officers: The US Armed Forces
 | place = [[Westport, Connecticut]]
 | publisher = [[Greenwood Press]]
 | year = 1996
 | page = 457
 | isbn = 0-313-29546-8
}}
*{{Cite book
 | last = Brownstein
 | first = Herbert S.
 | title = The Swoose: Odyssey of a B-17
 | location = Washington, DC
 | publisher = Smithsonian Institution Press
 | year = 1993
 | isbn = 1-56098-196-2
}}

==External links==
{{Portal|Biography|United States Air Force|United States Army}}
*{{Cite web
 | last = Dunn
 | first = Peter
 | year = 2001
 | url = http://home.st.net.au/~dunn/usaaf/generalbrett.htm
 | title = Lieutenant General George H. Brett in Australia During WW2
 | accessdate = 22 January 2009
}}

{{Good article}}

{{s-start}}
{{s-mil}}
{{succession box|title=[[United States Army Air Corps#Chiefs of Air Corps|Chief of Air Corps]]| before= [[Henry H. Arnold]]| after=None| | years=1938&ndash;1941
}}
{{s-end}}

{{Authority control}}

{{DEFAULTSORT:Brett, George}}
[[Category:1886 births]]
[[Category:1963 deaths]]
[[Category:American military personnel of World War II]]
[[Category:Military personnel from Cleveland]]
[[Category:Recipients of the Distinguished Service Medal (United States)]]
[[Category:Recipients of the Silver Star]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Order of Orange-Nassau]]
[[Category:Honorary Knights Commander of the Order of the Bath]]
[[Category:Members of the Early Birds of Aviation]]
[[Category:United States Army Command and General Staff College alumni]]
[[Category:Air Corps Tactical School alumni]]
[[Category:United States Army Air Forces generals]]
[[Category:United States Army generals]]
[[Category:American military personnel of World War I]]
[[Category:United States Army Air Service pilots of World War I]]
[[Category:People from Winter Park, Florida]]
[[Category:United States Army War College alumni]]