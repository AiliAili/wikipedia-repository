{{ Infobox scientist
| name              = Guy L. Fleming
| birth_date        = May 27, 1884
| birth_place       = Nebraska, United States
|death_date = {{Death date and age|1960|05|15|1884|05|27}}  
| death_place       = San Diego, California, United States
| residence         = 
| citizenship       = 
| nationality       = American
| ethnicity         = 
| fields            = [[Botany]]
| work_institution  = [[California Department of Parks and Recreation|California State Parks System]], [[San Diego Natural History Museum]] 
| alma_mater        = 
| doctoral_advisor  = 
| academic_advisors = 
| doctoral_students = 
| notable_students  = 
| known_for         = Conservation of [[Torrey pine|Pinus torreyana (Torrey pine)]]
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences        = 
| influenced        = 
| awards            = 
| signature         = <!--(filename only)-->
| signature_alt     = 
| footnotes         = 
}}

'''Guy L. Fleming''' (1884-1960) was an American naturalist whose conservation work led to the founding of [[Torrey Pines State Natural Reserve]], now a 2000-acre protected coastal area of La Jolla, San Diego.<ref name=nichol>{{cite web|last1=Nichol|first1=Hank|title=Guy Fleming (from Notes from the Naturalist)|url=http://torreypine.org/history2/protecting-pines/guy-fleming/|website=Torrey Pines State Natural Reserve|date=1994}}</ref> The [[Torrey pine]], ''Pinus torreyana'', is the rarest pine species in the United States.<ref>The Torrey pine is an endangered species that grows only in two places, the La Jolla coastal bluff that Fleming worked to protect, and on Santa Rosa Island of the [[Channel Islands of California|Channel Islands]].</ref>

As district superintendent of state parks  for southern California, Fleming also worked to protect major nature preserves that extend from the U.S. border with Mexico to northern San Diego county; these include the natural areas that became [[Anza-Borrego Desert State Park]], [[Cuyamaca Rancho State Park]] and [[Palomar Mountain#Palomar Mountain State Park|Palomar Mountain State Park]].<ref name=comstock>{{cite news|last1=Comstock|first1=John A.|title=State memorializes Guy Fleming's work|work=San Diego Union|date=March 6, 1961|page=B-2}}</ref> As a fellow of the San Diego Society of Natural History, he worked closely with scientists and staff of the [[San Diego Natural History Museum]], leading public nature walks for the museum and lecturing on environmental issues.<ref>{{cite journal|last1=Lindsay|first1=Diana|title=History in the California Desert: The Creation of the Anza Borrego Desert State Park--Largest in the United States|journal=Journal of San Diego History|date=Fall 1973|volume=19|issue=4|url=http://www.sandiegohistory.org/journal/73fall/anza.htm}}</ref>

== Biography ==
[[File:Pinus torreyana at State Reserve.jpg|thumb|upright 1.2|Pinus torreyana at State Reserve (photo: [https://www.flickr.com/people/12547641@N00 Richard O. Barry])]]
[[File:Guy and Margaret Fleming House - Built 1927. Torrey Pines State Reserve, San Diego, CA.JPG|thumb|upright 1.2|Guy and Margaret Fleming House - Built 1927. Torrey Pines State Reserve, San Diego, CA (photo:Racoats)]]
Born in Nebraska on May 27, 1884,<ref>{{cite web|last1=California Death Index, 1940-1997|title=Guy L. Fleming, 15 May 1960|url=https://familysearch.org/search/collection/results?count=20&query=%2Bgivenname%3Aguy~%20%2Bsurname%3Afleming~&collection_id=2015582|website=(database, FamilySearch)}}</ref> to Georgia Lowd and James Fleming (a carpenter),<ref>{{cite web|title=Illinois Statewide Marriage Index, 1763-1900|url=http://www.ilsos.gov/isavital/marriageSearch.do|website=Department of the Secretary of State}}</ref> Guy L. Fleming was twelve when his family moved to Corvallis, Oregon.<ref>{{cite web|title=Person Details for Guy Flemming in household of * J Flemming, "United States Census, 1900"|url=https://familysearch.org/ark:/61903/1:1:MSDC-72Y|website=(database) FamilySearch}}</ref>  In 1909, he moved to San Diego and found work as a gardener for the [[Little Landers|Little Lander Colony]] of San Ysidro, laying out and planting the village park. His work caught the attention of former San Diego County Horticultural Commissioner and chairman of the advisory board for the Little Landers, George P. Hall,<ref>{{cite journal|last1=Lee|first1=Lawrence B.|title=The Little Landers Colony of San Ysidro|journal=Journal of San Diego History|date=Winter 1975|volume=21|issue=1|url=https://www.sandiegohistory.org/journal/75winter/littlelanders.htm}}</ref> who encouraged Fleming to study botany. In 1911 he worked in the nursery preparing for the 1915 [[Panama-California Exposition]]; during the fair, he became a guide, giving talks on plants and landscaping.<ref name=nichol />

In 1921, Fleming was hired by [[Ellen Browning Scripps]] as the custodian and naturalist of her property of Torrey Pines; in that year, the city of San Diego also hired him to be caretaker of the city-owned portion of Torrey Pines property adjacent to the Scripps land. Working with landscape architect [[Ralph D. Cornell]], Fleming developed plans<ref name=nichol /> to preserve the Torrey pines on both the city and Scripps properties, proposing a nature park for the area.<ref name=fleming>{{cite news|last1=Fleming|first1=Guy|title=Patriarchs of ancient forest are preserved at Torrey Pines|work=San Diego Union|date=January 20, 1929|page=9}}</ref>  In 1922, they studied the only other stand of Torrey pines on Santa Rosa Island.<ref name=nichol />

In 1923, Fleming became a fellow of the San Diego Society of Natural History, working throughout the 1920s to educate the public on conservation issues, leading nature walks and lecturing on the region's botanical diversity. He participated in study of pines in Baja California and of the proposed [[Kings Canyon National Park]] region and conducted a timber survey of Cuyamaca.<ref name=nichol />

In 1927, Fleming married artist Margaret Doubleday Eddy;<ref>{{cite news|last1=Morgan|first1=Judith|title=Artist captures beauty of Torrey Pines in etchings|work=San Diego Union|date=May 22, 1969|page=B-5}} Margaret Fleming's father [[William Abner Eddy]] invented the Eddy kite.</ref> together they built a small home on the Scripps property at Torrey Pines. In 1932, Fleming was appointed District Superintendent for all southern California state parks (then 20 parks); during the Depression, he had responsibility for the administration of six [[Civilian Conservation Corps|CCC]] camps.<ref name=nichol />

Fleming retired from the [[California Department of Parks and Recreation|California State Parks System]] in 1948, but continued to be active in conservation efforts. In 1950, he was a founding member of the Torrey Pines Association, working to integrate the Torrey Pines City Park into the California State Park System.<ref name=anderson>{{cite news|last1=Anderson|first1=George A.|title=Guy Fleming saw vision come true|work=San Diego Union|date=May 29, 1960|page=C-2}}</ref>

Fleming died on 15 May 1960.

== References ==
{{Reflist}}

== External links ==
* [http://torreypine.org/ Torrey Pines State Natural Reserve]
* [http://www.torreypines.org/ Torrey Pines Association]
* [http://www.sdnhm.org/science/research-library The San Diego Natural History Museum] holds archival material on Guy Fleming's conservation work.

{{San Diego Natural History Museum}}

{{DEFAULTSORT:Fleming, Guy}}
<!--- Categories --->
[[Category:American conservationists]]
[[Category:American naturalists]]
[[Category:Activists from California]]
[[Category:Articles created via the Article Wizard]]
[[Category:1884 births]]
[[Category:1960 deaths]]
[[Category:People associated with the San Diego Natural History Museum]]
[[Category:Natural history of San Diego County, California]]<!--naturalist/museum-->
[[Category:People from La Jolla, San Diego]]
[[Category:People from San Diego]]