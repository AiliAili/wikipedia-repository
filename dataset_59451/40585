{{Infobox Korean name
| hangul = 천정희
| hanja  = !
| mr     = Chŏn Jŏnghŭi
| rr     = Cheon Jeong-hui
}}

'''Cheon Jung-Hee''' is a South Korean mathematician and [[cryptographer]] whose research interest includes [[computational number theory]], [[cryptography]], and [[information security]]. He is one of the inventors of braid cryptography, one of [[group-based cryptography]]. He is particularly known for his work on an efficient algorithm on strong [[Diffie–Hellman problem|DH problem]]. He received the best paper award in [[Asiacrypt]] 2008 for improving [[Pollard rho algorithm]], and the best paper award in [[Eurocrypt]] 2015 for attacking Multilinear Maps.<ref>[https://www.iacr.org/conferences/asiacrypt2008/ 14th Annual International Conference on the Theory and Application of Cryptology & Information Security AsiaCrpyt 2008 Dec 7-11, 2008, Melbourne, Australia]</ref><ref>[https://www.cosic.esat.kuleuven.be/eurocrypt_2015/program.shtml 34th Annual International Conference on the Theory and Applications of Cryptographic Techniques Eurocrypt 2015 April 26-30, Sofia, Bulgaria]</ref>

He is a professor of Mathematical Sciences at the [[Seoul National University]] (SNU). He received the [[Bachelor of Science]], [[Master of Science]] and [[Ph.D]] degrees in Mathematics from [[KAIST]] in 1991, and 1997, respectively. Before joining to SNU, he was in [[ETRI]], [[Brown university]], and [[Information and Communications University|ICU]].

He is a program co-chair of ICISC 2008, MathCrypt 2013, [[Algorithmic Number Theory Symposium|ANTS]]-XI and [[Asiacrypt]] 2015. 
<ref>[http://www.icisc.org/icisc08/asp/02.html 11th International Conference on Information Security and Cryptology ICISC 2008 Dec 3-5, 2008, Seoul, Korea]</ref>
<ref>[https://archive.is/20150404110820/http://open.nims.re.kr/new/event/event.php?workType=home&Idx=113 MathCrypt 2013 July 4-5, 2013, Busan, Korea]</ref>
<ref>[https://ants2014.kookmin.ac.kr/ 11th Algorithmic Number Theory Symposium ANTS-XI August 6–11, 2014, GyeongJu, Korea] {{webarchive |url=https://web.archive.org/web/20130928025512/https://ants2014.kookmin.ac.kr/ |date=September 28, 2013 }}</ref>
<ref>[https://www.math.auckland.ac.nz/~sgal018/AC2015/index.html 21st Annual International Conference on the Theory and Application of Cryptology and Information Security AsiaCrpyt 2015 Nov 29-Dec 3, 2015, Auckland, New Zealand]</ref>

== References ==
{{Reflist}}

== External links ==
*[http://cse.snu.ac.kr/professor/%EC%B2%9C%EC%A0%95%ED%9D%AC Faculty page] at Seoul National University Department of Computer Science and Engineering

{{DEFAULTSORT:Cheon, Jung-hee}}

[[Category:Number theorists]]
[[Category:Living people]]
[[Category:South Korean mathematicians]]
[[Category:KAIST alumni]]
[[Category:Seoul National University faculty]]
[[Category:Year of birth missing (living people)]]