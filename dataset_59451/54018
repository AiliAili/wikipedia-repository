{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox stadium
| stadium_name      = Adelaide Super-Drome
| nickname          = 
| image             = 
| location          = State Sports Park, Main North Road, [[Gepps Cross, South Australia]] 5094
| coordinates       = {{coord|34|50|29|S|138|36|45|E|display=it}}
| broke_ground      = 1993
| opened            = 1993
| closed            =
| demolished        =
| owner             = [[South Australian Government]]
| operator          = Office for Recreation & Sport
| surface           = softwood
| construction_cost = 
| architect         = Carlo Gnezda (building)<br>Ron Webb (track)
| former_names      = 
| tenants           = [[Australian Institute of Sport|AIS]] [[Track Cycling]] (1993-present)<br>Cycling South Australia
| seating_capacity  = 3,000
|}}

The '''Adelaide Super-Drome''' is located at [[Adelaide, South Australia]]'s State Sports Park, Main North Road, [[Gepps Cross, South Australia|Gepps Cross]]. The Super-Drome was designed by [[Architect]] Carlo Gnezda and was opened in 1993.<ref>[http://sketchup.google.com/3dwarehouse/details?mid=e5a3addd4483e91b7a3948642f440190 3D Warehouse - Adelaide Super-Drome]</ref> From 1993 the venue was managed and promoted by [[1984 Summer Olympic Games|1984 Olympic Games]] [[Cycling at the 1984 Summer Olympics – Men's team pursuit|Men's team pursuit]] gold medalist [[Michael Turtur]].<ref>[http://www.sports-reference.com/olympics/athletes/tu/michael-turtur-1.html Olympic results]</ref> He was assisted by the venue's track designer Ron Webb in bringing out international competitors.
 
It is the headquarters for the [[Australian Institute of Sport]]’s [[Track Cycling]] Program due to its fully accredited international-standard training and competition facilities, for Adelaide's mild climate, and for being a short distance to the [[Adelaide Hills]].<ref>[http://www.ausport.gov.au/ais/sports/cycling/home AIS Track Cycling] {{webarchive |url=https://web.archive.org/web/20121127051602/http://www.ausport.gov.au/ais/sports/cycling/home |date=27 November 2012 }}</ref>

==Events and Usage==
The Super-Drome has hosted international events and has been used as a training base for teams competing in Michael Turtur's [[Tour Down Under]] which is run annually in Adelaide and the surrounding countryside since 1999. The Super-Drome was also the site of the [[2011 Oceania Track Championships]].

It is a popular destination for international track cycling teams looking for a unique environment for a training camp prior to competitions in Adelaide and other States in Australia.

On many weeknights the Super-Drome is the venue for the [[South Australian Futsal League]], where all matches are played.

==Facilities==
The indoor velodrome is a 250m international standard timber track made from specially specified Nordic Pine with 43° banking in the turns, constructed under the supervision of British velodrome specialist Ron Webb, while 3,000 spectators are accommodated in fixed tiered seating.<ref>[http://www.ronwebbcycletracks.com/ R.V.Webb Consultants]</ref> On the upper level there is a large corporate function room with catering and bar facilities which has excellent views over the track. On this level there is a separate equipped media room.

In the centre of the cycling track is a multipurpose concrete floor used for various sports which once had a swimming pool set up on the tracks infield as part of a [[triathlon]] course.

It is the location for the headquarters and office of Cycling South Australia.

==References==
{{reflist}}

==External links==
*[http://ors.sa.gov.au/our_venues/adelaide_super-drome Adelaide Super-Drome] at Office for Recreation & Sport SA
*[http://safl.org.au/ South Australian Futsal League Official Website]

{{Adelaide landmarks}}

[[Category:Cycle racing in Australia]]
[[Category:Velodromes in Australia]]
[[Category:Sports venues in Adelaide]]
[[Category:Government of South Australia]]
[[Category:Australian Institute of Sport|*]]
[[Category:1993 establishments in Australia]]
[[Category:Sports venues completed in 1993]]