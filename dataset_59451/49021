{{Infobox military unit
|unit_name= Kerala Thunderbolts (India)
|image= Kerala_Police_Commando_Thunderbolt.jpg
|caption=Kerala Thunderbolts Logo
|dates= 2012 – present
|country= {{flagicon|India}}   [[India]]
|allegiance=
|branch= [[Image:Flag of Kerala Police.svg|23px]][[Kerala Police]]
|type= [[SWAT]]
|role=Primary tasks: 
* [[Counter-terrorism]]
* [[Jungle warfare]]
* [[Personnel recovery]]
* [[hostage rescue]]
|size= 200 commandos
|command_structure=
|garrison=  [[Thrissur]]<ref>{{cite web 
|url=http://news.justkerala.in/thunderbolt-commandos-failed-to-trace-maoists.php
|title=Thunderbolt Commandos Failed to Trace Maoists
|publisher=Just Kerala
|accessdate=2014-12-11}}</ref>
|garrison_label=Regimental Centre
|nickname=
|patron=
|motto= Swift, Strong and Secure.
|colors=
|colors_label=Operations
|march=
|mascot=
|battles= 
|anniversaries= 24 August.
|decorations=
<!-- Commanders -->
|current_commander=
|current_commander_label=
|ceremonial_chief=
|ceremonial_chief_label=
|colonel_of_the_regiment=
|colonel_of_the_regiment_label=
|notable_commanders=
}}

'''Kerala Thunderbolts''' is an elite command force of the [[Kerala Police]] under the India Reserve Battalion set up in accordance with the Indian central government's directions post the [[2008 Mumbai attacks]]. The commando force has been created to counter possible terror strikes and carry out counterinsurgency operations in [[Kerala]].  The force modeled on the [[Special Protection Group]] and [[National Security Guard]] are trained to engage in air, water and land attacks.<ref name=mbumi>{{cite news|title=Thunder Bolts add feather to Kerala Police|url=http://www.mathrubhumi.com/english/story.php?id=127184|accessdate=3 September 2013|newspaper=Mathrubhumi|date=14 August 2012|agency=UNI}}</ref><ref name=hbl>{{cite news|title=Kerala Thunder Bolts ready for action|url=http://www.thehindubusinessline.com/news/states/kerala-thunder-bolts-ready-for-action/article3762385.ece|accessdate=3 September 2013|newspaper=Business Line|date=13 August 2012|agency=PTI}}</ref>

== Known operations ==

A Thunderbolt team carried out a combing operation in the [[Malapuram]] district following reports of sighting of Maoist insurgents in February, 2013.<ref name=satp>{{cite web|title=Kerala Timeline 2013|url=http://www.satp.org/satporgtp/countries/india/maoist/timelines/2013/Kerala.htm|work=South Asia Terrorism Portal|publisher=Institute for Conflict Management|accessdate=3 September 2013}}</ref>
In March 2013, a 30 member Thunderbolt team was involved in a counterinsurgency combing operation in search of suspected Maoist insurgents in [[Kannur]] district in Kerala. On December 6, 2014, A gunfight broke out  in a forest in Kerala’s hilly [[Wayanad]] district between the   Thunderbolt unit and the left-wing rebels (Maoist).It was the first direct encounter between police and Maoists in the history of Kerala and no causality reported. <ref name=satp /><ref>{{cite news|title=Thunderbolt team begins hunt for Maoists in Western Ghats|url=http://newindianexpress.com/states/kerala/Thunderbolt-team-begins-hunt-for-Maoists-in-Western-Ghats/2013/05/14/article1589196.ece|accessdate=3 September 2013|newspaper=The New Indian Express|date=14 May 2013|agency=Express News Service|location=Thrissur}}</ref>

== Selection & Training ==

The commando force consists of 2 companies of 160 personnel, who have been recruited after undergoing 2 years of rigorous and specialized training at various institutions around the country including Army’s Counter Insurgency and Jungle Warfare School, Mizoram.<ref name=TNE>{{cite news|title=Thunderbolt team set to go Hi-tech|url=http://newindianexpress.com/states/kerala/Thunderbolt-team-set-to-go-Hi-tech/2013/05/27/article1607887.ece|accessdate=3 September 2013|newspaper=The New Indian Express|date=27 May 2013|agency=Express News Service|location=Kochi}}</ref>
Chief Minister Oommen Chandy, announced the approval of the Central Government for raising a second battalion of 40 more personnel, in the wake of increased terrorist activity across India.<ref name=2ndbatt />

The recruits, in the 18-21 age group, were selected after going through the Three Star physical Efficiency Test and Endurance test, as specified by the [[Kerala Public Service Commission]]. The second stage training was completed at Commando School, [[Chennai]], National Disaster Management School, [[Coimbatore]], Counter insurgency and Anti Terrorism School, [[Silchar]], National Adventure School, [[Munnar]], Underwater Operation and Diving school, [[Kochi]] and Airborne Operation Air force School, [[New Delhi]].<ref name=2ndbatt>{{cite web|title=Kerala gets approval for 2nd battalion of Thunderbolts: CM|url=http://news.chennaionline.com/newsitem.aspx?NEWSID=7f5a0a82-294b-4fbd-9cf2-317a96d342fa=South|publisher=Chennai Online|accessdate=3 September 2013|date=24 August 2012}}</ref>

== Equipment ==

The following equipment is used by the Thunderbolts<ref name=TNE />

=== Weapons ===

* [[M4 carbine]]
* [[Heckler & Koch MP5]]
* [[Glock]] 19
* [[Steyr AUG]]
* [[IMI Tavor TAR-21|X95]]
* [[Taser]]
* [[AK-47]]
* [[INSAS]]

=== Communications ===

* Dicom Tactical Radio Sat phone

=== Uniform ===

* Green Camouflage for Jungle warfare
* Blue shade on Black for Operations
* Black for night operations
* Navy Blue Safari Suit for VVIP Security

=== Other equipment ===

* Thermal heat detectors
* Reflex sights

== See also ==
* [[Nilambur incident]]

== References ==
{{reflist}}

== External links ==
* https://www.youtube.com/watch?v=q9g5TIgSwLc - in Malayalam

[[Category:Counter-terrorist organizations]]
[[Category:Kerala Police]]