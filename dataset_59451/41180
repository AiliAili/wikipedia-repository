{{Taxobox
| status = LC
| status_system = IUCN3.1
| status_ref = <ref>{{IUCN|id=22696792 |title=''Phalacrocorax carbo'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
| image = Phalacrocorax carbo Vic.jpg
| image_caption = In [[Victoria (Australia)|Victoria]], Australia.
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Bird|Aves]]
| ordo = [[Suliformes]]
| familia = [[Phalacrocoracidae]]
| genus = ''[[Phalacrocorax]]''
| species = '''''P. carbo'''''
| binomial = ''Phalacrocorax carbo''
| binomial_authority = ([[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]])
| range_map = Great Cormorant Range.png
| range_map_caption = Global range{{leftlegend|#007F00|Year-Round Range|outline=gray}}{{leftlegend|#E0CF01|Summer Range|outline=gray}}{{leftlegend|#0080FF|Winter Range|outline=gray}}
}}

[[File:Aalscholver in broedkleed op ijs-4676907.webm|thumb|Adult great cormorant in breeding plumage. [[Texel]], Netherlands (2010)]]
The '''great cormorant''' (''Phalacrocorax carbo''), known as the '''great black cormorant''' across the Northern Hemisphere, the '''black cormorant''' in Australia, the '''large cormorant''' in India and the '''black shag''' further south in New Zealand, is a widespread member of the [[cormorant]] family of [[seabird]]s.<ref name=Ali1993/> The [[genus]] name is [[Latin]]ised [[Ancient Greek]], from φαλακρός (''phalakros'', "bald") and κόραξ (''korax'', "raven"), and ''carbo'' is Latin for "[[charcoal]]".<ref name= job90>{{cite book | last= Jobling | first= James A | year= 2010| title= The Helm Dictionary of Scientific Bird Names | publisher = Christopher Helm | location = London | isbn = 978-1-4081-2501-4 |pages =90, 301}}</ref>

It breeds in much of the [[Old World]] and the [[Atlantic Ocean|Atlantic]] coast of [[North America]].

==Description==
The great cormorant is a large black bird, but there is a wide variation in size in the species wide range. Weight is reported to vary from {{convert|1.5|kg|lb|abbr=on}}<ref name=Ribak2005/> to {{convert|5.3|kg|lb|abbr=on}}.<ref name=CanadianEncyclopedia/> Males are typically larger and heavier than females, with the nominate race (''P. c. carbo'') averaging about 10% larger in linear measurements than the smallest race in [[Europe]] (''P. c. sinensis'').<ref name=Koffijberg1995/> The lightest average weights cited are in [[Germany]] (''P. c. sinensis''), where 36 males averaged {{convert|2.28|kg|lb|abbr=on}} and 17 females averaged {{convert|1.94|kg|lb|abbr=on}}.<ref name=CRC/> The highest come from [[Prince Edward Island]] in [[Canada]] (''P. c. carbo''), where 11 males averaged {{convert|3.68|kg|lb|abbr=on}} and 11 females averaged {{convert|2.94|kg|lb|abbr=on}}.<ref name=Hogan1979/><ref name=Hatch/> Length can vary from {{convert|70|to|102|cm|in|abbr=on}} and wingspan from {{convert|121|to|160|cm|in|abbr=on}}.<ref name=Hatch/><ref name=Stevenson/> They are tied as the second largest extant species of cormorant after the [[flightless cormorant]], with the [[Japanese cormorant]] averaging at a similar size. In bulk if not in linear dimensions, the [[Blue-eyed shag]] species complex of the [[Southern Ocean]]s are scarcely smaller at average.<ref name=CRC/> It has a longish tail and yellow throat-patch. Adults have white patches on the thighs and on the throat in the breeding season. In [[Europe]]an waters it can be distinguished from the [[common shag]] by its larger size, heavier build, thicker bill, lack of a crest and [[Feather|plumage]] without any green tinge. In eastern North America, it is similarly larger and bulkier than [[double-crested cormorant]], and the latter species has more yellow on the throat and bill and lack the white thigh patches frequently seen on great cormorants. Great cormorants are mostly silent, but they make various guttural noises at their breeding colonies.
[[File:Phalacrocorax Carbo Albino 2.jpg|thumb|upright|Albino in [[Lake Kerkini]], [[Greece]]|left]]

===Variations===
A very rare variation of the great cormorant is caused by [[albinism]]. The ''Phalacrocorax carbo'' albino suffers from loss of eyesight and/or hearing, thus it rarely manages to survive in the wild.

==Distribution==
This is a very common and widespread [[bird]] species. It feeds on the sea, in [[Estuary|estuaries]], and on freshwater lakes and rivers. Northern birds [[bird migration|migrate]] south and winter along any coast that is well-supplied with [[fish]].

The type subspecies, ''P. c. carbo'', is found mainly in [[Atlantic]] waters and nearby inland areas: on western European coasts and south to North Africa, the [[Faroe Islands]], [[Iceland]] and [[Greenland]]; and on the eastern seaboard of [[North America]], though in America it breeds only in the north of its range, in the [[Canada|Canadian]] [[maritime provinces]].

The subspecies found in [[Australasia]]n waters, ''P. c. novaehollandiae'', has a crest. In [[New Zealand]], it is known as the '''black shag''' or by its [[Māori language|Māori]] name; '''kawau'''.<ref name=FGBNZ/> The [[syntype]] is in the collection of the [[Museum of New Zealand Te Papa Tongarewa]].<ref name=tepapa/>

The {{convert|80|–|100|cm|in|abbr=on}} long [[white-breasted cormorant]] ''P. c. lucidus'' found in sub-Saharan Africa, has a white neck and breast. It is often treated as a full species, ''Phalacrocorax lucidus'' (e.g. {{harvnb|Sibley|Monroe|1990}}, {{harvnb|Sinclair|Hockey|Tarboton|2002}}).

In addition to the Australasian and African forms, ''Phalacrocorax carbo novaehollandiae'' and ''P. c. lucidus'' mentioned above, other geographically distinct subspecies are recognised, including ''P. c. sinensis'' (western Europe to east [[Asia]]), ''P. c. maroccanus'' (north-western Africa), and ''P. c. hanedae'' ([[Japan]]).

Some authors{{who|date=December 2011}} treat all these as [[allospecies]] of a ''P. carbo'' superspecies group.

==Behaviour==
[[File:Phalacrocorax carbo MWNH 0534.JPG|left|thumb|Egg, Collection [[Museum Wiesbaden]]]]
[[File:Great Cormorant 0010 - swallowing eel - East Potomac Park - 2013-08-25.jpg|thumb|Cormorant swallowing a just caught eel]]
The great cormorant breeds mainly on coasts, nesting on cliffs or in trees (which are eventually killed by the droppings), but also increasingly inland. Three or four [[bird egg|eggs]] are laid in a nest of [[seaweed]] or twigs.

The great cormorant can dive to considerable depths, but often feeds in shallow water. It frequently brings prey to the surface. A wide variety of fish are taken: cormorants are often noticed eating [[eel]]s, but this may reflect the considerable time taken to subdue an eel and position it for swallowing, rather than any dominance of eels in the diet. In [[United Kingdom|British]] waters, dive times of 20–30 seconds are common, with a recovery time on the surface around a third of the dive time.

==Relationships with humans==
Many fishermen see in the great cormorant a competitor for fish. Because of this, it was hunted nearly to extinction in the past. Thanks to conservation efforts, its numbers increased. At the moment, there are about 1.2 million birds in Europe (based on winter counts; late summer counts would show higher numbers).<ref name=WICRG/> Increasing populations have once again brought the cormorant into conflict with fisheries.<ref name=FAO/><ref name=europarl/> For example, in Britain, where inland breeding was once uncommon, there are now increasing numbers of birds breeding inland, and many inland fish farms and fisheries now claim to be suffering high losses due to these birds. In the UK each year, some licences are issued to [[Culling|cull]] specified numbers of cormorants in order to help reduce predation; it is, however, still illegal to kill a bird without such a licence.
[[File:Cormorant fishing -Suzhou -China-6July2005.jpg|[[Cormorant fishing]] in [[Suzhou]], China|thumb|left]]
[[Cormorant fishing]] is practised in [[China]], [[Japan]], and elsewhere around the globe. In it, fishermen tie a line around the throats of cormorants, tight enough to prevent swallowing, and deploy them from small boats. The cormorants catch fish without being able to fully swallow them, and the fishermen are able to retrieve the fish simply by forcing open the cormorants' mouths, apparently engaging the regurgitation reflex.

In Norway, cormorant is a traditional game bird. Each year c. 10,000 cormorants are shot to be eaten.<ref name=intercafeproject/> In North Norway, cormorants are traditionally seen as semi-sacred.{{citation needed|date=June 2009}} It is regarded as good luck to have cormorants gather near your village or settlement. An old legend states that people who die far out at sea, their bodies never recovered, spend eternity on the island Utrøst – which can only occasionally be found by mortals. The inhabitants of Utrøst can only visit their homes in the shape of cormorants.
{{Clear}}


<gallery>
Great cormorant (Phalacrocorax carbo) immature.jpg|immature, [[Kazinga Channel]], [[Uganda]]
File:Storskarv - Kranium - Phalacrocorax carbo.jpg|Cranium
File: Phalacrocorax carbo ja01.jpg|Drying after fishing
File:Great cormorant (Phalacrocorax carbo).JPG|On [[Farmoor Reservoir]], [[Oxfordshire]]
File:Great cormorant (Phalacrocorax carbo) in flight.jpg|Above [[Farmoor Reservoir]], [[Oxfordshire]]
File:Lithuania Juodkrante Great Cormorant colony 1.jpg|Colony in [[Juodkrantė]], Lithuania, and damage to the trees in which they are nesting
File:Aalscholver Phalacrocorax carbo Jos Zwarts 2.tif|Drawing by Jos Zwarts
File:Kormorane in lake Vistonis.jpg|Kormorane in lake Vistonis, Greece
</gallery>

==Videos==
<gallery>
File:Aalscholver op havenpaal-4676903.webm|Resting on a post in a port in Den Oever, the Netherlands
File:Aalscholver op paal-4676900.webm|Stretching wings while sitting on a pole
File:Chasse du Grand Cormoran.webm|Great cormorant hunting in Odessa
</gallery>

==References==
<!-- AnimalBehavior51:273,1197. BiologyLetters1:469. Forktail16:147. RevistaBrasileiraDeZoologia23:807. Sylvia41:137. -->
{{reflist|refs=
<ref name=Ali1993>{{cite book |last=Ali |first=S. |year=1993 |title=The Book of Indian Birds |publisher=Bombay Natural History Society |location=Bombay |isbn=978-0-19-563731-1}}</ref>

<ref name=CanadianEncyclopedia>{{cite web |url=http://www.thecanadianencyclopedia.com/en/article/cormorant/ |title=Cormorant |website=[http://www.thecanadianencyclopedia.com The Canadian Encyclopedia] |accessdate=21 August 2012}}</ref>

<ref name=CRC>{{cite book |title=CRC Handbook of Avian Body Masses |editor-first=John B. |editor-last=Dunning Jr. |publisher=CRC Press |year=1992 |isbn=978-0-8493-4258-5}}</ref>

<ref name=europarl>{{cite web |title=European Parliament resolution |date=4 December 2008 |url=http://www.europarl.europa.eu/sides/getDoc.do?type=TA&reference=P6-TA-2008-0583&language=EN&ring=A6-2008-0434 |quote="on the adoption of a European Cormorant Management Plan to minimise the increasing impact of cormorants on fish stocks, fishing and aquaculture" }}</ref>

<ref name=FAO>{{cite web |url=ftp://ftp.fao.org/docrep/fao/010/i0210e/i0210e00.pdf |title=Workshop on a European Cormorant management Plan, 20–21 November 2007 |publisher=EIFAC, European Inland Fisheries Advisory Commission}}</ref>

<ref name=FGBNZ>{{cite book |first1=Barrie |last1=Heather |first2=Hugh |last2=Robertson |title=The Field guide to the Birds of New Zealand |edition=revised |publisher=Viking |year=2005 |isbn=978-0143020400}}</ref>

<ref name=Hatch>{{cite web |last1=Hatch |first1=Jeremy J. |first2=Kevin M. |last2=Brown |first3=Geoffrey G. |last3=Hogan |first4=Ralph D. |last4=Morris |year=2000 |title=Great Cormorant (Phalacrocorax carbo) |work=The Birds of North America Online |editor-first=A. |editor-last=Poole |location=Ithaca |publisher=Cornell Lab of Ornithology |url=http://bna.birds.cornell.edu/bna/species/553 |doi=10.2173/bna.553}}</ref>

<ref name=Hogan1979>{{Cite thesis |last=Hogan |first=G. |title=Breeding parameters of Great Cormorants (Phalacrocorax carbo) at mixed species colonies on Prince Edward Island, Canada |type=Master's Thesis |url=http://hdl.handle.net/10464/1789 |year=1979 |publisher=Brock University |location=St. Catharines, ON }}</ref>

<ref name=intercafeproject>{{cite web |url=http://www.intercafeproject.net/pdf/REDCAFEFINALREPORT.pdf |title=Reducing the conflict between Cormorants and fisheries on a pan-European scale |publisher=REDCAFE |work=Final Report |page=12 |quote="Around 10,000 adult Cormorants (of the ‘Atlantic’ carbo race) are hunted legally as game in Norway outside the breeding season."}}</ref>

<ref name=Koffijberg1995>{{cite journal |last1=Koffijberg |first1=K. |first2=M.R. |last2=Van Eerden |year=1995 |title=Sexual dimorphism in the cormorant Phalacrocorax carbo sinensis: possible implications for differences in structural size |journal=Ardea |volume=83 |pages=37–46 |url=http://www.avibirds.com/pdf/A/Aalscholver27.pdf}}</ref>

<ref name=Ribak2005>{{cite journal |last1=Ribak |first1=Gal |last2=Weihs |first2=Daniel |last3=Arad |first3=Zeev |title=Water retention in the plumage of diving great cormorants Phalacrocorax carbo sinensis |year=2005 |journal=Journal of Avian Biology |volume=36 |issue=2 |pages=89 |doi=10.1111/j.0908-8857.2005.03499.x }}</ref>

<ref name=Stevenson>{{Cite book |title=Field Guide to the Birds of East Africa: Kenya, Tanzania, Uganda, Rwanda, Burundi |last1=Stevenson |first1=Terry |last2=Fanshawe |first2=John |publisher=Elsevier Science |year=2001 |isbn=978-0-85661-079-0}}</ref>

<ref name=tepapa>{{cite web |title=Phalacrocorax carbo novaehollandiae; syntype |work=Collections Online |publisher=Museum of New Zealand Te Papa Tongarewa |url=http://collections.tepapa.govt.nz/objectdetails.aspx?oid=358520 |accessdate=18 July 2010}}</ref>

<ref name=WICRG>{{cite web |publisher=Wetland International Cormorant Research Group |url=http://web.tiscali.it/sv2001/Cormorant_Counts_2003-2006_Summary.pdf |title=Cormorants in the western Palearctic, Distribution and numbers on a wider European scale |archiveurl=https://web.archive.org/web/20110607140951/http://web.tiscali.it/sv2001/Cormorant_Counts_2003-2006_Summary.pdf |archivedate=7 June 2011 |deadurl=yes}}</ref>
}}

==Further reading==
* {{cite book |last1=Sibley |first1=C.G. |last2=Monroe |first2=B.L. |year=1990 |title=Distribution and taxonomy of birds of the world |location=New Haven CT |publisher=Yale University Press |ref=harv}}
* {{cite book |first1=Ian |last1=Sinclair |first2=Phil |last2=Hockey |first3=Warwick |last3=Tarboton |title=SASOL Birds of Southern Africa |publisher=Struik |year=2002 |isbn=1-86872-721-1 |ref=harv}}

;Separation of ''carbo'' and ''sinensis''
* {{cite journal |last1=Newson |first1=Stuart |first2=Graham |last2=Ekins |first3=Baz |last3=Hughes |first4=Ian |last4=Russell |first5=Robin |last5=Sellers |year=2005 |title=Separation of North Atlantic and Continental Cormorants |journal=[[Birding World]] |volume=18 |issue=3 |pages=107–111}}
* {{Cite journal |last=MIllington |first=Richard |authorlink=Millington, Richard |year=2005 |title=Identification of North Atlantic and Continental Cormorants |journal=[[Birding World]] |volume=18 |issue=3 |pages=112–123}}
* Murray, T and Cabot, D. (2015). The Breeding Status of Great Cormorant (''Phalacrocorax carbo carbo'') in Co. Wexford. ''Ir. Nat. J.'' '''34''': 89-94.

==External links==
{{commons category|Phalacrocorax carbo}}
{{wikispecies|Phalacrocorax carbo}}
* [http://aulaenred.ibercaja.es/wp-content/uploads/29_CormorantPcarbo.pdf Ageing and sexing (PDF) by Javier Blasco-Zumeta & Gerd-Michael Heinze]
* [http://sabap2.adu.org.za/docs/sabap1/055.pdf Great cormorant Species text in The Atlas of Southern African Birds]
* {{BirdLife|22696792|Phalacrocorax carbo}}
* {{Avibase|name=Phalacrocorax carbo}}
* {{InternetBirdCollection|great-cormorant-phalacrocorax-carbo}}
* {{VIREO|Great+cormorant}}
* {{IUCN_Map|22696792|Phalacrocorax carbo}}
* {{Xeno-canto species|Phalacrocorax|carbo|Great cormorant}}

{{Suliformes}}

{{taxonbar}}
{{Authority control}}

{{DEFAULTSORT:cormorant, great}}
[[Category:Phalacrocorax|great cormorant]]
[[Category:Cosmopolitan birds]]
[[Category:Birds of Japan]]
[[Category:Birds described in 1758|great cormorant]]
[[Category:Articles containing video clips]]