{{Accounting}}

A '''trial balance''' is a list of all the [[general ledger]] accounts (both revenue and capital) contained in the ledger of a business. This list will contain the name of each nominal ledger account and the value of that nominal ledger balance. Each nominal ledger account will hold either a debit balance or a credit balance. The debit balance values will be listed in the [[Debits and credits|debit]] column of the trial balance and the [[Debits and credits|credit]] value balance will be listed in the credit column. The trading profit and loss statement and [[balance sheet]] and other financial reports can then be produced using the ledger accounts listed on the trial balance.

== History ==
The process was first described by [[Luca Pacioli]] in the 1494 work ''Particularis de Computis et Scripturis''. Although it did not use the term, he essentially prescribed a technique similar to a post-closing trial balance.<ref name="ChatfieldVangermeersch2014">{{cite book|author1=Michael Chatfield|author2=Richard Vangermeersch|title=The History of Accounting (RLE Accounting): An International Encylopedia|url=https://books.google.com/books?id=DmnMAgAAQBAJ&pg=PA587|date=5 February 2014|publisher=Routledge|isbn=978-1-134-67545-6|pages=587–}}</ref>

== Usage ==
The purpose of a trial balance is to prove that the value of all the debit value balances equal the total of all the credit value balances. If the total of the debit column does not equal the total value of the credit column then this would show that there is an error in the nominal ledger accounts. This error must be found before a profit and loss statement and balance sheet can be produced.

The trial balance is usually prepared by a bookkeeper or accountant who has used [[Double-entry bookkeeping system#Purchase invoice daybook|daybooks]] to record financial transactions and then post them to the nominal ledgers and personal ledger accounts. The trial balance is a part of the [[double-entry bookkeeping]] system and uses the classic [[Debits and credits#"T" accounts|'T' account]] format for presenting values.

== Limitations ==
A trial balance only checks the sum of debits against the sum of credits. That is why it does not guarantee that there are no errors. The following are the main classes of errors that are not detected by the trial balance.

*An '''error of original entry''' is when both sides of a transaction include the wrong amount.<ref name="errors">{{cite book|title=AAT Foundation - Course Companion - Units 1 - 4|publisher=BPP Professional Education|date=April 2004|edition=Fourth|isbn=0-7517-1583-2|page=411}}</ref> For example, if a purchase invoice for £21 is entered as £12, this will result in an incorrect debit entry (to purchases), and an incorrect credit entry (to the relevant creditor account), both for £9 less, so the total of both columns will be £9 less, and will thus balance.
*An '''error of omission''' is when a transaction is completely omitted from the accounting records.<ref name="Hanif2001">{{cite book|author=Mohammed Hanif|title=Modern Acc. Vol I, 2E|url=https://books.google.com/books?id=ktvMGB1s2u4C&pg=SA4-PA2|date=1 May 2001|publisher=Tata McGraw-Hill Education|isbn=978-0-07-463017-4|pages=4.15}}</ref> As the debits and credits for the transaction would balance, omitting it would still leave the totals balanced. A variation of this error is omitting one of the ledger account totals from the trial balance (but in this case the trial balance will not balance).
*An '''error of reversal''' is when entries are made to the correct amount, but with debits instead of credits, and vice versa.<ref name="IzharHontoir2001">{{cite book|author1=Riad Izhar|author2=Janet Hontoir|title=Accounting, Costing and Management|url=https://books.google.com/books?id=OC_R99HPsosC&pg=PA61|year=2001|publisher=Oxford University Press|isbn=978-0-19-832823-0|pages=61–62}}</ref> For example, if a cash sale for £100 is debited to the Sales account, and credited to the Cash account. Such an error will not affect the totals.
*An '''error of commission''' is when the entries are made at the correct amount, and the appropriate side (debit or credit), but one or more entries are made to the wrong account of the correct type.<ref name="KumarSharma2001">{{cite book|author1=Arun Kumar|author2=Rachana Sharma|title=Auditing: Theory and Practice|url=https://books.google.com/books?id=XPraqutMtLgC&pg=PA20|date=1 January 2001|publisher=Atlantic Publishers & Dist|isbn=978-81-7156-720-1|pages=20–}}</ref> For example, if fuel costs are incorrectly debited to the postage account (both expense accounts). This will not affect the totals.
*An '''error of principle''' is when the entries are made to the correct amount, and the appropriate side (debit or credit), as with an error of commission, but the ''wrong'' type of account is used. For example, if fuel costs (an expense account), are debited to stock (an asset account).<ref name="Hanif2001"/> This will not affect the totals. 
*'''Compensating errors''' are multiple unrelated errors that would individually lead to an imbalance, but together cancel each other out.<ref name="Hanif2001"/>

==References==
{{Library resources box 
|by=no 
|onlinebooks=no 
|others=no 
|about=yes 
|label=Trial balance }}
{{reflist}}

[[Category:Financial statements]]
[[Category:Accounting terminology]]

[[de:Zwischenberichterstattung#Zwischenberichterstattung und internes Berichtswesen]]