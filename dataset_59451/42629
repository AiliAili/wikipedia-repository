<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=DAR-23
 | image=File:Dar23n002.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft|Amateur-built]] [[ultralight aircraft]]
 | national origin=[[Bulgaria]]
 | manufacturer=[[Aeroplanes DAR]]
 | designer=
 | first flight=August 2001
 | introduced=2001
 | retired=
 | status=Under re-development (March 2014)<ref name="aeroplanesdar.com">{{cite web|url=http://aeroplanesdar.com/products/dar-23/|title=DAR 23 «  AeroplanesDAR|work=aeroplanesdar.com|accessdate=4 October 2015}}</ref>
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=At least two prototypes
 | program cost= <!--Total program cost-->
 | unit cost= <!-- [[US$]] (date) -->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''DAR-23''' is a [[Bulgaria]]n [[ultralight aircraft]], designed and produced by [[Aeroplanes DAR]], first flying in August 2001.<ref name="Hist">{{cite web|url = http://aeroplanesdar.com/company-history/|title = Company History|accessdate = 21 September 2015|author = [[Aeroplanes DAR]]|year = 2014}}</ref>

The aircraft is supplied as a [[Homebuilt aircraft|kit]] for amateur construction.<ref name="KitplanesDec2012">Bernard, Mary and Suzanne B. Bopp: ''Aeroplanes DAR-23'', Kitplanes, Volume 29, Number 12, December 2012, page 26. Belvoir Publications. ISSN 0891-1851</ref>

The DAR-23 project started in December 2000, with the prototype completed in August 2001. The design was first displayed in North America at [[AirVenture]] in July 2012. There is no indication that any more than two prototypes have been completed or that series production has commenced.<ref name="KitplanesDec2012" /><ref name="DARAbout">{{cite web|url=http://www.aeroplanesdar.com/company-profile.php |title=Company Profile |accessdate=24 November 2012 |last=[[Aeroplanes DAR]] |year=2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121111010525/http://www.aeroplanesdar.com:80/company-profile.php |archivedate=11 November 2012 |df= }}</ref>

In March 2014 the company indicated that the design was being re-developed.<ref name="aeroplanesdar.com"/>

==Design and development==
The DAR-23 features a [[strut-braced]] [[high-wing]], a two-seats-in-[[side-by-side configuration]] open cockpit with a windsield, fixed [[tricycle landing gear]] and a single engine in [[tractor configuration]], mounted high above the cockpit.<ref name="KitplanesDec2012" />

The aircraft structure is made from a combination of 1050, 2024, 3130 and 6164 [[aluminum]] tubing, with the main beam, [[Aircraft fairing|cockpit pod]] and main [[landing gear]] made from composites. The landing gear is pyramidal in design. Its all-metal wing employs composite [[flaperon]]s. Controls are actuated by push-pull cables, while the [[Elevator (aircraft)|elevator]] [[Trim tab|trim]] is electric. The nosewheel is steerable. The aircraft's recommended engine power range is {{convert|50|to|64|hp|kW|0|abbr=on}} and standard engines used include the {{convert|50|hp|kW|0|abbr=on}} [[Hirth F-23]], the {{convert|50|hp|kW|0|abbr=on}} [[Rotax 503]] and the {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] [[two-stroke]] powerplants. Construction time from the supplied standard kit is estimated as 350 hours, although a quick-build kit is also available.<ref name="KitplanesDec2012" /><ref name="DAR">{{cite web|url=http://www.aeroplanesdar.com/dar-23.php |title=DAR-23 |accessdate=21 November 2012 |last=[[Aeroplanes DAR]] |year=2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121011184938/http://www.aeroplanesdar.com:80/dar-23.php |archivedate=11 October 2012 |df= }}</ref>
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (DAR-23) ==
{{Aero specs missing}}
{{Aircraft specs
|ref=Kitplanes and DAR<ref name="KitplanesDec2012" /><ref name="DAR" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=750
|gross weight note=
|fuel capacity={{convert|22|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hirth F-23]]
|eng1 type=twin cylinder, air-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=50<!-- prop engines -->

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=Y

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{commons category|DAR}}
*{{Official website|http://aeroplanesdar.com/products/dar-23/}}
{{DAR aircraft}}

[[Category:Homebuilt aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Bulgarian ultralight aircraft 2010–2019]]
[[Category:DAR aircraft|DAR 23]]
[[Category:High-wing aircraft]]