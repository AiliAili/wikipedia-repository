{{Infobox scientist
|image = Pablo DT Valenzuela.JPG
| name                    = Pablo Valenzuela
| birth_date              = {{birth date and age|1941|6|13}}<ref>{{cite web|title=Pablo DT Valenzuela|url=http://www.whoislog.info/profile/pablo-dt-valenzuela.html|accessdate=19 January 2013}}</ref> 
| birth_place             = [[Santiago]], <br>[[Chile]]
| death_date              = 
| death_place             = 
| residence               = 
| citizenship             =
| nationality             = [[Chile]]an
| ethnicity               =
| field                   = [[Biotechnology]]
| work_institution        = Fundacion Ciencia Para la Vida<br>[[Pontifical Catholic University of Chile]]<br>[[Universidad Nacional Andres Bello]]<br>[[Universidad San Sebastian]]
| alma_mater              = [[University of Chile]], <br> [[Northwestern University]]
| doctoral_advisor        = 
| doctoral_students       = 
| known_for               = Molecular genetic studies of HBV, HCV, HIV and the invention of the first recombinant vaccine, against HBV. Directed scientists which discovered HCV at Chiron Corporation.
| author_abbreviation_bot = 
| author_abbreviation_zoo = 
| prizes                  = Chilean National Award in Applied Sciences and Technologies (2002)
| signature               = 
| religion                = 
| footnotes               = 
}}

'''Pablo DT Valenzuela''' (born June 13, 1941) is a Chilean [[biochemist]] dedicated to biotechnology development.<ref>[http://www.thisischile.cl/Articles.aspx?id=2062&sec=362&itz=interface-acerca-gente-chilenosdestacados&eje=Acerca&idioma=2&t=pablo-valenzuela-valdes This is Chile]</ref> He is known for his genetic studies of [[hepatitis virus]]es; participated as R&D Director in the discovery of [[hepatitis C virus]] and the invention of the [[Hepatitis B vaccine|world's first recombinant vaccine]] (against [[hepatitis B virus]]). He is one of the cofounders of the biotechnology company [[Chiron Corporation]]<ref>[http://www.chemeurope.com/en/encyclopedia/Chiron_Corporation.html Chiron Corporation History]</ref> and of [[Fundacion Ciencia para la Vida]],<ref>[http://www.cienciavida.cl/ Fundacion Ciencia Para la Vida]</ref> a private non profit institution where he is currently working.

== Biography==
Pablo Valenzuela studied biochemistry at Universidad de Chile and earned his Ph.D. degree (1970) in Chemistry at [[Northwestern University]], did a postdoctoral training at [[University of California]], San Francisco and held a position as Professor in the Biochemistry Department of that institution. In 1981, together with [[William J. Rutter]] and Edward Penhoet founded the biotechnology company [[Chiron Corporation]] that in 1997 was the second-largest biotechnology company in the world, after [[Amgen]].<ref>[https://select.nytimes.com/gst/abstract.html?res=FA0611FA3D590C738FDDA80894DF494D81&scp=2&sq=Pablo%20DT%20Valenzuela&st=cse New York Times]</ref> As Research Director Pablo Valenzuela developed a variety of biotechnological products, specially in the blood banking industry.<ref>[https://www.nytimes.com/1986/10/13/business/biotechnology-spotlight-now-shines-on-chiron.html  Biotechnology Spotlight Now Shines On Chiron The New York Times]</ref><ref>[https://www.nytimes.com/1993/04/04/business/a-new-model-for-biotechnology.html?scp=9&sq=Pablo%20DT%20Valenzuela&st=cse A New Model for Biotechnology]</ref> The invention of the recombinant vaccine against Hepatitis B virus was chosen by Business Week as one of the three most innovative technological products of the year 1986. In Chile Pablo Valenzuela founded Bios Chile, the first biotechnology company in that country, and in 1997 together with Bernardita Mendez he cofounded Fundacion Ciencia Para la Vida a non profit foundation that carries out scientific and technological research. He is the father of Chilean American singer/songwriter, [[Francisca Valenzuela]].

==Work==
Pablo DT Valenzuela is the scientist responsible for the development of biotechnology products in USA and Chile in the area of international diagnostics, blood banking and pharmaceutical.  He is cofounder and responsible of early activities of biotechnology Start-ups in USA and Chile. Pablo Valenzuela is also Professor and Investigator in graduate programs, generating scientific publications and patents.<br>
Pablo Valenzuela was recipient of the Chilean National Prize in Applied Sciences in 2002 and is member of the Chilean Academy of Sciences.

===Science & Technology Developments in USA===
* Cofounder of [[Chiron Corporation]].
* Ten products in the area of international diagnostics and blood banking ([[Hepatitis C]], [[AIDS]], [[Hepatitis B]]) that have been commercialized by [[Chiron Corporation]], [[Abbott Laboratories|Abbott]] and [[Johnson & Johnson]].
* Four products in the pharmaceutical area ([[Hepatitis B vaccine]], human [[insulin]], [[beta-interferon]]<ref>[https://www.nytimes.com/1993/07/24/business/company-news-fda-approves-a-multiple-sclerosis-drug.html F.D.A. Approves a Multiple Sclerosis Drug]</ref> and a wound healing gel based on [[PDGF]]) and one product for the food industry (bovine [[rennin]]), commercialized worldwide by [[Chr. Hansen]].

=== Science & Technology Developments in Chile ===
* Co-Founder and President of Bios Chile<ref>[http://www.grupobios.cl/grupo-bios/en Bios Chile]</ref> and Austral Biologicals.<ref>[http://www.australbiologicals.com/index.php Austral Biologicals]</ref>
* Co-Founder and Director of Fundación Ciencia para la Vida.
* Co-Founder and President of Andes Biotechnologies (start-up 2009).
* Biotechnology products, like vaccines for the salmon aquiculture,  licensed to [[Novartis]].
* Products for human health and blood banking ([[Chagas disease|Chagas]], [[Helicobacter pylori|H. pylori]], [[rotavirus]], blood tests).<ref>[http://www.grupobios.cl/productos/en GrupoBios Products]</ref>
* Responsible Director of the Millennium Institute for Fundamental and Applied Biology, one of 15 centers of excellence in Chile.<ref>[http://www.institutomilenio.cl/ Millennium Institute]</ref>

== References ==
{{Reflist|2}}

== External links ==
* [http://www.cienciavida.cl/ Fundacion Ciencia para la Vida]
*[http://www.ibiology.org/ibiomagazine/issue-3/pablo-valenzuela-life-sciences-in-chile.html Pablo Valenzuela's short talk: "Life Sciences in Chile" (also in Spanish)]

{{DEFAULTSORT:Valenzuela, Pablo Dt}}
[[Category:1941 births]]
[[Category:Living people]]
[[Category:Biochemists]]
[[Category:Chilean scientists]]