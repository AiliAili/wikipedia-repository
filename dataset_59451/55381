{{Infobox journal
| title = Agriculture, Ecosystems & Environment
| cover = 
| formernames = Agriculture and Environment, Agro-Ecosystems
| editor = J. Fuhrer
| discipline = [[Agroecosystems]], [[ecology]], [[environmental science]]
| abbreviation = Agric. Ecosyst. Environ.
| publisher = [[Elsevier]]
| country =
| frequency = 18/year
| history = 1974-present
| openaccess = 
| license = 
| impact = 2.859
| impact-year = 2012
| website = http://www.journals.elsevier.com/agriculture-ecosystems-and-environment/
| link1 = http://www.sciencedirect.com/science/journal/01678809
| link1-name = Online access
| link2 = http://www.sciencedirect.com/science/journal/03041131
| link2-name = Online archives, ''Agriculture and Environment''
| link3 = http://www.sciencedirect.com/science/journal/03043746
| link3-name = Online archives, ''Agro-Ecosystems''
| JSTOR = 
| OCLC = 9506512
| LCCN = 83643491 
| CODEN = AEENDO
| ISSN = 0167-8809
| ISSN2 = 0304-1131
| ISSN2label = ''Agriculture and Environment'':
| ISSN3 = 0304-3746
| ISSN3label = ''Agro-Ecosystems'':
| eISSN = 
}}
'''''Agriculture, Ecosystems & Environment''''' is a [[peer-reviewed]] [[scientific journal]] published eighteen times per year by [[Elsevier]]. It covers research on the interrelationships between the [[natural environment]]s and [[agroecosystems]], and their effects on each other. The [[editor-in-chief]] is J. Fuhrer ([[Agroscope]]).

== History ==
The journal in its current form originated in 1983 from a merger between ''Agriculture and Environment'' and ''Agro-Ecosystems'', both of which were established in 1974.<ref name=LC>{{cite web |url=http://lccn.loc.gov/83643491 |title=Agriculture, Ecosystems & Environment |work=Library of Congress Catalog |publisher=[[Library of Congress]] |format= |accessdate=2014-06-28}}</ref>

==Abstracting and indexing==
This journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[AGRICOLA]]
* [[BIOSIS]]
* [[Elsevier BIOBASE]]
* [[Current Contents]]/Agriculture, Biology & Environmental Sciences
* [[CSA Illumina|Environmental Abstracts]]/[[Environmental Sciences and Pollution Management]]
* [[GEOBASE]]
* [[Science Citation Index]]
* [[Scopus]]
* [[EMBiology]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 2.859.<ref name=WoS>{{cite book |year=2013 |chapter=Agriculture, Ecosystems & Environment|title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{official website|http://www.journals.elsevier.com/agriculture-ecosystems-and-environment/}}

{{DEFAULTSORT:Agriculture, Ecosystems and Environment}}
[[Category:Elsevier academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1974]]
[[Category:Agricultural journals]]
[[Category:Environmental science journals]]