<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= Embraer E-Jet family <br />E170 / E175 / E190 / E195
 |image= File:EI-RDB Embraer 175 Alitalia BCN.jpg
 |alt= A white, green and red Embraer E175 aircraft in landing configuration.
|caption= [[Alitalia CityLiner]] Embraer E175
 }}{{Infobox aircraft type
 |more users=[[Azul Brazilian Airlines]]<br> [[JetBlue Airways]] <br/>[[Compass Airlines (North America)|Compass Airlines]]<!--Only three (3) "more users" total; please separate with <br/> -->
 |developed into= [[Embraer E-Jet E2 family]]
|type= [[Narrow-body aircraft|Narrow-body]] [[jet airliner]]
 |national origin = [[Brazil]]
 |manufacturer= [[Embraer]]
 |designer=
 |first flight= February 19, 2002
 |introduced= March 17, 2004 with [[LOT Polish Airlines]]
 |retired=
 |status=In service
 |primary user= [[Republic Airlines]]<!--Limit one (1) primary user. Top 4 users listed in 'primary user' and 'more users' fields based on number of 737s in their fleets. -->
 |produced= 2001–present
 |number built=1300<br/>{{as of|2016|12|31}}<ref>{{cite web|url=http://www.embraer.com/Documents/noticias/001%20embraer%20deliveries%204q16-ins-vpf-i-17%20vale%20este.pdf|title= Embraer reaches targets with delivery of 108 commercial jets and 117 executive jets in 2016 |work=embraer.com}}</ref> 
|unit cost= E170: [[US$]]38.7  million <br/>E190: US$46.2 million<ref>[https://www.bloomberg.com/news/articles/2013-06-18/embraer-wins-709-million-of-orders-for-current-e-jets Embraer CEO Sees U.S. Airlines Back for More Amid Orders]
Bloomberg business by Tim Catts, Robert Wall and Leslie Picker, 
June 18, 2013</ref><br/>E195: US$47.0 million<ref>{{cite web|url=http://www.flightglobal.com/news/articles/analysis-lessors-spurn-cseries-overtures-370412/|title=ANALYSIS: Lessors spurn CSeries overtures|author=Reed Business Information Limited|work=flightglobal.com|accessdate=25 January 2015}}</ref><br/>(2012 prices)
 |variants with their own articles= [[Embraer Lineage 1000]]
 }}
|}

The '''Embraer E-Jet family''' is a series of [[narrow-body aircraft|narrow-body]] medium-range [[twinjet|twin-engine]] [[jet airliner]]s produced by [[Brazil]]ian aerospace [[Conglomerate (company)|conglomerate]] [[Embraer]]. Launched at the [[Paris Air Show]] in 1999, and entering production in 2002, the aircraft series has been a commercial success primarily for its safety and efficiency.<ref>{{cite web |url= https://www.flightglobal.com/news/articles/embraer-e-jet-sales-success-spurred-deployment-hiccups-213920/ |title= Embraer: E-Jet sales success spurred deployment hiccups-22/05/2007-Washington DC-Flight International |accessdate=2016-06-23}}</ref> The aircraft is used by mainline and regional airlines around the world. {{As of|2015|9|30}}, there is a backlog of 263 firm orders for the E-Jets, 433 [[Option (aircraft purchasing)|options]] and 1158 units delivered.<ref name="Embraer">{{cite web |title= Embraer In Numbers |publisher= Embraer.com |url= http://www.embraer.com.br/en-US/ConhecaEmbraer/EmbraerNumeros/Pages/Home.aspx |accessdate= 2012-10-16}}</ref>

==Design and development==
The Embraer E-Jets line is composed of two main commercial families and a [[Bizjet|business jet]] variant. The smaller E170 and E175 make up the base model aircraft. The E190 and E195 are stretched versions, with different [[Jet Engine|engines]] and larger [[wing]], [[horizontal stabilizer]] and [[landing gear]] structures. The 170 and 175 share 95% commonality, as do the 190 and 195. The two families share near 89% commonality, with identical [[fuselage]] cross-sections and [[avionics]], featuring the [[Honeywell Primus]] Epic [[Electronic flight instrument system]] (EFIS) suite.<ref name=JetBlue/>  The E-jets also have [[winglets]] to improve efficiency.

All E-Jets use four-abreast seating (2+2) and have a "double-bubble" design, which Embraer developed for its commercial passenger jets, that provides stand-up headroom. The E190/195 series of aircraft have capacities similar to the initial versions of the [[McDonnell Douglas DC-9]] and [[Boeing 737]], which have always been considered mainline airliners.<ref name=":0">{{Cite web|title = Aircraft ICAO and IATA codes: Embraer|url = http://www.airlineupdate.com/content_public/codes/aircraft/embraer.htm|website = www.airlineupdate.com|accessdate = 2015-12-29}}</ref> The E-Jets have turbofan engines designed to reduce noise, which allows them to operate in airports that have strict noise restrictions, such as [[London City Airport]].<ref>[http://www.londoncityairport.com/content/pdf/Types%20of%20Aircraft_and_Concessions.pdf List of permitted aircraft on LCY]</ref>

Embraer first disclosed that it was studying a new 70-seat aircraft, which it called the EMB 170, in 1997, concurrently with announcing the development of its [[Embraer ERJ 145 family|ERJ 135]].<ref>{{cite journal |last=Walker |first=Karen |last2=Lopez |first2=Ramon |date=12–18 March 1997 |title= Embraer seeks Paris show launch for new 37-seat regional turbofan |journal=Flight International |publisher=Reed Business Information |volume=151 |issue=4565 |pages=4 |accessdate=30 October 2013}}</ref> The EMB 170 was to feature a new wing and larger-diameter fuselage mated to the nose and cockpit of the [[Embraer ERJ 145 family|ERJ 145]].<ref>{{cite journal |author=<!--Staff writer(s); no by-line.--> |date=25 June – 1 July 1997 |title=Paris report '97: Continental Express is eager for small regional jet |journal=Flight International |publisher=Reed Business Information |volume=151 |issue=4579 |pages=10 |accessdate=30 October 2013}}</ref><ref>{{cite journal |last=Warwick |first=Graham |date=24–30 September 1997 |title= Embraer launches 37-seater |journal=Flight International |publisher=Reed Business Information |volume=152 |issue=4593 |pages=7 |accessdate=30 October 2013}}</ref> In February 1999, Embraer announced it had abandoned the derivative approach in favour of an all-new design.<ref>Endres (2001), p.288</ref><ref>Lewis (2001), p.34</ref>

The E-jet family was formally launched at the [[Paris Air Show]] on 14 June 1999<ref name="centrohistoricoembraer.com.br">http://www.centrohistoricoembraer.com.br/pt-BR/HistoriaAeronaves/Paginas/EMBRAER-170.aspx</ref> as ERJ-170 and ERJ-190, designations later changed to EMBRAER 170 and EMBRAER 190. Launch customers for the aircraft were the French airline [[Régional Compagnie Aérienne Européenne]] with ten orders and five options for the E170;<ref name=":0" /> and the Swiss airline [[Crossair]] with an order for 30 E170s and 30 E190s.<ref>{{cite web |archiveurl= https://web.archive.org/web/20100203181350/http://www.flug-revue.rotor.com/FRTypen/FRERJ170.htm |title= Embraer ERJ-170 |publisher= Flug Revue |archivedate= 3 February 2010 |deadurl=Y|url= http://www.flug-revue.rotor.com/FRTypen/FRERJ170.htm }}</ref>  
Production of parts to build the prototype and test airframes began in July 2000.<ref>Endres (2001), p. 289.</ref> The first prototype (PP-XJE)<ref>http://www.airfleets.net/ficheapp/plane-e170-1.htm</ref> rolled out on October 29, 2001 <ref name="centrohistoricoembraer.com.br"/> at São José dos Campos, Brazil. Its first flight occurred on February 19, 2002, marking the beginning of a multi-year flight test campaign. Full production began in 2002, at a new factory built by Embraer at its [[São José dos Campos]] base.<ref name="FGFactoryp50">Kingsley-Jones and Wastnage (2001). p. 50.</ref>  After several delays in the certification process, the E170 received [[Type certificate|type certification]] from the [[National aviation authority|aviation authorities]] of Brazil, Europe and the United States in February 2004.<ref name="Embraer 170 Commercial Jetliner"/><ref name="AINdelay"/>

===E-Jets Second Generation===
{{main|Embraer E-Jet E2 family}}

In November 2011, Embraer announced that it would develop revamped versions of the E-Jets family with improved engines, rather than an all-new aircraft.<ref name=FI_Embraer_re-engined_E-Jets>{{cite web |title= Embraer Commits to re-engined E-Jets |url= http://www.flightglobal.com/news/articles/embraer-commits-to-re-engined-e-jets-364603/ |accessdate = November 12, 2011}}</ref>  The new variants are to be powered by new more efficient engines with larger diameter fans, and include slightly taller landing gear, and possibly a new [[aluminum]] or [[Carbon-fiber-reinforced polymer|carbon fiber]]-based wing. The new E-Jet variants are to be better-positioned to compete with the Bombardier CSeries. The new variants are to enter service in 2018.<ref name=Embraer_Selects>{{cite web |title = Embraer Selects Pratt & Whitney’s PurePower Engines for Second Generation of E-Jets |publisher=Embraer |url= http://www.embraer.com.br/en-US/ImprensaEventos/Press-releases/noticias/Pages/Embraer-seleciona-os-motores-PurePower-da-Pratt-E-Whitney-para-a-segunda-geracao-de-E-Jets.aspx |date= January 8, 2013 |accessdate= March 5, 2013}}</ref>

GE, [[Pratt & Whitney]], and [[Rolls-Royce plc|Rolls-Royce]] were possible engine suppliers for Embraer E-Jet family. However, there is [[Aviadvigatel PS-90|Aviadvigate]]<nowiki/>l as a possible Russian option .<ref name="Flightglobal">{{cite web |title= E-Jet revamp promises three-way engine 'dogfight' |publisher= Flightglobal |url= http://www.flightglobal.com/news/articles/istat-2012-e-jet-revamp-promises-three-way-engine-dogfight-369694/ |last= Ostrower |first= Jon |date= March 20, 2012 |accessdate= 2012-03-20}}</ref> Pratt & Whitney's [[Pratt & Whitney PW1000G|geared turbofan engine]] was selected in January 2013 for the new E-Jets versions.<ref name="AINonline">{{cite web |title= Embraer To Re-engine E-Jets with Geared Turbofan |publisher= AINonline |url= http://www.ainonline.com/aviation-news/2013-01-08/embraer-re-engine-e-jets-geared-turbofan |last= Polek |first= Gregory |date= January 8, 2013 |accessdate= January 8, 2013}}</ref><ref>{{cite news |title= Pratt & Whitney wins contract for Embraer's new E-Jets |url= http://www.reuters.com/article/2013/01/08/us-prattwhitney-embraer-idUSBRE90712E20130108 |date= 8 January 2013|accessdate = January 8, 2013 |work=Reuters}}</ref> The Honeywell Primus Epic 2 was selected as the avionics package.<ref>{{cite web|url=http://honeywell.com/News/Pages/Honeywell-Signs-$28-Billion-Lifetime-Contract-With-Embraer-to-Provide-Avionics-for-Second-Generation-of-E-Jets.aspx|title=Honeywell|publisher=}}</ref>

In February 2012, Embraer announced it was studying the development of a new variant with 130 seating capacity.<ref>[http://exame.abril.com.br/negocios/empresas/industria/noticias/embraer-estuda-produzir-aviao-maior-com-130-lugares-2 "Embraer estuda produzir avião maior, com 130 lugares" (in Portuguese).] ''Exame, '' 3 February 2012. Retrieved: 5 February 2012.</ref> The study was expected to be completed by the end of 2012.<ref>[http://www.aviationnewsreleases.com/2012/02/embraer-to-complete-e-jet-study-by-year.html "Embraer To Complete E-Jet Study By Year End".] 14 February 2012. Retrieved: 14 February 2012.</ref>

==Operational history==
[[File:LOT Embraer ERJ-170-100LR 170LR Wadman-1.jpg|thumbnail|E170 of [[LOT Polish Airlines]]]]
[[File:Oman Air Embraer ERJ-170-200LR 175LR A4O-EB DXB 2013-01-20.png|thumb|E175 of [[Oman Air]]]]
[[File:Helvetic_Airways,_HB-JVN_Embraer_ERJ-190_(17521029132).jpg|thumb|E190 of [[Helvetic Airways]]]]
[[File:Embraer_ERJ-190-200LR_195LR,_Lufthansa_Regional_(Lufthansa_CityLine)_AN1892887.jpg|thumb|E195 of [[Lufthansa CityLine]]]]
[[File:Embraer ERJ-190-100ECJ Lineage 1000 - Ryabtsev.jpg|thumb|Lineage 1000 at the 2009 Dubai Airshow]]
The first E170s were delivered in the second week of March 2004 to [[LOT Polish Airlines]], followed by [[US Airways]] subsidiary [[MidAtlantic Airways]] and [[Alitalia]]<ref name="AINdelay">{{cite web |url=http://www.ainonline.com/aviation-news/aviation-international-news/2007-03-30/embraer-170-finds-its-bearings-first-airplanes-enter-service |title=Embraer 170 finds its bearings as first airplanes enter service |last1=Polek |first1=Gregory |date=March 30, 2007|website= ainonline.com |publisher=Aviation International News |accessdate=30 October 2013}}</ref><ref>{{cite press release |author=<!--Staff writer(s); no by-line.--> |title=EMBRAER DELIVERS TWO EMBRAER 170 JETS TO US AIRWAYS |url=http://www.embraer.com.br/en-US/ImprensaEventos/Press-releases/noticias/Pages/EMBRAER-ENTREGA-DOIS-JATOS-EMBRAER-170-A-US-AIRWAYS.aspx |location=São José dos Campos, Brazil |publisher=Embraer |date=8 March 2004 |accessdate=30 October 2013 }}</ref> (launch customer Crossair had in the meantime ceased to exist after its takeover of [[Swissair]]; and fellow launch customer Régional Compagnie Aérienne deferred its order,<ref>Lewis (2001), p.36</ref> not receiving its first E-jet&mdash;an E190LR&mdash;until 2006.<ref>[http://www.planespotters.net/Airline/Regional-(France) Régional Compagnie Aérienne Européenne fleet list at planespotters.net] retrieved 30 October 2013</ref>) LOT operated the first commercial flight of an E-jet on 17 March 2004, from Warsaw to Vienna.<ref>[http://www.lot.com/us/en/history History - LOT Polish Airlines] retrieved 30 October 2013</ref> The largest single order for any type of E-Jets has come from [[JetBlue]], which ordered 100 Embraer 190s in 2003 and took its first delivery in 2005.<ref name=JetBlue>[http://m.aviationweek.com/commercial-aviation/jetblue-ceo-laments-embraer-190-costs JetBlue CEO Laments Embraer 190 Costs] AWIN First,
Jens Flottau,
Apr 22, 2013</ref>

The 400th E-jet was delivered in 2008, to [[Republic Airlines]] in the U.S.<ref name="AIN1000th"/> On 6 November of that year, JetBlue set the record for the longest flight of the E-190 family when one of its aircraft made a non-stop flight from [[Anchorage, Alaska]] ([[Ted Stevens Anchorage International Airport]]) to [[Buffalo, New York]] ([[Buffalo Niagara International Airport]]), a total of {{convert|2694|nmi|km|0|abbr=on}}. This was an empty aircraft on a non-revenue flight. The aircraft eventually returned to JFK after a two-month-long charter service with Vice Presidential candidate [[Sarah Palin]].<ref>{{cite news |last= Mutzabaugh |first= Ben |url= http://www.usatoday.com/travel/flights/2008-11-07-jetblue-e190-record_N.htm |title= JetBlue sets world record with McCain-Palin campaign plane |publisher=Usatoday.com |date=2008-11-07 |accessdate=2012-10-16}}</ref> In September 2009, the 600th E-jet built was delivered to LOT Polish Airlines.<ref>{{cite web|url=http://www.shephard.co.uk/news/3907/ |title=600th production E-Jet delivered to LOT - News |publisher=Shephard |date=2009-09-15 |accessdate=30 October 2013}}</ref> Kenya Airways received its 12th Ejet from Embraer which was also the 900th Ejet ever produced on October 10, 2012.<ref>{{cite web|url=http://centreforaviation.com/news/kenya-airways-receives-embraers-900th-e-jet-180238 |title=Kenya Airways receives Embraer's 900th E-Jet |publisher=CAPA |date=2012-10-10 |accessdate=10 July 2014}}</ref>

On 13 September 2013, a ceremony was held at the Embraer factory in São José dos Campos to mark the delivery of the 1,000th E-jet family aircraft, an E175, to Republic Airlines. The E175 was delivered in an [[American Eagle (airline brand)|American Eagle]] colour scheme with a special "1,000th E-Jet" decal above the cabin windows.<ref name="AIN1000th">{{cite web |url=http://www.ainonline.com/aviation-news/ain-air-transport-perspective/2013-09-16/embraer-sets-sights-beyond-1000th-e-jet-delivery |title=Embraer Sets Sights Beyond 1,000th E-Jet Delivery |last1=Polek |first1=Gregory |date=16 September 2013 |website= ainonline.com |publisher=Aviation International News |accessdate=30 October 2013}}</ref><ref name="LowFare">{{cite journal |last=Baldwin |first=Bernie |date=October–November 2013 |title= On to the next 1,000|journal= Low-Fare & Regional Airlines|location=Farnborough, United Kingdom |publisher=HMG Aerospace |volume=30 |issue=5 |pages=14 |accessdate=30 October 2013}}</ref>

==Variants==
==={{anchor|E-170 and 175}}E170 and E175===<!-- This section is linked from [[Air Canada]] -->
The E170/E175 models<ref name=":0" /> in the 80-seat range are the smaller in the EJet family. They are powered with [[GE Aviation|General Electric]] [[General Electric CF34|CF34-8E]] engines of 14,200 pounds (62.28&nbsp;kN) [[thrust]] each. The E170 and E175 directly compete with the [[Bombardier CRJ700|Bombardier CRJ-700]] and [[Bombardier CRJ-900]], respectively, and loosely compete with the turboprop [[de Havilland Canada Dash 8|Bombardier Q400]]. They also seek to replace the market segment occupied by earlier competing designs such as the [[BAe 146]] and [[Fokker 70]].

The Embraer 170 was the first version produced. The [[prototype]] 170-001, registration PP-XJE, was rolled out on 29 October 2001, with first flight 119 days later on 19 February 2002. The aircraft was displayed to the public in May 2002 at the [[European Regions Airline Association|Regional Airline Association]] convention. After a positive response from the airline community, Embraer launched the E175. First flight of the stretched E175 was on June 2003.<ref name="Embraer 170 Commercial Jetliner">{{cite web |url= http://www.aerospace-technology.com/projects/embraer_170/ |title=Embraer 170 |publisher= Aerospace-technology.com |date= 2011-06-15 |accessdate= 2012-10-16}}{{Unreliable source?|reason=domain on WP:BLACKLIST|date=June 2016}}</ref> The launch U.S. customer for the E170<ref name=":0" /> was [[US Airways]], after FAA certification, the aircraft entered into revenue service on April 4, 2004 operated by the MidAtlantic division of US Airways, Inc. The first E175 was delivered to [[Air Canada]] and entered service in July 2005.<ref name="Embraer 170 Commercial Jetliner"/> The 170-001 prototype performed its last flight on April 11, 2012. Its destiny was disassembly in the US for spare parts.

==={{anchor|E-190 and 195|E-190.2F195}}E190 and E195===<!-- This section is linked from [[Air Canada]] -->
The E190/195 models are a larger stretch of the E170/175 models fitted with a new, larger wing, a larger horizontal stabilizer and a new engine, the GE CF34-10E, rated at 18,500&nbsp;lb (82.30&nbsp;kN). These aircraft compete with the [[Bombardier CRJ700|Bombardier CRJ-1000]] and [[Bombardier CSeries|CS100]], the [[Boeing 717|Boeing 717-200]] and [[Boeing 737 Next Generation|737-600]], and the [[Airbus A318]]. It can carry up to 100 passengers in a two-class configuration or up to 124 in single-class high density configuration.<ref>http://www.embraercommercialaviation.com/AircraftPDF/E195_Cabin.pdf</ref>
 
The first flight of the E190 was on March 12, 2004 (PP-XMA),<ref name="ANAC">{{cite web |title= RAB - Registro Aeronautico Brasileiro |publisher= ANAC |url= http://www.anac.gov.br/aeronaves/cons_rab.asp |accessdate= 2011-01-31}}</ref> with the first flight of the E195 (PP-XMJ)<ref name="ANAC"/> on December 7 of the same year. The launch customer of the E190 was [[New York City|New York]]-based [[low-cost carrier]] [[JetBlue]] with 100 orders options in 2003 and took its first delivery in 2005.<ref name=JetBlue/> British low-cost carrier [[Flybe]] was the first operator of the E195, had 14 orders and 12 options, and started E195 operations on 22 September 2006.<ref name="FlybeFleet">{{cite web|url=http://www.flybe.com/vacancies/pilots_fleet.htm |title=About our fleet |work=flybe.com |year=2007 |accessdate=2008-05-23 |archiveurl=https://web.archive.org/web/20080224153710/http://www.flybe.com/vacancies/pilots_fleet.htm |archivedate=2008-02-24 |deadurl=yes |df= }}</ref>

[[Air Canada]] operates 45 E190 aircraft fitted with 9 business-class and 88 economy-class seats as part of its primary fleet. JetBlue and American Airlines also operate the E190 as part of their own fleet.

===Embraer Lineage 1000===
{{main|Embraer Lineage 1000}}
On 2 May 2006, Embraer announced plans for the business jet variant of the E190,<ref name=":0" /> type name ERJ190-100 ECJ. It has the same structure as the E190, but with an extended range of up to 4,200&nbsp;nmi, and luxury seating for up to 19. It was certified by the USA Federal Aviation Administration on 7 January 2009. The first two production aircraft were delivered in December 2008.

===Undeveloped variants===
====E195X====
Embraer considered producing an aircraft which was known as the E195X, a stretched version of the E195. It would have seated approximately 130 passengers. The E195X was apparently a response to an [[American Airlines]] request for an aircraft to replace its [[McDonnell Douglas MD-80]]s.<ref name="Kirby26Jan10">{{cite web |url= http://www.flightglobal.com/articles/2010/01/26/337597/proposed-stretch-dubbed-e-195x-by-embraer.html |title= Proposed stretch dubbed E-195X by Embraer |last= Kirby |first= Mark |authorlink= |date= January 2010 |accessdate = 2010-01-26}}</ref> Embraer abandoned plans for the 195X in May 2010, following concerns that its range would be too short.<ref>{{cite web |title = Embraer kills 195X over range concerns|publisher = Flight International|year = 2010|url = https://www.flightglobal.com/news/articles/embraer-kills-195x-over-range-concerns-341914/|accessdate = 2010-05-14|dead-url = no}}</ref>

==Operators==
{{main|List of Embraer E-Jets operators}}
As of December 2015, the Embraer fleet consists of the following aircraft:<ref name="ps">[http://www.ch-aviation.com/portal/airline/FB#al_profile_tab_fleet ch-aviation.com - Bulgaria Air] retrieved 14 December 2015</ref>

*'''Embraer 170''' ('''E170''' or EMB 170-100)—{{as of|July 2015}}, 180 Embraer 170 aircraft (all variants) are in airline service, with 5 orders. Major operators include: [[Republic Airline]] (69), [[HOP!]] (16), [[Saudia]] (15), [[J-Air]] (15), [[EgyptAir Express]] (12), [[Aeromexico Connect]] (8), [[LOT Polish Airlines]] (7), [[Compass Airlines (North America)]] (6) and [[BA CityFlyer]] (6). Nine other airlines operate the type in smaller numbers.<ref name=FI>{{cite web |url=https://d1fmezig7cekam.cloudfront.net/VPP/Global/Flight/Airline%20Business/AB%20home/Edit/WorldAirlinerCensus2015.pdf |title= World Airliner Census |work= Flight International |date=July 2015 |page=14 |accessdate=September 27, 2015}}</ref>
*'''Embraer 175''' ('''E175''' or EMB 170-200)—{{as of|July 2015}}, 285 Embraer 175 aircraft are in airline service, with 165 further orders. Major operators include [[Republic Airline]] (116), [[Compass Airlines (North America)]] (56), [[Mesa Airlines]] (48), [[SkyWest Airlines]] (58), [[Sky Regional Airlines]] (15), [[Alitalia CityLiner]] (15), [[LOT Polish Airlines]] (12) and [[Flybe]] (11). Major firm orders include 55 aircraft for [[Shuttle America]], 40 aircraft for [[Envoy Air]], and 33 for [[Horizon Air]].
*'''Embraer 190''' ('''E190''' or EMB 190-100)—{{as of|July 2015}}, 506 Embraer 190 aircraft (all variants) are in airline service, with 47 orders. Major operators include [[JetBlue Airways]] (60), [[Air Canada]] (45), [[Tianjin Airlines]] (45), [[Aeromexico Connect]] (30), [[KLM Cityhopper]] (30), [[Austral Lineas Aereas]] (26), [[Azul Brazilian Airlines]] (22), [[China Southern Airlines]] (20), [[TAP Express]] (10), [[American Airlines]] (19), [[Virgin Australia]] (18), [[Conviasa]] (15), [[Bulgaria air]] (4) and other operators with fewer aircraft.<ref name="FI"/>
*'''Embraer 195''' ('''E195''' or EMB 190-200)—{{as of|July 2015}}, 134 Embraer 195 aircraft (all variants) are in airline service, with 25 firm orders. Major operators are [[Azul Brazilian Airlines]] (66), [[Lufthansa CityLine]] (24), [[Air Europa]] (11), [[Borajet]] (10), [[Air Dolomiti]] (10), [[Flybe]] (9), [[LOT Polish Airlines]] (6) and other operators with fewer aircraft. Azul Brazilian Airlines have ordered an additional 5 aircraft of this type.<ref name="FI"/>

==Orders and deliveries==
List of Embraer's E-Jet family deliveries and orders:
{| class="wikitable" style="text-align: center;"
|-
! Model
! Firm Orders
! Options
! Deliveries 
! Firm Order Backlog
|-
! E170 
|193
|6
|190
|3
|- 
! E175 
|525
|244
|421
|104
|-
! E190 
|590
|55
|534
|56
|- 
! E195 
|166
|3
|155
|12
|- 
!Total
!1474
!308
!1300
!175
|}
Source: Embraer's order book on December 31, 2016.<ref>[http://www.embraer.com/Documents/noticias/054%20Embraer%20deliveries%203Q16-Ins-VPF-I-16.pdf Embraer’s Commercial Aircraft Deliveries Up 38% in]</ref><ref>[http://www.embraer.com/Documents/noticias/054%20Embraer%20deliveries%203Q16-Ins-VPF-I-16.pdf 3Q16]</ref>

==Accidents and incidents==
*On 18 February 2007, [[Shuttle America]] Flight 6448 (an ERJ-170 operating for Delta Connection) ran off the runway on landing at [[Cleveland Hopkins International Airport]], [[Ohio]] in poor visibility during a snowstorm. None of the 75 passengers and crew aboard were injured, and the aircraft, while significantly damaged, was repaired and returned to service.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=20070218-0|title=ASN Aircraft accident Embraer ERJ 170-100SE (ERJ-170SE) N862RW Cleveland-Hopkins International Airport, OH (CLE)|work=aviation-safety.net|accessdate=26 October 2016}}</ref>
*On 17 July 2007, [[Copa Airlines Colombia|Aero República]] Flight 7330 overran the runway while landing at [[Simón Bolívar International Airport (Colombia)|Simón Bolívar International Airport]] in [[Santa Marta]], [[Colombia]]. The ERJ-190 slid down an embankment off the side of the runway and came to rest with the nose in shallow water. The aircraft was damaged beyond repair, but all 60 aboard evacuated unharmed.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=20070717-1|title=ASN Aircraft accident Embraer ERJ 190-100 IGW (ERJ-190AR) HK-4455 Santa Marta-Simón Bolívar Airport (SMR)|work=aviation-safety.net|accessdate=28 October 2016}}</ref>
*On 24 August 2010, [[Henan Airlines Flight 8387]], an E190 that departed from [[Harbin]], [[People's Republic of China]], crash landed about 1&nbsp;km short of the runway at [[Yichun Lindu Airport]], resulting in 44 deaths.<ref>{{cite news |title=Safety concerns raised about China crash runway |agency=Associated Press |url=http://seattletimes.nwsource.com/html/nationworld/2012708491_apaschinaplanecrash.html |accessdate=2010-12-18 |work=The Seattle Times |first=David |last=Wivell |date=2010-08-25}}</ref>
*On 16 September 2011, an ERJ-190 operated by [[TAME]] landed long and ran off the end of the runway at [[Old Mariscal Sucre International Airport|Mariscal Sucre International Airport]] in [[Quito]], colliding with approach equipment and a brick wall. The crew reportedly failed to adhere to the manufacturer's procedures in the event of a flap malfunction, continuing the approach in spite of the aircraft's condition. Eleven of the 103 aboard received minor injuries, and the aircraft was written off.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=20110916-0|title=ASN Aircraft accident Embraer ERJ 190-100 IGW (ERJ-190AR) HC-CEZ Quito-Mariscal Sucre Airport (UIO)|work=aviation-safety.net|accessdate=28 October 2016}}</ref>
*On 29 November 2013, [[LAM Mozambique Airlines Flight 470]], an E190, crashed in Namibia, killing all 33 aboard (27 passengers, 6 crew members) by the [[Suicide by pilot|deliberate actions of the pilot]].<ref>{{cite news|url=http://www.reuters.com/article/2013/11/30/us-mozambique-flight-idUSBRE9AT02920131130 |title=Mozambique Airlines plane crashes in Namibia, killing 33 |publisher=Reuters|date=30 November 2013 |accessdate=30 November 2013}}</ref>  The first officer reportedly left the cockpit to use the bathroom.  He was then locked out by the captain, who dramatically reduced the aircraft’s altitude and ignored various automated warnings ahead of the high-speed impact.<ref>{{cite web|url=http://atwonline.com/safety/investigation-indicates-lam-mozambique-e-190-crash-could-have-been-deliberate|title=Investigation indicates LAM Mozambique E-190 crash could have been deliberate|work=atwonline.com|accessdate=25 January 2015}}</ref>

==Specifications==
[[File:Embraer ERJ-190-100ECJ Lineage 1000 Cabin.jpg|thumb|Flight deck of a 190 Lineage 1000]]
[[File:JetBlue Embraer 190 (3696473641) (2).jpg|thumbnail|[[General Electric CF34]] engine on [[JetBlue]] E190]]
{| class=wikitable style="text-align: center;"
|-
! Variant
! E170<br>(ERJ170-100)
! E175<br>(ERJ170-200)
! E190<br>(ERJ190-100)
! E195<br>(ERJ190-200)
|-
! Flight deck crew
| colspan=4 | 2 pilots
|-
! Passenger capacity
| 80 (1-class, 29 in/30 in pitch)<br> 78 (1-class, 30 in/31 in)<br> 70 (1-class, 32 in)<br> 70 (2-class, 36 in/32 in)<br><ref>{{cite web |url=http://www.embraercommercialjets.com/img//download/138.pdf |title=Embraer 170 Specifications |publisher=Embraercommercialjets.com |accessdate=2012-10-16}}</ref>
| 88 (1-class, 30 in pitch)<br> 86 (1-class, 31 in)<br> 78 (1-class, 32 in)<br> 78 (2-class, standard)<br><ref>{{cite web|url=http://www.embraercommercialjets.com/img//download/137.pdf |title=Embraer 175 Specifications |publisher=Embraercommercialjets.com |accessdate=2012-10-16}}</ref>
| 114 (1-class, 29 in/30 in pitch)<br> 106 (1-class, 31 in)<br> 98 (1-class, 32 in)<br> 94 (2-class, standard)<br><ref>{{cite web|url=http://www.embraercommercialjets.com/img//download/136.pdf |title=Embraer 190 Specifications |publisher=Embraercommercialjets.com |accessdate=2012-10-16}}</ref>
| 122 (1-class, 30 in/31 in pitch)<br> 118 (1-class, 31 in)<br> 108 (1-class, 32 in)<br> 106 (2-class, standard)<br><ref>{{cite web |url=http://www.embraercommercialjets.com/img//download/135.pdf |title=Embraer 195 Specifications |publisher=Embraercommercialjets.com |accessdate=2012-10-16}}</ref>
|-
! Length
| 29.90 m<br/> (98&nbsp;ft 1 in)
| 31.68 m (103&nbsp;ft 11 in)
| 36.24 m (118&nbsp;ft 11 in)
| 38.65 m<br/> (126&nbsp;ft 10 in)
|-
! Wingspan
| 26.00 m (85&nbsp;ft 4 in)
| 26.00 m (85&nbsp;ft 4 in) /<br/> 28.70 m (94&nbsp;ft 2 in) (Enhanced Wing Tip version)
| colspan=2 | 28.72 m (94&nbsp;ft 3 in)
|-
! Height
| colspan=2 | 9.67 m<br/> (32&nbsp;ft 4 in)
| colspan=2 | 10.57<ref>http://www.embraercommercialaviation.com/AircraftPDF/E190_Weights.pdf</ref> m<br/> (34&nbsp;ft 7 in)
|-
! Empty Weight
| {{convert|21140|kg|lb|abbr=on}}
| {{convert|21810|kg|lb|abbr=on}}
| {{convert|28080|kg|lb|abbr=on}}
| {{convert|28970|kg|lb|abbr=on}}
|-
! [[Maximum Takeoff Weight|Maximum takeoff weight]]
| {{convert|35990|kg|lb|abbr=on}} (STD) <br/>{{convert|37200|kg|lb|abbr=on}} (LR) <br/>{{convert|38600|kg|lb|abbr=on}} (AR)
| {{convert|37500|kg|lb|abbr=on}} (STD) <br/>{{convert|38790|kg|lb|abbr=on}} (LR) <br/>{{convert|40370|kg|lb|abbr=on}} (AR)
| {{convert|47790|kg|lb|abbr=on}} (STD) <br/>{{convert|50300|kg|lb|abbr=on}} (LR) <br/>{{convert|51800|kg|lb|abbr=on}} (AR)
| {{convert|48790|kg|lb|abbr=on}} (STD) <br/>{{convert|50790|kg|lb|abbr=on}} (LR) <br/>{{convert|52290|kg|lb|abbr=on}} (AR)
|-
! Max payload weight 
| {{convert|9100|kg|lb|abbr=on}} (STD&LR) <br/>{{convert|9840|kg|lb|abbr=on}} (AR)
| {{convert|10080|kg|lb|abbr=on}} (STD&LR) <br/>{{convert|10360|kg|lb|abbr=on}} (AR)
| {{convert|13080|kg|lb|abbr=on}}
| {{convert|13650|kg|lb|abbr=on}}
|-
! Takeoff Run at [[Maximum Takeoff Weight|MTOW]]
| 1,644 m (5,394&nbsp;ft)
| 2,244 m (7,362&nbsp;ft)
| 2,056 m (6,745&nbsp;ft)
| 2,179 m (7,149&nbsp;ft)
|-
! Powerplants
| colspan=2 |2× [[General Electric CF34|GE CF34-8E]] turbofans <br/> 61.4&nbsp;[[kilonewton|kN]] (13,800 [[pound-force|lb<sub>f</sub>]]) thrust each <br/> {{convert|63.2|kN|lbf|abbr=on}} APR thrust each
| colspan=2 |2× [[General Electric CF34|GE CF34-10E]] turbofans <br/> 82.3&nbsp;kN (18,500 [[pound-force|lb<sub>f</sub>]]) thrust each <br/> {{convert|89|kN|lbf|abbr=on}} APR thrust each
|-
! Maximum speed
| colspan=4 | {{convert|890|km/h|mph +kn|abbr=on|}} [[mach number|Mach]] 0.82)
|-
! Range
| STD: 3,334&nbsp;km (1,800 nmi)<br/> LR: 3,889&nbsp;km (2,100 nmi)<br/> AR: 3,982&nbsp;km (2,150 nmi)
| STD: 3,241&nbsp;km (1,750 nmi)<br/> LR: 3,982&nbsp;km (2,150 nmi)<br/> AR: 4,074&nbsp;km (2,200 nmi)
| STD: 3,426&nbsp;km (1,850 nmi)<br/> LR: 4,445&nbsp;km (2,400 nmi)<br/> AR: 4,537&nbsp;km (2,450 nmi)
| STD: 2,963&nbsp;km (1,600 nmi)<br/> LR: 3,704&nbsp;km (2,000 nmi)<br/> AR: 4,260&nbsp;km (2,300 nmi)
|- 
! Maximum fuel load
| colspan=2 | {{convert|9335|kg|abbr=on}} 
| colspan=2 | {{convert|12971|kg|abbr=on}} 
|-
! Service ceiling
| colspan=4 | 12,500&nbsp;m (41,000&nbsp;ft)
|-
! Thrust-to-weight
| 0.42:1
| 0.39:1
| 0.41:1
| 0.39:1
|-
! 
| colspan=4 | '''Fuselage and cabin cross-section'''
|-
! Outer width
| colspan=4 | 3.01 m (9&nbsp;ft 11 in)
|-
! Cabin width
| colspan=4 | 2.74 m (9&nbsp;ft 0 in)
|-
! Outer height
| colspan=4 | 3.35 m (11&nbsp;ft 0 in)
|-
! Cabin height
|colspan=4 | 2.00 m (6&nbsp;ft 7 in)
|}
Sources: Embraer Ejet<ref>[http://www.embraercommercialjets.com/#/en/familia-ejets/1 Embraer E-jet specifications], accessed Dec 26, 2009</ref>

==See also==
{{Portal|Aviation|Brazil}}
{{aircontent|
|related=
* [[Embraer Regional Jet]]
* [[Embraer Lineage 1000]]
* [[Embraer KC-390]]
* [[Embraer E-Jet E2 family]]
|similar aircraft=
* [[Airbus A318]]
* [[Antonov An-148]]
* [[Boeing 717]]
* [[Boeing 737-600]]
* [[Bombardier CRJ700 series]]
* [[Bombardier CSeries]]
* [[Comac ARJ21]]
* [[Fokker 70]]/[[Fokker 100|100]]
* [[Mitsubishi Regional Jet|Mitsubishi MRJ 70/MRJ 90]]
* [[Sukhoi Superjet 100]]
* [[Tupolev Tu-334]]
|lists=
* [[List of jet airliners]]
* [[List of civil aircraft]]
* [[List of active Brazilian military aircraft]]
|see also = 
* [[Comparison of commercial aircraft]]
}}

==References==
===Citations===
{{Reflist|30em}}

===Bibliography===
* {{cite book |last=Endres |first=Günter |year=2001 |title=The Illustrated Directory of Modern Commercial Aircraft |location=London |publisher=Salamander Books |isbn=978-1840652871 }}<!--|accessdate=30 October 2013 -->
* {{cite journal |last=Kingsley-Jones |first=Max |last2=Wastnage |first2=Justin |date=28 August – 3 September 2001 |title= World Airliners: Regional Realities |journal=Flight International |publisher=Reed Business Information |volume=160 |issue=4795 |pages=38–62 |accessdate=30 October 2013}}
* {{cite journal |last=Lewis |first=Paul |date=23–29 October 2001 |title= New by Design |journal=Flight International |publisher=Reed Business Information |volume=160 |issue=4803 |pages=34–36 |accessdate=30 October 2013}}

==External links==
{{Commonscat inline|Embraer E-Jets}}
* [http://www.embraercommercialjets.com/ Official Embraer E-Jets website]
* [http://www.airliners.net/info/stats.main?id=406 Embraer E-Jets images on Airliners.net]

{{Embraer}}

{{DEFAULTSORT:Embraer E-Jet Family}}
[[Category:Brazilian airliners 2000–2009]]
[[Category:Embraer aircraft]]
[[Category:Twinjets]]