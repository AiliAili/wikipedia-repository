{{Infobox scientist
| name              = Tony Weiss
|image         = Professor Tony Weiss.jpg
|image_size    = 
|caption       = 
| alt               = 
| birth_date        = <!-- {{Birth date and age |YYYY|MM|DD|YYYY|MM|DD}} (birth date) -->
| birth_place       = Australia
| death_date        = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place       = 
| residence         = Australia
| citizenship       = 
| nationality       = Australian
| fields            = 
| workplaces        = [[University of Sydney]], [[Charles Perkins Centre]], [[Bosch Institute]]
| alma_mater        = 
| doctoral_advisor  = 
| academic_advisors = 
| doctoral_students = 
| notable_students  = 
| known_for         = 
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences        = 
| influenced        = 
| awards            = 
| signature         = <!--(filename only)-->
| signature_alt     = 
| footnotes         = 
| spouse            = 
}}

'''Anthony Steven Weiss''' [[Fellow of the Royal Society of Chemistry|FRSC]] [[FRACI]] [[CChem]] [[FRSA]] [[Australian Academy of Technological Sciences and Engineering|FTSE]] FAIMBE [[FAICD]] [http://www.fellowsbse.org/#!current-fellows/c1d94 FBSE] is the leading scientist in human [[tropoelastin]] research and synthetic human [[elastin]]. He holds the [[Samuel McCaughey]] Chair in [[Biochemistry]], heads the [[Charles Perkins Centre]] Node in Tissue Engineering and Regenerative Medicine, and is Professor of Biochemistry and Molecular [[Biotechnology]] at the [[University of Sydney]].<ref>{{cite web|url=http://sydney.edu.au/science/people/tony.weiss.php|title=Professor Tony Weiss|publisher=}}</ref><ref>{{cite web|url=https://www.raci.org.au/raci-news/professor-tony-weiss-named-a-fellow-of-the-royal-society-of-chemistry|title=Professor Tony Weiss Named a Fellow of the Royal Society of Chemistry|publisher=}}</ref><ref>https://www.atse.org.au/Documents/focus/191-productivity-and-innovation.pdf</ref><ref>{{Cite web|url=http://weisslab.net|title=Weiss Lab Home|website=Weiss Lab|access-date=2016-05-14}}</ref> His discoveries are on human elastic materials that accelerate the healing and repair of arteries, skin and 3D human tissue components. He is a Fellow of the [[Royal Society of Chemistry]]. Weiss is on the editorial boards of the [[American Chemical Society]] Biomaterials Science and Engineering, Applied Materials Today ([[Elsevier]]), [[Biomacromolecules]], [[Biomaterials]] , [[Biomedical Materials (journal)|Biomedical Materials]], BioNanoScience ([[Springer Publishing|Springer]]) and Tissue Engineering ([[Mary Ann Liebert, Inc.]]). He is a  biotechnology company founder,<ref>{{Cite web|title = Elastagen - Elastagen|url = http://www.elastagen.com|website = Elastagen|access-date = 2016-02-19|language = en-US}}</ref> promoter of national and international technology development, and has received national and international awards.<ref>{{cite web|url=http://faobmb.com/2015/11/24/07-02-2014-winner-of-2014-faobmb-entrepreneur-award-professor-anthony-weiss-australia/|title=07.02.2014 – Winner of 2014 FAOBMB Entrepreneur Award: Professor Anthony Weiss (Australia)|publisher=}}</ref>

His awards include the Innovator of Influence,<ref>{{cite web|url=http://asiforum.net|title=Home|work=Australian Science and Innovation Forum}}</ref> Applied Research Medal given by the [[Royal Australian Chemical Institute]],<ref>http://www.raci.org.au/document/item/2180</ref> Federation of Asian and Oceanian Biochemists & Molecular Biologists Entrepreneurship Award,<ref>{{cite web|url=http://faobmb.com/awards/faobmb-entrepreneurship-award|title=FAOBMB Entrepreneurship Award|publisher=}}</ref> Australasian Society for Biomaterials & Tissue Engineering Research Excellence Award,<ref>{{cite web|url=http://www.asbte.org/#!award-winners/c1501|title=Australasian Society for Biomaterials and Tissue Engineering|work=Australasian Society for Biomaterials and Tissue Engineering}}</ref> and election to the Governing Board of the World [[Tissue Engineering and Regenerative Medicine International Society]].<ref>{{Cite web|url=http://www.termis.org/officers.php|title=TERMIS|website=www.termis.org|access-date=2016-05-02}}</ref>

==Early life and education==
Anthony Weiss was born in Sydney, Australia and received his PhD from the [[University of Sydney]]. He was a [[Fulbright scholarship|Fulbright Scholar]] at [[Stanford University]] and [[National Institutes of Health|NIH Fogarty International Fellow]].<ref>{{Cite web|url=http://www.bic.canterbury.ac.nz/documents/Interface-Abstract-book.pdf|title=Interface Workshop|last=|first=|date=|website=Biomolecular Interaction Centre|publisher=University of Canterbury|access-date=May 2, 2016}}</ref> He is a [[Fellow of the Royal Society of Chemistry]], [[Fellow of the Australian Academy of Technological Sciences and Engineering]],<ref>{{Cite web|url=http://sydney.edu.au/news/science/397.html?newscategoryid=62&newsstoryid=14254|title=News {{!}} The University of Sydney|website=sydney.edu.au|access-date=2016-05-02}}</ref> [[Fellow of the Royal Australian Chemical Institute]]<ref>{{Cite web|url=http://www.raci.org.au/document/item/2180|title=2015 National Awards|last=|first=|date=|website=RACI|publisher=|access-date=May 2, 2016}}</ref> and [[Chartered Chemist]], [[Royal Society of Arts, Manufactures and Commerce|Fellow of the Royal Society of Arts Manufactures and Commerce]],<ref>{{Cite web|url=https://www.researchgate.net/profile/Anthony_Weiss2/publications|title=Anthony S Weiss - Publications|website=www.researchgate.net|access-date=2016-05-02}}</ref> [[American Institute for Medical and Biological Engineering|Fellow of the American Institute for Medical and Biological Engineering]],<ref>{{Cite web|url=http://aimbe.org/college-of-fellows/COF-1559/|title=Anthony Weiss, Ph.D  COF-1559 - AIMBE|website=aimbe.org|access-date=2016-05-02}}</ref> [[Fellow of the Australian Institute of Company Directors]]<ref>{{Cite web|url=http://chemaust.raci.org.au/sites/default/files/pdf/2016/CiA_Feb2016.pdf|title=2015 RACI National Award winners|last=|first=|date=|website=Chemistry in Australia|publisher=RACI|access-date=May 2, 2016}}</ref> and [[Fellow of Biomaterials Science and Engineering]].

==References==
{{reflist|30em}}

{{Authority control}}

{{DEFAULTSORT:Weiss, Anthony}}
[[Category:University of Sydney faculty]]
[[Category:Fellows of the Royal Society of Arts]]
[[Category:Fellows of the Australian Institute of Company Directors]]
[[Category:Fellows of the Royal Society of Chemistry]]
[[Category:Academic journal editors]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Fulbright Scholars]]
[[Category:Fellows of the Australian Academy of Technological Sciences and Engineering]]