{|{{Infobox Aircraft Begin
  |name = DA50 Super Star & Magnum
  |image = File:IMG DiamondDA50.JPG
  |caption = DA50 at Airventure 2007
}}{{Infobox Aircraft Type
  |type = 
  |manufacturer = [[Diamond Aircraft Industries]]
  |national origin = Austria
  |designer = 
  |first flight = 4 April 2007
  |introduced = 
  |retired =
  |status = 
  |primary user = 
  |more users = 
  |produced = 
  |number built = 1 prototype
  |unit cost = 
  |variants with their own articles = 
}}
|}

The '''Diamond DA50 Super Star''' and '''DA50 Magnum''' are Austrian five-place single-engine composite aircraft designed by [[Diamond Aircraft Industries]].<ref name=diamond>{{cite web|url = http://www.diamondaircraft.com/aircraft/da50/index.php?sect=spec|title = DA50 Superstar Brochure|accessdate = 2008-03-18|last =[[Diamond Aircraft]] |authorlink = |year = 2007|archiveurl = https://web.archive.org/web/20080223005903/http://www.diamondaircraft.com/aircraft/da50/index.php?sect=spec|archivedate = 2008-02-23}}</ref> Introduced in 2006, the project was later put on hold until the turboprop JP7 variant was introduced in 2015.

==Development==
The DA50 was unofficially presented in December 2006 at a company Christmas party and is to be able to be operated with gasoline and diesel engines up to 350&nbsp;hp. The new five seater will have one of the largest cabins of the new generation of general aviation airplanes.<ref name=diamond/>

The avgas powered version of the DA50 is to be equipped with a single [[FADEC]]-equipped [[Continental IO-550|Continental TSIO-550-J]] powerplant with twin turbo chargers producing {{convert|350|hp|kW|0|abbr=on}}. The plane's wingspan is currently reported as 38', 4", with overall fuselage length of 29 feet. The maximum takeoff weight is planned to be 3,527&nbsp;lbs. The Diamond DA50 Super Star will be pressurized and offer a [[Ballistic Recovery Systems]] aircraft parachute system as an option.<ref name=diamond/>

The DA50 made its maiden test flight on April 4, 2007 at the [[Wiener Neustadt East Airport]], Austria.<ref>{{cite web|url=http://www.diamondair.com/news/2007_04_05.php |title=Diamond Super Star makes first flight! |accessdate=2008-03-18 |last= |authorlink= |year=n.d. |archiveurl=https://web.archive.org/web/20080225235156/http://www.diamondair.com/news/2007_04_05.php |archivedate=2008-02-25 |deadurl=yes |df= }}</ref> The plane made its public debut at the [[AERO Friedrichshafen]] show in Germany on April 19, 2007.

The DA50 project was put on hold in 2009 as the economy entered an extended downturn and Diamond turned all their attention to the [[Diamond D-Jet]].<ref>Bill Cox, [http://www.flydominion.com/dominion/wp-content/uploads/2011/06/Premier-Edition-Plane-Pilot-Magazine.pdf "Diamond DA40 XLS: Premier Edition"], ''Plane and Pilot'', 21 June 2011</ref>

On 19 January 2015 the prototype now designated DA50-JP7 and powered by a [[Ivchenko-Progress Motor Sich AI-450S]] turboprop powerplant, was test-flown in Wiener Neustadt. In 2015 [[type certificate|type certification]] was expected near the end of 2016.<ref name="DA50-JP7">{{cite web|url = http://www.diamond-air.at/media-center/press-releases/news/article/maiden-flight-of-the-da50-jp7-with-turbine.html|title = Maiden Flight of the DA50-JP7 with turbine |accessdate = 2015-01-21|last = |authorlink = |year = n.d. |archiveurl = https://web.archive.org/web/20150121202832/http://www.diamond-air.at/media-center/press-releases/news/article/maiden-flight-of-the-da50-jp7-with-turbine.html|archivedate = 2015-01-21}}</ref>

At the April 2017 [[AERO Friedrichshafen]] show, Diamond launched variants powered by diesel Safran/[[SMA Engines]] : the four-seat 230shp DA50-IV; five-seat 260shp DA50-V which was on display, made its first flight in March and is scheduled for certification in 2018; and seven-seat 360shp DA50-VII - also available with a 375shp petrol [[Lycoming engine]] or an AI-450S turboprop.<ref>{{cite news |url= https://www.flightglobal.com/news/articles/aero-diamond-enters-helicopter-market-with-dart-280-435959/ |title= Diamond enters helicopter market with DART 280 unveiling |date= 6 April 2017 |work= Flight Global}}</ref>

==Variants==
;DA50 SuperStar
: Pressurized version powered by a [[Continental IO-550|Teledyne Continental TSIOF-550J]]. US [[Federal Aviation Administration|FAA]] [[type certificate|certification]] was initially forecast for 2012.<ref name="Avweb05Apr09">{{cite web|url = http://www.avweb.com/avwebflash/news/DieselDA50HeadedToU.S._200082-1.html|title = Diesel DA50 Headed To U.S.|accessdate = 2009-04-06|last = Peppler|first = Graeme|authorlink = |date=April 2009}}</ref><ref name="DA50SuperStarSpecs">{{cite web|url = http://www.diamondaircraft.com/aircraft/da50/specs.php|title = DA50 SuperStar Specifications|accessdate = 19 July 2010|last = [[Diamond Aircraft]]|authorlink = |year = n.d.}}</ref>
;DA50 Magnum
: Unpressurized version, with a standard integral oxygen system, powered by an [[Austro Engine E4|Austro Engine AE 300]] {{convert|170|hp|kW|0|abbr=on}} diesel, US FAA certification originally forecast for mid-2010<ref name="Avweb05Apr09" /><ref name="MagnumSpecs">{{cite web|url = http://www.diamondaircraft.com/aircraft/da50/specs_da50_magnum.php|title = DA50 Magnum Specs and Options|accessdate = 19 July 2010|last = [[Diamond Aircraft]]|authorlink = |year = n.d.}}</ref><ref name="MagnumProject">{{cite web|url = http://www.diamond-air.at/da50_magnum+M52087573ab0.html|title = DA50 Magnum (Future Project)|accessdate = 19 July 2010|last = [[Diamond Aircraft]]|authorlink = |year = n.d.}}</ref><ref>{{cite journal|magazine=AOPA Pilot|date=March 2015|title=Diamond DA50 turboprop enters flight testing|author=Alton K Marsh|page=24}}</ref>
;DA50-JP7
: 7-seat version powered by a {{convert|465|hp|kW|0|abbr=on}} [[FADEC]]-controlled [[Ivchenko-Progress Motor Sich AI-450S]] turboprop engine. Two sub-variants are planned, one "tundra" version with rugged landing gear, and one high-performance version for training and private customers. Certification is expected mid to end 2016.<ref name="DA50-JP7" />
;DA50-IV
:Four seat version powered by a {{convert|230|hp|kW|0|abbr=on}} [[SMA SR305-230]] [[diesel engine]] announced at the [[Aero Friedrichshafen]] show in April 2017.<ref name="Niles06Apr17">{{cite web|url = http://www.avweb.com/avwebflash/news/Diamond-Launches-High-Performance-Singles-228802-1.html|title = Diamond Launches High-Performance Singles|accessdate = 7 April 2017|last = Niles|first = Russ |work = AVweb|date = 6 April 2017}}</ref>
;DA50-V
:Five seat version powered by an [[SMA Engines]] {{convert|260|hp|kW|0|abbr=on}} diesel engine, It was first flown in March 2017 and introduced at the Aero Friedrichshafen show in April 2017. Certification is planned for 2018.<ref name="Niles06Apr17"/>
;DA50-VII
:Seven seat version powered by a [[SMA Engines]] [[SMA SR460]] {{convert|360|hp|kW|0|abbr=on}} diesel engine, a {{convert|375|hp|kW|0|abbr=on}} [[Lycoming Engines]] gasoline powerplant or a {{convert|450|hp|kW|0|abbr=on}} [[Ivchenko-Progress Motor Sich AI-450S]] [[turboprop]]. The model was announced at the Aero Friedrichshafen show in April 2017.<ref name="Niles06Apr17"/>

==Specifications (DA50 SuperStar)==
{{Aircraft specs
|ref=Diamond Aircraft<ref name=diamond /><ref name="DA50SuperStarSpecs" />
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=four passengers
|length m=
|length ft=28
|length in=11
|length note=
|span m=
|span ft=38
|span in=4
|span note=
|height m=
|height ft=7
|height in=3
|height note=
|wing area sqm=
|wing area sqft=148
|wing area note=
|airfoil=
|empty weight kg=
|empty weight lb=2200
|empty weight note=
|gross weight kg=
|gross weight lb=3560
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=74 US gallons (280 litres)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental IO-550|Teledyne Continental TSIOF-550J]]
|eng1 type=six cylinder, four-stroke, piston aircraft engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=350<!-- prop engines -->
|eng1 shp=<!-- prop engines -->

|prop blade number=3<!-- propeller aircraft -->
|prop name=constant speed, with 4-bladed propeller optional
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=200
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=

|avionics=*[[Garmin G1000]] three-screen
}}

==See also==
{{aircontent
|similar aircraft=

|sequence=

|see also=

|lists=
}}

==References==
{{reflist}}

==External links==
{{Commons category|Diamond DA-50}}
*[http://www.diamond-air.at/news_detail+M5d159204a41.html Press release from Diamond on the DA50 Super Star]
*[http://www.diamondair.com/news/2007_04_05.php Photos of the DA50 Super Star]
*[http://www.diamondair.com/aircraft/da50/ DA50 Brochure]
*[http://www.diamond-air.at/home+M52087573ab0.html Diamond Aircraft - corporate web site]
*[http://www.diamondair.com Diamond Aircraft - North American web site]
*[http://www.aero-news.net/index.cfm?ContentBlockID=466d0dd3-d10a-48f5-8468-c31ff5c1a9fa&#d DA50 Makes American Debut At AirVenture]

{{Diamond Aircraft}}

[[Category:Austrian civil utility aircraft 2000–2009]]
[[Category:Diamond aircraft|DA50]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:T-tail aircraft]]