{{refimprove|date=November 2016}}{{Infobox military person
| name         = Günther Schack
| birth_date   = {{birth date|1917|11|12|df=y}}
| death_date   = {{death date and age|2003|6|14|1917|11|12|df=y}}
| birth_place  = [[Bartoszyce|Bartenstein]], [[German Empire]]
| death_place  = Schmidt near [[Nideggen]], [[Germany]]
| image        = Schack1.jpg
| caption      = Günther Schack
| nickname     = 
| allegiance   = {{flag|Nazi Germany}}
| serviceyears = 1939–45
| rank         = [[Major (Germany)|Major]]
| branch       = {{Luftwaffe}}
| commands     = 9./[[JG 51]], I./[[JG 51]], IV./[[JG 3]]
| unit         = [[JG 51]], [[JG 3]]
| battles      = [[World War II]]
*[[Eastern Front (World War II)|Eastern Front]]
| awards    = [[Knight's Cross of the Iron Cross with Oak Leaves]]
| laterwork = 
}}
'''Günther Schack''' (12 November 1917 – 14 June 2003) was a [[Nazi Germany|German]] [[World War II]] [[fighter ace]] who served in the [[Luftwaffe]] from 1939 until the end of [[World War II]] in 1945. A flying ace or fighter ace is a [[military aviation|military aviator]] credited with shooting down five or more enemy [[aircraft]] during aerial combat.<ref>Spick 1996, pp. 3–4.</ref> 
He was a recipient of the Oakleaves to the Knight's Cross in recognition of his combat success.
He claimed 174 enemy aircraft shot down, all of them on the [[Eastern Front (World War II)|Russian front]]. He survived being shot down 15 times during his 780 combat missions.<ref>Spick 1996, p. 228.</ref> After the war he lived secluded in the [[Eifel]] Mountains, and devoted himself to philosophical research.

==Early life==
Günther Schack was born in [[Bartoszyce|Bartenstein]] in [[Eastern Prussia]] in 1917 as the son of a doctor. He studied at the [[University of Stuttgart]] and at the [[RWTH Aachen|''Rheinisch-Westfälische Technische Hochschule'']] in [[Aachen]].{{citation needed|date=September 2016}}

==Military career==
Schack was accepted into the [[Luftwaffe]] on 2 September 1939.<ref>Aces of the Luftwaffe website.</ref> After being trained as a fighter pilot, ''[[Gefreiter]]'' (Privat First Class) Schhack was posted to 7th ''Staffel'' of [[Jagdgeschwader 51]] (JG 51&mdash;7th Squadron of the 51st Fighter Wing) on 18 March 1941.<ref name="Obermaier p65">Obermaier 1989, p. 65.</ref><ref group="Note">For an explanation of the meaning of Luftwaffe unit designation see [[Luftwaffe Organization]]</ref>

Schack claimed his first aerial victory in combat on 23 July 1941 on the [[Eastern Front (World War II)|Eastern Front]]. He struggled to score against the poorly led and poorly trained Soviet pilots. He achieved his third air victory after 100 combat missions, on 10 November 1941. He flew his 250th combat mission on 30 July 1942, when he claimed only his 5th aerial victory.<ref name="Obermaier p65"/> In November 1942, with his personal score now at 18, his unit returned to Jesau in East Prussia to re-equip onto the new [[Focke Wulf Fw 190]]A fighter.

<!-- Deleted image removed: [[File:Emil Lang Oak Leaves.jpg|thumb|upright=1.5|left|[[Alfred Grislawski]], [[Emil Lang]], Günther Schack (3rd from left), [[Otto Kittel]] and [[Anton Hafner]] receiving the Knight's Cross of the Iron Cross with Oak Leaves from [[Adolf Hitler]] and [[Hermann Göring]] at the [[Berghof (residence)|Berghof]], [[Obersalzberg]] on 5 May 1944. Hafner is shaking Hitler's hand|alt=Five men all wearing military uniforms and decorations standing in row. The man on the far right is shaking hands with another man whose back is facing the camera. Another man is standing behind the men shaking hands.]] -->
He returned to the Eastern Front, still serving in the 7./JG 51, whereupon his career started to take off. On 17 December 1942, on his first mission back at the front, Schack shot down five Soviet [[Petlyakov Pe-2]] bombers within minutes of each other.<ref>OKL 1942</ref>  Six weeks later, on 29 January 1943 Schack almost repeated this when his ''[[Schwarm]]'' (flight of four aircraft), on a [[Junkers Ju 87]] escort mission, encountered eight Soviet [[Petlyakov Pe-2]] flying in a [[line astern]] crossing the German lines at [[Novosil]]. Within five minutes all eight were shot down including four by Schack.<ref>Weal 1996, pp. 14–15.</ref><ref>OKL 1943</ref> He was promoted to ''[[Leutnant]]'' (Second Lieutenant) on 1 January 1943, and after his 48th aerial victory (on 1 April 1943), he was ordered back to Germany to serve as a flight instructor with ''[[Ergänzungs-Jagdgruppe Ost]]''.

He returned to the front in early July 1943, this time posted to 8./JG 51, and back on the [[Messerschmitt Bf 109|Bf 109-G]]. Part of III./JG 51, it was based at [[Bryansk]] covering the northern pincer attack for the [[Battle of Kursk]]. He claimed 10 victories in July and but then a further 40 in August. Flying out of [[Konotop]] on 1 September, he flew four missions, and had to belly-land after each one. He reached his century-mark in aerial combat on 3 September 1943, and was awarded the [[Knight's Cross of the Iron Cross]] on 26 October for 116 victories. He was the 52nd ''Luftwaffe'' pilot to achieve the century-mark.<ref>Obermaier 1989, p. 243.</ref> On 9 December 1943, as an ''Oberleutnant'', he was made ''[[Staffelkapitän]]'' (squadron leader) of 9./JG 51.

1944 saw the German forces pushed inexorably out of Russia and III./JG 51 covered the retreat of [[Army Group Centre]], moving from [[Orsha]] to [[Terespol]] then back to Minsk and Kaunus, with Günther scoring consistently. ''Oberleutnant'' Schack was awarded the [[Knight's Cross of the Iron Cross with Oak Leaves]] on 20 April 1944 for 133 aerial victories.<ref>Weal 2001, p. 78.</ref> Schack was then promoted to ''[[Hauptmann]]'' (Captain) on 1 July 1944 and on 13 August became the 28th pilot to score 150 victories. On 16 December 1944, he became a ''[[Gruppenkommandeur]]'' (Group Commander), in charge of I./JG 51. By now, the ''Gruppe'' had been forced back to the Baltic coast in East Prussia. On 12 April he had to [[Parachute|bail out]] following aerial combat, and suffered serious burns. Soon after, in late April, the ''Gruppe'' was disbanded, and on 1 May 1945, he was named the final commander of the IV./JG 3 "Udet". By the end of the war, he had scored 174 victories in 780 aerial combats, and was shot down 15 times (taking to his parachute four times).

==Later life==
Schack was once shot down by [[Lieutenant]] Hollis "Bud" Nowlin of the [[357th Fighter Group]] over [[East Prussia]]. Nowlin and Schack met again first in Germany and then again in the fall of 1991 at the 357th Fighter Group reunion in Georgia.<ref>Klinkowitz 1996, p. 64.</ref>

==Awards==
* [[Iron Cross]] (1939)
** 2nd Class (10 August 1941)<ref name="Thomas p245">Thomas 1998, p. 245.</ref>
** 1st Class (15 June 1942)<ref name="Thomas p245"/>
* [[Ehrenpokal der Luftwaffe]] on 25 January 1943 as ''[[Feldwebel]]'' and pilot<ref>Patzwall 2008, p. 177.</ref>{{refn|According to Obermaier on 16 January 1943.<ref name="Obermaier p65"/>|group="Note"}}
* [[German Cross]] in Gold on 26 February 1943 as ''Feldwebel'' in the III./JG 51<ref>Patzwall & Scherzer 2001, p. 397.</ref>
* [[Knight's Cross of the Iron Cross with Oak Leaves]]
** Knight's Cross on 29 October 1943 as ''[[Leutnant]]'' (war officer) and pilot in the 9./JG 51 "Mölders"<ref name="Scherzer p653">Scherzer 2007, p. 653.</ref>
** 460th Oak Leaves on 20 April 1944 as ''Leutnant'' (war officer) and ''[[Staffelkapitän]]'' of the 9./JG 51 "Mölders"<ref name="Scherzer p653"/>

== Works ==
* Schack, Günther (1995). ''Betet für die Juden, betet für die Christen''. Nideggen. ISBN 3-9800329-3-0.
* Schack, Günther (1975). ''Die Homokratie im Erdkreis''.
* Schack, Günther (1975). ''Die Homokratie im Lebenskreis''.
* Schack, Günther (1975). ''Die Homokratie im Völkerkreis''.

==Notes==
{{Reflist|group="Note"}}

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* Klinkowitz, Jerome (1996). ''Yanks Over Europe: American Flyers in World War II''. Kentucky, USA: The University Press of Kentucky. ISBN 0-8131-1961-8.
* {{Cite book
  |last=Obermaier
  |first=Ernst 
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D. 
  |last2=Scherzer
  |first2=Veit 
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D. 
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
}}
* {{Cite book
  |last=Scherzer
  |first=Veit 
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
* {{Cite book
  |last=Spick
  |first=Mike
  |year=1996
  |title=Luftwaffe Fighter Aces
  |location=New York
  |publisher=[[Ivy Books]]
  |isbn=978-0-8041-1696-1
}}
* {{Cite book
  |last=Thomas
  |first=Franz
  |year=1998
  |title=Die Eichenlaubträger 1939–1945 Band 2: L–Z
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 2: L–Z
  |language=German
  |location=Osnabrück, Germany
  |publisher=Biblio-Verlag
  |isbn=978-3-7648-2300-9
}}
* Weal, John (1996). ''Focke-Wulf Fw 190 Aces of the Russian Front''. London, UK: Osprey Publishing. ISBN 1-85532-518-7.
* Weal, John (2001). ''Bf109 Aces of the Russian Front''. Oxford: Osprey Publishing Limited. ISBN 1-84176-084-6.
{{Refend}}

{{Knight's Cross recipients of JG 51}}
{{Top German World War II Aces}}
{{Authority control}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Schack}}
[[Category:1917 births]]
[[Category:2003 deaths]]
[[Category:People from Bartoszyce]]
[[Category:People from East Prussia]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross with Oak Leaves]]