{{Infobox character
| name        = Drogo
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Jason Momoa]]<br />(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = Khal Drogo-Jason Momoa.jpg
| caption     = [[Jason Momoa]] portraying Khal Drogo
| first       = '''Novel''': <br />''[[A Game of Thrones]]'' (1996) <br />'''Television''': <br />"[[Winter Is Coming]]" (2011)
| last        = '''Novel'''; <br />''A Game of Thrones'' (1996) <br />'''Television''';  <br />"[[Valar Morghulis]]" (2012)
| occupation  =
| title       = Khal
| alias       = 
| gender      = Male
| family      = 
| spouse      = [[Daenerys Targaryen]]
| children    = Rhaego {{small|([[stillborn]])}}
| relatives   = Bharbo {{small|(father)}}
| nationality = Dothraki
}}

'''''Khal'' Drogo''' is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''.

Introduced in 1996's ''[[A Game of Thrones]]'', Drogo is a ''khal'', a leader of the [[Dothraki]], a tribe of warriors who roam the continent of [[Essos]].

Drogo is portrayed by [[Jason Momoa]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/khal-drogo/bio/khal-drogo.html |title=''Game of Thrones'' Cast and Crew: Khal Drogo played by Jason Momoa |publisher=[[HBO]] | accessdate=December 29, 2015}}</ref><ref>{{cite web|url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/index.html|title=The Official Website for the HBO Series Game of Thrones|work=HBO}}</ref><ref>{{cite web|url=http://grrm.livejournal.com/164794.html |title=From HBO |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20160307150640/http://grrm.livejournal.com/164794.html |archivedate=2016-03-07 |df= }}</ref>

==Storylines==
Drogo is a powerful ''khal'', or warlord, of the [[World of A Song of Ice and Fire#The Dothraki Sea|Dothraki]] people, a tribal nation of horse riders in the steppes beyond the [[Free Cities (A Song of Ice and Fire)|Free Cities]]. He is an accomplished warrior and has never been defeated in battle. In ''A Game of Thrones'', [[Viserys Targaryen]] and [[Illyrio Mopatis]] marry [[Daenerys Targaryen]] to ''Khal'' Drogo to get his support and warriors for the invasion of Westeros.<ref>{{cite web|url=http://www.tor.com/2011/03/25/a-read-of-ice-and-fire-a-game-of-thrones-part-2/|title=A Read of Ice and Fire: A Game of Thrones, Part 2|work=Tor.com}}</ref> He proves a kind, sensitive, and loving husband. After there is a failed attempt on Daenerys's life, he vows to invade Westeros, but is wounded in a subsequent battle. Daenerys unwittingly sacrifices their unborn son to save him with blood magic. While this saves his life, he is left in a [[catatonic]] state. Daenerys euthanizes him by smothering him with a pillow. At his [[funeral pyre]], she walks with her dragon eggs into the flames and emerges unscathed with three baby dragons. He is the namesake of her favorite dragon, Drogon.<ref>{{cite web|url=http://www.tor.com/2011/12/02/a-read-of-ice-and-fire-a-game-of-thrones-part-34/|title=A Read of Ice and Fire: A Game of Thrones, Part 34|work=Tor.com}}</ref>

''Khal'' Drogo is not a [[Narration#Third-person|point of view]] character in the novels, so his actions are witnessed and interpreted through the eyes of [[Daenerys Targaryen]].<ref>{{cite web|url=http://viewers-guide.hbo.com/map/special/areas-of-control/4/|title=Game of Thrones Viewer's Guide|publisher=HBO}}</ref>

==Television adaptation==
[[File:Jason Momoa Supercon 2014.jpg|thumb|left|upright|[[Jason Momoa]] plays the role of Drogo in the [[Game of Thrones|television series]].]]
Khal Drogo is played by the American actor [[Jason Momoa]] in the television adaption of the series of books.<ref>{{cite web|url=http://www.denofgeek.com/tv/20779/jason-momoa-interview-game-of-thrones-playing-conan-and-more|title='Game of Thrones: Jason Momoa as Khal Drogo|work=DenofGeek}}</ref> At his audition for the part of Drogo he performed the ''[[Ka Mate]]'' [[haka]].<ref>{{cite web|url=http://www.huffingtonpost.com/2014/10/19/jason-momoa-audition-tape_n_6010482.html|title=Jason Momoa Audition tape|publisher=Huffingtonpost}}</ref> Momoa related his experience in reading for the role:

<blockquote>When I read that ''Khal'' Drogo role, I was blown away. I couldn’t believe it was happening, I had to have that role. I was like, "Nobody is going to take that role from me." […] People say it’s easy [to play the role]—"You’re just sitting there!" But it’s extremely hard to be extremely intimidating, and say everything but not say anything.<ref name="Hypable">{{cite web|url=http://www.hypable.com/game-of-thrones-actor-jason-momoa-thinks-playing-khal-drogo-is-phenomenal/|title=Khal Drogo: 'Game of Thrones' actor talks|publisher=Hypable}}</ref></blockquote>

Momoa received positive reviews for his portrayal of the character,<ref>{{cite web|url=http://www.ew.com/article/2011/06/20/game-of-thrones-jason-momoa|title='Game of Thrones' warlord Jason Momoa talks season finale|publisher=EW}}</ref> and contributed his own ideas to his portrayal. For example, in one scene, Drogo cements his position as the lead ''khal'' by ripping out the tongue of a dissenter, Mago. The scene was not in the script, but was suggested by Momoa after he made the observation that Drogo, who is said to be a great warrior, had never been shown demonstrating his prowess as a warrior onscreen.<ref>Jennings, Mike (February 29, 2012). [http://www.denofgeek.com/tv/21095/46-things-we-learned-from-the-game-of-thrones-blu-rays "46 things we learned from the Game Of Thrones Blu-rays"]. [[Den of Geek!]]</ref>

Remarking on the character's early exit:

<blockquote>I was reading it and I was like: "Holy s–t! F–k, I’m dead!" […] It's amazing what [George R. R. Martin] sets up. Here's your lead characters, you're supposed to think about them one way, and you hate them, then you love them, and then they're killed and it's a whirlwind of emotion. All the little kids and even the smallest of characters just grow and grow and grow. He built a beautiful world. I'm bummed I'm not going back. To play ''Khal'' Drogo was phenomenal and I wished there was more stuff he could have done, I'm going to miss that character.<ref name="Hypable" /></blockquote>

==Family tree of House Targaryen==
* See: [[Family tree of House Targaryen|Extended family tree of House Targaryen]]
{{Family tree of Maekar Targaryen}}

== References ==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Drogo, Khal}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional characters introduced in 1996]]
[[Category:Fictional swordsmen]]
[[Category:Fictional gladiators]]
[[Category:Characters in American novels of the 20th century]]