{{Infobox single
|Name           = Burn
|Cover          = Usher - Burn - CD cover.jpg
|Artist         = [[Usher (entertainer)|Usher]]
|from Album     = [[Confessions (Usher album)|Confessions]]
|Released       =  March 21, 2004
|Format         = {{flatlist|
* [[CD single]]
* [[Music download|digital download]]
}}
|Recorded       = 
|Genre          = [[Contemporary R&B|R&B]]
|Length         = 4:15
|Label          = [[Arista Records|Arista]]
|Writer         = {{flatlist|
* [[Usher (entertainer)|Usher]]
* [[Jermaine Dupri]]
* [[Bryan-Michael Cox]]
}}
|Producer       = {{flatlist|
* Jermaine Dupri
* Bryan Michael Cox
}}
|Last single    = "[[Yeah! (Usher song)|Yeah!]]"<br />(2004)
|This single    = "'''Burn'''"<br />(2004)
|Next single    = "[[Confessions Part II]]"<br />(2004)
}}

"'''Burn'''" is a song by American [[Contemporary R&B|R&B]] singer [[Usher (singer)|Usher]], which he wrote with American songwriters [[Jermaine Dupri]], [[Bryan-Michael Cox]]. The song was produced by Dupri and Cox for Usher's fourth studio album, ''[[Confessions (Usher album)|Confessions]]'' (2004). "Burn" is about breakup in a relationship, and the public referred to it as an allusion to Usher's personal struggles. Originally planned as the album's [[lead single]], "Burn" was pushed back after favorable responses for the song "[[Yeah! (Usher song)|Yeah!]]". "Burn" was released as the second single from the album on March 21, 2004. 

"Burn" topped various charts around the world, including the [[Billboard Hot 100|''Billboard'' Hot 100]] for eight non-consecutive weeks; it succeeded "Yeah!" at number one.  Both singles gave Usher nineteen consecutive weeks at the top spot, longer than any solo artist of the Hot 100 era. "Burn" was certified platinum in Australia and United States, and gold in New Zealand. The song was well received by critics and garnered award nominations. In 2009 it was named the 21st most successful song of the 2000s, on the ''Billboard'' Hot 100 Songs of the Decade. This song won the 2005 Kids' Choice Award for Favorite Song.

==Background==
When Usher planned to make a new record after his third album, ''[[8701]]'' (2001), he decided to not branch out that much with musical collaborators and continue building music with his previous producers.<ref name="ReidRoad">{{Cite news |last=Reid |first=Shaheem |title=Road To The Grammys: The Making Of Usher's Confessions |url=http://www.mtv.com/news/articles/1496656/20050207/usher.jhtml |publisher=[[MTV News]]. [[Viacom Media Networks]]|date=February 7, 2005 |accessdate=2008-02-29}}</ref> Usher again enlisted record-producer [[Jermaine Dupri]], who had collaborated on his two previous albums, along with [[The Neptunes]], [[R. Kelly]], among others to work on his fourth studio album ''[[Confessions (Usher album)|Confessions]]'' (2004). Dupri contacted his frequent collaborator [[Bryan-Michael Cox]], who had also made hits like the 2001 single "[[U Got It Bad]]" for Usher. During the early session for the album, Dupri and Cox talked about a situation which later became "Burn". At that time, Usher's two-year relationship with [[TLC (band)|TLC]]'s [[Rozonda Thomas|Chilli]] was flaming out.<ref name="Vineyard">{{Cite news |last=Vineyard |first=Jennifer |title=In Book Proposal, Dupri Calls Em A Hater, Says Usher's Confessions Are Really His |url=http://www.mtv.com/news/articles/1521586/20060125/dupri_jermaine.jhtml |publisher=MTV News. Viacom Media Networks|date=January 25, 2006 |accessdate=2008-03-03}}</ref> They said, "Yo, you gotta let that burn&nbsp;... That's a song right there", and started writing.<ref name="ReidRoad"/>

==Release==
Usher submitted the album to his label [[Arista Records]] after he felt it was already completed.<ref name="ReidShook"/> After he and the company's then-president [[L.A. Reid|Antonio "LA" Reid]] listened to the songs, they felt the album needed a first single and that they needed one or two more songs to create, which caused the postponement of the album's release.<ref name="ReidShook"/><ref name="ReidKingMe"/> Usher went back to the studio and collaborated with [[Lil Jon]] who said, "He needed a single. They had 'Burn,' 'Burn' was hot, but they needed that first powerful monster. That's when I came in."<ref name="ReidKingMe"/> They worked for few more tracks, including "Red Light", which was not included in the first release of the album, and "Yeah!", which features [[Ludacris]] and Lil Jon.<ref name="ReidRoad"/><ref name="ReidShook"/>

However, everybody in the label was scared to decide what to consider as the lead single. Reid was choosing whether "Yeah!" would be released then, considering that they had "Burn".<ref name="ReidKingMe"/> Usher was also doubtful if the former was the right choice, after he wanted an R&B record.<ref name="ReidRoad"/> Until "Yeah!" was leaked, "Burn" was chosen as the official first single from ''Confessions''.<ref>{{Cite news |last=Reid |first=Shaheem |title=Usher To Share His Confessions In March |url=http://www.mtv.com/news/articles/1483836/20031219/usher.jhtml |publisher=MTV News. Viacom Media Networks|date=February 7, 2005 |accessdate=2008-02-29}}</ref> "Yeah!", which was intended as a promotional song and a teaser for Usher's fans, was released to street [[Disc jockey|DJs]] and [[mixtape]]s. However, the song's favorable responses led to another direction;<ref name="Skeleton"/><ref name="ReidShook"/> "Yeah!" was pursued to be the lead single and "Burn" was set as its follow-up.<ref name="ReidRoad"/> "Burn" was released in the United States on July 6, 2004 as a [[CD single]] and [[7-inch single|7" single]].<ref>{{cite web |title=Burn &#91;CD #1&#93; |url=http://www.allmusic.com/album/burn-cd-1-mw0000769551|publisher=[[Allmusic]]. [[Rovi Corporation]]|accessdate=August 12, 2012}}</ref>

==Lyrical interpretation==
Usher decided about the new material "to let it all hang out by singing about some of his own little secrets, as well as a few bones from his homies' skeleton-filled closets."<ref name="ReidRoad"/> The public speculated that the material in the new album he was referring to was his recent personal struggles in which he promised a "real talk" on it.<ref name="ReidKingMe">{{Cite news |last=Reid |first=Shaheem |title=Usher: King Me – Part 3 |url=http://www.mtv.com/bands/u/usher/news_feature_052404/index3.jhtml |publisher=MTV News. Viacom Media Networks|accessdate=2008-02-29}}</ref><ref>{{Cite news |last=Reid |first=Shaheem |title=Usher Works With R. Kelly, Neptunes To Deliver Real Talk On New LP |url=http://www.mtv.com/news/articles/1474674/20030723/usher.jhtml |publisher=MTV News. Viacom Media Networks|date=July 24, 2004 |accessdate=2008-02-29}}</ref> In early 2004, Usher broke up with Chilli due to "irreconcilable differences and because they found it almost impossible to make compromises."<ref name="ReidShook">{{Cite news |last=Reid |first=Shaheem |title=The Road To Confessions: How Usher 'Shook A Million' |url=http://www.mtv.com/news/articles/1486094/20040331/usher.jhtml |publisher=MTV News. Viacom Media Networks|date=March 31, 2004 |accessdate=2008-02-29}}</ref><ref name="ReidKingMe"/> Usher said in an interview: "It's unfortunate when you have to let a situation go because it's not working", which added reference to the breakup.<ref name="Skeleton">{{Cite news |title=Usher Lets Skeletons Out Of The Closet On Confessions |url=http://www.mtv.com/news/articles/1485074/20040217/usher.jhtml |publisher=MTV News. Viacom Media Networks|date=February 17, 2004 |accessdate=2008-02-29}}</ref> It was later revealed that chilli, in fact broke up with usher because of cheating and the media said otherwise because of the lyrics in the song, which was not based on their relationship. Dupri, however, confessed that his personal life is the real story of the album.<ref name="Vineyard"/> Usher said he took inspirations collectively by looking at his friends' personal situations that they gone through.<ref>{{Cite news |last=Reid |first=Shaheem |title=Usher Says He's Not A Baby's Daddy |url=http://www.mtv.com/news/articles/1485933/20040323/usher.jhtml |publisher=MTV News. Viacom Media Networks|date=March 23, 2004 |accessdate=2008-03-03}}</ref>

==Composition==
{{Listen
| filename    = Usher Burn.ogg
| title       = "Burn"
| description = "Burn" is a slow jam, that combines [[Contemporary R&B|R&B]] and [[Ballad (music)|ballad]] genres.
| pos         = right
}}
"Burn" is a slow jam,<ref name="Caramanica">{{cite journal|last=Caramanica|first=Jon|date=April 2004|title=Review: ''Confessions''|journal=[[Blender (magazine)|Blender]]|publisher=Alpha Media Group|page=132}}</ref> combining [[Contemporary R&B|R&B]] and [[Ballad (music)|ballad]] genres. The song is performed with a moderate [[Groove (music)|groove]]. It is composed in the key of [[C-sharp major]].<ref>{{Cite web |title=Usher Digital Sheet Music: Burn  |url=http://www.musicnotes.com/sheetmusic/scorch.asp?ppn=SC0008474 |publisher=Musicnotes.com. [[Sony/ATV Music Publishing]]|accessdate=2008-02-29}}</ref> The [[melody]] line of the song has influences from "[[Ignition (song)|Ignition (Remix)]]" by R. Kelly and "[[How to Deal]]" by [[Frankie J]].<ref name="Cibula"/> "Burn"crb has a combination of robotic noises, [[synthesizer|synthesized]] strings and guitar lines.<ref name="Cibula"/>

The lyrics are constructed in the traditional [[verse-chorus form]]. The song starts a spoken intro, giving way to the first verse. It continues to the chorus, following the second verse and chorus. The [[bridge (music)|bridge]] follows, leading to a break and finalizing in the chorus. "Burn" was considered a "window to Usher's inner thoughts", along with the controversial track "Confessions" and "[[Confessions Part II]]". The song is about breakups and ending relationships.<ref name="ReidRoad"/><ref name="Skeleton"/> According to Matt Cibula of [[Popmatters]], "Burn" is constructed from "two-step concept". In the lyrics "You know that it's over / You know that it was through / Let it burn / Got to let it burn", Usher breaks up with his woman but found her sad about feeling bad about what happened to their relationship. However, Usher says that she must deal with it before she can accept the truth. For the lines "It's been fifty-eleven days / Umpteen hours / I'm gonna be burnin' / Till you return", the direction changes after Usher realized that breaking up with her was a huge mistake and that he wanted her back to him.<ref name="Cibula">{{Cite web |last=Cibula |first=Matt |title=Usher: By the Numbers! |url=http://www.popmatters.com/music/reviews/u/usher-confessions.shtml |publisher=[[PopMatters]] |date=April 6, 2004 |accessdate=2008-02-29}}</ref>

==Critical reception==
"Burn" was lauded by contemporary music critics. Jem Aswad of ''[[Entertainment Weekly]]'' complimented Dupri and Cox for producing what he called the "best song" from the album, along with "[[Confessions Part II]]" which they also produced. Aswad found the songs feature "mellifluous melodies".<ref>{{Cite web |last=Aswad |first=Jem |title=Confessions (2004): Usher |url=http://www.ew.com/ew/article/0,,602386,00.html |work=[[Entertainment Weekly]] |publisher=[[Time Warner]]|date=March 26, 2004 |accessdate=2008-02-29}}</ref> Laura Sinagra of ''[[Rolling Stone]]'' found Usher's singing a "sweet falsetto on the weepy breakup song ", adding, it "convincingly marries resolve and regret, but when it comes to rough stuff, there's still no 'u' in p-i-m-p."<ref>{{Cite web|last=Sinagra |first=Laura |title=Usher: Confessions |url=http://www.rollingstone.com/artists/usher/albums/album/5165180/review/5944284/confessions |work=[[Rolling Stone]] |accessdate=2008-02-29 |archiveurl=https://web.archive.org/web/20071213035104/http://www.rollingstone.com/artists/usher/albums/album/5165180/review/5944284/confessions |archivedate=2007-12-13 |deadurl=yes |df= }}</ref> Cibula called the song brilliant and considers its step one and step two technique a hit.<ref name="Cibula"/> Jon Caramanica of ''[[Blender (magazine)|Blender]]'' complimented the song for living up as the only "serviceable" among all ballad-influenced songs in the album which "often drown in their own inanity."<ref name="Caramanica"/> Ande Kellman of [[Allmusic]] considered "Burn" as one of the Usher's best moments in the album, together with "[[Caught Up (Usher song)|Caught Up]]", the final single from ''Confessions''.<ref>{{Cite web |last=Kellman |first=Andy |title=Confessions: Usher |url={{Allmusic|class=album|id=r681098|pure_url=yes}}|publisher=Allmusic. Rovi Corporation|accessdate=2008-03-03}}</ref> Steve Jones of ''[[USA Today]]'' stated that Usher is singing about a relationship that cannot be saved because of the "flame has simply died".<ref>{{Cite web |last=Jones |first=Steve |title='Confessions' time: Usher continues his ascendancy |url=http://www.usatoday.com/life/music/reviews/2004-03-22-usher-confessions_x.htm |work=[[USA Today]] |publisher=Gannett Co. Inc |date=March 22, 2004 |accessdate=2008-03-03}}</ref>

"Burn" was nominated at the [[47th Grammy Awards]] for [[Grammy Award for Best Male R&B Vocal Performance|Best Male R&B Vocal Performance]] and [[Grammy Award for Best R&B Song|Best R&B Song]].<ref>{{Cite web |last=Jeckell |first=Barry |title=Kanye West Leads Grammy Nominees |url=http://www.billboard.com/articles/news/65368/kanye-west-leads-grammy-nominees |work=Billboard |publisher=Prometheus Global Media|date=December 7, 2004 |accessdate=2008-02-29}}</ref> The song earned British record company [[EMI]] the "Publisher of the Year" at the [[American Society of Composers, Authors, and Publishers]] 2005 Pop Music Awards.<ref>{{Cite web |title=ASCAP Pop Music Awards 2005 |url=http://www.ascap.com/eventsawards/awards/popawards/2005/ |publisher=ASCAP |year=2005 |accessdate=2008-02-29}}</ref>

==Chart performance==
"Burn" was another commercial success for Usher. In the United States, the single debuted on the [[Billboard Hot 100|''Billboard'' Hot 100]] at number sixty-five, months prior to its physical release.<ref name="aCharts">{{Cite web |title="Burn" Global Chart Positions and Trajectories |url=http://acharts.us/song/412 |publisher=αCharts |accessdate=2008-02-28}}</ref> It reached the top spot on May 29, 2004, replacing "Yeah!"'s twelve-week run at number one.<ref>{{Cite web |title=Billboard Hot 100 |url=http://acharts.us/billboard_hot_100/2004/21 |publisher=αCharts |date=May 29, 2004 |accessdate=2008-02-29|archiveurl = https://web.archive.org/web/20080209115022/http://acharts.us/billboard_hot_100/2004/21 |archivedate = February 9, 2008}}</ref> The single was beaten by [[Fantasia (singer)|Fantasia]]'s 2004 single "[[I Believe (Fantasia song)|I Believe]]", which propelled on the chart on its debut.<ref>{{Cite web |title=Billboard Hot 100 |url=http://acharts.us/billboard_hot_100/2004/28 |publisher=αCharts |date=July 17, 2004 |accessdate=2008-02-29 |archiveurl = https://web.archive.org/web/20080209115444/http://acharts.us/billboard_hot_100/2004/28 |archivedate = February 9, 2008}}</ref> It returned to number one for one last week, before it was finally knocked off by the album's third single "[[Confessions Part II]]".<ref>{{Cite web |title=Billboard Hot 100 |url=http://acharts.us/billboard_hot_100/2004/30 |publisher=αCharts |date=July 31, 2004 |accessdate=2008-02-29}}</ref> The single failed to remain on the top spot as long as "Yeah!" did, staying only for eight non-consecutive weeks.<ref name="aCharts"/> "Burn" was the fifth most-played song in 2004 for earning 355,228 total plays, alongside "Yeah!" which topped the tally for 496,805 spins.<ref>{{Cite news |last=Vineyard |first=Jennifer |title= Usher's 'Yeah!' Was Most Played Song Of 2004 |url=http://www.mtv.com/news/articles/1495342/20050105/usher.jhtml |publisher=MTV News. Viacom Media Networks|date=January 5, 2005 |accessdate=2008-03-01}}</ref> The single was certified platinum by the [[Recording Industry Association of America]] for shipping 1,000,000 units.<ref>{{Cite web |title=Gold and Platinum |url=http://www.riaa.com/goldandplatinumdata.php?resultpage=2&table=SEARCH_RESULTS&action=&title=&artist=usher&format=&debutLP=&category=&sex=&releaseDate=&requestNo=&type=&level=&label=&company=&certificationDate=&awardDescription=&catalogNo=&aSex=&rec_id=&charField=&gold=&platinum=&multiPlat=&level2=&certDate=&album=&id=&after=&before=&startMonth=1&endMonth=1&startYear=1958&endYear=2008&sort=Artist&perPage=25 |publisher=[[Recording Industry Association of America]] (RIAA) |accessdate=2008-03-02}}</ref> It became the second best-selling single in the United States, behind Usher's single "[[Yeah! (Usher song)|Yeah!]]".<ref>{{Cite web |title=The Billboard Hot 100: 2004 |url=http://billboard.com/bbcom/charts/yearend_chart_display.jsp?f=The+Billboard+Hot+100&g=Year-end+Singles&year=2004 |work=Billboard |publisher=Prometheus Global Media|accessdate=2008-03-02}}</ref> This gives him the distinction, alongside The Beatles in 1964, to have 2 of his singles occupying the top 2 spots on the Billboard Year-End Chart. Like "Yeah!", "Burn" helped ''Confessions'' remain on the top spot.<ref>{{Cite news |last=D'Angelo |first=Joe |title='Burn' Keeps Usher Hot — And On Top |url=http://www.mtv.com/news/articles/1487119/20040519/usher.jhtml |publisher=MTV News. Viacom Media Networks|date=May 19, 2004 |accessdate=2008-03-03}}</ref>

Internationally, several music markets responded equally well. In the United Kingdom, the single debuted at number one and stayed for two weeks.<ref name="aCharts"/> Across European countries, the single performed well, reaching the top ten in [[Denmark]], Ireland, [[Norway]], the [[Netherlands]], and [[Switzerland]]. It entered the top twenty in [[Austria]], [[Belgium]], Germany and [[Sweden]].<ref name="aCharts"/> In Australia, the single debuted at number three and peaked at number two. The single was certified platinum by the [[Australian Recording Industry Association]] for selling 70,000 units.<ref>{{Cite web|title=ARIA Charts - Accreditations - 2004 Singles |url=http://www.aria.com.au/pages/aria-charts-accreditations-singles-2004.htm |publisher=[[Australian Recording Industry Association]] (ARIA) |accessdate=2008-02-29 |deadurl=yes |archiveurl=http://www.webcitation.org/64wvNA3FI?url=http%3A%2F%2Fwww.aria.com.au%2Fpages%2Faria-charts-accreditations-singles-2004.htm |archivedate=2012-01-25 |df= }}</ref> At the 2004 year ender charts, "Burn" became the thirty-first best-selling single in Australia.<ref>{{Cite web|title=ARIA Charts - End Of Year Charts - Top 100 Singles 2004 |url=http://www.aria.com.au/pages/aria-charts-end-of-year-charts-top-100-singles-2004.htm |publisher=Australian Recording Industry Association (ARIA) |accessdate=2008-03-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20150502063756/http://www.aria.com.au:80/pages/aria-charts-end-of-year-charts-top-100-singles-2004.htm |archivedate=2015-05-02 |df= }}</ref> In the New Zealand, it peaked at number one for three weeks, and remained on the chart for twenty-three weeks.<ref name="aCharts"/> The single was certified gold by the [[Recording Industry Association of New Zealand]].

== Music video ==
===Background===
The music video for "Burn" was directed by [[Jake Nava]], who had produced a wide array of videos for [[Atomic Kitten]], [[Beyoncé Knowles]], among others. It was shot at the former Hollywood house of American [[traditional pop music|popular singer]] [[Frank Sinatra]]. The video features model [[Jessica Clark (actress)|Jessica Clark]].<ref>{{Cite news |title=Usher - Usher Falls For Model Girlfriend |url=http://www.contactmusic.com/new/xmlfeed.nsf/mndwebpages/usher%20falls%20for%20model%20girlfriend |publisher=Contactmusic.com |date=April 30, 2004 |accessdate=2008-02-29}}</ref> In the July 2008 issue of ''[[Vibe (magazine)|Vibe]]'' magazine, Usher told writer Mitzi Miller, "Women have started to become lovers of each other as a result of not having enough men."<ref name="VIBE">{{Cite web|title=Caught Up : VIBE.com |url=http://www.vibe.com/news/cover_stories/2008/06/usher_july_cover_story/ |accessdate=2008-07-05 |publisher=VIBE.com |archiveurl=https://web.archive.org/web/20080704091846/http://www.vibe.com/news/cover_stories/2008/06/usher_july_cover_story/ |archivedate=2008-07-04 |deadurl=yes |df= }}</ref> On June 26, 2008, [[AfterEllen.com]] writer [[Sarah Warn]] revealed that Jessica Clark, the lead in Usher's "Burn" music video, was in fact an openly gay model. In the article, Warn writes, "Maybe it's not a lack of men that's turning women gay, Usher--maybe it's you!"<ref name="AfterEllen">{{Cite web |title=Best. Lesbian. Week. Ever. (June 27, 2008) |url=http://www.afterellen.com/blwe/06-27-08?page=0%2C2 |publisher= [[AfterEllen.com]]|accessdate=2008-07-05}}</ref>

===Synopsis and reception===
[[File:Usher Burn.PNG|thumb|220px|right|Usher dancing in front of an [[Aston Martin DB5]], while the scenery sets aflame.]]
The video starts with Usher sitting on a sofa with a [[theatrical scenery|backdrop]] of his girlfriend. When the verse starts, Usher went to a wide glass window pane, looking at his girlfriend swimming in the pool. The surface aflame after she immersed in the water. The next scene continues to Usher with his mistress having sex. While sitting on the edge of the bed, Usher reminisces the moments he and his girlfriend were having an intimate moment in the same bed. The bedsheets burns, following to Usher riding a silver right-hand drive [[Aston Martin DB5]] with a British registration - EGF 158B (the car was featured in the TV series [[Fastlane (TV series)|Fastlane]]). The video cuts with the backdrop also burning. Continuing to the car scene, Usher stops as he sees his imaginary girl again. He went out and dances, executing various hand routines. Video intercuts follow and the video ends with Usher standing with his back. Also, right before the last chorus, the screen changes from a small screen, to a full one with no framework.

The music video debuted on [[MTV]]'s ''[[Total Request Live]]'' at number six on May 4, 2004, the same debut with "Confessions Part II".<ref name="TRL">{{Cite web |title=TRL Debuts |url=http://host17.hrwebservices.net/~atrl/trlarchive/db.html |publisher=Popfusion |accessdate=2008-02-29|archiveurl = https://web.archive.org/web/20080307193841/http://host17.hrwebservices.net/~atrl/trlarchive/db.html |archivedate = March 7, 2008}}</ref> The video reached the top spot and remained on the countdown for thirty-three days.<ref name="TRL"/> "Burn" topped [[MuchMusic]]'s ''[[Countdown (MuchMusic TV series)|Countdown]]'' on July 24, 2004, and remained on the chart for fifteen weeks.<ref name="Top40">{{Cite web |title=Usher: Burn |url=http://top40-charts.com/songs/full.php?sid=9610&sort=chartid |publisher=Top40-chart.com |accessdate=2008-02-29}}</ref>

==Impact==
Besides from Usher, Cox has benefited for co-creating ''Confessions'', as well as from the success of "Burn". He has been doing records for [[Alicia Keys]], [[B2K]], [[Mariah Carey]] and [[Destiny's Child]], but he felt 2004 introduced him to another landscape in the music industry. His contribution to the song has elevated him to fame, as well as people looking back to his past records. "Burn" earned him two Grammy nominations. Cox stated, "Everybody who does this for a living, dreams about being nominated. It's the ultimate accomplishment. I've always been the silent guy — I come in, do my job and head out. I like to leave all the glory and shine to others, but this is the validation that means the most to me. It also makes me want to work harder to get that same recognition again."<ref>{{Cite news |last=Hall |first=Rashaun |title=Usher's Success Lifts Songwriter/Producer Bryan-Michael Cox |url=http://www.mtv.com/news/articles/1495827/20050112/usher.jhtml |publisher=MTV News. Viacom Media Networks |date=January 12, 2005 |accessdate=2008-03-03}}</ref>

==Charts==
{{col-start}}
{{col-2}}

===Weekly charts===
{|class="wikitable sortable plainrowheaders"
!scope="col"|Chart (2004)
!scope="col"|Peak<br />position
|-
!scope="row"{{singlechart|Australia|2|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|Austria|21|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|Flanders|18|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|Wallonia|28|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|Denmark|10|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|France|29|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|Germany|11|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|Hungarytop10|10|year=2004|week=29}}
|-
!scope="row"{{singlechart|Ireland|2|year=2004|week=27}}
|-
!scope="row"{{singlechart|Dutch100|12|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|New Zealand|1|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|Norway|10|artist=Usher|song=Burn}}
|-
{{singlechart|Scotland|2|date=2014-09-21|rowheader=true|accessdate=Juli 11, 2004}}
|-
!scope="row"{{singlechart|Sweden|18|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|Switzerland|10|artist=Usher|song=Burn}}
|-
!scope="row"{{singlechart|UKchartstats|1|artist=Usher|song=Burn|songid=215}}
|-
!scope="row"{{singlechart|Billboardhot100|1|artist=Usher|song=Burn|artistid={{BillboardID|Usher}}}}
|-
!scope="row"{{singlechart|Billboardrandbhiphop|1|artist=Usher|song=Burn|artistid={{BillboardID|Usher}}}}
|-
!scope="row"{{singlechart|Billboardpopsongs|2|artist=Usher|song=Burn|artistid={{BillboardID|Usher}}}}
|}

{{col-2}}

===Year-end charts===
{|class="wikitable sortable plainrowheaders"
!scope="col"|Chart (2004)
!scope="col"|Position
|-
!scope="row"| Australia (ARIA)<ref>{{cite web|title=2004 ARIA Singles Chart|url=http://www.ariacharts.com.au/Annual-Charts/2004/Singles-Chart|website=ARIA Charts|publisher=ARIA - Australian Recording Industry Association Ltd.|accessdate=19 March 2016}}</ref>
|style="text-align:center;"|31
|-
!scope="row"| Belgium (Ultratop Flanders)<ref>{{cite web|title=Jaaroverzichten 2004|url=http://www.ultratop.be/nl/annual.asp?year=2004|website=Ultratop|publisher=Ultratop & Hung Medien|accessdate=19 March 2016|language=Dutch}}</ref>
|style="text-align:center;"|95
|-
!scope="row"| Germany (Official German Charts)<ref>{{cite web|title=Top 100 Single-Jahrescharts 2004|url=https://www.offiziellecharts.de/charts/single-jahr/for-date-2004|website=Offizielle Deutsche Charts|publisher=GfK Entertainment|accessdate=19 March 2016|language=German}}</ref>
|style="text-align:center;"|80
|-
!scope="row"| Netherlands (Dutch Top 40)<ref>{{cite web|title=Top 100-Jaaroverzicht van 2004|url=http://www.top40.nl/bijzondere-lijsten/top-100-jaaroverzichten/2004|website=Dutch Top 40|publisher=Stichting Nederlandse Top 40|accessdate=19 March 2016|language=Dutch}}</ref>
|style="text-align:center;"|84
|-
!scope="row"| New Zealand (Recorded Music NZ)<ref>{{cite web|title=Top Selling Singles of 2004|url=http://nztop40.co.nz/chart/?chart=2085|website=NZTop40|publisher=Recorded Music New Zealand Limited|accessdate=19 March 2016}}</ref>
|style="text-align:center;"|7
|-
!scope="row"| Switzerland (Schweizer Hitparade)<ref>{{cite web|title=Schweizer Jahreshitparade 2004|url=http://www.hitparade.ch/charts/jahreshitparade/2004|website=Hitparade|publisher=Hung Medien|accessdate=19 March 2016|language=Swedish}}</ref>
|style="text-align:center;"|63
|-
!scope="row"| UK Singles (Official Charts Company)<ref>{{cite web|title=ChartsPlusYE2004|url=http://www.ukchartsplus.co.uk/ChartsPlusYE2004.pdf|website=UKChartsPlus|publisher=ChartsPlus|accessdate=19 March 2016|format=PDF}}</ref>
|style="text-align:center;"|27
|-
!scope="row"| US ''Billboard'' Hot 100<ref>{{cite web|title=Top 100 Songs of 2004 - Billboard Year End Charts|url=http://www.bobborst.com/popculture/top-100-songs-of-the-year/?year=2004|website=Bobborst|publisher=Bobborst|accessdate=19 March 2016}}</ref>
|style="text-align:center;"|2
|}

===Decade-end charts===
{|class="wikitable plainrowheaders"
|-
!scope="col"|Chart (2000–2009)
!scope="col"|Position
|-
!scope="row"| US ''Billboard'' Hot 100<ref name="decade-end">{{Cite web|title= Top 100 Music Hits, Top 100 Music Charts, Top 100 Songs & The Hot 100: Best of the 2000s – Billboard 100 |url=http://www.billboard.com/charts-decade-end/hot-100-songs?year=2009#/charts-decade-end/hot-100-songs?year=2009&begin=21&order=position|work=Billboard |publisher=Prometheus Global Media|accessdate=August 12, 2012}}</ref>
|style="text-align:center;"|21
|}
{{col-end}}

==Certifications==
{{Certification Table Top}}
{{Certification Table Entry|region=Australia|type=single|title=Burn|artist=Usher|award=Platinum|certyear=2004}}
{{Certification Table Entry|region=New Zealand|type=single|title=Burn|artist=Usher|award=Platinum|certyear=2004|id=1443}}
{{Certification Table Entry|region=United Kingdom|type=single|title=Burn|artist=Usher|award=Silver|certyear=2013|relyear=2004}}
{{Certification Table Entry|region=United States|type=single|title=Burn|artist=Usher|award=Platinum|salesamount=1,000,000|relyear=2004}}
{{Certification Table Bottom}}

==Other versions==
An [[a cappella]] version was created by [[South Asian]] fusion a cappella group [[Stanford Raagapella]].<ref>{{Cite web |title=Stanford Raagapella |url=http://www.raagapella.com |publisher=Stanford Raagapella.com|accessdate=2012-08-25}}</ref>

==See also==
{{Wikipedia books|Confessions}}
*[[List of number-one singles from the 2000s (New Zealand)#2004|List of number-one singles from the 2000s (New Zealand)]]
*[[List of number-one singles from the 2000s (UK)#2004|List of number-one singles from the 2000s (UK)]]
*[[List of Hot 100 number-one singles of 2004 (U.S.)]]
*[[List of number-one R&B singles of 2004 (U.S.)]]
*[[List of Billboard Rhythmic number-one songs of the 2000s#2004|List of Billboard Rhythmic number-one songs of the 2000s]]

==References==
{{Reflist|colwidth=30em}}

==External links==
* {{MetroLyrics song|usher|burn}}<!-- Licensed lyrics provider -->

{{Usher singles}}

{{good article}}

[[Category:2004 singles]]
[[Category:Usher (singer) songs]]
[[Category:Billboard Hot 100 number-one singles]]
[[Category:Billboard Hot R&B/Hip-Hop Songs number-one singles]]
[[Category:UK Singles Chart number-one singles]]
[[Category:UK R&B Singles Chart number-one singles]]
[[Category:Number-one singles in New Zealand]]
[[Category:Music videos directed by Jake Nava]]
[[Category:Songs written by Bryan-Michael Cox]]
[[Category:Songs written by Jermaine Dupri]]
[[Category:Pop ballads]]
[[Category:Rhythm and blues ballads]]
[[Category:Song recordings produced by Jermaine Dupri]]
[[Category:Songs written by Usher (entertainer)]]
[[Category:2004 songs]]
[[Category:Arista Records singles]]