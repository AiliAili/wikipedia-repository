{{featured article}}
{{Infobox coin
| Country             = United States
| Denomination        = Flying Eagle cent
| Value               = 1 cent (.01
| Unit                = [[United States dollar|U.S. dollars]])
| Mass                = 4.67
| Diameter            = 19
| Edge                = Plain
| Composition =
  {{plainlist |
* 88.0% copper
* 12.0% nickel
  }}
| Mint mark = None.  Struck at the [[Philadelphia Mint]] without mint mark.
| Years of Minting    = 1856–1858
| Obverse             = {{Css Image Crop|Image = NNC-US-1858-1C-Flying Eagle Cent.jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 3|Location = center}}
| Obverse Design      = Eagle in flight
| Obverse Designer    = [[James B. Longacre]]
| Obverse Design Date = 1856
| Reverse             = {{Css Image Crop|Image = NNC-US-1858-1C-Flying Eagle Cent.jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 238|Location = center}}
| Reverse Design      = Denomination enclosed by wreath
| Reverse Designer    = James B. Longacre
| Reverse Design Date = 1856
}}
The '''Flying Eagle cent''' is a one-cent piece struck by the [[United States Mint|Mint of the United States]] as a [[pattern coin]] in 1856, and for circulation in 1857 and 1858. The coin was designed by Mint Chief Engraver [[James B. Longacre]], with the eagle in flight based on the work of Longacre's predecessor, [[Christian Gobrecht]].

By the early 1850s, the [[large cent (United States coin)|large cent]] (about the size of a [[Half dollar (United States coin)|half dollar]]) being issued by the Mint was becoming both unpopular in commerce and expensive to coin.  After experimenting with various sizes and compositions, the Mint decided on an alloy of 88% copper and 12% nickel for a new, smaller cent. After the Mint produced patterns with an 1856 date and gave them to legislators and officials, Congress formally authorized the new piece in February 1857.

The new cent was issued in exchange for the worn Spanish colonial silver coin that had circulated in the U.S. until then, as well as for its larger predecessor. So many cents were issued that they choked commercial channels, especially as they were not [[legal tender]] and no one had to take them.  The eagle design did not strike well, and was replaced in 1859 by Longacre's [[Indian Head cent]].

== Inception ==
{{main article|Large cent (United States coin)}}
The cent was the first official United States coin to be struck at the [[Philadelphia Mint]] in 1793.{{sfn|Breen|p=177}}  These pieces, today known as [[large cent (United States coin)|large cents]], were made of pure copper and were about the size of a [[half dollar (United States coin)|half dollar]].{{sfn|Lange|p=27}}  They were struck every year, except 1815 due to a shortage of metal, but were slow to become established in commerce. Worn Spanish colonial silver pieces were then commonly used as money throughout the United States.{{sfn|Lange|pp=40–41}}

[[File:Feucht reverse.jpg|thumb|left|Reverse of the 1837 [[Feuchtwanger Cent|Feuchtwanger cent]]]]
The Mint then struck silver or gold in response to deposits by those holding bullion, and made little profit from those transactions.  By the 1840s, profits, or [[seignorage]], from monetizing copper into cents helped fund the Mint.  In 1849, copper prices rose sharply, causing the Department of the Treasury to investigate possible alternatives to the large one-cent pieces.{{sfn|Snow|p=7}}  The cent was unpopular in trade; as it was not a [[legal tender]], nobody had to take it, and banks and merchants often refused it.{{sfn|Breen|p=215}}  The cent was disliked for its large size as well.  In 1837, the eccentric New York chemist [[Lewis Feuchtwanger]] had experimented with a smaller cent size in making model coins as part of a plan to sell his alloy (similar to base-metal [[German silver]]) to the government for use in coinage.  [[Feuchtwanger Cent|His pieces]] circulated as [[hard times token]]s in the recession years of the late 1830s and early 1840s.{{sfn|Breen|p=215}}

By 1850, it was no longer profitable for the Mint to strike cents, and on May 14, New York Senator [[Daniel S. Dickinson]] introduced legislation for a cent made out of [[billon (alloy)|billon]], copper with a small amount of silver.   At the time, it was widely felt that coins should contain a large proportion of their face value in metal.   The coin would be annular; that is, it would have a hole in the middle.  The Mint struck experimental pieces, and found that it was difficult to eject such pieces from the presses where they were struck, and that it was expensive to recover the silver from the alloy.  Provisions for a smaller cent were dropped from the legislation that gave congressional approval for the [[Three-cent piece (United States coin)|three-cent piece]] in 1851.{{sfn|Taxay|pp=233–234}}  Numismatic historian [[Walter Breen]] suggested that one factor in rejecting the holed coins was that they reminded many of [[Cash (Chinese coin)|Chinese cash]] coins with their minimal purchasing value.{{sfn|Breen|p=215}} A drop in copper prices in 1851 and  early 1852 made the matter of a smaller cent less urgent at the Department of the Treasury, which supervised Mint activities.{{sfn|Snow|p=7}}

[[File:1856 half cent pattern.jpg|thumb|Pattern half cent in copper-nickel, struck to display the alloy as a coin]]
Copper prices resurged in late 1852 and into 1853 past the $0.40 per pound that the Mint viewed as the break-even point for cent manufacture after considering the cost of production; {{convert|1|lb}} of copper made 42⅔ large cents.  In 1853, patterns using a base-metal alloy were struck using a quarter eagle obverse die, about the size of a dime.{{sfn|Snow|pp=7–8}}  Some of the proposed alloys contained the metal [[nickel]].{{sfn|Breen|p=215}}  Also considered for use in the cent was "French bronze" (95% copper with the remainder tin and zinc){{efn|Later used for the cent from 1864 until 1982, except in 1943. See {{harvnb|Yeoman|pp=115–122}}}} and various varieties of German silver.  In his 1854 annual report, [[Director of the United States Mint|Mint Director]] [[James Ross Snowden]] advocated the issue of small, bronze cents, as well as the elimination of the [[half cent (United States coin)|half cent]], which he described as useless in commerce.{{sfn|Taxay|p=235}}  A number of pattern cents were struck in 1854 and 1855.  These featured various designs, including several depictions of [[Liberty (goddess)|Liberty]] and two adaptations of work by the late Mint chief engraver, [[Christian Gobrecht]]: one showing a seated Liberty, which Gobrecht had placed on the silver coins in the 1830s, and another of a flying eagle, which Gobrecht had created based upon a sketch by [[Titian Peale]].{{sfn|Snow|pp=9, 225}}

== Preparation ==
[[File:James Barton Longacre - Ambrotype by Isaac Rehn, 1855.jpg|thumb|right|upright|[[James B. Longacre]] designed the Flying Eagle cent.]]

In early 1856, Snowden proposed legislation to allow him to issue a smaller cent, but leaving the size and metallic composition up to him and Secretary of the Treasury [[James Guthrie (Kentucky)|James Guthrie]].  Under the plan, the new piece would be legal tender, up to ten cents.  It would be issued in exchange for the old Spanish silver still circulating in the United States.  In the exchange, the Spanish silver would be given full value (12½ cents per [[Spanish colonial real|real]], or bit) when normally such pieces traded at about a 20% discount due to wear.  The loss the government would take on the trade would be paid for by the seignorage on the base-metal pieces.  The new cents would also be issued for the old cents, and in exchange for the same value in half cents—that denomination was to be discontinued.{{sfn|Taxay|p=235}}  The bill was introduced in the Senate on March 25, 1856.  The old cent weighed {{convert|168|gr|g}}; on April 16, the bill was amended to provide for a cent of at least 95% copper weighing at least {{convert|96|gr|g}} and passed the Senate in that form.{{sfn|Breen|p=235}}{{sfn|Taxay|p=167}}

While the legislation was being considered, Mint Melter and Refiner<!-- "Melter and Refiner" is the title. --> [[James Curtis Booth]] was conducting experiments on alloys that might be appropriate for the new cent.{{sfn|Taxay|p=235}}  In July 1856, Snowden wrote to Guthrie, proposing an alloy of 88% copper and 12% nickel as ideal and suggesting amendments to the pending bill that would accomplish this.  Booth also wrote to Guthrie to boost the alloy;{{sfn|Taxay|pp=235–237}} both men proposed a weight of {{convert|72|gr|g}} as convenient as 80 cents would equal a troy pound (373 g), although the avoirdupois pound (454 g) was more commonly used for base metals.{{sfn|Snow|p=9}}

[[File:1854 flying eagle cent.jpg|thumb|left|upright|Early pattern coin for the Flying Eagle cent]]

The Mint's chief engraver, [[James B. Longacre]], was instructed to prepare designs for pattern coins.{{sfn|Snow|p=9}}  Initially, Longacre worked with Liberty head designs such as were common at the time, but Snowden asked that a flying eagle design be prepared.  This occurred as Booth's experiments continued; the first cent patterns with the flying eagle design were about the size of a quarter.{{sfn|Julian}}  To promote the new alloy, the Mint had 50 half cents struck in it, and had them sent to Washington for Treasury officials to show to officials and congressmen.{{sfn|Breen|p=215}}  In early November 1856, Longacre prepared dies in what would prove to be the final design, depicting a flying eagle on the obverse and a wreathed denomination on the reverse, in the size sought by Booth.{{sfn|Julian}}

The Mint struck at least several hundred patterns using Longacre's flying eagle design in the proposed composition.  In an effort to secure public acceptance of the new pieces, these were distributed to various congressmen and other officials, initially in November 1856.  Two hundred were sent to the House Committee on Coinage, Weights and Measures, while four were given to President [[Franklin Pierce]].  At least 634 specimens were distributed, and possibly several thousand; extra were available on request.  This was the origin of  the highly collectable 1856 Flying Eagle cent, which is considered by numismatists as part of the Flying Eagle series although it was actually a pattern or transition piece, not an official coin, as congressional approval had not yet been granted.  Additional 1856 small cents were later struck by Snowden for illicit sale, and to exchange for pieces the Mint sought for [[National Numismatic Collection|its coin collection]].{{sfn|Julian}}{{sfn|Snow|pp=15–16}}

[[File:1776 Potosi 2 reales obv.jpg|thumb|upright|left|A Spanish colonial two-reales piece ("two bits") from the Potosí Mint (today in Bolivia)]]
In December 1856, Snowden wrote to Missouri Representative [[John S. Phelps]], hoping for progress with the legislation, and stating that he was already "pressed on all hands, and from every quarter, for the new cent—in fact, the public are very anxious for its issue".{{sfn|Taxay|p=237}}  When the legislation, amended to include the weight and alloy the Mint had decided on, was debated in the House of Representatives on December 24, it was opposed by Tennessee Congressman [[George Washington Jones (Tennessee politician)|George Washington Jones]] over the legal tender provision; Jones felt that under the Constitution, only gold and silver should be made legal tender.  Phelps defended the bill on the ground that Congress had the constitutional power to regulate the value of money, but when the bill was brought back up to be considered on January 14, 1857, the legal tender provision had been removed.  This time, the bill was opposed by New York Congressman [[Thomas R. Whitney]], who objected to a provision in the bill that legalized the Mint's practice of designing and striking medals commissioned by the public, feeling that the government should not compete with private medallists.  The provision was removed, and the bill passed the following day.  The House version was then considered by the Senate, which debated it on February 4, and passed it with a further amendment allowing the redemption of the Spanish coins for a minimum of two years.  The House agreed to this on February 18, and President Pierce signed the bill on the 21st.{{sfn|Taxay|p=237}}  The act made foreign gold and silver coins no longer legal tender, but Spanish dollars were redeemable at their nominal value for two years in exchange for the new copper-nickel cents.  The half cent was abolished.{{sfn|Bureau of the Mint|pp=44–45}} The new pieces would be the same size (19&nbsp;mm), though somewhat heavier, than cents are today.{{sfn|Yeoman|pp=113, 126}}

In anticipation of the success of the legislation, most of the 333,456 large cents struck in 1857 never left the Philadelphia Mint, and were later melted.{{sfn|Snow|p=16}}  Snowden purchased a new set of rollers and other equipment so that the Mint could produce its own cent [[planchet]]s, the first time it had done so in over 50 years.{{sfn|Taxay|pp=237–238}}  Although the legislation was still a day from final passage, Snowden recommended Longacre's designs to Guthrie on February 20. Guthrie approved them on the 24th, though he requested that the edge of the coin be made less sharp; Snowden promised to comply.  Flying Eagle cents were struck beginning in April 1857 and were held pending official release.{{sfn|Snow|pp=17–19}}  The Mint stored the pieces pending accumulation of a sufficient supply; in mid-May, Snowden notified Philadelphia newspapers that distribution would begin on May 25.{{sfn|Julian}}

== Design ==
[[File:Peter the eagle.jpg|thumb|right|[[Peter (eagle)|Peter the eagle]]]]
Longacre's obverse of an eagle in flight is based on that of the Gobrecht dollar, struck in small quantities from 1836 to 1839.  Although Gobrecht's model is not known with certainty, some sources state that the bird in flight was based on [[Peter (eagle)|Peter the eagle]], a tame bird fed by Mint workers in the early 1830s until it was caught up in machinery and killed.  The bird was stuffed, and is still displayed at the Philadelphia Mint.{{sfn|Snow|pp=9–12, 224}}

Despite its derivative nature, Longacre's eagle has been widely admired.  According to art historian [[Cornelius Vermeule]] in his book on U.S. coins, the flying eagle motif, when used in the 1830s, was "the first numismatic bird that could be said to derive from nature rather than from colonial carving or heraldry".{{sfn|Vermeule|pp=40–41}}  Vermeule described the Flying Eagle cent's replacement, the [[Indian Head cent]], as "far less attractive to the eye than the Peale-Gobrecht flying eagle and its variants".{{sfn|Vermeule|p=55}}  Sculptor [[Augustus Saint-Gaudens]], when commissioned in 1905 to provide new designs for American coinage, sought to return a flying eagle design to the cent, writing to President [[Theodore Roosevelt]], "I am using a flying eagle, a modification of the device which was used on the cent of 1857.  I had not seen that coin for many years, and was so impressed by it that I thought if carried out with some modifications, nothing better could be done.  It is by all odds the best design on any American coin."{{sfn|Snow|p=10}}  Saint-Gaudens did return the flying eagle to American coinage, but his design was used for the reverse of the double eagle rather than the cent.{{sfn|Snow|p=10}}

The wreath on the reverse is also derivative, having been previously used on Longacre's Type II gold dollar of 1854, and the [[three-dollar piece]] of the same year.{{sfn|Vermeule|p=55}} It is composed of leaves of wheat, corn, cotton and tobacco, thus including produce associated with both the North and the South .  The cotton leaves are sometimes said to be maple leaves; the two types are not dissimilar, and maple leaves are more widely known than cotton leaves.  An ear of corn is also visible.{{sfn|Snow|pp=9–12, 224}}

== Release, production, and collecting ==
[[File:Harper's Magazine Feb 1857 cent.jpg|thumb|left|upright|From the February 1857 ''Harper's Magazine'': "[[Brother Jonathan]]'s New Baby", the Flying Eagle cent, as the neglected large cent wails.]]

The Philadelphia Mint released the new cents to the public on May 25, 1857.  In anticipation of large popular demand, Mint authorities built a temporary wooden structure in the courtyard of the Philadelphia facility.  On the morning of the date of release, hundreds of people queued, one line for those exchanging Spanish silver for cents, the other for those bringing in old copper cents and half cents.  From 9 am clerks paid out cents for the old pieces; outside the Mint precincts, early purchasers sold the new cents at a premium.  Snowden wrote to Guthrie, "the demand for them is enormous&nbsp;... we had on hand this morning $30,000 worth, that is three million pieces.  Nearly all of this amount will be paid out today."{{sfn|Taxay|pp=237–238}}  The 1856 specimen became publicly known about the time of issuance, and had the public checking their pocket change; 1856 small cents sold for as much as $2 by 1859.{{sfn|Snow|p=15}}  The public interest in the new cents set off a coin collecting boom:  in addition to seeking the rare 1856 cent, some tried to collect sets of large cents back to 1793, and found they would have to pay a premium for the rarer dates.{{sfn|Julian}}

The Mint had trouble striking the new design.  This was due to the hard copper-nickel alloy and the fact that the eagle on one side of the piece was directly opposite parts of the reverse design; efforts to bring out the design more fully led to increased die breakage.  Many Flying Eagle cents show weaknesses, especially at the eagle's head and tail, which are opposite the wreath.{{sfn|Lange|p=94}}{{sfn|Snow|pp=22, 72}}  In 1857, Snowden suggested the replacement of the eagle with a head of [[Christopher Columbus]].  Longacre replied that as there had been objections to proposals to place [[George Washington]] on the coinage, there would also be to a Columbus design.{{sfn|Snow|p=22}}  Despite the difficulties, the 17,450,000 Flying Eagle cents struck at Philadelphia in 1857 constituted the greatest production of a single coin in a year at a U.S. mint to that time.{{sfn|Guth & Garrett|p=22}}

[[File:1858 flying eagle pattern.jpg|thumb|right|Pattern coins with a smaller eagle were prepared in 1858; the bird was thought too scrawny.]]
In 1858, the Mint tried to alleviate the breakage problem using a new version of the cent with a shallower relief.  This attempt led to the major variety of the series, as coins of the revised version have smaller letters in the inscriptions than those struck earlier.  The two [[Glossary of numismatics#variety|varieties]] are about equally common, and were probably struck side by side for some period as the Mint used up older dies.  Efforts to conserve dies were the probable cause of another variety, the 1858/7, as 1857-dated dies were overstruck to allow them to be used in the new year.{{sfn|Snow|pp=64–70}}

The Mint prepared pattern coins with a much smaller eagle in 1858, which struck well, but which officials disliked.{{sfn|Snow|p=22}}  Snowden directed Longacre to prepare various patterns that he could select from for a new piece to replace the Flying Eagle cent as of January 1, 1859.  The Mint produced between 60 and 100 sets of twelve patterns showing various designs; these were circulated to officials and also were quietly sold by the Mint over the next several years.  Longacre's design showing Liberty wearing an Indian-style headress was adopted, with a wreath with lower relief for the reverse of the Indian Head cent, solving the metal flow issues.{{sfn|Breen|p=217}} On November 4, 1858, Snowden wrote to the Treasury Department, stating that the Flying Eagle cent had proved "not very acceptable to the general population" as they felt the bird was not true to life, and that the Native American design would "giv[e] it the character of America".{{sfn|Snow|p=23}}

[[File:Spanish American difficulty.jpg|thumb|left|upright|The February 1857 suggestion of ''Harper's Weekly'' that "The Spanish-American Difficulty" had been solved by making Spanish silver non-legal tender proved premature.]]
By September 1857, the volume of Spanish silver coming to the Mint had been so large that Snowden gave up the idea of being able to pay for it just with cents, authorizing payment with gold and silver coins.  On March 3, 1859, the redemption of the foreign pieces was extended for an additional two years.  As commerce was choked with the new cents, Congress repealed this provision in July 1860, though Snowden continued the practice for more than a year without authorization from Congress.  ''Bankers Magazine'' for October 1861 reported the end of the exchange, and quoted the Philadelphia ''Press'': "the large issue of the new nickel cents has rendered them almost as much of a nuisance as the old Spanish currency".{{sfn|Taxay|pp=238–239}}  According to Breen,  "the foreign silver coins had been legal tender, receivable for all kinds of payments including postage stamps and some taxes; the nickel cents were not.  They quickly filled shopkeepers' cashboxes to the exclusion of almost everything else; they began to be legally refused in trade."{{sfn|Breen|p=216}}  The glut was ended by the hoarding of all federal coinage in the wake of the economic upset caused by the [[American Civil War|Civil War]].{{sfn|Taxay|p=242}}{{sfn|Lange|pp=94–97}}

After the war, the hoarded Flying Eagle cents re-entered circulation.  Many remained there only a few years, being pulled out from among the new bronze cents in Treasury Department redemption programs in the 1860s and 1870s—thirteen million copper-nickel cents were retired by exchange for other base-metal coinage.  By the 1880s, it was a rarity in circulation.{{sfn|Snow|pp=57,64}}{{sfn|Carothers|p=207}} The 2015 edition of [[R.S. Yeoman]]'s ''[[A Guide Book of United States Coins]]'' lists the 1857, 1858 large letters, and 1858 small letters each at $28 in Good-4 condition, the lowest collectable grade.  The 1856 is $7,000 in that grade, rising to $22,000 in uncirculated MS-63.  The 1858/7 starts at $75 in G-4, rising to $10,000 in MS-63.{{sfn|Yeoman|p=113}}  An 1856 cent in MS-66 condition sold at auction in January 2004 for $172,500.{{sfn|Guth & Garrett|p=22}}

== References ==

'''Explanatory notes'''

{{notes}}

'''Citations'''

{{reflist|20em}}

'''Bibliography'''

* {{cite book
  | last = Breen
  | first = Walter
  | year = 1988
  | title = Walter Breen's Complete Encyclopedia of U.S. and Colonial Coins
  | publisher = Doubleday
  | location = New York
  | isbn = 978-0-385-14207-6
  | ref = {{sfnRef|Breen}}
  }}
* {{cite book
  | author = Bureau of the Mint
  | year = 1904
  | title = Laws of the United States Relating to the Coinage
  | publisher = United States Government Printing Office
  | location = Washington, D.C.
  | oclc = 8109299
  | url = https://books.google.com/books?id=rmtAAAAAYAAJ
  | ref = {{sfnRef|Bureau of the Mint}}
  }}
* {{cite book
  | last = Carothers
  | first = Neil
  | year = 1930
  | title = Fractional Money: A History of Small Coins and Fractional Paper Currency of the United States
  | publisher = John Wiley & Sons, Inc. (reprinted 1988 by Bowers and Merena Galleries, Inc., Wolfeboro, N.H.)
  | location = New York
  | isbn = 0-943161-12-6
  | ref = {{sfnRef|Carothers}}
  }}
* {{cite book
  | last1 = Guth
  | first1 = Ron
  | last2 = Garrett
  | first2 = Jeff
  | year = 2005
  | title = United States Coinage: A Study by Type
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-1782-4
  | ref = {{sfnRef|Guth & Garrett}}
  }}
* {{cite journal
  | last = Julian
  | first = R.W.
  | date = August 13, 2007
  | title = Flying Eagle premiered copper-nickel alloy
  | journal = Numismatic News
  | publisher = Krause Publications
  | location = Iola, Wis.
  | url = http://www.numismaster.com/ta/numis/Article.jsp?ad=article&ArticleId=2690
  | accessdate = February 2, 2013
  | ref = {{sfnRef|Julian}}
  }}
* {{cite book
  | last = Lange
  | first = David W.
  | year = 2006
  | title = History of the United States Mint and its Coinage
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-1972-9
  | ref = {{sfnRef|Lange}}
  }}
* {{cite book
  | last = Snow
  | first = Richard
  | year = 2009
  | title = A Guide Book of Flying Eagle and Indian Head Cents
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-2831-8
  | ref = {{sfnRef|Snow}}
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | title = The U.S. Mint and Coinage
  | publisher = Sanford J. Durst Numismatic Publications
  | location = New York
  | year = 1983
  | edition = reprint of 1966
  | isbn = 978-0-915262-68-7
  | ref = {{sfnRef|Taxay}}
  }}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | year = 1971
  | authorlink = Cornelius Vermeule
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, Mass.
  | isbn = 978-0-674-62840-3
  | ref = {{sfnRef|Vermeule}}
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | year = 2014
  | authorlink = Richard S. Yeoman
  | title = [[A Guide Book of United States Coins]] (The Official Red Book)
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | edition = 68th
  | isbn = 978-0-7948-4215-4
  | ref = {{sfnRef|Yeoman}}
  }}

== External links ==
* [http://www.coinpage.com/flying%20eagle-pictures.html Flying Eagle Cent Pictures]
* [http://flyingeaglecent.com/ Flying Eagle Cent Coin Guide]

{{s-start}}
{{s-bef | before = [[Braided Hair cent]] }}
{{s-ttl | title  = [[Penny (United States coin)|United States one-cent coin]] | years = 1856–1858 }}
{{s-aft | after  = [[Indian Head cent]] }}
{{end}}

{{Coinage (United States)}}

[[Category:1856 introductions]]
[[Category:One-cent coins of the United States]]