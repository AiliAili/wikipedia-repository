__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=R.V
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Bomber]]
 | national origin=Germany
 | manufacturer=[[Siemens-Schuckert]]
 | designer=[[Bruno and Franz Steffen]]<ref name="gt572">Grey & Thetford 1962, p.572</ref>
 | first flight=c. June 1916<ref name="hg189">Haddow & Grosz 1963, p.189</ref>
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1<ref name="hg1889">Haddow & Grosz 1963, p.188–89</ref>
 | developed from= [[Siemens-Schuckert R.I]]<ref name="hg180">Haddow & Grosz 1963, p.180</ref>
 | variants with their own articles=
}}
|}

The '''Siemens-Schuckert R.V''' was a bomber aircraft built in Germany during World War I.<ref name="jea">Taylor 1989, p.808</ref><ref name="iea">''The Illustrated Encyclopedia of Aircraft'', p.2920</ref> It was one of six aircraft based on the [[Siemens-Schuckert R.I]] that were originally intended to be identical, but which each developed in a different direction and were designated as different aircraft types by the German Inspectorate of Flying Troops (the [[Idflieg]]).<ref name="hg184">Haddow & Grosz 1963, p.184</ref> Development of the R.V benefited from the experience that Siemens-Schuckert and the ''Idflieg'' had gained with the [[Siemens-Schuckert R.II|R.II]], [[Siemens-Schuckert R.III|R.III]], and [[Siemens-Schuckert R.IV|R.IV]], particularly in its choice of powerplants, where the R.V was spared from the troublesome [[Maybach HS]] engine.<ref name="hg188">Haddow & Grosz 1963, p.188</ref> Between September 1916 and February 1917, the aircraft saw service on the [[Eastern Front (World War I)|Eastern Front]] before it was damaged in an accident and dismantled for spare parts.<ref name="hg1889"/>

==Design and development==
As designed, the R.V was a large three-bay biplane with unstaggered wings of unequal span and a fully enclosed cabin.<ref name="hg188"/> Three 180-kW (240-hp) Maybach HS engines were mounted internally in the fuselage, and transmitted their power via driveshafts to two propellers mounted tractor-fashion on the interplane struts nearest the fuselage.<ref name="hg174">Haddow & Grosz 1963, p.174</ref> The main undercarriage consisted of divided units, each of which carried dual wheels, and the tail was supported by a pair of tailwheels.<ref name="hg188"/> The fuselage was forked into an upper and lower section, which allowed a clear field of fire to the rear of the aircraft.<ref name="gt572"/> The Maybach engines had been a never-ending source of trouble on the R.II, R.III, and R.IV, and by June 1916, Siemens-Schuckert had obtained permission from the ''Idflieg'' to substitute Benz Bz.IV engines on the R.III<ref>Haddow & Grosz 1963, p.186–87</ref> The firm made the same change on the R.V, which was almost complete by then.<ref name="hg1889"/> The substitution required an almost complete rebuild of the aircraft, and included adding an extra bay to the wings, increasing their span.<ref name="hg189"/> The ''Idflieg'' accepted the R.V in this form, after agreeing to a reduction in the aircraft's original specifications.<ref name="hg189"/>

==Operational history==
Siemens-Schuckert delivered the R.V to the ''[[Riesenflugzeugersatzabteilung]]'' (Rea — "giant aircraft support unit") at [[Döberitz]] on 13 August 1916.<ref name="hg189"/> From there, it was assigned to ''[[Riesenflugzeugabteilung 501]]'' (Rfa 501), and joined the squadron at [[Vilna]] on 3 September.<ref name="hg189"/> It was used operationally until the week of 14 February 1917, when the aircraft was severely damaged during a hard landing at night that fractured its fuselage.<ref name="hg189"/> The R.V was then dismantled and sent back to Döberitz where it could be used for spare parts for other Siemens-Schuckert bombers.<ref name="hg189"/>

Specific details of several operational missions while with Rfa 501 have survived:<ref name="hg56">Haddow & Grosz 1963, p.56</ref> 
* 14–15 October 1916 — railway station at [[Wileyka]]
* 26 November 1916 — troop camp at [[Iža|Iza]]
* 7 January 1917 — railway station at [[Poloczany]]

Additionally, the R.V carried out the following raids together with the [[Siemens-Schuckert R.VI|R.VI]]:<ref name="hg56"/>
* 19 January 1917 — troop camp at Iza
* 30 January 1917 — railway station at Wileyka
* 8 February 1917 — railway station at [[Molodeczne]]
* 12 February 1917 — railway station at [[Zalesie, Silesian Voivodeship|Zalesie]]

<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{aerospecs
|ref=<!-- reference -->Kroschel & Stützer 1994, p.141
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=4
|capacity=
|length m=17.7
|length ft=58
|length in=1
|span m=34.33
|span ft=112
|span in=8
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=4.6
|height ft=15
|height in=8
|wing area sqm=177
|wing area sqft=1,910
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=5,300
|empty weight lb=11,600
|gross weight kg=6,766
|gross weight lb=14,885
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=3
|eng1 type=[[Benz Bz.IV]]
|eng1 kw=<!-- prop engines -->150
|eng1 hp=<!-- prop engines -->200
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=132
|max speed mph=83
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=480
|range miles=300
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=3,000
|ceiling ft=9,800
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=3 × 7.9-mm machine guns
|armament2=500&nbsp;kg of bombs
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Siemens-Schuckert aircraft}}
* {{cite book| last=Gray |first=Peter |author2=Owen Thetford  |title=German Aircraft of the First World War |publisher=Putnam |location=London |year=1962}}
* {{cite book| last=Haddow |first=G.W. |author2=Peter M. Grosz  |title=The German Giants: The Story of the R-planes 1914–1919 |publisher=Putnam |location=London |year=1962}}
* {{cite book |title=The Illustrated Encyclopedia of Aircraft |publisher=Aerospace Publishing|location=London |pages= }}
* {{cite book| last=Kroschel |first=Günter |author2=Helmut Stützer  |title=Die Deutschen Militärflugzeuge 1910–1918 |publisher=Mittler |location=Herford |year=1994}}
* {{cite book| last=Taylor |first=Michael J.H. |title=Jane's Encyclopedia of Aviation |publisher=Studio Editions |location=London |year=1989}}
<!-- ==External links== -->

{{Siemens-Schuckert aircraft}}
{{Idflieg R-class designations}}

[[Category:German bomber aircraft 1910–1919]]
[[Category:Siemens-Schuckert aircraft|R.V]]
[[Category:Three-engined twin-prop tractor aircraft]]
[[Category:Biplanes]]