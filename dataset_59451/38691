{{Infobox ice hockey player
| image = 
| image_size = 
| position = [[Winger (ice hockey)|Left Wing]]
| played_for =[[Phoenix Roadrunners (IHL)|Phoenix Roadrunners]]<br>[[South Carolina Stingrays]]<br />[[Chicago Cheetahs]]<br>[[Chicago Wolves]]
| shoots = Left
| height_ft = 6
| height_in = 0
| weight_lb = 180
| birth_date = {{birth date|1967|12|8|mf=y}}
| birth_place = [[Downers Grove, Illinois|Downers Grove]], [[Illinois|IL]], [[United States|USA]]
| death_date = {{death date and age|2005|2|9|1967|12|8}}
| death_place = [[St. Charles, Illinois|St. Charles]], [[Illinois|IL]], [[United States|USA]]
| draft = Undrafted
| career_start = 1991
| career_end = 1999
}}
'''Timothy G. Breslin'''<ref>{{cite web|url=http://www.legacy.com/obituaries/chicagotribune/obituary.aspx?n=timothy-g-breslin&pid=3159482&fhid=2168#fbLoggedOut|title=Timothy G. Breslin Obituary|work=[[Chicago Tribune]]|date=2005-02-13 |accessdate=2012-12-27}}</ref> (December 8, 1967 – February 9, 2005) was a professional [[ice hockey]] [[Winger (ice hockey)|left wing]]. Breslin played eight seasons in the [[International Hockey League (1945–2001)|International Hockey League]] (IHL) with the [[Phoenix Roadrunners (IHL)|Phoenix Roadrunners]] and [[Chicago Wolves]] and part of a season in the [[ECHL]] with the [[South Carolina Stingrays]]. He also played [[Major professional sports leagues in the United States and Canada|major league]] roller hockey in [[Roller Hockey International]] (RHI) with the [[Chicago Cheetahs]].

Breslin attended [[Lake Superior State University]]. While a freshman he helped the [[Lake Superior State Lakers men's ice hockey|Lakers]] win the school's first national championship in 1988. He served as an [[alternate captain]] in his senior season while also tying two school records, points in a game (7) and points in a series (10). Undrafted out of college, he signed with the [[Los Angeles Kings]] as a free agent. He spent four years in their minor league system playing for Phoenix and South Carolina. After a brief stint in the RHI, he joined the Wolves as a free agent. As a member of the Wolves, Breslin was highly involved in charitable activities which led to him winning [[IHL Man of the Year]] honors in the [[1996–97 AHL season|1996–97 season]]. He was a member of Chicago's [[Turner Cup]] champion team the following year.

Late in 2004 Breslin was diagnosed with cancer and died 11 weeks later on February 9, 2005 due to complications from [[appendiceal cancer]]. To honor him the Wolves created the [[Tim Breslin Unsung Hero Award]] and the Tim Breslin Memorial Scholarship. As a way of helping his family financially, they hosted an exhibition game dubbed the Breslin Cup.

== Early life ==
Tim was born in [[Downers Grove, Illinois]] on December 8, 1967 to James and Kathleen Breslin.<ref>{{cite web|url=http://www.nhl.com/ice/player.htm?id=8458861|title=Tim Breslin player card|publisher=[[National Hockey League]]|accessdate=2012-12-27}}</ref><ref name=tb37/> He was one of six children, having three sisters and two brothers. Breslin grew up in [[Addison, Illinois]] where he began playing hockey at age five.<ref name=tb37>{{cite web|title=Tim Breslin, 37, Chicago Wolves hockey player|url=http://www.highbeam.com/doc/1P2-1562646.html|publisher=''[[Chicago Sun-Times]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-12-27|date=2005-02-17|last=Thomas|first=Monifa}}</ref> He learned the game while playing with his brothers on a frozen pond near the family home and on a backyard rink his father made.<ref name=wsj/> As he got older Breslin played in local leagues and [[Driscoll Catholic High School]]'s club team, which afforded him the opportunity to join the [[Dubuque Fighting Saints (1980–2001)|Dubuque Fighting Saints]] in the [[United States Hockey League]].<ref name=tb37/><ref name=icep2>{{cite web|title=Ice Guy: The Chicago Wolves' Tim Breslin Gets High Marks For Serving The Community - page 2|url=http://articles.chicagotribune.com/1997-12-21/features/9712210359_1_chicago-wolves-tim-breslin-reading-program/2|work=[[Chicago Tribune]]|accessdate=2012-12-27|page=2|date=1997-12-21|last=Mandernach|first=Mark}}</ref>

== Playing career ==

=== Amateur ===
Breslin joined the Fighting Saints for his high school senior year and played an additional season after graduation.<ref name=icep2/> His performance in Dubuque helped earn a scholarship to [[Lake Superior State University]] (LSSU).<ref name=home>{{cite web|title=No Place Like Home for Wolves' Breslin|url=http://www.highbeam.com/doc/1P2-4313175.html|publisher=''[[Chicago Sun-Times]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-12-27|date=1995-12-10|last=Ziehm|first=Len}}</ref> In his freshman season with the [[Lake Superior State Lakers men's ice hockey|Lakers]], Breslin contributed 6 goals and 20 points, as LSSU finished first in the [[Central Collegiate Hockey Association]] (CCHA).<ref name=stats>{{cite web|url=http://www.hockeydb.com/ihdb/stats/pdisplay.php?pid=562|title=Tim Breslin Career Statistics|publisher=Internet Hockey Database|accessdate=2012-12-28}}</ref><ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/leagues/seasons/ccha19721988.html|title=1987–88 NCAA - Central Collegiate Hockey Assn. - Div. 1 Standings|publisher=Internet Hockey Database|accessdate=2012-12-28}}</ref> Advancing to the [[1988 NCAA Division I Men's Ice Hockey Tournament]], the Lakers defeated [[Merrimack Warriors men's ice hockey|Merrimack College]] and [[Maine Black Bears men's ice hockey|University of Maine]] to reach the championship game.<ref>{{cite web|url=http://insidecollegehockey.com/6History/ncaa_88.htm|title=1988 NCAA Tournament|publisher=Inside College Hockey, Inc.|accessdate=2012-12-28}}</ref> In the title game LSSU defeated [[St. Lawrence Saints men's ice hockey|St. Lawrence]] to earn its first National Championship.<ref>{{cite web|title=Lake Superior St. Wins NCAA|url=http://www.highbeam.com/doc/1P2-1249068.html|publisher=''[[The Washington Post]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-12-28|date=1988-04-03}}</ref> For the 1988–89 season Breslin improved to 7 goals while remaining at 20 points.<ref name=stats/> LSSU finished second in the CCHA and in the [[1989 NCAA Division I Men's Ice Hockey Tournament|1989 NCAA Tournament]] they lost to eventual national champion  [[Harvard Crimson men's ice hockey|Harvard]] in the quarterfinals.<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/leagues/seasons/ccha19721989.html|title=1988–89 NCAA - Central Collegiate Hockey Assn. - Div. 1 Standings|publisher=Internet Hockey Database|accessdate=2012-12-28}}</ref><ref>{{cite web|url=http://insidecollegehockey.com/6History/ncaa_89.htm|title=1989 NCAA Tournament|publisher=Inside College Hockey, Inc.|accessdate=2012-12-28}}</ref> In his junior season Breslin continued to produce at about the same pace, registering 8 goals and 25 points.<ref name=stats/> The Lakers again finished second in the CCHA, and in the [[1990 NCAA Division I Men's Ice Hockey Tournament|1990 NCAA Tournament]] they advanced to the quarterfinals for the second straight year where they were defeated by national champion runner-up [[Colgate Raiders men's ice hockey|Colgate]].<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/leagues/seasons/ccha19721990.html|title=1989–90 NCAA - Central Collegiate Hockey Assn. - Div. 1 Standings|publisher=Internet Hockey Database|accessdate=2012-12-28}}</ref><ref>{{cite web|url=http://insidecollegehockey.com/6History/ncaa_90.htm|title=1990 NCAA Tournament|publisher=Inside College Hockey, Inc.|accessdate=2012-12-28}}</ref>

In his senior season Breslin was named one of the team's [[alternate captain]]s.<ref name=cbs>{{cite web|url=http://www.cstv.com/sports/m-hockey/stories/021005aak.html|title=Lakers Grieve Tim Breslin's Passing|publisher=[[CBS Sports Network]]|accessdate=2012-12-28|date=2005-02-10}}</ref> Playing on a line with future [[National Hockey League]] (NHL) player [[Doug Weight]], Breslin had a break-out season.<ref>{{cite web|title=More Than the Score; Cancer claims life of former Fighting Saints forward|url=http://www.highbeam.com/doc/1P2-11158405.html|publisher=''[[Telegraph Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-12-27|date=2005-02-15|last=Leitner|first=Jim}}</ref> He set career highs in goals (25), assists (37), and points (62).<ref name=stats/> During the year he tied a school record for points in a game with a seven-point performance against [[Ohio State Buckeyes men's ice hockey|Ohio State]]. The scoring output helped him tie another school record when the two teams played the following day. Adding another three points in the second game, Breslin brought his two-game total to 10, tying the record for points in a series.<ref>{{cite web|url=http://lssulakers.cstv.com/sports/m-hockey/individual.html|title=Lakers individual records|publisher=[[Lake Superior State University]]|accessdate=2012-12-28}}</ref> Lake Superior State finished first in the CCHA, but was again defeated in the quarterfinals of the [[1991 NCAA Division I Men's Ice Hockey Tournament|1991 NCAA Tournament]].<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/leagues/seasons/ccha19721991.html|title=1990–91 NCAA - Central Collegiate Hockey Assn. - Div. 1 Standings|publisher=Internet Hockey Database|accessdate=2012-12-28}}</ref><ref>{{cite web|url=http://insidecollegehockey.com/6History/ncaa_91.htm|title=1991 NCAA Tournament|publisher=Inside College Hockey, Inc.|accessdate=2012-12-29}}</ref>

=== Professional ===
Undrafted out of college, Breslin signed with the [[Los Angeles Kings]] as a free agent.<ref name=home/> He attended Kings training camp, where he played with his professional hockey hero, [[Wayne Gretzky]].<ref>{{cite web|title=Hockey player puts focus on goals Wolves star stresses importance of hard work, education.|url=http://www.highbeam.com/doc/1G1-68897185.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-12-28|date=1998-09-19|last= Gutowski|first=Christy}}</ref> Breslin failed to make the team and Los Angeles assigned him to their [[International Hockey League (1945–2001)|International Hockey League]] (IHL) affiliate the [[Phoenix Roadrunners (IHL)|Phoenix Roadrunners]].<ref name=wsj/><ref name=home/> In his [[1991–92 IHL season|first season]] with Phoenix, Breslin broke his arm early in the season. He returned to play just three weeks after the injury, but eventually re-broke the arm and missed the remainder of the season.<ref name=icep2/> He finished his first professional season playing in 45 games, scoring 8 goals and 29 points.<ref name=stats/> In the [[1992–93 IHL season|1992–93 season]] Breslin improved his production to 14 goals and a career-high 44 points.<ref name=stats/> Phoenix finished the season with a league-low 26 wins and 58 points.<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/leagues/seasons/ihl19461993.html|title=1992–93 International Hockey League Standings|publisher=Internet Hockey Database|accessdate=2012-12-30}}</ref> He began his [[1993–94 IHL season|third professional season]] with the Roadrunners, but after five games he was reassigned to the [[ECHL]]'s [[South Carolina Stingrays]]. Breslin played nine games for the Stingrays, registering six points, before being recalled by Phoenix.<ref>{{cite news|url=
https://news.google.com/newspapers?id=Gj1SAAAAIBAJ&sjid=_jYNAAAAIBAJ&pg=4563,2513468&dq=tim+breslin+stingrays&hl=en|title=Stingrays Face Tough Road Test|publisher=[[The Post and Courier]]|accessdate=2012-12-30|date=1993-12-07|last=Namm|first=Keith}}</ref> Finishing the year with the Roadrunners, he registered 9 goals and 27 points in 50 games.<ref name=stats/> Phoenix improved to 85 points but failed to make the playoffs for the third straight year.<ref>{{cite web|url=http://www.hockeydb.com/stte/phoenix-roadrunners-7457.html|title=Phoenix Roadrunners Statistics and History|publisher=Internet Hockey Database|accessdate=2012-12-30}}</ref> At the conclusion of the season, Breslin's contract with the Kings expired.<ref name=icep2/>

In the summer Breslin joined the [[Chicago Cheetahs]] of the [[Roller Hockey International]] (RHI) league for the [[1994 RHI season|1994 season]]. Playing in just 6 games, Breslin recorded 5 goals and 13 points.<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/leagues/seasons/teams/0017601994.html |title=1993–94 Chicago Cheetahs roster and statistics|publisher=Internet Hockey Database|accessdate=2012-12-30}}</ref> Prior to the start of the [[1994–95 IHL season]], Breslin signed a one-year contract with the [[Chicago Wolves]]. He was one of the first three players signed by the Wolves who began their first season as an IHL [[expansion team]].<ref name=home/><ref name=award>{{cite web|url=http://www.chicagowolves.com/community/scholarships-a-awards/tim-breslin|title=Tim Breslin Scholarship and Awards |publisher=[[Chicago Wolves]]|accessdate=2012-12-26}}</ref><ref>{{cite web|title=Upstart Wolves Skate a Fine Line as Second Season Starts|url=http://www.highbeam.com/doc/1P2-4304520.html|publisher=''[[Chicago Sun-Times]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-07-27|date=1995-10-06|last=Smith|first=William}}</ref> In his first season with the Wolves, Breslin notched 7 goals and 28 points.<ref name=stats/> Chicago finished with 80 points—good enough for third in their division and to qualify for the playoffs.<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/leagues/seasons/ihl19461995.html|title=1994–95 International Hockey League Standings|publisher=Internet Hockey Database|accessdate=2012-12-30}}</ref> Facing the [[Kalamazoo Wings (1974–2000)|Kalamazoo Wings]] in the first round, Chicago was swept in three straight games.<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/playoffdisplay.php?league=ihl1946&season=1995&leaguenm=IHL|title=1994–95 IHL Playoff Results|publisher=Internet Hockey Database|accessdate=2012-12-30}}</ref> Playing in his first professional playoffs, Breslin contributed two points playing in all three games.<ref name=stats/> In the off-season Breslin negotiated his own contract to stay in Chicago, signing a two-year deal.<ref name=home/> Over the next two seasons, Breslin averaged 56 games played a season due to knee and shoulder injuries.<ref name=stats/><ref>{{cite web|title=Breslin, Wolves finally have some fun.(Sports)|url=http://www.highbeam.com/doc/1G1-69111524.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1997-03-09|last=  Miles|first=Bruce}}</ref><ref>{{cite web|title=Breslin on the board in return to action.(Sports)|url=http://www.highbeam.com/doc/1G1-69141010.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1996-10-19|last=Miles|first=Bruce}}</ref> As a team the Wolves qualified for the playoffs in both seasons but failed to advance beyond the second round.<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/playoffdisplay.php?league=ihl1946&season=1996&leaguenm=IHL|title=1995–96 IHL Playoff Results|publisher=Internet Hockey Database|accessdate=2013-01-03}}</ref><ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/playoffdisplay.php?league=ihl1946&season=1997&leaguenm=IHL|title=1996–97 IHL Playoff Results|publisher=Internet Hockey Database|accessdate=2013-01-03}}</ref> At the conclusion of the [[1996–97 IHL season|1996–97 season]], Breslin was awarded the I. John Snider trophy as [[IHL Man of the Year]], an award given to recognize outstanding community service.<ref>{{cite web|title=Wolves' Breslin to receive IHL honor.(Sports)|url=http://www.highbeam.com/doc/1G1-69045145.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1997-05-30|last=Miles|first=Bruce}}</ref>

In the off-season Breslin again re-signed with the Wolves.<ref name=reunited>{{cite web|title=Wolves re-sign Breslin; reunited with Nardella.(Sports)|url=http://www.highbeam.com/doc/1G1-69082298.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1997-08-05|last=Miles|first=Bruce}}</ref> Chicago also brought in [[John Anderson (ice hockey)|John Anderson]] to be the team's new head coach.<ref>{{cite web|title=Wolves go a new direction with Anderson, Cheveldayoff.|url=http://www.highbeam.com/doc/1G1-69078561.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1997-08-05|last= Miles|first=Bruce}}</ref> Under Anderson, Breslin had posted a career high in assists with 26 and games played with 81.<ref name=stats/> Chicago won the West division and finished with the second-best record in the league.<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/leagues/seasons/ihl19461998.html|title=1997–98 International Hockey League Standings|publisher=Internet Hockey Database|accessdate=2013-01-03}}</ref> In the postseason the Wolves stormed through the first three rounds, losing only four games, to advance to the [[1997–98 IHL season#Turner Cup-Playoffs|1998 Turner Cup Finals]].<ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/playoffdisplay.php?league=ihl1946&season=1998&leaguenm=IHL|title=1997–98 IHL Playoff Results|publisher=Internet Hockey Database|accessdate=2013-01-03}}</ref> In game 1 of the finals against the [[Detroit Vipers]], Chicago blew a two-goal lead early in the third period. They regained the lead with just over eight minutes to play. In the final minutes of the game, Breslin added an insurance [[empty net goal]] giving the Wolves a 4–2 victory.<ref>{{cite web|title=Wolves handle adversity, take opener.(Sports)|url=http://www.highbeam.com/doc/1G1-68952962.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1998-05-31|last=Sassone|first=Tim}}</ref> It was Breslin's only goal of the playoffs.<ref name=stats/> The series eventually went to a seventh and deciding game that the Wolves won 3–0 to capture the franchise's first [[Turner Cup]].<ref>{{cite web|title=Brawl heightens Game 7 tension.(Sports)|url=http://www.highbeam.com/doc/1G1-68956681.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1998-06-16|last= Pitts|first=Brian}}</ref> For the [[1998–99 IHL season|1998–99 season]] Breslin's production dipped to 7 goals and 21 points.<ref name=stats/> The Wolves advanced to the third round the playoffs, but Breslin played in only 4 of the team's 10 games.<ref name=stats/><ref>{{cite web|url=http://www.hockeydb.com/ihdb/stats/playoffdisplay.php?league=ihl1946&season=1999&leaguenm=IHL|title=1998–99 IHL Playoff Results|publisher=Internet Hockey Database|accessdate=2013-01-03}}</ref> On the first day of training camp the following year, Breslin announced his retirement from professional hockey.<ref>{{cite web|title=Wolves veteran wing Breslin retires|url=http://www.highbeam.com/doc/1P2-4508966.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1999-08-15|last=Ziehm|first=Len}}</ref>

== Playing style ==
Listed as a [[Winger (ice hockey)|left wing]], Breslin was capable of playing all three [[Forward (ice hockey)|forward]] positions, an ability which gave his coaches flexibility and allowed him to play in any situation.<ref name=stats/><ref name=reunited/> Not a big player, standing six feet tall weighing 180 pounds, he played a gritty, physical, brand of hockey primary in a checking [[Line (ice hockey)|line]] role.<ref name=stats/><ref name=twice/> As a checker it was his responsibility to shut down the opposition and keep them from scoring, emphasizing defense over offense.<ref name=twice>{{cite web|title=Breslin scores twice as Wolves win seventh straight.(Sports)|url=http://www.highbeam.com/doc/1G1-68889879.html|publisher=''[[Daily Herald (Arlington Heights)|Daily Herald]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2013-01-03|date=1998-11-02|last= Pitts|first=Brian}}</ref><ref>{{cite web|title=Hawks rely on shutdown unit to stifle Sharks|url=http://www.icehogs.com/news/icehogs/index.html?article_id=2552&content_type=printable&plugin_id=news.front.system&block_id=5001|accessdate=2013-01-03|date=2010-05-15|publisher=[[Chicago Blackhawks]]}}</ref> Because of his versatility and role, he was often a member of his team's [[penalty kill]]ing unit.<ref name=twice/> Breslin was known for his team-first attitude and willingness to do whatever was asked of him in order to help the team succeed.<ref name=award/> Former Wolves [[General Manager]] [[Kevin Cheveldayoff]] said of Breslin, "You could always count on Tim to come and compete every night and do what was needed for the team to win".<ref name=tb37/> Not the most talented of players, Breslin's work ethic helped him to be a better player. Head coach Anderson once stated, "He is the kind of player who maybe isn't in the very upper echelon in skill factor, but his dedication and hard work make up for that."<ref>{{cite web|url=http://www.chicagowolves.com/features/438-wolves-to-present-fahey-with-unsung-hero-award|title=Wolves to Present Fahey with Unsung Hero Award|publisher=[[Chicago Wolves]]|accessdate=2012-12-26}}</ref>

== Personal ==
Breslin married Jami Rutili, and the couple had three children, Shane, Paige, and Chase.<ref name=wsj/> He earned a degree in recreation management while at Lake Superior State.<ref name=home/> During his career, Breslin was involved in numerous charities. He delivered food on [[Thanksgiving (United States)|Thanksgiving]] for the [[Chris Zorich]] Foundation; conducted [[self-esteem]] workshops for an anti-drug, anti-gang organization; participated in the Wolves' Read to Succeed program, in which he read to children at local libraries; and several others. In a 1997 interview, Breslin said that the most special charity he helped was one started by him and his wife. The Extra Effort program was started at Indian Trail Junior High School in Addison, where Jami was a teacher. Each month the program gave both a male and a female student four tickets to a Wolves game and a gift bag. The students were chosen based on attitude, effort and attendance.<ref name=icep1>{{cite web|title=Ice Guy: The Chicago Wolves' Tim Breslin Gets High Marks For Serving The Community - page 1|url=http://articles.chicagotribune.com/1997-12-21/features/9712210359_1_chicago-wolves-tim-breslin-reading-program|work=[[Chicago Tribune]]|accessdate=2012-12-27|page=1|date=1997-12-21|last=Mandernach|first=Mark}}</ref> After retiring from professional hockey, he coached youth hockey and helped manage an Irish pub.<ref name=wsj>{{cite web|title= A Different Kind Of Star Athlete: In a year of scandal, a reminder of why we root |url=https://online.wsj.com/article/SB113416182227218695.html|work=[[The Wall Street Journal]]|accessdate=2012-12-27|date=2005-12-10|last=Gruley|first=Bryan}}</ref>

==Death and legacy==
[[File:Breslin Banner.JPG|thumb|180px|[[Chicago Wolves]] banner honoring Breslin]]
After complaining of stomach pains for several days, Breslin was taken to a hospital on Thanksgiving of 2004. There he was diagnosed with [[appendiceal cancer]] and underwent surgery. He died eleven weeks later on February 9, 2005 due to complications from the cancer.<ref name=wsj/><ref>{{cite web|title=Keeping Life in Check|url=http://www.kanecountymagazine.com/articles/2011/12/30/Cover%20Keeping%20Life%20In%20Check/index.xml#.UN3QdaxKIU4|publisher=Kane County Magazine|accessdate=2012-12-27|date=2011-12-30|last=Demitropoulos |first=Betsy}}</ref> The Wolves honored Breslin by wearing a helmet sticker shaped like a [[shamrock]] with his initials inside of it for the [[2004–05 AHL season|2004–05 season]].<ref name=cup/> As a way of helping his family financially, the Wolves and [[Chicago Blackhawks]] put on a charity game called the "Breslin Cup". The game featured 40 players, most of whom were former players from the two franchises. All the proceeds from the game went to a trust set up for the family.<ref name=cup>{{cite web|title=Wolves announce plans for Breslin Cup|url=http://theahl.com/wolves-announce-plans-for-breslin-cup-p136465|publisher=[[American Hockey League]]|accessdate=2012-12-26|date=2005-05-05}}</ref><ref>{{cite web|title=Breslin Cup to benefit family of late player|url=http://www.highbeam.com/doc/1P2-1576336.html|publisher=''[[Chicago Sun-Times]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-12-26|date=2005-06-01|last=Ziehm|first=Len}}</ref> The game drew around 10,000 fans, with the Wolves' alumni defeating the Blackhawks' alums in a [[Shootout (ice hockey)|shootout]].<ref>{{cite web|title=Former Hawks, Wolves play benefit to honor cancer victim Tim Breslin|url=http://www.highbeam.com/doc/1P2-1577718.html|publisher=''[[Chicago Sun-Times]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-12-26|date=2005-06-11|last=Jones|first=Jennifer}}</ref> The Breslin Cup and associated events raised over $250,000 for the family.<ref name=wsj/>

The [[2005–06 AHL season|following season]] the Wolves further honored Breslin with an on-ice ceremony that concluded with the raising of a banner of his name.<ref>{{cite web|title=Knights' big finish buries Wolves|url=http://www.highbeam.com/doc/1P2-1603511.html|publisher=''[[Chicago Sun-Times]]'' {{Subscription required|via=[[HighBeam Research]]}}|accessdate=2012-12-26|date=2005-12-04}}</ref> They also created a team award called the [[Tim Breslin Unsung Hero Award]], to be given annually to a player who "best typifies Breslin's on-ice spirit and team-first attitude", and a college scholarship, the Tim Breslin Memorial Scholarship, in his honor.<ref name=award/> A winner is chosen each season from students who fill out an entry form and write a corresponding essay. The scholarship pays for one semester of college.<ref>{{cite web|url=http://chicagowolves.org/images/community/Educational/1213-TimBreslinScholarship.pdf|title=Tim Breslin Memorial scholarship: Presented by the Chicago Wolves Professional Hockey Team|publisher=[[Chicago Wolves]]|accessdate=2012-12-26}}</ref> Breslin was inducted into the Illinois Hockey Hall of Fame in 2013.<ref>{{cite web|url=http://www.chicagowolves.com/news/releases/item/3268-levin-nardella-breslin-enshrined-by-illinois-hockey-hall-of-fame|title=Levin, Nardella, Breslin Enshrined By Illinois Hockey Hall Of Fame |publisher=[[Chicago Wolves]]|date=2012-09-21|accessdate=2013-01-28}}</ref>

== Career statistics ==
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:70em"
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan="3" bgcolor="#ffffff" | &nbsp;
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5" | [[Regular season|Regular&nbsp;season]]
! rowspan="99" bgcolor="#ffffff" | &nbsp;
! colspan="5" | [[Playoffs]]
|- ALIGN="center" bgcolor="#e0e0e0"
! [[Season (sports)|Season]]
! Team
! League
! GP
! [[Goal (ice hockey)|G]]
! [[Assist (ice hockey)|A]]
! [[Point (ice hockey)|Pts]]
! [[Penalty (ice hockey)|PIM]]
! GP
! G
! A
! Pts
! PIM
|-
| 1987–88
| [[Lake Superior State Lakers men's ice hockey|Lake Superior State University]]
| [[Central Collegiate Hockey Association|CCHA]]
| 38||6||14||20||18
| —||—||—||—||—
|- bgcolor="#f0f0f0"
| 1988–89
| Lake Superior State University
| CCHA
| 42||7||13||20||34
| —||—||—||—||—
|-
| 1989–90
| Lake Superior State University
| CCHA 
| 46||8||17||25||20
| —||—||—||—||—
|- bgcolor="#f0f0f0"
| 1989–90
| Lake Superior State University
| CCHA
| 45||25||37||62||26
| —||—||—||—||—
|-	
| [[1991–92 IHL season|1991–92]]
| [[Phoenix Roadrunners (IHL)|Phoenix Roadrunners]]
| [[International Hockey League (1945–2001)|IHL]]
| 45||8||21||29||12
| —||—||—||—||—
|- bgcolor="#f0f0f0"
| [[1992–93 IHL season|1992–93]]
| Phoenix Roadrunners
| IHL
| 79||14||30||44||55
| —||—||—||—||—
|-
| [[1993–94 IHL season|1993–94]]
| Phoenix Roadrunners
| IHL
| 50||9||18||27||29
| —||—||—||—||—
|- bgcolor="#f0f0f0"
| [[1993–94 IHL season|1993–94]]
| [[South Carolina Stingrays]]
| [[ECHL]]
| 9||3||3||6||4
| —||—||—||—||—
|-
| [[1994 RHI season|1994]]
| [[Chicago Cheetahs]]
| [[Roller Hockey International|RHI]]
| 6||5||8||13||4
| —||—||—||—||—
|- bgcolor="#f0f0f0"
| [[1994–95 IHL season|1994–95]]
| [[Chicago Wolves]]
| IHL
| 71||7||21||28||62
| 3||1||1||2||0
|-
| [[1995–96 IHL season|1995–96]]
| Chicago Wolves
| IHL
| 62||11||11||22||56
| 9||2||2||4||12
|- bgcolor="#f0f0f0"
| [[1996–97 IHL season|1996–97]]
| Chicago Wolves
| IHL
| 44||2||10||12||18
| 4||0||2||2||4
|-
| [[1997–98 IHL season|1997–98]]
| Chicago Wolves
| IHL
| 81||10||26||36||90
| 21||1||3||4||22
|- bgcolor="#f0f0f0"
| [[1998–99 IHL season|1998–99]]
| Chicago Wolves
| IHL
| 72||7||14||21||72
| 4||0||0||0||4
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan="3" | IHL totals
! 504!!68!!151!!219!!394
! 41!!4!!8!!12!!42
|}

==References==
{{Reflist|2}}

==External links==
*{{Eliteprospects}}
*{{hockeydb|562}}

{{good article}}

{{DEFAULTSORT:Breslin, Tim}}
[[Category:1967 births]]
[[Category:2005 deaths]]
[[Category:American ice hockey left wingers]]
[[Category:Chicago Wolves (IHL) players]]
[[Category:Ice hockey people from Illinois]]
[[Category:Lake Superior State Lakers men's ice hockey players]]
[[Category:Phoenix Roadrunners (IHL) players]]
[[Category:South Carolina Stingrays players]]
[[Category:People from Downers Grove, Illinois]]
[[Category:People from Addison, Illinois]]