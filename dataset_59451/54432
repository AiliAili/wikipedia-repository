{{Use dmy dates|date=October 2015}}
{{Use British English|date=October 2015}}
{{Infobox television
| show_name            = Mrs Biggs
| image                = File:Mrs Biggs titlecard.jpg
| caption              = First episode title card
| alt                  = Series title over a railway track
| show_name_2          = 
| genre                =  
| creator              =
| developer            = 
| writer               = [[Jeff Pope]]
| director             = Paul Whittington
| creative_director    = 
| presenter            = 
| starring             = [[Sheridan Smith]]<br />[[Daniel Mays]]
| judges               = 
| voices               = 
| narrated             = 
| theme_music_composer = 
| opentheme            = 
| endtheme             = 
| composer             = Bryony Marks
| country              = United Kingdom Australia co-production
| language             = English
| num_series           = 1<!-- or num_series -->
| num_episodes         = 5
| list_episodes        = #Episode list
| executive_producer   = Jeff Pope
| producer             = Kwadjo Dajan<br />Tony Wright (Australia)
| editor               = 
| location             = 
| cinematography       = [[Fabian Wagner]]
| camera               = 
| runtime              = 63-66 minutes (1,5)<br />43-46 minutes (2-4)
| company              = ITV Studios<br />December media
| distributor          = 
| network              = [[ITV (TV channel)|ITV]], [[STV (TV channel)|STV]], [[UTV (TV channel)|UTV]] (UK);<br/ > [[Seven Network]] (Australia)
| picture_format       = 
| audio_format         = 
| first_run            = 
| first_aired          = {{Start date|2012|9|5|df=yes}}
| last_aired           = {{End date|2012|10|3|df=yes}}
| preceded_by          = 
| followed_by          = 
| related              = 
| website              = 
| website_title        = 
| production_website   = 
}}
'''''Mrs Biggs''''' is a 2012 [[British television]] series based on the true story of the wife of the [[Great Train Robbery (1963)|Great Train Robber]], [[Ronnie Biggs]]. The series covers Mrs Charmian Biggs' journey from naïve young woman to Biggs' wife and the mother of three young sons. Money worries force her husband to ask for a loan from [[Bruce Reynolds]], planner of the most famous crime in British history, the Great Train Robbery of August 1963. The aftermath of the train robbery and Biggs subsequent escape from prison leads to a life of flight for Charmian and her children as she tries to keep the family together.<ref>{{cite web|url=http://www.itv.com/presscentre/presspacks/mrsbiggs/default.html |accessdate=6 September 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20120914232904/http://www.itv.com/presscentre/presspacks/mrsbiggs/default.html |archivedate=14 September 2012 }}</ref>

Charmian Biggs is played by [[Sheridan Smith]]; Ronnie Biggs is played by [[Daniel Mays]]. The series was written by [[Jeff Pope]], in co-operation with the real Charmian Biggs.<ref name="auto">Page 18/19 of the ITV press pack</ref> Smith's performance as Charmian received widespread critical acclaim, and she ultimately won the 2013 BAFTA Television Award for Best Actress. 
 
==Cast==
*[[Sheridan Smith]] as Charmian Biggs. The wife of the Great Train Robber Ronnie Biggs. For her performance Smith won the [[British Academy Television Award for Best Actress|BAFTA TV Award for Best Actress]] and nominations for the [[18th National Television Awards|National Television Award for Outstanding Female Dramatic Perforamce]] and the [[Royal Television Society|Royal Television Society for Best Actress]]
*[[Daniel Mays]] as [[Ronnie Biggs]]. For his performance Mays gained a nomination for the [[18th National Television Awards|National Television Awards for Outstanding Male Dramatic Performance]]
*[[Jay Simpson (actor)|Jay Simpson]] as [[Bruce Reynolds]]
*[[Claire Rushbrook]] as Ruby Wright
*[[Jack Lowden]] as Alan Wright
*[[Tom Brooke]] as Mike Haynes
*[[Leo Gregory]] as Eric Flower
*[[Freya Stafford]] as Julie Flower
*[[Denise Roberts]] as Annie
*[[Adrian Scarborough]] as Bernard Powell, Charmian's father
*[[Caroline Goodall]] as Muriel Powell, Charmian's mother
*Florence Bell as Gillian Powell, Charmian's sister
*[[Phil Cornwell]] as [[Detective Sergeant]] [[Jack Slipper]]
*Robin Hooper as Mr Kerslake
*[[Luke Newberry]] as Gordon
*[[Iain McKee]] as Charlie Wilson
*Matthew Cullum as Buster Edwards
*Jon Foster as Goody
*[[Ron Cook]] as Peter
*George Oliver as Polish Henry

==Production notes==
The series was filmed in London, Surrey, Buckinghamshire, Manchester, Adelaide and Melbourne.<ref>Page 4 of the ITV press pack</ref>

Scenes of the Great Train Robbery were recreated on the [[East Lancashire Railway]] using a locomotive from the same batch of engines involved in the 1963 raid.<ref>Page 24 of the ITV press pack.</ref>

Charmian Biggs acted as a consultant on the series and flew to Britain from Australia in February 2012, just before filming began. She also visited Ronnie, who was ill; the couple had divorced in 1976 but remain on good terms. Some of the names in the series were changed for legal reasons.

Charmian met actress Sheridan Smith and sat with her at the manuscript read-through.<ref name="auto"/>

While filming in Australia, Sheridan Smith and Daniel Mays spent an evening at Charmian’s house, where she showed them her archive of personal letters and scrapbooks.<ref>Page 25 of the ITV press pack</ref>

Charmian herself appears in the background of one of the scenes in the public gallery of the Australian court when the lawyer is arguing for her to be released.<ref>Page 10 of the ITV press release</ref>

==Episode list==
{| class="wikitable plainrowheaders" width="100%" style="background:#FFFFFF;"
!style="background: #bfe0bf;"| #
!style="background: #bfe0bf;"| Title
!style="background: #bfe0bf;"| Directed by
!style="background: #bfe0bf;"| Written by
!style="background: #bfe0bf;"| Original air date
!style="background: #bfe0bf;"| Viewers (millions)<br /> <small> Sourced by [[Broadcasters' Audience Research Board|BARB]]; includes [[ITV HD]] and ITV +1</small>
|-
{{Episode list
 |EpisodeNumber = 1
 |Title= Episode One
 |DirectedBy= Paul Whittington 
 |WrittenBy=[[Jeff Pope]]
 |OriginalAirDate= {{start date|2012|9|5|df=y}}
 |Aux4= 5.05
 |ShortSummary=Charmian meets [[Ronnie Biggs]] on a train. As Ronnie is a carpenter and petty criminal, Charmian's father, a headmaster, does not approve and forbids her to see him. In love, Charmian leaves home, steals from her employer and goes on the run with Ronnie and his best friend Mike. When captured, Biggs is imprisoned and Charmian is given a suspended sentence. On release, Charmian and Ronnie marry, her pregnancy forcing her father's consent. Going straight but needing money, unbeknown to Charmian, Ronnie is drawn into [[Bruce Reynolds]]'s plans for the [[Great Train Robbery (1963)|Great Train Robbery]].
 |LineColor=bfe0bf
 }}
{{Episode list
 |EpisodeNumber = 2
 |Title= Episode Two
 |DirectedBy= Paul Whittington
 |WrittenBy= Jeff Pope
 |OriginalAirDate={{start date|2012|9|12|df=y}}
 |Aux4= 5.24
 |ShortSummary=Ronnie returns from his [[aboriculture|tree-felling]] job and Charmian realises he has been involved in the Great Train Robbery. This is confirmed when he shows her his share; £147,000. Charmian lives in fear of discovery and they disperse the money amongst trusted friends. In September 1963 Ronnie is arrested and Charmian is besieged by the press. She is drawn into the disappeared Bruce Reynolds's circle of friends. They deal brutally with an [[extortionist]] demanding money from her for protection. At the trial Ronnie is given 30 years' imprisonment and Charmian is disowned by her parents. 
 |LineColor=bfe0bf
 }}
{{Episode list
 |EpisodeNumber = 3
 |Title= Episode Three
 |DirectedBy= Paul Whittington
 |WrittenBy= Jeff Pope
 |OriginalAirDate= {{start date|2012|9|19|df=y}}
 |Aux4= 5.33
 |ShortSummary=With no chance of remission for Ronnie, Charmian has an affair with Ruby Wright's son, Alan. She becomes pregnant and has a [[Abortion in the United Kingdom|back-street abortion]]. In July 1965, Biggs and three others escape from [[Wandsworth (HM Prison)|Wandsworth prison]], in a prison break organised by underworld friends. In December 1965 Charmian meets him in Paris. Determined to reunite the family, they take on new names and in June 1966 Charmian and her two boys fly to Darwin, Australia. They reunite with Ronnie again, and after another name change the family settle in Adelaide. Charmian becomes pregnant and a third son is born. They receive news from England that Bruce Reynolds has been arrested in Mexico. 
 |LineColor=bfe0bf
 }}
{{Episode list
 |EpisodeNumber = 4
 |Title= Episode Four
 |DirectedBy=Paul Whittington
 |WrittenBy= Jeff Pope
 |OriginalAirDate={{start date|2012|9|26|df=y}}
 |Aux4= 5.22
 |ShortSummary=Biggs's picture appears in an Australian magazine. After another change of name, with the robbery money now all gone, they move to Melbourne and both get jobs. A London ‘grass’ lets slip that Ronnie Biggs is hiding ‘somewhere in Australia’ and Biggs's  picture appears on the evening news in Melbourne. Biggs goes into hiding; Charmian is arrested and the children are taken into care. Charmian agrees to sell her story to the [[Kerry Packer|Packer organization]] in exchange for a sum of cash and a lawyer to represent her in court. The lawyer wins her freedom and helps her get her children back. She secretly meets Biggs before he takes a steamer to Panama and overland to Rio De Janeiro. 
 |LineColor=bfe0bf
 }}
{{Episode list
 |EpisodeNumber = 5
 |Title= Episode Five
 |DirectedBy= Paul Whittington
 |WrittenBy= Jeff Pope
 |OriginalAirDate={{start date|2012|10|3|df=y}}
 |Aux4= 5.13
 |ShortSummary=With the Packer money, Charmian buys a new house for herself and the boys. She misses Ron, and he is lonely, working as a roofer in a dingy Rio suburb. Tragedy strikes when their eldest son, Nicky, is killed in a car crash and Charmian has to smuggle out a letter to Biggs. Charmian studies for a degree as a mature student. Biggs engages in drink, drugs and women, eventually meeting Raimunda, a beautiful young Brazilian dancer. Biggs meets with a ''[[Daily Express]]'' journalist in Rio to sell his life story. The journalist tips off the police, and Biggs is thrown into prison in Brasilia, facing extradition. He discovers that Raimunda is pregnant, and realizes that if the baby is his, the Brazilian authorities might not deport him. Charmian flies out to Rio for an emotional reunion, but Ron asks her to divorce him so that he can marry Raimunda. When Biggs’s extradition is refused, Charmian plans for life in Rio and takes her sons to meet their father for the first time in nearly five years. Biggs asks Charmian to live with him and Raimunda in Rio; she refuses and sets our for England to see her family for the first time in over a decade. Her father is delighted when she tells him she and Ron are to divorce but that her life is now in Australia where she won’t just be ‘Mrs Biggs, the wife of the Great Train Robber’.
 |LineColor=bfe0bf
 }}
 |}

==DVD==
A region 2, two disc set of the series was released on 15 October 2012.

==References==
{{reflist}}

==External links==
*{{IMDb title|2186101|Mrs Biggs}}

[[Category:2010s British television series]]
[[Category:2012 British television programme debuts]]
[[Category:2012 British television programme endings]]
[[Category:British crime television series]]
[[Category:English-language television programming]]
[[Category:ITV television dramas]]
[[Category:Television series set in the 1960s]]
[[Category:Television series set in the 1970s]]
[[Category:Television shows set in Brazil]]
[[Category:Television shows set in England]]
[[Category:Television shows set in Melbourne]]
[[Category:Television shows set in South Australia]]
[[Category:Television series by ITV Studios]]