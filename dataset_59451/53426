{{refimprove|date=April 2016}}
[[File:CAQ TagLineAffiliatedCPA.gif|alt=CAQ logo|thumb|CAQ logo|262x262px]]{{Infobox company |
 name = The Center for Audit Quality |
 logo = |
 type = Autonomous member-funded non-profit |
 foundation = 2007 |
 location = [[Washington, D.C.]], United States |
 key_people = [[Cynthia M. Fornelli]], Executive Director|
 homepage = [http://www.TheCAQ.org www.TheCAQ.org]
}}
==About the Center for Audit Quality==
About the Center for Audit Quality 
The Center for Audit Quality (CAQ) is an autonomous, nonpartisan, nonprofit public policy advocacy organization based in Washington, DC. It is affiliated with the American Institute of CPAs.

The CAQ is "dedicated to enhancing investor confidence and public trust in the global capital markets by fostering high quality performance by public company auditors; convening and collaborating with other stakeholders to advance the discussion of critical issues requiring action and intervention; and, advocating policies and standards that promote public company auditors’ objectivity, effectiveness, and responsiveness to dynamic market conditions."

Founded in 2007, the organization has been led since its inception by Executive Director [[Cynthia M. Fornelli]], a U.S. business leader and former securities lawyer.

==Membership and structure==
The CAQ is self-supporting and entirely funded by member dues. Membership in the CAQ is open to U.S. accounting firms registered with the [[Public Company Accounting Oversight Board]] (PCAOB). Associate membership is available for U.S. accounting firms not registered with the PCAOB. The CAQ also offers international firm subscriptions.

==Policy Issues==
The CAQ engages with regulators and auditing and accounting standards-setters on policy proposals and public company auditing issues. It also provides a forum for its member firms to discuss those issues, share perspectives, and formulate positions. The CAQ works on a wide range of public policy issues, including the following:
*The auditor's reporting model
*Audit quality indicators
*The role of audit committees
*Financial reporting fraud deterrence and detection 
*Accounting estimates and fair value
*Financial disclosure 
*Auditor of the future
*Data security and privacy
*Sarbanes-Oxley Act implementation and PCAOB rulemaking
*IFRS
*Mandatory audit firm rotation
*Going concern
*Audit engagement partner identification
*Corporate governance
*Company use of non-GAAP accounting measures
*Cybersecurity
*Audit profession talent pipeline

The CAQ [http://www.thecaq.org/policy policy page]<ref>http://www.thecaq.org/policy</ref> provides more information on the CAQ's policy work.

==Resources on Auditing, Financial Reporting, and Corporate Governance==
The CAQ has produced a range of resources aimed at fostering high performance by public company auditors and enhancing investor confidence. These resources include guides, white papers, webcasts, and research studies.

Examples include:
*[http://www.thecaq.org/reports-and-publications/professional-judgment-resource Professional Judgment Resource]<ref>http://www.thecaq.org/reports-and-publications/professional-judgment-resource</ref>
*[http://www.thecaq.org/resources/comment-letters CAQ comment letters]<ref>http://www.thecaq.org/resources/comment-letters</ref>
*[http://www.thecaq.org/resources/comment-letters CAQ investor education video series]<ref>http://www.thecaq.org/resources/comment-letters</ref>
*[http://www.thecaq.org/survey-overwhelming-majority-american-investors-are-confident-us-markets Annual Main Street Investor Survey] Conducted each year since 2007, the CAQ's Main Street Investor Survey gauges investor confidence in U.S. capital markets, global capital markets, audited financial information, and investing in U.S. publicly traded companies

Visit the CAQ [http://www.thecaq.org/resources resources]<ref>http://www.thecaq.org/resources</ref> page for more information.

==Research==
The CAQ facilitates research initiatives and provides independent funding of scholarly research projects to address new or significant issues relating to public company auditing and the capital markets.

Since 2009, the CAQ through its Research Advisory Board, has annually funded independent academic research projects on auditing-related topics, including audit quality, professional judgment, professional skepticism, and the value of the audit.

In 2013, the CAQ and the Auditing Section of the American Accounting Association created a joint program designed to facilitate accounting and auditing academics’ ability to obtain access to audit firm personnel to participate in their research projects.

Other CAQ research efforts include:
*[http://www.thecaq.org/reports-and-publications/intersecting-roles Intersecting Roles: Fostering Effective Working Relationships Among External Audit, Internal Audit, and the Audit Committee]:<ref>http://www.thecaq.org/reports-and-publications/intersecting-roles</ref> Published jointly by the CAQ and The Institute of Internal Auditors, a report capturing key points and takeaways from a series of 2014 roundtable meetings of internal auditors, external auditors, and audit committee chairs. (March 2015)
*[http://www.thecaq.org/reports-and-publications/audit-committee-transparency-barometer Audit Committee Transparency Barometer]:<ref>http://www.thecaq.org/reports-and-publications/audit-committee-transparency-barometer</ref> Published jointly by the Center for Audit Quality and Audit Analytics, the Audit Committee Transparency Barometer presents findings from an extensive analysis of 2015 audit committee disclosures in proxy statements, measuring the robustness of these disclosures among S&P Composite 1500 companies. (November 2015)
*[http://www.thecaq.org/reports-and-publications/the-fraud-resistant-organization The Fraud-Resistant Organization]:<ref>http://www.thecaq.org/reports-and-publications/the-fraud-resistant-organization</ref> The Fraud-Resistant Organization: Tools, Traits, and Techniques to Deter and Detect Financial Reporting Fraud provides valuable information about the conditions that might make an organization more susceptible to fraud—and how to mitigate those conditions. (November 2014)
*[http://www.thecaq.org/survey-overwhelming-majority-american-investors-are-confident-us-markets The CAQ's 10th Annual Main Street Investor Survey] Conducted each year since 2007, the CAQ's Main Street Investor survey gauges investor confidence in U.S. capital markets, global capital markets, audited financial information, and investing in U.S. publicly traded companies. (September 2016)
*[http://www.thecaq.org/reports-and-publications/financial-restatement-trends-in-the-united-states-2003-2012/financial-restatement-trends-in-the-united-states-2003-2012 Financial Restatement Trends in the United States: 2003-2012]:<ref>http://www.thecaq.org/reports-and-publications/financial-restatement-trends-in-the-united-states-2003-2012/financial-restatement-trends-in-the-united-states-2003-2012</ref> Analysis of the trends in and characteristics of restatements of public company financial statements provides useful information for all stakeholders in the capital markets. (July 2014)

More CAQ’s research can be found [http://www.thecaq.org/reports-and-publications on its website].<ref>http://www.thecaq.org/reports-and-publications</ref>

==Governing board==

A Governing Board made up of chief executive officers from leading public company auditing firms and the AICPA, as well as three independent members from outside the public company auditing profession leads the CAQ.

'''Governing Board Chair'''
*Catherine M. Engelbert, CEO, Deloitte LLP

'''Governing Board Vice Chair'''
*Joseph M. Adams, Managing Partner and CEO, RSM US LLP

'''Governing Board Members''' <ref>http://www.thecaq.org/about-us/governing-board</ref>
*Wayne Berson, CEO, BDO USA, LLP
*Jeffrey Brown, Professor of Business and Dean, College of Business, University of Illinois at Urbana-Champaign
*Lynne Doughtie, U.S. Chairman and CEO- Elect, KPMG LLP
*Michele J. Hooper, President and CEO, The Directors’ Council
*Stephen R. Howe, Jr., U.S. Chairman and Americas Managing Partner, EY LLP
*J. Michael (Mike) McGuire, CEO, Grant Thornton LLP
*Barry Melancon, President and CEO, American Institute of CPAs
*James L. Powers, CEO, Crowe Horwath LLP
*Timothy F. Ryan, Chairman and Senior Partner, PricewaterhouseCoopers LLP
*Mary Schapiro, Vice Chairman Advisory Board, Promontory Financial Group

==Notes or references==
{{reflist}}

==External links==
*[http://www.thecaq.org CAQ Website]
*[[Cynthia M. Fornelli]]
*[http://www.antifraudcollaboration.org Anti-Fraud Collaboration]
*[http://auditcommitteecollaboration.org Audit Committee Collaboration]
*[https://www.discoveraudit.org Discover Audit]

{{DEFAULTSORT:Center For Audit Quality}}
[[Category:Auditing organizations]]
[[Category:Financial markets]]
[[Category:Industry trade groups based in the United States]]
[[Category:Accounting organizations]]
[[Category:Accounting in the United States]]