<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Breguet 1100
 | image=Breguet 1100 fighter on the ground c1958.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Single-seat light tactical [[fighter aircraft|fighter]] and [[ground-attack]] aircraft
 | national origin=[[France]]
 | manufacturer=[[Breguet Aviation]]
 | designer=
 | first flight=31 March 1957
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Breguet 1100''' was a twin-engine [[France|French]] light [[fighter aircraft|fighter]] also suitable for [[ground-attack]] and built for the French armed forces. First flown in 1957, only one was completed as budget cuts led to cancellation of the programme.

==Design and development==
The single-engine [[Breguet 1001 Taon]] was designed and built for a [[NATO]] fast ground-attack fighter competition. The Breguet 1100 was a development designed instead to a [[France|French]] Ministère de l'Air ({{lang-en|Air Ministry}}) specification, which called for two engines and a pressurised [[cockpit]] in an aircraft performing a similar role.<ref name=JAWA57/> The 1100 flew before the Taon, as the latter was delayed to incorporate the new [[area rule]] late in its construction.<ref name=G&S/>

The Breguet 1100 was built entirely from bonded alloy and included many honeycomb structures.<ref name=JAWA57/> Its swept wing was about 15% greater in span and 35% greater in area than that of the Taon, though it was intended that production Taons would share the 1100's wing.<ref name=G&S/>  The 1100 had broad [[chord (aircraft)|chord]], short-span [[aileron]]s and narrow, long-span [[flap (aircraft)|flaps]] with [[spoiler (aeronautics)|spoiler]]s in front of them.<ref name=JAWA57/> The two types had similar side air intakes but the 1100's twin mid-fuselage engines and jet pipes meant that the [[fuselage]] was broader and lacked any area rule waisting, as well as being rather longer. They also shared similar swept, straight-edged tail surfaces, cockpits in the nose with narrow [[aircraft fairing|fairings]] running over the length of the upper fuselage and [[tricycle landing gear]].<ref name=G&S/>

Sixteen different armament packages were available, including four {{convert|0.5|in|mm|abbr=on|disp=flip|0}} [[Browning machine gun]]s, two {{convert|30|mm|in|abbr=on||0}} [[DEFA]] [[cannon]], 35 [[Matra]] unguided rockets or a pack of fifteen {{convert|68|mm|in|abbr=on||0}} [[SNEB]] 22 rockets.<ref name=JAWA57/><ref name=G&S/>

The 1100 first flew on 31 March 1957, exceeding Mach 1. This first prototype was the only one to fly as the second was abandoned when 80% complete and an ordered third prototype, a navalized version designated '''1100M''', was not begun.<ref name=G&S/> Before mid-1959 the Breguet 1100 programme had been cancelled due to government spending cuts.<ref name=Flight/>
<!-- ==Operational history== -->

==Variants==
;Breguet 1100: First two prototypes, second unfinished.
;Breguet 1100M: Navalized third prototype: not built.

==Specifications (1100) ==
{{Aircraft specs
|ref=The Complete Book of Fighters<ref name=G&S/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=12.52
|length note=
|span m=7.83
|span note=
|height m=4.35
|height note=
|wing area sqm=19.52
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=3793
|empty weight note=equipped
|gross weight kg=6545
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Turbomeca Gabizo]]
|eng1 type=Axial flow turbojet
|eng1 lbf=2668
|eng1 note=
|eng1 lbf-ab=3307
<!--
        Performance
-->
|perfhide=

|max speed kmh=1128
|max speed note=at sea level
|max speed mach=0.92
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
}}

==References==
{{reflist|refs=

<ref name=G&S>{{cite book |last=Green |first=William |first2= Gordon|last2= Swanborough |title=The Complete Book of Fighters |date=1994|publisher=Salamander Books|location=Godalming, UK|isbn=1-85833-777-1|page=89}}</ref>

<ref name=JAWA57>{{cite book |title= Jane's All the World's Aircraft 1957-58|last= Bridgman |first= Leonard |coauthors= |edition= |year=1957|publisher=Sampson Low, Marston & Co. Ltd|location= London|isbn=|page=140}}</ref>

<ref name=Flight>{{cite magazine |last= |first= |authorlink= |coauthors= |date=12 June 1959 |title= The French industry|magazine= [[Flight International|Flight]]|volume=75 |issue=2629 |page=795 |url= http://www.flightglobal.com/pdfarchive/view/1959/1959%20-%201642.html  }}</ref>

}}
<!-- ==Further reading== -->

==External links==
*[http://www.aviafrance.com/aviafrance1.php?ID=985&ID_CONSTRUCTEUR=249&ANNEE=0&ID_MISSION=0&MOTCLEF= Aviafrance Bre.1100 page]
{{Breguet aircraft}}

[[Category:French fighter aircraft 1950–1959]]
[[Category:Breguet aircraft|Breguet 1100]]