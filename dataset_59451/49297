{{Use dmy dates|date=March 2012}}
'''Lil Bitts''' (born Shivonne Liesl-Anne Churche, 31 October 1984)<ref>[http://www.spindmusic.com/profiles/lilbitts.html Lil Bitts], Spin D Music Profiles. Retrieved 2012-01-09.</ref> is a soca musician from [[Trinidad and Tobago]]. She is best known for her hits "Bump", "Crush" and "Hold Meh".

==Biography==

Lil Bitts began singing Social Commentary at the age of nine, leaving Sacred Heart Girls RC School with the title of "[[Calypso music|Calypso]] Monarch" in 1996. She played the tenor [[steel pan]] with Woodtrin Steel Orchestra (1998-1999) when they won the National Junior Panorama Competition and a finalist in the National Calypso Juniors in 1999.

After taking second place at the Inter Calypso School Competition in 2001, she began the move from Social Commentary to [[Soca music]], coming fifth at the National Junior Soca Monarch Competition later that year. Her self-penned song "Esta Fiesta" on the Christmas Riddum won Best Soca Parang of 2005.

"Crush", written by [[Kernal Roberts]], enabled her to make her debut into the [[International Soca Monarch]] Competition 2006. She was a finalist in the Groovy Soca category in 2010 with "Careful" penned by her brother, [[Sherrard Churche]].

Lil Bitts, together with Mel (Shivorne Melissa Mitchell), Miki (Tamika Ward-Lewis) and CutieRae (Elena Rachel Rawlins), launched the Making Music to Benefit Charities (M2BC) Foundation in 2009. The Foundation organises fund raisers and hosts various events throughout Trinidad to raise money to purchase instruments for numerous homes and orphanages in Trinidad.<ref>[http://blog.nurse-karen.com/2009/10/booster-shot-artistes-doing-their-lil.html Booster Shot:Artistes doing their Lil Bitts for Charity], Nurse Karen etc., 18 October 2009. Retrieved 2012-01-16.</ref>

In 2010, Lil Bitts established an entertainment services company, Go 4wd Entertainment.<ref name="TrinEX Mischief">Wayne Bowman [http://www.trinidadexpress.com/featured-news/Lil__Bitts_up_to_Mischief-136122558.html ''Lil' Bitts up to Mischief''], Trinidad Express, 23 December 2011. Retrieved 2012-01-08.</ref> The company aims to serve new and rising young artistes, providing development as well as booking services.

Lil Bitts has performed in [[Antigua]], [[The Bahamas]], [[Barbados]], Canada,<ref>[http://www.caribanatoronto.com/event/socarave Soca Rave @ Kool Haus Nightclub], caribanatoronto.com. Retrieved 2012-01-17</ref> [[Grenada]], [[Guyana]],<ref>[http://www.guyanalive.com/gallery/v/Team_Toronto/2011-07-31_Caribana_Rave_The_Main_Event_Soca_Shock/IMG_5645.jpg.html Caribana Rave: The Main Event - Soca Shock], Guyanalive.com, 31 July 2007, Retrieved 2012-01-16</ref> [[Jamaica]],<ref>[http://www.trinijunglejuice.com/tjjnews/articles/2091/1/Bacchanal-Jamaica-showed-no-signs-of-slowing-down-with-Cassi-amp-Lil-Bitts/Page1.html Bacchanal Jamaica showed no signs of slowing down with Cassi & Lil' Bitts], trinijunglejuice.com, 28 March 2011, Retrieved 2012-01-16</ref> [[Saint Vincent and the Grenadines|St. Vincent]] and the United States whilst regularly appearing on stage in Trinidad.<ref>[http://caribflyer.com/carib/lots-of-water-in-agua-soca/ Lots of water in Agua Soca], caribflyer.com, 7 February 2011, Retrieved 2012-01-16</ref> She is actively supported by her fan club, known as the Bitts Army.<ref name="TrinEX Mischief" />

She is pursuing an Associate Degree in Performing Arts at the [[College of Science, Technology and Applied Arts of Trinidad and Tobago]] (COSTAATT).

'''International Soca Monarch and Groovy Soca Monarch Competitions'''

*2011 - Semi finalist Power Soca and Groovy Soca categories.<ref>[http://caribflyer.com/carib/2011-soca-monarch-semi-finalists/ 2011 Soca Monarch – Semi Finalists], Carib101.com, 2 February 2011. Retrieved 2012-01-16.</ref>
*2012 - Semi finalist Groovy Soca category <ref>[http://www.socarevolution.com/2012/01/soca-monarch-2012-semi-finalists.html Soca Monarch 2012 Semi-finalists], socarevolution.com, 16 January 2012. Retrieved 2012-01-17.</ref>
*2013 - Finalist Power Soca category.<ref>[http://www.socamonarch.net/new/media/latest-news/191-play-whe-international-power-soca-monarch-a-digicel-international-groovy-soca-monarch-competitions-2013-semi-finalist.html Soca Monarch 2013 Finalists], socamonarch.net, 22 January 2013. Retrieved 2012-01-22.</ref> Finals to be held on February 8, 2013.

==Discography==

===Solo===
*''Spin Yuh Towel'' (2002)
*''Groove Mih'' (2002)
*''Doh Leh Go'' (2003)
*''Bump'' (2005)
*''Esta Fiesta'' (2005)
*''Ah Little Bit'' (2007)
*''Hold Meh'' (2009)
*''Careful'' (2010)
*''Everywhere'' (2010)
*''Go Down Low'' (2011)
*''Sweetness'' (2011)
*''We Own The Night'' (2011)
*''Parade of De Bandz'' (2011)
*''Mischief'' (2011)
*''Ketchin It'' (2011)
*''Stormin'' (2012)
*''Horner Woman (Free Up Riddim)'' (2012)
*''Juk (PM1 Riddim)'' (2012)
*''Panorama'' (2012)
*''Raise De Dust'' (2012)
*''Instruction'' (2013)

===Collaborations===
*''In My Country'' (featuring [[Bunji Garlin]]) (2004)
*''Somebody'' (featuring [[Baron (musician)]]) (2006)
*''Crush'' (featuring [[Sean Caruth]]) (2006)
*''Juicy Bitts'' (featuring [[Juicy Jahbami]]) (2007)
*''Luv U For Eva'' (featuring [[Bunji Garlin]]) (2007)
*''Dis Year'' (featuring [[Olatunji Yearwood]]) (2009)
*''Careful'' (remix featuring [[Skinny Fabulous]]) (2010)

==References==
{{reflist}}

==External links==
*[http://www.trinidadexpress.com/woman-magazine/21_Questions_with_Lil_Bitts-136115313.html 21 Questions with Lil Bitts (Trinidad Express, Dec 22, 2011)]
*[http://www.guardian.co.tt/entertainment/2011/01/20/weakness-sweetnessbig-tune-lil-bitts A weakness for Sweetness...big tune for Lil Bitts (Guardian Trinidad, Jan 20, 2011)]
*[http://www.reverbnation.com/lilbitts Lil Bitts on Reverbnation.com]
*[https://www.youtube.com/bittsy31 Lil Bitts on Youtube.com]
*[http://www.facebook.com/ShivonneLilBittsChurche  Lil Bitts on Facebook.com]

{{DEFAULTSORT:Lil Bitts}}
[[Category:Living people]]
[[Category:1984 births]]
[[Category:Soca musicians]]
[[Category:Trinidad and Tobago musicians]]