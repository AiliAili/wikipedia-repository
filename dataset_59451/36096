{{italic title}}{{good article}}{{Automatic taxobox
| name = ''Alioramus''
| image = Texas Alioramus.jpg
| image_width = 250px
| image_caption = Skeleton mount in Texas
| fossil_range = [[Late Cretaceous]], {{fossil range|70}}
| authority= [[Sergei Kurzanov|Kurzanov]], 1976
| type_species = {{extinct}}''Alioramus remotus''
| type_species_authority = Kurzanov, 1976
| subdivision_ranks = [[Species]]
| subdivision = 
{{extinct}}''A. remotus'' <small>Kurzanov, 1976</small><br>
{{extinct}}''A. altai'' <small>Brusatte ''et al.'', 2009</small>
| synonyms =
{{collapsible list|bullets = true
 |title=<small>Genus synonymy</small>
|''[[Qianzhousaurus]]''? <br><small>[[Junchang Lü|Lü]] et al., 2014</small><ref name=Horneri>{{cite journal|last1=Carr|first1=Thomas D.|last2=Varricchio|first2=David J.|last3=Sedlmayr|first3=Jayc C.|last4=Roberts|first4=Eric M.|last5=Moore|first5=Jason R.|title=A new tyrannosaur with evidence for anagenesis and crocodile-like facial sensory system|journal=Scientific Reports|date=2017|volume=7|pages=44942|doi=10.1038/srep44942}}</ref>
}}
}}
'''''Alioramus''''' ({{IPAc-en|ˌ|æ|l|i|oʊ-|ˈ|r|eɪ|m|ə|s}}; meaning 'different branch') is a [[genus]] of [[tyrannosaurid]] [[theropod]] [[dinosaur]]s from the [[Late Cretaceous]] period of [[Asia]]. The [[type species]], ''A. remotus'', is known from a partial [[skull]] and three [[metatarsal|foot bones]] recovered from [[Mongolia]]n sediments which were deposited in a humid [[floodplain]] about 70&nbsp;[[million years ago]]. These remains were named and described by [[Soviet]] paleontologist [[Sergei Kurzanov]] in 1976. A second species, ''A. altai'', known from a much more complete skeleton, was named and described by [[Stephen L. Brusatte]] and colleagues in 2009. Its relationships to other tyrannosaurid genera are unclear, with some evidence supporting a hypothesis that ''Alioramus'' is closely related to the contemporary species ''[[Tarbosaurus bataar]]''.

''Alioramus'' were [[biped]]al like all known theropods, and their sharp teeth indicate that they were [[carnivore]]s. Known specimens were smaller than other tyrannosaurids like ''Tarbosaurus bataar'' and ''[[Tyrannosaurus rex]]'', but their adult size is difficult to estimate since both ''Alioramus'' species are known only from juvenile or sub-adult remains. The recent discovery of ''[[Qianzhousaurus]]'' indicates that it belongs to a distinct branch of tyrannosaur.<ref>{{cite journal|title=A new clade of Asian Late Cretaceous long-snouted tyrannosaurids|doi=10.1038/ncomms4788|author=Junchang Lü |author2=Laiping Yi |author3=Stephen L. Brusatte |author4=Ling Yang |author5=Hua Li |author6=Liu Chen|journal=[[Nature Communications]]|volume=5|issue=3788|date=7 May 2014 |pmid=24807588}}</ref> The genus ''Alioramus'' is characterized by a row of five bony crests along the top of the snout, a greater number of teeth than any other genus of tyrannosaurid, and a lower skull than other tyrannosaurids.

==Description==
[[File:Alioramus skeletal steveoc.png|thumb|left|Size of ''A. remotus'' compared with a human]]
''Alioramus remotus'' was estimated at {{convert|5|to|6|m|ft}} in length when originally described by Sergei Kurzanov in 1976.<ref name=kurzanov1976>{{cite_journal |last=Kurzanov |first=Sergei M. |authorlink=Sergei Kurzanov |title=A new carnosaur from the Late Cretaceous of Nogon-Tsav, Mongolia |language=Russian |journal=The Joint Soviet-Mongolian Paleontological Expedition Transactions |volume=3 |pages=93–104}}</ref> Kurzanov, however, did not correct for lengthening of the skull by deformation during [[fossilization]], which may indicate a shorter overall body length for this individual. If this specimen is a juvenile, then adult ''Alioramus'' would have reached greater lengths, but no confirmed adult specimens are known.<ref name=holtz2004>{{cite_book |last=Holtz |first=Thomas R. |authorlink=Thomas R. Holtz, Jr. |year=2004 |chapter=Tyrannosauroidea |editor= [[David B. Weishampel|Weishampel, David B.]] |editor2=[[Peter Dodson|Dodson, Peter]] |editor3=Osmólska, Halszka |title=The Dinosauria |edition=Second |publisher=University of California Press |location=Berkeley |pages=111–136 |isbn=0-520-24209-2}}</ref>

The skull of ''A. remotus'' was approximately {{convert|45|cm|in}} long.<ref name=currie2000>{{cite_book |last=Currie |first=Philip J. |authorlink=Phil Currie |year=2000 |chapter=Theropods from the Cretaceous of Mongolia |title=The Age of Dinosaurs in Russia and Mongolia |location=Cambridge |publisher=Cambridge University Press |pages=434–455 |isbn=978-0-521-54582-2}}</ref> In general, it is long and low, a shape typical of more [[basal (phylogenetics)|basal]] [[tyrannosauroid]]s and juveniles of larger tyrannosaurids. The [[premaxilla]]ry bones at the tip of the snout in ''Alioramus remotus'' have not been found, but are taller than wide in all tyrannosauroids for which they are known.<ref name=holtz2004/> The [[nasal bone]]s are fused and ornamented with a row of five irregular bony crests that protrude upwards from the midline, where the nasal bones are [[Suture (anatomical)|sutured]] together. These crests all measure more than {{convert|1|cm|in}} tall.<ref name=kurzanov1976/>
[[File:Alioramus_Life_Restoration.jpg|left|thumb|Artist's impression of ''A. remotus'']]
At the back of the skull there is a protrusion, called the ''nuchal crest'', arising from the fused [[parietal bone]]s, a feature shared with all tyrannosaurids. In ''Alioramus'', the nuchal crest is greatly thickened, similarly to ''Tarbosaurus'' and ''Tyrannosaurus''. Like the rest of the skull, the [[mandible|lower jaw]] of ''Alioramus'' was long and slender, another possible juvenile characteristic.<ref name=holtz2004/> As in ''Tarbosaurus'', a ridge on the outer surface of the [[angular bone]] of the lower jaw articulated with the rear of the [[dentary bone]], locking the two bones together and removing much of the flexibility seen in other tyrannosaurids.<ref name=hurumsabath2003>{{cite_journal |last=Hurum |first= Jørn H. |last2=Sabath |first2=Karol |year=2003 |title=Giant theropod dinosaurs from Asia and North America: Skulls of ''Tarbosaurus bataar'' and ''Tyrannosaurus rex'' compared |journal=Acta Palaeontologica Polonica |volume=48 |issue=2 |pages=161–190 }}</ref> Other tyrannosaurids had four premaxillary teeth, ''D''-shaped in [[Cross section (geometry)|cross section]], on each side. Including 16 or 17 in each [[maxilla]], and 18 in each [[dentary]], ''Alioramus'' had 76 or 78 teeth, more than any other tyrannosaurid.<ref name=currie2003a>{{cite_journal |last=Currie |first=Philip J. |authorlink=Phil Currie |year=2003 |title=Cranial anatomy of tyrannosaurids from the Late Cretaceous of Alberta |journal=Acta Palaeontologica Polonica |volume=48 |issue=2 |pages=191–226 }}</ref> The braincase of ''A. altai'' was intermediate between the basal theropod and [[avialan]] conditions.<ref>{{cite web|url=http://www.ploscollections.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0023393;jsessionid=B5ED8399160D7F46A7647ADE513F5B9C.ambra01 |title=Variation, Variability, and the Origin of the Avian Endocranium: Insights from the Anatomy of Alioramus altai (Theropoda: Tyrannosauroidea) |publisher=PLOS Collections |accessdate=2012-11-09}}</ref>

The rest of the skeleton of ''Alioramus remotus'' is completely unknown except for three [[metatarsal]]s (bones of the upper foot), but the discovery of ''A. altai'', which is known from substantially more complete remains, has shed light on the anatomy of the genus.<ref name=SLBetal09>{{cite journal |last=Brusatte |first=Stephen L. |author2=Carr, Thomas D.|author3= Erickson, Gregory M.|author4= Bever, Gabe S.|author5= Norell, Mark A. |year=2009 |title=A long-snouted, multihorned tyrannosaurid from the Late Cretaceous of Mongolia |journal=Proceedings of the National Academy of Sciences of the United States of America |volume= 106|doi=10.1073/pnas.0906911106 |pmid=19805035 |issue=41 |pages=17261–6 |pmc=2765207}}</ref>

==Classification and systematics==
[[Paleontologist]]s have long [[biological classification|classified]] ''Alioramus'' within the [[Taxonomic rank|superfamily]] Tyrannosauroidea, but because its remains were for many years poorly known, a more precise classification had remained elusive until the discovery of ''A. altai''.<ref name=holtz2004/> A [[cladistic]] analysis published in 2003 found ''Alioramus'' could be further classified into the family Tyrannosauridae and the [[subfamily]] Tyrannosaurinae, alongside ''Tyrannosaurus'', ''Tarbosaurus'' and ''[[Daspletosaurus]]''.<ref name=currieetal2003>{{cite_journal |last=Currie |first=Philip J. |last2=Hurum |first2=Jørn H |last3=Sabath |first3=Karol |authorlink=Phil Currie |year=2003 |title=Skull structure and evolution in tyrannosaurid phylogeny |journal=Acta Palaeontologica Polonica | volume=48 |issue=2 |pages=227–234 }}</ref> A 2004 study supported this result but suggested it was equally probable that ''Alioramus'' belonged outside the family Tyrannosauridae entirely, with its supposed juvenile characters actually reflecting a more basal position within Tyrannosauroidea.<ref name=holtz2004/> Another study omitted ''Alioramus'' altogether due to the only specimen's fragmentary nature.<ref name=carretal2005>{{cite_journal |last=Carr |first=Thomas D. |last2=Williamson |first2=Thomas E. |last3=Schwimmer |first3=David R. |year=2005 |title=A new genus and species of tyrannosauroid from the Late Cretaceous (middle Campanian) Demopolis Formation of Alabama |journal=Journal of Vertebrate Paleontology |volume=25 |issue=1 |pages=119–143 |url=http://www.bioone.org/perlserv/?request=get-abstract&doi=10.1671%2F0272-4634(2005)025%5B0119%3AANGASO%5D2.0.CO%3B2 |doi=10.1671/0272-4634(2005)025[0119:ANGASO]2.0.CO;2}}</ref> The discovery of ''A. altai'' in 2009 confirmed the placement of the genus within the Tyrannosaurinae.<ref name=SLBetal09/>
[[File:Alioramus skull steveoc.png|thumb|''A. remotus'' skull diagram, known portions in white]]
[[File:Endocranial cast of Alioramus altai.png|thumb|Endocranial cast of ''Alioramus altai'']]
''Tarbosaurus'' and ''Alioramus'' shared several skull features, including a locking mechanism in the lower jaw between the dentary and angular bones, and both lacked the prong of the nasal bones which connected to the [[lacrimal bone]]s in all other tyrannosaurids except adult ''Daspletosaurus''. The two genera may be closely related, representing an Asian branch of the Tyrannosauridae.<ref name=hurumsabath2003/><ref name=currieetal2003/> Some specimens of ''Tarbosaurus'' have a row of bumps on the nasal bones like those of ''Alioramus'', although much lower. The long and low shape of the only known ''Alioramus remotus'' skull indicated that it was immature when it died and might even have been a juvenile ''Tarbosaurus'', which lived in the same time and place. The more prominent nasal crests and much higher tooth count of ''Alioramus'', however, suggested it was a separate taxon, even if it is known only from juvenile remains,<ref name=currie2003a/> confirmed by the discovery of ''A. altai''.<ref name=SLBetal09/> Specimens identified as immature ''Tarbosaurus'' have the same tooth count as adults.<ref name=maleev1955>{{cite_journal |last=Maleev |first=Evgeny A. |authorlink=Evgeny Maleev |year=1955 |title=New carnivorous dinosaurs from the Upper Cretaceous of Mongolia |journal=[[Doklady Akademii Nauk SSSR]] |volume=104 |issue=5 |pages=779–783 |language=Russian}}</ref><ref name=currie2003b>{{cite_journal |last=Currie |first=Philip J. |authorlink=Phil Currie |year=2003 |title=Allometric growth in tyrannosaurids (Dinosauria: Theropoda) from the Upper Cretaceous of North America and Asia |journal=Canadian Journal of Earth Sciences |volume=40 |issue=4 |pages=651–665 |doi=10.1139/e02-083}}</ref>

Recently a cladogram has been published finding ''Alioramus'' just outside Tyrannosauridae. Below is the cladogram by Loewen (2013).<ref name=Loewen13>{{Cite journal | last1 = Loewen | first1 = M.A. | authorlink = Mark Loewen| last2 = Irmis | first2 = R.B. | authorlink2 = Randall B. Irmis| last3 = Sertich | first3 = J.J.W. | authorlink3 = Joseph Sertich| last4 = Currie | first4 = P. J. | authorlink4 = Philip J. Currie| last5 = Sampson | first5 = S. D. | authorlink5 = Scott D. Sampson| year = 2013| title = Tyrant Dinosaur Evolution Tracks the Rise and Fall of Late Cretaceous Oceans | url = http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0079420| editor-last = Evans | editor-first = David C| editor-link = David C. Evans| journal = [[PLoS ONE]] | volume = 8 | issue = 11 | pages = e79420 | doi = 10.1371/journal.pone.0079420 | pmid =  24223179| pmc = 3819173| ref = {{sfnRef|Loewen ''et al.''|2013}}}}</ref>

{{clade| style=font-size:100%; line-height:100%
|1={{clade
   |1=''[[Dilong paradoxus]]''
   |2={{clade
      |1=''[[Eotyrannus lengi]]''
      |2={{clade
         |1=''[[Bagaraatan ostromi]]''
         |2={{clade
            |1=''[[Raptorex kriegsteini]]''
            |2={{clade
               |1=''[[Dryptosaurus aquilunguis]]''
               |2={{clade
                  |1={{clade
                     |1=''[[Alectrosaurus olseni]]''
                     |2=''[[Xiongguanlong baimoensis]]'' }}
                  |2={{clade
                     |1=''[[Appalachiosaurus montgomeriensis]]''
                     |2={{clade
                        |1={{clade
                           |1='''''Alioramus altai'''''
                           |2='''''Alioramus remotus''''' }}
                        |2=[[Tyrannosauridae]] }} }} }} }} }} }} }} }} }}

==Discovery and naming==
[[File:Alioramus, Tyrannosauridae. Late Cretaceous (68mya) from Mongolia.jpg|thumb|Skull at Wyoming Dinosaur Center, Thermopolis]]
The [[holotype]] ([[Paleontological Institute of Russian Academy of Sciences|PIN]] 3141/1) of ''Alioramus'' is a partial skull associated with three [[metatarsal]]s. A joint [[USSR|Soviet]]-Mongolian expedition to the [[Gobi Desert]] in the early 1970s found these remains at a locality known as Nogon-Tsav in the Mongolian province of [[Bayankhongor Province|Bayankhongor]]. ''Alioramus'' was named and described by [[Russia]]n paleontologist Sergei Kurzanov in 1976. Its crests and low skull profile looked so different from other tyrannosaurids that Kurzanov believed his find was far removed from other members of the family. Accordingly, he gave it the generic name ''Alioramus'', derived from the [[Latin]] ''alius'' ('other') and ''ramus'' ('branch'), and the specific name ''A. remotus'', which means 'removed' in Latin.<ref name=kurzanov1976/> ''Alioramus'' is known from the  holotypes of ''A. remotus''<ref name=holtz2004/> and ''A. altai''.<ref name=SLBetal09/>

==Paleoecology==
[[File:Map mn bayankhongor aimag.png|left|thumb|[[Bayankhongor Province|Bayankhongor]], the [[Mongolia]]n [[Aimags of Mongolia|aimag]] (province) where ''Alioramus'' remains were discovered]] 
The Beds of Nogon-Tsav are considered to be the same age as the [[Nemegt Formation]].<ref name=kurzanov1976/> This [[geologic formation]] has never been [[radiometric dating|dated radiometrically]], but the [[fauna]] present in the fossil record indicate it was probably deposited during the [[Maastrichtian]] [[faunal stage|stage]], at the end of the [[Late Cretaceous]].<ref name=jerzykiewiczrussell1991>{{cite_journal |last=Jerzykiewicz |first=Tomasz |first2=Dale A.|last2=Russell|authorlink2=Dale Russell |year=1991 |title=Late Mesozoic stratigraphy and vertebrates of the Gobi Basin  |journal=Cretaceous Research |volume=12 |issue=4 |pages=345–377 |doi=10.1016/0195-6671(91)90015-5}}</ref>

The Maastrichtian stage in Mongolia, as preserved in the Nemegt Formation and at Nogon-Tsav, was characterized by a wetter and more humid climate compared with the semi-arid environment preserved in the earlier, underlying [[Barun Goyot Formation|Barun Goyot]] and [[Djadochta Formation]]s. Nemegt sediments preserve [[floodplain]]s, large [[river]] channels and [[Paleosol|soil deposits]], but [[caliche (mineral)|caliche]] deposits indicate periodic droughts.<ref name=osmolska1997>{{cite_book |last=Osmólska |first=Halszka |authorlink=Halszka Osmólska |year=1997 |chapter=Nemegt Formation |title=The Encyclopedia of Dinosaurs |editor=[[Phil Currie|Currie, Philip J.]] |editor2=Kevin Padian |location=San Diego |publisher=Academic Press |isbn=0-12-226810-5|pages=471–472}}</ref> This environment supported a more diverse and generally larger dinosaur fauna than in earlier times. Kurzanov reported that other theropods, including ''Tarbosaurus'', [[ornithomimosaur]]s and [[therizinosaur]]s were discovered at the same locality,<ref name=kurzanov1976/> but these remains have never been reported in detail. If the Nogon Tsav [[fauna]] was similar to that of the Nemegt Formation, [[troodontid]] theropods, as well as [[pachycephalosaur]]s, [[ankylosaurid]]s and [[hadrosaur]]s would also have been present.<ref name=jerzykiewiczrussell1991/> [[Titanosauria]]n [[sauropod]]s were also potential prey for predators in the Nemegt.<ref name=hurumsabath2003/>

==See also==
{{Portal|Dinosaurs}}
* [[Timeline of tyrannosaur research]]

==References==
{{Reflist}}

==External links==
*[https://web.archive.org/web/20070110004450/http://www.paleograveyard.com/alioramus.html Skull image of ''Alioramus''] at [https://web.archive.org/web/20070326170159/http://paleograveyard.com/ The Grave Yard]
*[http://www.pnas.org/content/106/41/17261/F3.large.jpg Illustration showing the pieces found of ''Alioramus altai''.]

{{Tyrannosauroidea}}

{{taxonbar}}

[[Category:Late Cretaceous dinosaurs of Asia]]
[[Category:Fossil taxa described in 1976]]
[[Category:Taxa named by Sergei Kurzanov]]
[[Category:Maastrichtian life]]
[[Category:Alioramini]]
[[Category:Nemegt fauna]]