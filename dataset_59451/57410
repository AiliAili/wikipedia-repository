'''Index Copernicus''' (IC) is an online [[database]] of user-contributed information, including scientist [[User profile|profiles]], as well as of scientific [[institution]]s, [[publication]]s and [[project]]s established in 1999 in Poland, and operated by Index Copernicus International.  The database, named after [[Nicolaus Copernicus]] (who triggered the [[Copernican Revolution]]), has several assessment tools to track the impact of scientific works and publications, individual scientists, or research institutions. In addition to the productivity aspects, IC also offers the traditional [[Abstract (summary)|abstracting]] and [[Bibliographic index|indexing]] of scientific publications. 

== Origins ==
The Index Copernicus aimed to offer an alternative to the English language dominance of publication indexing systems. The enterprise was co-financed by the [[European Regional Development Fund]] under the name: "Electronic Publishing House of Scientific Journals system of Index Copernicus Ltd."{{cn|date=July 2016}}

== Controversy ==
IC's journal ranking system was criticized in 2013 by [[Jeffrey Beall]] because of the high proportion of [[Predatory open access publishing|predatory journals]] included in it and its suspect evaluation methodology; he characterized the resulting "IC Value" as "a pretty worthless measure".<ref name=Beall>{{cite web|url=http://scholarlyoa.com/2013/11/21/index-copernicus-has-no-value/ |title=Index Copernicus Has No Value |work=Scholarly Open Access |author=[[Jeffrey Beall]] |date=2013-11-21 |accessdate=2014-06-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20140403010813/http://scholarlyoa.com/2013/11/21/index-copernicus-has-no-value/ |archivedate=2014-04-03 |df= }}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.indexcopernicus.com/}}


[[Category:Scientific databases]]
[[Category:Online databases]]
[[Category:Polish websites]]
[[Category:1999 establishments in Poland]]
[[Category:Citation metrics]]
[[Category:Citation indices]]


{{Publishing-stub}}
{{Poland-stub}}