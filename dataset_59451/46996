{{Infobox company
| name             = Chicago Pneumatic
| logo             = [[File:Chicago Pneumatic CP Full Logo.png|200px]]
| type             = [[Privately held company|Private]]
| industry         = Mechanical or Industrial Engineering
| foundation       = Chicago, Illinois, U.S. ({{Start date|1894}})
| founder          = John W. Duntley
| area_served      = Worldwide
| parent           = [[Atlas Copco|Atlas Copco AB]]<ref>{{cite web|url=http://aggregatesandminingtoday.com/atlas-copco-focus-on-chicago-pneumatic-brand,2009-3,508,0,0,current-news.aspx |title=Atlas Copco Focus on Chicago Pneumatic Brand - A & M Today |publisher=Aggregatesandminingtoday.com |date=2009-03-01 |accessdate=2013-01-27}}</ref><ref>{{cite web|url=http://www.cp.com/usen/careers/ |title=Help build things that last - Chicago Pneumatic United States |publisher=Cp.com |date=2008-10-03 |accessdate=2013-01-27}}</ref>
| subsid           = KTS Co., Ltd.<ref>{{cite web|url=http://www.autoserviceworld.com/news/chicago-pneumatic-acquires-kts/1000072801/ |title=Chicago Pneumatic Acquires KTS &#124; Automotive Service World |publisher=Autoserviceworld.com |date= |accessdate=2013-01-27}}</ref><ref>{{cite web|url=http://www.tyrepress.com/index.php?news=12064 |title=Chicago Pneumatic Buys Japanese Tool Manufacturer |publisher=Tyrepress.com |date= |accessdate=2013-01-27}}</ref>
| products         = {{unbulleted list|[[Industrial tools]]|[[Maintenance tools]]|[[Workshop Equipment]]|[[Construction tools]]|[[Light towers]]|[[Compressors]]|[[Electric generator|Generator]]s}}
| homepage         = {{URL|www.cp.com}}
}}
'''Chicago Pneumatic''', also known as "'''CP'''", is an industrial manufacturer providing power tools, air compressors, generators, light towers and hydraulic equipment. Products are sold in more than 150 countries through a worldwide distribution network. CP is active on markets such as tools for industrial production, vehicle service, maintenance repair operation for mining, construction, infrastructure equipment.

==History==
Chicago Pneumatic is a brand name in the pneumatic tool industry with a history traced back to 1889. John W. Duntley had in mind the idea of sourcing and selling construction tools "that weren’t yet available". In 1894, he established the Chicago Pneumatic Tool Company,<ref>{{cite web|url=http://www.cp.com/usen/whoweare/ourmission/ |title=Over a century of excellence - Chicago Pneumatic United States |publisher=Cp.com |date=2008-10-03 |accessdate=2013-01-27}}</ref> with an office in [[Chicago]]. The first plant to begin manufacturing product specifically for CP was the [[Joseph Boyer|Boyer]] Machine Shop in [[St. Louis County, Missouri|St. Louis]], Missouri.<ref name=Cplivre>{{cite book|last=Rosbrook|first=David|title=Chicago Pneumatic, The First Hundred Years|year=2001}}</ref> In 1901, Duntley met steel magnate [[Charles M. Schwab]], who invested heavily in the company. On December 28, the company was incorporated and the first single-valve pneumatic hammer was patented.<ref name=CpS>{{cite web|title=Chicago Pneumatic, decades of innovation|url=http://www.cp.com/en/whoweare/ourmission/decadesofinnovation/index.aspx}}</ref>

1904 was the year of expansion for CP.  Offices were opened in England, Canada, and Germany; and new lines of products had been developed such as air tools and rock drills.

In 1912, CP began to produce an horizontal 2-cycle semi-Diesel oil engine<ref name=CpSD>{{cite web|title=Chicago Pneumatic, a brief history of the Franklin plant, 1901 to the Fifties |url=http://www.energymachinery.com/HTML/CPH.html |deadurl=yes |archiveurl=https://web.archive.org/web/20120411072755/http://www.energymachinery.com:80/HTML/CPH.html |archivedate=2012-04-11 |df= }}</ref> to power CP compressors. One year later, CP finalized the Simplate valve; it deleted valve gear, offered controllability with high speeds and brought more capacity.

In 1925, CP manufactured the Benz Diesel engine<ref name=CpBZ>{{cite web|title=Franklin, Pennsylvania; Chicago Pneumatic Tool Company|url=http://venango.pa-roots.com/franklinchicagopneumatictool.html}}</ref> that was used in various racing cars in Europe at that time. The same year, CP began manufacturing rotary oil-well drilling equipment.

In 1939, CP designed and manufactured the world’s first impact wrench, both pneumatic and electric versions.

CP developed the “hot dimpling machine” in response to war effort demands, a device heating rivets to 1000&nbsp;°F and using 100,000 pounds/inch² of pressure to squeeze the rivet head into its final shape.
[[File:Evolution of CP Logo since 1904.jpg|thumb||left|Evolution of Chicago Pneumatic Logo since 1904]]
In 1943, [[The Saturday Evening Post]] published a cover picture by [[Norman Rockwell]] portraying a female aircraft worker, [[Rosie the Riveter#Saturday Evening Post|Rosie the Riveter, eating her lunch]] with a CP riveting hammer in her lap.<ref name=CpRH>{{cite web | title=Chicago Pneumatic rivet hammer|url=http://amhistory.si.edu/militaryhistory/collection/object.asp?ID=662}}</ref>

The 1950s and 1960s were an era of performance research. CP drill bits broke depth records approaching 20,000 feet and were used in oil prospecting. The portable broach puller for aircraft rivets was introduced in 1957.<ref name="Cplivre" />  A Chicago Pneumatic electric motor played a role in the U.S. [[Apollo 11|Apollo space mission]] to the Moon.<ref name="CpS" /> It powered a pump that inflated three bags on the capsule upon its splashdown in the Pacific Ocean on July 24, 1969. The bags ensured the escape hatch was on top and the astronauts could open it safely.  CP introduced in 1969 the world’s first speed ratchet “CP728” at [[Ford Motor Company]].

In 1970, the CP611 impact wrench was used in the steel erection phase of the [[Construction of the World Trade Center|World Trade Centers]] (New York City). Sold for several years into industrial markets, CP torque impact wrenches were introduced in the 1970s into the automotive market. In 1987, Chicago Pneumatic became part of the Swedish conglomerate [[Atlas Copco]]. During 1988, more new products were launched than at any time since the late '70s, such as screwdrivers, assembly tools and new ratchet wrenches. The following year, the current logo was designed and adopted.

In 1990, CP won a silver award from the AMA with its "23 parts" advertising campaign.<ref name="Cplivre" /> The advertisement portrayed how over 250 light assembly tools could be made from only 23 interchangeable component parts. In 1994, the production of compactors and portable power generators began.

2007 marked the inauguration of a new technology center in [[Nantes]], France.

In 2010, a new global design highlighting the brand colors&nbsp;— red and black&nbsp;— was adopted.

==Organization structure and activities==
===CP Power Tools===
The CP Tools main center for power tools is the Technocenter in Nantes (France), where over 100 people work in R&D. Teams research, develop, design, build prototypes, test, and certify industrial tools on the market.  Industrial (Metalworking and Energy) and vehicle service tools are also manufactured in [[Budapest]], Hungary, [[Qingdao]], China, [[Nazik, India|Nazik]], India, [[Osaka]], Japan, and [[Clifton, New Jersey]], United States.

Two distribution facilities provide the world market with powertools products; [[Charlotte, North Carolina]] that serves the United States and Canadian markets, and [[Hoeselt]], Belgium, for the rest of the world. Two Logistic hubs are located in France and China.

===CP Construction===
The main product marketing functions are located in [[Essen]], Germany, but design, development and production are located in facilities worldwide.  In Essen, specialized teams design and produce medium-size to heavy rig-mounted hydraulic breakers, from 550&nbsp;kg (1,212&nbsp;lb.) up to 4,200&nbsp;kg (9,259&nbsp;lb.) The light compaction equipment and the handheld hydraulic construction equipment are developed in the [[Ruse, Bulgaria|Rousse]] plant in Bulgaria.

Plants in [[Kalmar]], Sweden), and Nasik, India, collaborate closely on product design, prototypes and testing of the pneumatic handheld tools. Two distribution centers, located in Texas (U.S.) and Hoeselt (Belgium) provide products to distributors worldwide.

===CP Compressor===
The main centers are located in [[Antwerp]], Belgium, for compressors and [[Zaragoza]], Spain, for generators. Production is located in facilities worldwide. Small and medium-sized stationary compressors are produced in Sweden, Belgium, Italy, Brazil, Russia, China, India and the United States, whereas Brazil, Italy, Russia, India, China and the U.S. specialize in the piston range. The portable compressors are manufactured in Belgium, Brazil, China, India and the United States. Generators are mainly manufactured in Spain and the United States.

The company [[Neuman & Esser]] acquired the Chicago Pneumatic Tool Co. [[reciprocating compressor]] technology in 1993. Along with Chicago Pneumatic Tool the customer service organization NEAC Compressor Service is owner of eight other brands.<ref name="News-Herald 2008">City bids farewell to remaining ties to CPT, In: The News-Herald: Monday, Feb. 11, 2008, p. 1 and 5</ref>

==Products==
{| class="wikitable"
|-
! Power Tools<ref name=CpPT>{{cite web|title=CP Power Tools|url=http://www.cp.com/en/whatwedo/powertools/}}</ref> !! Workshop Equipment<ref name=CpWSE>{{cite web|title=CP Workshop Equipment|url=http://www.cp.com/en/whatwedo/workshop-equipment/}}</ref> !! Construction Tools<ref name=CpCST>{{cite web|title=CP Construction|url=http://www.cp.com/en/whatwedo/constructiontools/}}</ref>  !! Compressor Technique<ref name=CpCSC>{{cite web|title=CP Stationary Compressors|url=http://www.cp.com/en/whatwedo/stationarycompressors/}}</ref><ref name=CpCPC>{{cite web|title=CP Portable Compressors|url=http://www.cp.com/en/whatwedo/portablecompressors/}}</ref>
|-
| Impact Wrenches || Air Hydraulic Jacks || Handheld Pneumatic Tools || Stationary Compressors
|-
| Ratchet Wrenches || Trolley Jacks || Handheld Hydraulic Equipment || Portable Compressors
|-
| Screwdrivers || Bottle Jacks || Handheld Petrol Equipment || Generators
|-
| Drills || Jack Stands || Compaction Equipment ||  Light Towers
|-
| Grinders || Single Wheel Dolly || Rig-mounted Hydraulic Breakers  || 
|-
| Sanders & Polishers || Workshop Presses || Rig-mounted Hydraulic Compactors  || 
|-
| Hammers & Scalers || Work Lights || ||
|-
| Riveters ||Cordless Tools || ||
|-
| Specialty Tools || || ||
|-
| Superior Bolting Tools TITAN || || ||
|}

CP products were used on many famous construction projects,<ref name="CpS" /><ref name=CpFH>{{cite web|title=Fraserburgh Heritage Center; Consolidated Pneumatic Tool Co. Ltd|url=http://www.fraserburghheritage.com/default.asp?page=15}}</ref> among which are:

<gallery>
File:Empire_State_Building_from_the_Top_of_the_Rock.jpg|[[Empire State Building|Empire State Building, New York City]]
File:GoldenGateBridge-001.jpg|[[Golden Gate Bridge|Golden Gate Bridge, San Francisco]]
File:Lincolntunnel-1955.jpg|[[Lincoln Tunnel|Lincoln Tunnel, New York City]]
File:Hell_Gate_and_Triborough_Bridges_New_York_City_Queens.jpg|[[Triborough Bridge|Triborough Bridge, New York City]]
File:HooverDamFrontWater.jpg|[[Hoover Dam|Boulder Dam, Arizona]]
File:Grand_Coulee_Dam.jpg|[[Grand Coulee Dam|Grand Coulee Dam, Washington]]
File:Sydney_Harbour_Bridge_from_Circular_Quay.jpg|[[Sydney Harbour Bridge|Sydney Harbour Bridge, Australia]]
</gallery>

==Distribution==
Chicago Pneumatic products are sold exclusively through authorized distributors worldwide. The distributor network consists of specialized distributors in construction, demolition, vehicle service, industrial maintenance, public works and general industry.

==References==
{{Reflist}}

==Further reading==
* [http://www.fraserburghheritage.com/default.asp?page=15 Consolidated Pneumatic Tool Co. Ltd]
* [http://www.nber.org/books/jero34-1 Mechanization in Industry, Harry Jerome, 1934]

== External links ==
* [http://www.cp.com/en Official site]
* [http://www.cp-nolimits.com Cordless Tools site]
* [http://www.titanti.com TITAN site]
* [https://www.youtube.com/user/CPCHANNELS CP Youtube channel]
* [http://www.linkedin.com/company/chicago-pneumatic CP Linkedin company page]
* [https://web.archive.org/web/20120411072755/http://www.energymachinery.com:80/HTML/CPH.html A Brief History of the Franklin Plant]

{{Atlas Copco}}

[[Category:Power tool manufacturers]]