{{Infobox rail accident
| title            = 2002 Farragut derailment
| date             = September 15, 2002
| location         = [[Farragut, Tennessee|Farragut]], [[Tennessee]]
| country          = [[United States]]
| line             =
| operator         = [[Norfolk Southern Railway]]
| type             = Derailment, hazardous materials release
| trains           = 1
| pax              = 0
| deaths           = 0
| injuries         = 17
| image            = Farragut derailment 2.JPG
| caption          = Derailed cars block a roadway in Farragut, TN in 2002
| damage           = {{US$|1020000|2002}}
| coordinates      = {{coord|35|51|14|N|84|09|32|W|format=dms|display=inline, title|type:event_region:US-TN}}
| cause            = Failure to promptly investigate suspected track damage<ref name=ntsb />
}}

The '''2002 Farragut derailment''' occurred on the morning of Sunday, September 15, in [[Farragut, Tennessee]]. [[Norfolk Southern Railway|Norfolk Southern]] freight train 15T derailed 27 cars, resulting in the release of oleum or fuming [[sulfuric acid]]. Roughly 2,600 residents were evacuated from nearby homes for three days until hazardous materials crews were able to mitigate the scene.<ref name=ntsb >{{cite book|title=RAILROAD ACCIDENT BRIEF: DERAILMENT OF NORFOLK SOUTHERN TRAIN 15T AT FARRAGUT, TENNESSEE, SEPTEMBER 15, 2002|year=2003|publisher=National Transportation Safety Board|location=Washington, DC|pages=8|url=http://www.ntsb.gov/doclib/reports/2003/RAB0305.pdf}}</ref> No fatalities or major injuries were reported as a result of the derailment, but property damage and losses were calculated at US$1.02 million.

== Background ==
On the morning of the derailment, Norfolk Southern train 703 approached the [[Railroad switch|spring switch]] at the west end of the Boyd Siding and was given a signal restriction. The crew of 703 stopped in the siding and examined the spring switch at the east end of the siding, noting a 1/4-inch gap was present between the switch point and the stock rail. The crew manually operated the switch several times and noted that the switch still had a small 1/8-inch gap. The 703 engineer radioed Norfolk Southern dispatch to report the issue. Train 703 then proceeded through the switch without incident.

A maintenance person was sent to investigate the report, arriving at the site around 11:00&nbsp;AM EDT. He observed that the signals were clear and that the spring switch appeared to be properly aligned against the rail, likely having completely closed after train 703 passed through the switch earlier. The signal maintainer later told investigators that he felt the switch "looked like it could use a little oil".<ref name=ntsb />

The signal maintainer checked with the dispatcher to obtain a [[track warrant]] so that he could perform work on the active rail line. He was informed that two trains were approaching the siding and decided that once both trains passed the area he would obtain the warrant and perform an internal inspection of the switch. The signal maintainer was waiting in the immediate area of the switch as train 15T approached from the east and observed a clear signal on the switch.

== Derailment ==
At 11:20&nbsp;AM EDT, Norfolk Southern train 15T was traveling westbound at {{convert|38|mph|km/h|abbr=on}} when it passed through the eastern end of the Boyd Siding (normal track speed in the area is listed at 50&nbsp;mph). As the locomotive passed over the switch, the engineer stated that he felt a tug. The crew of 15T looked back and observed the train derailing. Of the 142 cars, two locomotives and 25 cars derailed. The crew immediately noted a white cloud rising from the derailed cars and radioed the dispatcher at the same time as the signal maintainer was calling the incident in via telephone.<ref name=ntsb />

Of the derailed cars, a flatbed car carrying two [[US Army]] tanks derailed. The 18th car of the train, a tank car carrying {{convert|10600|gal|l}} of oleum (fuming [[sulfuric acid]]) was crushed by other cars and began to leak its contents.<ref name=CBS>{{cite web|last=Esterbrook|first=John|title=Tenn. Derailment Forces Evacuations|url=http://www.cbsnews.com/news/tenn-derailment-forces-evacuations/|work=CBS News|publisher=Associated Press|accessdate=29 December 2013}}</ref>  The result of this spill was a large plume of acid vapors that began to envelop the area and drift southeast with the prevailing wind.
{| class="wikitable"
|-
| [[File:Farragut derailment 3.JPG|thumb|US Army tanks on a derailed flatbed car]]
 || [[File:Farragut derailment 5.JPG|thumb|Fire Department using a ground monitor to suppress vapors.]]
 || [[File:Farragut derailment 6.JPG|thumb|A crushed tank car involved in the derailment]]
|}

== Response ==
Responders from the [[Rural/Metro|Rural/Metro Fire Department]] and Knox County Sheriffs Office began to arrive within five minutes and met with the train crew. Once the train's [[consist]] was reviewed and the sulfuric acid identified, an [[Emergency evacuation|evacuation]] of a {{convert|4.4|sqmi|km2|adj=on}} area surrounding the derailment was undertaken. Roughly 2,600 residents were either evacuated or attempted to return to their homes and were told of the accident and denied entry.

As a result of incidental exposure to the toxic cloud, 15 residents and two law enforcement officers were treated and released from local hospitals for irritation.<ref>{{cite web|title=Oleum, Sulfur Trioxide, and Sulfuric Acid|url=http://www.aristatek.com/newsletter/DEC09/DEC09ts.pdf|accessdate=29 December 2013}}</ref>

== Aftermath ==
One report in the [[Federal Register]] in 2004 indicated that the [[Federal Railroad Administration]] recommended that in the presence of any condition that might impede safe passage of a train, the dispatcher should immediately issue a speed restriction to 20&nbsp;mph and that signal or track maintainers should be given priority on occupying track for the purpose of investigating a signal system or track component problem.<ref name="GPO report">{{cite web|last=Cothen Jr.|first=Grady|title=Notice of Safety Advisory 2004-02|url=http://www.gpo.gov/fdsys/pkg/FR-2004-08-10/html/04-18193.htm|work=Federal Register, Vol 69 Num 153|publisher=US Government Printing Office|accessdate=29 December 2013|date=10 August 2004}}</ref>

== References ==
{{reflist}}

{{2002 railway accidents}}

[[Category:Railway accidents in 2002]]
[[Category:Derailments in the United States]]
[[Category:Railway accidents in Tennessee]]
[[Category:Rail transportation in Tennessee]]
[[Category:Accidents and incidents involving Norfolk Southern Railway]]
[[Category:2002 in Tennessee]]