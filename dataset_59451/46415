{{Infobox military person
|name= Edward André Gabriel Barrett
|birth_date= {{birth date|1827|02|04}}
|birth_place = [[New Orleans]], [[New Orleans]], U.S.
|birth_name=Edward André Gabriel Barrett
|death_date = {{death date and age|1880|03|31|1827|02|04}}
|death_place= [[New York City]], [[New York (state)|New York]], U.S.
|image=
|caption=  
|nickname=
|allegiance= {{flag|United States|name=United States of America|1851|size=23px}}
|branch= {{flag|United States Navy|1851|size=23px}}
|serviceyears= 1840–1877
|rank=[[Commodore (rank)|Commodore]]
|commands={{USS|Quinnebaug|1866|6}}<br />{{USS|Portsmouth|1843|6}}<br/>{{USS|Savannah|1842|6}}<br/>{{USS|Catskill|1862|6}}<br/>{{USS|Canandaigua|1862|6}}<br/>{{USS|Massasoit|1863|6}}<br/>{{USS|Ticonderoga|1814|6}}<br/>{{USS|Plymouth|1844|6}}
|battles=[[American Civil War]]<br />[[Mexican–American War|Mexican-American war]]
|spouse =Palmira De Ribrocchi
|relations = {{plain list|
*Thomas Barrett (father)
*Marie Henriette Griffon d’Anneville (mother)
*Giuseppe Alfredo Barrett (son)
*Paula Jenny Barrett (daughter)
*Virginia M. Elena Barrett (daughter)
*Camillo Barrett (son)
}}}}
[[Commodore (United States)|Commodore]] '''Edward André Gabriel Barrett''', [[United States Navy]], was born in [[New Orleans]], [[Louisiana]]  on 4 February 1827 and died of [[malaria]] in [[New York City]] on 31 March 1880.<ref>[https://query.nytimes.com/gst/abstract.html?res=9F0DE2DC1F31EE3ABC4C53DFB266838B699FDE Burial of Commodore Barrett -  New York Times]</ref> He was buried in the [[Holy Cross Cemetery, Brooklyn|Holy Cross Catholic Cemetery]] in [[New York City]]. He was a member of the [[Military Order of the Loyal Legion of the United States]] (MOLLUS) <ref>{{cite web|url=http://suvcw.org/mollus/orgmem/OrgmemB.htm|title=MOLLUS--Original Companions, Civil War Officers, Surnames Beginning with the Letter B|work=suvcw.org|accessdate=12 August 2015}}</ref><ref>{{cite web|url=http://suvcw.org/mollus/compan1.htm|title=MOLLUS--Prominent Companions of the MOLLUS|work=suvcw.org|accessdate=12 August 2015}}</ref> and participated as a protagonist, in an active way to the [[American Civil War]], to the development of world power the US and its military fleet, to fight slavery .

==Early life and naval career==
The second of six children, he married the Noble Palmira De Ribrocchi of [[Tortona|Tortona, Piedmont]], Italy, in 1850 in [[Genoa]]. Palmira was the daughter of the Noble Giovanni Battista De Ribrocchi and Jousserandot Jeanne Francoise, of the Persange Barons.<ref name="auto">"Tortona insigne" - Un millennio di storia delle famiglie tortonesi - Copertina rigida – 1978 di Berruti Aldo edito dalla Cassa di Rispamio di Tortona;</ref> They had four children: Joseph Alfred, Paula Jenny, Virginia M. Elena and Camillo, who became a Garibaldian volunteer.<ref name="auto1">{{cite web | title = Camillo Barrett (1851-1924) | url = http://www.garibaldino-barrett.com | language = it |date= |accessdate=2015-08-01}};</ref> From an article in the ''Army and Navy Journal'',<ref>Army And Navy Journal April 3, 1880 - Pag 714</ref> He later remarried in America, probably only civilly and had two other children, who were respectively 8 and 3 years old at the time of his death (1880).{{citation needed|date=July 2016}}

Edward Barrett’s family, of Creole origins, was one of the most aristocratic of [[Louisiana]]. He descended on his mother side, from Marquis [[Louis Coulon de Villiers|De Villiers]], commander in chief of the French forces, to whom [[George Washington|Washington]] surrendered after [[Edward Braddock|Braddock]]’s defeat.([[Fort Necessity National Battlefield|Fort Necessity]]). His father, Thomas Barrett, was Collector of the Port of [[New Orleans]] for 15 years. This assignment was personally given to him by [[Andrew Jackson|President Jackson]]. His mother, Marie Henriette Griffon d’Anneville, was from New Orleans.

He entered the US Navy at the age of 13. On 3 November 1840 he was appointed (Aspiring) [[Sub-lieutenant]] and assigned to the [[Sloop-of-war|sloop]] {{USS|Warren|1827|6}} and {{USS|Levant|1837|6}} in the [[Pensacola Bay|Bay of Pensacola]]. He was later transferred aboard the [[frigate]] {{USS|Macedonian|1836|6}}, of the squadron of the West Indies, under [[William Nicholson Jeffers|Commodore Nickolson's]] command.

His first voyage was aboard USS ''Levant'', in the West Indies’ squadron. In July 1842 he was sent to hospital in [[Norfolk, Virginia]]. The following August he transferred to the frigate {{USS|Columbus|1774|2}} of the [[Mediterranean Squadron (United States)|squadron of the Mediterranean]]. Upon arriving at the base he was sent to the sloop {{USS|Preble|1839|2}} and then again to ''Columbus'' departing for Brazil.<ref name="auto2">The records of living officers of the U.S. navy and Marine corps - p 98</ref>

==Command assignments, 1847–1863==

In February 1846 he entered the Naval Academy of the United States and, in August 1847, was appointed [[Officer (armed forces)|Officer]] and immediately assigned to {{USS|Mississippi|1841|6}}, of the Gulf squadron.<ref name="auto2"/> Upon arrival in [[Veracruz (city)|Vera Cruz]] he served alternately on {{USS|Raritan|1843|6}} and, during the siege of the city, on {{USS|John Adams|1799|6}}. He took over from the defunct [[Sub-lieutenant]] Nelson in command of the Ambulance Corps working with the battery vessel. In 1847 he participated in the Battle of Alvarado aboard USS ''Mississippi''.<ref name="auto2"/> He then commanded the [[brigantine]] ''Cosa'': the first and best conquest made during the war and proceeded with it to New Orleans. Returning to his ship of origin, he captured ''Maria Theresa'', then moved back on ''Raritan'' and participated in the attack on Tuspan.

He served as a volunteer in the expedition of Frontera, of Tabasco and of Laguna. After the war he went home on sick leave.<ref name="auto2"/>

In 1848 he resumed service and was sent to the Coast of Africa as an officer aboard {{USS|Jamestown|1844|6}}. He remained in that position for about two years. He then returned to the Mediterranean aboard {{USS|Lexington|1825|6}}, where he obtained a two-year license: these were the years of his marriage, fatherhood and of his longest stay in Europe.

Barrett spent most of the [[Crimean War]] period with the Mediterranean Squadron.<ref name="auto2"/> The main purpose of the US Navy in the Mediterranean was to protect the interests of, as well as, the American citizens during the various revolutions that shook Europe in 1848. Many ships returned to [[Norfolk, Virginia]] in May 1850, when calm returned. In 1852 he reached the [[flagship]] of the Naval Squadron, {{USS|Cumberland|1842|6}} and then moved on to {{USS|St. Louis|1828|6}} and {{USS|Saranac|1848|6}}.

In 1854 he was appointed Commodore Breeze’s aide-de-flag. From 1855 to 1858 he was assigned to {{USS|Constellation|1854|6}} and {{USS|Congress|1841|6}} in the Mediterranean, and in 1858 he embarked on USS ''Constellation'', returning to the United States.<ref name="auto2"/> Towards the end of 1858 he served aboard {{USS|Portsmouth|1843|6}} on the African coast (to oppose the slave trade).

In 1860 he went,  aboard {{USS|Dacotah|1859|6}}, to the East Indies and at the beginning of 1861, having just arrived, was appointed Instructor of Artillery and organized, for volunteers, the [[training ship]] {{USS|Savannah|1842|6}} until 1863.<ref name="auto2"/>

==American Civil War, 1860–1865==
During the first phase of the conflict, Barrett took on the task of training officers and prepared two texts. The texts are still known in military literature. They are "Naval Howitzer"<ref name="auto3">"Naval Howitzer", Edward Barrett 1863, reprint of 2005 - Wind Canyon Books (Brawley-CA 92227)</ref> and "Gunnery Instructions"<ref name="google.it">{{cite web|url=https://books.google.it/books?id=1XNAqYosKyIC&printsec=frontcover&dq=edward+barrett&hl=it&sa=X&ved=0CD8Q6AEwBGoVChMIzYWm1faixwIViTI-Ch1G5g_1#v=onepage&q=edward%20barrett&f=false|title=GUNNERY INSTRUCTIONS|work=google.it|accessdate=12 August 2015}}</ref>

In 1862, after allegations due to his birth in the South, Barrett was tried by martial court but was declared innocent, and received praise for his professional and patriotic services.

Barrett commanded {{USS|Massasoit|1863|6}} in 1863 and {{USS|Catskill|1862|6}}, a {{sclass-|Passaic|monitor|1}}<ref>In the image of {{USS|Catskill|1862|6}}, we can see Commodore E. Barrett sitting at the controls of the ship (see: [http://usawc.summon.serialssolutions.com/search?s.q=edward+barrett U. S. Army War College Library and Archives]). Compare to [http://dp.la/item/b7d77a13a42cf5d873b6f15dfb0f9cd6 DPLA Digital Public Library Of America]</ref> outside [[Charleston, South Carolina|Charleston]].<ref>[http://www.amazon.com/Monitors-U-S-1861-1937-Naval-History/dp/B0060ZBGD4 Monitors of the U.S. Navy 1861-1937]. Paperback – 1969 by Naval History Div. US Navy (Author)</ref> he was assigned to this command directly by the Secretary of the Navy, Welles, replacing the previous Commander Rodgers, who had died in combat. The ship was quickly repaired and ready to resume service. They had to conquer Charleston, but, first of all, stop the illegal trade which financed the South.

In 1864 his ship captured {{ship|CSS|Prince Albert}}, a corsair [[schooner]] and on 18 February 1865, he captured the boat ''Celt'', while trying to force the blockade; the ship was secretly leaving Charleston on the night of 14 February.

USS ''Catskill'' also captured the British vessel ''Deer'' in similar circumstances; this was the last conquest made outside Charleston and the only conquest made by a Monitor. Both combatants considered the fall of Charleston disastrous for the fall of the South. Both Dahlgren (in two messages to Secretary Welles and [[David Dixon Porter|Admiral Porter]]) and Wilkinson admitted it: the city of cotton, symbol of the spirit of the South, had fallen into the Unionist hands and, with it, all the hopes of the Confederates ended.

This also tells us that Commodore Edward Barrett was a great manipulator, as well as a brave fighter. The fact that a " monitor " managed to capture a boat that fast, equipped to transport cotton and violate the blockade, is reported in several newspaper articles which appeared after his death<ref name="auto4">Army and Navy Journal April 10, 1880 - Pag 731</ref>

In 1865 he reorganized the Artillery Department of the Arsenal of Norfolk, Virginia and was appointed Inspector of Small Arms for the US Navy in 1866.<ref>GENERAL ORDERS AND CIRCULARS US Navy, pag 149</ref>

==Last years==
The same year he took command of {{USS|Quinnebaug|1866|6}} (the first warship that went into Cape Town, Africa), based in Brazil. In 1873 {{USS|Ticonderoga|1814|6}}, the following year {{USS|Canandaigua|1862|6}} and in 1875 {{USS|Plymouth|1867|6}}.

In addition to these services, it is worth recalling that Commander E. Barrett was assigned to the first warship which sailed into the harbor of Joliette in Marseille and on {{USS|Congress|1799|6}}, which was the first to enter the dam of [[Livorno]], Italy. It was the first shipment that remounted the Yang - tse - Kiang to Hankow.<ref>GENERAL ORDERS AND CIRCULARS US Navy, pag 145</ref>

He commanded USS ''Plymouth'', the ship sent by the Government to attend the closing of the International Expo. It was also the first to test the Eads jetties in the spring of 1877. In that period six sailors of USS ''Plymouth'' were awarded the Medal of Honor.<ref>see [[Benjamin Franklin Tilley|Tilley]] and [[Henry Clay Cochrane|Cochrane]]</ref> On the occasion of the July 1877 riots, (Labor strikes), Barrett was called to Washington, where he organized a brigade of naval sailors and marines for the protection of the city and public establishments and held the command of the first aid force until all danger had passed<ref name="auto4"/>

Edward Barrett wrote several works, still remembered and studied today including: “Temporary Fortifications”, “Dead Reckoning”, "Gunnery Instructions"<ref name="google.it"/> and "Naval Howitzer".<ref name="auto3"/>

In 1876, with the permission of the author, he supervised an anthology of the works of [[Thomas Carlyle]]<ref name="amazon.com">[http://www.amazon.com/The-Carlyle-Anthology-Edward-Barrett/dp/B001U9ZD34 The Carlyle Anthology Hardcover – 1876]</ref>

==Family==
Commodore E. Barrett was married to Palmira De Ribrocchi<ref name="auto"/>  and had four children:

*Giuseppe Alfredo Barrett 
*Paula Jenny Barrett 
*Virginia M. Elena Barrett 
*Camillo Barrett (1851 - 1924)<ref name="auto1"/>

==References==
{{Reflist|colwidth=30em}}

==Bibliography==
* "The United States Army and Navy Journal and Gazette of The Regular and Volunteer Forces", Vol. 17, Army and Navy Journal Incorporated, 1880
* [http://www.amazon.co.uk/gp/product/5876208884?keywords=9785876208880&qid=1439304391&ref_=sr_1_1&s=books&sr=1-1 "The records of living officers of the U.S. navy and Marine corps"] - L.R. Hamersly 
* "Tortona insigne" - Un millennio di storia delle famiglie tortonesi - Copertina rigida – 1978 di Berruti Aldo edito dalla Cassa di Rispamio di Tortona;
* "Naval Howitzer", Edward Barrett 1863, reprint of 2005 - Wind Canyon Books (Brawley-CA 92227)
* [https://books.google.it/books?id=1XNAqYosKyIC&printsec=frontcover&dq=edward+barrett&hl=it&sa=X&ved=0CD8Q6AEwBGoVChMIzYWm1faixwIViTI-Ch1G5g_1#v=onepage&q=edward%20barrett&f=false "GUNNERY INSTRUCTIONS",  Di LIEUT-COMMANDER EDWARD BARRETT, E'book]
* "General Orders and Circulars US Navy" 
* [http://www.amazon.com/Monitors-U-S-1861-1937-Naval-History/dp/B0060ZBGD4 "Monitors of the U.S. Navy 1861-1937"]. Paperback – 1969 by Naval History Div. US Navy (Author)
* "United Service", Vol. 1, 1879
* "Army and Navy Journal 1880, April 13, 1880"
* "Army and Navy Journal 1880, April 10, 1880"

==External links==
{{commons|Edward Gabriel Andrè Barrett}}
* [http://www.nytimes.com The New York Times]
* [http://suvcw.org/mollus/molid.htm MOLLUS Web Site]
* [http://dp.la/item/b7d77a13a42cf5d873b6f15dfb0f9cd6 DPLA Digital Public Library Of America]
* [http://www.cmdrbarrett.net Edward Gabriel André Barrett Web Site]

{{DEFAULTSORT:Barrett, Edward Gabriel Andre (Naval Officer)}}
[[Category:1880 deaths]]
[[Category:Edo period]]
[[Category:People from New Orleans]]
[[Category:United States Navy admirals]]
[[Category:1827 births]]
[[Category:United States Navy Commodores]]
[[Category:People from New York City]]
[[Category:19th-century American naval officers]]