The '''Economic History Society''' is a [[learned society]] that was established at the [[London School of Economics]] in 1926 to support the research and teaching of [[economic history]] in the [[United Kingdom]] and internationally. The society also acts as a pressure group working to influence government policy in the interests of history and economic affairs, alongside other societies and professional bodies with similar interests. In addition, the Society regularly liaises{{Clarify|date=April 2010}} with funding bodies such as [[Higher Education Funding Council for England]], the [[Arts and Humanities Research Council]], and the [[Economic and Social Research Council]].

==History==
The Economic History Society was established at a general meeting held at the [[London School of Economics]] on 14 July 1926. [[R. H. Tawney]] took the chair and, after the resolution to form the society had been carried unanimously, the meeting discussed the constitution and aims of the society and proceeded to elect its first officers, with Sir [[William Ashley (economic historian)|William Ashley]] as the first President. The publication of ''[[The Economic History Review]]'' was also discussed and Tawney and Lipson were appointed as joint editors.

==Aims==
The objects of the Economic History Society as stated in its Constitution, are:
* Promote the study of economic and social history
* Establish closer relations between students and teachers of economic and [[social history]]
* To publish the ''Economic History Review''
* To publish and sponsor other publications in the fields of economic and social history
* To hold an annual conference and to hold or participate in any other conference or meeting as may be deemed expedient in accordance with (a) and (b) above
* To co-operate with other organisations having kindred purposes

== See also==
* [[Historiography of the United Kingdom]]

==External links==
*{{Official website|http://www.ehs.org.uk/}}
*[http://archives.lse.ac.uk/TreeBrowse.aspx?src=CalmView.Catalog&field=RefNo&key=ECONOMIC%20HISTORY%20SOCIETY Catalogue of the Economic History Society papers at LSE Archives]

[[Category:Learned societies of the United Kingdom|Economic History Society, The]]
[[Category:Organizations established in 1926|Economic History Society, The]]
[[Category:1926 establishments in the United Kingdom]]
[[Category:Economic history societies]]