{{Infobox journal
| title = George Mason Law Review
| image = 
| discipline = Law Review
| language = English
| former_name = International School of Law Review
George Mason Independent Law Review
| abbreviation = Geo. Mason L. Rev.
| publisher = William S. Hein & Co., Inc.
| country = United States
| frequency = 5 issues per year
| history = 1976 - present
| website = http://www.georgemasonlawreview.org/
| ISSN = 1068-3801
}}

{{Italic title}}
The '''''George Mason Law Review''''' is a traditional student-run [[law review]]<ref>[http://georgemasonlawreview.org The George Mason Law Review Homepage]</ref> at [[Antonin Scalia Law School]] at [[George Mason University]]. It is the flagship law review of the Antonin Scalia Law School.

== History ==
The [[Antonin Scalia Law School]] at [[George Mason University]] was formerly the International School of Law, whose student-run publication, the ''International School of Law Review'' began in 1976. When the school became George Mason School of Law in 1979, at which time the publication became the ''George Mason University Law Review''. In 1992, the student-run law review split with the law school's administration, publishing as the ''George Mason Independent Law Review''. During this time the ''George Mason Independent Law Review'' maintained a traditional law review format by publishing both professional and student works. While another law review under the name ''George Mason University Law Review'' only published articles written by the students of the law school. In the Fall of 1995, pursuant to an agreement with the dean of the law school, the two law reviews merged and ''George Mason Independent Law Review'' began operating as the modern ''George Mason Law Review''.

== Membership ==
Membership on the law review is granted through a competitive process. First-year students must participate in a Write-On competition<ref>{{Cite web|url=http://www.georgemasonlawreview.org/write-on-competition/|title=Write-On Competition – George Mason Law Review|website=www.georgemasonlawreview.org|language=en-US|access-date=2017-03-24}}</ref> after completing their final exams in the spring semester. The Law Review then considers the student's first-year grades and performance in the write on to make offers of membership. The George Mason Law Review offers membership to somewhere between 20 percent of the first year law school class.

== George Mason Law Review Annual Symposium on Antitrust ==
Each year, the review holds a symposium on Antitrust Law, held in Washington, DC.<ref>[http://www.law.gmu.edu/gmulawreview/symposium/ George Mason Law Review Antitrust Symposium]</ref> The symposium consists of a keynote address by a leader in the field of antitrust law followed by panels discussing aspects of antitrust law. Past speakers have included [[Joshua D. Wright]] and [[J. Thomas Rosch]], Commissioners, [[Federal Trade Commission]], Thomas O. Barnett, Assistant Attorney General, Antitrust Division, [[U.S. Department of Justice]], [[Neelie Kroes]], European Commissioner for Competition, and others. The Law Review publishes an issue dedicated to the symposium.

==Notable articles==
Notable articles published in the ''George Mason Law Review'' include:
*[[Jerry A. Hausman|Hausman, J. A.]], and G. K. Leonard. “Economic Analysis of Differentiated Products Mergers Using Real World Data.” Geo. Mason L. Rev. 5 (1996): 321.
*Nelson, R. H. “Privatizing the Neighborhood: A Proposal to Replace Zoning with Private Collective Property Rights to Existing Neighborhoods.” 7 Geo. Mason L. Rev. 827 (1998).
*[[Carl Shapiro|Shapiro, C.]] “Exclusivity in Network Industries.” 7 Geo. Mason L. Rev. 673 (1998).
*[[John Yoo|Yoo, J.]] "[http://www.law.gmu.edu/assets/subsites/gmulawreview/files/14-3/Documents/Yoo.pdf The Terrorist Surveillance Program and the Constitution."] 14 Geo. Mason L. Rev. 565 (2007).

==References==
{{Reflist}}

[[Category:American law journals]]
[[Category:General law journals]]
[[Category:George Mason University academic journals]]
[[Category:Law journals edited by students]]
[[Category:Publications established in 1976]]