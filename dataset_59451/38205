{{good article}}
{{Use British English|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox University Boat Race
| name= 31st Boat Race
| winner = Cambridge
| margin = 3 and 1/2 lengths
| winning_time= 22 minutes 35 seconds
| overall = 15–16
| umpire = [[Joseph William Chitty]]<br>(Oxford)
| date= {{Start date|1874|3|28|df=y}}
| prevseason= [[The Boat Race 1873|1873]]
| nextseason= [[The Boat Race 1875|1875]]
| reserve_winner = 
| women_winner = 
}}

The '''31st Boat Race''' took place on the 28 March 1874. [[The Boat Race]] is an annual [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  In a race umpired by former Oxford rower [[Joseph William Chitty]], Cambridge won by three and a half lengths in their fifth consecutive victory.

==Background==
[[File:SirJosephWilliamChitty.jpg|right|upright|thumb|[[Joseph William Chitty]] was the umpire for the 1874 Boat Race.]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 4 December 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 3 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 4 December 2014 | publisher = The Boat Race Company Limited}}</ref>  Cambridge went into the race as reigning champions, having defeated Oxford by three lengths in the [[The Boat Race 1873|previous year's race]], while Oxford led overall with sixteen wins to Cambridge's fourteen.<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher= The Boat Race Company Limited | title = Boat Race – Results| accessdate = 4 December 2014}}</ref>

Cambridge were coached by John Graham Chambers (who rowed for Cambridge in the [[The Boat Race 1862|1862]] and [[The Boat Race 1863|1863 races]], and was non-rowing boat club president for the [[The Boat Race 1865|1865 race]]),<ref name=burn104>Burnell, p. 104</ref> William Henry Lowe (who rowed for Cambridge in the [[The Boat Race 1868|1868]], [[The Boat Race 1870|1870]] and [[The Boat Race 1871|1871 races]]) and [[John Goldie (barrister)|John Goldie]] (the Cambridge boat club president and rower for the [[The Boat Race 1869|1869]], 1870 and 1871 races).<ref name=burn104/>  Oxford's coach was S. D. Darbishire (who rowed for the Dark Blues in 1868, 1869 and 1870).<ref>Burnell, p. 97</ref>

Cambridge opted not to use the boat built for them by [[Harry Clasper]] specifically for the race, in favour of one constructed by Waites which had been used by [[Trinity College Boat Club|1st Trinity Boat Club]].<ref name=drink67>Drinkwater, p. 67</ref> The race was umpired by [[Joseph William Chitty]] who had rowed for Oxford twice in 1849 (in the [[The Boat Race 1849 (March)|March]] and [[The Boat Race 1849 (December)|December races]]) and the [[The Boat Race 1852|1852 race]], while the starter was Edward Searle.<ref>Burnell, pp. 49, 97</ref>

==Crews==
The Cambridge crew weighed an average of 11&nbsp;[[Stone (unit)|st]] 10.625&nbsp;[[Pound (mass)|lb]] (74.5&nbsp;kg), {{convert|1.5|lb|kg|1}} more than their opponents.<ref name=burn60/>  Oxford saw the return of two former [[Blue (university sport)|Blues]] in William Edward Sherwood and A. W. Nicholson (who was rowing in his third Boat Race).  Cambridge's crew included five Blues, with James Brooks Close and Charles Stokes Read returning for a third time.<ref name=burn60/>  

The Light Blue crew included Australian George Francis Armytage, the only non-British participant in the race, who had been educated at [[Geelong Grammar School]].<ref>{{Cite web | url = http://venn.lib.cam.ac.uk/cgi-bin/search-2014.pl?sur=Armytage&suro=w&fir=G&firo=b&cit=&cito=c&c=all&tex=blue&sye=&eye=&col=all&maxcount=50 | title = George [Francis] Armytage | publisher = [[University of Cambridge]] | accessdate = 8 December 2014}}</ref>  Author and former Oxford rower G. C. Drinkwater described the Oxford crew as "something of a disappointment" while declaring that Cambridge were "a very fine crew".<ref name=drink67/>
{| class=wikitable
|-
! rowspan="2" |Seat
! colspan="3" |Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
! colspan="3" |Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] ||P. J. Hibbert || [[Lady Margaret Boat Club]] || 11 st 0 lb || H. W. Benson || [[Brasenose College, Oxford|Brasenose]] || 11 st 0 lb
|-
| 2 || G. F. Armytage || [[Jesus College, Cambridge|Jesus]] || 11 st 6 lb || J. S. Sinclair || [[Oriel College, Oxford|Oriel]] || 11 st 5.5 lb
|-  
| 3 || J. B. Close (P) || [[Trinity College, Cambridge|1st Trinity]] || 10 st 13 lb ||  W. E. Sherwood || [[Christ Church, Oxford|Christ Church]] || 11 st 8 lb
|-
| 4 || A. S. Estcourt || [[Trinity Hall, Cambridge|Trinity Hall]] || 11 st 8 lb || A. R. Harding || [[Merton College, Oxford|Merton]] || 11 st 1.5 lb
|-
| 5 || W. C. Lecky-Browne || [[Jesus College, Cambridge|Jesus]]|| 12 st 3 lb || J. Williams || [[Lincoln College, Oxford|Lincoln]] || 13 st 0.5 lb
|-
| 6 || J. A. Aylmer || [[Trinity College, Cambridge|1st Trinity]] || 12 st 10 lb || A. W. Nicholson (P) || [[Magdalen College, Oxford|Magdalen]] || 12 st 10 lb
|-
| 7 || C. S. Read || [[Trinity College, Cambridge|1st Trinity]] || 12 st 12 lb || H. J. Staymer || [[St John's College, Oxford|St John's]] || 11 st 10.5 lb
|-
| [[Stroke (rowing)|Stroke]] || [[Herbert Rhodes|H. E. Rhodes]] || [[Jesus College, Cambridge|Jesus]]|| 11 st 5 lb || J. P. Way || [[Brasenose College, Oxford|Brasenose]] || 10 st 9 lb
|-
| [[Coxswain (rowing)|Cox]] || C. H. Candy || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 7 st 7 lb || W. F. A. Lambert || [[Wadham College, Oxford|Wadham]] || 7 st 4 lb
|-
! colspan="7"|Source:<ref name=burn60>Burnell, p. 60</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]], along which the race is conducted]]
Cambridge won the [[coin flipping|toss]] and elected to start from the Middlesex side of the river, handing the Surrey station to Oxford.<ref>Drinkwater, pp. 67&ndash;68</ref>   The race started at 11.14am and was "rowed on an exceptionally sluggish tide".<ref name=drink67/> Cambridge made a good start and held a half-length lead at [[Hammersmith Bridge]].  As the bend of the river began to give advantage to the Surrey side, Oxford drew back into contention and the crews were level at the bottom of Chiswick Reach.<ref name=drink68>Drinkwater, p. 68</ref>  The position of moored barges provided a good course and smooth water for the Dark Blues who took a half-length lead.<ref name=drink68/>

Despite this, fatigue began to set in to the Oxford crew and Cambridge came back into contention.  With a steady rhythm, the Light Blues were level at the Bathing Place of Athens and held a clear water advantage by The Bull's Head pub.<ref name=drink68/>  A length ahead by [[Barnes Railway Bridge|Barnes Bridge]], Cambridge pulled further ahead to win by three-and-a-half lengths in a time of 22 minutes 35 seconds.  It was Cambridge's fifth consecutive victory and took the overall record to 16&ndash;15 in favour of Oxford.<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1874}}
[[Category:1874 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1874 sports events]]