{{Italic title}}
The '''''Jerusalem Review of Legal Studies''''' is a [[law journal]] covering books and research in [[legal theory]]. It is based on symposia in which an author of a book or a research project is invited to respond to critical commentaries. The critical commentaries and the response of the author are published in the journal. Previous symposia covered a book by Doug Husak on criminalization; [[Will Kymlicka]] on [[multiculturalism]], Rae Langton on [[pornography]], and David Rabban on [[History of law#United States|history of American law]]. It was established by [[David Enoch (jurisprudent)|David Enoch]] and [[Alon Harel]] ([[Hebrew University]]) and is published by the Hebrew University law faculty. Since 2012, the journal is published by [[Oxford University Press]].<ref>{{cite web |url=http://www.oxfordjournals.org/news/2011/12/05/jrls.html |title=Oxford Journals &#124; News &#124; Jerusalem Review of Legal Studies joins Oxford University Press |work= |accessdate=2011-12-06}}</ref>

== Abstracting and indexing ==
The ''Jerusalem Review of Legal Studies'' is abstracted and indexed by [[HeinOnline]].<ref>{{cite web |title=JERUSALEM REVIEW OF LEGAL STUDIES |url=http://www.heinonline.org/HOL/TitleSummary?index=journals/jerusrls&collection=journals&base=js |work=HeinOnline Law Journal Library |publisher=William S. Hein & Co., Inc. |accessdate=28 October 2011}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://law.huji.ac.il/eng/pirsumim.asp?cat=2062&in=1957}}
* [http://jrls.oxfordjournals.org/ Journal page] at publisher's website
* {{ISSN|2219-7117}}
{{Hebrew University of Jerusalem}}

[[Category:Hebrew University of Jerusalem]]
[[Category:Israeli law journals]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Publications established in 2010]]
[[Category:Oxford University Press academic journals]]

{{law-journal-stub}}