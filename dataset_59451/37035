{{good article}}
{{Infobox food
| name         = Bacon Explosion
| image        = The Food at Davids Kitchen 166.jpg
| image_size   = 250px
| caption      = A Bacon Explosion on a grill
| country      = [[United States]]
| region       = [[Kansas City metropolitan area|Kansas City]]
| creators     = Aaron Chronister and Jason Day
| year         = 2008
| type         = Meat
| course       = Main course
| served       = Hot
| main_ingredient = [[Bacon]], [[sausage]]
| calories     = 5,000+
}}
A '''Bacon Explosion''' is a [[pork]] dish that consists of [[bacon]] wrapped around a filling of spiced [[sausage]] and crumbled bacon. The [[Gridiron football|American-]][[Football (ball)#American and Canadian football|football]]-sized dish is [[Smoking (cooking)|smoked]] or [[baking|baked]]. It became widely known after being posted on the BBQ Addicts blog,<ref name=NYTimes/><ref name=Telegraph/> and quickly spread to the mainstream press with numerous stories discussing the dish.<ref name=NYTimes>{{cite news |url=https://www.nytimes.com/2009/01/28/dining/28bacon.html |title=Take Bacon. Add Sausage. Blog |work=[[The New York Times]] |first=Damon |last=Darlin |date=27 January 2009 |accessdate=28 January 2009 |archiveurl=https://web.archive.org/web/20090402054928/http://www.nytimes.com/2009/01/28/dining/28bacon.html |archivedate=April 2, 2009 |deadurl=no}}</ref> In time, the articles began to discuss the Internet "buzz" itself.<ref name=NYTimes/>

The Bacon Explosion is made of bacon, sausage, [[barbecue sauce]] and barbecue seasoning or [[spice rub|rub]]. The bacon is assembled in a weave to hold the sausage, sauce and crumbled bacon. Once rolled, the Bacon Explosion is cooked, basted, cut and served. The Bacon Explosion's creators produced a cookbook featuring the recipes which ultimately won the 2010 Gourmand World Cookbook Awards for "Best Barbecue Book in the World". The Bacon Explosion also won at the 2013 Blue Ribbon Bacon Festival.

==History and origin==
Jason Day and Aaron Chronister posted the dish in December 2008 on their "BBQ Addicts" blog.<ref name="bbqaddicts">{{cite web |url=http://www.bbqaddicts.com/blog/recipes/bacon-explosion/ |title=Bacon Explosion: The BBQ Sausage Recipe of all Recipes |work=BBQ Addicts |first=Jason |last=Day |date=23 December 2008 |accessdate=28 January 2009 |archiveurl=https://web.archive.org/web/20090116143733/http://www.bbqaddicts.com/blog/recipes/bacon-explosion/ |archivedate=January 16, 2009 |deadurl=no}}</ref> It quickly became an [[Internet phenomenon]], generating more than 500,000 hits and 16,000 links to the blog, and was even included on political blogs because "[[Republican Party (United States)|Republicans]] like meat."<ref name=NYTimes/><ref name=Telegraph>{{cite news |url=http://www.telegraph.co.uk/foodanddrink/foodanddrinknews/4399224/Bacon-Explosion-recipe-is-most-popular-on-the-web.html |title=Bacon Explosion recipe is most popular on the web |work=[[The Daily Telegraph]] |first=Anita |last=Singh |date=30 January 2009 |accessdate=31 January 2009 |archiveurl=https://web.archive.org/web/20090131160112/http://www.telegraph.co.uk/foodanddrink/foodanddrinknews/4399224/Bacon-Explosion-recipe-is-most-popular-on-the-web.html |archivedate=31 January 2009 |deadurl=no}}</ref> There are fan clubs and follow-up videos of various attempts to create the dish.<ref name=Telegraph/>

The inventors are experienced barbecue competition participants from [[Kansas City, Missouri|Kansas City]], and compete in cook-offs as the Burnt Finger BBQ team.<ref name=Telegraph/> According to the ''Telegraph'', "They came up with the delicacy after being challenged on [[Twitter]] to create the ultimate bacon recipe."<ref name=Telegraph/> They christened their innovation the "Bacon Explosion: The BBQ Sausage Recipe of all Recipes."<ref name=Telegraph/> The Bacon Explosion is similar to a number of previously published recipes. Day and Chronister do not claim to have invented the concept,<ref name=NYTimes/> but assert the term "Bacon Explosion" as a trademark.<ref name="bbqaddicts"/>

==Preparation==
Preparing a Bacon Explosion "requires the minimum of culinary talent" and the ingredient list is short.<ref name=Telegraph/> It is made from {{convert|2|lb|g|sigfig=2}} of thick cut bacon, 2&nbsp;pounds of [[Italian sausage]], one jar of [[barbecue sauce]], and one jar of barbecue rub/seasoning.<ref name=Telegraph/> The Bacon Explosion is constructed by weaving the bacon together to serve as a base. The base is seasoned and followed by the layering of sausage meat and crumbled bacon. Barbecue sauce and more seasoning is added before rolling it into a giant "sausage-shaped monster" inside [[aluminium foil|aluminum foil]].<ref name=Telegraph/> It takes about an hour per inch of thickness to cook and is then basted with more barbecue sauce, sliced into rounds, and served.<ref name=Telegraph/> A prepared Bacon Explosion contains at least {{convert|5000|kcal|kJ|sigfig=2|lk=on}} and {{convert|500|g|oz|sigfig=2|order=flip|abbr=on}} of fat,<ref name=NYTimes/> though a smaller {{convert|8|oz|g|sigfig=2|adj=on}} portion contains {{convert|878|kcal|kJ|sigfig=2|abbr=on}} and {{convert|60|g|oz|sigfig=2|order=flip|abbr=on}} of fat.<ref>{{cite web |url=http://www.headsupeating.com/nutrition-facts/+Bacon+Explosion/18385/ |title=Bacon Explosion by Weight |work=Stay Cut |first=Greg |last=Taylor |date=30 January 2009}}</ref>

<gallery mode="packed" heights="125px" caption="Preparation and result">
File:Bacon Explosion preperation 01.JPG|The woven bacon base
File:Bacon Explosion preperation 02.JPG|Sauced and ready for rolling
File:Bacon Explosion preperation 03.JPG|Rolled and seasoned prior to cooking
File:Bacon Explosion finished.JPG|Interior shown after cooking
</gallery>

==Recognition==
The popularity of the recipe has led to international coverage;<ref name=Telegraph/> including the US and the UK, German and Dutch media.<ref>{{cite web |url=http://www.express.de/nachrichten/news/living/dieses-grill-monster-erobert-das-internet_artikel_1232796976332.html |title='Bacon Explosion': Dieses Grill-Monster erobert das Internet |work=Express |date=30 January 2009 |accessdate=3 February 2009 |archiveurl=https://web.archive.org/web/20090208223655/http://www.express.de/nachrichten/news/living/dieses-grill-monster-erobert-das-internet_artikel_1232796976332.html |archivedate=8 February 2009 |deadurl=no}}</ref><ref>{{cite web |url=http://www.vleesmagazine.nl/industrienieuws/nieuws/2012/8/bacon-explosion-de-hit-op-internet-10113174 |title=Bacon Explosion dé hit op internet |work=Vlees Magazine |first=Caroline |last=van der Plas |date=12 August 2012 |accessdate=25 March 2016 |archiveurl=https://web.archive.org/web/20160325141146/http://www.vleesmagazine.nl/industrienieuws/nieuws/2012/8/bacon-explosion-de-hit-op-internet-10113174 |archivedate=25 March 2016 |deadurl=no}}</ref>  ''[[The Daily Telegraph]]'' assessed that the "recipe is most popular on the web" and that the "5,000 [[Food_calorie|calorie]] barbeque dish has become one of the most popular meal ideas in the world."<ref name=Telegraph/> Commentary in major publications about the health/obesity of Americans quickly suggested dishes like Bacon Explosion as the reason for "Why Americans are fat", while another asserted that it isn't something a doctor would recommend.<ref>{{cite news |url=http://voices.kansascity.com/node/3497 |title=The 'Bacon Explosion': Why Americans are fat |work=[[The Kansas City Star]] |first=Yael T. |last=Abouhalkah |date=29 January 2009 |accessdate=31 January 2009 |archiveurl=https://web.archive.org/web/20090131161407/http://voices.kansascity.com/node/3497 |archivedate=31 January 2009 |deadurl=no}}</ref><ref>{{cite news |url=http://www.abcnews.go.com/Health/Diet/story?id=6759625&page=1 |title=Super Bowl Foods the Doc Won't Recommend |work=[[ABC News]] |first1=Monica |last1=Nista |first2=Lauren |last2=Cox |date=30 January 2009 |accessdate=31 January 2009 |archiveurl=https://web.archive.org/web/20090202060640/http://abcnews.go.com/Health/Diet/story?id=6759625&page=1 |archivedate=2 February 2009 |deadurl=no}}</ref> It has also been cited as an example of the use of [[Web 2.0]] technology (Chronister is an Internet marketer).<ref>{{cite news |url=http://www.usnews.com/blogs/risky-business/2009/1/29/bacon-explosion-an-artery-clogging-example-of-web-20-strategies.html |title=Bacon Explosion: An Artery-Clogging Example Of Web 2.0 Strategies |work=[[U.S. News & World Report]] |first=Matthew |last=Bandyk |date=29 January 2009 |accessdate=3 February 2009 |archiveurl=https://web.archive.org/web/20090201035036/http://www.usnews.com/blogs/risky-business/2009/1/29/bacon-explosion-an-artery-clogging-example-of-web-20-strategies.html |archivedate=1 February 2009 |deadurl=no}}</ref>

Day and Chronister were reported to have "landed a six-figure book deal" for their book ''BBQ Makes Everything Better''.<ref>{{cite web |url=http://www.eatmedaily.com/2009/03/big-deals-creators-of-the-bacon-explosion-writing-a-book/ |title=Big Deals: Creators of the Bacon Explosion Writing a Book |work=Eat Me Daily |date=3 March 2009 |accessdate=16 January 2014}}</ref> The book, containing the recipe, became the US winner in the 2010 Gourmand World Cookbook Awards in the "Best Barbecue Book" category.<ref name=gwc>{{cite web |url=http://www.bbqaddicts.com/blog/general/bbq-cookbook-awards/ |title=Gourmand World Cookbook Award Winner |work=BBQ Addicts |first=Aaron |last=Chronister |date=20 December 2010 |accessdate=16 January 2014}}</ref> The 2010 US winner ''The Essential New York Times Cookbook: Classic Recipes for a New Century'' by Amanda Hesser also contained the recipe for the Bacon Explosion.<ref name=gwc /> ''BBQ Makes Everything Better'' went on to win the "Best Barbecue Book in the World" category by the judges of the 2010 Gourmand World Cookbook Awards, and remained as the sole entry from an American.<ref>{{cite web |url=http://www.bbqaddicts.com/blog/general/best-bbq-cookbook-in-the-world/ |title=Gourmand World Cookbook Awards - Best In The World |work=BBQ Addicts |first=Jason |last=Day |date=22 March 2011 |accessdate=16 January 2014}}</ref> The Bacon Explosion won "Savory Dish" at the 2013 Blue Ribbon Bacon Festival which secured an entry in the Bacon World Championships.<ref>{{cite web |url=http://www.esquire.com/blogs/food-for-men/bacon-explosion-successor-2013 |title=What Will Dethrone the Bacon Explosion? |work=[[Esquire (magazine)|Esquire]] |series=Eat Like a Man |first=Jonathan |last=Bender |date=31 August 2013 |accessdate=16 January 2014}}</ref><ref>{{cite web |url=http://blueribbonbaconfestival.com/contests/ |title=Past Winners: 2013 Contest Winners |publisher=Blue Ribbon Bacon Festival |accessdate=16 January 2014}}</ref>

==See also==
{{Portal|Bacon|Food}}
* [[Bacon mania]]
* [[List of pork dishes]]
* [[List of sausage dishes]]
* [[List of smoked foods]]

==References==
{{Reflist|2}}

==External links==
{{Commons category|Bacon explosion}}
*[http://www.bbqaddicts.com BBQ Addicts]
*[https://www.nytimes.com/2009/01/28/dining/28bacon.html?_r=1 Photos of 'Bacon Explosion' construction] at ''[[The New York Times]]''

{{Bacon}}

[[Category:2008 introductions]]
[[Category:Bacon dishes]]
[[Category:Internet memes]]
[[Category:Pork dishes]]
[[Category:Smoked meat]]
[[Category:Sausage dishes]]
[[Category:Baked goods]]