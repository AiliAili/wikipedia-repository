{{for|the France-based trio who were successful in the disco era|Gibson Brothers}}
{{Infobox musical artist <!-- For individuals; see Wikipedia:WikiProject_Musicians -->
| name                = The Gibson Brothers
| image               =File:Gibson Brothers 2010 MerleFest.jpg
| alt                 =
| caption             =The Gibson Brothers playing at [[MerleFest]] in 2010.
|  image_size          =
| background          = non_vocal_instrumentalist <!-- includes instrumentalists who also sing -->
| birth_name          =
| alias               =
| birth_date          = Eric, born October 23, 1970; Leigh, October 11, 1971
| birth_place         = Plattsburgh, New York, United States
| death_date          =
| origin              =
| instrument          =
| genre               = [[Bluegrass music|Bluegrass]], [[Americana music|Americana]]
| occupation          =
| years_active        = 1987-present
| label               = Rounder, Compass, Sugar Hill, Hay Holler
| associated_acts     =
| website             = gibsonbrothers.com
| notable_instruments =
}}
'''The Gibson Brothers''' are an American brother duet, who have performed [[bluegrass music]] professionally since the late 1980s.<ref>{{cite book|last=Gooley|first=Lawrence P.|title=History of Churubusco|year=2010|publisher=Bloated Toe Publishing|isbn=978-0-9795741-5-3|location=Peru, NY|pages=197–201}}</ref><ref>{{cite journal|last=Stuart|first=Chris|title=The Gibson Brothers - Lifers|date=April 2010|volume=44|issue=10|pages=26–29}}</ref>

The [[International Bluegrass Music Association]] (IBMA) awarded the Gibson Brothers “Emerging Artist of the Year” honors in 1998 following the Alan O’Bryant produced album ''Another Night of Waiting'' on the Hay Holler label. They debuted on the Grand Ole Opry on April 11, 2003. “Ring the Bell” (written by Chet O'Keefe) garnered “Song of the Year” and “Gospel Recorded Performance of the Year” honors in 2010. Their first tour abroad was to Ireland and then Germany in 2010. In 2012 they returned to Germany with stops in Denmark, France, and Italy also. The brothers debuted on Garrison Keillor's Prairie Home Companion on October 4, 2014 at the Fitzgerald Theater in St. Paul, Minnesota and returned January 17, 2015.

== Awards ==

=== International Bluegrass Music Association Awards <ref>{{cite web|title=Recipient History - IBMA Awards |url=http://ibmaawards.org/node/636 |archive-url=http://archive.is/20140102220850/http://ibmaawards.org/node/636 |dead-url=yes |archive-date=2 January 2014 |accessdate=21 January 2014 }}</ref> ===

* Emerging Artist of the Year – 1998
* Song of the Year – “Ring the Bell" – 2010 and “They Called It Music” - 2013
* Vocal Group of the Year – 2011 and 2013
* Album of the Year – “Help My Brother” - 2011
* Gospel Recorded Performance of the Year - “Ring the Bell” – 2010 and “Singing as We Rise – Gibson Brothers with Ricky Skaggs – 2012
* Songwriter of the Year – Eric Gibson - 2013
* Entertainer of the Year – 2012 and 2013

=== Society for the Preservation of Bluegrass Music in America Awards<ref>{{cite web|title=38th ANNUAL SPBGMA BLUEGRASS MUSIC AWARDS, 2012 Award Winners|url=http://spbgma.com/level2/natawd2012.html|accessdate=21 January 2014}}</ref> ===

* Bluegrass Album of the Year – “Help My Brother” – 2012
* Song of the Year – “Help My Brother” – 2012
* Songwriters of the Year – Leigh & Eric Gibson - 2012 
* Song of the Year - "They Called It Music" - 2014 <ref>{{cite web|title=40th Annual SPBGMA Bluegrass Music Awards|url=http://www.spbgma.com/level2/natawd2014.html|accessdate=4 February 2014}}</ref>
* Gospel Group of the Year (Contemporary):  The Gibson Brothers - 2015 
* Vocal Group of the Year:  The Gibson Brothers - 2015 <ref>{{cite web|title=41st Annual SPBGMA  Bluegrass Music Awards |url=http://www.spbgma.com/level2/natawd2014.html|accessdate=29 March 2015}}</ref>

=== Bluegrass Unlimited National Bluegrass Survey ===

"Long Forgotten Dream" was their first album to appear on the Bluegrass Unlimited (BU) National Bluegrass Survey with four months on the chart;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=September 1996|volume=31|issue=3|page=57}}</ref> Spread Your Wings followed with three months;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=October 1997|volume=32|issue=4|page=71}}</ref> and Another Night of Waiting for nine months with a peak ranking of No. 7.<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=September 1998|volume=33|issue=3|page=75}}</ref>
In July 2003 "Bona Fide" became their first album to reach No. 1 on the BU National Bluegrass Survey;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=June 2003|volume=38|page=71}}</ref> "A Long Way Back Home" occupied the No. 1 position for four months;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=December 2004|volume=39|page=91}}</ref> "Red Letter Day"  for two months;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=August 2006|volume=41|page=67}}</ref> "Iron and Diamonds" for one month;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=October 2008|volume=43}}</ref> "Ring the Bell" for two months;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=February 2010|volume=44|page=41}}</ref> "Help My Brother" for eight months;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=January 2012|volume=46|page=75}}</ref> and "They Called It Music" for six months.<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=December 2013|volume=48|page=45}}</ref>

Singles from their recordings have consistently made the BU Songs Chart with "Long Forgotten Dream" lasting eight months, "Picture in the Moonlight" eleven and "She Paints a Picture" thirteen. The first single to attain No. 1 position was held by: "Mountain Song" for two consecutive months;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=October 2004|volume=39|page=71}}</ref> "Ring the Bell" for three;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=March 2010|volume=44|page=51}}</ref> "Farm of Yesterday" for two;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=October 2010|volume=45|page=71}}</ref> "Help My Brother" for three;<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=September 2011|volume=46|page=61}}</ref> and "They Called It Music" for three.<ref>{{cite journal|title=Bluegrass Unlimited National Bluegrass Survey|journal=Bluegrass Unlimited|date=December 2013|volume=48|page=45}}</ref>

=== Honorary Degrees ===
On May 16, 2015 both Eric and Leigh received honorary [[Doctor of Fine Arts|doctorates of fine arts]] from the [[State University of New York]] during the spring commencement at [[SUNY Plattsburgh]].<ref>{{cite journal|last1=Halsey|first1=Derek|title=The Gibson Brothers - Brotherhood. Honoring the Tradition of Sibling Harmonies|journal=Bluegrass Unlimited|volume=49|issue=12|pages=28–32}}</ref>

=== Recordings and Labels ===

* Underneath a Harvest Moon – Big Elm 4194 – 1994
* Long Forgotten Dream – Hay Holler HHH-CD-1201 – 1996
* Spread Your Wings – Hay Holler HH-CD-1335 – 1997
* Another Night of Waiting – Hay Holler HH-CD-1341 – 1998
* Bona Fide - Sugar Hill SUG-CD-3965 – 2003
* A Long Way Back Home – Sugar Hill SUG-CD-3986 – 2004
* Red Letter Day - Sugar Hill SUG-CD-2002 – 2005
* Iron and Diamonds – Sugar Hill SUG-CD-4039 – 2008
* Ring the Bell – Compass 7 4506 2 - 2009
* Help My Brother – Compass 7 4549 2 – 2011
* They Called It Music – Compass 7 4599 2 – 2013
* Brotherhood – Rounder 11661-35986-02 – 2015

The Gibson Brothers also appear on recordings by:
* Rick Hayes – Fly by Night – Kang Records kr3222 – 2008
* Joe Walsh – Sweet Loam – Skinny Elephant – 2010
* Terry Baucom – In a Groove – John Boy and Billy, Inc. – 2011
* Peter Rowan – The Old School – Compass 7 4600 2 - 2013
* Jon Weisberger – I've Been Mostly Awake – Wise Kings - 2014

== References ==

{{reflist}}

{{Authority control}}

[[Category:Living people]]
[[Category:20th-century American singers]]
[[Category:20th-century composers]]
[[Category:21st-century American singers]]
[[Category:21st-century composers]]
[[Category:American country banjoists]]
[[Category:American acoustic guitarists]]
[[Category:American male guitarists]]
[[Category:American bluegrass guitarists]]
[[Category:American bluegrass musicians]]
[[Category:American male singer-songwriters]]
[[Category:American country music groups]]
[[Category:Country music duos]]
[[Category:Family musical groups]]
[[Category:Sibling musical duos]]
[[Category:Sibling duos]]
[[Category:Singers from New York]]
[[Category:Songwriters from New York]]
[[Category:20th-century American guitarists]]
[[Category:21st-century American guitarists]]
[[Category:Guitarists from New York]]