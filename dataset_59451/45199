{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name= Andrew Beauchamp-Proctor
|image= Andrew Beauchamp-Proctor VC IWM Q 067596.jpg
|image_size= 
|alt= 
|caption= Captain Beauchamp-Proctor in 1918
|nickname= Prockie
|birth_date= {{birth date|1894|09|04|df=yes}}
|birth_place= [[Mossel Bay]], South Africa
|death_date= {{death date and age|1921|06|21|1894|09|04|df=yes}}
|death_place= [[RAF Upavon]], England
|placeofburial=  [[Mafikeng]], South Africa
|allegiance= South Africa<br/>United Kingdom
|branch= [[South African Army]] (1914–15)<br/>[[Royal Flying Corps]] (1917–18)<br/>[[Royal Air Force]] (1918–21)
|serviceyears= 1914–1915<br/>1917–1921
|rank= [[Flight Lieutenant]]
|unit= Duke of Edinburgh's Own Rifles<br/>[[No. 84 Squadron RAF]]
|commands=
|battles= [[First World War]]
|awards= [[Victoria Cross]]<br/>[[Distinguished Service Order]]<br/>[[Military Cross]] & [[Medal bar|Bar]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]<br/>[[Mentioned in Despatches]]
|relations= 
|laterwork= 
}}
'''Andrew Frederick Weatherby (Anthony) Beauchamp-Proctor''', {{postnominals|country=GBR|size=100%|sep=,|VC|DSO|MC1|DFC}} (4 September 1894 – 21 June 1921) was a South African airman and a recipient of the [[Victoria Cross]], the highest award for [[Courage|gallantry]] in the face of the enemy that can be awarded to British and [[Commonwealth of Nations|Commonwealth]] forces. He was South Africa's leading ace of the First World War, being credited with 54 aerial victories.

==Early life==
Beauchamp-Proctor was born 4 September 1894 in [[Mossel Bay]], [[Cape Province]], the second son of a school teacher and attended the oldest school in the country, [[South African College Schools]], [[Cape Town]], where he was a resident of the oldest residence in the country, College House Residence (where his father was warden). He was studying [[engineering]] at [[University of Cape Town]] when the European war broke out.  He took leave from his studies to join the [[Duke of Edinburgh's Own Rifles]]. He served as a signalman in the [[German South-West Africa]] campaign.

In August 1915, he was demobilised with an honorable discharge. He promptly went to work with the South African Field Telegraph and re-enrolled in university. He managed to complete his third year of college before re-enlisting, this time with the [[Royal Flying Corps]] (RFC), in March 1917.

He was accepted as an Air Mechanic Third Class. From there, he passed on to pilot training at the [[No 1 School of Military Aeronautics|School of Military Aeronautics]] at [[Oxford]] in England, where he was also commissioned. He managed to learn to fly despite his wiry stature of five feet two inches (1.57 m). His aircraft was altered to accommodate him; his seat was raised so he could have a better view from the cockpit and so he could reach controls. Blocks of wood were also fastened on his rudder bar so he could reach it.

On 10 June 1917, he soloed after just over five hours flying time. He crashed upon landing, wiping out the landing gear. Nevertheless, he continued to fly solo. He was passed on to a bomber squadron, Number 84, with a little under ten hours flying experience. When he joined [[No. 84 Squadron RFC|84 Squadron]] in July 1917, it was re-forming as a fighter squadron.

==France 1917–18==
On 23 September 1917, the unit went to France flying [[Royal Aircraft Factory S.E.5|SE5]]s.  Under the command of Major [[William Sholto Douglas]], the unit became one of the most effective scout squadrons in the RFC/RAF (Royal Air Force) during 1918. The squadron would be credited with a victory total of 323, and would produce 25 aces. However, Beauchamp-Proctor would be pre-eminent, with almost triple the number of successes of the second leading ace. He was not particularly esteemed as a flier, but was a deadly shot.

Beauchamp-Proctor's piloting skills can be judged by the fact he had three landing accidents before he ever shot down an enemy plane. He continued to fly the SE5 with modifications to the aircraft's seat and controls, something his Philadelphia-born American squadron mate, Joseph "Child Yank" Boudwin, who stood only two inches taller and who would himself eventually be posted to the [[United States Army Air Service|USAAS]]' [[25th Aero Squadron]] just days before the Armistice, also had to use. The alterations to relatively primitive controls could have contributed to Beauchamp-Proctor's poor airmanship.

His initial confirmed victory did not come until the turn of the year. On 3 January 1918, he sent a German two-seater 'down out of control'. He then claimed four more victories in February, becoming an ace on the final day of the month. Only one of his five victories resulted in the destruction of an enemy; the others were planes sent down as 'out of control'.

March brought four more victories; three of them were scored within five minutes on 17 March. He tallied one kill in April.

Among his 11 victories for the month of May were 5 on 19 May. On that morning, he knocked an enemy observation plane out of the battle; fifteen minutes later, he destroyed an [[Albatros D.V]] scout. That evening, at about 6:35 PM, he downed three more Albatros D.Vs. By 31 May, his roll had climbed to 21 victims—16 fighters and five observation aircraft. By this point, he had destroyed six enemy planes single-handed, and shared the destruction of two others. He drove ten down out of control, and shared in another 'out of control' victory. Two of his victims were captured. Certainly a creditable record, and like many other aces, with no conquests over balloons. 

The next day marked a change of focus for him; he shot down an observation balloon. Balloons, guarded by anti-aircraft artillery and patrolling fighter airplanes, were very dangerous targets. Commonly they were hunted by co-ordinated packs of fighters. For the remainder of his career, he would choose to try to blind the enemy by concentrating on shooting down kite balloons and observation aircraft. Also notable is the drop in his "out of control" victories; from here on out, the record shows destruction after destruction of the enemy. His June string would only run to 13 June, but in that time, he would destroy four balloons, an observation two-seater and a fighter. Only one fighter went down out of control. On 22 June, he was awarded the [[Military Cross]].

July would pass without incident. On 3 August, he was granted one of the first ever [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Crosses]].

The break in his victory string lasted almost a month, as he went on home leave and helped a recruitment drive for the RAF. On 8 August, he returned and resumed with tally number 29, another balloon. On 9 August, Beauchamp-Proctor was leading No. 84 Squadron on a patrol over their base at [[Bertangles]], with Boudwin and six-foot-four tall [[Hugh Saunders]] as wingmen. The threesome  got involved in a heated engagement at 2:00 pm, that involved them in combat against Fokker D.VII fighters of [[Jagdgeschwader 1 (World War I)|JG I]], led that day by the future Nazi ''Reichsmarschall'' [[Hermann Göring]]. Unsuccessful at increasing his total that day, Beauchamp-Proctor would claim an additional 14 aircraft, and end the month with his claims list extended to 43. One memorable day was 22 August; he attacked a line of six enemy balloon over the British 3rd Corps front. He set the first one afire with his machine guns and forced the other five to the ground, the observers taking to their parachutes. His 15 kills for August would include 5 balloons, all destroyed, and two more two-seater planes. He was now up to 43 victories.

His September claims would be all balloons – four of them.

In the first few days of October, he would destroy three more balloons and three [[Fokker D.VII]] fighters, one of which burned. Another D.VII spun down out of control.

On 8 October, he was hit by ground fire and wounded in the arm, ending his front line service.

Beauchamp-Proctor's victory total was 54; two (and one shared) captured enemy aircraft, 13 (and three shared) balloons destroyed, 15 (and one shared) aircraft destroyed, and 15 (and one shared) aircraft 'out of control'.<ref>'Above the Trenches'; Shores, Franks & Guest. page 68.</ref> His 16 balloons downed made him the leading British Empire balloon buster.

On 2 November, he was awarded the [[Distinguished Service Order]], followed by the [[Victoria Cross]] on 30 November.

==Post war==
He was discharged from hospital in March 1919 and embarked on a four-month-long lecture tour of the USA, before returning to England and qualifying as a seaplane pilot with a permanent commission as a [[flight lieutenant]] in the RAF.

After his VC investiture at [[Buckingham Palace]] in November 1919 he was awarded a year’s leave, and this enabled him finish his BSc degree in Engineering.

==Death==
Beauchamp-Proctor was killed on 21 June 1921 in a training accident flying a [[Sopwith Snipe]], in preparation for an air show at the [[RAF Hendon]]. His aircraft went into a vicious spin after performing a slow loop, and he was killed in the ensuing crash. At least one observer remarked that the loss of control and subsequent crash of the aircraft could have been linked to Proctor's diminutive size.

He was originally buried at [[Upavon]], Wiltshire, but in August 1921 his body was returned to South Africa<ref name="84 Squadron">{{cite web | url=http://www.84squadron.co.uk/html/body_proctor_vc.html | title=84 Squadron profile | accessdate=January 14, 2013}}</ref> where he was given a state funeral.

There still exists confusion over Beauchamp-Proctor's given name.  For decades he was listed as "Anthony" but more recent scholarship indicates "Andrew", which apparently is the name on his tombstone.

==Citations==
===Military Cross===
{{quote|For conspicuous gallantry and devotion to duty. While on offensive patrol he observed an enemy two-seater plane attempting to cross our lines. He engaged it and opened fire, with the result that it fell over on its side and crashed to earth. On a later occasion, when on patrol, he observed three enemy scouts attacking one of our bombing machines. He attacked one of these, and after firing 100 rounds in it, it fell over on its back and was seen to descend in that position from 5,000 feet. He then attacked another group of hostile scouts, one of which he shot down completely out of control, and another crumpled up and crashed to earth. In addition to these, he has destroyed another hostile machine, and shot down three completely out of control. He has at all times displayed the utmost dash and initiative, and is a patrol leader of great merit and resource.}}

MC citation, Supplement to the London Gazette, 22 June 1918

===Distinguished Flying Cross===
{{quote|Lt. (T./Capt.) Andrew Weatherby Beauchamp-Proctor, M.C.
A brilliant and fearless leader of our offensive patrols.
His formation has destroyed thirteen enemy machines and brought down thirteen more out of control in a period of a few months.
On a recent morning his patrol of five aeroplanes attacked an enemy formation of thirty machines and was successful in destroying two of them. In the evening he again attacked an enemy formation with great dash, destroying one machine and forcing two others to collide, resulting in their destruction.}}

DFC citation, Supplement to the London Gazette, 3 August 1918

===Distinguished Service Order===
{{quote|A fighting pilot of great skill, and a splendid leader. He rendered brilliant service on 22 August, when his Flight was detailed to neutralise hostile balloons. Having shot down one balloon in flames, he attacked the occupants of five others in succession with machine-gun fire, compelling the occupants in each case to take to parachutes. He then drove down another balloon to within fifty feet of the ground, when it burst into flames. In all he has accounted for thirty-three enemy machines and seven balloons.}}

DSO citation, Supplement to the London Gazette, 2 November 1918

===Victoria Cross===
{{quote|Between 8 August 1918, and 8 October 1918, this officer proved himself victor in twenty-six decisive combats, destroying twelve enemy kite balloons, ten enemy aircraft, and driving down four other enemy aircraft completely out of control.
Between 1 October 1918, and 5 October 1918, he destroyed two enemy scouts, burnt three enemy kite balloons, and drove down one enemy scout completely out of control.
On 1 October 1918, in a general engagement with about twenty-eight machines, he crashed one Fokker biplane near Fontaine and a second near Ramicourt; on 2 October he burnt a hostile balloon near Selvjgny; on 3 October he drove down, completely out of control, an enemy scout near Mont d'Origny, and burnt a hostile balloon; on 5 October, the third hostile balloon near Bohain.
On 8 October 1918, while flying home at a low altitude, after destroying an enemy two-seater near Maretz, he was painfully wounded in the arm by machine-gun fire, but, continuing, he landed safely at his-aerodrome, and after making his report was admitted to hospital.<br>
In all he has proved himself conqueror over fifty-four foes, destroying twenty-two enemy machines, sixteen enemy kite balloons, and driving down sixteen enemy aircraft completely out of control.<br>
Captain Beauchamp-Proctor's work in attacking enemy troops on the ground and in reconnaissance during the withdrawal following on the Battle of St. Quentin from 21 March 1918, and during the victorious advance of our Armies commencing on 8 August, has been almost unsurpassed in its Brilliancy, and. as such has made an impression on those serving in his squadron and those around him that will not be easily forgotten.
Capt. Beauchamp-Proctor was awarded Military Cross on 22 June 1918; D.F. Cross on 2 July 1918; Bar to M.C. on 16 September 1918; and Distinguished Service Order on 2 November 1918.<ref name="VCGazette"> {{London Gazette |date=29 November 1918 |issue=31042 |startpage=14204 |supp=yes}}</ref>}}

==References==
;Citations
{{reflist}}
;Bibliography
* 'Above the Trenches' (Shores, Franks & Guest; Grub Street 1990)

==External links==
*[http://www.homeusers.prestel.co.uk/stewart/wiltshir.htm Location of grave and VC medal] ''(Wiltshire)''
*[http://www.theaerodrome.com/aces/safrica/beauchamp.php www.theaerodrome.com – Andrew Beauchamp Proctor]
* http://www.84squadron.co.uk/html/body_proctor_vc.html  Accessed 15 September 2008.
* http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=10583662  Accessed 15 September 2008.
* http://www.theaerodrome.com/services/gbritain/rfc/84.php  Accessed 15 September 2008.
* http://www.firstworldwar.com/bio/proctor.htm  Accessed 15 September 2008.

{{wwi-air}}
{{Royal Flying Corps}}

{{DEFAULTSORT:Beauchamp-Proctor, Anthony Frederick Weatherby}}
[[Category:1894 births]]
[[Category:1921 deaths]]
[[Category:People from Mossel Bay]]
[[Category:South African World War I recipients of the Victoria Cross]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:Royal Air Force recipients of the Victoria Cross]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:South African people of British descent]]
[[Category:South African World War I flying aces]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force officers]]
[[Category:Aviators killed in aviation accidents or incidents in the United Kingdom]]
[[Category:University of Cape Town alumni]]
[[Category:Victims of aviation accidents or incidents in the United Kingdom]]
[[Category:Accidental deaths in London]]
[[Category:Recipients of the Military Cross]]