{{Refimprove|date=October 2008}}
{{Sound measurements}}

'''Sound power''' or '''acoustic power''' is the rate at which [[sound energy]] is emitted, reflected, transmitted or received, per unit time.<ref name=clinical>{{cite book|url=https://books.google.com/books?id=ElPyvaJbDiwC&pg=PA94&dq=sound+power+loudness&hl=en&sa=X&ei=YF0XVdbsEpOBNoeFgKgN&ved=0CCcQ6AEwAA#v=onepage&q=sound%20power%20loudness&f=false|title=Clinical Measurement of Speech and Voice|author=Ronald J. Baken, Robert F. Orlikoff|publisher=Cengage Learning|year=2000|isbn=9781565938694|page=94}}</ref> The [[International System of Units|SI unit]] of sound power is the [[watt]] (W).<ref name=clinical/> It is the power of the sound force on a surface of the medium of propagation of the sound wave. For a sound source, unlike sound pressure, sound power is neither room-dependent nor distance-dependent. Sound pressure is a measurement at a point in space near the source, while the sound power of a source is the total power emitted by that source in all directions. Sound power passing through an area is sometimes called '''sound flux''' or '''acoustic flux''' through that area.

==Mathematical definition==
Sound power, denoted ''P'', is defined by<ref>Landau & Lifshitz, "Fluid Mechanics", Course of Theoretical Physics, Vol. 6</ref>
:<math>P = \mathbf f \cdot \mathbf v = Ap\, \mathbf u \cdot \mathbf v = Apv</math>
where
*'''f''' is the sound force of unit vector '''u''';
*'''v''' is the [[particle velocity]] of projection ''v'' along '''u''';
*''A'' is the area;
*''p'' is the [[sound pressure]].

In a [[Transmission medium|medium]], the sound power is given by
:<math>P = \frac{A p^2}{\rho c} \cos \theta,</math>
where
*''A'' is the area of the surface;
*''ρ'' is the [[mass density]];
*''c'' is the [[sound velocity]];
*''θ'' is the angle between the direction of propagation of the sound and the normal to the surface.

For example, a sound at SPL = 85&nbsp;dB or ''p'' = 0.356 Pa in air (''ρ'' = 1.2&nbsp;kg·m<sup>−3</sup> and ''c'' = 343 m·s<sup>−1</sup>) through a surface of area ''A'' = 1 m<sup>2</sup> normal to the direction of propagation (''θ'' = 0 °) has a sound energy flux ''P'' = 0.3&nbsp;mW.

This is the parameter one would be interested in when converting noise back into usable energy, along with any losses in the capturing device.

==Table of selected sound sources==
[[File:Atlas Copco XAHS 347-pic7-Max. sound power level.jpg|thumb|Maximum sound power level ([[A-weighting|''L<sub>WA</sub>'']]) related to a portable [[air compressor]].]]
Here is a table of some examples.<ref>{{cite web | url = http://www.engineeringtoolbox.com/sound-power-level-d_58.html | title = Sound Power | publisher = The Engineering Toolbox | accessdate = 28 November 2013 }}</ref>

:{| class="wikitable"
! Situation and<br>sound source !! Sound power<br>([[Watt|W]]) !! Sound power level<br>([[Decibel|dB]] ref 10<sup>−12</sup> W)
|-
| [[Saturn V]] rocket || align="right" | '''100,000,000''' || align="right" | 200
|-
| [[Project Artemis]] Sonar || align="right" | '''1,000,000''' || align="right" | 180
|-
| [[Turbojet]] engine || align="right" | '''100,000''' || align="right" | 170
|-
| [[Turbofan]] aircraft at take-off || align="right" | '''1,000''' || align="right" | 150
|-
| [[Turboprop]] aircraft at take-off || align="right" | '''100''' || align="right" | 140
|-
| [[Machine gun]] <br>Large [[pipe organ]]|| align="right" | '''10''' || align="right" | 130
|-
| [[Symphony orchestra]]<br>Heavy [[thunder]]<br>[[Sonic boom]] || align="right" | '''1''' || align="right" | 120
|-
| [[Rock concert]]<br>[[Chain saw]]<br>Accelerating [[motorcycle]]|| align="right" | '''0.1''' || align="right" | 110
|-
| [[Lawn mower]]<br>Car at highway speed<br>[[Rapid transit|Subway steel wheels]] || align="right" | '''0.01''' || align="right" | 100
|-
| Large [[Diesel engine|diesel vehicle]] || align="right" | '''0.001''' || align="right" | 90
|-
| Loud [[alarm clock]] || align="right" | '''0.0001''' || align="right" | 80
|-
| Relatively quiet [[vacuum cleaner]] || align="right" | '''10<sup>−5</sup>''' || align="right" | 70
|-
| [[Hair dryer]] || align="right" | '''10<sup>−6</sup>''' || align="right" | 60
|-
| Radio or TV || align="right" | '''10<sup>−7</sup>''' || align="right" | 50
|-
| [[Refrigerator]]<br/>Low voice || align="right" | '''10<sup>−8</sup>''' || align="right" | 40
|-
| Quiet conversation || align="right" | '''10<sup>−9</sup>''' || align="right" | 30
|-
| Whisper of one person<br>Wristwatch ticking|| align="right" | '''10<sup>−10</sup>''' || align="right" | 20
|-
| Human breath of one person || align="right" | '''10<sup>−11</sup>''' || align="right" | 10
|-
| Reference value || align="right" | '''10<sup>−12</sup>''' || align="right" | 0
|}

==Relationships with other quantities==
Sound power is related to [[sound intensity]]:
:<math>P = AI,</math>
where
*''A'' is the area;
*''I'' is the sound intensity.

Sound power is related [[sound energy density]]:
:<math>P = Acw,</math>
where
*''c'' is the [[speed of sound]];
*''w'' is the sound energy density.

==Sound power level==
{{Other uses|Sound level (disambiguation){{!}}Sound level}}
'''Sound power level''' (SWL) or '''acoustic power level''' is a [[Level (logarithmic quantity)|logarithmic measure]] of the power of a sound relative to a reference value.<br>
Sound power level, denoted ''L''<sub>''W''</sub> and measured in [[Decibel|dB]], is defined by<ref name=IEC60027-3>[http://webstore.iec.ch/webstore/webstore.nsf/artnum/028981 "Letter symbols to be used in electrical technology – Part 3: Logarithmic and related quantities, and their units"], ''IEC 60027-3 Ed. 3.0'', International Electrotechnical Commission, 19 July 2002.</ref>
:<math>L_W = \frac{1}{2} \ln\!\left(\frac{P}{P_0}\right)\!~\mathrm{Np} = \log_{10}\!\left(\frac{P}{P_0}\right)\!~\mathrm{B} = 10 \log_{10}\!\left(\frac{P}{P_0}\right)\!~\mathrm{dB},</math>
where
*''P'' is the sound power;
*''P''<sub>0</sub> is the ''reference sound power'';
*{{no break|1=1 Np = 1}} is the [[neper]];
*{{no break|1=1 B = {{sfrac|2}} ln 10}} is the [[Decibel|bel]];
*{{no break|1=1 dB = {{sfrac|20}} ln 10 }} is the [[decibel]].

The commonly used reference sound power in air is<ref>Ross Roeser, Michael Valente, ''Audiology: Diagnosis'' (Thieme 2007), p. 240.</ref>
:<math>P_0 = 1~\mathrm{pW}.</math>
The proper notations for sound power level using this reference are {{nobreak|''L''<sub>''W''/(1 pW)</sub>}} or {{nobreak|''L''<sub>''W''</sub> (re 1 pW)}}, but the suffix notations {{nobreak|dB SWL}}, {{nobreak|dB(SWL)}}, dBSWL, or dB<sub>SWL</sub> are very common, even if they are not accepted by the SI.<ref name=NIST2008>Thompson, A. and Taylor, B. N. sec 8.7, "Logarithmic quantities and units: level, neper, bel", ''Guide for the Use of the International System of Units (SI) 2008 Edition'', NIST Special Publication 811, 2nd printing (November 2008), SP811 [http://physics.nist.gov/cuu/pdf/sp811.pdf PDF]</ref>

The reference sound power ''P''<sub>0</sub> is defined as the sound power with the reference sound intensity {{nowrap|1=''I''<sub>0</sub> = 1 pW/m<sup>2</sup>}} passing through a surface of area {{nowrap|1=''A''<sub>0</sub> = 1 m<sup>2</sup>}}:
:<math>P_0 = A_0 I_0,</math>
hence the reference value {{nowrap|1=''P''<sub>0</sub> = 1 pW}}.

===Relationship with sound pressure level===
The generic calculation of sound power from sound pressure is as follows:
:<math>L_W = L_p + 10 \log_{10}\!\left(\frac{A_S}{A_0}\right)\!~\mathrm{dB},</math>
where:
<math>{A_S}</math> defines the area of a surface that wholly encompasses the source.  This surface may be any shape, but it must fully enclose the source. 
 
In the case of a sound source located in free field positioned over a reflecting plane (i.e. the ground), in air at ambient temperature, the sound power level at distance ''r'' from the sound source is approximately related to [[sound pressure level]] (SPL)  by<ref name=Chadderton>Chadderton, David V. ''Building services engineering'', pp. 301, 306, 309, 322. Taylor & Francis, 2004. ISBN 0-415-31535-2</ref>
:<math>L_W = L_p + 10 \log_{10}\!\left(\frac{2\pi r^2}{A_0}\right)\!~\mathrm{dB},</math>
where
*''L''<sub>''p''</sub> is the sound pressure level;
*''A''<sub>0</sub> = 1 m<sup>2</sup>;
*<math> {2\pi r^2},</math> defines the surface area of a hemisphere; and 
*''r'' must be sufficient that the hemisphere fully encloses the source.

Derivation of this equation:
:<math>\begin{align}
L_W &= \frac{1}{2} \ln\!\left(\frac{P}{P_0}\right)\\
        &= \frac{1}{2} \ln\!\left(\frac{AI}{A_0 I_0}\right)\\
        &= \frac{1}{2} \ln\!\left(\frac{I}{I_0}\right) + \frac{1}{2} \ln\!\left(\frac{A}{A_0}\right)\!.
\end{align}</math>
For a ''progressive'' spherical wave,
:<math>z_0 = \frac{p}{v},</math>
:<math>A = 4\pi r^2,</math> (the surface area of sphere)
where ''z''<sub>0</sub> is the [[Acoustic impedance#Characteristic specific acoustic impedance|characteristic specific acoustic impedance]].

Consequently,
:<math>I = pv = \frac{p^2}{z_0},</math>
and since by definition {{nobreak|1=''I''<sub>0</sub> = ''p''<sub>0</sub><sup>2</sup>/''z''<sub>0</sub>}}, where {{nobreak|1=''p''<sub>0</sub> = 20 μPa}} is the reference sound pressure,
:<math>\begin{align}
L_W &= \frac{1}{2} \ln\!\left(\frac{p^2}{p_0^2}\right) + \frac{1}{2} \ln\!\left(\frac{4\pi r^2}{A_0}\right)\\
        &= \ln\!\left(\frac{p}{p_0}\right) + \frac{1}{2} \ln\!\left(\frac{4\pi r^2}{A_0}\right)\\
        &= L_p + 10 \log_{10}\!\left(\frac{4\pi r^2}{A_0}\right)\!~\mathrm{dB}.
\end{align}</math>

The sound power estimated practically does not depend on distance. The sound pressure used in the calculation may be affected by distance due to viscous effects in the propagation of sound unless this is accounted for.

==References==
{{Reflist}}

==External links==
*[http://www.sengpielaudio.com/SoundPressureAndSoundPower.pdf Sound power and Sound pressure. Cause and Effect]
*[http://www.sengpielaudio.com/calculator-ak-ohm.htm Ohm's Law as Acoustic Equivalent. Calculations]
*[http://www.sengpielaudio.com/RelationshipsOfAcousticQuantities.pdf Relationships of Acoustic Quantities Associated with a Plane Progressive Acoustic Sound Wave]
*[http://wwwn.cdc.gov/niosh-sound-vibration/default.aspx NIOSH Powertools Database]
*[http://www.lmsintl.com/testing/testlab/acoustics/sound-power-testing Sound Power Testing]

[[Category:Acoustics]]
[[Category:Sound]]
[[Category:Sound measurements]]
[[Category:Physical quantities]]
[[Category:Power (physics)]]