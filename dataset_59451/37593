{{good article}}
{{Infobox NRHP
| name           = Beechwood
| nrhp_type      = cp | nocat = yes
| partof         = [[Scarborough Historic District]]
| partof_refnum  = 84003433<ref name="nris"/>
| image          = Beechwood 2016 15.png
| caption        = West [[Multiview orthographic projection#Elevation|elevation]]
| alt            = Part of a large white house
| location       = 1–3 Beechwood Way, [[Scarborough, New York]] 10510
| area           = {{convert|33.3|acre|ha}}<ref name="ScHD"/>
| built          = 1780
| architect      = 
| architecture   = 
| added          = September 7, 1984
{{Infobox historic site
| embed             = yes
| coordinates       = {{coord|41.134361|-73.862139|region:US-NY_type:landmark_scale:5000|display=inline,title}}
| map_dot_mark      = Blue 000080 pog.svg
| map_dot_label     = Beechwood
| locmapin          = USA Briarcliff#New York#USA
| map_caption       = Location of Beechwood
}}
| visitation_num = 
| visitation_year = 
| refnum         = 
| mpsub          = 
| governing_body = 
}}
'''Beechwood''' is a [[Hudson River]] estate in Scarborough-on-Hudson, in [[Briarcliff Manor, New York]]. The estate was most notably the home of [[Frank A. Vanderlip]] and his family, and is a contributing property to the [[Scarborough Historic District]]. The house and property were owned by the Vanderlip family from 1906 to 1979. The property is now a 37-condominium complex as the result of a development project that began in the 1980s.

Contemporarily, Beechwood is known for being a filming location of the 1970 film ''[[House of Dark Shadows]]'',<ref name="HDS"/> and a filming location and the primary setting of ''[[Savages (1972 film)|Savages]]'', a 1972 [[Merchant Ivory]] film.<ref name="Savages"/><ref name="Savages2"/> In June 2016, ''Money Man: Frank Vanderlip and the Birth of the Federal Reserve'' premiered there. The film documents Vanderlip's life and was filmed at Beechwood.<ref name="MoneyMan">{{cite news|last=Reif|first=Carol|title=Documentary About Federal Reserve's Architect Premieres In Briarcliff|newspaper=Briarcliff Daily Voice|url=http://briarcliff.dailyvoice.com/events/documentary-about-federal-reserves-architect-premieres-in-briarcliff/662273/|date=May 31, 2016|accessdate=May 31, 2016}}</ref>

==History==
{{multiple image|perrow=2|align=left|total_width=360|header=House exterior
|image1=Beechwood in Briarcliff Manor.tif|alt1=Northern portion of east elevation
|image2=Beechwood in Briarcliff Manor (2).tif|alt2=Southern portion of east elevation, part of the two-story white house
|image3=Beechwood in Briarcliff Manor (4).tif|alt3=North side and octagonal library, side and octagonal wing of the white house
|image4=Beechwood in Briarcliff Manor (5).tif|alt4=Rear elevation, classically ornate side of the two-story white house
}}
The first portion of the main residence dates to 1780, and includes the original kitchen's fireplace.<ref name="1952history"/>{{rp|page=8}} Benjamin and Ann Folger were among the earliest residents, and named their residence "Heartt Place". In the 1830s, Folger deeded the estate to a self-proclaimed prophet, [[Robert Matthews (religious figure)|Robert Matthews]], who believed himself to be the resurrected [[Saint Matthias|Matthias]] of the [[New Testament]].<ref name="Changing Landscape"/>{{rp|page=18}} Matthews convinced his followers to fund an expansion to the house, which he had named "Zion Hill". During this time, Isabella Baumfree ([[Sojourner Truth]]) was a housekeeper to him. After he spent the money his followers and Folger had given him, Matthews became violent. Further on, he was tried for murder, and acquitted for lack of evidence.<ref name="pamphlet"/> Matthews was later found guilty of assaulting his grown daughter, and he served a short jail term.<ref name="Changing Landscape"/>{{rp|page=19}}

The property containing the mansion had been in the Remsen family for decades. Anna Remsen Webb was one of the inheritors of the estate. In the 1890s, her husband's half-brother [[Henry Walter Webb]] substantially added to the estate from numerous properties, including the Remsen estate and William Creighton's estate (Creighton had named his house "Beechwood" after he purchased it in 1836). Henry Webb attached the name Beechwood to the entire estate and house. He renovated and expanded the mansion, hiring [[R. H. Robertson]] to double the size of the house. Robertson designed the expansion in the [[Colonial Revival]] style, to be compatible with the [[Neoclassical architecture|neoclassical]] [[Federal style]] of the original but more ornate.<ref name="WestNRHP"/>

Frank Vanderlip purchased the {{convert|23|acre|ha|adj=on}} property from Webb's widow in 1906, and bought more property to make the estate a total {{convert|125|acre|ha}}. He hired [[William Welles Bosworth]] soon after to further enlarge the house and to design a wing for his library and the lawns of the estate.<ref name="ScHD"/> In 1907, while Vanderlip was vice president of the First National City Bank (later [[Citibank]]), he had two fluted smoked granite columns from the headquarters [[55 Wall Street]] shipped to Beechwood (55 Wall Street was being remodeled and the columns were re-spaced, with two left over). He had the columns placed two-thirds above ground in Beechwood's entranceway off of Albany Post Road (now [[U.S. Route 9 in New York|U.S. Route 9]]), an entrance which was later closed due to increasing traffic volume on Route 9 (the current entrance is off Scarborough Station Road). Vanderlip also made a cage for his children's pet rabbits using a discarded wrought-iron elevator, also discarded from the bank. Among the guests the Vanderlips hosted at the house were [[Woodrow Wilson]],<ref name="Wilson"/> [[Henry Ford]], [[Sarah Bernhardt]], [[Annie Oakley]], [[Franklin D. Roosevelt]], [[John D. Rockefeller]], and [[Isadora Duncan]].<ref name="Vanderbilts"/>  The [[Wright Brothers]] even landed a plane on the property.  In 1910, Vanderlip bought the nearby mansion [[Woodlea]], although his wife Narcissa Vanderlip prevented the family from moving, due to her preference of Beechwood over the grandiose Woodlea.<ref name="Changing Landscape"/>{{rp|page=89}} In 1979, Vanderlip descendants sold the property.<ref name="WestNRHP"/> Three condominiums were built during a transformation of the mansion in the 1980s. A later expansion resulted in a total of 37 condominiums on the property's 33 acres.<ref name="NYTScarborough"/>

==Description==
{{multiple image|align=right|direction=vertical|width=180|header=Lawn
|image1=Beechwood 2016 19.png|alt1=The garden's pergola
|image2=Beechwood 2016 20.png|alt2=The garden's pergola
|image3=Beechwood 2016 17.png|alt3=The garden's fountain and teahouse
}}
Near the center of the property, at the southwest corner of Route 9 and Scarborough Station Road, sits the [[wikt:eponymous|eponymous]] mansion that features two large [[portico]]ed entryways, a two-story octagonal library, numerous porches, [[veranda]]s, and over 100 interior rooms. Other major structures included a hunting lodge, a second mansion built for the Vanderlips' daughter Charlotte, a home for the Vanderlips' physician, and the [[Scarborough School]], a progressive school which the Vanderlips established just south of the mansion in 1916.<ref name="ScHD"/>

The {{convert|80|acre|ha|adj=on}} private parkland was designed by [[Frederick Law Olmsted]] for the Vanderlips and has expansive lawns, a grove of large [[beech]] trees, imported trees, and an [[Italianate]] garden with an alcove, fountain, and small pool with ''[[Wisteria]]''-covered trellises. The lawns, formal gardens, and stone gazebo, erected by the Vanderlips, have been preserved and feature in wedding ceremonies that occasionally occur on the property.<ref name="NYTScarborough"/>

The Beechwood estate also contained a [[carriage house]], [[gatehouse]], [[squash court]] (no longer extant), and a white-stucco artist's studio named Beech Twig, which was home to author [[John Cheever]], whose children attended the school on the property. The family rented the house until they moved to Ossining.<ref name="Cheever"/> Descriptions of the building's interior closely match descriptions employed by Cheever in some short stories. Novelist [[Richard Yates (novelist)|Richard Yates]] also lived in the same house as a child, as well as other artists, writers, and composers. The estate's garage is located northeast of the mansion, and is a flat-roof, two-story concrete building dating to the early 1900s.<ref name="ScHD"/>

<gallery widths="155px" heights="160px" perrow=5 mode=packed>>
File:Guest House at Beechwood (South).JPG|Beechtwig, one of two guest houses|alt=A small white house
File:Garage at Beechwood.jpg|Beechwood's two-story seven-bay Art&nbsp;Deco garage|alt=A large Art Deco garage building
File:Entranceway to Beechwood.jpg|The former entranceway, designed by [[William Welles Bosworth]]|alt=Gates to an unused driveway
File:Entranceway Column at Beechwood.jpg|An Ionic column from [[55 Wall Street]]|alt=A large grey stone Ionic column and fence
</gallery>

== See also ==
{{Wikipedia books|Briarcliff Manor}}
* [[Scarborough School]]
{{Portal bar|Architecture|Briarcliff Manor, New York|Hudson Valley}}

==References==
{{reflist|30em|refs=
<ref name="ScHD">{{cite web|last=O'Brien|first=Austin|title=National Register of Historic Places Inventory—Nomination form - Scarborough Historic District|work=[[National Park Service]]|publisher=[[United States Department of the Interior]]|url=https://cris.parks.ny.gov/Uploads/ViewDoc.aspx?mode=A&id=32257|date=August 6, 1984|accessdate=August 4, 2015}} ''See also:'' {{cite web|url=https://cris.parks.ny.gov/Uploads/ViewDoc.aspx?mode=A&id=32255|title=Accompanying photographs}}</ref>
<ref name="Changing Landscape">{{cite book|last=Cheever|first=Mary|title=The Changing Landscape: A History of Briarcliff Manor-Scarborough|year=1990|publisher=Phoenix Publishing|location=West Kennebunk, Maine|isbn=0-914659-49-9|oclc=22274920|lccn=90045613|ol=1884671M}}</ref>
<ref name="WestNRHP">{{cite book|last=Williams|first=Gray|title=Picturing Our Past: National Register Sites in Westchester County|year=2003|publisher=Westchester County Historical Society|isbn=0-915585-14-6}}</ref>
<ref name="pamphlet">{{cite book|last=Gelard|first=Donna|title=Explore Briarcliff Manor: A driving tour|year=2002|publisher=Briarcliff Manor Centennial Committee|others=Contributing Editor Elsie Smith; layout and typography by Lorraine Gelard; map, illustrations, and calligraphy by Allison Krasner}}</ref>
<ref name="nris">{{NRISref|version=2009a}}</ref>
<ref name="Savages">{{cite book|last=Ivory|first=James|title=Savages, Shakespeare Wallah: Two Films by James Ivory|year=1973|url=https://books.google.com/books?id=34E1VS2VAAgC|accessdate=July 14, 2014|publisher=Plexus Publishing Ltd.|location=New York|isbn=0-394-17799-1|pages=7–9|edition=1st}}</ref>
<ref name="Savages2">{{cite news|last=Madsen|first=Krista|title=Movies Made Here: Recapping the Reviews of 2011|url=http://tarrytown.patch.com/groups/opinion/p/movies-made-here-recapping-the-review-of-2011|accessdate=July 14, 2014|newspaper=Tarrytown-Sleepy Hollow Patch|date=February 19, 2014}}</ref>
<ref name="HDS">{{cite web|last=Stafford|first=Jeff|title=House of Dark Shadows|url=http://www.tcm.com/this-month/article/383036%7C424/House-of-Dark-Shadows.html|work=Turner Classic Movies|publisher=Turner Entertainment Networks, Inc.|accessdate=February 27, 2014}}</ref>
<ref name="Cheever">{{cite news|last=McGrath|first=Charles|title=The First Suburbanite|url=https://www.nytimes.com/2009/03/01/magazine/01cheever-t.html|accessdate=July 5, 2014|newspaper=The New York Times|date=February 27, 2009}}</ref>
<ref name="NYTScarborough">{{cite news|last=Brenner|first=Elsa|title=If You're Thinking of Living In/Scarborough; Where Plutocrats Enjoyed a River View|url=https://www.nytimes.com/2002/06/30/realestate/if-you-re-thinking-living-scarborough-where-plutocrats-enjoyed-river-view.html|accessdate=May 5, 2014|newspaper=The New York Times|date=June 30, 2002}}</ref>
<ref name="Vanderbilts">{{cite book|last1=Foreman|first1=John|last2=Stimson|first2=Robbe Pierce|title=The Vanderbilts and the Gilded Age: Architectural Aspirations, 1879–1901|date=May 1991|publisher=St. Martin's Press|location=New York, New York|isbn=0-312-05984-1|pages=152–169|chapter=7|edition=1st|oclc=22957281|lccn=90027083}}</ref>
<ref name="Wilson">{{cite book|last=Sutton|first=Anthony C.|title=The Federal Reserve Conspiracy|date=1995|publisher=CPA Book Publisher|location=Boring, Oregon|url=https://archive.org/details/TheFederalReserveConspiracy|accessdate=September 27, 2014|isbn=0-944379-08-7|page=78|oclc=52473942}}</ref>
<ref name="1952history">{{cite book|title=Our Village: Briarcliff Manor, N.Y. 1902 to 1952|publisher=Historical Committee of the Semi–Centennial|year=1952|lccn=83238400|oclc=24569093}}</ref>
}}

==External links==
{{Commons category-inline}}

{{Briarcliff Manor, New York}}
{{National Register of Historic Places in New York}}

[[Category:Briarcliff Manor, New York]]
[[Category:Houses in Westchester County, New York]]
[[Category:Parks in Westchester County, New York]]
[[Category:Historic district contributing properties in New York]]
[[Category:National Register of Historic Places in Westchester County, New York]]
[[Category:U.S. Route 9]]
[[Category:Colonial Revival architecture in New York]]
[[Category:Federal architecture in New York]]