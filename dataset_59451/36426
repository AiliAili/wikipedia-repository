{{Use dmy dates|date=September 2013}}
{{Use British English|date=September 2013}}
{{Infobox album
| Name          = Animals
| Type          = Studio album
| Artist        = [[Pink Floyd]]
| Cover         = Pink Floyd-Animals-Frontal.jpg
| Alt           = An image of the Battersea Power Station in England, where a giant pig can be seen flying between its left chimneys.
| Released      = {{Start date|1977|1|23|df=yes}}
| Recorded      = April–December 1976
| Studio        = [[Britannia Row Studios]], [[London]]
| Genre         = [[Progressive rock]]<ref>{{cite journal|last=Greene|first=Andy|date=16 August 2013|url=http://www.rollingstone.com/music/news/weekend-rock-question-what-is-the-best-prog-rock-album-of-the-1970s-20130816|title=Weekend Rock Question: What Is the Best Prog Rock Album of the 1970s?|journal=[[Rolling Stone]]|location=New York|accessdate=18 July 2014}}</ref>
| Length        = {{Duration|m=41|s=41}}
| Label         = {{flatlist|
*[[Harvest Records|Harvest]]
*[[Columbia Records|Columbia]]
}}
| Producer      = [[Pink Floyd]]
| Border        = no
| Language      = 
| Last album    = ''[[Wish You Were Here (Pink Floyd album)|Wish You Were Here]]''<br />(1975)
| This album    = '''''Animals'''''<br />(1977)
| Next album    = ''[[The Wall]]''<br />(1979)
| Misc          = 
}}

'''''Animals''''' is the tenth [[album|studio album]] by the English [[rock music|rock]] band [[Pink Floyd]], released on 23 January 1977 by [[Harvest Records]] in the United Kingdom and by [[Columbia Records]] in the United States. It is a [[concept album]] that provides a scathing critique of the social-political conditions of late-70s Britain, and presents a marked change in musical style from their earlier work. ''Animals'' was recorded at the band's studio, Britannia Row, in London, but its production was punctuated by the early signs of discord that three years later would culminate in keyboardist [[Richard Wright (musician)|Richard Wright]] leaving the band. The album's cover image, a pig floating between two chimneys of the [[Battersea Power Station]], was conceived by the band's bassist and lead songwriter [[Roger Waters]], and was designed by long-time collaborator [[Storm Thorgerson]] of [[Hipgnosis]].

The album was released to generally positive reviews in the United Kingdom, where it reached number 2 on the [[UK Albums Chart]]. It was also a success in the United States, reaching number 3 on the [[Billboard 200|US ''Billboard'' 200]] chart, and although it scored on US charts for half a year, steady sales have resulted in its certification by the [[RIAA certification|RIAA]] at [[Music recording sales certification|4x platinum]].  The size of the venues of the band's [[In the Flesh (1977 Pink Floyd 'Animals' tour)|In the Flesh Tour]] prompted an incident in which Waters spat at members of the audience, setting the background for the band's next studio album ''[[The Wall]]'', released two years later.

==Recording==
In 1975, Pink Floyd bought a three-storey block of church halls at [[Britannia Row Studios|35 Britannia Row]] in [[Islington]], north London. Their deal with Harvest Records' parent company [[EMI]] for unlimited studio time in return for a reduced percentage of sales had expired, and they converted the building into a recording studio and storage facility. Its construction took up most of 1975, and in April 1976 the band started work on their tenth studio album, ''Animals'', at the new facility.<ref name="Masonpp218220">{{Harvnb|Mason|2005|pp=218–220}}</ref><ref>{{Harvnb|Blake|2008|p=239}}</ref>

''Animals'' was engineered by a previous Floyd collaborator, Brian Humphries,<ref name="Masonpp218220"/> and recording took place at Britannia Row from April to December 1976, continuing into early 1977.<ref name="Poveyp208">{{Harvnb|Povey|2007|p=208}}</ref>  "Raving and Drooling" and "You've Got to Be Crazy", two songs previously performed live and considered for ''Wish You Were Here'', reappeared as "Sheep" and "Dogs" respectively.<ref name="Masonpp218220"/>  They were reworked to fit the new concept, and separated by a Waters-penned composition, "Pigs (Three Different Ones)".  With the exception of "Dogs" (co-written by Gilmour) the album's five tracks were written by Waters. The song contains references to Waters' private life; his new romantic interest was [[Carolyne Christie|Carolyne Anne Christie]] (married to [[Rock Scully]], manager of the Grateful Dead).<ref>{{Harvnb|Blake|2008|pp=244–245}}</ref>  Gilmour was distracted by the birth of his first child, and contributed little else towards the songwriting of the album.  Similarly, neither Mason nor Wright contributed as much as they had on previous albums, and ''Animals'' was the first Pink Floyd album not to contain a composer's credit for Wright.<ref>{{Harvnb|Blake|2008|pp=242–243}}</ref>

{{Quote box |quoted=true |bgcolor=#FFFFF0 |salign=center|quote=Roger's thing is to dominate, but I am happy to stand up for myself and argue vociferously as to the merits of different pieces of music, which is what I did on ''Animals''.  I didn't feel remotely squeezed out of that album.  Ninety per cent of the song "Dogs" was mine.  That song was almost the whole of one side, so that's half of ''Animals''.|source=– David Gilmour, ''Mojo'' (2008)<ref>{{Citation|last=Blake |first=Mark |title=Read David Gilmour Summer 2008 Interview for Mojo. Great Read! |url=http://www.neptunepinkfloyd.co.uk/index.php/npf-mag/426-david-gilmour-mojo-interview |publisher=neptunepinkfloyd.co.uk |year=2008 |accessdate=14 October 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20090905174714/http://www.neptunepinkfloyd.co.uk/index.php/npf-mag/426-david-gilmour-mojo-interview |archivedate=5 September 2009 }}</ref>|align=right|width=33%}}

The band had discussed employing another guitarist for future tours, and [[Snowy White]] was therefore invited into the studio. When Waters and Mason inadvertently erased one of Gilmour's completed guitar solos, White was asked to record a solo on "Pigs on the Wing". Although his performance was omitted from the vinyl release, it was included on the [[8-track tape|eight-track cartridge]] version. White later performed on the ''Animals'' tour.<ref name="Masonpp218220"/>  Mason recalled that he enjoyed working on ''Animals'' more than he had working on ''Wish You Were Here''.<ref>{{Harvnb|Mason|2005|p=220}}</ref>

==Concept==
Loosely based on [[George Orwell]]'s political fable ''[[Animal Farm]]'', the album's lyrics describe various classes in society as different kinds of animals: the combative dogs, despotic ruthless pigs, and the "mindless and unquestioning herd" of sheep.<ref name="Schaffnerp199">{{Harvnb|Schaffner|1991|p=199}}</ref>  Whereas the novella focuses on [[Stalinism]], the album is a critique of [[capitalism]] and differs again in that the sheep eventually rise up to overpower the dogs.<ref name="Schaffnerp199"/><ref>{{Harvnb|Blake|2008|pp=241–242}}</ref>  The album was developed from a collection of unrelated songs into a concept which, in the words of author Glenn Povey, "described the apparent social and moral decay of society, likening the human condition to that of mere animals".<ref>{{Harvnb|Povey|2007|p=200}}</ref>

Apart from its critique of society, the album is also a part-response to the [[punk rock]] movement,<ref>{{citation |url=https://books.google.com/books?id=U3rJxPYT32MC&pg=PA610 |author1=Browne, Pat |title=The guide to United States popular culture |chapter=Pink Floyd |page=610 |date=15 June 2001 |isbn=978-0-87972-821-2}}</ref> which grew in popularity as a [[nihilism|nihilistic]] statement against the prevailing social and political conditions, and also a reaction to the general complacency and nostalgia that appeared to surround [[rock music]].  Pink Floyd were an obvious target for punk musicians, notably [[John Lydon|Johnny Rotten]] of [[The Sex Pistols]], who wore a Pink Floyd T-shirt on which the words "I hate" had been written in ink; Lydon, however, has constantly said it was done for a laugh (he was a fan of several progressive rock bands of the era, including [[Magma (band)|Magma]] and [[Van Der Graaf Generator]]).  Drummer [[Nick Mason]] later stated that he welcomed the "Punk Rock insurrection" and viewed it as a welcome return to the underground scene from which Pink Floyd originated.  In 1977 he produced [[The Damned (band)|The Damned]]'s second album, ''[[Music for Pleasure (The Damned album)|Music for Pleasure]]'', at Britannia Row.<ref>{{Harvnb|Schaffner|1991|pp=194–196}}</ref>

{{listen | pos = right | filename = Pink floyd pigs.ogg | title = "Pigs (Three Different Ones)" | description = The "[[Pigs (Three Different Ones)|Pigs]]" on ''Animals'' represent the people whom Waters viewed as being at the top of the social ladder | format = [[Ogg]]}}
In his 2008 book ''Comfortably Numb'', author [[Mark Blake (writer)|Mark Blake]] argues that "[[Dogs (Pink Floyd song)|Dogs]]" contains some of [[David Gilmour]]'s finest work; although the guitarist sings only one lead vocal, his performance is "explosive".<ref name="Blakep243">{{Harvnb|Blake|2008|p=243}}</ref>  The song also contains notable contributions from keyboardist [[Richard Wright (musician)|Richard Wright]], which echo the funereal synthesizer sounds used on the band's previous album, ''[[Wish You Were Here (Pink Floyd album)|Wish You Were Here]]''.  

"[[Pigs (Three Different Ones)]]" is audibly similar to "[[Have a Cigar]]", with bluesy guitar fills and elaborate bass lines.  Of the song's three pigs, the only one directly identified is morality campaigner [[Mary Whitehouse]], who amongst other things is described as a "house-proud town mouse".<ref name="Blakepp243244">{{Harvnb|Blake|2008|pp=243–244}}</ref> 

"[[Sheep (song)|Sheep]]" contains a modified version of [[Psalm 23]], which continues the traditional "The Lord is my shepherd" with words like "he maketh me to hang on hooks in high places and converteth me to lamb cutlets" (referring to the sheep of the title).  Towards the end of the song, the eponymous sheep rise up and kill the dogs, but later retire back to their homes.  

The album is book-ended by each half of "[[Pigs on the Wing]]", a simple love song in which a glimmer of hope is offered despite the anger expressed in the album's three other songs. Described by author Andy Mabbett as "[sitting] in stark contrast to the heavyweight material between them",<ref>{{Harvnb|Mabbett|1995|p=70}}</ref> the two halves of the song were heavily influenced by Waters' relationship with his then-wife.<ref name="Blakepp243244"/><ref name="Mabbettpp7071">{{Harvnb|Mabbett|1995|pp=70–71}}</ref>

==Packaging==
{{See also|Pink Floyd pigs}}
[[File:Battersea Power Station in London.jpg|alt=Photo of a large building with four tall chimneys.|right|thumb|[[Battersea Power Station]] is the subject for the album's cover image.]]
Once the album was complete, work began on its cover.  Hipgnosis, designer of the band's previous album covers, offered three ideas, one of which was a small child entering his parents' bedroom to find them having sex: "copulating, like animals!"<ref>{{Harvnb|Blake|2008|p=245}}</ref>  The final concept was, unusually, designed by Waters.  At the time he lived near [[Clapham Common]], and regularly drove past [[Battersea Power Station]], which was by then approaching the end of its useful life.  A view of the building was chosen for the cover image, and the band commissioned German company Ballon Fabrik (who had previously constructed [[Zeppelin]] airships)<ref>{{Harvnb|Povey|2007|p=201}}</ref> and Australian artist [[Jeffrey Shaw]]<ref name=medienkunstnetz>{{Citation|url=http://www.medienkunstnetz.de/works/pig-for-pink-floyd/|title=Jeffrey Shaw, Pig for Pink Floyd|publisher=medienkunstnetz.de|accessdate=21 May 2009}}</ref> to build a {{convert|40|ft|m|adj=on}} porcine balloon (known as ''Algie'').  The balloon was inflated with [[helium]] and maneuvered into position on 2&nbsp;December 1976, with a marksman ready to fire if it escaped.  Unfortunately inclement weather delayed work, and the band's manager [[Steve O'Rourke]] neglected to book the marksman for a second day; the balloon broke free of its moorings and disappeared from view.  The pig flew over [[Heathrow]], resulting in panic and cancelled flights; pilots also spotted the pig in the air.  It eventually landed in [[Kent]] and was recovered by a local farmer, who was apparently furious that it had "scared his cows".<ref name="Blakep246">{{Harvnb|Blake|2008|p=246}}</ref>  The balloon was recovered and filming continued for a third day, but as the early photographs of the power station were considered better, the image of the pig was later superimposed onto one of those.<ref name="Blakep246"/><ref>{{Harvnb|Mason|2005|pp=223–225}}</ref>

During the "Isles of Wonder" short film shot by [[Danny Boyle]] and shown as part of the Opening Ceremonies of the [[2012 Summer Olympics]] in London, the camera zooms down the length of the River Thames, from a small spring in the countryside all the way to the Olympic venue. During the fly-by, a pig can be seen floating above Battersea Power Station.<ref>{{cite web|url=http://www.nbcolympics.com/video/2012/opening-ceremony-the-isles-of-wonder.html |title=Opening Ceremony: The Isles of Wonder&nbsp;– Video |publisher=NBC Olympics |date= |accessdate=30 July 2012}}</ref>

The album's theme continues onto the record's picture labels.  Side one's label shows a [[fisheye lens]] view of a dog and the English countryside, and side two features a pig and sheep, in the same setting.  Mason's handwriting is used as a [[typeface]] throughout the packaging.  The [[gatefold]] features monochrome photographs of the dereliction around the power station.

==Release==
{{Album ratings
| subtitle = Retrospective reviews
| rev1 = [[AllMusic]]
| rev1Score = {{rating|4|5}}<ref name="allmusic">{{Allmusic |class=album |id=animals-mw0000191390 |label=Album review |first=Stephen Thomas |last=Erlewine |accessdate=30 April 2013}}</ref>
| rev2 = ''[[The Daily Telegraph]]''
| rev2Score = {{Rating|3|5}}<ref>{{cite news|last=McCormick|first=Neil|date=20 May 2014|url=http://www.telegraph.co.uk/culture/music/8790376/Pink-Floyds-14-studio-albums-rated.html|title=Pink Floyd's 14 studio albums rated|newspaper=[[The Daily Telegraph]]|location=London|accessdate=18 July 2014}}</ref>
| rev3 = ''[[Encyclopedia of Popular Music]]''
| rev3Score = {{Rating|4|5}}{{sfn|Larkin|2011|pp=2065–66}}
| rev4 = ''[[The Great Rock Discography]]''
| rev4Score = 8/10<ref name="Acclaimed">{{cite web | title = Pink Floyd ''Animals'' | url = http://www.acclaimedmusic.net/Current/A2675.htm | publisher = [[Acclaimed Music]] | accessdate = 29 August 2015}}</ref>
|rev5 = [[MusicHound]]
|rev5score = 3/5<ref>{{cite book | last1 = Graff | first1 = Gary | last2 = Durchholz | first2 = Daniel (eds) | title = MusicHound Rock: The Essential Album Guide | publisher = Visible Ink Press | location = Farmington Hills, MI | year = 1999 |page=872 | isbn = 1-57859-061-2}}
</ref>
| rev6 = [[Pitchfork Media]]
| rev6Score = 10/10<ref>{{Citation|url=http://pitchfork.com/reviews/albums/6307-animals/|title=Album review|publisher=pitchfork.com|accessdate=4 July 2011}}</ref>
| rev7 = [[PopMatters]]
| rev7Score = 9/10<ref>{{cite web|last=Garratt|first=John|date=22 November 2011|url=http://www.popmatters.com/review/149844-pink-floyd-animals/|title=Pink Floyd: Animals|publisher=[[PopMatters]]|accessdate=18 July 2014}}</ref>
| rev8 = ''[[The Rolling Stone Album Guide]]''
| rev8Score = {{Rating|2|5}}<ref name="RSguide">{{cite web|last=Sheffield|first=Rob|authorlink=Rob Sheffield|url=http://www.rollingstone.com/music/artists/pink-floyd/albumguide|archiveurl=https://web.archive.org/web/20110217230328/http://www.rollingstone.com/music/artists/pink-floyd/albumguide|title=Pink Floyd: Album Guide|work=[[Rolling Stone]]|publisher=[[Jann Wenner|Wenner Media]], [[Fireside Books]]|date=2 November 2004|archivedate=17 February 2011|accessdate=27 December 2014}}</ref>
}}
{{Quote box |quoted=true |bgcolor=#FFFFF0 |salign=center|quote=''Animals'' was a slog.  It wasn't a fun record to make, but this was when Roger ''really'' started to believe that he was the sole writer for the band.  He believed that it was only because of him that the band was still going, and obviously, when he started to develop his ego trips, the person he would have his conflicts with would be me.|source=–Richard Wright<ref name="Blakep243"/>|align=right|width=33%}}
The album's release followed [[Capital Radio]]'s broadcast two days earlier of ''The Pink Floyd Story'', and an evening press conference held at the power station two days before that.<ref name="Poveyp208"/>  The broadcast was originally to have been an exclusive for the London-based station, who since mid-December had been broadcasting ''The Pink Floyd Story'', but a copy was given to [[John Peel]], who played side one of the album in its entirety a day earlier.<ref name="Poveyp208"/><ref name="Blakep246"/>

''Animals'' was released in the UK on 23&nbsp;January 1977,<ref name="Blakep246"/>{{#tag:ref|Povey (2007) suggests the album was released on 21&nbsp;January, Mason (2005) suggests 28&nbsp;January|group="nb"}} and in the US on 12&nbsp;February.  It reached number two in the UK, and three in the US.<ref name="Poveyp347">{{Harvnb|Povey|2007|p=347}}</ref> Thanks to the album and the band's back catalogue, noted ''[[British Hit Singles & Albums|The Guinness Book of British Hit Albums]]'', "Pink Floyd bested [[ABBA]] for most weeks on [[UK Albums Chart|chart]] ''(in 1977)'', 108 to 106."<ref>*Roberts, David (editor). ''[[British Hit Singles & Albums|The Guinness Book of British Hit Albums]]'', p18. Guinness Publishing Ltd. 7th edition (1996). ISBN 0-85112-619-7</ref>

''[[NME]]'' called ''Animals'' "one of the most extreme, relentless, harrowing and downright iconoclastic hunks of music to have been made available this side of the sun",<ref name="Blakep247">{{Harvnb|Blake|2008|p=247}}</ref> and ''Melody Maker''{{'s}} Karl Dallas described it as "[an] uncomfortable taste of reality in a medium that has become in recent years, increasingly soporific".<ref name="Blakep247"/>  ''[[Rolling Stone]]''{{'s}} Frank Rose was unimpressed, writing: "The 1977 Floyd has turned bitter and morose.  They complain about the duplicity of human behavior (and then title their songs after animals – get it?).  They sound like they've just discovered this – their message has become pointless and tedious."<ref name="Rollingstonereview">{{Citation | last = Rose | first = Frank | title = Pink Floyd ''Animals'' | url = http://www.rollingstone.com/artists/pinkfloyd/albums/album/89221/review/5943065/animals  | publisher = rollingstone.com, archived at web.archive.org | date = 24 March 1977 | accessdate = 13 October 2009 | archiveurl = https://web.archive.org/web/20080618145836/http://www.rollingstone.com/artists/pinkfloyd/albums/album/89221/review/5943065/animals | archivedate = 18 June 2008}}</ref> [[Robert Christgau]] of ''[[The Village Voice]]'' gave the album a "B+" rating and found the negative reaction overly cynical, reasoning that the album functions simply as "a piece of well-constructed political [[program music]]&nbsp;... lyrical, ugly, and rousing, all in the right places".<ref name="Christgau">{{cite news|last=Christgau|first=Robert|authorlink=Robert Christgau|date=25 April 1977|url=http://www.robertchristgau.com/xg/cg/cgv4-77.php|title=Christgau's Consumer Guide|newspaper=[[The Village Voice]]|location=New York|accessdate=18 July 2014}}</ref>

In his 2004 autobiography ''[[Inside Out: A Personal History of Pink Floyd|Inside Out]]'', Nick Mason suggests that the album's perceived harshness, when compared to previous Floyd releases, may be a result of a "workman-like mood in the studio", and an unconscious reaction to the accusations from the aforementioned punk genre that bands like Pink Floyd represented "dinosaur rock".<ref>{{Harvnb|Mason|2005|pp=220–221}}</ref>  Animals was certified by the RIAA as 4× [[Music recording sales certification|Platinum]] on 31&nbsp;January 1995.<ref>{{Citation|title=Searchable database |url=http://www.riaa.com/goldandplatinumdata.php?table=SEARCH |publisher=riaa.com |accessdate=13 October 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20070626050454/http://www.riaa.com/goldandplatinumdata.php?table=SEARCH |archivedate=26 June 2007 |df=dmy }}</ref>

===Reissues===
Originally released on [[Harvest Records]] in the UK and [[Columbia Records]] in the US, ''Animals'' was issued on [[Compact Disc]] (CD) in 1985,{{#tag:ref|EMI CDP 7461282|group="nb"}} and in the US in 1987.{{#tag:ref|Columbia CK 34474|group="nb"}}  It was reissued as a digitally remastered CD with new artwork in 1994,{{#tag:ref|EMI CD EMD 1060|group="nb"}} and as a digitally remastered limited-edition vinyl album in 1997.{{#tag:ref|EMI EMD 1116|group="nb"}}  An anniversary edition was released in the US in the same year,{{#tag:ref|Columbia CK 68521|group="nb"}} followed in 2000 by a reissue from Capitol Records.{{#tag:ref|Capitol CDP 724382974826|group="nb"}}<ref name="Poveyp347"/>  The album was also included in the ''[[Shine On (box set)|Shine On]]'' box set in 1992, and in the 2011 [[Why Pink Floyd...?]] re-release series both in the box set and as a standalone 'Discovery' edition CD{{#tag:ref|EMI 50999 028951 2 3|group="nb"}}.

===Tour===
<!-- [[File:Soldier Field Chicago aerial view.jpg|alt=Photo of a large, empty stadium with a city skyline in the background.|right|thumb|The band played at [[Soldier Field]] in [[Chicago]] during their [[In the Flesh Tour]] in 1977.]] -->
The album became the subject material for the band's [[In the Flesh (1977 Pink Floyd 'Animals' tour)|In the Flesh Tour]], which began in [[Dortmund]] on the same day the album was released.  The tour continued through continental Europe in February, the UK in March, the US for three weeks in April and May, and another three weeks in the US in June and July.  ''Algie'' became the inspiration for a number of pig themes used throughout.  An inflatable pig was floated over the audience, and during each performance was replaced with a cheaper, but explosive version.  On one occasion the mild [[propane]] gas was replaced with an oxygen-acetylene mixture, producing a massive (and dangerous) explosion.  German promoter Marcel Avram presented the band with a piglet in [[Munich]], only for it to leave a trail of broken mirrors and excrement across its mirrored hotel room, leaving manager O'Rourke to deal with the resulting fallout.<ref>{{Harvnb|Mason|2005|pp=225–226}}</ref>

The band were joined by familiar figures such as [[Dick Parry]] and [[Snowy White]],<ref>{{Harvnb|Blake|2008|pp=248–249}}</ref> but relations within the band became fraught.  Waters took to arriving at the venues alone, departing as soon as each performance was over.  On one occasion, Wright flew back to England, threatening to leave the band.  The size of the venues was also an issue; in [[Chicago]], the promoters claimed to have sold out the 67,000 person regular capacity of the [[Soldier Field]] stadium (after which ticket sales should have been ended), but Waters and O'Rourke were suspicious.  They hired a helicopter, photographer and attorney, and discovered that the actual attendance was 95,000; a shortfall to the band of $640,000.<ref>{{Harvnb|Blake|2008|pp=252–253}}</ref>  The end of the tour was a low point for Gilmour, who felt that they had by now achieved the success they originally sought, and that there was nothing else they could look forward to.<ref>{{Harvnb|Mason|2005|p=230}}</ref>  In July 1977 – on the final date at the [[Olympic Stadium (Montreal)|Montreal Olympic Stadium]] – a small group of noisy and excited fans in the front row of the audience irritated Waters to such an extent that he spat at one of them.  He was not the only person who felt depressed about playing to such large audiences, as Gilmour refused to perform a third encore.<ref name="Masonpp235236"/>{{sfn|Povey|2007|p=217}}  Waters later spoke with producer [[Bob Ezrin]] and told him of his sense of alienation on the tour, and how he sometimes felt like building a wall to separate himself from the audience.  The spitting incident would later form the basis of a new concept,<ref name="Masonpp235236">{{Harvnb|Mason|2005|pp=235–236}}</ref> which would eventually become one of the band's most successful album releases, ''[[The Wall]]''.

==Track listing==
<!-- DO ''not'' CHANGE TRACK TIMES. SEE TALK PAGE (archive 1) FOR MORE INFORMATION. -->All tracks written and all lead vocals performed by [[Roger Waters]], except where noted.
{{Track listing
| music_credits   = yes
| wi   = yes
| extra_column    = Lead vocals
| headline        = Side one
| total_length    = 18:28
| title1          = [[Pigs on the Wing|Pigs on the Wing 1]]
| music1          = 
| extra1          = 
| length1         = 1:25
| title2          = [[Dogs (Pink Floyd song)|Dogs]]
| music2          = Waters, Gilmour
| extra2          = Gilmour, Waters
| length2         = 17:03
}}

{{Track listing
| music_credits   = yes
| extra_column    = Lead vocals
| headline        = Side two
| total_length    = 23:13
| title1          = [[Pigs (Three Different Ones)]]
| music1          = 
| extra1          = 
| length1         = 11:25
| title2          = [[Sheep (song)|Sheep]]
| music2          = 
| extra2          = 
| length2         = 10:25
| title3          = [[Pigs on the Wing|Pigs on the Wing 2]]
| music3          = 
| extra3          = 
| length3         = 1:23
}}

==Personnel==
===Pink Floyd===
*[[David Gilmour]] - [[lead guitar]], [[Singing|co-lead vocals]], [[rhythm guitar|rhythm]] and [[acoustic guitar]] on "Dogs", [[bass guitar]] on "Pigs (Three Different Ones)" and "Sheep", [[talk box]] on "Pigs (Three Different Ones)"
*[[Nick Mason]] - [[drum kit|drums]], [[percussion]], [[tape editing|tape effects]]
*[[Roger Waters]] - [[singing|lead]] and [[singing|harmony vocals]], [[acoustic guitar]] on "Pigs on the Wing", [[rhythm guitar]] on "Pigs (Three Different Ones)" and "Sheep", [[tape editing|tape effects]], [[vocoder]], [[bass guitar]] on "Dogs"
*[[Richard Wright (musician)|Richard Wright]] - [[Hammond organ]], [[rhodes piano|electric piano]], [[Minimoog]], [[ARP String Synthesizer|ARP string synthesizer]], [[piano]], [[clavinet]], [[vocal harmony|harmony vocals]] on "Dogs"

===Production===
*Pink Floyd
*Brian Humphries&nbsp;– [[audio engineer|engineering]]
*Roger Waters&nbsp;– sleeve concept
*[[Storm Thorgerson]]&nbsp;– sleeve design (organiser)
*Aubrey Powell&nbsp;– sleeve design (organiser), photography
*Nick Mason&nbsp;– [[graphics]]
*[[Peter Christopherson]]&nbsp;– [[photography]]
*Howard Bartrop&nbsp;– photography
*Nic Tucker&nbsp;– photography
*Bob Ellis&nbsp;– photography
*Rob Brimson&nbsp;– photography
*Colin Jones&nbsp;– photography
*E. R. G. Amsterdam&nbsp;– inflatable pig design

==Charts and certifications==
{{col-start}}
{{col-2}}

===Charts===
{|class="wikitable sortable"
|-
! style="width:20em;"|Chart (1977)
!Peak<br />position
|-
|Australia ([[Kent Music Report]])<ref>{{cite book|last=Kent|first=David|authorlink=David Kent (historian)|title=Australian Chart Book 1970–1992|publisher=Australian Chart Book|location=St. Ives, N.S.W.|year=1993|edition=Illustrated|page=233|isbn=0-646-11917-6}}</ref>
|align="center"|3
|-
|{{albumchart|Austria|2|artist=Pink Floyd|album=Animals|accessdate=5 August 2014}}
|-
|{{albumchart|Canada|12|chartid=5269a|artist=Pink Floyd|album=Animals|accessdate=9 June 2016}}
|-
|{{albumchart|Netherlands|1|artist=Pink Floyd|album=Animals|accessdate=5 August 2014}}
|-
|{{albumchart|Germany4|1|M|artist=Pink Floyd|album=Animals|url=http://www.musikmarkt.de/Charts/Chartsgalerie/Albumcharts/Albumcharts-1977/15.04.1977|title=15.04.1977|publisher=Musikmarkt GmbH & Co. KG|work=Musikmarkt|accessdate=9 June 2016}}
|-
|{{albumchart|New Zealand|1|M|artist=Pink Floyd|album=Animals|url=https://web.archive.org/web/20160507031017/http://charts.org.nz/weekchart.asp?year=1977&date=19770327&cat=a|title=New Zealand charts portal (27/03/1977)|work=charts.org.nz|accessdate=9 June 2016}}
|-
|{{albumchart|Norway|2|M|artist=Pink Floyd|album=Animals|url=https://web.archive.org/web/20160424031157/http://norwegiancharts.com/weekchart.asp?year=1977&date=197711&cat=a|title=Norwegian charts portal (11/1977)|work=norwegiancharts.com|accessdate=9 June 2016}}
|-
|{{albumchart|Sweden|3|artist=Pink Floyd|album=Animals|accessdate=5 August 2014}}
|-
|{{albumchart|UK|2|artist=Pink Floyd|accessdate=9 June 2016}}
|-
|{{albumchart|Billboard200|3|artist=Pink Floyd|accessdate=9 June 2016}}
|-
! style="width:20em;"|Chart (2006)
!Peak<br />position
|-
|{{albumchart|Italy|36|M|artist=Pink Floyd|album=Animals|url=http://www.fimi.it/classifiche#/category:album/id:102|accessdate=22 June 2016|title=Archivio - Album - Classifica settimanale WK 30 (dal 24-07-2006 al 30-07-2006)|publisher=[[Federation of the Italian Music Industry]]}}
|-
! style="width:20em;"|Chart (2010)
!Peak<br />position
|-
|{{albumchart|Italy|79|M|artist=Pink Floyd|album=Animals|url=http://www.fimi.it/classifiche#/category:album/id:305|accessdate=5 August 2014|title=Archivio - Album - Classifica settimanale WK 44 (dal 01-11-2010 al 07-11-2010)|publisher=Federation of the Italian Music Industry}}
|-
! style="width:20em;"|Chart (2011)
!Peak<br />position
|-
|{{albumchart|France|80|M|artist=Pink Floyd|album=Animals|url=https://web.archive.org/web/20160722134932/http://www.lescharts.com/weekchart.asp?cat=a&year=2011&date=20111001|title=Les charts francais (01/10/2011)|work=lescharts.com|accessdate=9 June 2016}}
|-
|{{albumchart|Norway|37|M|artist=Pink Floyd|album=Animals|url=https://web.archive.org/web/20160424131308/http://norwegiancharts.com/weekchart.asp?cat=a&year=2011&date=201139|title=Norwegian charts portal (39/2011)|work=norwegiancharts.com|accessdate=9 June 2016}}
|-
|{{Albumchart|Switzerland|71|artist=Pink Floyd|album=Animals|accessdate=5 August 2014}}
|-
! style="width:20em;"|Chart (2012)
!Peak<br />position
|-
|{{Albumchart|Spain|76|artist=Pink Floyd|album=Animals|accessdate=5 August 2014}}
|-
|}

{{col-2}}

===Certifications===
{{Certification Table Top}}
{{Certification Table Entry|region=Austria|award=Gold|relyear=1977|certyear=1994|artist=Pink Floyd|title=Animals|accessdate=5 August 2014|type=album|autocat=true}}
{{Certification Table Entry|region=Canada|award=Platinum|number=2|relyear=1977|certyear=1979|artist=Pink Floyd|title=Animals|accessdate=5 August 2014|type=album|autocat=true}}
{{Certification Table Entry|region=France|award=Platinum|relyear=1977|artist=Pink Floyd|title=Animals|accessdate=5 August 2014|type=album|autocat=true|recent=false}}
{{Certification Table Entry|region=Germany|award=Platinum|relyear=1977|certyear=1990|artist=Pink Floyd|title=Animals|accessdate=5 August 2014|type=album|autocat=true}}
{{Certification Table Entry|region=Italy|award=Gold|relyear=1977|certyear=2014|artist=Pink Floyd|title=Animals|accessdate=5 August 2014|type=album|autocat=true}}
{{Certification Table Entry|region=United Kingdom|award=Gold|relyear=1977|certyear=1977|artist=Pink Floyd|title=Animals|accessdate=5 August 2014|type=album|autocat=true}}
{{Certification Table Entry|region=United States|award=Platinum|number=4|relyear=1977|certyear=1995|artist=Pink Floyd|title=Animals|accessdate=5 August 2014|type=album|autocat=true}}
{{Certification Table Bottom}}
{{col-end}}

==See also==
*''[[Live Frogs Set 2]]''

==References==
'''Notes'''
{{Reflist|group="nb"|30em}}

'''Footnotes'''
{{Reflist|30em}}

'''Bibliography'''
{{Refbegin}}
*{{Citation | last = Blake | first = Mark | authorlink = Mark Blake (writer) | title = Comfortably Numb&nbsp;– The Inside Story of Pink Floyd | publisher = Da Capo Press | year = 2008 | isbn = 0-306-81752-7}}
* {{cite book|ref=harv|last=Larkin|first=Colin|authorlink=Colin Larkin (writer)|title=[[The Encyclopedia of Popular Music]]|year=2011|publisher=[[Omnibus Press]]|isbn=0-85712-595-8}}
*{{Citation | last = Mason | first = Nick | authorlink = Nick Mason | title = Inside Out&nbsp;– A Personal History of Pink Floyd | publisher = Phoenix | edition = Paperback | editor = Philip Dodd | year = 2005 | isbn = 0-7538-1906-6}}
*{{Citation | last = Mabbett | first = Andy | title = The Complete Guide to the Music of Pink Floyd | publisher = Omnibus Pr | year = 1995 | isbn =  0-7119-4301-X}}
*{{Citation | last = Povey | first = Glenn | title = Echoes | url = https://books.google.com/books?id=qnnl3FnO-B4C | publisher = Mind Head Publishing | year = 2007 | isbn = 0-9554624-0-1}}
*{{Citation | last = Schaffner | first = Nicholas | title = Saucerful of Secrets | publisher = London: Sidgwick & Jackson | year = 1991 | edition = 1st | isbn = 0-283-06127-8}}
{{Refend}}

==External links==
{{Wikiquote}}

{{Animals (Pink Floyd album)}}
{{Pink Floyd}}

{{Good article}}

{{DEFAULTSORT:Animals (Album)}}
[[Category:1977 albums]]
[[Category:Albums produced by David Gilmour]]
[[Category:Albums produced by Nick Mason]]
[[Category:Albums produced by Richard Wright (musician)]]
[[Category:Albums produced by Roger Waters]]
[[Category:Albums with cover art by Hipgnosis]]
[[Category:Albums with cover art by Storm Thorgerson]]
[[Category:Capitol Records albums]]
[[Category:Columbia Records albums]]
[[Category:Concept albums]]
[[Category:Dystopian fiction]]
[[Category:Personifications]]
[[Category:EMI Records albums]]
[[Category:English-language albums]]
[[Category:Harvest Records albums]]
[[Category:Pink Floyd albums]]