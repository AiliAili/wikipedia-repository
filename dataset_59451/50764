{{Infobox journal
| title = Briefings in Bioinformatics
| cover = [[File:Briefings_in_Bioinformatics_cover.gif]]
| editor = Martin Bishop
| discipline = [[Bioinformatics]]
| former_names = 
| abbreviation = Brief. Bioinform.
| publisher = [[Oxford University Press]]
| country = 
| frequency = Bimonthly
| history = 2000-present
| openaccess = 
| license = 
| impact = 8.399
| impact-year = 2015
| website = http://bib.oxfordjournals.org/
| link1 = http://bib.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://bib.oxfordjournals.org/archive
| link2-name = Online archive
| JSTOR = 
| OCLC = 44107488
| LCCN = 
| CODEN = BBIMFX
| ISSN = 1467-5463
| eISSN = 1477-4054
}}
'''''Briefings in Bioinformatics''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering [[bioinformatics]], including reviews of databases and analytical tools for genetics and molecular biology. It is published by [[Oxford University Press]].
The [[EMBnet]] community was initially involved in the creation of the journal. BiB was also supported by an educational grant from EMBnet.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|2|
* [[Current Contents]]/Life Sciences
* [[Abstracts on Hygiene and Communicable Diseases]]
* [[Animal Breeding Abstracts]]
* [[BIOSIS Previews]]
* [[Biological Abstracts]]
* [[Biotechnology Citation Index]]
* [[CAB Abstracts]]
* [[EMBASE]]
* [[Global Health]]
* [[ProQuest]]
* [[Science Citation Index|Science Citation Index Expanded]]
}}
According to the ''[[Journal Citation Reports]]'', the journal had a 2012 [[impact factor]] of 5.298, ranking it 3rd out of 47 journals in the category "Mathematical & Computational Biology"<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Mathematical & Computational Biology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-10-18 |work=Web of Science |postscript=.}}</ref> and 9th out of 75 journals in the category "Biochemical Research Methods".<ref name=WoS2>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Biochemical Research Methods |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-10-18 |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://bib.oxfordjournals.org/}}

[[Category:Bimonthly journals]]
[[Category:Biomedical informatics journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Publications established in 2000]]
[[Category:English-language journals]]