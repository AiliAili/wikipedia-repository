{{EngvarB |date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{infobox book | 
| name          = The Eustace Diamonds
| image         = Camperdown and Lizzie.jpg
| image_size = 200px
| caption = Mr Camperdown asking Lizzie Eustace where the diamonds are
| author        = [[Anthony Trollope]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        = [[Palliser novels|Palliser]]
| genre         = [[Family saga|Family-saga novel]]
| publisher     = [[Fortnightly Review]] (serial); [[Chapman and Hall|Chapman & Hall]] (book)
| release_date  = July 1871 – February 1873 (serial); October 1872 (book)
| media_type    = Print ([[Serial (literature)|serial]], hard~ & paperback)
| pages         =
| preceded_by   = [[Phineas Finn]]
| followed_by   = [[Phineas Redux]]
}}
'''''The Eustace Diamonds''''' is a novel by [[Anthony Trollope]], first published in 1871 as a serial in the ''[[Fortnightly Review]]''. It is the third of the "[[Palliser novels|Palliser]]" series of novels.<ref name = bbc>{{cite web|title=The Eustace Diamonds Episode guide|url=http://www.bbc.co.uk/programmes/b01pmbq3/episodes/guide |publisher= BBC | place = [[United Kingdom|UK]] | accessdate=25 February 2014}}</ref><ref name= global>{{cite web|title=The Eustace Diamonds |url=http://global.oup.com/academic/product/the-eustace-diamonds-9780199587780;jsessionid=568ED44F0E4A9F17594A06EEDDF85784?cc=np&lang=en& |publisher= OUP |accessdate= 25 February 2014}}</ref><ref name=britannica>{{cite web|title=The Eustace Diamonds|url= http://www.britannica.com/EBchecked/topic/1780631/The-Eustace-Diamonds | work = Encyclopædia | publisher= Britannica |accessdate= 25 February 2014}}</ref><ref name=jstor>{{cite web |last=Kendrick|first=Walter M|title=The Eustace Diamonds: The Truth of Trollope's Fiction|url= http://www.jstor.org/discover/10.2307/2872607?uid=3738752&uid=2&uid=4&sid=21103518770067 |publisher= JStor |accessdate= 25 February 2014}}</ref><ref name=journals-cambridge>{{cite web |title=Victorian Literature and Culture | work = Journals | url= http://journals.cambridge.org/action/displayAbstract?fromPage=online&aid=7281064 |publisher= Cambridge |accessdate= 25 February 2014}}</ref><ref name =victorianweb>{{cite web|last= James|first= PD |title= The Eustace Diamonds |url= http://www.victorianweb.org/authors/trollope/tsociety/eustace.html | work = Authors | publisher= Victorian Web |accessdate= 25 February 2014}}</ref>

==Plot summary==
In this novel, the characters of [[Plantagenet Palliser]], his wife Lady Glencora and their uncle the ailing Duke of Omnium are in the background. The plot centres on Lizzie Greystock, a fortune-hunter who ensnares the sickly, dissipated Sir Florian Eustace and is soon left a very wealthy widow and mother. While clever and beautiful, Lizzie has several character flaws; the greatest of these is an almost pathological delight in lying, even when it cannot benefit her. (Trollope comments that Lizzie sees lies as "more beautiful than the truth.") Before he dies, the disillusioned Sir Florian discovers all this, but does not think to change the generous terms of his will.<ref name=librivox>{{cite web| last = Trollope | first = Anthony | author-link = Anthony Trollope | title=The Eustace Diamonds|url= https://librivox.org/the-eustace-diamonds-by-anthony-trollope/ |publisher= Libri vox |accessdate=25 February 2014}}</ref>

The diamonds of the book's title are a necklace, a family heirloom that Sir Florian gave to Lizzie to wear. Though they belong to her husband's estate (and thus eventually will be the property of her son), Lizzie refuses to relinquish them. She lies about the terms under which they were given to her, leaving their ownership unclear. The indignant Eustace family lawyer, Mr Camperdown, strives to retrieve the necklace, putting the Eustaces in an awkward position. On the one hand, the diamonds are valuable and Lizzie may not have a legal claim to them, but on the other, they do not want to antagonise the mother of the heir to the family estate (Lizzie having only a [[life interest]]).

Meanwhile, after a respectable period of mourning, Lizzie searches for another husband, a dashing "Corsair" more in keeping with her extravagantly romantic fantasies. She becomes engaged to a dull, but honourable politician, Lord Fawn, but they have a falling out when her character becomes better known, especially her determination to keep the diamonds. She then considers her cousin, Frank Greystock, even though he is already engaged to Lucy Morris, a poor but much beloved governess of the Fawn daughters. Greystock is a successful lawyer and Member of Parliament, but his income is inadequate to his position and spendthrift lifestyle. Lizzie believes he can shield her from the legal proceedings being initiated by Mr Camperdown.  Another more Corsair-like possibility is one of the guests at her Scottish home, the older Lord George de Bruce Carruthers, a man who supports himself in a somewhat mysterious manner.

Among the other guests is a young woman named Lucinda Roanoke, whose financially straitened aunt, Mrs Carbuncle, is desperate to marry her off. Despite Lucinda's deep detestation of the brutish Sir Griffin Tewett, the aunt has her way and the mismatched couple become engaged.

Things take a dramatic turn on a trip to London. Lizzie, out of fear of Mr Camperdown, keeps her diamonds with her in a conspicuous strongbox. One night, at an inn, the strongbox is stolen and everybody assumes the jewellery is lost. As it turns out, Lizzie had taken the gems out and put them under her pillow, but acting on her first instincts, she [[perjury|perjures]] herself when she has to report the theft to the magistrate, thinking that she can sell the diamonds and let the robbers take the blame. Suspicion falls on both Lizzie and Lord George, acting either together or separately. In any case, the thieves, aided by Lizzie's disloyal maid, Patience Crabstick, try again and succeed in their second attempt. Lizzie feigns illness and takes to her bed. Lady Glencora Palliser pays Lizzie a visit to offer her sympathy.

The police begin to unravel the mystery, putting Lizzie in a very uncomfortable position. In the end, the diamonds are lost, the police discover the truth, and Lizzie is forced to confess her lies, though she escapes legal retribution since her testimony is needed to convict the criminals. Both Frank Greystock and Lord George become disgusted by her conduct and desert her. Lucinda Roanoke grows to loathe Sir Griffin more and more intensely until, on what would have been the day of their wedding, she loses her sanity.  Frank Greystock returns to Fawn Court to marry Lucy Morris.  Mr Emilius, a foreign crypto-Jewish clergyman, woos Lizzie while she is in a vulnerable state and succeeds in marrying her (though it is hinted earlier in the book and is later confirmed in ''[[Phineas Redux]]'' that he is already married).

==Editions (selected)==
*''The Eustace Diamonds''. 3 vols. London, 1873 [1872].
*''The Eustace Diamonds''; with a preface by Michael Sadleir; illustrations by [[Blair Hughes-Stanton]]. 2 vols. London: Oxford University Press, 1950.
*''The Eustace Diamonds''; with an introduction by [[Simon Raven]]. [St. Albans]: Panther, [1968]
*''The Eustace Diamonds''; introduction by [[P. D. James]]. London: Trollope Society, 1990
*''The Eustace Diamonds''; the Royal edition. 2 vols.; plates. Philadelphia: Gebbie and Company, 1902.
*''The Oxford Trollope''; Crown edition. General editors: [[Michael Sadleir]] & Frederick Page. ''The Eustace Diamonds''. 2 vols. London: Oxford University Press, 1950

==References==
{{reflist |32em}}

==External links==
* {{gutenberg |no= 7381|name= The Eustace Diamonds}}
* {{librivox book | title=The Eustace Diamonds | author=Anthony Trollope}}

{{Anthony Trollope}}

{{DEFAULTSORT:Eustace Diamonds, The}}
[[Category:1871 novels]]
[[Category:Family saga novels]]
[[Category:Novels by Anthony Trollope]]
[[Category:Works originally published in Fortnightly Review]]
[[Category:Novels first published in serial form]]
[[Category:Chapman & Hall books]]