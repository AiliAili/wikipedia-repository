{{EngvarB|date=July 2014}}
{{Use dmy dates|date=July 2014}}
'''Tactical Unit''' (機動部隊, kei tung bou deui) is a series of films produced by [[Johnnie To]] with studio [[Milkyway Image]], featuring the adventures of two columns of [[Police Tactical Unit (Hong Kong)|PTU]] officers, the [[Kowloon West Police Station]] and its [[Criminal Investigation Department|CID]] [[police officers|officers]], of Hong Kong. The films are in Cantonese.

The film series spun off from the feature film directed by Johnnie To, ''[[PTU (film)|PTU: Police Tactical Unit]]'', giving it five sequels.<ref>YesAsia, [http://www.yesasia.com/us/tactical-unit-complete-series-dvd-5-disc-us-version/1020212255-0-0-0-en/info.html Tactical Unit – Complete Series] (accessed 21 Dec 2009)</ref><ref>Amazon.com, [http://www.amazon.com/dp/B002BPF2KK PTU – Tactical Unit] (accessed 21 December 2009)</ref><ref>The Film Catalogue, [http://www.thefilmcatalogue.com/catalog/FilmDetail.php?id=5437 "Tactical Unit 1–5"] (accessed 2009)</ref> Original conception on PTU started before 2002.<ref>Time, [http://www.time.com/time/arts/article/0,8599,172589,00.html "Fulltime Filmmaker"], '''Richard Corliss''', ''Monday, 27 August 2001'' (accessed 22 December 2009)</ref>

==Series==
Films in the series include:

{|class=wikitable
|-
! International title 
! Traditional Chinese title 
! Cantonese title 
! English translation 
! Year 
! Director 
! Notes 
! References
|-
| [[PTU: Police Tactical Unit]]
| PTU
| PTU
| [[PTU (film)|PTU]]
| 2003
| [[Johnnie To]]
|
: Also called ''Tactical Unit: Into the Perilous Night'' <ref>Telegraph (London), [http://www.telegraph.co.uk/culture/film/3666028/A-film-as-emotional-as-the-songs-she-sang.html "A film as emotional as the songs she sang"], '''Sukhdev Sandhu''', ''12:01AM BST 22 Jun 2007'' (accessed 22 Dec 2009)</ref>
: Also called ''Police Tactical Unit''<ref>ScreenHead, [http://www.screenhead.com/reviews/police-tactical-unit-debuts-on-dvd-march-25/ "POLICE TACTICAL UNIT debuts on DVD March 25"] (accessed 12 December 2009)</ref>
|<ref>{{imdb title|0250638|PTU}}</ref><ref>HK Cinemagic, [http://www.hkcinemagic.com/en/movie.asp?id=3819 PTU: Into The Perilous Night] (accessed 21 Dec 2009)</ref><ref>Love HK Film, [http://www.lovehkfilm.com/reviews/ptu.htm PTU] (accessed 21 Dec 2009)</ref><ref>HKMDB, [http://hkmdb.com/db/movies/view.mhtml?id=10393&display_set=big5 "PTU"] (accessed 30 Dec 2009)</ref>
|-
| [[Tactical Unit: The Code]] <!-- (DVD 6864) --> 
| 機動部隊─警例 
| Kei Tung Bou Deui: Ging Lai
| Mobile Tactical Unit – Police Cases
| 2008
| [[Wing-cheong Law]]
|
: This film is sometimes called ''PTU 2''<ref name=HKCM-Code>HK Cinemagic, [http://www.hkcinemagic.com/en/movie.asp?id=10604 Tactical Unit: The Code] (accessed 21 December 2009)</ref><ref>Amazon.com, [http://www.amazon.com/dp/B001UUO4RY Tactical Unit – The Code (HK Edition) DVD a.k.a. PTU 2 (2008)] (accessed 21 Dec 2009)</ref>
|<ref name=HKCM-Code/><ref>Love HK Film, [http://www.lovehkfilm.com/reviews_2/tactical_unit_code.html "The Code"] (accessed 21 Dec 2009)</ref><ref>{{imdb title|1256635|Tactical Unit: The Code}}</ref><ref>HKMDB, [http://hkmdb.com/db/movies/view.mhtml?id=13492&display_set=eng "機動部隊之警例"] (accessed 30 December 2009)</ref> 
|-
| [[Tactical Unit: No Way Out]] <!-- (DVD 6865) --> 
| 機動部隊─絕路 
| Kei Tung Bou Deui: Juet Lou
| Mobile Tactical Unit – Road to Ruin
| 2009
| [[Lawrence Ah Mon]] 
|
|<ref>HK Cinemagic [http://www.hkcinemagic.com/en/movie.asp?id=10854 Tactical Unit: No Way Out] (accessed 21 December 2009)</ref><ref>{{imdb title|1401663|Tactical Unit: No Way Out}}</ref><ref>HKMDB, [http://hkmdb.com/db/movies/view.mhtml?id=13798&display_set=eng "機動部隊絕路"] (accessed 2009-30-12)</ref>
|-
| [[Tactical Unit: Human Nature]] <!-- (DVD 6866) --> 
| 機動部隊─人性 
| Kei Tung Bou Deui: Yan Sing
| Mobile Tactical Unit – Humanity
| 2009
| [[Andy Ng]]
|
|<ref>HK Cinemagic, [http://www.hkcinemagic.com/en/movie.asp?id=10855 Tactical Unit: Human Nature] (accessed 21 December 2009)</ref><ref>{{imdb title|1401664|Tactical Unit: Human Nature}}</ref><ref>HKMDB, [http://hkmdb.com/db/movies/view.mhtml?id=13806&display_set=eng "機動部隊-人性"] (accessed 30 December 2009)</ref>
|-
| [[Tactical Unit: Comrades in Arms]] 
| 機動部隊─衕袍 
| Kei Tung Bou Deui: Tung Pou
| Mobile Tactical Unit – Colleagues 
| 2009
| [[Wing-cheong Law]] 
|
: This film is also called ''PTU 2''<ref name=HKCM-Comrades>Hong Kong Cinemagic, [http://www.hkcinemagic.com/en/movie.asp?id=10729 PTU2] (accessed 21 Dec 2009)</ref><ref name=IMDB-Comrades/><ref>CCTV, [http://www.cctv.com/program/cultureexpress/20090427/101705.shtml 'The movie "P-T-U 2"'], ''04-27-2009 08:54'' (accessed 22 Dec 2009)</ref>
: This film is also called ''Tactical Unit''<ref>Yahoo! Malaysia – Movies, [http://malaysia.movies.yahoo.com/Tactical+Unit/movie/15082/ "Tactical Unit (2009) – Movie"] (accessed 12 Dec 2009)</ref>
|<ref name=HKCM-Comrades/><ref name=IMDB-Comrades>{{imdb title|1344008|Tactical Unit: Comrades in Arms}}</ref><ref>Love HK Film, [http://www.lovehkfilm.com/reviews_2/tactical_unit_comrades.html Comrades in Arms] (accessed 21 Dec 2009)</ref><ref>HKMDB, [http://hkmdb.com/db/movies/view.mhtml?id=13674&display_set=eng "機動部隊之衕袍"] (accessed 30 Dec 2009)</ref>
|-
| [[Tactical Unit: Partners]] <!-- (DVD 6877) --> 
| 機動部隊─伙伴 
| Kei Tung Bou Deui: Fo Pun
| Mobile Tactical Unit – Partners
| 2009
| [[Lawrence Ah Mon]]
|
|<ref>HK Cinemagic, [http://www.hkcinemagic.com/en/movie.asp?id=10862 Tactical Unit: Partners] (accessed 21 December 2009)</ref><ref>{{imdb title|1403854|Tactical Unit: Partners}}</ref><ref>HKMDB, [http://hkmdb.com/db/movies/view.mhtml?id=13906&display_set=eng "機動部隊伙伴"] (accessed 30 Dec 2009)</ref>
|-
|}

The film series stars [[Simon Yam]], [[Maggie Shiu]] and [[Lam Suet]].

==Films==
===Partners===
"Partners" is the last film in the film series.<ref name=BH-P>Beyond Hollywood, [http://www.beyondhollywood.com/tactical-unit-partners-2009-movie-review/ "Tactical Unit: Partners (2009) Movie Review"], '''James Mudge''', ''13 June 2009'' (accessed 31 Dec 2009)</ref>

The film revolves around the criminal underworld of dark skinned non-Chinese, and discrimination by Chinese against the darker skinned non-Chinese, with the PTU and CID investigating a criminal plot by an Indian criminal kingpin.<ref name=BH-P/>

===Comrades in Arms===
{{main|Tactical Unit: Comrades in Arms}}
"Comrades in Arms" has been featured in the ''2009 Hong Kong Film Panorama'' in Brussels,<ref>7th Space, [http://7thspace.com/headlines/328035/hong_kong_film_panorama_opens_in_brusselswith_photos.html Hong Kong Film Panorama opens in Brussels], '''HKSAR Government''' (accessed 22 Dec 2009)</ref> the ''2009 New York Asian Film Festival'' of New York City,<ref>New York Times, [https://www.nytimes.com/2009/06/19/movies/19asia.html Asian Movies All Over the Map], David Kehr, 18 June 2009 (accessed 22 Dec 2009)</ref> the 2009 ''Zero em Comportamento'' festival of Lisbon,<ref>{{pt icon}} [http://www.guiadacidade.pt/portugal/?G=artigos.index&artid=21592&distritoid=11 Mostra de Cinema de Hong Kong], 8 Oct 2009 (accessed 22 December 2009)</ref> and the 2009 [[Fantasia Festival]] of Montreal.<ref>{{fr icon}} Fantasia, [http://fantasiafest.com/2009/fr/films/film_detail.php?id=80 Comrades in Arms], 2009</ref>

The film deals with the rivalry of the two PTU columns commanded by May and Sam, as May has been proffered a promotion, and Sam has been in the field much longer, and the actions of their commander, Ho in preferring May's column over Sam's in their PTU platoon. Their rivalry leads to disrupted interaction and communication when many PTU units are used in a search for a gang of criminals holed up on a forested mountain.<ref>Fantasia, [http://www.fantasiafest.com/2009/en/films/film_detail.php?id=80 "Tactical Unit: Comrades in Arms"] (accessed 31 Dec 2009)</ref>

===Human Nature===
"Human Nature" focuses on the always-in-debt CID officer Tong, and his travails to try to repay the debt he owes a loanshark. With his feeble attempts to repay the debt, he gets himself entangled with a gang of killers from mainland China, who are out killing criminals to rob them of their money, and using an ideological basis to justify the killings as protecting mainland China from the criminal scum of Hong Kong.<ref name=BH-HN>Beyond Hollywood, [http://www.beyondhollywood.com/tactical-unit-human-nature-2008-movie-review/ "Tactical Unit: Human Nature (2008) Movie Review"], '''James Mudge''', ''10 May 2009'' (accessed 2009-31-12)</ref>

===No Way Out===
"No Way Out" is the second sequel to the original PTU.<ref name=BH-NWO>Beyond Hollywood, [http://www.beyondhollywood.com/tactical-unit-no-way-out-2009-movie-review/ "Tactical Unit: No Way Out (2009) Movie Review"], '''James Mudge''', ''9 April 2009'' (accessed 31 Dec 2009)</ref>

The film focuses on the retarded ''Fai'' and his girlfriend, rather than the members of the PTU, and life in the [[Temple Street, Hong Kong|Temple Street]] district. It follows Fai, as he is physically coerced to do things by the PTU, and two rival triads, and the resultant consequences and violence that is paid in return for those actions.<ref name=BH-NWO/>

===The Code===
"The Code" has been featured in the ''2008 Far East Film Festival'' of Udine; it is the first entry spun off of PTU.<ref>{{it icon}} Aidanews, [http://www.aidanews.it/default.asp?id=14&ACT=5&content=230&mnu=14 Il Far East Film Festival Di Udine], 19 aprile 2008 (accessed 2009-21-12)</ref><ref>{{it icon}} Scanner.it, [http://www.scanner.it/cinema/FarEastFilm104137.php "Far East Film 10"], '''Matteo Merli''' (accessed 22 December 2009)</ref> The script for the film was written by [[Yip Tin Shing]].<ref name=BH-TC>Beyond Hollywood, [http://www.beyondhollywood.com/tactical-unit-the-code-2008-movie-review/ "Tactical Unit: The Code (2008) Movie Review"], '''James Mudge''', ''8 March 2009'' (accessed 31 December 2009)</ref>

The film focuses on the aftermath of an unreported beating meted out by a PTU column on a person in a back alley that was caught on a camera placed by the tourism bureau. The West Kowloon Police Station comes under investigation by an investigative committee of the police, as the film follows various members of the police with different motivations try to find the person who was beaten, to settle different goals, with a subplot of another member of the PTU having gotten into administrative trouble before the investigation began and having difficulty dealing with it.<ref name=BH-TC/>

===Into the Perilous Night===
{{main|PTU (film)}}
"Into The Perilous Night" has won many awards. In 2004, it won five awards at the ninth annual [[Golden Bauhinia Awards]].<ref>Xinhua, [http://news.xinhuanet.com/english/2004-03/29/content_1389582.htm "PTU sweeps the ninth Golden Bauhinia Awards"], ''2004-03-29 12:58:34'' (accessed 22 Dec 2009)</ref> It also gave Johnnie To a Best Director award at the 2004 [[Hong Kong Film Awards]].<ref>Xinhua, [http://news.xinhuanet.com/english/2008-06/12/content_8351880.htm "Johnnie To: awards no big deal"], ''2008-06-12 08:41:40'' (accessed 2009-22-12)</ref> The film has also been featured in many film festivals. It was shown at the 2003 St. Louis International Film Festival;<ref>St. Louis Post-Dispatch, "Today at the 12th Annual St. Louis International Film Festival", ''14 November 2003'', Page E6</ref> It has a score of 57% at RottenTomatoes.com<ref name=RT-PTU>Rotten Tomatoes, [http://www.rottentomatoes.com/m/ptu/ "PTU: Police Tactical Unit (2006)"] (accessed 31 December 2009)</ref>

The film focuses on the aftermath of CID Lo losing his service revolver, and the night that follows, with PTU patrols and rival criminal gangs seeing action on the street in the climax of the film.<ref name=RT-PTU/>

== Knock-offs ==
In addition to the official entries in the series, as in Hong Kong tradition, other films have been named with similar titles, to take advantage of the fame and popularity surrounding PTU, which have no relation to the series at all.

One of these is "PTU女警之偶然陷阱" (''PTU File – Death Trap'') from 2005, written, produced and directed by [[Tony Leung Hung-Wah]].<ref>HKMDB, [http://hkmdb.com/db/movies/view.mhtml?id=10798&display_set=eng "PTU女警之偶然陷阱"] (accessed 2009)</ref><ref>{{imdb title|0898098|PTU neui ging ji ngau yin haam jing}}</ref><ref>Love HK Film, [http://www.lovehkfilm.com/reviews_2/ptu_file_death_trap.htm "PTU File – Death Trap"] (accessed 2009)</ref><ref>HK Cinemagic, [http://www.hkcinemagic.com/en/movie.asp?id=7442 "PTU File Death Trap (2005)"] (accessed 2009)</ref>

== References ==
{{reflist}}

== External links ==
* {{imdb title|0250638|PTU}}
* {{imdb title|1256635|Tactical Unit: The Code}}
* {{imdb title|1401663|Tactical Unit: No Way Out}}
* {{imdb title|1401664|Tactical Unit: Human Nature}}
* {{imdb title|1344008|Tactical Unit: Comrades in Arms}}
* {{imdb title|1403854|Tactical Unit: Partners}}

== See also ==
* ''[[Breaking News (2004 film)|Breaking News]]'', a similar film by Johnnie To

{{Johnnie To}}
{{MilkywayImage}}

[[Category:Hong Kong films]]
[[Category:Cantonese-language films]]
[[Category:Film series]]