{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name = Monkey Gone to Heaven
| Cover = Pixies_monkeygonetoheaven.jpg
| Alt =
| Artist = [[Pixies]]
| Album = [[Doolittle (album)|Doolittle]]
| A-side = "Monkey Gone to Heaven"
| B-side = "Manta Ray"<br />
"Weird at My School"<br />
"Dancing the Manta Ray"
| Released = {{Start date|1989|3|20}}
| Format = [[Vinyl record]] ([[7"]], [[12"]]), [[Compact Disc single|Compact Disc]]
| Recorded = November 1988 at [[Downtown Recorders]], Boston, Massachusetts, United States<br />Strings recorded on December 4, 1988, at [[Carriage House Studios]], Stamford, Connecticut, United States
| Genre = [[Alternative rock]]
| Length = 2:56
| Label = [[Elektra Records|Elektra]]
| Producer = [[Gil Norton]]
| Writer=[[Black Francis]]
| Last single = "[[Gigantic (song)|Gigantic]]"<br />(1988)
| This single = "'''Monkey Gone to Heaven'''"<br />(1989)
| Next single = "[[Here Comes Your Man]]"<br />(1989)
}}
"'''Monkey Gone to Heaven'''" is a song by the American [[alternative rock]] band [[Pixies]] and is the seventh track on their 1989 album ''[[Doolittle (album)|Doolittle]]''. The song was written and sung by [[frontman]] [[Black Francis]] and was produced by [[Gil Norton]]. Referencing environmentalism and biblical numerology, the song's lyrics mirrored themes that were explored in ''Doolittle''. "Monkey Gone to Heaven" was the first Pixies song to feature guest musicians: two cellists, Arthur Fiacco and Ann Rorich, and two violinists, Karen Karlsrud and Corine Metter.

"Monkey Gone to Heaven" was released as the first [[single (music)|single]] from ''Doolittle'' in the United States and United Kingdom. As the band had signed to [[Elektra Records]] shortly before, the single also marked their first American and [[major label]] release. It was critically well-received; ''[[Rolling Stone]]'''s David Fricke said "Monkey Gone to Heaven" was "a corrosive, compelling meditation on God and garbage".<ref name="rollingstone">Fricke, David. "Pixies Cast Their Spell". ''Rolling Stone''. June 1989.</ref> In the years since its release, the song has received several accolades from music publications.

== Lyrics and meaning ==
"Monkey Gone to Heaven"'s main theme is [[environmentalism]]. The song mainly deals with man's destruction of the ocean and "confusion of man's place in the universe". "On one hand, it's [the ocean] this big organic toilet. Things get flushed and repurified or decomposed and it's this big, dark, mysterious place", Black later said, "It's also a very mythological place where there are octopus's gardens, the Bermuda Triangle, Atlantis, and mermaids."<ref name="pg96" /> Francis came up with the song's hook, "this monkey's gone to heaven", long before the song itself was written. The line itself forms a basis for the song, which revolves around man's relationship with the divine and environmentalism. After Francis set the developing lyrics to music, he rushed to [[lead guitarist]] [[Joey Santiago|Joey Santiago's]] apartment to play it to him. Santiago later commented on the nascent performance: "It was early in the morning, I was still so tired. [Francis said] 'Hey Joe, I need to come over. I need to show you something.' [...] It was awesome, really good. He had the 'If man is five' part there, and he was laughing. [...] It was hilarious".<ref name="pg96">Sisario, Ben. ''Doolittle 33⅓''. Continuum, 2006. ISBN 0-8264-1774-4. p. 96</ref>

"Monkey Gone to Heaven" includes references to numerology in the lyrics "If man is five/then the Devil is six/and God is seven". Francis later expanded on the significance of the lyrics in an interview to ''[[Alternative Press (music magazine)|Alternative Press]]'', saying "It's a reference from what I understand to be [[Biblical Numerology|Hebrew numerology]], and I don't know a lot about it or any of it really. I just remember someone telling me of the supposed fact that in the Hebrew language, especially in the Bible, you can find lots of references to man in the 5th and Satan in the 6th and God in the 7th. [...] I didn't go to the library and figure it out".<ref>Goldman, Marlene.  "Here and There and Everywhere". ''Alternative Press'' Vol IV, No 22. September 1989.</ref> The song's numerology is alluded to on the single's cover, which features figures of five, six and seven, and also a monkey with a halo.

Ben Sisario, author of ''Doolittle 33⅓'', offers a slightly different interpretation of the song: "[[Neptune (mythology)|Neptune]], the god of this realm [in reference to Francis' ocean comment], the 'underwater guy who controlled the sea,' hung out down there, the personification of man's relationship with the earth. And what happens to Neptune? He gets 'killed by ten million pounds of sludge from New York to New Jersey.' Same thing with the "creature in the sky," who gets stuck up there in a hole in the ozone layer. Man the divine manifestation effectively dies, and what remains is his degraded animal nature; the chintzy halo stuck on the primate's head is the symbol of that unhappy fall".<ref>Sisario, 2006. p. 97.</ref>

== Structure ==
[[File:MonkeyCelloBass.png|thumb|right|400px|The cello part for the verses of "Monkey Gone to Heaven"]]
"Monkey Gone to Heaven" is written in the [[key (music)|key]] of [[D major]], and opens with Francis' [[rhythm guitar]] playing a short [[chord progression]] backed by the bass guitar of [[Kim Deal]] and drums of [[David Lovering]]. The guitar intensity fades as Francis begins to sing, leaving Deal's bassline and Lovering's steady drum-beat.<ref name="s98">Sisario, 2006. p. 98.</ref> Between each line of the verse, Francis pauses, leaving the drums and bass playing. [[Joey Santiago|Joey Santiago's]] [[lead guitar]] does not feature at all during the verses. By the end of the second line of each verse, the [[cello]] part joins in, following the bassline closely.<ref>{{cite web | url={{Allmusic|class=song|id=t1528292|pure_url=yes}} | title=Monkey Gone to Heaven > Song Review | accessdate=2007-06-08 | author=Janovitz, Bill | publisher=''[[Allmusic]]''}}</ref>

{{Listen
|filename=MonkeyGoneToHeaven.ogg
|title=Pixies — "Monkey Gone to Heaven"
|description=This sample contains the end of the second verse, the bridge and chorus.}}

As the first verse finishes, the opening chord progression is repeated. This leads into the chorus (where Francis and Deal repeat "This monkey's gone to heaven") with the lead guitar of Santiago playing two notes repeatedly. The two [[violin]]s play a melody throughout, accompanied by a [[piano]] in the background.<ref name="s98" /> There is then a short [[Guitar solo|solo]] by Santiago, who repeats the melody three times, to bridge the chorus and second verse. The second verse and chorus follow the same format. At the end of the second chorus, Francis shouts "Rock me Joe!"; Santiago then begins a guitar solo lasting seventeen seconds, with backing violins for the second half of the solo.

After the solo, Francis sings "If man is five" several times. There is no backing, apart from the lead guitar, for several seconds, but then the song's chord progression is heard again. This is repeated for "If the devil is six". At the end of the second chord progression, the song's main backing restarts again, with Francis screaming "Then God is seven" as the chorus approaches. The final repeated chorus of "This monkey's gone to heaven" ends the song as the string section becomes more prominent.

== Recording and production ==
The band's parts were recorded at [[Downtown Recorders]] in [[Boston, Massachusetts]]. The string section of "Monkey Gone to Heaven" was recorded while ''[[Doolittle (album)|Doolittle]]'' was being mixed at [[Carriage House Studios]] in [[Stamford, Connecticut|Stamford]], [[Connecticut]]. [[Gil Norton]], the album's producer, was inspired to add a string section to the song after seeing Deal plucking the strings of a grand piano during recording.<ref>Frank, Ganz, 2005. p. 113.</ref> The production team, led by Norton, asked the studio owner John Montagnese to bring in string players for one evening session. The studio was often used for recording [[orchestra]]l scores for [[B-movie]]s such as ''[[Missing in Action]]'' and ''[[Silver Bullet (film)|Silver Bullet]]''. Montagnese hired four classical musicians from a local orchestra for the recording, with the session taking place on the afternoon of December 4, 1988.<ref name="sos">{{cite web | url=http://www.soundonsound.com/sos/dec05/articles/classictracks.htm | title=Classic Tracks: The Pixies 'Monkey Gone To Heaven' | accessdate=2008-01-06 |date=December 2005 | author=Buskin, Richard | publisher=SoundOnSound}}</ref>

Arthur Fiacco, a [[cello|cellist]], arrived at Carriage House first. He was dressed in formal black and white attire, having traveled from an afternoon concert. Fiacco was surprised to find there were no scores written for the musicians to play; he then wrote a part based on riffs Francis had shown him.<ref name="sos" /> The [[violin]]ists, Corinne Metter and Karen Karlsrud, also followed the directions of Francis and Norton. Another cellist, Ann Rorich, credited on the album and single, was sent home; according to Fiacco he doubled her parts.<ref>Sisario, 2006. p. 99.</ref>

== Release and music video ==
"Monkey Gone to Heaven", the first single from ''Doolittle'', was released to [[radio station]]s for rotation in April 1989 in the United States. The single reached number five on the US [[Modern Rock Tracks]] chart,<ref>{{cite web | url={{BillboardURLbyName|artist=pixies|chart=Alternative Songs}}  | title=Artist Chart History&nbsp;— Pixies | accessdate=2008-01-06 | publisher=Billboard.com}}</ref> with the help of [[Elektra Records]]' marketing. In the United Kingdom, "Monkey Gone to Heaven" was released on April 1, 1989 and spent three weeks in the UK chart, debuting at number 60.<ref name=occ>{{cite web|title=Pixies - Full Official Chart History|url=http://www.officialcharts.com/artist/16940/pixies/|website=Official Charts Company|publisher=Official Charts Company|accessdate=19 January 2016}}</ref>

The [[music video]], the Pixies' first, features the band playing their instruments on a stage, with the camera alternating to focus on each band member. Filmed in [[black-and-white]], "searchlights" cross the stage and several camera effects are used, such as [[slow-motion]]. The camera switches to color for a few seconds several times during the video, before reverting to black-and-white. Halfway through the video, fog appears on-stage, covering the band. The members of the string section are not seen in the video.

The song would later be re-recorded by [[Black Francis]] and released on his 2004 album ''[[Frank Black Francis]]''.

South African band Absinthe covered the song on their album ''A Rendezvous at Nirvana''.<ref>https://itunes.apple.com/za/album/a-rendezvous-at-nirvana/id633801588 Retrieved 13 January 2014</ref>

== Reception ==
In general, "Monkey Gone to Heaven" received a positive critical reaction. British magazine ''[[NME]]'', reviewing the UK 7" single in March 1989, said: "All the smart bastards are mixing strings with grunge guitars nowadays and the Pixies are no exception. Snarled vocals, [[Science fiction|sci-fi]] lyrics, and the usual molten lava flow of guitars burn another crater where your ears used to be. 'Monkey Gone to Heaven' pukes acid and poetry into America's [[album oriented rock|AOR]] heartland before being splattered by the faster and more direct sting of the second track 'Manta Ray'."<ref>"The Pixies&nbsp;— Monkey Gone to Heaven". ''NME''. March 1989.</ref> Upon the release of ''Doolittle'' in April 1989, ''NME''<nowiki>'</nowiki>s Edwin Pouncey added: "the wonderful 'Monkey Gone to Heaven' is laced with lush but unobtrusive strings which nibble round the edge of the song and push it into a new realm of arrangement for the band. The opportunity to give 'Monkey' the full Philharmonic treatment, complete with heavenly harp, must have been a temptation to them. Wisely such a folly has been resisted."<ref>Pouncey, Edwin. "Pixies&nbsp;— Doolittle" ''NME''. April 1989.</ref>

''[[Q (magazine)|Q]]'', in their review of ''Doolittle'', described "Monkey Gone to Heaven": "It's not pretty, but its carefully structured noise and straight forward rhythmic insistence makes perfect sense: a gut feeling that is doubled when it gets within sniffing distance of a tune, as on 'Monkey Gone to Heaven' or 'Debaser'."<ref>Kane, Peter. "Pixies&nbsp;— Doolittle" ''Q'' number 32. May 1989.</ref> ''[[Rolling Stone]]''<nowiki>'</nowiki>s David Fricke, reviewing ''Doolittle'', said "Monkey Gone to Heaven" was "a corrosive, compelling meditation on God and garbage."<ref name="rollingstone" /> The critical success of "Monkey Gone to Heaven" was also reflected commercially; the song reached number five on the ''[[Billboard (magazine)|Billboard]]'' [[Modern Rock Tracks]] chart, marking the Pixies' debut in the American charts.<ref>{{cite web | url={{BillboardURLbyName|artist=pixies|chart=all}} | title=Artist History&nbsp;— Pixies | accessdate=2007-04-20 | publisher=Billboard.com|archiveurl = https://web.archive.org/web/20070929224129/{{BillboardURLbyName|artist=pixies|chart=all}} |archivedate = September 29, 2007|deadurl=yes}}</ref> However, the song did not perform as well in the British charts, reaching a peak position of number 60 and falling off the charts after three weeks.<ref>{{cite web | url=http://www.polyhex.com/music/chartruns/chartruns.php | title=UK Singles Chart | accessdate=2007-03-31 | publisher=PolyHex}}</ref>

== Track listing ==
All songs were written by [[Black Francis]]

;UK 7" single
# "Monkey Gone to Heaven"&nbsp;– 2:56
# "Manta Ray"&nbsp;– 2:38

;UK/US 12"/CD single
# "Monkey Gone to Heaven"&nbsp;– 2:56
# "Manta Ray"&nbsp;– 2:38
# "Weird at My School"&nbsp;– 1:59
# "Dancing the Manta Ray"&nbsp;– 2:13

== Accolades ==
The information regarding accolades attributed to "Monkey Gone to Heaven" is adapted from [[Acclaimed Music]].<ref name="acclaimed">{{cite web | url= http://www.acclaimedmusic.net/061024/S245.htm | title = Monkey Gone to Heaven| accessdate=2007-01-28 | publisher=[[Acclaimed Music]]}}</ref>
{|class="wikitable"
|-
! Publication
! Country
! Accolade
! Year
! Rank
|-
|''[[Melody Maker]]''
|UK
|Single of the Year
|1989
|#1
|-
|''NME''
|UK
|Single of the Year
|1989
|#22
|-
|''Rolling Stone''
|U.S.
|Single of the Year
|1989
|#5
|-
|''[[The Village Voice]]''
|U.S.
|Single of the Year
|1989
|#24<ref>{{cite web | url=http://www.villagevoice.com/specials/pazznjop/04/search_return.php?poll_year=1989&type=S&keyword= | title=Pazz & Jop | accessdate=2007-04-21 | publisher=VillageVoice.net}}</ref>
|-
|''Rolling Stone''
|U.S.
|500 Greatest Songs of All Time
|2004
|#410<ref>{{cite web | url=http://www.rollingstone.com/news/story/6596255/monkey_gone_to_heaven | title=Rolling Stone: Monkey Gone to Heaven | accessdate=2007-04-21 | publisher=''Rolling Stone'' | date=2004-11-04|archiveurl = https://web.archive.org/web/20071001175912/http://www.rollingstone.com/news/story/6596255/monkey_gone_to_heaven |archivedate = October 1, 2007|deadurl=yes}}</ref>
|-
|''NME''
|UK
|50 Greatest Indie Anthems Ever
|2007
|#35<ref>{{cite web | url=http://www.nme.com/news/nme/28031 | title=The Greatest Indie Anthems Ever&nbsp;— countdown continues | accessdate=2008-01-06 | publisher=''NME'' | date=2007-05-01}}</ref>
|-
|''NME''
|UK
|500 Greatest Songs Of All Time
|2014
|#197<ref>{{cite web | url=http://www.nme.com/photos/the-500-greatest-songs-of-all-time-200-101/330881/1/1#4 | title=The 500 Greatest Songs Of All Time - 200-101 | accessdate=2014-02-08 | publisher=''NME'' | date=2014-02-08}}</ref>
|}

== See also ==
{{Portal|Alternative rock}}
*"[[Fire Water Burn]]" by [[Bloodhound Gang]] references the song in its lyrics

== References ==
* Frank, Josh; Ganz, Caryn. ''[[Fool the World: The Oral History of a Band Called Pixies]]''. Virgin Books, 2005. ISBN 0-312-34007-9.
* Sisario, Ben. ''Doolittle 33⅓''. Continuum, 2006. ISBN 0-8264-1774-4.

== Notes ==
{{Reflist|30em}}

== External links ==
{{Spoken Wikipedia|en-monkey_gone_to_heaven.ogg|2010-06-19}}
* [http://www.last.fm/music/Pixies/_/Monkey+Gone+to+Heaven "Monkey Gone to Heaven"] at [[Last.fm]]
* [http://www.google.com/musics?lid=V1ZrbpEE9IP&aid=qe-FXrQD4RF&sid=TXUL7gfOdRN "Monkey Gone to Heaven" at Google Music]
* {{MetroLyrics song|pixies|monkey-gone-to-heaven}}<!-- Licensed lyrics provider -->

{{Pixies}}

{{featured article}}

[[Category:1989 singles]]
[[Category:Environmental songs]]
[[Category:Pixies (band) songs]]
[[Category:Songs written by Black Francis]]
[[Category:Elektra Records singles]]
[[Category:1989 songs]]
[[Category:Song recordings produced by Gil Norton]]