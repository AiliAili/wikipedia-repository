{{Infobox person
| honorific_prefix          =
| name                      = Harold Geneen
| honorific_suffix          =
| native_name               =
| native_name_lang          =
| image                     = Harold Geneen.jpg
| image_size                = 200
| alt                       =
| caption                   =
| birth_name                = Harold Sydney Geneen 
| birth_date                = {{Birth date|1910|1|22}} 
| birth_place               = [[Bournemouth]], [[Dorset]], England
| death_date                = {{Death date and age|1997|11|21|1910|1|22}}
| death_place               = New York City
| resting_place             = 
| nationality               = American
| other_names               =
| ethnicity                 = 
| citizenship               =
| education                 =
| alma_mater                =
| occupation                = Businessman
| years_active              =
| employer                  = [[Raytheon]]<br />ITT Corporation
| organization              = 
| agent                     =
| known_for                 = CEO of [[ITT Corporation]]
| religion                  = 
| denomination              = 
| spouse                    = June (Hjelm) Geneen
| partner                   =
| children                  =
| signature_size            =
| website                   = <!-- {{URL|www.example.com}} -->
| footnotes                 =
| box_width                 =
}}

'''Harold "Hal" Sydney Geneen''' (January 22, 1910 – November 21, 1997), was an [[United States|American]] [[businessman]] most famous for serving as president of the [[ITT Corporation]].

==Biography==
Geneen was born on January 22, 1910 in [[Bournemouth]], [[Hampshire]], [[England]].His father was Russian-Jewish, and his mother was Italian and Catholic.<ref>http://bsr.london.edu/lbs-article/311/index.html</ref> He migrated to the [[United States]] as an infant with his parents. He studied [[accounting]] at [[New York University]].
<!-- Deleted image removed: [[Image:HaroldGeneen.jpg|right|thumb|200px|Harold Geneen]] -->

Between 1956–1959 he was [[senior vice president]] of [[Raytheon]], developing his management structure, allowing a large degree of freedom for divisions while maintaining a high degree of financial and other accountability, which was surprising as he had been ejected from his prior employer, Jones and Laughlin Steel Company, for playing fast and loose with the company books.

From 1959–1977 he was the president and [[CEO]] of [[ITT Corporation|International Telephone and Telegraph Corp.]] (ITT). He grew the company from a medium-sized business with $765 million sales in 1961 into multinational conglomerate with $17 billion sales in 1970. He extended its interests from manufacturing of telegraph equipment into [[insurance]], [[hotels]], [[real estate]] management and other areas. Under Geneen's management, ITT became the archetypal modern multinational conglomerate. ITT grew primarily through a series of approximately 350 acquisitions and mergers in 80 countries. Some of the largest of these were [[The Hartford|Hartford Fire Insurance Company]] (1970) and [[Sheraton Hotel]]s.

ITT had many overseas interests. In Europe it had telephone subsidiaries in numerous countries. In Brazil, it owned the telephone company. Washington feared that President [[João Goulart]] would nationalize it. Geneen was friends with the [[Director of Central Intelligence]] [[John McCone]]. The [[Central Intelligence Agency|CIA]] performed [[psyops]] against Goulart, performed [[character assassination]], pumped money into opposition groups, and enlisted the help of the [[Agency for International Development]] and the [[AFL-CIO]]. The [[1964 Brazilian coup d'état]] exiled Goulart and the military dictatorship of [[Humberto de Alencar Castelo Branco]] took over. McCone went to work for ITT a few years later. The dictatorship lasted until 1985.<ref>''Burn Before Reading'', Admiral Stansfield Turner, 2005, Hyperion, pg. 99. Also see the article on [[Humberto de Alencar Castelo Branco]]. Also see [http://www.gwu.edu/~nsarchiv/NSAEBB/NSAEBB118/index.htm BRAZIL MARKS 40th ANNIVERSARY OF MILITARY COUP], National Security Archive, George Washington University. Edited by Peter Kornbluh, 2004.</ref>

ITT also had some $200 million-worth of investments in [[Chile]]. Under Geneen's leadership, ITT funneled $350,000 to [[Salvador Allende|Allende]]'s opponent, [[Jorge Alessandri]].<ref>Gilpin, Kenneth N. [https://www.nytimes.com/1997/11/23/business/harold-s-geneen-87-dies-nurtured-itt.html ''"Harold S. Geneen, 87, Dies"''] ''The New York Times'', November 23, 1997</ref> When [[Salvador Allende|Allende]] won the presidential election, ITT offered the [[CIA]] $1,000,000 to defeat [[Salvador Allende|Allende]], though the offer was rejected.<ref>[http://foia.state.gov/Reports/ChurchReport.asp ''Staff Report of the Select Committee To Study Governmental Operations With Respect to Intelligence Activities''], December 18, 1975</ref> Declassified documents released by the [[CIA]] in 2000 reveal that ITT financially helped opponents of [[Salvador Allende]]'s government prepare a [[military coup]].<ref>[http://foia.state.gov/Reports/HincheyReport.asp#17 Hinchey Report] at US Dept. of State</ref><ref>{{cite news| url=https://www.nytimes.com/2003/01/30/world/edward-korry-81-is-dead-falsely-tied-to-chile-coup.html?pagewanted=1 | work=The New York Times | title=Edward Korry, 81, Is Dead; Falsely Tied to Chile Coup | first=David | last=Stout | date=January 30, 2003 | accessdate=May 5, 2010}}</ref> On September 28, 1973, an ITT building in [[New York City]], [[New York (state)|New York]], was bombed by the [[Weather Underground]] for involvement in the [[1973 Chilean coup d'état|September 11th]] overthrow of the Allende government.<ref>{{cite news| url=https://select.nytimes.com/gst/abstract.html?res=FB0815FA3554137A93CBAB1782D85F478785F9 | work=The New York Times | title=I.T.T. OFFICE HERE DAMAGED BY BOMB; Caller Linked Explosion at Latin-American Section to 'Crimes in Chile' I.T.T. Latin-American Office on Madison Ave. Damaged by Bomb Fire in Rome Office Bombing on the Coast Rally the Opponents | date=September 29, 1973 | accessdate=May 5, 2010 | first=Paul L. | last=Montgomery}}</ref><ref>Ayers, Bill. [https://books.google.com/books?id=X2OJhrWo6PcC&pg=PT257&lpg=PT257&dq=itt+bomb+1973&source=bl&ots=S3lDi8Uupm&sig=yfXYDKZLw7kYAYi_BssJzcyM-ak&hl=en&sa=X&ei=EkDyTrbJF4OMiALv0pnTDg&sqi=2&ved=0CD8Q6AEwBQ#v=onepage&q=itt%20bomb%201973&f=false ''Sing a Battle Song: The Revolutionary Poetry, Statements, and Communiques of The Weather Underground'']</ref>

In 1977 Geneen retired as [[CEO]] and president of ITT, was chairman of the board until 1979, and stayed on the board for four more years.<ref>{{cite news| url=http://articles.latimes.com/1997/nov/23/local/me-56924 | work=Los Angeles Times | title=Harold Geneen, 87; Led ITT's Growth for 18 Years | date=November 23, 1997}}</ref> His successors, particularly [[Rand Araskog]], steadily sold off parts of the business.

In his obituary, the ''[[New York Times]]'' stated that he remained active in business and on the boards of several educational institutes until his death, and had boasted that "his post-retirement deal-making had earned him far more than he ever made at ITT."<ref>{{cite news| url=https://www.nytimes.com/1997/11/23/business/harold-s-geneen-87-dies-nurtured-itt.html?pagewanted=all | work=The New York Times | first=Kenneth N. | last=Gilpin | title=Harold S. Geneen, 87, Dies; Nurtured ITT | date=November 23, 1997}}</ref>

Geneen's widow, June Geneen, born in [[Berlin, New Hampshire]], died in Boston in October, 2012.

== Quotes ==
{{Unreferenced section|May 2011|date=May 2011}}
* ''The worst disease which can afflict executives in their work is not, as popularly supposed, alcoholism; it's egotism.''
* ''You can't run a business or anything else on a theory.''
* ''Leadership is practiced not so much in words as in attitude and in actions.''
* ''The only unforgivable sin in business is to run out of cash.''
* ''In business, words are words; explanations are explanations, promises are promises, but only performance is reality.
* ''In the business world, everyone is paid in two coins: Cash and Experience. Take the experience first. The cash will come later.''
* ''Better a good decision quickly than the best decision too late.''
* ''Telephones, hotels, insurance—it's all the same. If you know the numbers inside out, you know the company inside out.''
* ''Leadership cannot really be taught, it can only be learned.''
* ''I'd hate to spend the rest of my life trying to outwit an 18-inch fish.'' <ref>https://www.nytimes.com/1997/11/23/business/harold-s-geneen-87-dies-nurtured-itt.html?pagewanted=all (NY Times Obituary)</ref>
* ''A true leader has to have a genuine open-door policy so that his people are not afraid to approach him for any reason.''

==Books==
Harold Geneen wrote and co-authored several books:
*{{cite book |author1=Geneen, Harold |author2=Brent Bowers | title=Synergy and Other Lies: Downsizing, Bureaucracy, and Corporate Culture Debunked | publisher=St. Martin's Griffin | year=1999 | location=New York | isbn=0-312-20080-3}}
*{{cite book |author1=Geneen, Harold |author2=Brent Bowers | title=The Synergy Myth and Other Ailments Of Business Today | publisher=[[St. Martin's Press]] | year=1997 | location=New York | isbn=0-312-14724-4}}
*{{cite book |author1=Geneen, Harold |author2=Alvin Moscow | title=Alta Dirección: Las Normas Básicas Para Triunfar en los Negocios | publisher=Ediciones Grijalbo | year=1986 | location=Barcelona | isbn=84-253-1871-8}}
*{{cite book |author1=Geneen, Harold |author2=Alvin Moscow | title=Managing | publisher=[[Doubleday (publisher)|Doubleday]] | year=1984 | location=Garden City, NY | isbn=0-385-17496-9}}

== References ==
{{Reflist}}

== External links ==

{{Wikiquote}}
*[http://www.basicfamouspeople.com/index.php?aid=6785 Harold Geneen Biography]
*[http://www.hbs.edu/leadership/database/leaders/300/ Harold Geneen's bio]
*[http://nofie.com/harold-geneen/ Harold Geneen's detailed profile and pics]

{{ITT Corporation}}

{{Authority control}}

{{DEFAULTSORT:Geneen, Harold}}
[[Category:1910 births]]
[[Category:1997 deaths]]
[[Category:American businesspeople]]
[[Category:People from Bournemouth]]
[[Category:ITT Corporation]]
[[Category:Raytheon people]]
[[Category:20th-century American businesspeople]]
[[Category:English emigrants to the United States]]
[[Category:New York University alumni]]
[[Category:American business writers]]