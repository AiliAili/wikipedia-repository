{{multiple issues|
{{COI|date=March 2014}}
{{cleanup reorganize|date=March 2014}}
{{third-party|date=March 2014}}
}}
{{Infobox scientist
| name              = Sian Leah Beilock
| image             = Sian L Beilock-Rome, Italy-December 2007.jpg
| image_size        = 
| alt               = Sian Beilock sitting in front of ruins in Rome, Italy in December 2007
| caption           = Sian L. Beilock&nbsp;— Rome, Italy, December 2007
| birth_date        = 
| birth_place       = Berkeley, California, USA
| death_date        = 
| death_place       = 
| resting_place             = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline,title}} -->
| residence         = Chicago, Illinois, USA
| citizenship       = USA
| nationality       = 
| fields            = [[Psychology]], [[education]], [[neuroscience]]
| workplaces        = [[The University of Chicago]]
| alma_mater        = [[Michigan State University]], Ph.D., M.A. [[University of California, San Diego]], B.S.
| thesis_title      = 
| thesis_url        = 
| thesis_year       = 
| doctoral_advisor  = [[Deb L. Feltz]] and [[Thomas H. Carr]]
| academic_advisors = 
| doctoral_students = 
| notable_students  = 
| known_for         = science of choking under pressure;
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences        = 
| influenced        = 
| awards            = 2012 Outstanding Early Career Award ([[Psychonomic Society]])<ref>{{cite web|title=2012 Psychonomic Society Annual Meeting|url=http://www.psychonomic.org/Assets/91293d70-a509-4ee6-b3d0-3128173edf01/635239516391270000/ps-2012-abstract-book-web-pdf|publisher=Psychonomic Society|accessdate=6 February 2014}}</ref>  [[Robert L. Fantz]] Memorial Award for Young Psychologists (2011-American Psychological Foundation)<ref>{{cite web|title=Robert L. Fantz Memorial Award for Young Psychologists|url=http://www.apa.org/apf/funding/fantz.aspx|publisher=American Psychological Foundation (APF)|accessdate=6 February 2014}}</ref> 2017 [[Troland Research Award]] ([[National Academy of Sciences]]) <ref>{{cite web|title=2017 NAS Troland Research Award|url=http://www.nasonline.org/programs/awards/2017/Sian-Beilock.html|publisher=National Academy of Sciences|accessdate=11 March 2017}}</ref> 
| signature         = <!--(filename only)-->
| signature_alt     = 
| website           = 
| footnotes         = 
| spouse            = 
}}
'''Sian L. Beilock''' is a Professor at [[The University of Chicago]]. She works on research concerning [[Choke (sports)|choking under pressure in sports]] and other fields. She is a member of the Department of Psychology at The University of Chicago, and serves on their Committee on Education.

==Background==

Beilock attended the [[University of California, San Diego]], where she received a BS in Cognitive Science (with a minor in Psychology). She was awarded a Doctor of Philosophy (PhD) from [[Michigan State University]] in 2003. From 2003 to 2005, she was an assistant professor in the Department of Psychology at [[Miami University]].  Since 2005, she has been on the faculty at The University of Chicago.<ref>{{cite web|last=Beilock|first=Sian L.|title=Faculty Homepage|url=http://psychology.uchicago.edu/people/faculty/sbeilock.shtml|publisher=University of Chicago|accessdate=5 February 2014}}</ref>

==Career==

===Early career===

During and subsequent to her PhD research, Beilock explored differences between novice and expert athletic performances. Her work demonstrated that because well-learned motor skills are performed largely outside conscious awareness, expert performers have poorer memories for the step-by-step process of their actions than their less experienced counterparts, which she termed "expertise-induced amnesia".<ref>{{cite journal|last=Beilock|first=Sian L.|author2=Carr, Thomas H. |title=On the fragility of skilled performance: What governs choking under pressure?|journal=Journal of Experimental Psychology: General|year=2001|volume=130|issue=4|pages=701–725|doi=10.1037/0096-3445.130.4.701|url=http://psycnet.apa.org/journals/xge/130/4/701/|accessdate=5 February 2014}}</ref><ref>{{cite journal|last=Beilock|first=Sian L.|author2=Carr, Thomas H. |author3=MacMahon, Clare |author4= Starkes, Janet L. |title=When paying attention becomes counterproductive: Impact of divided versus skill-focused attention on novice and experienced performance of sensorimotor skills.|journal=Journal of Experimental Psychology: Applied|year=2002|volume=8|issue=1|pages=6–16|doi=10.1037/1076-898X.8.1.6|url=http://psycnet.apa.org/journals/xap/8/1/6/701/|accessdate=5 February 2014}}</ref>   This discovery led Beilock to propose the explicit monitoring theory of choking under pressure, whereby stressful situations cause a breakdown in highly learned motor skills because, in an effort to control performance and ensure success, individuals pay attention to details of their actions that would normally run unconsciously, disrupting otherwise fluid performances.

===Current===

Later in her career, Beilock's research focused on why people perform poorly in stressful academic situations, such as taking a high-stakes mathematics exam. Beilock found that worries during those situations rob individuals of the [[working memory]] or cognitive horsepower they would normally have to focus.<ref>{{cite journal|last=Beilock|first=Sian L.|title=Math Performance in Stressful Situations|journal=Current Directions in Psychological Science|date=October 2008|volume=17|issue=5|pages=339–343|doi=10.1111/j.1467-8721.2008.00602.x}}</ref>  Working memory is similar to a mental scratchpad, facilitating the manipulation of information held in consciousness.  When working memory is compromised, performance can suffer. Counter-intuitively, Beilock demonstrated that those students who have the highest working memory are the ones most likely to perform poorly in stressful exam situations.<ref>{{cite journal|last=Beilock|first=Sian L.|author2=Carr, Thomas H. |title=When High-Powered People Fail|journal=Psychological Science|date=February 2005|volume=16|issue=2|pages=101–105|doi=10.1111/j.0956-7976.2005.00789.x|pmid=15686575}}</ref>  Because people with more working memory rely on their brainpower more, they can be affected to a greater extent in stressful academic situations. There is debate about how to interpret high-stakes test scores.<ref>{{cite web|last=Anderson|first=Jill|title=Measuring Up: A Q&A with Professor Dan Koretz|url=http://www.gse.harvard.edu/news_events/features/2008/05/8_koretz.php|publisher=Graduate School of Education, Harvard University|accessdate=5 February 2014|date=May 8, 2008}}</ref>  Beilock's work demonstrated that stressful situations during tests might diminish meaningful differences between students that, under less-stressful situations, might exhibit greater differences in performance. This work raises the possibility that high-stakes standardized tests have been filtering out and excluding persons with the most working memory from acceptance to programs, jobs, and institutions which use such tests as screening mechanisms. Beilock's research on choking was chronicled in PBS’s ''[[Nova (TV series)|Nova]]'' program "How Smart Can We Get?"<ref>{{cite web|title=How Smart Can We Get?|url=http://www.pbs.org/wgbh/nova/body/how-smart-can-we-get.html|publisher=NOVA|accessdate=5 February 2014|date=October 24, 2012 (air date)}} Sian Beilock is listed as a participant in this program.</ref>

In her work, Beilock has explored how simply being aware of a negative stereotype may affect performance (e.g. a girl aware of negative stereotypes regarding gender and math). Termed [[stereotype threat]],  Beilock has argued that this is another form of choking under pressure.<ref>{{cite journal|last=Beilock|first=Sian L.|author2=Rydell, Robert J. |author3=McConnell, Allen R. |title=Stereotype threat and working memory: Mechanisms, alleviation, and spillover.|journal=Journal of Experimental Psychology: General|year=2007|volume=136|issue=2|pages=256–276|doi=10.1037/0096-3445.136.2.256}}</ref> Beilock has also looked at [[math anxiety]] (i.e. a fear or apprehension of math or math-related tasks). She argued that math anxiety is not simply a proxy for poor math skills, but worries about the situation which may rob anxious individuals of the cognitive resources they would normally have. Moreover, she has shown how math anxiety can be passed from teacher to student. Specifically, when female elementary school teachers are anxious about their own math abilities, their female students may learn less math during the school year, and are more likely to endorse gender stereotypes regarding math.<ref>{{cite journal|last=Beilock|first=S. L.|author2=Gunderson, E. A. |author3=Ramirez, G. |author4= Levine, S. C. |title=Female teachers' math anxiety affects girls' math achievement|journal=Proceedings of the National Academy of Sciences|date=25 January 2010|volume=107|issue=5|pages=1860–1863|doi=10.1073/pnas.0910967107}}</ref>   Beilock has also explored the neural basis of math anxiety, showing that the brain areas active when highly math-anxious people prepare to do math overlap with the same brain areas that register the threat of bodily harm, and in some cases, physical pain.<ref>{{cite journal|last=Lyons|first=Ian M.|author2=Beilock, Sian L. |author3=Chapouthier, Georges |title=When Math Hurts: Math Anxiety Predicts Pain Network Activation in Anticipation of Doing Math|journal=PLoS ONE|date=31 October 2012|volume=7|issue=10|pages=e48076|doi=10.1371/journal.pone.0048076}}</ref>

Finally, her work explores learning in math and science more broadly by elucidating the basic cognitive and neural building blocks of numerical ability.<ref>{{cite journal|last=Lyons|first=Ian M.|author2=Ansari, Daniel |author3=Beilock, Sian L. |title=Symbolic estrangement: Evidence against a strong association between numerical symbols and the quantities they represent.|journal=Journal of Experimental Psychology: General|year=2012|volume=141|issue=4|pages=635–641|doi=10.1037/a0027248}}</ref> She is also investigating how different types of science laboratory experiences (especially experiences that are highly interactive) help students learn physics concepts such as torque and angular momentum.<ref>{{cite journal|last=Kontra|first=Carly|author2=Goldin-Meadow, Susan |author3=Beilock, Sian L. |title=Embodied Learning Across the Life Span|journal=Topics in Cognitive Science|date=October 2012|volume=4|issue=4|pages=731–739|doi=10.1111/j.1756-8765.2012.01221.x}}</ref> Her work has been funded by the [[Institute of Education Sciences]]<ref>{{cite web|title=An Exploration of Malleable Social and Cognitive Factors Associated with Early Elementary School Students' Mathematics Achievement|url=http://ies.ed.gov/funding/grantsearch/details.asp?ID=1201|publisher=Institute of Education Sciences|accessdate=5 February 2014}}</ref> and the [[National Science Foundation]],<ref>{{cite web|title=Writing About Anxiety Helps Students Ace Exams|url=http://www.nsf.gov/news/news_summ.jsp?cntn_id=118396|publisher=National Science Foundation|accessdate=5 February 2014}}</ref> including a CAREER award.<ref>{{cite web|title=CAREER: Women in the Math and Sciences: Counteracting the Impact of Negative Group Stereotypes on Performance|url=http://www.nsf.gov/awardsearch/showAward?AWD_ID=0746970&HistoricalAwards=false|publisher=National Science Foundation (nsf.gov)|accessdate=13 February 2014}}</ref>

===Cognitive Science and Education===

Beilock's research relates to educational practice and policy.<ref>{{cite web|last=Beilock|first=Sian|title=Back to school: Dealing with academic stress: Simple psychological interventions can reduce stress and improve academic performance|url=http://www.apa.org/science/about/psa/2011/09/academic-stress.aspx|publisher=American Psychological Association|accessdate=5 February 2014|year=2011}}</ref> Her work demonstrates that students' attitudes and anxieties (as well as those of their teachers) are critical to student success.{{citation needed|date=July 2014}} In her work, she has developed simple psychological interventions to help people perform their best under stress.<ref>{{cite web|last=Paul|first=Annie Murphy|title=How to Be a Better Test-Taker|url=https://www.nytimes.com/2012/04/15/education/edlife/how-to-be-a-better-test-taker.html|publisher=The New York Times|accessdate=5 February 2014|date=April 13, 2012}}</ref> One intervention, expressive writing, is borrowed from [[James W. Pennebaker|James Pennebaker]]’s work. Beilock and her former PhD student, Gerardo Ramirez, have found that students&nbsp;– especially those highest in test anxiety&nbsp;– who write about their worries before a high-stakes test are less likely to perform poorly due to stress.<ref>{{cite journal|last=Ramirez|first=G.|author2=Beilock, S. L. |title=Writing About Testing Worries Boosts Exam Performance in the Classroom|journal=Science|date=13 January 2011|volume=331|issue=6014|pages=211–213|doi=10.1126/science.1199427}}</ref>

==Books==

* Beilock, S. L. (2010). ''Choke: What the Secrets of the Brain Reveal about Getting It Right When You Have To''. Simon & Schuster: Free Press.
* Beilock, S. L. (2015). ''How the Body Knows Its Mind: The Surprising Power of the Physical Environment to Influence How You Think and Feel''. Simon & Schuster: Atria Books.

==References==

{{Reflist|2}}

==External links==
*[http://www.psychologytoday.com/blog/choke Choke&nbsp;– Beilock’s Psychology Today Blog]
*[http://www.psychologicalscience.org/ Association for Psychological Science]
*[http://www.psychonomic.org/ Psychonomic Society]
*[http://www.aaas.org/ The American Association for the Advancement of Science (AAAS)]
*[http://www.reducingstereotypethreat.org/ ReducingStereotypeThreat.org]

===Affiliations===
*[http://psychology.uchicago.edu/people/faculty/sbeilock.shtml Sian L. Beilock, Department of Psychology, University of Chicago]
*[http://www.spatiallearning.org/ Spatial Intelligence and Learning Center (SILC)]
*[http://hpl.uchicago.edu/ Human Performance Lab]

{{DEFAULTSORT:Beilock, Sian}}
[[Category:Living people]]
[[Category:Cognitive psychologists]]
[[Category:Year of birth missing (living people)]]