{{Infobox artwork
| title              = Ashurbanipal 
| image_file         = San Francisco Civic Center Historic District 09.jpg
| caption            = ''Ashurbanipal'' in April 2011
| painting_alignment = 
| image_size         =250px
| alt                = 
| artist             = [[Fred Parhad]]
| catalogue          = 
| year               = 1987–1988<ref name=Smithsonian/>
| type               = [[Sculpture]]
| material           = [[Bronze]]
| subject            = [[Ashurbanipal]]
| height_metric      = 
| width_metric       = 
| length_metric      = 
| height_imperial    = 15
| width_imperial     = 
| length_imperial    = 
| diameter_metric    = 
| diameter_imperial  = 
| dimensions         = 
| metric_unit        = m<!--don't leave blank—either don't include it, or include the default cm. -->
| imperial_unit      = ft<!--don't leave blank—either don't include it, or include the default in. -->
| weight             = approximately {{Convert|1800|lb|kg|abbr=on}}
| condition          = 
| city               = [[San Francisco]], [[California]],<br>United States
| museum             = 
| accession          = 
| pushpin_map        = San Francisco
| pushpin_map_caption= Location in San Francisco
| map_size = 250px
| coordinates        = {{coord|37.77973|-122.415934|type:landmark_region:US-CA|format=dms|display=inline,title}}
| owner              = Administered by the City and County of San Francisco and the [[San Francisco Arts Commission]]
| url                = <!--{{URL|example.com}} Only for official web pages-->
}}
'''''Ashurbanipal''''', also known as the '''Ashurbanipal Monument''' or the '''Statue of Ashurbanipal''',<ref name=AIM>{{Cite web|url=http://www.atour.com/history/landmarks/20120808d.html|title=1988: USA, California, San Jose: The Ashurbanipal Monument|date=August 8, 2012|accessdate=August 9, 2013|publisher=Assyrian Information Management}}</ref> is a [[bronze sculpture]] by [[Fred Parhad]], an artist of [[Assyrian people|Assyrian]] descent. It is located in the [[Civic Center, San Francisco|Civic Center]] of [[San Francisco]], [[California]], in the United States. The {{Convert|15|ft|adj=on}} [[statue]] depicting the Assyrian King [[Ashurbanipal]] (reigned 668-627 BC) the last strong king of the [[Neo-Assyrian Empire]], was commissioned by the Assyrian Foundation for the Arts and presented to the City of San Francisco in 1988 as a gift from the [[Assyrian people]]. The sculpture reportedly cost $100,000 and was the first "sizable" bronze statue of Ashurbanipal.<ref name=Fernandez/> It is administered by the City and County of San Francisco and the [[San Francisco Arts Commission]].

Parhad's work was met with some criticism by local Assyrians, who argued it was inaccurate to portray Ashurbanipal holding a [[clay tablet]] and a lion, or wearing a [[skirt]]. The critics thought the statue looked more like the [[Sumer]]ian king [[Gilgamesh]]; Renee Kovacs, a "scholar and self-stated Assyriologist", believed the sculpture depicted neither figure, but rather a [[Mesopotamia]]n "protective figure". Parhad defended the accuracy of his work, while also admitting that he took [[Artistic license|artistic liberties]].<ref name=Fernandez/>

==Background==
[[File:Hero lion Dur-Sharrukin Louvre AO19862.jpg|thumb|left|Possible representation of Enkidu as [[Master of Animals]] grasping a lion and snake, in an [[Assyrian palace relief]], from [[Dur-Sharrukin]], now Louvre]]
In [[Assyrian sculpture]], the famous colossal entrance way guardian figures of ''[[lamassu]]'' were often acccompanied by a hero grasping a wriggling lion with one hand and typically a snake with the other, also colossal and in high relief; these are generally the only other types of high relief in Assyrian sculpture.  They continue the [[Master of Animals]] tradition in Mesopotamian art, and may represent [[Enkidu]], a central figure in the [[Ancient Mesopotamia]]n ''[[Epic of Gilgamesh]]''.  In the palace of [[Sargon II]] at [[Khorsabad]], a group of at least seven ''lamassu'' and two such heros with lions surrounded the entrance to the "throne room", "a concentration of figures which produced an overwhelming impression of power."<ref>[[Henri Frankfort|Frankfort, Henri]], ''The Art and Architecture of the Ancient Orient'', pp. 147-148, 148 quoted, Pelican History of Art, 4th ed 1970, Penguin (now Yale History of Art), ISBN 0140561072</ref>  The arrangement was repeated in [[Sennacherib]]'s palace at [[Nineveh]].<ref>Russell, John M., Section 6. "c 1000–539 BC., (i) Neo-Assyrian." in Dominique Collon, et al. "Mesopotamia, §III: Sculpture." Grove Art Online, [[Oxford Art Online]], Oxford University Press, accessed 19 November 2016, [http://www.oxfordartonline.com/subscriber/article/grove/art/T057228pg3 subscription required]</ref>

==History==
''Ashurbanipal'' was designed by Fred Parhad, an [[Iraq]]i-born artist of [[Assyria]]n descent. Parhad rejected formal arts studies at the [[University of California, Berkeley]] and relocated to New York, where the [[Metropolitan Museum of Art]] allowed him to study its Assyrian collection.<ref>{{Cite web|url=http://www.caroun.com/Sculpture/Artists/USA/FredParhad/FredParhad.html|title=Fred Parhad|accessdate=August 9, 2013|publisher=Caroun.com}}</ref> The work was commissioned by the Assyrian Foundation for the Arts under the direction of its president, [[Narsai David]].<ref name=Ishaya>Sources by Arianne Ishaya:
* {{Cite journal|work=Journal of Assyrian Academic Studies|first=Arianne|last=Ishaya|title=Settling Into Diaspora: A History of Urmia Assyrians in the United States|accessdate=August 9, 2013|year=2006|volume=20|number=1|page=26|url=http://www.jaas.org/edocs/v20n1/Arianne-diaspora.pdf|format=PDF}}
* {{Cite book|url=https://books.google.com/books?id=Xo_iYXOR0EAC&pg=PA262&lpg=PA262&dq#v=onepage&q&f=false|title=Familiar Faces in Unfamiliar Places: Assyrians in the California Heartland 1911–2010|accessdate=August 9, 2013|page=262|first=Arianne|last=Ishaya|date=October 7, 2010|publisher=Xlibris Corporation}}</ref> The Assyrian Universal Alliance Foundation also claims to have commissioned the work.<ref>{{Cite web|url=http://www.auaf.us/Who%20is%20who/Art/Art.htm|title=Who Is Who|publisher=Assyrian Universal Alliance Foundation|accessdate=August 9, 2013}}</ref> Funds were collected from [[Assyrian/Chaldean/Syriac Americans|Assyrians throughout the United States]].<ref name=Fernandez>{{Cite news|url=https://news.google.com/newspapers?nid=2209&dat=19871228&id=1v4yAAAAIBAJ&sjid=efwFAAAAIBAJ&pg=6964,9530701|title=Statue of Assyrian king in skirt stirs controversy|work=[[The Telegraph (Nashua)|The Telegraph]]|location=Nashua, New Hampshire|volume=119|number=228|oclc=22532489|first=Elizabeth|last=Fernandez|date=December 31, 1987|accessdate=August 9, 2013}}</ref>

In 1987, ''[[The Telegraph (Nashua)|The Telegraph]]'' reported that the work cost $100,000 and was the first "sizable" bronze statue of [[Ashurbanipal]].<ref name=Fernandez/> It was presented to the City of San Francisco as a gift from the [[Assyrian people]] on May 29, 1988,<ref>{{Cite web|url=http://www.batw.org/articles/georgia-hesse_december-2012/|title="Promoting Yourself in the Digital Age" – by Georgia I. Hesse|date=December 2, 2012|accessdate=August 9, 2013|publisher=Bay Area Travel Writers}}</ref> unveiled at the entry to the [[Asian Art Museum of San Francisco|Asian Art Museum]] on [[Van Ness Avenue (San Francisco)|Van Ness Avenue]].<ref name=AIM/> The statue now stands on Fulton Street between the [[San Francisco Public Library|Main Library]] and Asian Art Museum, within the city's [[Civic Center, San Francisco|Civic Center]].<ref name=HMD/><ref>{{Cite web|url=http://sfpl.org/index.php?pg=2000023301|title=Art Works of the Main Library|publisher=[[San Francisco Public Library]]|accessdate=August 9, 2013}}</ref> It is administered by the City and County of San Francisco and the [[San Francisco Arts Commission]].<ref name=Smithsonian>{{Cite  web|url=http://collections.si.edu/search/record/siris_ari_310289|accessdate=August 11, 2013|publisher=[[Smithsonian Institution]]|title= Ashurbanipal, (sculpture)}}</ref>

The [[Smithsonian Institution]] lists Frank Tomsick as the installation's architect and MBT Associates as its architectural firm. ''Ashurbanipal'' was surveyed by the Smithsonian's [[Save Outdoor Sculpture!]] program in 1992.<ref name=Smithsonian/> In 1996, plans for a Civic Center pedestrian mall were being developed by the San Francisco Planning and Urban Research Association; one planner advocated for construction of an Assyrian garden, including [[Nelumbo nucifera|lotus]] blossoms, pomegranate trees and reeds, at the site of the statue.<ref>{{Cite news|url=http://www.sfgate.com/default/article/A-mall-sans-shops-or-cars-3157841.php|title=A mall sans shops or cars|first=Gerald D.|last=Adams|work=[[San Francisco Chronicle]]|date=February 26, 1996|accessdate=August 12, 2013|issn=1932-8672}}</ref>

==Description==
{{multiple image
| align     = left
| direction = vertical
| header    = Inscriptions under the statue
| width     = 200
| image1    = Inscription under the Statue of Ashurbanipal.jpg
| image2    = Inscription under the Statue of Ashurbanipal multilingual inscriptions.jpg
}}

The {{Convert|8|ft|adj=on}} [[patina]]ted [[bronze]] statue,<ref name=Smithsonian/><ref name=Fernandez/> mounted on a base and a [[plinth]] to reach a total height of {{Convert|15|ft}}, weighs approximately {{Convert|1800|lb}}. It depicts Ashurbanipal, the Assyrian king known for building the eponymously named [[Library of Ashurbanipal]], the first and largest library in [[Nineveh]].<ref name=Ishaya/> The bearded king is shown wearing [[earring]]s and a [[tunic]]; he is holding a book (or [[clay tablet]]) in one arm and a lion cub in the other.<ref name=Smithsonian/><ref name=Fernandez/> According to the Historical Marker Database, the tablet reads in [[cuneiform]]: "Peace unto heaven and earth / Peace unto countries and cities / Peace unto the dwellers in all lands / This is the statue presented to the City of San Francisco by the Assyrian people in the 210th year of America's sovereignty".<ref name=HMD>{{Cite web|url=http://www.hmdb.org/marker.asp?marker=32080|title=Ashurbanipal Monument|accessdate=August 9, 2013|publisher=Historical Marker Database}}</ref>

The "larger-than-life", full length statue stands above a plinth adorned with a lotus blossum design and a [[concrete]] base with an [[anti-graffiti coating]].<ref name=Smithsonian/> The base includes rosettes and a bronze plaque.<ref name=Smithsonian/> One inscription below the statue reads the text of the tablet in English, [[Akkadian language|Akkadian]] cuneiform and [[Aramaic language|Aramaic]]. The text "Ashurbanipal, King of Assyria, 669–627 B.C." appears above and the text "Dedicated May 29, 1988" appears below, both in English.<ref name=HMD/> Another inscription below the statue reads, "Presented to the City of San Francisco by the Assyrian Foundation for the Arts through donations of American Assyrian Association of San Francisco / Assyrian American National Federation", followed by a list of names of donors.<ref name=HMD/> In December 2010, the ''[[San Francisco Chronicle]]'' reported that a large plaque from the sculpture was missing.<ref>{{Cite news|url=http://www.sfgate.com/default/article/Passenger-climbs-aboard-needles-and-all-2453277.php|title=Passenger climbs aboard, needles and all|date=December 17, 2010|first= Leah|last=Garchik|work=San Francisco Chronicle|accessdate=August 9, 2013|issn=1932-8672}}</ref>

==Reception==
[[File:San Francisco Civic Center Historic District 11.jpg|thumb|right|The statue, mounted on its base in front of the [[Asian Art Museum of San Francisco|Asian Art Museum]], in 2011]]

In December 1987, as news began to circulate about the commissioned work, local Assyrians accused Parhad of misrepresenting Ashurbanipal. Criticisms included the depiction of the king holding both a book and a lion, which they argued he "wouldn't do", and for dressing him in a skirt, which they claimed he would never have worn.<ref name=Fernandez/> The critics thought the statue made a better portrayal of the [[Sumer]]ian king [[Gilgamesh]]. One critic said: <blockquote>It's very simple. The statue represents Gilgamesh.... No Assyrian has a right to imagine things about our king. It's exactly like making a copy of the Statue of Liberty and saying it is George Washington.... Assyrian kings didn't wear miniskirts. They wouldn't have been holding a lion or a book. It's an insult to the Assyrians.<ref name=Fernandez/></blockquote> Narsai David responded: <blockquote>They are entitled to their opinion.... We have never said this is a museum-quality reproduction.... We have always said this is a characterization of Ashurbanipal as done by a 20th-century artist. If they choose to think of this as Gilgamesh, they are free to do so.<ref name=Fernandez/></blockquote>

Renee Kovacs, a "scholar and self-stated Assyriologist", said the statue depicted neither Ashurbanipal or Gilgamesh, but rather a [[Mesopotamia]]n "protective figure, like a guard."<ref name=Fernandez/> Parhad defended the accuracy of his work, while also admitting that he took [[Artistic license|artistic liberties]] and attempted to incorporate the various aspects of Assyrian culture, from its hunting mastery to its admiration for writing. He said of the sculpture: <blockquote>The piece has authentic qualities to it, but it is also my statue.... With the earrings and the clothing and the hair and his daggers, it is Ashurbanipal. But in the choice of (stance), the fact that he is holding a tablet and a lion, that is mine. I wanted myself to be represented in the piece.<ref name=Fernandez/></blockquote>

==See also==
* [[1988 in art]]
* [[Art in the San Francisco Bay Area]]
* [[Art of Mesopotamia]]
* [[Cultural depictions of lions]]
* [[History of sculpture]]
* [[Sculpture of the United States]]

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Statue of Ashurbanipal}}
* [https://web.archive.org/web/20121103033541/http://www.auaf.us:80/Ashurbanipal%20king/Ashurbanipal%20king.htm Ashurbanipal and His Library] at the Assyrian Universal Alliance Foundation; includes a photograph by Robert B. Livingston
* [https://web.archive.org/web/20080925005058/http://www.faridparhadart.com:80/view044.htm Ashurbanipal Monument] at the artist's official site
* [http://www.dcmemorials.com/index_indiv0000011.htm Ashurbanipal Statue at the Main San Francisco Library in San Francisco, California] at dcmemorials.com
* [http://www.artandarchitecture-sf.com/tag/fred-parhad Civic Center – Ashurbanipal] at Art and Architecture – San Francisco

{{Portal bar|Assyrians|California|San Francisco Bay Area|Visual arts}}

[[Category:1988 establishments in California]]
[[Category:1988 sculptures]]
[[Category:Bronze sculptures in California]]
[[Category:Lions in art]]
[[Category:Outdoor sculptures in San Francisco]]
[[Category:Sculptures of men in California]]
[[Category:Statues in California]]
[[Category:1988 in San Francisco]]

{{Good article}}