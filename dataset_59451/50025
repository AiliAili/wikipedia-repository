{{About|the 2011 role-playing game|the 2014 graphic adventure|Game of Thrones (2014 video game)|other ''Game of Thrones'' games|Works based on A Song of Ice and Fire}}

{{Infobox video game
| title = Game of Thrones
| image = [[File:Game of Thrones RPG.jpg]]
| developer = [[Cyanide (company)|Cyanide]]
| publisher = {{vgrelease|NA|[[Atlus]]}}{{vgrelease|[[Europe|EU]] / [[Australia|AUS]]|[[Focus Home Interactive]]}}
| engine = [[Unreal Engine 3]]
| platforms = [[Microsoft Windows]], [[PlayStation 3]], [[Xbox 360]]
| released = '''Microsoft Windows'''{{Video game release|NA|May 15, 2011|EU|June 8, 2011|AU|June 21, 2011|WW|May 16, 2011 <small>(digital)</small>}}'''Xbox 360, {{nowrap|PlayStation 3}}'''{{Video game release|NA|May 15, 2011|EU|June 8, 2011|AU|June 14, 2011}}
| genre = [[Action role-playing video game|Action role-playing]]
| modes = [[Single-player video game|Single-player]]
}}

'''''Game of Thrones''''' is an [[action role-playing video game|action role-playing]] [[video game]] based on ''[[A Game of Thrones]]'', the first of the ''[[A Song of Ice and Fire]]'' novels by [[George R. R. Martin]], and in part also on the novels' TV adaptation by [[HBO]], ''[[Game of Thrones]]''.

The game was developed by [[Cyanide (company)|Cyanide]] and published by [[Atlus]] in North America and [[Focus Home Interactive]] in Europe and Australia. [[James Cosmo]] and [[Conleth Hill]] reprise their roles from the HBO series as Lord Commander Jeor Mormont and Lord Varys, respectively. George R. R. Martin has a [[cameo appearance]] as Maester Martin in Castlewood. The game also uses other assets from the HBO series, such as the music.

==Plot summary==

The game takes place concurrently with season 1 of [[Game of Thrones]], and switches between two characters, Alester Sarwyck and Mors Westford. Alester, a knight, was the heir to Sarwyck, a lordly house under Lannister, but had fled to Essos 15 years before, being traumatized by an unspecified event in the course of Robert's Rebellion. During that time, he converted to the religion of R'hllor and became a priest. He returns to Riverspring, his home, after hearing of his father's death. On the other hand, Mors Westford is one of the best rangers of the Night's Watch, and prior to that, he was one of the best knights for Lannister. Near the end of Robert's Rebellion, he refused a direct order to kill Elia Martell and her two infants, thereby putting his life in danger, as well as his wife's and his daughter's. He sent the latter two into hiding and was persuaded to join the Night's Watch to avoid execution. Mors is also a skinchanger, and has a dog which he can control at will.

Alester, at his father's funeral, learns that his younger brother Gawen was recently disinherited, and has gone missing following his father's death. Also, Gawen is suspected of murdering his father with poison. Meanwhile, Alester's half-brother, Valarr Hill, a bastard and a knight of the queen's guard, is engaged to Alester's sister, Elyana, and is set to become Riverspring's next lord, which Alester is determined to stop. As the funeral ends, a riot occurs among starving peasants. Alester takes command and uses either force or negotiation. Then, to arrest Gawen, Alester goes to King's Landing to look for clues. While there, he is arrested by the City Watch at Valarr's instigation. Varys helps him escape and meet with the queen, Cersei, who decides to hire him for secret missions, and to make him compete against Valarr. The two are sent to kill a bastard named Harry Waters, who, for some reason, is protected by knights from Jon Arryn, led by Godric Donnerly. With Godric dead, they learn that Arryn was protecting another woman and sent her to the Wall. Valarr sends Yohn to dress up as Godric, and with 100 men, to seize her dead or alive. Alester then encounters Lord Arwood Harlton, who offers to help find Gawen. They find a genealogical book which proves that the queen's children are illegitimate. They search a sewer for Gawen but find bandits sent by Janos Slynt. Alester infiltrates the City Watch and finds a body, apparently Gawen's, and a letter showing that Slynt was hired, in turn, by Valarr. At this point, Harlton reveals that he is part of a conspiracy to reinstate the Targaryens, as was Alester's father. Alester joins and goes to Harlton's estate, Castlewood.

While all this happens, Mors Westford is tasked with hunting down Night's Watch deserters. On one chase, he takes three newly sworn men to the castle Icemark, but they are attacked by wildlings. Two men die, one man named Poddy deserts, and Mors survives only when rescued by Qhorin Halfhand. Mors pursues and kills Poddy. Back at Castle Black, Mors receives a letter from Jon Arryn asking him to protect a girl named Jeyne Greystone. By coincidence, Arryn's knights, led by Godric, appear to help Mors locate her. They find her in Mole's Town but Jeyne insists that Godric is an imposter. Mors and others have several fights against the false Godric's party, including Night's Watch deserters who were bribed. As Yohn, the false Godric, dies, he reveals he was sent by Valarr. Jeor Mormont is furious and wants to strike back. He orders Mors to go south, to hide Jeyne and to bring Valarr to justice. Weeks later, Mors and Jeyne arrive at the safe-house of his family, now deserted, where Jeyne reveals that she is a Targaryen bastard, and what is more, one of King Robert's mistresses and pregnant with the king's child. Shortly after, Mors discovers his wife and daughter's graves. They are attacked by Valarr's men, but soldiers from Harlton counterattack and take the pair into custody.

Harlton reveals that, as a top advisor, he arranged Jeyne's relationship with the king to produce a half-Targaryen, half-Baratheon to be a puppet queen. Mors is tortured in the dungeon, but one night, aids another prisoner, Gawen, in going free. Gawen meets with his brother Alester, living upstairs, and reveals that Harlton murdered their father and has been deceiving Alester. Alester liberates Mors, but Gawen is killed. He and Mors are forced to flee without Jeyne. At Riverspring, they find that Valarr is holding the entire town hostage, but they successfully break in, derailing Valarr's wedding. Mors duels Valarr, but is killed by Valarr's shadow-magic. As he dies, Valarr reveals that he was behind the murder of Mors's family. The wedding guests proclaim the duel invalid because of the shadow-magic, and try to support Alester, but Valarr reacts by massacring the town, also killing Elyana. Alester and his supporters organize a resistance while Valarr flees. Afterward, Alester pays respects to Mors by performing a fire-kiss, part of his religion, which unwittingly brings Mors back to life. As Valarr attacks Castlewood seeking to capture Jeyne, Alester and Mors sneak into the building, but find that Jeyne is in labor. Harlton is killed by Valarr's shadow-magic, while Jeyne, who has delivered, purposely gets killed, hoping that people will believe that the baby was unborn. Back in Riverspring, Alester and Mors learn that the king is dead. A few days later, as Stark is being executed, they find and kill Valarr, having stolen a Valyrian sword to fight off Valarr's demons. Valarr, with his dying breath, reveals that he and Alester killed Mors's family together, acting on Lannister orders. This is what traumatized Alester so deeply. Alester and Mors duel to the death. One of them survives and is confronted by Varys, who offers to send Jeyne's baby to Essos to be cared for. Four possible endings:

* Alester survives, sends the baby to Essos, and confronts the queen, whereupon he is dragged away for a quick execution.
* Alester survives, gives the baby to the queen, and inherits Riverspring, but is depressed and contemplates suicide.
* Mors survives, sends the baby to Essos, and returns to the wall, regularly executing deserters and becoming highly jaded.
* Mors survives and deserts the Night's Watch, hoping to raise the baby in Essos himself, as sworn brothers approach and fight him.

==Development==
{{expand section|date=May 2013}}

===Downloadable content===
In November 2012 [[Cyanide (company)|Cyanide]] published the [[downloadable content]] (DLC) "Behind the Wall". It is set 10 years before the plot of ''Game of Thrones''. It was released for PC platforms, PlayStation 3 and Xbox 360 in Europe. A North American release was planned, but dropped due to the poor critical reception the game had gotten. Gorold, Mors, and a Sworn Brother named Weasel are forced to go beyond the Wall, after a Night's Watch builder, Maekar, is abducted by wildlings. Weasel betrays Gorold and Mors to a wildling chief named Bael. Bael forces Mors and Gorold to take part in his pit fights. Mors meets his dog here, who is also forced to fight.

After Mors and Gorold make an unpopular decision in the pits, a wildling, presumably angry at losing his wager, breaks into Mors' cage to assassinate him, but Mors' dog kills him. This enables Mors to find Gorold and escape the camp. However, Mors insists that they find Maekar first. Maekar is found to have had his arms and legs removed, and reveals that they were taken to provide meat to feed the pit fighters. He also tells Mors that he revealed the Wall's secrets and weaknesses to the Wildlings. Mors sends Gorold back to get reinforcements, and proceeds to massacre the entire Wildling encampment. When the Sworn Brothers return, one comments that it is "like a slaughterhouse," and Gorold, finding Mors, replies, "Aye... And there's the butcher," giving Mors his nickname.

==Reception==
{{Video game reviews
<!-- Aggregators -->
| GR = (PC) 57.30%<ref name=GRPC>{{cite web |url=http://www.gamerankings.com/pc/642431-game-of-thrones/index.html |publisher=[[GameRankings]] |title=Game of Thrones (PC) |accessdate=2012-10-24}}</ref><br />(X360) 53.59%<ref name=GRX360>{{cite web |url=http://www.gamerankings.com/xbox360/639686-game-of-thrones/index.html |publisher=[[GameRankings]] |title=Game of Thrones (X360) |accessdate=2012-10-24}}</ref><br />(PS3) 52.86%<ref name=GRPS3>{{cite web |url=http://www.gamerankings.com/ps3/639685-game-of-thrones/index.html |publisher=[[GameRankings]] |title=Game of Thrones (PS3) |accessdate=2012-10-24}}</ref>
| MC = (PC) 58/100<ref name=MCPC>{{cite web|url=http://www.metacritic.com/game/pc/game-of-thrones |title=Game of Thrones for PC Reviews, Ratings, Credits, and More |publisher=Metacritic |accessdate=2012-10-24}}</ref><br />(PS3) 53/100<ref name=MCPS3>{{cite web|url=http://www.metacritic.com/game/playstation-3/game-of-thrones |title=Game of Thrones for PlayStation 3 Reviews, Ratings, Credits, and More |publisher=Metacritic |accessdate=2012-10-24}}</ref><br />(X360) 52/100<ref name=MCX360>{{cite web|url=http://www.metacritic.com/game/xbox-360/game-of-thrones |title=Game of Thrones for Xbox 360 Reviews, Ratings, Credits, and More |publisher=Metacritic |accessdate=2012-10-24}}</ref>
<!-- Reviewers -->
| IGN = 4/10<ref name=IGN>{{cite news|last=Legarie|first=Destin|title=Game of Thrones Review|url=http://www.ign.com/articles/2012/05/15/game-of-thrones-review|accessdate=18 May 2012|newspaper=[[IGN]]|date=15 May 2012}}</ref>
}} 
''Game of Thrones'' received mixed reviews. Aggregating review websites [[GameRankings]] and [[Metacritic]] gave the PC version 57.30% and 58/100,<ref name=GRPC /><ref name=MCPC /> the Xbox 360 version 53.59% and 52/100<ref name=GRX360 /><ref name=MCX360 /> and the PlayStation 3 version 52.86% and 53/100 respectively.<ref name=GRPS3 /><ref name=MCPS3 /> [[IGN]] praised the well-crafted plot, but criticized its poor execution through low-quality graphics, sound design, animations and voice acting, as well as a repetitive combat experience.<ref name=IGN />

{{clear}}

==References==
{{reflist}}

==External links==
* {{official website|http://www.gameofthrones-thegame.com/}}

{{ASOIAF}}
{{Cyanide}}

[[Category:2012 video games]]
[[Category:Action role-playing video games]]
[[Category:Atlus games]]
[[Category:Fantasy video games]]
[[Category:PlayStation 3 games]]
[[Category:Single-player-only video games]]
[[Category:Unreal Engine games]]
[[Category:Video games based on A Song of Ice and Fire]]
[[Category:Video games based on television series]]
[[Category:Video games developed in France]]
[[Category:Video games with alternate endings]]
[[Category:Windows games]]
[[Category:Xbox 360 games]]