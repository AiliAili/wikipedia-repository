__NOTOC__
{|{{Infobox Aircraft Begin
|name            = 1907 Monoplane
|image           = Epps 1907 Monoplane replica 002 crop.jpg
|size            = 280 px
|alt             =
|caption         = Epps 1907 Monoplane replica at Valiant Air Command Warbird Museum.
|long caption    =
}}
{{Infobox Aircraft Type
|type            = Experimental
|national origin = United States
|manufacturer    =
|designer        = [[Ben T. Epps]]
|first flight    = October 1907
|introduction    = <!--Date the aircraft is to enter military or revenue service; use sparingly. -->
|introduced      =
|retired         = <!--Date the aircraft left service. If vague or more than a few dates, skip this -->
|status          = <!--In most cases, redundant; use sparingly -->
|primary user    = <!-- List only one user; for military aircraft, this is a nation or a service arm. Please DON'T add those tiny flags, as they limit horizontal space. -->
|more users      = <!-- Limited to THREE (3) 'more users' here (4 total users).  Separate users with <br/>. -->
|produced        = <!--Years in production (eg. 1970-1999) if still in active use but no longer built -->
|number built    = <!--Number of flight capable aircraft completed, for aircraft under development or still in production only those aircraft that have flown should be included. -->
|program cost    = <!--Total program cost-->
|unit cost       = <!--Incremental or flyaway cost for military or retail price for commercial aircraft -->
|developed from  = <!--The aircraft which formed the basis for this aircraft -->
|variants with their own articles = <!--Variants OF this aircraft -->
|developed into  = <!--For derivative aircraft based on this aircraft -->
}}
|}

The '''Epps 1907 Monoplane''' was a pioneering aircraft built and flown in 1907 by [[Ben T. Epps]] of [[Athens, Georgia]] from an original design. The aircraft consisted of an open framework suspended below a wire-braced monoplane wing. The undercarriage consisted of three bicycle wheels,<ref name="hudson">Hudson 2002</ref> one at the front of this frame, and two behind it. A buggy seat<ref name="hudson"/> was located beneath the wing for the pilot. A 15-horsepower (11&nbsp;kW) two-cylinder [[Anzani]]<ref name="ghs">The Georgia Historical Society et al. 2007</ref><ref name="ea">''Epps Aviation: Above and Beyond'', p.3</ref> motorcycle engine<ref name="aued">Aued 2007</ref><ref name="who">"Who Was Ben Epps?</ref> was mounted behind the seat and drove a two-bladed propeller from an exhaust fan<ref name="aued"/> mounted pusher-fashion behind the wing's trailing edge. A biplane elevator unit was carried on struts at the front of the aircraft, and a single rudder on struts to its rear. The airframe was made from scrap timber collected from a sawmill,<ref name="aued"/> with the flying surfaces covered in cotton.<ref name="aued"/> Only the undersurfaces of the wings were covered.<ref name="cleland">Cleland 1985, p.4B</ref>

Inspired by the [[Wright Brothers]]<ref name="aued"/><ref name="nelson01"/> and pioneering European aviators,<ref name="aued"/> Epps first conceived of the design at the age of sixteen.<ref name="hof">"Epps, Ben T.</ref> In 1907, he built the aircraft in the workshop of his bicycle, electrical contracting, and automobile repair business on Washington Street, Athens.<ref name="hudson"/>

In October 1907, he flew the machine from a cow pasture<ref name="who"/> near Brooklyn Creek.<ref name="aued"/> After rolling downhill,<ref name="hudson"/><ref name="aued"/><ref name="who"/> Epps took off and flew around 100 yards (90 metres) at a maximum altitude of around 50 feet (15 metres).<ref name="hudson"/><ref name="who"/> The flight ended in a crash,<ref name="cleland"/><ref name="nelson01">Nelson 2001</ref> but made Epps Georgia's first aviator.<ref name="who"/><ref name="nelson01"/> In 1949, Lola Trammel told ''[[The Atlanta Journal Magazine]]'' that Epps had already made a successful flight in the machine prior to the public demonstration, testing the machine by moonlight with the help of friends at two o'clock in the morning.<ref name="mcmichael">McMichael 2007, p.1</ref>

The [[Valiant Air Command Warbird Museum]] in [[Titusville, Florida]] has a replica of the aircraft on display.<ref name="vac">"Photos from 1999 Bulletins</ref> Bearing the registration N1907, it was constructed by John D. Pruett.<ref name="faa">FAA Registry</ref>

==Specifications==
{{aerospecs
|ref=<!-- reference -->The Georgia Historical Society et al 2007
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->eng
|crew=One pilot
|capacity=
|length m=
|length ft=
|length in=
|span m=10
|span ft=35
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=
|gross weight lb=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=2-cylinder [[Anzani]]
|eng1 kw=<!-- prop engines -->11
|eng1 hp=<!-- prop engines -->15
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|perfhide=Y
|max speed kmh=
|max speed mph=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
* {{cite journal |last=Aued |first=Blake |title=Ben Epps' milestone turns 100 |journal= [[Athens Banner-Herald]] |date=14 October 2007 |url=http://www.onlineathens.com/stories/101407/news_20071014048.shtml |accessdate=2010-03-01 }}
* {{cite journal |last=Cleland |first=Max |title=Epps launched aviation in Georgia, a little late |journal=The Rockmart Journal |date=23 October 1985 |pages=4B |url=https://news.google.com/newspapers?id=EAUKAAAAIBAJ&sjid=ID4DAAAAIBAJ&pg=6719%2C2479741 |accessdate=2010-03-06 }}
* {{cite book |title=Epps Aviation: Above and Beyond |publisher=Epps Aviation |url=http://www.eppsaviation.com/pdf/Epps-Aviation_Coporate-Brochure.pdf |accessdate=2010-03-06}}
* {{cite web |title=Epps, Ben T. |work=Georgia Aviation Hall of Fame |url=http://www.gahof.org/Featured-GA-Aviator.14.0.html?hofuid=14 |accessdate=2010-03-01 }}
* [http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=1907 FAA Registry search for N1907], retrieved 2010-03-06.
* The Georgia Historical Society, Athens–Clarke County Government, Georgia Aviation Hall of Fame, and the Epps Cousins Club (2007). [http://www.georgiahistory.com/markers/133 ''First Flight in Georgia: Ben Epps' Garage''] (Historical Marker 29-5, located {{nowrap|120 E. Washington Street, Athens, Georgia — {{coord|33|57.533|N|83|22.617|W|display=inline}} }}).
* {{cite web |last=Hudson |first=Paul Stephen |title= Ben Epps (1888-1937) |work=The New Georgia Encyclopedia |date=6 December 2002 |publisher=Georgia Humanities Council |location=Athens, Georgia |url=http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-770 |accessdate=2010-03-01 }}
* {{cite journal|last=McMichael |first=Pate |title=A Wing and a Prayer |journal=Aviation Gazette |year=2007 |publisher=Center of Innovation for Aerospace |pages=1, 7 |url=http://aerospace.georgiainnovation.org/default/download_pdf/70 |accessdate=2010-03-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20100705163642/http://aerospace.georgiainnovation.org/default/download_pdf/70 |archivedate=2010-07-05 |df= }}
* {{cite journal |last=Nelson |first=Don |title=Epps, pioneer of Georgia aviation, was an Athenian |journal= [[Athens Banner-Herald]] |date=28 October 2001 |url=http://www.onlineathens.com/stories/103001/ath_epps.shtml |accessdate=2010-03-01 }}
* {{cite web|title=Photos from 1999 Bulletins |work=Valiant Air Command Warbird Museum |url=http://www.vacwarbirds.org/airplanephotos/1999/index.html |accessdate=2010-03-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20090614112339/http://www.vacwarbirds.org/airplanephotos/1999/index.html |archivedate=June 14, 2009 }} 
* {{cite web |title= Who Was Ben Epps? |work=Athens–Clarke Heritage Foundation |date=1 October 2009 |publisher=Athens–Clarke Heritage Foundation |location=Athens, Georgia |url=http://www.achfonline.org/docs/Who%20Was%20Ben%20T.%20Epps.3.pdf |accessdate=2010-03-01 }}

==External links==
* [http://fax.libs.uga.edu/Ms3074/iMs3074.html Digitized copy of the ''Benjamin Thomas Epps Scrapbook 1904–1963''], Hargrett Rare Book and Manuscript Library, [[University of Georgia]]

{{Epps aircraft}}

[[Category:United States experimental aircraft 1900–1909]]
[[Category:Individual aircraft]]