{{multiple issues|
{{Orphan|date=November 2014}}
{{notability|Companies|date=October 2014}}
}}

{{Infobox company
|name          = LaPorte CPAs and Business Advisors
|type          = [[Professional Corporation]]
|industry              = [[Accounting]]
|foundation            = [[New Orleans]], [[Louisiana]], USA (1946)
|location_city         = 111 Veterans Memorial Blvd., Suite 600 <br> [[Metairie]], [[Louisiana]]  70005
|area_served           = Gulf Coast region
|services              = [[Accounting]]<br />[[Assurance services|Audit and assurance]]<br/>[[Business Consulting|Consulting]]<br/>[[Tax]]<br/>Other
|revenue               = {{increase}} $22 Million [[U.S. dollar|USD]] <small>(2013)</small>
|homepage              = http://www.laporte.com
}}

'''LaPorte CPAs and Business Advisors''' is an accounting and business advisory firm based in [[New Orleans]], USA.  From 1973 until 2010, the company was known as LaPorte Sehrt Romig & Hand in the Gulf Coast region marketplace and is commonly referred to as LaPorte. The firm sells audit, tax, accounting, and consulting services.  It belongs to the [[McGladrey]] Alliance of companies.

== Milestones ==

2004 Merged with Smith, Huval & Associates, L.L.C., in [[Covington, Louisiana]]<br/>
2007 Merged with The Gautreau Group, L.L.C., in Baton Rouge, Louisiana, and established a Baton Rouge office.<ref>{{cite web|title=LaPorte to Merge with Gautreau|url=http://www.accountingtoday.com/news/24760-1.html|publisher=Accounting Today|accessdate=14 May 2014|author=WebCPA staff|date=July 18, 2007}}</ref><ref>{{cite web|title=New Orleans accounting firm to merge with Baton Rouge group|url=http://blog.nola.com/tpmoney/2007/07/new_orleans_accounting_firm_to.html|publisher=The Times-Picayune|accessdate=12 May 2010|author=Kimberly Quillen|date=July 13, 2007}}</ref><br/>
2010 Merged with Hidalgo, Banfill, Zlotnik & Kermali, P.C., in Houston, Texas, and established a Houston office<ref>{{cite web|title=LaPorte Sehrt Romig Hand will acquire CPA firm in Houston|url=http://www.nola.com/business/index.ssf/2010/08/laporte_sehrt_romig_hand_will.html|publisher=The Times-Picayune|accessdate=10 August 2010|author=Kimberly Quillen|date=August 10, 2010}}</ref><ref>{{cite web|title=Houston CPA firm sets merger with New Orleans group|url=http://www.bizjournals.com/houston/stories/2010/08/09/daily15.html|publisher=Houston Business Journal|accessdate=10 August 2010|author=Casey Wooten|date=Aug 10, 2010}}</ref><ref>{{cite web|title=LaPorte Sehrt Expands into Texas|url=http://www.accountingtoday.com/news/LaPorte-Sehrt-Expands-Texas-55685-1.html||publisher=Accounting Today|author=WebCPA Staff|date=September 22, 2010}}</ref><br/>
2011 Changed firm name from LaPorte Sehrt Romig & Hand to LaPorte CPAs & Business Advisors<ref>{{cite web|title=LaPorte Sehrt CPAs changes name|url=http://www.bizjournals.com/houston/news/2011/08/29/laporte-sehrt-cpas-changes-name.html|publisher=Houston Business Journal|accessdate=19 March 2014|author=Houston Business Journal Staff|date=Aug 29, 2011}}</ref>

== National Industry Recognition ==

*''INSIDE Public Accounting'' named LaPorte one of the “Top 200 Accounting Firms” for 2010, 2011, 2012, and 2013.<ref>{{cite journal|title=Beyond The IPA Top 100|journal=Inside Public Accounting|date=September 2010|issue=September 2014|url=http://www.insidepublicaccounting.com/PDF/top200_2010.pdf|format=PDF|accessdate=19 March 2014}}</ref><ref>{{cite journal|title=SPECIAL REPORT THE 2011 BEYOND THE IPA 100|journal=Inside Public Accounting|date=2011|issue=September 2011|url=http://www.insidepublicaccounting.com/PDF/2011_beyondtheIPA100.pdf|format=PDF|accessdate=19 March 2014}}</ref><ref>{{cite web|title=SPECIAL REPORT THE 2012 IPA 200|url=http://www.insidepublicaccounting.com/PDF/IPA200_2012.pdf|publisher=Inside Public Accounting|accessdate=19 March 2014|author=Inside Public Accounting Staff|format=PDF|date=September 2012}}</ref><ref>{{cite journal|title=IPA SPECIAL SUPPLEMENT THE 2013 IPA 200|journal=Inside Public Accounting|date=September 2013|volume=27|series=9A|issue=September 2013|page=6|url=http://www.insidepublicaccounting.com/PDF/IPA200_2013.pdf|format=PDF|author=Inside Public Accounting Staff}}</ref>
*Accountants Media Group, publishers of ''Accounting Today'', included LaPorte among 35 national “Beyond the Top 100: Firms to Watch” and rated LaPorte ninth of 17 on the journal’s “2013 Regional Leaders” among Gulf Coast firms.<ref>{{cite web|title=Top 100 Firms|url=http://digital.accountingtoday.com/accountingtoday/2013top100firms?pg=20#pg20|publisher=Accounting Today|accessdate=14 May 2014|author=Accounting Today Staff|pages=20|format=PDF|year=2013}}</ref>
*Accountants Media Group, publishers of ''Accounting Today'', named LaPorte one of the “Best Accounting Firms to Work for” in the United States for four consecutive years (2008, 2009, 2010, and 2011).<ref>{{cite web|title=The Best Accounting Firms to Work For|url=http://bestaccountingfirmstoworkfor.com/Final_BestFirms08REV.pdf|publisher=Accounting Today|accessdate=20 May 2014|author=Daniel Hood|location=New York, NY|format=PDF|date=10 Oct 2008}}</ref><ref>{{cite web|title=The 2009 Best Accounting Firms to Work For|url=http://www.accountingtoday.com/ato_issues/23_19/the-2009-best-accounting-firms-to-work-for-52631-1.html|publisher=Accounting Today|accessdate=20 May 2014|author=Staff Editor|date=14 Dec 2009}}</ref><ref>{{cite web|title=2010 "Best Accounting Firms to Work for"|url=http://www.bestaccountingfirmstoworkfor.com/index.php?option=com_content&task=view&id=57|publisher=AccountingToday|accessdate=14 May 2014|author=AccountingToday Staff|year=2010}}</ref>

== Local Recognition ==

''New Orleans CityBusiness'' has included LaPorte on its list of “Best Places to Work” for seven consecutive years (2007 to 2013).<ref>{{cite web|title=Best Places to Work 2007|url=http://neworleanscitybusiness.com/best-places-to-work/best-places-to-work-2007/|publisher=New Orleans Publishing Group|accessdate=15 June 2013}}</ref><ref>{{cite web|title=Best Places to Work 2008|url=http://neworleanscitybusiness.com/best-places-to-work/best-places-to-work-2008/|publisher=New Orleans Publishing Group|accessdate=14 May 2014|year=2008}}</ref><ref>{{cite web|title=Best Places to Work 2009|url=http://neworleanscitybusiness.com/best-places-to-work/best-places-to-work-2009-2/|publisher=New Orleans Publishing Group|accessdate=19 March 2014|year=2009}}</ref><ref>{{cite web|title=CityBusiness announces Best Places to Work for 2010|url=http://neworleanscitybusiness.com/blog/2010/10/07/citybusiness-announces-best-places-to-work-for-2010/|publisher=New Orleans Publishing Group|accessdate=14 May 2014|author=Christian Moises|date=October 7, 2010}}</ref><ref>{{cite web|title=Best Places to Work 2011|url=http://neworleanscitybusiness.com/best-places-to-work/best-places-to-work-2011/|publisher=New Orleans Publishing Group|accessdate=14 May 2014|author=Staff Editor|year=2011}}</ref><ref>{{cite web|title=Best Places to Work 2012|url=http://neworleanscitybusiness.com/best-places-to-work/best-places-to-work-2012/|publisher=New Orleans Publishing Group|accessdate=14 May 2014|author=Staff Editor|year=2012}}</ref><ref>{{cite web|title=Best Places to Work 2013|url=http://neworleanscitybusiness.com/best-places-to-work/best-places-to-work-2013/|publisher=New Orleans Publishing Group|accessdate=14 May 2014|author=Staff Editor|year=2013}}</ref>

== Legal Structure ==

LaPorte is structured as A Professional Accounting Corporation ([[Professional corporation|APAC]]).

== References ==

{{reflist}}

== External links ==
*[http://www.laporte.com LaPorte]
*[http://mcgladrey.com/content/mcgladrey/en_US/who-we-are/mcgladrey-alliance.html Who we are]
*[http://www.journalofaccountancy.com/issues/2009/mar/redesignedform990.html Bonnie Wyllie authors article on Redesigned Form 990]
*[https://books.google.com/books/about/Measuring_Damages_Involving_Individuals.html?id=IJtaNwAACAAJ Holly Sharp authors Measuring Damages Involving Individuals]
*[https://www.youtube.com/watch?v=Hd4HQpS72CQ Ted Mason speaks about the McGladrey Alliance Forum]
*[http://www.accountingtoday.com/acto_blog/mcgladrey-names-2011-partner-development-graduates-56879-1.html McGladrey names 2011 partner development graduates]
*[http://ylcnola.org/stephen-romig-2 Stephen Romig named a Role Model of the Year by Young Leadership Council]
*[http://lagniappe.lcpa.org/Vizion5/viewer.aspx?shareKey=lZVhvZ#issueID=24&pageID=10 LaPorte’s Michele Avery authors article on business valuation]

[[Category:1946 establishments in Louisiana]]
[[Category:Accounting firms of the United States]]
[[Category:Companies based in New Orleans]]
[[Category:Companies established in 1946]]