{{Infobox journal
| title = International Journal of Humanoid Robotics
| cover = [[File:International Journal of Humanoid Robotics.gif]]
| discipline = [[Robotics]], [[engineering]]
| abbreviation = Int. J. Humanoid Robot.
| publisher = [[World Scientific]]
| country =
| frequency = Quarterly
| impact = 0.368
| impact-year = 2012
| website = http://www.worldscinet.com/ijhr/ijhr.shtml
| link2 = http://www.worldscientific.com/loi/ijhr
| link2-name = Online archive
| ISSN = 0219-8436
| eISSN = 1793-6942
| OCLC = 648909734
}}
The '''''International Journal of Humanoid Robotics''''' is a quarterly [[Peer review|peer-reviewed]] [[scientific journal]] covering the development of intelligent humanoid robots, both theoretical and practical, with an emphasis on future projections. Some areas covered include design, mental architecture, kinematics, visual perception and [[human–robot interaction]]. It was established in 2004 and is published by [[World Scientific]]. The [[editors-in-chief]] are Ming Xie ([[Nanyang Technological University]]), Juyang Weng ([[Michigan State University]]), and Jean-Guy Fontaine ([[Istituto Italiano di Tecnologia]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Compendex]], [[Science Citation Index Expanded]], [[Current Contents]]/Engineering, Computing, and Technology, [[Inspec]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.368.<ref name=WoS>{{cite book |year=2013 |chapter=International Journal of Humanoid Robotics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.worldscinet.com/ijhr/ijhr.shtml}}


[[Category:Robotics journals]]
[[Category:Publications established in 2004]]
[[Category:World Scientific academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]