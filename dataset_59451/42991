{{Redirect|XF11|the near-Earth asteroid|(35396) 1997 XF11}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = XF-11
 |image = Xf11 usaf.jpg
 |caption = The second Hughes XF-11 during a 1947 test flight
}}{{Infobox Aircraft Type
 |type = [[Reconnaissance]]
 |manufacturer = [[Hughes Aircraft]]
 |designer = [[Howard Hughes]]
 |first flight = 7 July 1946
 |introduced =
 |retired =
 |produced =
 |number built = 2
 |status = Cancelled
 |unit cost =
 |primary user = [[U.S. Army Air Force]]
 |more users = 
 |developed from = [[Hughes D-2]]
 |variants with their own articles = 
}}
|}

The '''Hughes XF-11''' was a prototype military [[reconnaissance aircraft]], designed and flown by [[Howard Hughes]] and built by [[Hughes Aircraft]] for the [[United States Army Air Forces]]. Although 100 F-11s were ordered in 1943, only two prototypes and a mockup were completed. During the first XF-11 flight in 1946, Howard Hughes crashed the aircraft in [[Beverly Hills, California]].<ref>Parker, Dana T. ''Building Victory: Aircraft Manufacturing in the Los Angeles Area in World War II,'' pp. 49-51, Cypress, CA, 2013. ISBN 978-0-9897906-0-4.</ref> The production aircraft had been cancelled in May 1945, but the second prototype was completed and successfully flown in 1947. The program was extremely controversial from the beginning, leading the U.S. Senate to investigate the F-11 and the [[Hughes H-4 Hercules]] flying boat in 1947–1948.

==Design and development==
While Hughes had designed its predecessors to be fighter variants, the F-11 was intended to meet the same operational objective as the [[Republic XF-12 Rainbow]]. Specifications called for a fast, long-range, high-altitude photographic reconnaissance aircraft. A highly modified version of the earlier private-venture [[Hughes D-2]] project, in configuration the aircraft resembled the [[World War II]] [[Lockheed P-38 Lightning]], but was much larger and heavier.<ref>Winchester 2005, p. 222.</ref> It was a tricycle-gear, twin-engine, [[twin-boom]] all-metal [[monoplane]] with a pressurized central crew [[nacelle]], with a much larger [[wingspan|span]] and much higher [[aspect ratio (wing)|aspect ratio]] than the P-38's wing.

The XF-11 used [[Pratt & Whitney R-4360]]-31 28-cylinder radial engines. Each engine drove a pair of [[contra-rotating propellers|contra-rotating]] four-bladed, controllable-pitch propellers, which can increase performance and stability, at the cost of increased mechanical complexity. Due to constant problems with the contra-rotating propulsion system, the second prototype had regular single four-bladed propellers.

On the urgent recommendation of Colonel [[Elliott Roosevelt]], who led a team surveying several reconnaissance aircraft proposals in September 1943, General [[Henry H. Arnold|Henry "Hap" Arnold]], chief of the [[U.S. Army Air Forces]], ordered 100 F-11s for delivery beginning in 1944. In this, Arnold overrode the strenuous objections of the USAAF [[Air Force Materiel Command|Materiel Command]], which held that Hughes did not have the industrial capacity or proven track record to deliver on his promises. (Materiel Command did succeed in mandating that the F-11 be made of aluminum, unlike its wooden D-2 predecessor.) Arnold made the decision "much against my better judgment and the advice of my staff" after consultations with the [[White House]].<ref>Hansen 2012, p. 541.</ref> The order was cancelled on 29 May 1945, but Hughes was allowed to complete and deliver the two prototypes.

Numerous difficulties of both a technical and managerial nature accompanied the program from the beginning. From 1946-1948, the Senate subcommittee to investigate the Defense Program, popularly known as the [[Truman Committee]] and then the [[Brewster Committee]], investigated the F-11 and H-4 programs, leading to the famous Hughes-Roosevelt hearings in August 1947.<ref>Hansen 2012, pp. 530–536.</ref> The program cost the federal government $22 million.

==Operational history==
[[File:1946-07-11 Hughes Plane Crash.ogv|thumb|thumbtime=3|1946 newsreel]]

The first prototype, tail number ''44-70155'', piloted by [[Howard Hughes|Hughes]], crashed on 7 July 1946 while on its maiden flight from the [[Hughes Aircraft Co.]] factory airfield at [[Culver City, California]].<ref name="Crash">[http://www.check-six.com/Crash_Sites/XF-11_crash_site.htm "Crash of the XF-11."] ''check-six.com''. Retrieved: 16 June 2010.</ref>

Hughes did not follow the agreed testing program and communications protocol, and remained airborne almost twice as long as planned. An hour into the flight (after onboard recording cameras had run out of film), a leak caused the right-hand propeller controls to lose their effectiveness and the rear propeller subsequently reversed its pitch, disrupting that engine's thrust, which caused the aircraft to yaw hard to the right.<ref name="Winchester p.223">Winchester 2005, p. 223.</ref> The USAAF account said that, "It appeared that loss of hydraulic fluid caused failure of the pitch change mechanism of right rear propeller. Mr. Hughes maintained full power of right engine and reduced that of left engine instead of trying to fly with right propeller windmilling without power. It was [[Wright Field]]'s understanding that the crash was attributed to pilot error."<ref>USAAF Materiel Command to Gen. Spaatz, 16 August 1946, at F-11 project file at Air Force Historical Research Agency</ref>

Rather than feathering the propeller, Hughes performed improvised troubleshooting (including raising and lowering the gear) during which he flew away from his factory runway. Constantly losing altitude, he finally attempted to reach the golf course of the [[Los Angeles Country Club]], but about 300&nbsp;yards short of the course, the aircraft suddenly lost altitude and clipped three houses. The third house was destroyed by fire, and Hughes was nearly killed.<ref name="Crash"/> The crash is dramatized in the 2004 film ''[[The Aviator (2004 film)|The Aviator]]''.

The second prototype was fitted with conventional propellers and flown by Hughes on 5 April 1947, after he had recuperated from his injuries. Initially, the USAAF had insisted that Hughes not be allowed to fly the aircraft, but after a personal appeal to Generals [[Ira Eaker]] and [[Carl Spaatz]], he was allowed to do so against posting of $5 million in security.<ref>Hansen 2012, p. 562, quoting Eaker's Nov. 1947 testimony to the Senate.</ref>

This test flight was uneventful, and the aircraft proved stable and controllable at high speed. It lacked low-speed stability, however, as the [[aileron]]s were ineffective at low altitudes. When the Army Air Forces (AAF) evaluated it against the XF-12, testing revealed the XF-11 was harder to fly and maintain, and projected that it would to be twice as expensive to build.<ref name="Winchester p.223"/> A small production order of 98 for the Republic F-12 had been issued, but the AAF soon chose the [[Boeing B-50 Superfortress#Variants|RB-50 Superfortress]], and [[Northrop F-15 Reporter]] instead, both of which had similar long-range photo-reconnaissance capability and were available at a much lower cost.

The F-12 production order was cancelled. When the United States Air Force was created as a separate service in September 1947, the XF-11 was redesignated the XR-11. The surviving XR-11 prototype arrived at [[Eglin Field]], Florida, in December 1948 from [[Wright Field]], Ohio, to undergo operational suitability testing<ref>Fort Walton, Florida, "New Ship At Eglin", ''Playground News'', 30 December 1948, Vol. 3, No. 48, p. 1.</ref> through July 1949<ref>Francillon, René J., "McDonnell Douglas Aircraft Since 1920, Vol. II", Naval Institute Press, Annapolis, Maryland, 1979, 1990, Library of Congress card number 88-61447, ISBN 1-55750-550-0, p. 76.</ref> but a production contract for 98 was cancelled. The airframe was transferred to [[Sheppard AFB]], Texas, on 26 July 1949 for use as a ground maintenance trainer by the 3750th Technical Training Wing, and was dropped from the USAF inventory in November 1949.<ref>Francillon, René J., "McDonnell Douglas Aircraft Since 1920, Vol. II", Naval Institute Press, Annapolis, Maryland, 1979, 1990, Library of Congress card number 88-61447, ISBN 1-55750-550-0, p. 77.</ref>

==Specifications (XF-11)==
[[File:XF-11 banking away.jpg|thumb|right|The second XF-11]]
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=
|crew=two, pilot and navigator/photographer
|capacity=
|payload main=
|payload alt=
|length main= 65 ft 5 in
|length alt= 19.94 m
|span main= 101 ft 4 in
|span alt= 30.89 m
|height main= 23 ft 2 in
|height alt= 7.06 m
|area main=  983 ft²
|area alt= 91.3 m²
|airfoil=
|empty weight main= 37,100 lb
|empty weight alt= 16,800 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main= 58,300 lb
|max takeoff weight alt= 26,400 kg
|more general=
|engine (prop)=  [[Pratt & Whitney R-4360]]-31 radial
|type of prop=
|number of props=2
|power main= 3,000 hp
|power alt= 2,240 kW
|power original=
|max speed main= 450 mph
|max speed alt= 720 km/h
|cruise speed main= 
|cruise speed alt=
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main=  5,000 miles
|range alt=8,000 km
|ceiling main=  44,000 ft
|ceiling alt= 13,415 m
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=
|armament=
|avionics=
}}

==See also==
{{aircontent
|related=
* [[Hughes D-2]]
|similar aircraft=
* [[Northrop F-15 Reporter]]
* [[Lockheed P-38 Lightning]] (F-5)
* [[Republic XF-12 Rainbow]]
|lists=
* [[List of military aircraft of the United States]]
|see also=
}}

==References==

===Notes===
{{Reflist}}

===Bibliography===
{{Refbegin}}
* Barton, Charles. "Howard Hughes and the 10,000 ft. Split-S." ''Air Classics'', Vol. 18, no. 8, August 1982.
* Hansen, Chris. ''Enfant Terrible: The Times and Schemes of General Elliott Roosevelt.'' Tucson, Arizona: Able Baker Press, 2012.  ISBN 978-0-61566-892-5.
* Winchester, Jim. "Hughes XF-11." ''Concept Aircraft: Prototypes, X-Planes and Experimental Aircraft''. Kent, UK: Grange Books plc., 2005. ISBN 978-1-84013-809-2.
{{Refend}}

==External links==
{{commons category|Hughes XF-11}}
* [http://www.check-six.com/Crash_Sites/XF-11_crash_site.htm Check-Six.com - The Crash of the XF-11 - Numerous details and photos of the crash]
* [http://digital.library.unlv.edu/hughes/xf11.php UNLV Library Archive - Hughes' account of the crash]

{{Hughes aircraft}}
{{USAAF reconnaissance aircraft}}

[[Category:Hughes aircraft|F-11]]
[[Category:United States military reconnaissance aircraft 1940–1949|Hughes F-11]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Twin-boom aircraft]]
[[Category:High-wing aircraft]]
[[Category:Cancelled military aircraft projects of the United States]]
[[Category:Aircraft with contra-rotating propellers]]
[[Category:Howard Hughes]]
[[Category:Articles containing video clips]]