{{EngvarB|date=April 2015}}
{{Use dmy dates|date=April 2015}}
'''Alfred Scott Broad''' (1854 – 27 April 1929) was an Australian artist, regarded as the first black-and-white artist born in South Australia to be published. He was known as "Alf", and was often referred to as "A. Scott Broad" as though his surname was "Scott-Broad", and was often written that way. An adult daughter was the subject of an unsolved mystery disappearance.

==History==
Alf was born in Adelaide a son of James Broad (ca.1830 – 14 June 1895) coachbuilder then music warehouseman and organ builder, who arrived in South Australia on the ''Osceola'' on 4 April 1851.<ref>{{cite news |url=http://nla.gov.au/nla.news-article54456998 |title=The late Mr. J. Broad |newspaper=[[South Australian Register]] |location=Adelaide |date=15 June 1895 |accessdate=23 January 2015 |page=5 |publisher=National Library of Australia}}</ref>

He studied at the South Australian School of Art and contributed drawings to ''[[Adelaide Punch]]'' from 1868, and ''Lantern'' (later ''[[Quiz and the Lantern]]'') from 1874 to 1890.
He set up a studio in his father's organ-building workshop on the corner of Gilles and Hanson Streets.<ref>{{cite news |url=http://nla.gov.au/nla.news-article36320208 |title=The Artists and Studios of Adelaide |newspaper=[[The South Australian Advertiser]] |location=Adelaide |date=1 January 1886 |accessdate=23 January 2015 |page=6 |publisher=National Library of Australia}}</ref>
He was principal illustrator for ''Portonian'' from 1871 to 1879. He moved to Melbourne, where he contributed to ''Australasian Sketcher'', Frearson's ''Illustrated Australian News''<ref>{{cite news |url=http://nla.gov.au/nla.news-article91292003 |title=Death of the Poet Longfellow |newspaper=[[South Australian Weekly Chronicle]] |location=Adelaide |date=1 April 1882 |accessdate=22 January 2015 |page=18 |publisher=National Library of Australia}}</ref> and ''[[Melbourne Punch]]''. He was appointed to the staff of ''Illustrated Sydney News''.<ref>McCulloch, Alan ''Encyclopedia of Australian Art'' Hutchinson of London, 1968.</ref>

He returned to South Australia, and contributed to Frearson's ''Pictorial Australian'' from 1886.<ref>{{cite web|url=http://nla.gov.au/nla.pic-an8955120|title=Aboriginal Life in Australia|publisher=National Library of Australia|accessdate=22 January 2015}} A hand-colored lithograph.</ref><ref>{{cite web|url=http://www.samemory.sa.gov.au/site/page.cfm?c=3753|title=SA Memory|publisher=State Library of SA|accessdate=22 January 2015}}</ref> He was in 1892 a founding member of the [[Adelaide Easel Club]].

The Art Gallery of South Australia has a watercolor by Alf Scott Broad: ''The First Stone House erected in South Australia'', depicting George Bates's house on Kangaroo Island, painted in 1887.<ref>{{cite news |url=http://nla.gov.au/nla.news-article46792278 |title=The Jubilee Exhibition |newspaper=[[South Australian Register]] |location=Adelaide |date=18 August 1887 |accessdate=22 January 2015 |page=6 |publisher=National Library of Australia}}</ref> and a print ''Glenelg, Holdfast Bay, S.A. 1837'' from ca.1880.<ref>{{cite web|url=http://www.artgallery.sa.gov.au/agsa/home/Collection/detail.jsp?ecatKey=992|title=Glenelg, Holdfast Bay, S.A. 1837|publisher=Art Gallery of South Australia|accessdate=22 January 2015}}</ref>

He ran an import business.<ref>{{cite news |url=http://nla.gov.au/nla.news-article53466801 |title=Late Mr. A. Scott Broad. |newspaper=[[The Register News-Pictorial]] |location=Adelaide |date=29 April 1929 |accessdate=23 January 2015 |page=14 |publisher=National Library of Australia}}</ref>

==Family==
James Broad ( – 1895) was married to Ann Matilda (ca.1825 – 14 August 1905): they had residence "Trevethan House" on Hanson Street, Adelaide from 1865 or earlier. Their sons were:
*John James Broad (ca.1853 – 1 March 1933), organ builder, married Elizabeth Rogers (1854 – 31 December 1928) on 25 March 1875.
:*Edie Muriel Broad (ca.1888 – 7 October 1948) married  [[A. E. Cawthorne|Augustus Eckersley "Gus" Cawthorne]] (ca.1887 – 15 July 1937), musician and noted businessman; they lived at Ningana Avenue [[Kings Park, South Australia|Kings Park]]<ref>{{cite news |url=http://nla.gov.au/nla.news-article30759813 |title=Obituary |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=16 July 1937 |accessdate=23 January 2015 |page=18 |publisher=National Library of Australia}} His wife given in BMD notices as "Ellie" and "Effie".</ref>
:*eldest daughter Eveleen Pearl ( – ) married Colin Dawson McFarlane on 5 December 1911
:*second son George Percival Broad ( – 9 April 1891)
*second son '''Alfred Scott Broad''' (1854 – 27 April 1929), artist and subject of this article, married Emmeline Fanny Ray ( – 27 October 1933) of Melbourne on 12 June 1884. She was an artist herself, and author of ''The Sex Problem'';<ref name=sex>{{cite news |url=http://nla.gov.au/nla.news-article105285704 |title=Our Adelaide Women of Interest |newspaper=[[Daily Herald (Adelaide)|Daily Herald]] |location=Adelaide |date=8 March 1913 |accessdate=23 January 2015 |page=5 Section: Magazine Section |publisher=National Library of Australia}}</ref> In 1912 they had a home "Helmsdale" in Glenelg;<ref name=sex/> his last address was 6 Kintore Avenue, [[Prospect, South Australia|Prospect]]. They had one adult son and at least one daughter (obituaries and a death notice<ref>{{cite news |url=http://nla.gov.au/nla.news-article47513190 |title=Deaths |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=24 January 1953 |accessdate=24 January 2015 |page=21 |publisher=National Library of Australia}}</ref> mention Elsie and Mavis, hard to verify):
:*Wilfred Ray Broad (ca.1885 – 30 December 1952) was a mining engineer and metallurgist at [[Broken Hill, New South Wales|Broken Hill]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article124923991 |title=Personal |newspaper=[[Daily Herald (Adelaide)|Daily Herald]] |location=Adelaide |date=5 March 1915 |accessdate=23 January 2015 |page=4 |publisher=National Library of Australia}}</ref> He married Marie McGrath (ca.1884 – 9 October 1930) of [[Nowra, New South Wales|Nowra]] on 27 December 1915. He married again, in 1933 or 1934, to Beatrice Annie "Trixie" Lock ( – 10 January 1935), killed in a car crash at [[Molong, New South Wales]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article146059058 |title=Obituary |newspaper=[[The Mount Barker Courier and Onkaparinga and Gumeracha Advertiser]] |location=SA |date=18 January 1935 |accessdate=23 January 2015 |page=1 |publisher=National Library of Australia}}</ref> His third wife Florence survived him.<!--
son Bertram Scott Broad (late 1887 – 3 November 1888)-->
:*Hilda <!--not Gladys Hilda; see elsewhere --> Scott Broad (ca.1893 –  ) disappeared from their Glenelg home in May 1913, and was never heard from again.<ref>{{cite news |url=http://nla.gov.au/nla.news-article131521393 |title=Hunt for Missing Friends. |newspaper=[[The News (Adelaide)|The News]] |location=Adelaide |date=15 July 1941 |accessdate=23 January 2015 |page=5 |publisher=National Library of Australia}}</ref>
*Edwin Broad (ca.1857 – March 1927) of the Adelaide Telegraph Department married Phoebe Eliza Webb (ca.1862 – 24 October 1939) of [[Mount Gambier, South Australia|Mount Gambier]] on 9 January 1883; they lived at Edith Street, Unley Park then 6 Rutland Ave. Unley Park.
:*eldest daughter Ethel Marguerite Broad married George R. Best ( – ) son of [[Robert Best (politician)|Sir Robert Best]], on 27 December 1915.
:*second daughter Gwendoline Amy married Leslie J. Cavanagh on 8 November 1913
:*son 11 May 1892
*Arthur Charles Broad (ca.1859 – 9 July 1936) of the city treasury married Esther Maud "Ettie" Carthy (ca.1862 – 9 July 1930) on 9 November 1882.
*fifth son Fred Scott Broad (23 May 1865 – 9 January 1927), photographer and noted [[Bowls|lawn bowler]], married Marie (Mary?) Tonkin ( – 24 July 1935) on 13 June 1887. They lived in Victoria then "The Boulevard", [[Hawthorn, South Australia|Hawthorn]].
:*Gladys (Gwladys?) Hilda Scott Broad ( – ) married Ernest Lisle Cocking (9 September 1886 – 1964) on 20 December 1916
*William Henry Peers Broad (10 July 1867 – ) married Helen Dove "Nellie" Low (ca.1856 – 9 February 1926) on 13 January 1892, lived at 9 Cambridge Terrace, Kingswood.
:*eldest son Leonard William Peers Broad ( – ) married Ella May Laing on 16 December 1923
:*youngest son Ivan Charles G. Broad ( – ) married Mary Lavinia Shipton on 30 June 1920. Ivan was a pioneering motorist.<ref>{{cite news |url=http://nla.gov.au/nla.news-article55831642 |title=6,400—Mile Drive into the Interior. |newspaper=[[The Mail (Adelaide)|The Mail]] |location=Adelaide |date=8 August 1936 |accessdate=24 January 2015 |page=8 |publisher=National Library of Australia}}</ref>
:*youngest daughter Ida Kathleen "Pat" ( – ) married Norman Alfred Watt on 13 March 1926
<!--
Alfred Scott Broad jun. married Nellie Dyer on 24 December 1903<ref>{{cite news |url=http://nla.gov.au/nla.news-article166409631 |title=Socialities. |newspaper=[[Quiz|Quiz (Adelaide, SA : 1900 – 1909)]] |location=Adelaide, SA |date=9 January 1903 |accessdate=23 January 2015 |page=8 |publisher=National Library of Australia}}</ref>
*daughter born 31 October 1903 – possibly Mavis (see below)
*son Edmund Scott Broad born 29 September 1915
They lived at King William Road, Unley, then Melbourne

*Mavis Lillian Broad, daughter of A. Scott Broad of Prospect, married A. Raskin of Kensington on 3 June 1933. She was a musician; featured in several Boy Scout functions in 1918.<ref>{{cite news |url=http://nla.gov.au/nla.news-article164150555 |title=Wonderful Scout Work. The Rally Exhibition |newspaper=[[Observer|Observer (Adelaide, SA : 1905 – 1931)]] |location=Adelaide, SA |date=30 March 1918 |accessdate=23 January 2015 |page=18 |publisher=National Library of Australia}}</ref>
-->

== References ==
{{Reflist}}

{{DEFAULTSORT:Broad, Alfred Scott}}
[[Category:Australian painters]]
[[Category:Australian cartoonists]]
[[Category:1854 births]]
[[Category:1929 deaths]]
[[Category:19th-century Australian painters]]
[[Category:20th-century Australian painters]]