{{Infobox journal
| title = The Journal of Clinical Pharmacology
| cover = [[File:The Journal of Clinical Pharmacology.jpg]]
| editor = Joseph S. Bertino, Jr.
| discipline = [[Pharmacology]]
| former_names = 
| abbreviation = J. Clin. Pharmacol.
| publisher = [[John Wiley & Sons]]
| country = 
| frequency = Monthly
| history = 1961-present
| openaccess = 
| license = 
| impact = 2.475
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1552-4604
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1552-4604/earlyview
| link1-name = Early View Articles
| ISSN = 
| eISSN = 1552-4604
| OCLC = 1580982
| LCCN = 
}}
'''''The Journal of Clinical Pharmacology''''' is a [[Peer review|peer-reviewed]] [[medical journal]] that covers the field of [[pharmacology]]. The [[editor-in-chief]] is Joseph S. Bertino, Jr. ([[Bertino Consulting]]). It was established in 1961 and is currently published by [[John Wiley & Sons]] in association with the [[American College of Clinical Pharmacology]].

== Abstracting and indexing ==
''The Journal of Clinical Pharmacology'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 2.475, ranking it 117 out of 254 journals in the category "Pharmacology & Pharmacy".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Pharmacology & Pharmacy|title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1552-4604}}
* [http://www.accp1.org/ American College of Clinical Pharmacology]

{{DEFAULTSORT:Journal of Clinical Pharmacology, The}}
[[Category:Wiley-Blackwell academic journals]]
[[Category:English-language journals]]
[[Category:Pharmacology journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1961]]