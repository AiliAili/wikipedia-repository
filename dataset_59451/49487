{{Infobox software
| name                   = IronRuby
| logo                   =
| screenshot             =
| caption                =
| developer              = [[Microsoft]] [[Dynamic Language Runtime]] Team
| latest release version = IronRuby 1.0
| latest release date    = {{Start date and age|2010|04|12}}
| latest preview version = IronRuby 1.1.3
| latest preview date    = {{Start date and age|2011|03|13}}
| programming_language   = [[C Sharp (programming language)|C#]]
| operating system       = [[Windows]] [[Linux]] [[Mac OS X]]
| platform               = [[.NET Framework]], [[Mono (software)|Mono]]
| status                 = [[Abandonware]]
| genre                  = Ruby programming language compiler<ref>{{cite web
| url = http://blogs.msdn.com/somasegar/archive/2007/07/23/early-look-at-ironruby.aspx
| title = Early look at IronRuby
| author = [[S. Somasegar]]
| accessdate = 2007-07-25}}</ref><ref>{{cite web
| url = http://rubyforge.org/projects/ironruby/
| title = RubyForge: IronRuby: Project Info
| accessdate = 2007-09-07}}</ref>
| license                = [[Apache License|Apache License, v2.0]]
| website                = {{URL|http://www.ironruby.net}}
}}

'''IronRuby''' is an implementation of the [[Ruby (programming language)|Ruby programming language]] targeting [[Microsoft]] [[.NET framework]]. It is implemented on top of the [[Dynamic Language Runtime]] (DLR), a library running on top of the [[Common Language Infrastructure]] that provides dynamic typing and dynamic method dispatch, among other things, for dynamic languages.

The project is currently inactive, with the last release of IronRuby (version 1.1.3) being in March 2011.

==History==

On April 30, 2007, at [[MIX (Microsoft)|MIX]] 2007, Microsoft announced IronRuby, which uses the same name as Wilco Bauwer's IronRuby project with permission.<ref>{{cite web | url = http://www.wilcob.com/Wilco/IronRuby/microsoft_ironruby.aspx | title = Microsoft's Iron Ruby | author = Wilco Bauwer | accessdate = 2007-07-24 |archiveurl = http://web.archive.org/web/20070929025914/http://www.wilcob.com/Wilco/IronRuby/microsoft_ironruby.aspx <!-- Bot retrieved archive --> |archivedate = 2007-09-29}}</ref> It was planned to be released to the public at [[O'Reilly Open Source Convention|OSCON]] 2007.<ref>{{cite web | url = http://www.iunknown.com/2007/05/microsoft_and_i.html | title = Microsoft and IronRuby | author = John Lam | accessdate = 2007-06-18}}</ref>

On July 23, 2007, as promised, John Lam and the DLR Design Team presented the pre-Alpha version of the IronRuby compiler at OSCON.  He also announced a quick timeline for further integration of IronRuby into the open source community.<ref>{{cite web | url = http://www.iunknown.com/2007/07/a-first-look-at.html | title = A First Look at IronRuby | author = John Lam | accessdate = 2007-07-23}}</ref>

On August 31, 2007, John Lam and the DLR Design Team released the code in its [[Software release life cycle#Alpha|pre-alpha]] stage on RubyForge.<ref>{{cite web
|url = http://www.iunknown.com/2007/08/ironruby-on-rub.html
|title = IronRuby on Rubyforge!
|quote = ''Today, you must check the source code out of the IronRuby Subversion repository on Rubyforge. You will need a Subversion client; we recommend TortoiseSVN. To build the sources from the command line, you must also have Ruby installed on your computer already''c
| last=Lam|first=John
|accessdate = 2007-08-31}}</ref> The source code has continued to be updated regularly by the core Microsoft team (but not for every [[Revision control|check-in]]). The team also does not accept community contributions for the core [[Dynamic Language Runtime]] library, at least for now.<ref>{{cite web
|url = http://rubyforge.org/pipermail/ironruby-core/2008-April/001507.html
|title = Regarding IronRuby... How true it sounds from this blog
|quote = ''The DLR is does not accept contributions from the community (...) Today we do not push to SVN on every successful SNAP check-in''
| last=Lam|first=John
|date = 2008-04-29
|accessdate = 2008-05-25}}</ref>

On July 24, 2008, the IronRuby team released the first binary alpha version, in line with [[O'Reilly Open Source Convention|OSCON]] 2008.<ref>{{cite web
|url = http://www.iunknown.com/2008/07/ironruby-at-oscon.html
|title = IronRuby at OSCON
| last=Lam|first=John
|quote = ''We’re shipping our first binary release. In this package, we’re taking a “batteries included” approach and shipping the Ruby standard libraries in it''
|date=2008-07-24
|accessdate = 2008-08-04}}</ref> On November 19, 2008, they released a second Alpha version.

The team actively worked to support [[Ruby on Rails|Rails]] on IronRuby.<ref>{{cite web
|url = http://en.oreilly.com/rails2008/public/schedule/detail/2056
|title = IronRuby on Rails
|accessdate = 2008-05-25}}</ref><ref>{{cite web
|url = http://rubyforge.org/pipermail/ironruby-core/2008-May/001911.html
|title = IronRuby r112 is out
| last=Lam|first=John
|date=2008-05-24
|accessdate = 2008-05-25}}</ref> Some Rails functional tests started to run, but a lot of work still needed to be done to be able to run Rails in a production environment.<ref>{{cite web
|url = http://rubyforge.org/pipermail/ironruby-core/2008-May/001909.html
|title = IronRuby / Rails Question
| last=Lam|first=John
|quote = ''I don't think we're near the end game yet :) We're barely able to run Rails functional tests now, and there's a lot more library work to be done before we can start thinking about deployment''
|date=2008-05-25
|accessdate = 2008-05-25}}</ref>

On May 21, 2009, they released 0.5 version in conjunction with RailsConf 2009. With this version, IronRuby could run some [[Ruby on Rails|Rails]] applications, but still not on a production environment.<ref>{{cite web
|url = http://blog.jimmy.schementi.com/2009/05/ironruby-at-railsconf-2009.html
|title = IronRuby at RailsConf 2009
| last=Schementi|first=Jimmy
|quote = ''IronRuby running Rails is not new, but doing it well or completely – is. IronRuby can now run real Rails applications, rather than just toy-hello-world examples. This does not mean IronRuby on Rails is ready for production, but it’s a great measure of forward progress''
|date=2008-05-25
|accessdate = 2008-05-25}}</ref>

Version 0.9 was announced as OSCON 2009.<ref>{{cite web
|url = http://en.oreilly.com/oscon2009/public/schedule/detail/7965
|title = IronRuby 0.9
|date = 2009-07-23
|accessdate = 2009-08-03}}</ref> This version improved performance.<ref>{{cite web
|url = http://antoniocangiano.com/2009/08/03/performance-of-ironruby-ruby-on-windows/
|title = Comparing the performance of IronRuby, Ruby 1.8 and Ruby 1.9 on Windows
| last=Cangiano|first=Antonio
|date = 2009-08-03
|accessdate = 2009-08-03}}</ref> Version 1.0 RC1 became available on 20 November 2009.<ref>{{cite web
|url = http://ironruby.codeplex.com/Release/ProjectReleases.aspx?ReleaseId=35312
|title = IronRuby 1.0RC1
|date = 2009-11-20
|accessdate = 2009-12-29}}</ref>

Version 1.0 became available on 12 April 2010, in two different versions:
* The preferred one, which runs on top of .NET 4.0.
* A version with more limited features, which ran on top of .NET 2.0. This version was the only one compatible with [[Mono (software)|Mono]]<ref>{{cite web
|url = http://ironruby.codeplex.com/releases/view/25901
|title = IronRuby 1.0 release notes
|date = 2010-04-12
| quote=''IronRuby now comes in two flavors - one that runs on top of .NET 4.0, and one that runs on any earlier framework starting with .NET 2.0 SP1. The .NET 4.0 flavor features faster startup time, compatibility with C#’s dynamic keyword, and access to the new features in .NET 4.0. So, the .NET 4.0 flavor is the preferred download now, as the Microsoft .NET Framework 4.0 is publicly available as of today. For Mono compatibility, use the zip file release for 2.0 SP1.''
|accessdate = 2010-04-17}}</ref>

The IronRuby team planned to support Ruby 1.8.6 only for 1.0 point releases, and 1.9 version only for upcoming 1.x releases, skipping support for Ruby 1.8.7.<ref>{{cite web
|url = http://rubyforge.org/pipermail/ironruby-core/2010-February/006301.html
|title = MRI 1.8.7 compatibility
|date = 2010-02-12
|accessdate = 2010-03-06}}</ref><ref>{{cite web
|url = http://rubyforge.org/pipermail/ironruby-core/2010-February/006323.html
|title = MRI 1.8.7 compatibility
|date = 2010-02-14
|quote=''IronRuby 1.0.x releases: ONLY ruby-1.8.6 compatible; IronRuby 1.x releases: ONLY ruby-1.9 compatible''
|accessdate = 2010-03-06}}</ref>

In July 2010, Microsoft let go [[Jimmy Schementi]], one of two remaining members of the IronRuby core team and stopped funding the project.<ref>{{cite web
| url=http://www.infoworld.com/%5Bprimary-term-alias-prefix%5D/%5Bprimary-term%5D/its-not-you-its-me-microsoft-kills-ironruby-551
| title=It's not you, it's me: Microsoft kills IronRuby
| publisher=[[InfoWorld]]
| date = 2010-08-11
| accessdate=2012-12-28}}</ref><ref>{{cite web
| url=http://blog.jimmy.schementi.com/2010/08/start-spreading-news-future-of-jimmy.html
| title="Start spreading the news": the future of Jimmy and IronRuby
| last=Schementi|first=Jimmy
| date = 2010-08-06
| quote=''Overall, I see a serious lack of commitment to IronRuby, and dynamic language on .NET in general. At the time of my leaving Tomas and myself were the only Microsoft employees working on IronRuby''
| accessdate=2012-12-28}}</ref> In October 2010 Microsoft announced the Iron projects (IronRuby and [[IronPython]]) were being changed to "external" projects and enabling "community members to make contributions without Microsoft's involvement or sponsorship by a Microsoft employee".<ref>{{cite web|last1=Zander|first1=Jason|title=New Components and Contributors for IronPython and IronRuby|url=http://blogs.msdn.com/b/jasonz/archive/2010/10/21/new-components-and-contributors-for-ironpython-and-ironruby.aspx|publisher=Microsoft|accessdate=27 December 2014}}</ref>

The last published release of IronRuby was on 13 March 2011 as version 1.1.3.<ref name="defunct">{{cite web | url=http://ironruby.codeplex.com/releases/view/60511 | title=IronRuby 1.1.3 | publisher=ironruby.codeplex.com | date = 2011-03-13 | accessdate=2013-05-19}}</ref>

==Architecture==

===Mono support===
IronRuby may run as well on [[Mono (software)|Mono]] as it does on Microsoft [[Common Language Runtime]] (CLR),<ref>{{cite web
| url=http://tirania.org/blog/archive/2009/Jul-27.html
| title=Improving Mono's compatibility with .NET CLR
| author=[[Miguel de Icaza]]
| quote=''For as long as we remember, most new versions of IronPython, IronRuby or the Dynamic Language Runtime exposed new missing functionality in Mono''
| date=2009-07-27
| accessdate=2009-08-03}}</ref> but as the IronRuby team only tests it with the CLR on [[Microsoft Windows|Windows]].,<ref>{{cite web
| url=http://rubyforge.org/pipermail/ironruby-core/2008-August/002553.html
| title=IronRuby and Mono
| last=Sanghyeon|first=Seo
| date=2008-08-06
| accessdate=2008-09-13}}</ref> it may not build on Mono depending on the build.<ref name="Vander Schelden">{{cite web
| url=http://rubyforge.org/pipermail/ironruby-core/2008-September/002787.html
| title=IronRuby and Mono
| last=Vander Schelden|first=Wim
| date=2008-09-04
| accessdate=2008-09-13}}</ref><ref>{{cite web
| url=http://rubyforge.org/pipermail/ironruby-core/2009-January/003654.html
| title=DLR Daily Builds (including IronRuby)
| last=Hall|first=Ben
| date=2009-01-23
| accessdate=2009-01-23}}</ref><ref>{{cite web
| url=http://rubyforge.org/pipermail/ironruby-core/2009-May/004638.html
| title=mono builds
| last=Porto Carrero|first=Ivan
| date=2009-05-26
| accessdate=2009-06-05}}</ref>

===.NET interoperability===
The interoperability between IronRuby classes and regular [[.NET Framework]] classes is very limited because many Ruby classes are not .NET classes.<ref>{{cite web
| url=http://rubyforge.org/pipermail/ironruby-core/2008-December/003390.html
| title=Xna+IronRuby+RubyNewb=headache
| last=Hagenlocher|first=Curt
| date=2008-12-16
| accessdate=2008-12-20}}</ref> However, better support for dynamic languages in [[.NET Framework#.NET Framework 4.0|.NET 4.0]] may increase interoperability in the future.<ref>{{cite web
| url=http://rubyforge.org/pipermail/ironruby-core/2008-December/003378.html
| title=WPF databinding with ruby objects
| last=Brotherus|first=Robert
| date=2008-12-12
| accessdate=2008-12-13}}</ref>

=== Silverlight support ===
IronRuby is supported on [[Silverlight]]. It can be used as a scripting engine in the browser just like the [[JavaScript]] engine.<ref>[http://ironruby.net/browser/index.html IronRuby in the browser - IronRuby.net]</ref> IronRuby scripts are passed like simple client-side JavaScript-scripts in <code><script></code>-tags. It is then also possible to modify embedded [[XAML]] markup.

The technology behind this is called Gestalt.

<source lang="html4strict">
//DLR initiation script.
<script src="http://gestalt.ironruby.net/dlr-latest.js" type="text/javascript">

//Client-side script passed to IronRuby and Silverlight.
<script type="text/ruby">
    window.Alert("Hello from Ruby")
</script>
</source>

The same works for [[IronPython]].

===Testing infrastructure===
IronRuby is integrating [[RubySpec]], which is a project to write a complete, executable specification for the Ruby programming language. The IronRuby Git repo includes a copy of the RubySpec tests, including the MSpec test framework.<ref>{{cite web
| url=https://github.com/ironruby/ironruby/wiki/RubySpec
| title=RubySpec
| quote=''The IronRuby GIT repo includes a copy of the RubySpec tests, including the MSpec test framework, under External.LCA_RESTRICTED\Languages\IronRuby\mspec. This makes it easy to modify existing tests or write new tests, and fix the bugs in the IronRuby sources, all in a single commit to the IronRuby repo.''| accessdate=2010-10-23}}</ref>

==License==
IronRuby was previously released under the [[Shared source#Microsoft Public License (Ms-PL)|Microsoft Public License]], which is [[Open Source Initiative|OSI]]-certified [[BSD licenses|BSD]]-style license.

On 16 July 2010, Microsoft re-licensed IronRuby along with the DLR under the Apache License, v2.0<ref>{{cite web
| url=http://ironruby.codeplex.com/license
| title=IronRuby License
| date=2010-07-16
| accessdate=2010-07-27}}</ref>

==Further reading==
* Shay Friedman, "IronRuby Unleashed", Sam's, 2010, ISBN 0-672-33078-4
* Ivan Porto Carrero and Adam Burmister, "IronRuby in Action", Manning, 2010, ISBN 1-933988-61-4

== See also ==
{{Portal|Free software|Computer programming}}
* [[IronPython]]
* [[IronScheme]]
* [[JRuby]]
* [[Ruby on Rails]], an open source web application framework for [[Ruby (programming language)|Ruby]]
{{Clear}}

==References==
{{Reflist|2}}

==External links==
*[http://www.ironruby.net IronRuby home page]
*[http://ironruby.codeplex.com/ IronRuby] on [[Codeplex]]
*[https://github.com/IronLanguages/main/ IronRuby source code]
*[http://blogs.msdn.com/somasegar/archive/2007/04/30/mix-07-silverlight-shines-brighter.aspx S. Somasegar's blog entry announcing IronRuby]
*[http://rubyconf2007.confreaks.com/d2t1p1_state_of_ironruby.html State of IronRuby] by John Lam at RubyConf 2007
*[http://channel9.msdn.com/pdc2008/TL44/ IronRuby: The Right Language for the Right Job] by John Lam at PDC2008

{{Common Language Infrastructure}}
{{Ruby programming language}}

[[Category:Ruby (programming language)]]
[[Category:.NET programming languages]]
[[Category:Beta software]]
[[Category:Software using the Apache license]]