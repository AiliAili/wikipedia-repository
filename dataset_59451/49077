{{Orphan|date=September 2011}}
{{infobox person
|name = Earl Schuyler (Sky) Kleinhans
|image = Earl Schuyler Kleinhans.jpg
|image_width = 150px
|birth_date = {{birth date|1905|02|03}}
|birth_place = [[Pittsburgh]]
|death_date = {{death date and age|1996|09|21|1905|02|03}}
|death_place = [[Irvine, California]]
|field = [[Aerospace Engineering]]
|occupation = [[Aeronautical engineer]]
|work_institutions = [[Sikorsky Aircraft|Sikorsky]], <br />[[Douglas Aircraft]]
|alma_mater = [[Massachusetts Institute of Technology]]
|known_for = [[Flying boat]] and [[Douglas Aircraft]] <br />airplane designs
|Awards = [[AIAA]] Fellow
}}

'''Earl Schuyler (Sky) Kleinhans''' (February 3, 1905 – September 21, 1996) was an airplane and [[flying boat]] aeronautical engineering pioneer with primary experience at [[Sikorsky Aircraft|Sikorsky]] and [[Douglas Aircraft]] where he advanced over a 36-year career to become [[chief engineer]] and retired as the Chairman of the Scientific Advisory Board in 1969 for [[McDonnell Douglas]].

== Early life ==
Some of the engineering talent of Sky Kleinhans was inherited. He was born in Pittsburg, Pennsylvania the son of a designer of large rolling mill machinery for steel mills. One day when Sky was about 3½ years old he was traveling with his father (Frank Brasil Kleinhans) when their buggy was sandwiched between two streetcars his father was killed and young Sky was ejected from the accident.<ref>{{cite web|url=http://www.frontlineaerospace.com/content/legacy |title=Legacy|author=Frontline Aerospace}}</ref>

Sky started at the Pittsburgh Academy in the fifth grade, then in 1916 his family moved to California where he graduated from [[Hollywood High School|Hollywood High]] in 1922.  Then he continued his education at the University of Southern California Branch (now [[UCLA]]) then transferred to [[UC Berkeley]] where he majored in Chemistry. He joined the [[Alpha Kappa Lambda]] fraternity there. In 1924 he attended [[Massachusetts Institute of Technology]], switching to math, physics and anything to do with [[aircraft]] or [[boat]]s, where he earned a BS in Mathematics in 1927.<ref>{{cite web|url=http://www.frontlineaerospace.com/content/legacy |title=Legacy|author=Frontline Aerospace}}</ref>

== Aviation career ==
After graduation in the summer 1927, he started work at [[Keystone Aircraft]] in Bristol Pennsylvania to develop [[Felixstowe F5L]] flying boats, an aircraft designed by [[John Cyril Porte]] at the [[Seaplane Experimental Station]] in England.

Then there was a series of aircraft design jobs with fledging airplane firms including [[Metal Aircraft Corporation Flamingo]] of Cincinnati Ohio, where Sky was Assistant Chief Engineer during the summer 1929 – December 1929. He worked on the Flamingo made famous as the airplane that flew [[Jimmie Angel]] to discover with worlds highest waterfall [[Angel Falls]].  Sometimes in those early years an aircraft can be designed, built and test flown in four to five months.<ref>{{cite web|url=http://www.frontlineaerospace.com/content/legacy/Kleinhans.pdf|title=Kleinhans|author=Frontline Aerospace}}</ref>

At Sikorsky Aviation Corporation, Stratford Connecticut, Sky was in charge of the XP3S1 aircraft development, the [[Sikorsky S-38]] and the Navy's XSS-1 and XSS-2, an experimental scout-observation flying-boats.<ref>{{cite web|url=http://www.frontlineaerospace.com/content/legacy/Kleinhans.pdf|title=Kleinhans|author=Frontline Aerospace}}</ref> Often he helped with the design of the [[Sikorsky S-40]], [[Sikorsky S-41]] and [[Sikorsky S-42]].

Sky started work in the summer of 1933 at the [[Douglas Aircraft]] Company hired by [[James H. Kindelberger]] (Dutch) and he was put in charge of all flying boats as a project engineer.  In 1935 he was appointed Assistant Chief Designer for all of Douglas Aircraft, including the B-19 and DC-4. From 1937-1940 he was promoted to Chief Designer, and then the WWII started in 1941 he became Assistant Chief Engineer, Edward F. Burton (designer of the [[Douglas DC-3]]) was now chief engineer, but by 1960 Sky became Chief Engineer then Director of research and engineering. His last position was Chairman of the Scientific Advisory board in until his retirement in 1967.

== Aircraft Designs ==
After his seaplane and flying boat work at Douglas he became very involved in transport aircraft the entire DC-3 through DC-9 series. His airplane design philosophy was driven by a combination of function and airline requirements embodied in five key points.<ref>Flight Global, January 1957 pages 80-82</ref>
*First—Capacity up by 50%
*Second—Operating cost reduced by 20%, this improves airline revenue
*Third—Performance 20% improvement in speed and range
*Fourth—Comfort, interiors had to look and feel different
*Safety—Bad-weather aids, autopilots, reverse thrusters, brakes
During Douglas’s long transport history they had not marketing a new aeroplane that was not an improvement in the type of aircraft it was replacing in each of these five cardinal points.

During his career Sky Kleinhans designed, assisted with the design, or managed a design team for the following aircraft.

*[[Naval Aircraft Factory PN|Keystone PK-1]], derived from the [[Felixstowe F5L]]
*[[Sikorsky S-38]]
*Sikorsky XSS-1 and XSS-2, an experimental scout-observation flying-boats with folding wings<ref>{{cite web|url=https://www.flickr.com/photos/sdasmarchives/4841312519/|title= Sikorsky XSS1 wings fold|author= San Diego Air & Space Museum Archive}}</ref>
*[[Sikorsky S-40]]
*[[Sikorsky S-41]]
*[[Sikorsky S-42]]
*[[Douglas XB-19]]
*[[Douglas DC-4E]]
*[[Douglas DC-4]]
*[[Douglas DC-6]]
*[[Douglas DC-7]]
*[[Douglas XB-42 Mixmaster]]
*[[Douglas XB-43 Jetmaster]]
*[[Douglas DC-8]]
*[[McDonnell Douglas DC-9]]
*[[Douglas X-3 Stiletto]]<ref>http://www.scribd.com/doc/45985597/Adventures-in-Research-a-History-of-Ames-Research-Center-1940-1965</ref>

== Family ==
[[File:Earl Schuyler Kleinhans and Family 1971.jpg]]

Sky married Virginia Winterstein in Bristol Pennsylvania in 1928 and they had two daughters: Charlotte Kleinhans Wood, and Letitia Kleinhans Aaron with four grandchildren, Ryan Schuyler Wood, of Broomfield Colorado; Denise Ann Buysse, of Oceanside California; Kathy Aaron Tate of Woodland Hills California, and Quentin Kennedy Aaron, of Memphis, Tennessee.

== Honors and Legacy ==
* AIAA Fellow

== References ==
{{reflist}}

== External links ==
* [http://www.frontlineaerospace.com/content/legacy Brief Biography]
* [http://www.google.pl/patents/about/2552181_KLEINHANS.html?id=amZhAAAAEBJ Patent: Jet Engine Sound Suppressor and Reverser]
* [http://www.freepatentsonline.com/2629568.html Patent: Tandem Rotor Helicopter]
* [http://www.freepatentsonline.com/3191094.html Patent: Static Electricity Discharger]
* [http://www.freepatentsonline.com/2351215.html Patent: Retractable Landing Gear]
* [http://www.freepatentsonline.com/2552181.html Patent: Ejection Seat]
* [http://www.freepatentsonline.com/2300722.html Patent: Hydraulic pressure fluid accumulator]
* [http://www.google.com/patents?id=ZVdtAAAAEBAJ&printsec=frontcover&dq=2696956&hl=en&ei=Ar_3TeapF5D6swOc2JWxBw&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCkQ6AEwAA Patent: Safety Mechanism For Operating Control Surfaces]

{{DEFAULTSORT:Kleinhans, Earl Schuyler}}
[[Category:1905 births]]
[[Category:1996 deaths]]
[[Category:People from Pittsburgh]]
[[Category:American aerospace engineers]]
[[Category:Aircraft designers]]
[[Category:Seaplanes and flying boats]]
[[Category:Aerodynamicists]]
[[Category:Fellows of the American Institute of Aeronautics and Astronautics]]