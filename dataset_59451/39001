{{Infobox Simpsons episode
|episode_name=Burns' Heir
|episode_no=99
|image=
|prod_code=1F16
|airdate=April 14, 1994
|show runner=[[David Mirkin]]
|writer=[[Jace Richdale]]
|director=[[Mark Kirkland]]
|blackboard="The Pledge of Allegiance does not end with "[[Hail Satan]]""<ref name="Book">{{cite book |last=Groening |first=Matt |authorlink=Matt Groening |editor1-first=Ray |editor1-last=Richmond |editor1-link=Ray Richmond |editor2-first=Antonia |editor2-last=Coffman |title=[[The Simpsons episode guides#The Simpsons: A Complete Guide to Our Favorite Family|The Simpsons: A Complete Guide to Our Favorite Family]]  |edition=1st |year=1997 |location=New York |publisher=[[HarperPerennial]] |lccn=98141857 |ol=433519M |oclc=37796735 |isbn=978-0-06-095252-5 |page=140 |ref={{harvid|Richmond & Coffman|1997}}}}.</ref>
|couch_gag=The Simpsons are balls that bounce onto the couch. [[Bart Simpson|Bart]] almost bounces away, but [[Homer Simpson|Homer]] reins him in and hurls him in place.<ref name="BBC">{{cite web |url=http://www.bbc.co.uk/cult/simpsons/episodeguide/season5/page18.shtml |title=Burns' Heir |accessdate=2008-04-12 |author1=Martyn, Warren |author2=Wood, Adrian |year=2000 |publisher=BBC}}</ref>
|guest_star=[[Phil Hartman]] as [[Lionel Hutz]]
|commentary=[[Matt Groening]]<br/>[[David Mirkin]]<br/>[[Jace Richdale]]<br/>[[Mark Kirkland]]<br/>[[David Silverman (animator)|David Silverman]]
|season=5
}}
"'''Burns' Heir'''" is the eighteenth episode of ''[[The Simpsons]]''<nowiki>'</nowiki> [[The Simpsons (season 5)|fifth season]]. It originally aired on the [[Fox Broadcasting Company|Fox network]] in the United States on April 14, 1994. In the episode, [[Montgomery Burns|Mr. Burns]] has a near-death experience which prompts him to find an heir to inherit his wealth after he dies. Although [[Bart Simpson|Bart]] is initially rejected, Burns soon decides to choose him after seeing him as "a creature of pure malevolence". Marge convinces Bart to go spend some time with Burns, and soon becomes more disruptive than normal to his own family and decides to go live with Mr. Burns.

"Burns' Heir" was written by [[Jace Richdale]], his only writing credit. [[David Silverman (animator)|David Silverman]] was originally going to direct the episode, but he was so swamped with his work as supervising director that it was reassigned to [[Mark Kirkland]]. While the Simpsons are at a movie, there is a parody of the [[THX]] sound [[Deep Note]]. The THX executives liked the parody so much that the scene was made into an actual THX movie trailer, with the scene being redone for the [[widescreen]] aspect ratio. A deleted scene from the episode sees Mr. Burns release a "Robotic [[Richard Simmons]]" as a way of getting rid of [[Homer Simpson|Homer]]. The scene was cut, but later included in the [[The Simpsons (season 7)|season seven]] clip show "[[The Simpsons 138th Episode Spectacular]]".

==Plot==
[[Montgomery Burns|Mr. Burns]] almost drowns while taking a bath after [[Waylon Smithers|Smithers]] puts a sponge on his head that weighs him down. Later, after realizing that no one will carry on his legacy when he dies, Burns decides to try to find an heir that will inherit his vast fortune. He holds an audition and many of the boys in [[Springfield (The Simpsons)|Springfield]] try out, including [[Nelson Muntz]], [[Martin Prince]], and [[Milhouse Van Houten]]. [[Bart Simpson|Bart]] and [[Lisa Simpson|Lisa]] also try out and fail; Lisa [[Sexism|because she is a girl]], and Bart because he read [[Homer Simpson|Homer]]'s badly-worded proposal. Angry and humiliated after the audition ends, only made worse by Burns kicking him in the butt with a mechanical boot, Bart pays him back by vandalizing his mansion. Mr. Burns approves of Bart's malevolence and decides to accept him as his heir.

[[Homer Simpson|Homer]] and [[Marge Simpson|Marge]] sign a legal document which officially names Bart as Mr. Burns' heir. Marge suggests that Bart should spend some time with Mr. Burns. While initially repelled by Mr. Burns' coldness, Bart begins to take a liking to him after Burns promises to give Bart whatever he wants out of his life. Bart decides to abandon his family because Mr. Burns allows him to do anything he likes. Bart's family becomes angry and wants him back, so they decide to sue Mr. Burns, but due to hiring [[Lionel Hutz]] as their lawyer, the court ends up deciding that Mr Burns is Bart's biological father. The Simpsons get a Private Investigator deprogrammer to kidnap Bart, but the deprogrammer wrongly takes [[Hans Moleman]] instead and brainwashes him into thinking he's a part of the Simpson family.

Meanwhile, Bart becomes lonely and wants to go back to his family. Mr. Burns does not want him to leave and tricks him into thinking that his family hates him, using a falsified video with actors playing the Simpson family. Bart decides that Burns is his true father and the two celebrate by firing employees at the [[Springfield Nuclear Power Plant]] by ejecting them through a trap door. However, one of the employees is Homer and Mr. Burns then tries to break Bart's ties with his family by forcing him to fire Homer. Bart instead "fires" Burns and drops him through a trap door. Bart decides that he loves his family, and moves back in with them.

==Production==
[[File:RichardSimmonsSept2011.jpg|thumb|215px|[[Richard Simmons]] did not voice his own robotic counterpart, planned by crew.]]

"Burns' Heir" was the first episode in which [[Jace Richdale]] received a writers' credit, although he was a part of the show's staff for several seasons. When he was starting out as a writer on the show, Richdale was told to come up with some story ideas and he came up with the basic plot off the top of his head.<ref name="Richdale">Richdale, Jace. (2004). Commentary for "Burns' Heir", in ''The Simpsons: The Complete Fifth Season'' [DVD]. 20th Century Fox.</ref> [[David Silverman (animator)|David Silverman]] was originally going to direct the episode, but he was so swamped with his work as supervising director that it was reassigned to [[Mark Kirkland]].<ref name="Silverman">Silverman, David. (2004). Commentary for "Burns' Heir", in ''The Simpsons: The Complete Fifth Season'' [DVD]. 20th Century Fox.</ref> While the Simpsons are at a movie, there is a parody of the [[THX]] sound [[Deep Note]]. During that scene, a man's head explodes in a reference to the film ''[[Scanners]]''. The THX executives liked the parody so much that the scene was made into an actual THX movie trailer, with the scene being redone for the [[widescreen]] aspect ratio.<ref name="Mirkin">Mirkin, David. (2004). Commentary for "Burns' Heir", in ''The Simpsons: The Complete Fifth Season'' [DVD]. 20th Century Fox.</ref>

A deleted scene from the episode sees Mr. Burns release a "Robotic [[Richard Simmons]]" as a way of getting rid of Homer, which dances to a recording of [[K.C. and the Sunshine Band]]'s "[[(Shake, Shake, Shake) Shake Your Booty|Shake Your Booty]]". Simmons was originally asked to guest star, according to [[David Mirkin]], he was "dying to do the show", but declined when he found out he would voice a robot.<ref name="Mirkin"/> It was fully animated, but was cut from "Burns' Heir" because it often did not get a good reaction during table reads.<ref name="Mirkin"/> According to [[Bill Oakley]], there was a "significant division of opinion amongst the staff as to whether Richard Simmons was a target ''The Simpsons'' should make fun of" because it was "well-trod territory".<ref name="Oakley"/> They also felt it distracted viewers from the story.<ref name="Mirkin"/> To the production staff's surprise, the scene would make the audience "erupt with laughter" when screened at animation conventions and college presentations, so they decided to insert it in the [[The Simpsons (season 7)|season seven]] clip show "[[The Simpsons 138th Episode Spectacular]]".<ref name="Oakley">{{cite video |people=Oakley, Bill |date=2005 |title=The Simpsons The Complete Seventh Season DVD commentary for the episode "The Simpsons 138th Episode Spectacular" |medium=DVD |publisher=20th Century Fox}}</ref>

==Cultural references==
The episode parodies [[THX]]'s infamous '[[Deep Note]]'.

The trailer advertising Mr. Burns' search for an heir is a loose parody of the trailer for ''[[Toys (1992 film)|Toys]]'', a 1992 comedy starring [[Robin Williams]].<ref name="Mirkin"/>

Mr. Burns also sings "[[Let's All Go to the Lobby]]".<ref name="BBC"/>

Marge's fantasy about Bart graduating from [[Harvard University]] with Mr. Burns' money is interrupted by a fantasy of her being lifted into the sky by [[Lee Majors]], accompanied by a sound effect from ''[[The Six Million Dollar Man]]'', in which Majors played the title character.

Mr. Burns states that he got the idea for installing cameras all through town from ''[[Sliver (film)|Sliver]]'', which he calls a "delightful romp".<ref name="Mirkin"/>

Moe using a home-made sliding action holster with a pistol while looking in a mirror is a reference to Travis Bickle from ''[[Taxi Driver]]''.

The scene in which Homer is secretly eating flowers is a reference to a scene in ''[[The Last Emperor]]'' where the empress eats flowers.<ref name="Mirkin"/>

The young boy saying, "Today, sir? Why, it's Christmas Day!" makes a reference to [[Charles Dickens]]' ''[[A Christmas Carol]]''.<ref name="Book"/>

The room in which "Bart" (who is really Hans Moleman) is deprogrammed in is room number 101, which is in reference to the novel ''[[Nineteen Eighty-Four]]''.

In the deleted scene, the Richard Simmons robot healing itself after Smithers shoots it with a shotgun is a reference to the [[T-1000]] from [[Terminator 2: Judgment Day]].<ref name="Mirkin"/>

==Reception==
In its original broadcast, "Burns' Heir" finished 53rd in ratings for the week of April 11–17, 1994, with a [[Nielsen ratings|Nielsen rating]] of 9.4, and was viewed in 8.85 million households.<ref name="NR"/> The show dropped four places in the rankings after finishing 49th the previous week.<ref>{{Cite news |title=CBS edges ABC in weekly ratings race |work=[[South Florida Sun-Sentinel]] |author=Williams, Scott |date=1994-04-21}}</ref> It was the third highest rated show on Fox that week following ''[[Living Single]]'' and ''[[Married... with Children]]''.<ref name="NR">{{cite news |title=Nielsen Ratings /Apr. 11-17 |work=Long Beach Press-Telegram |agency=Associated Press |date=1994-04-20 }}</ref>

The authors of the book ''I Can't Believe It's a Bigger and Better Updated Unofficial Simpsons Guide'', Warren Martyn and Adrian Wood, wrote that, "the episode lacks the emotional punch of others in which members of the family are separated."<ref name="BBC"/> DVD Movie Guide's Colin Jacobson wrote that the episode was "such a great concept that it’s a surprise no [one] went for it earlier." He felt that it "occasionally veers on the edge of mushiness, but it avoids becoming too sentimental. It's a blast to see Burns’ world from Bart’s point of view.<ref>{{cite web |accessdate=2009-05-05 |url=http://www.dvdmg.com/simpsonsseasonfive.shtml |title=The Simpsons: The Complete Fifth Season (1993) |publisher=DVD Movie Guide |date=2004-12-21 |author=Jacobson, Colin }}</ref> [[DVD Talk]] gave the episode a score of 5 out of 5<ref>{{cite web |accessdate=2009-05-05 |url=http://www.dvdtalk.com/reviews/13784/simpsons-the-complete-fifth-season-the/ |title=The Simpsons&nbsp;— The Complete Fifth Season |publisher=[[DVD Talk]] |date=2004-12-21 |author=Gibron, Bill }}</ref> while DVD Verdict gave the episode a Grade B score.<ref>{{cite web|accessdate=2009-05-05 |url=http://www.dvdverdict.com/reviews/simpsonsseason5.php |title=The Simpsons: The Complete Fifth Season |publisher=DVD Verdict |date=2005-02-23 |author=Bromley, Judge Patrick |deadurl=yes |archiveurl=https://web.archive.org/web/20090116115733/http://www.dvdverdict.com/reviews/simpsonsseason5.php |archivedate=2009-01-16 |df= }}</ref> Paul Campos of ''[[Rocky Mountain News]]'' described the Robotic Richard Simmons scene as "a level of surreal comedy that approaches a kind of genius".<ref>{{Cite news |title=Simpsons' charm is in telling truth |author=Campos, Paul |work=[[Rocky Mountain News]] |date=2000-01-11}}</ref>

Homer's quote, "Kids, you tried your best and you failed miserably. The lesson is never try", was added to ''[[The Oxford Dictionary of Quotations]]'' in August 2007.<ref>{{cite web |url=http://www.telegraph.co.uk/news/uknews/1561004/Simpsons-quotes-enter-new-Oxford-dictionary.html |title=Simpsons quotes enter new Oxford dictionary |accessdate=2009-05-02 |date=2007-08-24 |last=Shorto |first=Russell |work=[[The Daily Telegraph]]}}</ref>

==References==
{{Reflist|2}}

==External links==
{{wikiquote|The_Simpsons/Season_5#Burns.27_Heir|"Burns's Heir"}}
{{portal|The Simpsons}}
*[http://www.thesimpsons.com/#/recaps/season-5_episode-18 "Burns' Heir"] at The Simpsons.com
*{{snpp capsule|1F16}}
*[http://www.tv.com/the-simpsons/burns-heir/episode/1384/summary.html "Burns' Heir"] at [[TV.com]]
*{{imdb episode|0777149|Burns' Heir}}

{{The Simpsons episodes|5}}

{{good article}}

[[Category:The Simpsons (season 5) episodes]]
[[Category:1994 American television episodes]]

[[pt:Burns' Heir]]