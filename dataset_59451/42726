<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Sigma
 | image=Sigma Classic.JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Light-sport aircraft]]
 | national origin=Russia
 | manufacturer=Elitar ([[Samara, Russia|Samara]] VVV-Avia)
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production 2010 
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=at least 12
 | program cost= <!--Total program cost-->
 | unit cost= [[Euro|€]]80,000 (2015)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Elitar Sigma''' or ''' Elitar (Samara VVV-Avia) Sigma''' is a single engine light aircraft of wing and boom layout, seating two in [[side-by-side configuration]].  It was designed and built in Russia where production continued until at least 2010.

==Design and development==

Design of the Sigma, then known as the '''Sigma-4''', began in 1991 at the Sigma design bureau though it was later associated with the Albatros bureau.  From 2003 it was marketed by Elitar, a bureau formed in 1997. Manufacture of the eighth and later aircraft has been at the [[Samara, Russia|Samara]] based VVV-Avia factory.<ref name=JAWA11/>

The Sigma is a metal framed aircraft with largely [[composite material|composite]] skin, though with metal on the [[fin]] and [[aircraft fabric covering|fabric covering]] on the [[aileron]]s, which have GRP noses.  The [[flap (aircraft)|flaps]] are also [[Fiberglass|GRP]] structures.  The wing has constant [[chord (aircraft)|chord]], 1.5° of [[dihedral (aircraft)|dihedral]] and a full span combination of flaps and ailerons. The exposed boom aft of the wing [[trailing edge]] is slender and carries the rectangular horizontal tail surfaces, large triangular fin and parallel chord [[rudder]], plus a small [[ventral]] fin.  A [[Rotax 912 UL|Rotax 912 ULS]] [[flat four]] engine is mounted above the wing position and forward of its [[leading edge]].<ref name=JAWA11/><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 84. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 87. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

The teardrop shaped pod is mounted below the wing and braced to it, with a single [[aircraft fairing|faired]] [[lift strut]] on each side from its lower part.  Access to the side-by-side seating is by large glazed doors.  There is a luggage area behind the seats and a rear transparency.  The [[tricycle undercarriage]] is short, placing the Sigma close to the ground.  All wheels are on [[cantilever]] spring legs and enclosed by fairings.  The nosewheel leg is horizontal and the wheel [[caster]]s.  The Sigma can also be configured as a [[floatplane]], the floats attached with a complex of struts.  A MVEN K-600-00 [[ballistic parachute|ballistic recovery parachute]] is standard on Russian aircraft.<ref name=JAWA11/><ref name="WDLA11"/><ref name="WDLA15"/>

==Operational history==
Twelve or more Sigmas had been built by 2010. One, with a civil registration, flies at a Russian experimental station, another with an aero club in the [[Ukraine]].  Four kits have been delivered to New Zealand, three in 2005 and one in 2009.<ref name=JAWA11/>

The type achieved [[Light-sport aircraft|LSA]] compliance in the United States under the name ''Samara VVV-Avia	Elitar-Sigma C4E''. There have been two Sigmas on the US civil register, one of them a sales demonstrator which was deregistered and returned to the manufacturers after the sales campaign was abandoned in 2006.<ref name=JAWA11/><ref name="FAASLSA">{{cite web|url = https://www.faa.gov/aircraft/gen_av/light_sport/media/SLSA_Directory.xlsx|title = SLSA Make/Model Directory|accessdate = 23 March 2017|last = [[Federal Aviation Administration]]|date = 26 September 2016}}</ref>
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 2011/12<ref name=JAWA11/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=2
|length m=6.19
|length note=
|span m=9.80
|height m=2.91
|height note=
|wing area sqm=10.90
|wing area note=gross
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=384
|empty weight note=equipped
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=540
|max takeoff weight note=normal, LSA 598 kg (1,320 lb)
|fuel capacity=65 L (17.2 US gal, 14.3 Imp gal
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912UL|Rotax 912 ULS]]
|eng1 type=air- and water-cooled [[flat four]], geared down by 2.43:1
|eng1 kw=75.3
|eng1 note=
|power original=
|more power=

|prop blade number=3
|prop name=
|prop dia m=1.72
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=200
|max speed note=
|cruise speed kmh=130
|cruise speed mph=
|cruise speed kts=
|cruise speed note=economical, normal cruise  {{convert|170|km/h|mph kn|abbr=on}}
|stall speed kmh=76
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=flaps down
|never exceed speed kmh=240
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=650
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=5 hr 30 min
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=6.0
|climb rate note=maximum, at sea level
|time to altitude=

|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=

|power/mass=
|thrust/weight=

|more performance=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{Commons category|Elitar Sigma-4}}
{{reflist|refs=

<ref name=JAWA11>{{cite book |title= Jane's All the World's Aircraft 2011-12|last= Jackson |first= Paul |coauthors= |edition= |year=2011|publisher=IHS Jane's|location= Coulsdon, Surrey|isbn=978-0-7106-2955-5|pages=461}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:Soviet and Russian sport aircraft 1990–1999]]