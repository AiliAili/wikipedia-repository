{{Use dmy dates|date=March 2013}}
The Microsoft '''.NET Compact Framework''' (.NET CF) is a version of the [[.NET Framework]] that is designed to run on resource constrained mobile/embedded devices such as [[personal digital assistant]]s (PDAs), [[mobile phone]]s, factory controllers, [[set-top box]]es, etc. The .NET Compact Framework uses some of the same class libraries as the full .NET Framework and also a few libraries designed specifically for mobile devices such as [[.NET Compact Framework controls]]. However, the libraries are not exact copies of the .NET Framework; they are scaled down to use less space.

==Development==
It is possible to develop [[application software|applications]] that use the .NET Compact Framework in [[Microsoft Visual Studio#Visual Studio .NET 2003|Visual Studio .NET 2003]], in [[Microsoft Visual Studio#Visual Studio 2005|Visual Studio 2005]] and in [[Microsoft Visual Studio#Visual Studio 2008|Visual Studio 2008]], in [[C Sharp (programming language)|C#]] or [[Visual Basic .NET]]. Applications developed with [[Basic4ppc]] are also eventually compiled for the .NET CF. The resulting applications are designed to run on a special, mobile-device, high performance [[just-in-time compilation|JIT compiler]].

The Microsoft .NET Compact Framework 3.5 Redistributable contains the common language runtime and class libraries built for the .NET Compact Framework. In addition to version 3.5 support, it also supports applications developed for version 1.0 and 2.0. The .NET Compact Framework 3.5 provides new features such as Windows Communication Foundation, LINQ, SoundPlayer, new runtime tool support, and many other features.

The UI development is based on [[Windows Forms]] which is also available on the desktop version of the .NET Framework. User interfaces can easily be created with Visual Studio by placing [[.NET Compact Framework controls]] like buttons, text boxes, etc. on the forms. Also features like data binding are available for the .NET CF. A major disadvantage of the UI development is that modern looking applications with support for finger-based touch screen interaction are not that easy to implement. This is mainly due to the desktop-oriented user interface concept on which Windows Forms is based, although some third party libraries with custom controls for this purpose are available.

==Deployment==
To be able to run applications powered by the .NET Compact Framework, the platform must support the Microsoft .NET Compact Framework runtime. Some [[operating system]]s which do include .NET CF are Windows CE 4.1, Microsoft [[Pocket PC]], Microsoft Pocket PC 2002, Smartphone 2003, and Symbian v3. .NET Compact Framework applications can be run on desktop computers with the full .NET Framework as long as they only access the shared parts of both frameworks, though their user interface cannot be upgraded to look like that of an application developed for desktop PCs.
<!--
Additional Info:
Running .NET Compact Framework applications on the full .NET framework is a supported
and well documented path. If an application developed for full .NET Framework attempts
to load a .NET Compact Framework class library, the .NET Runtime will load the
class library as if it had been compiled with the full .NET Framework.
-->

A version of the .NET Compact Framework is also available for the [[Xbox 360]] console. While it features the same runtime as the regular .NET CF, only a subset of the class library is available.<ref name = "X360">{{cite web | url = http://msdn2.microsoft.com/en-us/library/bb203912.aspx | title = .NET Compact Framework for Xbox 360 | publisher = TechNet | accessdate = 2007-06-21| archiveurl= https://web.archive.org/web/20070609214416/http://msdn2.microsoft.com/en-us/library/bb203912.aspx| archivedate= 9 June 2007<!--Added by DASHBot-->}}</ref> This version is used by [[Microsoft XNA|XNA Framework]] to run [[managed code|managed games]] on the console. There are other limitations as well, such as the number of threads being limited to 256.<ref name="Pool">{{cite web|url=http://msdn2.microsoft.com/en-us/library/bb203914.aspx |title=Thread Pools in the .NET Compact Framework for Xbox 360 |publisher=TechNet |accessdate=2007-06-21 |archiveurl=http://www.webcitation.org/66LCIaRoL?url=http%3A%2F%2Fmsdn2.microsoft.com%2Fen-us%2Flibrary%2Fbb203914.aspx |archivedate=21 March 2012 |deadurl=no |df=dmy }}</ref> Unlike other versions of .NET CF, the Xbox 360 version allows setting [[processor affinity]] to threads created.<ref>{{cite web|url=http://msdn2.microsoft.com/en-us/library/bb203911.aspx |title=Additions to the .NET Compact Framework for Xbox 360 |publisher=TechNet |accessdate=2007-06-21 |archiveurl=http://www.webcitation.org/66LCJCriW?url=http%3A%2F%2Fmsdn2.microsoft.com%2Fen-us%2Flibrary%2Fbb203911.aspx |archivedate=22 March 2012 |deadurl=no |df=dmy }}</ref> The threads are scheduled among four concurrent threads running on the multiple processor cores of the system.<ref name="Pool"/>

==Extended backward compatibility==

A port of .NET CF 1.0 SP3 that supports Windows CE versions 2.0, 2.01, and 2.11 has been developed by an [[open-source software]] developer named D. Stefanov.<ref>{{cite web | url = http://netcfwince200.codeplex.com/ | title = Run Microsoft .NET Compact Framework 1.0/SP3 on Windows CE 2.00/2.11 | publisher = CodePlex | accessdate = 2009-03-29| archiveurl= https://web.archive.org/web/20090614080230/http://netcfwince200.codeplex.com/| archivedate= 14 June 2009<!--Added by DASHBot-->}}</ref> Users of legacy Windows CE devices such as handheld PCs and palm-size PCs may now run applications written for the .NET Compact Framework 1.0 on such devices.<ref>{{cite web | url = http://www.hpcfactor.com/news/?iid=751 | title = NET CF 1.0 successfully ported to CE 2.xx; will .NET CF 2.0 be next? | publisher = HPC:Factor | accessdate = 2009-05-25| archiveurl= https://web.archive.org/web/20090531175530/http://www.hpcfactor.com/news/?iid=751| archivedate= 31 May 2009<!--Added by DASHBot-->}}</ref> Support is available for [[ARM architecture|ARM]], [[MIPS architecture|MIPS]], [[x86]], and Hitachi [[SuperH]] (SH3, SH4) processors.

==Release history==
{| class="wikitable" style="margin:auto;"
|-
!Version name!!Version number!!Release date
|-
|1.0 RTM||1.0.2268.0||2002 late<ref>{{cite web|url=http://www.ddj.com/web-development/184406670 |title=.NET Compact Framework Nears Release |accessdate=2007-12-06 |archiveurl=http://www.webcitation.org/66LCAsSXM?url=http%3A%2F%2Fwww.ddj.com%2Fweb-development%2F184406670 |archivedate=21 March 2012 |deadurl=no |df=dmy }}</ref>
|-
|1.0 SP1||1.0.3111.0||2003
|-
|1.0 SP2||1.0.3316.0||unknown
|-
|1.0 SP3||1.0.4292.0||2005 January<ref>{{cite web | url = http://blogs.msdn.com/netcfteam/archive/2005/01/10/350249.aspx | title = .NET Compact Framework releases 1.0 SP3 | accessdate = 2007-12-06| archiveurl= https://web.archive.org/web/20071209193603/http://blogs.msdn.com/netcfteam/archive/2005/01/10/350249.aspx| archivedate= 9 December 2007<!--Added by DASHBot-->}}</ref>
|-
|2.0 RTM||2.0.5238.0||2005 October<ref>{{cite web|url=http://www.windowsfordevices.com/news/NS6191688737.html |title=Microsoft releases .NET CF 2.0 redistributable |accessdate=2007-12-06 |archiveurl=http://www.webcitation.org/66LCCqBCB?url=http%3A%2F%2Fwww.windowsfordevices.com%2Fnews%2FNS6191688737.html |archivedate=21 March 2012 |deadurl=yes |df=dmy }}</ref>
|-
|2.0 SP1||2.0.6129.0||2006 June<ref>{{cite web | url = http://blogs.msdn.com/netcfteam/archive/2006/06/21/642013.aspx | title = .NET Compact Framework v2.0 SP1 is done and is being released. | accessdate = 2007-12-06| archiveurl= https://web.archive.org/web/20071217145451/http://blogs.msdn.com/netcfteam/archive/2006/06/21/642013.aspx| archivedate= 17 December 2007<!--Added by DASHBot-->}}</ref>
|-
|2.0 SP2||2.0.7045.0||2007 March<ref>{{cite web | url = http://blogs.msdn.com/netcfteam/archive/2007/03/13/net-compact-framework-2-0-sp2-released.aspx | title = .NET Compact Framework 2.0 SP2 Released | accessdate = 2007-12-06| archiveurl= https://web.archive.org/web/20071219221213/http://blogs.msdn.com/netcfteam/archive/2007/03/13/net-compact-framework-2-0-sp2-released.aspx| archivedate= 19 December 2007<!--Added by DASHBot-->}}</ref>
|-
|3.5 Beta 1||3.5.7066.0||2007 May<ref>{{cite web | url = http://www.microsoft.com/downloads/details.aspx?FamilyID=1343D537-A62F-4A6E-9727-7791BF4CC2BD&displaylang=en | title = .NET Compact Framework 3.5 Beta1 Redistributable | accessdate = 2007-12-06| archiveurl= https://web.archive.org/web/20071213060637/http://www.microsoft.com/downloads/details.aspx?FamilyID=1343d537-a62f-4a6e-9727-7791bf4cc2bd&displaylang=en| archivedate= 13 December 2007<!--Added by DASHBot-->}}</ref>
|-
|3.5 Beta 2||3.5.7121.0||unknown
|-
|3.5 RTM||3.5.7283.0||2007 November 19
|-
|3.5||3.5.7283.0||2008 January 25<ref>{{cite web | url = http://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=e3821449-3c6b-42f1-9fd9-0041345b3385 | title = .NET Compact Framework 3.5 Redistributable | accessdate = 2008-05-05| archiveurl = http://www.webcitation.org/66LCGyMR4?url=http://www.microsoft.com/downloads/details.aspx?displaylang%3Den%26FamilyID%3De3821449-3c6b-42f1-9fd9-0041345b3385 | archivedate = 2012-03-21| deadurl=no}}</ref>
|-
|3.5||3.5.9040.0||2009 February 8<ref>{{cite web | url = http://support.microsoft.com/kb/970549 | title = FIX: A System.Net.WebException occurs when you run an application to send HTTPS Web requests to a server in an embedded device | accessdate = 2013-10-31}}</ref><ref>{{cite web | url = http://blogs.msdn.com/b/andrewarnottms/archive/2007/11/19/why-net-compact-framework-fails-to-call-some-https-web-servers.aspx | title = Why .NET Compact Framework fails to call some HTTPS web servers | accessdate = 2013-10-31}}</ref>
|-
|3.5||3.5.9198.0||2009 July 20<ref>{{cite web | url = http://support.microsoft.com/kb/975281 | title = FIX: You cannot scroll through a Web page or visit a link by using a .NET Compact Framework 3.5-based application that hosts a WebBrowser control in Windows Mobile 6.1.4 or in Windows Mobile 6.5 | accessdate = 2010-01-30| archiveurl= http://support.microsoft.com/kb/975281| archivedate= 17 January 2010<!--Added by DASHBot-->}}</ref>
|-
|3.5||3.5.10181.0||2010 June 29<ref>{{cite web | url = http://support.microsoft.com/kb/2436709 | title = PROBLEM: An application may crash when you try to open the View GC Heap window in the Remote Performance Monitor on a device that is running the .NET Compact Framework 3.5 | accessdate = 2010-06-29| archiveurl= http://support.microsoft.com/kb/2436709 | archivedate= 17 November 2010 <!--Added by jakobojvind-->}}</ref> (with WinCE QFE, June 2010)
|-
|3.5||3.5.11125.0||2011 June 15<ref>{{cite web | url = http://support.microsoft.com/kb/2556155 | title = FIX: Performance issues may occur when the ClassDesc classFlags member is accessed in a function in the .NET Compact Framework 3.5 in Windows Embedded CE 6.0 R3 | accessdate = 2014-06-27}}</ref> (with WinCE QFE, May 2011)
|-
|3.7||3.7.0.0||8 June 2009 18:38
|-
|3.7||3.7.8345.0||2009
|-
|3.9||unknown||June 2013<ref>[http://news.microsoft.com/2013/06/13/microsoft-announces-general-availability-of-windows-embedded-compact-2013/ Microsoft announces general availability of Windows Embedded Compact 2013]</ref><ref>{{cite web | url = http://blogs.msdn.com/b/dotnet/archive/2012/11/16/introducing-netcf-3-9-in-windows-embedded-compact-2013-a-faster-leaner-and-multi-core-runtime.aspx | title = Introducing NETCF 3.9 in Windows Embedded Compact 2013 – a faster, leaner and multi-core runtime! | accessdate = 2012-11-30}}</ref>
|}

==See also==
* [[.NET Compact Framework controls]]
* [[.NET Framework]]
* [[.NET Micro Framework]]
* [[Windows CE]]
* [[Windows Mobile]]

==References==
{{reflist|30em}}

==External links==
* [http://msdn.microsoft.com/en-us/library/f44bbwa1.aspx .NET Compact Framework]

{{Windows Mobile}}
{{Windows Phone}}
{{Common Language Infrastructure}}

{{DEFAULTSORT:.Net Compact Framework}}
[[Category:.NET Framework implementations|Compact Framework]]
[[Category:Windows CE]]
[[Category:Windows Mobile]]

[[uk:.NET Framework#.NET Compact Framework]]