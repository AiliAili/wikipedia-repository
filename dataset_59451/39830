{{Use Australian English|date=September 2016}}
{{Use dmy dates|date=October 2015}}
{{Infobox military person
|name= Roberts Dunstan
|birth_date= 5 November 1922
|death_date= 11 October 1989
|birth_place=[[Bendigo, Victoria|Bendigo]], Victoria
|death_place=[[Melbourne]]
|image= Roberts Dunstan UK0489.JPG
|caption= Roberts Dunstan leaning against the rear turret of a Lancaster bomber
|nickname=
|allegiance=Australia
|serviceyears= 1940–1945
|rank= [[Flight Lieutenant]]
|branch= [[Second Australian Imperial Force]]<br>[[Royal Australian Air Force]]
|commands=
|unit=
|battles= [[Second World War]]
|awards= [[Distinguished Service Order]]
|laterwork= Minister of Water Supply<br/>Minister of Public Works
}}
'''Roberts Christian "Bob" Dunstan''' [[Distinguished Service Order|DSO]] (5 November 1922 – 11 October 1989) was an Australian soldier and aviator during the [[Second World War]]. He was notable, among other things, for:
* serving with the [[Royal Australian Air Force]] (RAAF) as an [[air gunner]], after [[amputation|losing a leg]] in action with the [[Australian Army]];
* being the youngest Australian recipient of the [[Distinguished Service Order]] (DSO),<ref>''Victoria Parliamentary Debates'' (Hansard), Legislative Assembly, vol. 396, pp. 1539–1540.</ref> and;
* being, at the time he was elected, the youngest ever member of the [[Legislative Assembly of Victoria]].<ref>''The Age'', 27 June 1981, p. 21.</ref>

Dunstan was born in [[Bendigo, Victoria]] on 5 November 1922 and attended [[Geelong Grammar School]] between 1934 and 1939.

==Australian Army==
On 3 June 1940, five months before his 18th birthday, Dunstan joined the [[Second Australian Imperial Force|Australian Imperial Force]].<ref name="awm" /><ref>{{cite web|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=A&veteranId=433354|title=Dunstan, Roberts Christian (Army)|accessdate=25 October 2009|work=World War II Nominal Roll|publisher=Commonwealth of Australia}}</ref>

After training with the [[Royal Australian Engineers]], he was posted as a reinforcement to the [[6th Division (Australia)|2/8th Field Company]], in [[North Africa]].<ref name="awm" />

In January 1941, during the [[Siege of Tobruk#Capture of Tobruk|Allied campaign to capture Tobruk]], Dunstan was wounded in the knee; the wound became infected and later required the amputation of Dunstan's leg.<ref name="awm" /> After recuperating in Egypt, Dunstan was returned to Australia and medically discharged.<ref name="falconer" />

==Royal Australian Air Force==
After a brief return to civilian life, during which he studied law, Dunstan volunteered for service overseas with the [[Royal Australian Air Force]]. In 1942 he trained as an air gunner at [[Port Pirie]]. At the end of his course, Dunstan embarked for the United Kingdom with the rank of Sergeant.

He was posted, as a [[rear gunner]], to [[No. 460 Squadron RAAF]], an [[Avro Lancaster]] unit at [[RAF Binbrook]], in Lincolnshire.<ref name="falconer" /> He flew his first operation, to [[Düsseldorf]], on 11 June 1943 . In October he was commissioned as a [[Pilot Officer]].<ref name="falconer" />

During a raid on [[Kassel]] on 22/23 October 1943, the plane in which he was flying was hit by two incendiary bombs dropped by another Lancaster, which was off course.  The damage caused by this accident cut off the oxygen supply to Dunstan and the other gunner, [[Flight Sergeant]] Hegarty.  As a result of the oxygen starvation that both men suffered, neither saw the approach of an enemy night-fighter, whose attack badly damaged the Lancaster, one cannon shell passing through the rear-gunner's turret.  The aircraft managed to return home and make a crash-landing at [[Bisham]], the crew escaping unhurt.<ref>{{cite web|url=http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=7768223|title=Air combat reports—Image details—Dunstan, Flying Officer|work=DocumentsOnline|publisher=[[The National Archives (United Kingdom)|The National Archives]]|date=23 October 1943|accessdate=26 October 2009|format=fee usually required to view full pdf of original combat report|page=3}}</ref>

Dunstan completed a full tour of 30 operations and returned to Australia in August 1944,<ref name="falconer" /> and was awarded the [[Distinguished Service Order]] for his efforts as a "Cool and skilful Air Gunner despite handicap of one leg".<ref>{{cite web|url=http://www.awm.gov.au/cms_images/AWM192/00311/003110861.pdf|title=Recommendation for the award of the Distinguished Service Order to Robert Christian Dunstan|accessdate=25 October 2009|format=PDF|work=Index to Recommendations for Honours and Awards: Second World War|publisher=Australian War Memorial}}</ref>

He was discharged from the Royal Australian Air Force on 2 October 1945.<ref>{{cite web|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=R&veteranId=1046261|title=Dunstan, Roberts Christian (RAAF)|accessdate=25 October 2009|work=World War II Nominal Roll|publisher=Commonwealth of Australia}}</ref>

==Politician, journalist and film critic==
Dunstan attracted the attention of the media due to the unique nature of his experiences: an army and air force veteran, who had completed a full tour of 30 missions despite being an amputee.<ref name="falconer" />

He wrote about his experiences in a book, ''The Sand and the Sky'',<ref name="awm" /> and took a job as a journalist and film critic with the ''[[Melbourne Herald]]''.

After serving as local councillor, Dunstan stood for the Victorian parliament as a [[Liberal Party of Australia|Liberal]] candidate. Between 1956 and 1982 he was the member for [[Electoral district of Mornington|Mornington]].<ref name="awm" /> Dunstan also held two ministerial posts, with responsibility for (firstly)  water supply and (secondly) public works.<ref name="awm" /> Dunstan died in Melbourne on 11 October 1989.<ref name="awm" />

==References==
{{reflist|refs=
*<ref name="awm">{{cite web | title=Who's who in Australian Military History – Flight Lieutenant Roberts Christian (Robert) Dunstan, DSO | url=http://www.awm.gov.au/people/75031.asp | work= | publisher=Australian War Memorial | year=2009 | accessdate=2009-10-24}}</ref>
*<ref name="falconer">{{cite book |last= Falconer |first= Jonathan |title=Bomber Command Handbook 1939–1945 |year=2003 |publisher=Sutton Publishing |location=Stroud, England |pages= 203–204|isbn=0-7509-3171-X}}</ref>
}}

{{DEFAULTSORT:Dunstan, Roberts}}
[[Category:1922 births]]
[[Category:1989 deaths]]
[[Category:Australian amputees]]
[[Category:Australian Army soldiers]]
[[Category:Australian film critics]]
[[Category:Australian journalists]]
[[Category:Royal Australian Air Force personnel of World War II]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Liberal Party of Australia members of the Parliament of Victoria]]
[[Category:People from Bendigo]]
[[Category:Royal Australian Air Force officers]]
[[Category:20th-century Australian politicians]]
[[Category:People educated at Geelong Grammar School]]