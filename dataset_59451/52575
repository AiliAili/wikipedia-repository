{{Infobox journal
| title = Phytochemistry
| cover = 
| editor = Norman Lewis, Richard Robins
| discipline = [[Phytochemistry]]
| abbreviation = Phytochemistry
| publisher = [[Elsevier]]
| country = 
| frequency = 18/year
| history = 1961-present
| openaccess = 
| license =
| impact = 2.547
| impact-year = 2014
| website = http://www.journals.elsevier.com/phytochemistry/
| link1 = http://www.sciencedirect.com/science/journal/00319422
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 780563468
| LCCN = 65008341
| CODEN = PYTCAS
| ISSN = 0031-9422
| eISSN = 
}}
'''''Phytochemistry''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering pure and applied plant chemistry, plant biochemistry and molecular biology. It is published by [[Elsevier]] and is an official publication for the [[Phytochemical Society of Europe]] and the [[Phytochemical Society of North America]].

== Abstracting and indexing ==
''Phytochemistry'' is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[AGRICOLA]]
* [[BIOSIS]]
* [[Cambridge Scientific Abstracts]]
* [[Chemical Abstracts]]
* [[CAB Abstracts]]
* [[Current Contents]]/Agriculture, Biology & Environmental Sciences
* Current Contents/Life Sciences
* [[EMBASE]]
* [[Elsevier BIOBASE]]
* [[MEDLINE]]
* [[Medicinal and Aromatic Plant Abstracts]]
* [[PASCAL (database)|PASCAL]]
* [[Plant Science Database]]
* [[Science Citation Index]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.547.<ref name=WoS>{{cite book |year=2015 |chapter=Phytochemistry |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
{{Official website|http://www.journals.elsevier.com/phytochemistry/}}

[[Category:Biochemistry journals]]
[[Category:Botany journals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1961]]