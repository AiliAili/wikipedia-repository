{{Italic title}}
'''''British Journal for the Philosophy of Science''''' is a [[Peer review|peer-reviewed]], [[academic journal]] of [[philosophy]], owned by the British Society for the Philosophy of Science (BSPS),<ref>[http://www.thebsps.org/ British Society for the Philosophy of Science].</ref> and published by [[Oxford University Press]]. The journal publishes work that uses philosophical methods in addressing issues raised in the natural and human sciences.

One of the leading international journals in the field,<ref name="BJPS_Statistics">British Journal for the Philosophy of Science: [http://www.oxfordjournals.org/our_journals/phisci/statistics.html Editorial Statistics].</ref> it publishes outstanding new work on a variety of traditional and 'cutting edge' topics, from issues of explanation and realism to the applicability of mathematics, from the metaphysics of science to the nature of models and simulations, as well as foundational issues in the physical, life, and social sciences. Recent topics covered in the journal include the epistemology of measurement, mathematical non-causal explanations, signalling games, the nature of biochemical kinds, and approaches to human cognitive development, among many others. The journal seeks to advance the field by publishing innovative and thought-provoking papers, discussion notes and book reviews that open up new directions or shed new light on well-known issues.<ref>[http://bjps.oxfordjournals.org/content/by/year British Journal for the Philosophy of Science: Archive of all online content].</ref>

''The British Journal for the Philosophy of Science'' receives approximately 600 submissions a year.<ref>[http://www.oxfordjournals.org/our_journals/phisci/statistics.html The British Journal for the Philosophy of Science: Editorial Statistics].</ref> It is fully compliant with the RCUK open access policy,<ref>[http://www.oxfordjournals.org/oxfordopen/funder_policies/ Oxford Open].</ref> and is a member of the Committee of Publishing Ethics (COPE).<ref>Committee on Publication Ethics (COPE): "[http://publicationethics.org/about About COPE].</ref>

The Journal also runs a blog, [http://thebjps.typepad.com/my-blog/ Auxiliary Hypotheses].

== Editorial Board ==

=== Editors-in-Chief ===
Professor Steven French (University of Leeds) and Professor Michela Massimi (University of Edinburgh)

=== Assistant Editor ===
Dr Elizabeth Hannon (LSE/University of Leeds)

=== Associate Editors ===
Guido Bacciagaluppi (Utrecht University) <br />
Lara Buchak (UC Berkeley) <br />
Ellen Clarke (University of Oxford) <br />
Roman Frigg (LSE) <br />
Alyssa Ney (University of California, Davis) <br />
Anya Plutynski (Washington University, St. Louis) <br />
Robert Rupert (University of Colorado) <br />
Paul Weirich (University of Missouri-Columbia) <br />
Daniel Weiskopf (Georgia State University)

== Sir Karl Popper Essay Prize ==
The Sir Karl Popper Essay Prize is awarded to the best paper appearing in ''The British Journal for the Philosophy of Science'' that concerns topics in the philosophy of science to which Sir Karl made a significant contribution. It is awarded on the basis of the judgement of the Editors of the Journal from papers appearing in that year’s volume of the Journal.<ref name="PopperPrize">British Society for the Philosophy of Science: [http://www.thebsps.org/society/bsps/popper_prize.html The Sir Karl Popper Essay Prize] {{webarchive |url=https://web.archive.org/web/20140407062553/http://www.thebsps.org/society/bsps/popper_prize.html |date=April 7, 2014 }}.</ref>

=== History of the prize ===
The Prize was originally established at the wish of the late Dr Laurence B. Briskman, formerly of the Department of Philosophy, [[University of Edinburgh]], who died on 8 May 2002, having endowed an essay prize fund to encourage work in any area falling under the general description of the critical rationalist philosophy of [[Karl Popper]]. Briskman was greatly influenced by Popper, who remained the dominant intellectual influence on his philosophical outlook throughout his career. While originally open for submissions, the prize is since 2011 only awarded to papers having appeared in ''The British Journal for the Philosophy of Science''.<ref name="PopperPrize" />

=== Previous winners ===
{| class="wikitable"
|-
! Year !! Author !! Title
|-
| 2015   || Matthew Slater    || 'Natural Kindness'
|-
| 2014   || Rachael L. Brown   || 'What Evolvability Really Is'
|-
| 2013   || Charles Pence and Grant Ramsey   || 'A New Foundation for the Propensity Interpretation of Fitness'
|-
| 2012   || Elliott Wagner   || 'Deterministic Chaos and the Evolution of Meaning'
|-
| 2011   || No award made   || N/A
|-
| 2010   || Daniel Greco   || 'Significance Testing in Theory and Practice'
|-
| 2009   || Sebastian Lutz   || 'Criteria of Empirical Significance: a Success Story'
|-
| 2008   || Antoni Diller   || 'On Critical and Pancritical Rationalism'
|-
| 2007   || No award made   || N/A
|-
| 2006   || Maria Kronfeldner   || 'Darwinian Hypothesis Formation Revisited'
|-
| 2005   || No award made   || N/A
|-
| 2004   || Benjamin Elliott   || 'Falsifiable Statements in Theology: Karl Popper and Christian Thought'
|}

== Impact Factor ==
The 2014 impact factor for the BJPS was 1.281, while its five-year impact factor was 1.465, making it the leading philosophy of science journal.<ref name="BJPS_Statistics" />

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://bjps.oxfordjournals.org/}}
* Print: {{ISSN|0007-0882}}
* Online: {{ISSN|1464-3537}}

[[Category:Philosophy journals]]
[[Category:Philosophy of science literature]]
[[Category:Oxford University Press academic journals]]
[[Category:Academic journals associated with learned and professional societies of the United Kingdom]]
[[Category:Publications established in 1950]]
[[Category:Quarterly journals]]