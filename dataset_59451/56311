{{refimprove|date=April 2008}}
{{Infobox publisher
|parent=[[Cold Spring Harbor Laboratory]]
|status=Active
|country=[[United States of America|USA]]
|headquarters=[[Cold Spring Harbor, New York]]
|topics=[[Biology]]
|url={{URL|http://www.cshlpress.com}}
}}
'''Cold Spring Harbor Laboratory Press''' was founded in 1933 to aid in [[Cold Spring Harbor Laboratory|Cold Spring Harbor Laboratory's]] purpose of furthering the advance and spread of scientific knowledge.

CSHL Press publishes monographs, technical manuals, handbooks, review volumes, conference proceedings, scholarly journals and videotapes. These examine important topics in molecular biology, genetics, development, virology, neurobiology, immunology and cancer biology. Manuscripts for books and for journal publication are invited from scientists worldwide.

Revenue from sales of CSHL Press publications is used solely in support of research at Cold Spring Harbor Laboratory. 

==Journals==
{{main cat|Cold Spring Harbor Laboratory Press academic journals}}
[[Scientific journal]]s published by CSHL Press:<ref name=journallist>{{cite web |url=http://www.cshlpress.com/default.tpl?&startat=1&--woSECTIONSdatarq=2&--SECTIONSword=ww&sortby_1=journal |title=CSHL Press journal list |accessdate=7 January 2010 |publisher=CSHL Press homepage}}</ref>
* ''[[CSH Perspectives]]''
* ''[[CSH Protocols]]''
* ''[[Genes & Development]]''
* ''[[Genome Research]]''
* ''[[Learning & Memory]]''
* ''[[RNA (journal)|RNA]]''

==Operation centers==
CSHL Press has two operation centers. The main office is located in [[Woodbury, Nassau County, New York|Woodbury, New York]], near Cold Spring Harbor Laboratory, where editorial, marketing & advertising, composition, and fulfillment & distribution functions are performed. An additional book fulfillment & distribution operation is handled by NBN International in [[Plymouth]], [[United Kingdom]]. Its current executive director is John R. Inglis.

==References==
{{Reflist|2}}

==External links==
* {{Official website|http://www.cshlpress.com/}}
* [http://cshmonographs.org/csh/index.php/monographs/index Cold Spring Harbor Monographs]

[[Category:Book publishing companies based in New York]]
[[Category:Publishing companies established in 1933]]
[[Category:1933 establishments in New York]]
[[Category:Academic publishing companies]]
[[Category:American companies established in 1933]]