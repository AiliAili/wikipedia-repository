<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Jora
 | image=Jora Jora SRO 9-218 Landing on Drejø 2013-07-11 cropped.jpg
 | caption= Jora Jora SRO landing on [[Drejø]], [[Denmark]]
}}{{Infobox Aircraft Type
 | type=Two seat [[ultralight]]
 | national origin=[[Czech Republic]]
 | manufacturer=Jora Spol s.r.o.
 | designer=Oldrich Olsansky
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=At least 161 by 2009
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Jora Jora''' is a [[high wing]], [[T-tail]], single-engine, two-seat [[ultralight]] designed in the [[Czech Republic]] in 1993.  More than 160 had been sold by 2009.

==Design and development==

The Jora was designed by Oldrich Olsansky who also designed the similar [[Fantasy Air Allegro]].  It is built mostly from wood and [[laminates]], covered by [[polyester]] [[aircraft fabric covering|fabric]].  The main exception is the central [[fuselage]] section, which has a riveted tube frame.<ref name=JAWA11/>

The Jora has separate wings with constant [[chord (aircraft)|chord]] and square tips.  These are each built up around a single [[spruce]] [[spar (aviation)|spar]].  [[Plywood]] covering and polyester-filled laminate ribs form a box spar forward to the [[leading edge]], with spruce ribs and polyester covering aft.  Since 2006 all composite parts have been replaced by [[carbon fibre]] castings.  Each wing has a single, [[fairing (aircraft)|faired]] [[lift strut]] to the lower fuselage. Full-span combined [[aileron]]s and [[flap (aircraft)|flaps]] ([[flaperons]]), constructed like the wing, are attached to an auxiliary spar.  Separate ailerons and flaps are an option, in which case the wing profile ([[airfoil]]) is changed from the [[laminar flow]] UA-2 to SL-1.  The wings can be detached for transport, though wing folding is an option.<ref name=JAWA11/>

Apart from its tube centre section the fuselage is wholly laminate with strengthening [[bulkhead (partition)|bulkheads]] and ribs.  The fuselage becomes slender towards the [[fin]] and has a constant chord [[tailplane]] and [[elevator (aircraft)|elevators]].  The fin has sweep on its leading edge and extends into a small keel below the fuselage.  The cockpit seats two in [[side-by-side configuration]], with dual controls including a split, central [[control column]]. It is enclosed with a single-piece windscreen and deep side transparencies. The Jora normally has a fixed, [[tricycle undercarriage]], though a [[conventional undercarriage]] is an option. The mainwheels are mounted on inverted tubular steel A-frames, hinged to the lower fuselage; the nosewheel is on a forward-leaning leg and is steerable. Some Joras have single [[cantilever]] main legs.  Rubber springs are used on all legs; the main wheels have [[hydraulic brake|hydraulic]] [[disc brakes]].<ref name=JAWA11/>

The standard engine for the Jora is a 38.2&nbsp;kW (52&nbsp;hp) [[Rotax 582]], a two-cylinder [[two-stroke]] engine, housed under a short [[cowling]].  It can accept engines of up to 75&nbsp;kW (100&nbsp;hp).<ref name=JAWA11/>

==Operational history==
By 2009 at least 161 Joras had been sold to customers in [[Australia]], the [[Czech Republic]], [[Denmark]], [[Finland]], [[France]], [[Indonesia]], [[Ireland]], the [[Netherlands]], [[Norway]] and [[South Africa]]. 
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Rotax 582) ==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 2011/12<ref name=JAWA11/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|capacity=two
|length m=6.00
|length note=
|span m=10.60
|height m=1.95
|height note=
|wing area sqm=12.50
|wing area note=gross
|aspect ratio=
|airfoil=UA-2 [[laminar flow]] 
|empty weight kg=225
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=450
|max takeoff weight note=
|fuel capacity=48 L (12.7 US gal; 10.6 Imp gal)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 582]]
|eng1 type=two cylinder two stroke, 2.58:1 reduction gear
|eng1 kw=38.8
|eng1 note=
|power original=
|more power=

|prop blade number=3
|prop name=Junkers
|prop dia m=1.60<!-- propeller aircraft -->
|prop dia note=carbon composite
<!--
        Performance
-->
|perfhide=

|max speed kmh=140
|max speed note=
|cruise speed kmh=120
|cruise speed note=
|stall speed kmh=45
|stall speed note=
|never exceed speed kmh=162
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=563
|range miles=
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=3660
|ceiling note=
|g limits=+4/-2
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=2.53
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=36.0
|wing loading note=maximum
|power/mass=86 W/kg
|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|refs=

<ref name=JAWA11>{{cite book |title= Jane's All the World's Aircraft 2011-12|last= Jackson |first= Paul |coauthors= |edition= |year=2011|publisher=IHS Jane's|location= Coulsdon, Surrey|isbn=978-0-7106-2955-5|pages=168}}</ref>

}}

<!-- ==Further reading== -->

==External links==
{{commons category|Fantasy Air Jora}}
*{{official website|http://www.jora.cz//|name=Jora website}}{{deadlink|date=April 2017}}

<!-- Navboxes go here -->

[[Category:Czech and Czechoslovakian ultralight aircraft 1990–1999]]