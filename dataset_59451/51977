{{Infobox journal
| title = Journal of Neurochemistry
| cover = [[File:JNCcover.jpg]]
| editor = [[Jörg Schulz]]
| discipline = [[Neuroscience]]
| abbreviation = J. Neurochem.
| publisher = [[Wiley-Blackwell|Wiley]]
| country =
| frequency = 24/year
| history = 1956–present
| openaccess = [[Delayed open access|Delayed]], after 1 year
  [[Open Access]] option
| license =
| impact = 4.244
| impact-year = 2013
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1471-4159
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1471-4159/issues
| link1-name = Online archive
| link2 = http://mc.manuscriptcentral.com/jneurochem
| link2-name = Online submission
| link3 = https://itunes.apple.com/us/app/journal-neurochemistry-for/id655612235
| link3-name = Journal App for iPad
| link4 = http://www.neurochemistry.org/
| link4-name = International Society for Neurochemistry (ISN) 
| JSTOR =
| OCLC = 01782775
| LCCN =
| CODEN = JONRA9
| ISSN = 0022-3042
| eISSN = 1471-4159
}}
The '''''Journal of Neurochemistry''''' is a [[peer review|peer-reviewed]] [[scientific journal]] founded and run by the [[International Society for Neurochemistry]]. The journal has been in circulation since 1956.

''Journal of Neurochemistry'' publishes full-length presentations of original findings and reviews, highlighted by animated abstracts, as well as Editorials. It covers all aspects of [[neurochemistry]] and focuses on molecular, cellular and biochemical aspects of the nervous system, the pathogenesis of neurological disorders and the development of disease specific biomarkers. It is devoted to the prompt publication of original findings of the highest scientific priority and value that provide novel mechanistic insights, represent a clear advance over previous studies and have the potential to generate exciting future research in the following categories:

* Gene Regulation & Genetics
* Signal Transduction & Synaptic Transmission
* Brain Development & Cell Biology
* Bioenergetics & Metabolism
* Neuroinflammation & Neuroimmunology
* Neuronal Plasticity & Behavior
* Molecular Basis of Disease

[http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1471-4159/homepage/special_issues.htm Special issues] were published on [[Alzheimer's disease]] (2012), [[Stroke]] (2012) and [[Friedreich's Ataxia]] (2013).

Every issue of ''Journal of Neurochemistry'' is available online from volume 1, issue 1 (May 1956). Articles older than one year from after 1997 are available for free. In addition, authors can choose the [[Open Access]] option with a reduced fee for members of the International Society for Neurochemistry. Since 2013, articles can be downloaded and managed by the ''Journal of Neurochemistry'' App for iPad. From 2014 on ''Journal of Neurochemistry'' is published online only. The journal is currently published by [[Wiley-Blackwell|Wiley]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Elsevier BIOBASE]], [[Biochemistry and Biophysics Citation Index]], [[Biological Abstracts]], [[BIOSIS Previews]], [[Chemical Abstracts]], [[Current Contents]], [[MEDLINE]], [[Neuroscience Citation Index]], [[PsycINFO]], [[Science Citation Index]], [[Scopus]] and others. A complete list is available on the [http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1471-4159/homepage/ProductInformation.html journal's website]. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 3.973.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Neurochemistry |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-02-06 |series=[[Web of Science]] |postscript=.}}</ref>

== History - Former editors in chief ==
{| class="wikitable"
|-
! Eastern Hemisphere !! Western Hemisphere
|-
| 1956–1969 [[Derek Richter|D. Richter]] || 1956–1959 H. Waelsch
|-
| 1970–1975 A.N. Davison || 1959–1968 W.M. Sperry
|-
| 1975–1979 L.L. Iversen|| 1969–1973 D.B. Tower
|-
| 1980–1985 H.S. Bachelard|| 1974–1977 L. Sokoloff
|-
| 1985–1991 K.F. Tipton|| 1978–1981 K. Suzuki
|-
| 1991–1999 G.G. Lunt|| 1982–1985 W.T. Norton
|-
| 1999–2010 A.J. Turner|| 1986–1989 M.B. Lees
|-
| since 2011 J.B. Schulz|| 1990–1995 A.A. Boulton
|-
| || 1996–2006 B. Collier
|-
| || 2006–2012 S. Murphy
|}

== References ==
{{Reflist}}

== External links ==
{{Portal|Neuroscience}}
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1471-4159}}
* [https://itunes.apple.com/us/app/journal-neurochemistry-for/id655612235 Journal App for iPad]
* [http://www.neurochemistry.org/ International Society for Neurochemistry]
* [https://www.facebook.com/pages/International-Society-for-Neurochemistry/205062269527045 ISN and JNC on Facebook]

{{DEFAULTSORT:Journal Of Neurochemistry}}
[[Category:Neuroscience journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Publications established in 1956]]
[[Category:English-language journals]]
[[Category:Biweekly journals]]