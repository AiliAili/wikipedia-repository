<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=ANT-10 / R-7
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Surveillance aircraft|Reconnaissance]]/[[Light bomber]]
 | national origin=[[Soviet Union]]
 | manufacturer=[[Tupolev]]
 | designer=
 | first flight=30 January 1930
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Tupolev R-3]]
 | variants with their own articles=
}}
|}
The '''Tupolev ANT-10''' (also known as the '''R-7''') was a prototype single-engined light bomber/reconnaissance aircraft of the 1930s. Only a single example was built, the [[Polikarpov R-5]] being preferred.

==Development and design==

In 1928, the design bureau led by [[Nikolai Nikolaevich Polikarpov]] produced the [[Polikarpov R-5|R-5]] to replace the R-1, an unlicensed copy of the [[Airco DH.9A]], which was the [[Soviet Union]]'s standard light reconnaissance aircraft/bomber. As a response, the design bureau led by [[Andrei Tupolev]] produced a rival replacement for the R-1, based on Tupolev's earlier [[Tupolev R-3]]. Like the R-3, the new design, the ANT-10 was a single-engined [[biplane|sesquiplane]] with a [[duralumin]] structure, but with a much larger upper wing (based on that of the [[Tupolev I-4|I-4]] fighter). Like the R-5, it was powered by an imported [[BMW VI]] engine. It could carry 500&nbsp;kg (1,100&nbsp;lb) of bombs in an internal bomb-bay.<ref name="Gunston russ p391">Gunston 1995, p.391.</ref>

The ANT-10 (which received the [[Soviet Air Force]] designation R-7) made its first flight on 30 January 1930,<ref name="Duffy p55">Duffy and Kandalov 1996, p.55.</ref> but its performance was little better than the R-5, while the R-5's wooden structure was advantageous at a time of metal shortages. The type was therefore abandoned later in the year in favour of the R-5.<ref name="Duffy p55"/>

==Specifications==
{{Aircraft specs
|ref=Tupolev: The Man and His Aircraft<ref name="Duffy p208">Duffy and Kandalov 1996, p.208.</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=two
|capacity=
|length m=10.9
|length ft=
|length in=
|length note=
|span m=15.2
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=3.6
|height ft=
|height in=
|height note=
|wing area sqm=49
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=1720
|empty weight lb=
|empty weight note=
|gross weight kg=2920
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[BMW VI]]
|eng1 type=water-cooled [[V12 engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=500<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=235
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1100
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=5 hours <ref name="Gunston russ p392">Gunston 1995, p.392.</ref>
|ceiling m=5500
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=3.1 minutes to 1,000 m (3,280 ft)<ref name="Gunston russ p392"/> 
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|guns= 2× [[PV-1 machine gun]]s
|bombs= 500 kg (1,100 lb)
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=*[[Tupolev R-3]]<!-- related developments -->
|similar aircraft=*[[Polikarpov R-5]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
* Duffy, Paul and Andrei Kandalov. ''Tupolev,: The Man and His Aircraft''. Shrewsbury, UK: Airlife Publishing, 1996. ISBN 1-85310-728-X.
* [[Bill Gunston|Gunston, Bill]]. ''The Osprey Encyclopedia of Russian Aircraft 1875–1995''. London:Osprey, 1995. ISBN 1-85532-405-9.
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Tupolev aircraft}}

[[Category:Soviet military reconnaissance aircraft 1930–1939]]
[[Category:Tupolev aircraft|ANT-10]]
[[Category:Sesquiplanes]]