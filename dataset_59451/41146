{{about|the Afghan Star television series|the documentary film about the Afghan Star TV series|Afghan Star (film)}}
{{Infobox television
| show_name            = Afghan Star
| image                = Afghan Star Logo with black background.jpg
| caption              = ''Afghan Star Logo''
| show_name_2          = Sitara-e Afghan
| genre                = 
| creator              = [[Tolo TV]]
| developer            = 
| writer               = 
| director             = 
| creative_director    = 
| presenter            = Daud Sidiqi (Seasons 1 - 3)<br> Omid Nezami (Seasons 4 - 7, & 12) <br> Mustafa Azizyar (Seasons 8 - 11) <br> Ahmad Popal (Co-host for Season 8) <br> Sajid Janati (Co-host for Seasons 9 - 10) <br> Hesam Farzan (Co Host for Season 11 - present)
| judges               = Obaid Juenda <br> Aryana Sayeed <br> Qais Ulfat <br> Saida Gul Maina
| voices               = 
| narrated             = 
| theme_music_composer =Wahid Qasemi (current), Jawad Karimi 
| opentheme            = 
| endtheme             = 
| composer             = 
| country              =  [[Afghanistan]]
| language             = [[Dari Persian,Pashtu, Uzbek, Etc.]]
| num_seasons          = 12
| num_episodes         = 
| list_episodes        = 
| executive_producer   = 
| producer             =
| editor               = 
| location             = [[Kabul]]
| cinematography       = 
| camera               = 
| runtime              = 
| company              = 
| distributor          = 
| picture_format       = 
| audio_format         = 
| first_run            = 
| first_aired          = September 2005
| last_aired           = present
| preceded_by          = Superstar
| followed_by          = 
| related              = 
| website              = http://www.afghanstar.tv
| website_title        = Afghan Star
| production_website   = 
| channel              = [[Tolo TV]]
}}

'''''Afghan Star''''' is a [[reality television]] [[television show|show]] competition that searches for the most talented singers across [[Afghanistan]]. The program is broadcast on the [[Tolo TV]] channel. ''Afghan Star'' premiered in 2005, four years after the fall of the [[Taliban]], which had outlawed singing in 1996; it is one of the most-watched shows in Afghanistan.<ref>{{cite news|last1=Baker|first1=Aryn|title=Afghan Idol: A Subversive TV Hit|url=http://content.time.com/time/world/article/0,8599,1725113,00.html|accessdate=25 June 2015|work=[[Time (magazine)|Time]]|date=24 March 2008}}</ref>

==Format==
The format of ''Afghan Star'' is similar to the British ''[[Pop Idol]]'' or the French ''[[Nouvelle Star]]'' and their many [[Idol series|spinoffs]], though it is independently produced and not a licensed [[adaptation]]. The program starts with auditions from aspiring singers in their late teens and early 20s. A panel of judges choose the best auditionees and puts them through to the public voting round of the contest. At this stage, each week every singer performs and the viewers vote for which singer they prefer using their cell phones. In every round, whoever gets the lowest amount of votes leaves the competition until just one is left, who is declared the winner.

==Seasons==
=== Season 1===
The first season of ''Afghan Star'' was launched in September 2005, with more than 1,000 hopefuls from around Afghanistan gathering to partake in the talent search. The eventual winner of the series was [[Shakeeb Hamdard]] from [[Kabul]], who released his debut album ''Mashalla'' in late 2007.<ref>{{cite news|title=Background|url=http://afghanstar.tv/index.php?option=com_content&view=category&layout=blog&id=34&Itemid=54&lang=en|publisher=Afghan Star|accessdate=2009-05-10}}</ref>

=== Season 2===
''Afghan Star'' season two started in August 2006 with auditions in Kabul, [[Mazar-i-Sharif|Mazar]], [[Herat]], [[Kandahar]], [[Jalalabad]] and [[Peshawar]]. The talent net was widened with Afghans from [[Pakistan]] and [[Iran]] encouraged to participate. The second ''Afghan Star'' was even more successful than the first, with Najibullah Mahmoudi a 35-year-old man from Mazar voted the winner. He has signed a recording contract with Barbud Music, the same company that released Shakeeb Hamdard's music.<ref>{{cite news|title=Background|url=http://afghanstar.tv/index.php?option=com_content&view=category&layout=blog&id=34&Itemid=54&lang=en|publisher=Afghan Star|date=|accessdate=2009-05-10}}</ref>

=== Season 3===
The third season of ''Afghan Star'' debuted in October 2007. Auditions were held in [[Kunduz]] and [[Ghazni]] as well as cities visited in previous series. At this stage, ''Afghan Star'' became one of the most watched and talked about shows on Afghan television, regularly attracting 11 million viewers per episode. The high profile of the show meant heightened [[security]] was introduced around the studios and contestants of the show. The eventual winner of the third series of ''Afghan Star'' was Rafi Naabzada, once again from Mazar.<ref>{{cite news|title=Afghanistan's Pop Idol breaks barriers|url=http://news.bbc.co.uk/1/hi/world/south_asia/7262967.stm|publisher=''[[bbc.co.uk]]''|date=25 February 2008|accessdate=2009-05-10}}</ref><ref>{{cite news|title=Afghan Pop Idol winner declared|url=http://news.bbc.co.uk/1/hi/world/south_asia/7309029.stm|publisher=[[BBC]]|date=21 March 2008|accessdate=2009-05-10}}</ref>

=== Season 4===
The fourth season of ''Afghan Star'' debuted in December 2008. Changes to the format of the show were introduced, including an extra show broadcast per week. The ultimate winner of the season was Navid Forogh from Kabul.<ref>{{cite news|title=Afghan Star Final Highlights|url=http://afghanstar.tv/index.php?option=com_content&view=article&id=262%3Aafghan-star-final-&catid=46%3Alatest-news&Itemid=70&lang=en|publisher=Afghan Star'|date=24 March 2009|accessdate=2009-05-10}}</ref> The second runner-up was Mehran Gulzar and the contestant who become third was Navid Saberpoor.

=== Season 5===
''Afghan Star's'' fifth season started airing in October 2009, concluding in December of the same year. Auditions were conducted from all the cities visited in previous seasons. However, Nazir Khara and Monesa Sherzada-Hassan (the only female judge) did not return to the judging panel for this series. Instead, Ahmad Fanos became a new judge.<ref>{{cite web|url=http://afghanstar.tv/index.php?option=com_content&view=category&layout=blog&id=34&Itemid=54&lang=en |title=Background |publisher=Afghanstar |date= |accessdate=2012-08-06}}</ref> Wali Sazesh, a student from Herat was the winner of the series

=== "Superstar Season 1" ===
A special "Superstar" season of ''Afghan Star'' was scheduled for 2010. The series only featured contestants from previous seasons of ''Afghan Star''. The eventual winner of the contest was crowned the "Superstar" of all Afghan Stars. The contestants led by two coaches [[Ahmad Fanus]] and [[Nazir Khara]], each had 6 Contestants in their teams, The winner of that season was [[Siyar Walizada]] (team Ahmad Fanus)  from the first season of Afghan Star, and second place went to [[Sharif Deedar]] (team Nazir Khara) from second season of Afghan Star.

=== Season 6 ===
The sixth season debuted in November 2, 2010. This season was won by Omid Altaf. The prize for the winner was a new Nissan Car.

=== Season 7===
The seventh season debuted on November 4, 2011.<ref>{{cite web|url=http://www.afghanstar.tv/en/latest-news/season-7/46-latest-news/386-roshan-and-big-bear-to-sponsor-popular-talent-quest-afghan-star- |title=Roshan and Big Bear to Sponsor Popular Talent Quest "Afghan Star" |publisher=Afghanstar.tv |date=1 November 2011 |accessdate=2013-02-19}}</ref> The season was won by [[Reza Rezai]], the second runner up was Mateen Alokozai and the final Runner up was Almas Farahi. The prize for the winner was a new red Toyota Corrola, the second prize was a Pamir motorcycle, and the third prize was another motorcycle. The judges of the season were Qasim Ramishgar, Wajia Rastagar, and Ustad Arman. The music director was Farid Rastagar.

=== Season 8===
The eighth season debuted on November 2, 2012. In this season, all the judges and host were new. The judges were Wali Fateh Ali Khan, Shahlah Zaland, and Jawad Ghaziyar and the host was Mustafa Azizyar. There were many disputes between the judges, which attracted many viewers. Ramishgar returned later on. The music director was Ahmad Jawad Karimi. The winner of this season was Sajid Hussain Jannaty. Second place went to Haroon Andeshwar, and the runner up was Jamshid Sakhi.

=== Season 9===
The ninth season debuted on November 1, 2013. The host of the previous season, Mustafa Azizyar, continued to host the show. Qasem Rameshgar returned as one of the judges and the other two, Wali Fateh Ali Khan and Shahlah Zaland, were the same as the previous season. The guest judge was Tahir Shubab. The new music director was Wahid Qasemi. Last years winner, Sajid Janaty replaced Ahmad Popal as the host of Ba Setara Ha. This year's winner was Rabiullah Behzad, a student training to become a doctor. Second place went to Nayeb Nayab from Farah. Third place went to Arash Barez.

=== Season 10===
The 10th season debuted on October 31, 2014. The judges were the same except Wali Fateh Ali Khan was replaced by Ustad Sharif Ghazal. During the Top 160, Shadkam arrived as the new guest judge of the show. The show concluded on March 21, 2015, with Ali Saqi being crowned as the winner. Second place was Panjshanbe Maftoon, the student of the famous mahali singer [[Mir Maftoon]]. Third place went to Elyas Isaar. The show courted some controversy when contestant Hasher Ehsas was accused by [[District Unknown]] of plagiarising their song, "Two Seconds After The Blast", in a Wild Card Show on February 13, 2015.

=== "Superstar Season 2" ===
Superstar season 2 was the second season of Superstar (Abarsetara) debuted in May 2015, the season hosted by Arash Barez, and there were 12 Contestants from 10 previous seasons, the format of show was like a battle show with two coaches [[Obaid Juenda]] and [[Qais Ulfat]], each coach had 6 contestants in their teams, in top 6 there weren't any team and a new judge Wahid Kacemey came and also the two coaches become judges, the winner of season was [[Omid Parsa]] from Season 4 of Afghan Star,second place went to Elyas Issar from the most recent season of Afghan Star (Season 10), both finalists were from team Obaid Juenda. the prize for winner was a new Chevrolet Car and One Audio Album Recording in Barbud Music, as well as two music videos in the same studio.

=== Season 11===
Season 11 of Afghan Star debuted on October 30, 2015. Three of the four judges this season were replaced with the judges that were previously on The Voice of Afghanistan, Qais Ulfat, Aryana Sayeed, and Obaid Juenda. The fourth judge was Saida Gul Maina. For the first time, Afghan Star had auditions outside of Afghanistan, which were in Australia for one episode. Ahmad Popal replaced Sajid Janati for the host of Ba Setara Ha for a brief period, but Popal was replaced by Season 9 and Superstar Season 2 contestant Hesam Farzan. For the first time, a rapper, Ziba Hamidi, made the show's top 12, the voting segment of the show. Like the previous years, the show was concluded on Nowruz, on March 20th, 2016, with this year's winner being Habibullah Fani from Mazar-i-Sharif with second place going to Ashkan Arab, a contestant from Herat. Third place went to Rashed Aria. Unlike the prior seasons, the winner was simply awarded a trophy.

=== Season 12===
The show returned to its 12th season on November 4, 2016. The judges from Season 11 returned to the show. The host from season 8 to season 11, Mustafa Azizyar, was replaced by Omid Nezami, who hosted the show last time in Season 7. This season had some new and talented contestants, such as Sayid Jamal Mubariz, a rapper, Khalil Yousefi, a singer with 15 years of experience, Shaqayeq Roya, a contestant born in Iran, as well as many others. The contestants who made to the top 3 were awarded a trip to Almaty, Kazakhstan, and they were Sayid Jamal Mubariz, Zulala Hashemi, and Babak Mohammadi. The winner was awarded a motorcycle from Pamir Motorcycle. For the first time since the show's inception, a female contestant to make the show's Top 2, specifically Zulala Hashemi. The farthest a woman has made in this show was Lema Sahar making the top 3 in Season 3. Also for the first time in the show's history, a rapper (Sayid Jamal Mubariz) has made the show's final. Mubariz became the second rapper to make it into the show's top 12, with performances that were praised by the judges each week.


As it was customary with the previous seasons, the Grand Finale was hosted on Nowroz day, on March 21, 2017. Sayid Jamal Mubariz finished first, becoming the first rapper to do so, with Zulala Hashemi finishing second, making her the farthest placed female contestant. The top 3 contestants were awarded various prizes, with Mubariz winning the motorcycle, Hashemi winning an air conditioner and water dispenser, and top 3 finalists, Mubariz, Hashemi, and Babak Mohammadi winning a week long trip to Almaty, Kazakhstan. After being declared the winner, Mubariz decided to give the trophy to Hashemi, due to her placement in the show, as well as showing respect for women in Afghanistan, due to the dangers and hardships they face everyday in the country.

==Judges and hosts==
The judging panel and the host of the show have changed over the years, but the most recent line-up is predominantly male and includes the following individuals:

* '''Ustad Gulzaman''' has been a judge on ''Afghan Star'' since season one. He is renowned in the music industry in Afghanistan specialising in [[Pashto language|Pashto]] songs, and has been recognised as an [[Hindustani classical music|Ustad]] in music. He is also an accomplished composer and poet.<ref>{{cite news|title=Ustad Gulzaman|url=http://afghanstar.tv/index.php?option=com_content&view=article&id=77%3Austad-gulzaman&catid=37%3Ahost-and-judges&Itemid=58&lang=en|publisher=''afghanstar.tv''|date=26 December 2008|accessdate=2009-05-10}}</ref>
* '''Qasem Rameshgar''' has been a judge on ''Afghan Star'' since season four. Born in Kabul, Qasem Rameshgar attended the Arts Faculty of [[Kabul University]] where he studied music. After graduation he went to Russia to study the guitar for 4 years, between 1992 and 1996. He is now the head of the Afghanistan Music Centre, after being involved in music for 27 years.<ref>{{cite news|title=Qasem Rameshgar|url=http://afghanstar.tv/index.php?option=com_content&view=article&id=78%3Aqasem-rameshgar&catid=37%3Ahost-and-judges&Itemid=58&lang=en|publisher=''afghanstar.tv''|date=26 December 2008|accessdate=2009-05-10}}</ref>
* '''Ahmad Fanos''' has been a judge on ''Afghan Star'' since season Five. Originally from Kabul city, Fanos gained a master's degree in communication from [[Saint Petersburg]] in Russia. He subsequently studied singing, and released the famous song "Zendage Shor-e-Nazanin Darad". He specialises in Ghazal songs.<ref>{{cite web|url=http://afghanstar.tv/index.php?option=com_content&view=article&id=377%3Aahmad-fanoos&catid=37%3Ahost-and-judges&Itemid=58&lang=en |title=Kunduz |publisher=Afghanstar.tv |date= |accessdate=2012-08-06}}</ref>
* '''Omaid Nizami''' participated in Season three of ''Afghan Star'' where he made the Top 12. During Season 4, Omaid acted as the co-host for ''Afghan Star'' conducting many of the behind the scenes interviews and discussions. However, he was promoted to centre stage as the main host of the series, as a result of previous host, Daoud Siddiqi's departure to the United States ''(See below)''.<ref>{{cite news|title=Omaid Nizami (Host)|url=http://afghanstar.tv/index.php?option=com_content&view=article&id=180%3Aomaid-nizami-host&catid=37%3Ahost-and-judges&Itemid=58&lang=en|publisher=''afghanstar.tv''|date=1 February 2009|accessdate=2009-05-10}}</ref> He returned as the main host of series five.
*'''Mustafa Azizyar''' is the current host of ''Afghan Star'' since Season 8. It is not known how long he will stay as the host.
*'''Sajid Janati''' is the former host of Ba Setara Ha or With the Stars. He was the host for Season 9 and 10. Janati was replaced by Ahmad Popal for Season 11, but Popal was soon replaced by Hesam Farzan.
*'''Wahid Qasemi''' is the Music Director of Afghan Star since Season 9. He also helps the Stars with improving their voice for their performances.

==''Afghan Star'' documentary==
{{main|Afghan Star (film)}}
In 2009, U.S. distributor [[Zeitgeist Films]] released a documentary film about the third season of ''Afghan Star''. The British-produced film (directed by Havana Marking) won two awards at that year's [[Sundance Film Festival]]: ''World Cinema Audience Award: Documentary'' and ''World Cinema Directing Award: Documentary''.
 
The previous host of the ''Afghan Star'' TV series, Daoud Sediqi, travelled to the United States in February 2009 as part of the promotion of the film worldwide. Sediqi did not return home to Afghanistan. He instead applied, and was subsequently granted [[Right of asylum|asylum]] in the United States in June 2009.<ref>{{cite web|last=Grossman |first=Wendy |url=http://www.people.com/people/archive/article/0,,20306568,00.html |title=Afghanistan's 'Ryan Seacrest' a Star Far from Home |publisher=People.com |date=2009-09-07 |accessdate=2012-08-06}}</ref> Previous to working on ''Afghan Star'', Sediqi had been a DJ on [[Arman FM]] and a presenter on Tolo TV. He hosted all of ''Afghan Star'''s first three seasons, as well as the beginning of the fourth season, and was one of the most recognised names in the Afghan media at the time.<ref>{{cite news|title=One Afghan’s Own Private Exit Strategy|url=http://afghanstar.tv/index.php?option=com_content&view=article&id=263%3Aone-afghans-own-private-exit-strategy&catid=46%3Alatest-news&Itemid=70&lang=en|publisher=''afghanstar.tv''|date=24 March 2008|accessdate=2009-05-10}}</ref>

==References==
{{Reflist|30em}}

==External links==
* [http://afghanstar.tv/index.php?option=com_frontpage&Itemid=1 AfghanStar official website]
* [http://news.bbc.co.uk/1/hi/world/south_asia/7262967.stm BBC news article on AfghanStar]
* [http://www.barbudmusic.com/ Barbud Music (record company of AfghanStar winners Shakeb Hamdard & Najibullah Mahmoudi)]

[[Category:Afghan television series]]
[[Category:Singing talent shows]]
[[Category:2005 television series debuts]]