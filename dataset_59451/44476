{{other uses}}
{{allplot|date=February 2017}}

{{Infobox book| <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = A Connecticut Yankee in King Arthur's Court
| title_orig    =
| translator    =
| image         = Connecticut Yankee4 new.jpg
| caption       = 1889 frontispiece by [[Daniel Carter Beard]], restored
| author        = [[Mark Twain]]
| illustrator   =
| cover_artist  =
| country       = United States
| language      = English
| series        =
| genre         = [[humor]], [[satire]], [[alternate history (fiction)|alternate history]], [[science fiction]] ([[time travel]]), [[fantasy]]
| published     = 1889 ([[Charles L. Webster and Co.]])<ref>[[:File:1889. A Connecticut Yankee in King Arthur's Court.djvu|Facsimile of the original 1st edition]].</ref>
| isbn          =
| preceded_by   = 
| followed_by   =
| wikisource = A Connecticut Yankee in King Arthur's Court
}}
'''''A Connecticut Yankee in King Arthur's Court''''' is an [[1889 in literature|1889 novel]] by American [[humorist]] and writer [[Mark Twain]]. The book was originally titled '''''A Yankee in King Arthur's Court'''''. Some early editions are titled '''''A Yankee at the Court of King Arthur'''''.

In the book, a [[Yankee]] [[engineer]] from [[Connecticut]] is accidentally transported back in time to the court of [[King Arthur]], where he fools the inhabitants of that time into thinking that he is a [[magician (fantasy)|magician]], and soon uses his knowledge of modern [[technology]] to become a "magician" in earnest, stunning the English of the [[Early Middle Ages]] with such feats as [[demolition]]s, [[firework]]s, and the shoring up of a holy well. He attempts to modernize the past, but in the end he is unable to prevent [[Le Morte d'Arthur|the death of Arthur]] and an [[interdict]] against him by the [[Catholic Church]] of the time, which grows fearful of his power.

Twain wrote the book as a [[burlesque]] of [[Romanticism|Romantic]] notions of [[chivalry]] after being inspired by a dream in which he was a knight himself, severely inconvenienced by the weight and cumbersome nature of his [[armor]].

==Plot==

The novel is a comedy that sees 6th-Century England and its medieval culture through Hank Morgan's view; he is a 19th-century resident of [[Hartford, Connecticut]], who, after a blow to the head, awakens to find himself inexplicably transported back in time to [[History of Anglo-Saxon England|early medieval England]] where he meets King Arthur himself. The fictional Mr. Morgan, who had an image of that time that had been colored over the years by romantic myths, takes on the task of analyzing the problems and sharing his knowledge from 1300 years in the future to modernize, Americanize, and improve the lives of the people.

In addition, many passages are quoted directly from [[Sir Thomas Malory]]'s ''[[Le Morte d'Arthur]]'',  a medieval Arthurian collection of legends and one of the earlier sources.  The narrator who finds the Yankee in the "modern times" of Twain's nineteenth century is reading the book in the museum in which they both meet;  later, characters in the story retell parts of it in Malory's original language.  A chapter on medieval hermits also draws from the work of [[William Edward Hartpole Lecky]].

===Introduction to the "stranger"===
[[File:Library Walk 12.JPG|thumb|left|{{"'}}Bridgeport?' said I, pointing. 'Camelot', said he."]]
The story begins as a [[first-person narrative]] in [[Warwick Castle]], where a man details his recollection of a tale told to him by an "interested stranger" who is personified as a [[knight]] through his simple language and familiarity with ancient armor.<ref>Twain, Mark., Clemens, Samuel. (2007), p1 "It was in Warwick Castle that I came across the curious stranger&nbsp;... He attracted me by three things: his candid simplicity, his marvelous familiarity with ancient armor, and the restfulness of his company. As the stranger recalls tales of [[Sir Lancelot]], another man enters the castle and, through a first-person narrative establishes himself"</ref>

After a brief tale of [[Lancelot|Sir Lancelot]] of [[Camelot]] and his role in slaying two giants from the third-person narrative&mdash;taken directly from ''[[Le Morte d'Arthur]]''&mdash;the man named Hank Morgan enters and, after being given whiskey by the narrator, he is persuaded to reveal more of his story. Described through first-person narrative as a man familiar with the [[firearms]] and [[machinery]] trade, Hank is a man who had reached the level of superintendent due to his proficiency in firearms manufacturing, with two thousand subordinates. He describes the beginning of his tale by illustrating details of a disagreement with his subordinates, during which he sustained a head injury from a "[[blunt trauma|crusher]]" to the head caused by a man named "Hercules" using a crowbar.<ref>Twain, Mark., Clemens, Samuel. (2007), p2 "It was during a misunderstanding conducted with crowbars with a fellow we used to call Hercules. He laid me out with a crusher alongside the head that made everything crack"</ref> After passing out from the blow, Hank describes waking up underneath an oak tree in a rural area of Camelot, where he soon encounters the knight [[Sir Kay]], riding by.  Kay challenges him to a [[joust]], which is quickly lost by the unweaponed, unarmored Hank as he scuttles up a tree.  Kay captures Hank and leads him towards Camelot castle.<ref>Twain, Mark., Clemens, Samuel. (2007), p2 "At the end of an hour we saw a far-away town sleeping in a valley by a winding river; and beyond it on a hill, a vast gray fortress, with towers and turrets, the first I had ever seen out of a picture."</ref> Upon recognizing that he has time-traveled to the sixth century, Hank realizes that he is the ''de facto'' smartest person on Earth, and with his knowledge he should soon be running things.

Hank is ridiculed at King Arthur's court for his strange appearance and dress and is sentenced by King Arthur's court (particularly the magician [[Merlin]]) to burn at the stake on 21 June. By a stroke of luck, the date of the burning coincides with a historical [[solar eclipse]] in the year 528, of which Hank had learned in his earlier life. While in prison, he sends the boy he christens Clarence (whose real name is [[Amias Paulet|Amyas le Poulet]]) to inform the King that he will blot out the [[sun]] if he is executed.  Hank believes the current date to be 20 June; however, it is actually the 21st when he makes his threat, the day that the eclipse will occur at 12:03&nbsp;p.m.  When the King decides to burn him, the eclipse catches Hank by surprise. But he quickly uses it to his advantage and [[Historically significant lunar eclipses#1 March 1504|convinces the people that he caused the eclipse]].  He makes a bargain with the King, is released, and becomes the second most powerful person in the kingdom.

Hank is given the position of principal minister to the King and is treated by all with the utmost fear and awe.  His celebrity brings him to be known by a new title, elected by the people&nbsp;&mdash; "The Boss".  However, he proclaims that his only income will be taken as a percentage of any increase in the kingdom's [[gross national product]] that he succeeds in creating for the state as Arthur's chief minister, which King Arthur sees as fair. Although the people fear him and he has his new title, Hank is still seen as somewhat of an equal. The people might grovel to him if he were a knight or some form of nobility, but without that, Hank faces problems from time to time, as he refuses to seek to join such ranks.

=== The Takeover ===
After being made "the Boss", Hank learns about medieval practices and [[superstitions]]. Having superior knowledge, he is able to outdo the alleged sorcerers and miracle-working church officials. At one point, soon after the eclipse, people began gathering, hoping to see Hank perform another miracle. Merlin, jealous of Hank having replaced him both as the king's principal adviser and as the most powerful sorcerer of the realm, begins spreading rumors that Hank is a fake and cannot supply another miracle. Hank secretly manufactures gunpowder and a lightning rod, plants explosive charges in Merlin's tower, then places the lightning rod at the top and runs a wire to the explosive charges. He then announces (during a period when storms are frequent) that he will soon call down fire from heaven and destroy Merlin's tower, then challenges Merlin to use his sorcery to prevent it. Of course, Merlin's "incantations" fail utterly to prevent lightning striking the rod, triggering the explosive charges and leveling the tower, further diminishing Merlin's reputation.

Hank Morgan, in his position as King's Minister, uses his authority and his modern knowledge to industrialize the country behind the back of the rest of the ruling class. His assistant is Clarence, a young boy he meets at court, whom he educates and gradually lets in on most of his secrets, and eventually comes to rely on heavily. Hank sets up secret schools, which teach modern ideas and modern English, thereby removing the new generation from medieval concepts, and secretly constructs hidden factories, which produce modern tools and weapons. He carefully selects the individuals he allows to enter his factories and schools, seeking to select only the most promising and least indoctrinated in medieval ideas, favoring selection of the young and malleable whenever possible.

As Hank gradually adjusts to his new situation, he begins to attend medieval tournaments. A misunderstanding causes [[Sir Sagramore]] to challenge Hank to a duel to the death; the combat will take place when Sagramore returns from his quest for the [[Holy Grail]]. Hank accepts, and spends the next few years building up 19th-century infrastructure behind the nobility's back. At this point, he undertakes an adventure with a wandering girl named the Demoiselle Alisande a la Carteloise—nicknamed "Sandy" by Hank in short order—to save her royal "mistresses" being held captive by ogres. On the way, Hank struggles with the inconveniences of [[plate armor]] (actually an anachronism, which would not be developed until the [[High Middle Ages]] or see widespread use until the [[Late Middle Ages]]), and also encounters [[Morgan le Fay]]. The "princesses", "ogres", and "castles" are all revealed to be actually pigs owned by peasant swineherds, although to Sandy they still appear as royalty. Hank buys the pigs from the peasants and the two leave.

On the way back to Camelot, they find a travelling group of pilgrims headed for the Valley of Holiness. Another group of pilgrims, however, comes from that direction bearing the news that the valley's famous fountain has run dry. According to legend, long ago the fountain had gone dry before as soon as the monks of the valley's monastery built a bath with it; the bath was destroyed and the water instantly returned, but this time it has stopped with no clear cause. Hank is begged to restore the fountain, although Merlin is already trying. When Merlin fails, he claims that the fountain has been corrupted by a demon, and that it will never flow again. Hank, in order to look good, agrees that a demon has corrupted the fountain but also claims to be able to banish it; in reality, the "fountain" is simply leaking. He procures assistants from Camelot trained by himself, who bring along a pump and fireworks for special effects. They repair the fountain and Hank begins the "banishment" of the demon. At the end of several long German language phrases, he says "BGWJJILLIGKKK", which is simply a load of gibberish, but Merlin agrees with Hank that this is the name of the demon. The fountain restored, Hank goes on to debunk another magician who claims to be able to tell what any person in the world is doing, including King Arthur. However, Hank knows that the King is riding out to see the restored fountain, and not "resting from the chase" as the "false prophet" had foretold to the people. Hank correctly states that the King will arrive in the valley.

Hank has an idea to travel amongst the poor disguised as a peasant to find out how they truly live. King Arthur joins him, but has extreme difficulty in acting like a peasant convincingly. Although Arthur is somewhat disillusioned about the national standard of life after hearing the story of a mother infected with [[smallpox]], he still ends up getting Hank and himself hunted down by the members of a village after making several extremely erroneous remarks about agriculture. Although they are saved by a nobleman's entourage, the same nobleman later arrests them and sells them into slavery.

Hank steals a piece of metal in London and uses it to create a makeshift [[lock picking|lockpick]]. His plan is to free himself, the king, beat up their slave driver, and return to Camelot. However, before he can free the king, a man enters their quarters in the dark. Mistaking him for the slave driver, Hank rushes after him alone and starts a fight with him. They are both arrested. Although Hank lies his way out, in his absence the real slave driver has discovered Hank's escape. Since Hank was the most valuable slave&nbsp;&mdash; he was due to be sold the next day&nbsp;&mdash; the man becomes enraged and begins beating his other slaves, who fight back and kill him. All the slaves, including the king, will be hanged as soon as the missing one&nbsp;&mdash; Hank&nbsp;&mdash; is found. Hank is captured, but he and Arthur are rescued by a party of knights led by [[Lancelot]], riding bicycles. Following this, the king becomes extremely bitter against slavery and vows to abolish it when they get free, much to Hank's delight.

Sagramore returns from his quest, and fights Hank. Hank defeats him and seven others, including Galahad and Lancelot, using a lasso. When Merlin steals Hank's lasso, Sagramore returns to challenge him again. This time, Hank kills him with a revolver. He proceeds to challenge the knights of England to attack him en masse, which they do. After he kills nine more knights with his revolvers, the rest break and flee. The next day, Hank reveals his 19th century infrastructure to the country. With this fact he was called a wizard as he told Clarence to do so as well.

=== The Interdict ===
Three years later, Hank has married Sandy and they have a baby. While asleep and dreaming, Hank says, "Hello-Central"&nbsp;&mdash; a reference to calling a 19th-century [[telephone]] operator&nbsp;&mdash; and Sandy believes that the mystic phrase a good name for the baby, and names it accordingly. However, the baby falls critically ill and Hank's doctors advise him to take his family overseas while the baby recovers. In reality, it is a ploy by the Catholic Church to get Hank out of the country, leaving the country without effective leadership. During the weeks that Hank is absent, Arthur discovers Guinevere's infidelity with Lancelot. This causes a war between Lancelot and Arthur, who is eventually killed by [[Sir Mordred]].

The church then publishes "The [[Interdict]]" which causes all people to break away from Hank and revolt. Hank meets with his good friend Clarence who informs him of the war thus far. As time goes on, Clarence gathers 52 young cadets, from ages 14 to 17, who are to fight against all of England. Hank's band fortifies itself in Merlin's Cave with a minefield, [[electric fence|electric wire]] and [[Gatling gun]]s. The Catholic Church sends an army of 30,000 knights to attack them, but the knights are slaughtered by the cadets wielding Hank's modern weaponry.

However, Hank's men are now trapped in the cave by a wall of dead bodies. Hank attempts to go offer aid to any wounded, but is stabbed by the first wounded man he tries to help, [[Maleagant|Sir Meliagraunce]]. He is not seriously injured, but is bedridden. Disease begins to set in amongst them. One night, Clarence finds Merlin weaving a spell over Hank, proclaiming that he shall sleep for 1,300 years. Merlin begins laughing deliriously, but ends up electrocuting himself on one of the electric wires. Clarence and the others all apparently die from disease in the cave.

More than a millennium later, the narrator finishes the manuscript and finds Hank on his deathbed having a dream about Sandy. He attempts to make one last "effect", but dies before he can finish it.

==Characters==

*Hank Morgan
*Clarence (Amyas le Poulet) 
*Sandy (Alisande)
*King Arthur
*Merlin
*Sir Kay
*Sir Lancelot
*Queen Guinevere
*Sir Sagramor le Desirous
*Sir Dinadan
*Sir Galahad
*Morgan le Fay
*Marco
*Dowley
*Hello-Central
*Sir Maleagant
*Giovanni

==Publication history and response==
[[File:A Yankee in the Court of King Arthur book cover 1889.jpg|thumb|First English edition, 1889]]
Twain first conceived of the idea behind ''A Connecticut Yankee in King Arthur's Court'' in December 1884 and worked on it between 1885 and 1889.<ref>LeMaster, J. R. and James D. Wilson (editors). ''The Mark Twain Encyclopedia''. Taylor & Francis, 1993: 174–175. ISBN 082407212X</ref> It was first published in England by [[Chatto & Windus]] under the title ''A Yankee at the Court of King Arthur'' in December 1889.<ref>Rasmussen, R. Kent. ''Critical Companion to Mark Twain: A Literary Reference to His Life and Work''. New York: Facts on File, 2007: 96. ISBN 9780-8160-5398-8</ref> Writer and critic [[William Dean Howells]] called it Twain's best work and "an object-lesson in democracy".<ref>Bell, Michael Davitt. ''The Problem of American Realism: Studies in the Cultural History of a Literary Idea''. Chicago: The University of Chicago Press, 1996: 58. ISBN 0-226-04202-2</ref>

==Analysis==
The book pokes fun at contemporary society, but the main thrust is a [[satire]] of romanticized ideas of [[chivalry]], and of the idealization of the [[Middle Ages]] common in the novels of [[Sir Walter Scott]] and other 19th century literature. Twain had a particular dislike for Scott, blaming his kind of romanticizing of battle for the [[Southern United States|southern states]]' deciding to fight the [[American Civil War]]. He writes in ''[[Life on the Mississippi]]'':
{{Quote|It was Sir Walter that made every gentleman in the South a Major or a Colonel, or a General or a Judge, before the war; and it was he, also, that made these gentlemen value these bogus decorations. For it was he that created rank and caste down there, and also reverence for rank and caste, and pride and pleasure in them. [...] Sir Walter had so large a hand in making Southern character, as it existed before the war, that he is in great measure responsible for the war.|Mark Twain, ''Life on the Mississippi''.<ref>Mark Twain. ''Life on the Mississippi'', ch 46.</ref>}}

For example, the book portrays the medieval people as being very gullible, as when Merlin makes a "veil of invisibility" which, according to him, will make the wearer imperceptible to his enemies, though friends can still see him. The knight Sir [[Sagramore|Sagramor]] wears it to fight Hank, who pretends that he cannot see Sagramor for effect to the audience.

Hank Morgan's opinions are also strongly denunciatory towards the [[Catholic Church]] of the medieval period;  the Church is seen by the Yankee as an oppressive institution that stifles [[science]] and teaches peasants [[meekness]] only as a means of preventing the overthrow of Church rule and taxation.  The book also contains many depictions and condemnations of the dangers of [[superstition]] and the horrors of medieval [[slavery]].

It is possible to see the book as an important transitional work for Twain, in that earlier, sunnier passages recall the frontier humor of his tall tales such as ''[[The Celebrated Jumping Frog of Calaveras County]]'', while the corrosive view of human behavior in the apocalyptic latter chapters is more akin to darker, later Twain works such as ''[[The Mysterious Stranger]]'' and ''[[Letters from the Earth]]''.

George Hardy notes, "The final scenes of 'Connecticut Yankee' depict a mass force attempting to storm a position defended by wire and machine guns - and getting massacred, none reaching their objective. Deduct the fantasy anachronism of the assailants being Medieval knights, and you get a chillingly accurate prediction of a typical [[First World War]] battle.... The modern soldiers of 1914 with their bayonets had no more chance to win such a fight than Twain's knights".<ref>George Hardy, "Visions in a dark mirror" in Mary Wheatley (ed.), "The Beginnings of Science Fiction"</ref>

==As science fiction==

While ''Connecticut Yankee'' is sometimes credited as the foundational work in the [[time travel]] subgenre of [[science fiction]], Twain's novel had several important immediate predecessors. Among them are [[H.G. Wells]]'s story "[[The Chronic Argonauts]]" (1888), which was a precursor to ''[[The Time Machine]]'' (1895). Also published the year before ''Connecticut Yankee'' was [[Edward Bellamy]]'s wildly popular ''[[Looking Backward]]'' (1888), in which the protagonist is put into a hypnosis-induced sleep and wakes up in the year 2000. Yet another American novel that could have served as a more direct inspiration to Twain was ''[[The Fortunate Island]]'' (1882) by [[Charles Heber Clark]]. In this novel, a technically proficient American is shipwrecked on an island that broke off from Britain during Arthurian times, and never developed any further.<ref>"Preface", Allison R. Ensor. ''A Connecticut Yankee in King Arthur's Court: An Authoritative Text, Backgrounds, and Sources, Composition and Publication, Criticism''. New York: W. W. Norton (1982).</ref>

==Adaptations and references==
Since the beginning of the 20th century, this famous story has been adapted many times for the stage, feature-length [[motion picture]]s, and [[animated cartoon]]s. The earliest film version was [[20th Century Fox|Fox]]'s 1921 [[A Connecticut Yankee in King Arthur's Court (1921 film)|silent version]]. In 1927, the novel was adapted into the [[musical theatre|musical]] ''[[A Connecticut Yankee (musical)|A Connecticut Yankee]]'' by [[Rodgers and Hart|Richard Rodgers and Lorenz Hart]]. A 1931 film, also called ''[[A Connecticut Yankee (1931 film)|A Connecticut Yankee]]'', starred [[Will Rogers]]. The story was adapted as an hour-long radio play on the October 5, 1947 broadcast of the [[Ford Theatre]], starring [[Karl Swenson]]. A [[A Connecticut Yankee in King Arthur's Court (1949 film)|1949 musical film]] featured [[Bing Crosby]] and [[Rhonda Fleming]], with music by [[Jimmy Van Heusen]] and [[Victor Young]]. In 1960, [[Tennessee Ernie Ford]] starred in a television adaptation. In 1970, the book was adapted into a 74-minute animated TV special directed by [[Zoran Janjic]] with [[Orson Bean]] as the voice of the title character. In 1978 an episode of [[Once Upon a Classic]], "A Connecticut Yankee in King Arthur's Court", was an adaptation,<ref>{{IMDb title|0315868|Once Upon a Classic, A Connecticut Yankee in King Arthur's Court}}</ref> as was the [[Disney]] movie ''[[Unidentified Flying Oddball]]'', also known as ''A Spaceman in King Arthur's Court''. The TV series ''[[The Transformers (TV series)|The Transformers]]'' had a [[List of The Transformers episodes#Season 2 .281985.29|second-season]] episode, "A Decepticon Raider in King Arthur's Court", that had a group of [[Autobots]] and [[Decepticons]] sent back to medieval times.<ref>http://www.imdb.com/title/tt0808811/</ref> In 1988, the Soviet variation called ''[[New adventures of a Yankee in King Arthur's Court|New adventures of a Yankee in King Arthur's Court. Fantasy over Mark Twain's theme]]'' appeared. More recently it was adapted into a 1989 TV movie by [[Paul Zindel]] which starred [[Keshia Knight Pulliam]].<ref>http://www.imdb.com/title/tt0097104/</ref>

It has also inspired many variations and parodies, such as the 1979 [[Bugs Bunny]] special ''[[A Connecticut Rabbit In King Arthur's Court]]''. In 1995, [[The Walt Disney Company|Walt Disney Studios]] adapted the book into the feature film ''[[A Kid in King Arthur's Court]]''. ''[[Army of Darkness]]'' drew many inspirations from the novel. A 1992 cartoon series, ''[[King Arthur & the Knights of Justice]]'', could also be seen as deriving inspiration from the novel. In 1998 Disney made another adaption with Whoopi Goldberg in ''[[A Knight in Camelot]]''. The 2001 film ''[[Black Knight (film)|Black Knight]]'' similarly transports a modern-day American to Medieval England while adding racial element to the time-traveler plotline.

In the [[Carl Sagan]] novel ''[[Contact (novel)|Contact]]'', the protagonist, Eleanor Arroway, is reading ''A Connecticut Yankee in King Arthur's Court'', specifically the scene where Hank first approaches Camelot, when she finds out about her father's death. The quotation "'Bridgeport?' Said I. 'Camelot,' Said he" is also used later in the book, and the story is used as a metaphor for contact between civilizations at very different levels of technological and ethical advancement.<ref>{{cite book|last=Sagan|first=Carl|title=Contact|year=1985|publisher=Simon and Schuster|location=New York|isbn=0-671-00410-7|pages=9–10, 13, 342}}</ref>

''[[Once Upon a Time (TV series)|Once Upon a Time]]'' features Hank (called [[List_of Once Upon a Time characters#Guest characters|Sir Morgan]]) and his daughter (named Violet).

==See also==
{{portal|Novels|Connecticut}}
* [[Mark Twain bibliography]]
* [[List of films based on Arthurian legend]]
* ''[[A Dream of John Ball]]'' (1889) by [[William Morris]]

==References==
* {{cite book | last=Tuck | first=Donald H. | authorlink=Donald H. Tuck | title=The Encyclopedia of Science Fiction and Fantasy | location=Chicago | publisher=[[Advent (publisher)|Advent]] | pages=104 | year=1974|isbn=0-911682-20-1}}

==Notes==
{{reflist|30em}}

==External links==
{{wikisource|A Connecticut Yankee in King Arthur's Court|''A Connecticut Yankee in King Arthur's Court''}}
{{Commons category|A Connecticut Yankee in King Arthur's Court}}
* {{gutenberg|no=86|name=A Connecticut Yankee in King Arthur's Court}}
* {{librivox book | title=A Connecticut Yankee in King Arthur's Court | author=Mark Twain}}
* [http://www.sparknotes.com/lit/yankee/ SparkNotes on the book]

== See also ==
* [[1889 in science fiction]]

{{Twain}}
{{A Connecticut Yankee in King Arthur's Court}}

{{Authority control}}

{{DEFAULTSORT:Connecticut Yankee In King Arthur's Court, A}}
[[Category:1889 novels]]
[[Category:19th-century American novels]]
[[Category:1880s fantasy novels]]
[[Category:1880s science fiction novels]]
[[Category:American science fiction novels]]
[[Category:American alternate history novels]]
[[Category:American fantasy novels]]
[[Category:American fantasy novels adapted into films]]
[[Category:Modern Arthurian fiction]]
[[Category:Novels by Mark Twain]]
[[Category:Novels set in Connecticut]]
[[Category:Time travel novels]]
[[Category:Novels set in the 6th century]]
[[Category:Novels adapted into television programs]]