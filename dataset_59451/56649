{{italic title}}
{{Infobox journal
| title         = Ecology and Society
| cover         = 
| editor        = Carl Folke, Lance Gunderson
| discipline    = [[Ecology]], [[Environmental studies]]
| peer-reviewed = 
| language      = 
| former_names  = Conservation Ecology
| abbreviation  = 
| publisher     = [[Resilience Alliance]]
| country       = 
| frequency     = Quarterly
| history       = 
| openaccess    = Yes
| license       = 
| impact        = 2.77
| impact-year   = 2014
| website       = http://www.ecologyandsociety.org
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 1708-3087
| eISSN         = 
| boxwidth      = 
}}
'''''Ecology and Society''''' (formerly ''Conservation Ecology'') is a quarterly [[open access]] [[interdisciplinarity|interdisciplinary]] [[scientific journal]] published by the [[Resilience Alliance]]. It covers an array of disciplines from the [[natural science]]s, social sciences, and the humanities concerned with the relationship between society and the life-supporting [[ecosystem]]s on which human well-being ultimately depends. The journal's editors are Carl Folke ([[Stockholm Resilience Centre]]) and Lance Gunderson ([[Emory University]]). [[C. S. Holling]] was the founding editor.

Issues are available online as "in progress" as soon as articles are published. Special features are published separately throughout the year.  Many of the articles in ''Ecology and Society'' are published as part of special features.  Much of the research from a social-ecological systems perspective is published in ''Ecology and Society'' .   Recent special features include "Understanding Human Resilience in the Context of Interconnected Health and Social Systems", "Advancing the Understanding of Behavior in Social-Ecological Systems: Results from Lab and Field Experiments", and "A Framework for Analyzing, Comparing, and Diagnosing Social-Ecological Systems."

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.8, and it ranks 7 of 100 journals in the field of Environmental Studies and 45 of 145 in the field of Ecology.<ref name=WoS>{{cite book |year=2014 |chapter=Ecology and Society |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2016-01-27 |work=Web of Science |postscript=.}}</ref>

{{As of|January 2016}}, the three most cited articles from ''Ecology and Society'' were:
* BH Walker, C.S. Holling, S.R. Carpenter, A Kinzig. 2004. Resilience, adaptability and transformability in social-ecological systems. ''Ecology and Society'' 9(2):5
* DW Cash, NW Adger, F Berkes et al.  2006. Scale and cross-scale dynamics: Governance and information in a multilevel world. ''Ecology and Society'' 11(2):8
* JM Anderies, MA Janssen, E Ostrom. 2004.  A framework to analyze the robustness of social-ecological systems from an institutional perspective.  ''Ecology and Society'' 9(1):18

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.ecologyandsociety.org}}

[[Category:Open access journals]]
[[Category:Ecology journals]]
[[Category:Environmental social science journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1997]]
[[Category:Human ecology]]
[[Category:Environmental humanities journals]]