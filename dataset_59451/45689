{{Infobox person
|name          = Richard VanGrunsven 
|image         = 
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = 1939
|birth_place   = [[Forest Grove, Oregon]]
|death_date    = 
|death_place   = 
|death_cause   = 
|resting_place = 
|resting_place_coordinates = 
|residence     = 
|nationality   = 
|ethnicity     = 
|citizenship   = 
|other_names   = "Van"
|known_for     = 
|education     = Bachelors in Engineering
|alma_mater    = Verboot High, [[University of Portland]]<ref>{{cite journal|magazine=University of Portland Magazine|date=Spring 2008|title=A Creature of the Air}}</ref>
|employer      = [[Van's Aircraft]]
|occupation    = 
|home_town     = [[Cornelius, Oregon]]
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = 
|spouse        = 
|partner       = 
|children      = 
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}

'''Richard E. "Dick" VanGrunsven'''{{refn|Although commonly known as "Dick" by the media and public, VanGrunsven primarily uses "Richard".|group=}} (born 1939) is an American aircraft designer and [[Homebuilt aircraft|kit plane]] manufacturer. The number of VanGrunsven-designed homebuilt aircraft produced each year in North America exceeds the combined production of all commercial [[general aviation]] companies.<ref name="Wallace">{{cite journal|magazine=Sport Aviation|date=June 2012|author=Lane Wallace|title=Van's Air Force}}</ref>

In 1973 VanGrunsven founded the aircraft manufacturing company [[Van's Aircraft]], and in 2012 became the founding president of the [[Aircraft Kit Industry Association]] (AKIA), an [[United States|American]] aviation advocacy association.<ref name="Pew06Jul12">{{cite news|url = http://www.avweb.com/avwebflash/news/AKIA_experimental_homebuilt_kit_aircraft_association_FAA_NTSB_206938-1.html|title = AKIA -- The Kit Manufacturers' Association|accessdate = 9 July 2012|last = Pew|first = Glenn|date = 6 July 2012| work = AVweb}}</ref><ref name="Hirschman2012">{{cite news|url = http://www.aopa.org/aircraft/articles/2012/120629aircraft-kit-industry-forms-new-association.html|title = Aircraft kit industry forms new association |accessdate = 9 July 2012|last = Hirschman|first = Dave|year = 2012| work = [[Aircraft Owners and Pilots Association]]}}</ref><ref name="Wood05Jul12">{{cite news|url = http://www.generalaviationnews.com/2012/07/05/kit-manufacturers-form-new-association/|title = Kit manufacturers form new association|accessdate = 9 July 2012|last = Wood|first = Janice|date = 5 July 2012| work = General Aviation News}}</ref><ref name="Pew27Jul12">{{cite news|url = http://www.avweb.com/news/airventure/akia_experimental_safety_ntsb_aircraft_airventure_207094-1.html|title = Kit Association Responding To NTSB|accessdate = 28 July 2012|last = Pew|first = Glenn|date = 27 July 2012| work = AVweb}}</ref><ref name="KitplanesDec2012">Bernard, Mary and Suzanne B. Bopp: ''Q&A with AKIA President Dick VanGrunsven'', Kitplanes, Volume 29, Number 12, December 2012, page 28-29. Belvoir Publications. ISSN 0891-1851</ref>

== Life and career ==
[[File:Vans RV-8A N98VR Hondo TX 2007 Jun 02.jpg|right|thumb|An example of a Van's aircraft, the [[Van's Aircraft RV-8|RV-8A]]]]
VanGrunsven is the son of a [[Portland, Oregon|Portland]] area farmer.  He has seven brothers and sisters. His father was a pilot since the 1930s and operated a [[Piper J-3 Cub]], and later a [[Taylorcraft Aircraft|Taylorcraft]], from his farm. Richard learned to fly in 1956. His first aeronautical business was providing wheelpants for Taylorcraft. He graduated from the [[University of Portland]] in 1961 with an [[Bachelor of Engineering|engineering degree]]. He then joined the [[U.S. Air Force]] that same year. VanGrunsven originally planned to become a fighter pilot but a minor vision problem (acceptable in civilian aviation but not to the Air Force) led instead to him serving three years as a communications officer. After serving in the Air Force, he worked as a designer for [[Hyster]], an Oregon manufacturer of lift trucks. His free time was devoted to learning more about aviation.<ref>{{cite web|title=Van's Aircraft, Inc. History|url=http://www.fundinguniverse.com/company-histories/van-s-aircraft-inc-history/|accessdate=27 September 2015}}</ref><ref>{{cite web|title=A Creature of The Air|url=https://www.up.edu/portlandmag/2008_spring/creatureair_txt.html|accessdate=27 September 2015}}</ref> He now has earned Chief Flying Instructor, multi-engine, and Airline Transport Pilot ratings and has over 12,000 hours of flight time logged.<ref>{{cite web|title=Richard VanGrunsven|url=http://www.vansaircraft.com/public/pers-van.htm|accessdate=9 June 2012}}</ref>

Towards the mid-1960s, VanGrunsven purchased a [[Stits Playboy]] homebuilt aircraft and modified it by installing a larger engine. Later he modified the aircraft by installing cantilevered aluminum wings with flaps, creating the [[VanGrunsven RV-1|RV-1]] in 1965. A few years later he started a clean-sheet design, the all-aluminum [[Van's Aircraft RV-3|RV-3]] single-place aircraft. In 1973 he founded [[Van's Aircraft]].<ref>{{cite news|newspaper=The Portland Tribune|author=Joseph Gallivan|title=Olinger?s DIY pilots love to go RV-ing|date=17 July 2007}}</ref> The RV-3 was followed by the [[Van's Aircraft RV-4|RV-4]] tandem aircraft in 1979. Van's Aircraft continued to produce new designs with good all-round performance, reasonable costs, and continuous improvement in kit quality.<ref name="Wallace" />

VanGrunsven commutes regularly to his company in [[Aurora, Oregon]] using aircraft of his own design. His company has sold over 18,000 kits or sets of plans, with over 7,500 aircraft completed.<ref name="Wallace" />

Van's homebuilt designs are built in enough numbers that several groups, such as "Freedom Flight" and the thirteen member "Team RV", have organized all-RV formation demonstration teams.<ref>{{cite news|newspaper=Houston Chronicle|title=Group takes to the air to celebrate Independence Day|author=TARA SULLIVAN|date=22 June 2009}}</ref><ref>{{cite news|newspaper=The Dispatch|author=Brandon Thalor|title=Team RV Puts On Aerial Show For The Dispatch Writer, Photographer|date=17 June 2011}}</ref>

In 2006 VanGrunsven was inducted into the Oregon Aviation Hall of Fame.<ref>{{cite web|title=Oregon Aviation Historical Society |url=http://oregonaviation.files.wordpress.com/2012/01/2006_09.pdf|accessdate=9 June 2012}}</ref>

In November 2013 VanGrunsven was appointed to the board of directors of the [[Experimental Aircraft Association]] (EAA).<ref name="Pew22Nov13">{{cite news|url = http://www.avweb.com/avwebflash/news/EAA-Adds-Recognized-Names-To-Board221022-1.html|title = EAA Adds Recognized Names To Board|accessdate = 25 November 2013|last = Pew|first = Glenn|date = 22 November 2013| work = AVweb}}</ref> That same year he, along with [[Klapmeier brothers|Dale Klapmeier]], [[Burt Rutan]], [[Bob Hoover]] and others, launched a campaign and website made for honoring EAA's former long-time president [[Tom Poberezny]]. The website also contained a Roster of Support where EAA-attendees could add their signature advocating for Poberezny's return to the annual [[EAA AirVenture Oshkosh|EAA AirVenture Airshow]].<ref>{{cite web|url=http://generalaviationnews.com/2013/10/15/honorpoberezny-com-launches/|title=HonorPoberezny.com launches|author=General Aviation News Staff|work=General Aviation News}}</ref><ref>{{cite web|url=http://www.flyingmag.com/pilots-places/pilots-adventures-more/petition-urges-eaa-honor-tom-poberezny|title=Petition Urges EAA to Honor Tom Poberezny|author= Pia Bergqvist|work=Flying Magazine}}</ref>

[[Flying (magazine)|''Flying'']] magazine ranked VanGrunsven 22 on its list of the "51 Heroes of Aviation" and has labelled him the "undisputed leader in kit aircraft manufacturing".<ref>http://www.flyingmag.com/photo-gallery/photos/51-heroes-aviation?pnid=41832</ref>

== References ==
{{Reflist|30em}}

==External links==
*[https://www.vansaircraft.com/ www.vansaircraft.com]&nbsp;— Van's Aircraft site
*[http://www.akia.aero/ AKIA official site]
*[http://www.aircraftspruce.com/catalog/kitspages/monbuild.php?clickkey=2258228 2012 article from ''Aircraft Spruce'']

{{VansAircraft}}

{{DEFAULTSORT:VanGrunsven, Richard}}
[[Category:American aerospace engineers]]
[[Category:People from Forest Grove, Oregon]]
[[Category:University of Portland alumni]]
[[Category:Living people]]
[[Category:American engineers]]
[[Category:United States Air Force officers]]
[[Category:1939 births]]
[[Category:Cornelius, Oregon]]