{{Infobox organization
| name   = Cradles to Crayons
| logo   = 
| type   =
| founded_date      = {{Start date|2002}}
| founder           = Lynn Margherio
| location          = [[Boston, Massachusetts]], [[Philadelphia, Pennsylvania]], and [[Chicago, Illinois]]
| origins           =
| key_people        =
| areas_served       =
| product           =
| focus             =
| method            =
| revenue           =
| endowment         =
| num_volunteers    =
| num_employees     =
| num_members       =
| subsid            =
| owner             =
| Non-profit_slogan =
| homepage          = {{url|http://cradlestocrayons.org/}}
| dissolved         =
| footnotes         =
}}

'''Cradles to Crayons''' (C2C) is a [[non-profit organization]] that provides homeless and low-income children living in [[Boston, Massachusetts|Boston]], [[Massachusetts]], [[Philadelphia, Pennsylvania|Philadelphia]], [[Pennsylvania]] and [[Chicago, Illinois|Chicago]], [[Illinois]] with the essentials they require to thrive - to feel safe, warm, ready to learn, and valued.

==History==
Founder and CEO Lynn Margherio saw the opportunity for Cradles to Crayons while visiting her family over the holidays in 2001.  She noticed her nieces’ and nephews’ toys and clothes piled high in the playroom and closets at her brother’s house; these items were in perfectly good condition, but were no longer being used.  From these and similar experiences, C2C was shaped by a vision that existing community resources could be leveraged to benefit economically disadvantaged children, bridging the gap between those who have too much and those who do not have enough. In 2002, Cradles to Crayons officially opened its doors in Quincy, Massachusetts, and after five successful years providing critically needed items to local disadvantaged children, the Cradles to Crayons model was replicated to help children in Philadelphia, PA. In 2011, we relocated our Massachusetts Giving Factory to a larger space in Brighton with the goal of doubling our impact over the next three to five years. C2C’s unique business model provides an avenue through which children’s products in good and working condition can be recycled in a socially and environmentally responsible way.

Among communities across Massachusetts, C2C is looked to as a leader in the non-profit sector. In 2011, Cradles to Crayons was acknowledged as an innovative grassroots leader and recognized with the “Partnership of the Year” award from the Boston Business Journal for our work with Staples, and received the prestigious Neighborhood Builder Award from Bank of America. In that same year, C2C was honored to be chosen by Massachusetts Governor, Deval Patrick, to host his inaugural Project 351 event, a day of public service with 8th Grade representatives from each of Massachusetts’ 351 cities and towns. This year C2C was honored to serve again as a site for Project 351. [[Deval Patrick]] led a day of community service in the warehouse with hundreds of high schools students from Massachusetts.<ref name="Gov. Patrick leads students in day of service in Brighton.">{{cite news|url=http://www.wickedlocal.com/allston/features/x100919543/Gov-Patrick-leads-students-in-day-of-service-in-Brighton#axzz1canjH4RV |title=Gov. Patrick leads students in day of service in Brighton |last=Seidner |first=Matthew |date=21 January 2011 |publisher= The Allston-Brighton Tab |work=wickedlocal.com |accessdate=3 November 2011}}</ref>

==Services and programs==

Cradles to Crayons' programs operate year-round to support disadvantaged children across the state of Massachusetts, providing the essentials each child needs to overcome their unique situations and excel both inside and outside of the classroom. C2C collects new and gently used children’s items through grass roots community drives. Donated items are then sorted, inspected and packaged into personalized “C2C KidPacks” by volunteers. KidPacks are filled with clothing, shoes, books, arts and crafts, and often birthday presents.<ref name="bostonglobe">{{cite news |title=Cradles to crayons: the need is filled; Volunteers recycle toys and clothes |url=http://www.highbeam.com/doc/1P2-7890622.html |last=English |first=Bella |date=27 February 2005 |newspaper=Boston Globe |accessdate=24 March 2012}}</ref> KidPacks are brought directly to the families free of charge, through partnerships with hundreds of qualified social service agencies across Massachusetts such as Catholic Charities, The Home for Little Wanderers and Horizons for Homeless Children.<ref name="Cradles to Crayons is a good fit for their giving">{{cite news|url=http://www.bizjournals.com/boston/print-edition/2010/12/24/cradles-to-crayons-is-a-good-fit-for.html?page=all |title=Cradles to Crayons is a good fit for their giving |last=Moore |first=Mary |date=24 December 2010 |newspaper=Boston Business Journal |pages= |accessdate=2011-11-03}}</ref>

Cradles to Crayons' signature program, Everyday Essentials, operate without interruption, programs are focused with seasonal initiatives. From July through September, Ready for School, provides children with the materials they need to feel ready to learn from head to toe, including a special emphasis on providing thousands of new backpacks with necessary school supplies. Gear Up for Winter, supplies kids with the cold weather gear, that they need to stay warm. During the traditional spring cleaning months, Spring Greening encourages families to clean out their closets and donate their gently used items to C2C. Gear Up for Baby focuses on gathering infant and child safety equipment throughout May and June.

==Impact==
Over the years, Cradles to Crayons has distributed more than 290,000 packages of essential items to disadvantaged Massachusetts children, including 127,000 backpacks filled with brand new school supplies. In 2011 Cradles to Crayons engaged more than 24,000 volunteers in the "Giving Factory" warehouse to fill 55,000 packages for disadvantaged Massachusetts children. Cradles to Crayons’ overarching goal is to eliminate the effects of child poverty throughout the Commonwealth, their short-term goal is to continue connecting communities of need with communities of plenty.

==Media==
Cradles to Crayons was reviewed by Barack Obama`s Charity Navigator, an organization which evaluates the financial health, accountability and transparency of America's largest charities.<ref name="Charity Navigator Rating">{{cite news|url=http://www.charitynavigator.org/index.cfm?bay=search.summary&orgid=12402 |title=Charity Navigator Rating |author= |authorlink= |coauthors= |date= |format= |work= |publisher= Charity Navigator|pages= |language= |archiveurl= |archivedate= |quote= |accessdate=2011-11-03}}</ref>

Cradles to Crayons was featured in the December 2011 issue of [[The Oprah Magazine]].<ref name="Oprah Magazine">{{cite journal |url=http://www.oprah.com/spirit/How-to-Spend-a-Little-and-Give-a-Lot-Small-Donations/2#ixzz1f96v2P00 |title=16 Ways to Make an Incredible Impact With Less Than $15 |date=December 2011 |journal=O Magazine |accessdate=4 December 2011}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://cradlestocrayons.org/ Cradles to Crayons]
* [http://www.charitynavigator.org/ Charity Navigator]

[[Category:Articles created via the Article Wizard]]
[[Category:Companies established in 1992]]
[[Category:Non-profit organizations based in Boston]]
[[Category:Non-profit organizations based in Pennsylvania]]