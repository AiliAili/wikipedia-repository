{{Infobox person
| name        = Erica A. De Mane 
| image       = Erica De Mane.jpg
| caption     = At the Union Square Farmer's Market, September 2003
| birth_name  = 
| birth_date  = {{Birth date and age|1953|12|03}} 
| birth_place = Bronx, New York
| death_date  = 
| death_place = 
| nationality = American
| other_names = 
| occupation  = Writer
| known_for   = Books and journalism about southern Italian cuisine
}}

'''Erica De Mane''' (born December 3, 1953 in [[Bronx, NY]]) is a [[chef]], [[food writer]], [[liquer]] entrepreneur, and teacher who specializes in [[Italian cooking]]. She is the author of ''The Flavors of Southern Italy'', ''[[Pasta]] Improvvisata'', and ''Williams-Sonoma Pasta'', and contributed to the Italian section of the 1997 revision of ''[[Joy of Cooking]]''. She is a member of the [[International Association of Culinary Professionals]], the Italy-based international [[Slow Food]] movement, and the Culinary Historians of New York.

== Background ==
Erica developed her interest in cooking as a teenager, drawing inspiration from the recipes she grew up with in her family's southern Italian–American kitchen on [[Long Island]]. She studied journalism at the [[Fashion Institute of Technology]] from 1975 to 1977<ref>{{cite web |url= http://blog.fitnyc.edu/alumni/2013/03/15/erica-demane-fashions-springtime-dish/ |title=Erica DeMane Fashions a Springtime Dish » FIT Alumni |first=Yoland |last=Urrabazo |work=blog.fitnyc.edu |publisher=[[Fashion Institute of Technology]] |date=15 March 2013 |accessdate=4 October 2014}}</ref> and at [[New York University]] from 1977 to 1979, and attended the New York Restaurant School from 1983 to 1984. In 1985, she began cooking at [[Manhattan]] restaurants, including Le Madri and [[The Florent]]. Her play, ''Kitchen Arts'', a comedy about cooking, was produced at Manhattan's 13th Street Repertory Company<ref>http://www.13thstreetrep.org/past-shows.html</ref> in 1987.

== Cooking Style ==
As the titles of her books imply, Erica's cooking is improvisation-driven and her style derived from the flavors of [[southern Italy]]: the bitter olive oil and oranges, the honey laced [[agrodolce]] (sweet and sour), the salty anchovies, capers, and olives, and the mix of peasant and regal that are hallmarks of the cooking of the [[mezzogiorno]]. Her family's food, from inland [[Apulia]], with its flavors of tomato, garlic, and wild greens, inspired research that eventually took her to [[Sicily]], [[Naples]], and [[Calabria]]. There she studied the traditional cooking of the entire region, expanding her palate, and soon began improvising while never straying too far from the flavors that give southern Italian cooking its distinctive charm. Southern Italians recognize Erica's dishes, even at their most creative, as being purely southern Italian in spirit, at least in part because of her continuous search for the best and most authentic ingredients available. She has stated that she never makes a dish exactly the same way twice—a radically different approach from her family's, where the rules of traditional Italian cooking were set in stone.

== Books and Journalism ==
Erica's first published articles appeared in ''[[Food & Wine (magazine)|Food & Wine]]'' magazine<ref>http://www.foodandwine.com/chefs/erica-de-mane</ref> in 1993 (she also worked in their test kitchen), and subsequently in ''[[The New York Times]]'',<ref>{{cite journal|last1=De Mane|first1=Erica|title=Summer vegetables make pasta concoctions sing|journal=Pittsburgh Post-Gazette -|date=Aug 14, 1997|url=https://news.google.com/newspapers?nid=1129&dat=19970814&id=prNRAAAAIBAJ&sjid=o28DAAAAIBAJ&pg=3599,10212367}}</ref>  ''[[Gourmet (magazine)|Gourmet]]'', and ''Fine Cooking'',<ref>http://www.finecooking.com/search?cx=009096020989677304441%3Akkzh0x3f3yc&cof=FORID%3A9&ie=UTF-8&q=Erica+DeMane</ref> among other publications. Her monthly food column appeared in ''[[Marie Claire]]'' magazine from 1997 to 1999. A monthly column on the [[Mediterranean diet]] appeared in ''MyCurves'',<ref>http://curves.com.my/wp-content/uploads/2014/02/Curves-Feb-2014.pdf</ref> an online publication of [[Curves Fitness]], the international fitness chain, from 2012 to 2014. In 2015 she began developing recipes and writing for [[Weight Watchers]]. Her first book, ''Pasta Improvvisata'', published by Scribner in 1999, was chosen by ''The New York Times'' as one of the best [[cookbook]]s of the season for its June 1999, twice-yearly cookbook roundup.<ref>{{cite news |url= https://www.nytimes.com/books/99/06/06/reviews/990606.06cookint.html#pasta |title=Cooking |first=Corby |last=Kummer |work=[[The New York Times]] |date=6 June 1999 |publisher=[[New York Times Company|NYTC]] |location=[[New York, NY|New York]] |issn=0362-4331 |accessdate=6 October 2014}}</ref> That was followed by ''Pasta'',<ref>http://books.simonandschuster.com/Williams-Sonoma-Collection-Pasta/Erica-De-Mane/Williams-Sonoma-Collection/9780743224437</ref> for Williams-Sonoma, in 2003, and ''The Flavors of Southern Italy'', published by John Wiley & Sons in 2005.<ref>{{cite web |url= http://www.publishersweekly.com/978-0-471-27251-9 |title=Nonfiction Book Review: THE FLAVORS OF SOUTHERN ITALY |author=staff writer |work=[[Publishers Weekly]] |date=26 April 2004  |accessdate=6 October 2014}}</ref> The latter was chosen by both ''[[Publishers Weekly]]'' and Food & Wine as one of the best cookbooks of the year. She is currently working on a collection of essays on Italian flavor combinations and on bringing to market Enrica D, her own small-batch amaro, the classic, bitter Italian liqueur. A novel-in-progress about a hapless American magazine editor abroad in Italy, written with Barbara Calamari,<ref>http://www.goodreads.com/author/show/146002.Barbara_Calamari</ref> is tentatively entitled ''Devil Lady''.

== Media and Appearances ==
Since 1998, Erica's blog <ref>http://ericademane.com/</ref> (originally ''Skinny Guinea'', currently ''Improvisation Italian-Style'') has offered many of her recipes, as well as essays on her cooking philosophy, reviews of her books, and her carefully chosen artwork, illustrated by a popular running feature, Women with Fish.<ref>http://ericademane.com/about/women-with-fish/</ref> She has appeared on ''Food Talk with Arthur Schwartz'', on the Food Network, on Bloomberg Radio’s ''Dining with Peter Elliot'', on Heritage Food Radio,<ref>{{cite web|url=http://www.heritageradionetwork.com/episodes/8422-We-Dig-Plants-Episode-8422-Carmen-Devito-Alice-Marcus-Krieg-Erica-DeMane |title=Episodes &#8422; Heritage Radio Network |first=Carmen |last=Devito |work=[[Heritage Radio Network]] |date=27 July 2015 |accessdate=19 August 2015 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> and on other national and local radio and TV shows.
She has given cooking demonstrations at numerous gourmet shops, bookstores, farmers’ markets, and culinary events, including De Gustibus<ref>http://degustibusnyc.com/</ref> and ''The New York Times'' Style magazine’s “Taste of T.” She also teaches private and group cooking classes on southern Italian cooking and diet.<ref>{{cite web |url= http://www.ediblemanhattan.com/magazine/september-october-2009/why_the_economic_downturn_should_drive_you_to_the_bar/ |title=Why the Economic Downturn Should Drive You to the Bar - Edible Manhattan |author=editors |work=ediblemanhattan.com |date=September 11, 2009 |accessdate=6 October 2014}}</ref>

== Personal ==
Erica De Mane is married to Ferd Allen,<ref>http://www.forbes.com/sites/frederickallen/</ref> an editor at ''[[Forbes]]'' magazine. They live in Manhattan with Buddy and Red, her [[Japanese bobtail]]s.

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using<ref></ref> tags, these references will then appear here automatically -->
{{Reflist}}

== External links ==
* {{official website|http://www.ericademane.com}}

{{DEFAULTSORT:Mane, Erica De}}
[[Category:1953 births]]
[[Category:Living people]]
[[Category:People from New York City]]
[[Category:American cookbook writers]]
[[Category:American women writers]]
[[Category:Women cookbook writers]]
[[Category:Chefs of Italian cuisine]]