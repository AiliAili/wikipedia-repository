{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{Infobox military person
|name= John Henry Cound Brunt
|image= John Henry Brunt VC.jpg
|image_size= 
|alt= 
|caption= Second Lieutenant John Brunt c.1943, with the badges of the Sherwood Foresters
|birth_date= {{birth date|1922|12|06|df=yes}}
|death_date= {{Death date and age|1944|12|10|1922|12|06|df=yes}}
|birth_place= [[Priest Weston]], Shropshire
|death_place= [[Faenza]], Italy
|placeofburial= Faenza War Cemetery
|nickname= 
|allegiance= {{flag|United Kingdom}}
|branch= {{army|United Kingdom}}
|serviceyears= 1941–1944
|rank= [[Captain (British Army and Royal Marines)|Captain]]
|servicenumber= 
|unit= [[Queen's Own Royal West Kent Regiment]]<br/>[[Sherwood Foresters]], attached [[Royal Lincolnshire Regiment]]
|commands= 
|battles= [[World War II|Second World War]]
* [[North African Campaign]]
* [[Italian Campaign (World War II)|Italian Campaign]]
|awards= [[Victoria Cross]]<br/>[[Military Cross]]
|relations= 
|laterwork= 
}}
[[Captain (British Army and Royal Marines)|Captain]] '''John Henry Cound Brunt''' {{post-nominals|country=GBR|size=100%|sep=,|VC|MC}} (6 December 1922 – 10 December 1944) was an English recipient of the [[Victoria Cross]], the highest award for gallantry in the face of the enemy that can be awarded to British and [[Commonwealth of Nations|Commonwealth]] forces. He served in [[Italian Campaign (World War II)|Italy]] during the [[World War II|Second World War]] and was twice decorated for bravery in action before he was killed by [[mortar (weapon)|mortar]] fire.

==Early life==
John Henry Cound Brunt was born on 6 December 1922, on a farm in [[Priest Weston]], near [[Chirbury]], Shropshire to Thomas Henry Brunt and Nesta Mary Brunt (née Cound), and began his education at Chirbury village school.<ref name=swm>{{cite book|last=Francis|first=Peter|title=Shropshire War Memorials, Sites of Remembrance|year=2013|publisher=YouCaxton Publications|page=38|isbn=978-1-909644-11-3}}</ref> He had an elder sister named Dorothy (born 13 May 1920) and a younger sister Isobel (born 5 October 1923). When Dorothy was eight, the family moved to a farm near [[Whittington, Shropshire]], where John grew up. As he became older, his fearless nature became more apparent; every week, he read the comic ''"Tiny Tots"'', which featured instructions on "How to teach yourself to swim". One day, he asked Dorothy to take him to the [[Shropshire Union Canal|Shropshire Canal]], which went through their farmland. Before his sister could stop him, Brunt had taken off all his clothes and jumped into the canal. When they finally arrived home, their mother wanted to know why he had no clothes on, and John responded that he had been teaching himself to swim. As he got older, his daredevil attitude became even more serious; on one occasion, he was found swinging himself along the guttering of a [[dutch barn]] sixty feet above the farmyard.<ref name = "All for Valour">{{cite book|last=Snow|first=Richard|title=All for Valour (The Story of Captain John Brunt V.C., M.C.)|date=September 2006|publisher=The Marketing Solution|location=Paddock Wood, Kent}}</ref>

When old enough, Brunt was enrolled at [[Ellesmere College]], where his mischievous streak became quickly apparent through pranks and dares; once, while in the [[sanatorium]] with [[mumps]], he slipped a laxative into the matron's tea. Nevertheless, he is fondly remembered at the school. It was while he was at Ellesmere that he contracted [[measles]], resulting in his need to wear glasses.<ref name = "All for Valour" /> An enthusiastic sportsman, Brunt played [[cricket]], [[field hockey|hockey]], [[rugby football|rugby]], [[water polo]] and [[wrestling]]. He was the only pupil at the school to tackle the headmaster while playing rugby, injuring the older man's knee in the process.<ref name = "All for Valour" />

In 1934, the Brunt family moved to [[Paddock Wood]] in Kent and, in his school holidays, "Young John" (as he was known in the village) would come home. Although he was still a reckless individual, he was thought of very highly, and helped train the Paddock Wood [[British Home Guard|Home Guard]] between 1940 and 1943, assisted by his father. He spent his last days in Paddock Wood helping with the [[Hops|hop]] harvest.<ref name = "All for Valour" />

==Military career==
{{Rquote|right|John was one of the most popular officers in this Battalion, his light, energetic personality was a delight to all who met him... he carried out feats of daring that will long live in our memory.|Lt. Col. F.C.L. Bell|Letter to John Brunt's parents}}

Brunt joined the [[British Army]] when he left school, training as a [[Private (rank)|private]] with the [[Queen's Own Royal West Kent Regiment]] in 1941. He received a [[Officer (armed forces)#Commissioned officers|commission]]  as a [[second lieutenant]] on 2 January 1943,<ref name=Gazette35997>{{LondonGazette|issue=35997|supp=yes|startpage=1950|endpage=1953|date=27 April 1943|accessdate=13 December 2007}}</ref> and was posted to [[North African campaign|North Africa]]. Although he was commissioned in the [[Sherwood Foresters]],<ref name=Gazette35997 /> he never served with them, instead being posted to the 6th Battalion, [[Royal Lincolnshire Regiment]], having become friendly with Captain Alan Money, an officer in the Lincolns, on the boat to Africa.<ref name = "All for Valour" />

On 9 September 1943, Brunt's regiment landed at [[Salerno]] in Italy and Lieutenant Brunt was given command of No.9 Platoon in A Company. The unit subsequently moved south-east to establish a base in a farm near the river Asa.<ref name = "All for Valour" />

===Military Cross===
Between December 1943 and January 1944, Brunt commanded a battle patrol and saw near-constant action. In the early hours of 15 December, they received orders to destroy an enemy post based in some houses {{convert|200|yards|metres}} north of the [[River Peccia]]. In efforts to break the enemy line, he crossed and re-crossed the river so many times that the troops took to calling it "Brunt's Brook". After an intense five-minute bombardment, Brunt led a section into an assault. The first two houses contained two enemy soldiers, but it was the third house that provided the most resistance. Using grenades and [[Thompson submachine gun|Tommy guns]], they managed to kill eight enemy troops outside the house, as well as those inside, all belonging to the 1st Battalion, 2nd [[Herman Goering Panzer Grenadier Regiment]]. After thirty minutes of intense fighting, the patrol withdrew, having had one man killed and six wounded. While the rest of the section pulled back, Brunt remained behind with his sergeant and a private to retrieve a wounded soldier. For his actions, he was awarded the [[Military Cross]].<ref name = "All for Valour" /><ref>{{LondonGazette|issue=36394|startpage=937|date=22 February 1944|accessdate=13 December 2007}}</ref>

On 5 January 1944, Brunt was in a sick bed in a rear hospital. He pleaded with doctors to be allowed to leave to take part in an attack, and was given permission, leading his patrol under heavy fire.  He was back in the hospital twenty-four hours later with concussion after a piece of shrapnel almost split his helmet, but would have carried on fighting if it had not been for an [[non-commissioned officer]], who forcibly led him away from the front line.<ref name = "All for Valour" /> At the end of the campaign, Brunt is said to have commented to his friends, "I've won the M.C., now for the V.C.!"<ref name = "All for Valour" />

===Victoria Cross===
After resting in Syria and Egypt, Brunt returned to Italy on 3 July 1944, having been promoted to temporary [[Captain (British Army and Royal Marines)|captain]] and appointed second in command of "D" Company. By early December 1944, the regiment was operating near [[Ravenna]], fighting German troops who were retreating north through Italy. On the night of 3 December, the regiment began their attack on the town of [[Faenza]]. By the evening of 6 December, they had taken the village of [[Ragazzina]] near Faenza, and after heavy fighting the Lincolns established defensive positions in Faenza itself.<ref name = "All for Valour" /> For his actions during the engagement, Brunt was awarded the Victoria Cross. The full citation for the award appeared in a supplement to the [[London Gazette]] of 6 February 1945, reading:<ref>{{London Gazette|issue=36928|supp=yes|startpage=791 |date=6 February 1945 |accessdate=21 September 2008}}</ref>

{{cquote|''War Office, 8th&nbsp;February, 1945''

The KING has been graciously pleased to approve the posthumous award of the VICTORIA CROSS to:—

Lieutenant (temporary Captain) John Henry Cound Brunt, M.C. (258297), The Sherwood Foresters (Nottinghamshire and Derbyshire Regiment) (Paddock Wood, Kent).

In Italy, on the 9th&nbsp;December, 1944, the [[Platoon]] commanded by Captain Brunt was holding a vital sector of the line.

At dawn the German 90 [[Panzer Division|Panzer Grenadier Division]] counter-attacked the Battalion's forward positions in great strength with three [[Panzer IV|Mark IV tanks]] and infantry. The house, around which the Platoon was dug in, was destroyed and the whole area was subjected to intense mortar fire. The situation then became critical, as the anti-tank defences had been destroyed and two Sherman tanks knocked out. Captain Brunt, however, rallied his remaining men, and, moving to an alternative position, continued to hold the enemy infantry, although outnumbered by at least three to one. Personally firing a [[Bren gun]], Captain Brunt killed about fourteen of the enemy. His wireless set was destroyed by shell-fire, but on receiving a message by runner to withdraw to a [[Company (military unit)|Company]] locality some 200 yards to his left and rear, he remained behind to give covering fire. When his Bren ammunition was exhausted, he fired a [[PIAT]] and 2 in. [[Mortar (weapon)|Mortar]], left by casualties, before he himself dashed over the open ground to the new position. This aggressive defence caused the enemy to pause, so Captain Brunt took a party back to his previous position, and although fiercely engaged by small arms fire, carried away the wounded who had been left there.

Later in the day, a further counter-attack was put in by the enemy on two axes. Captain Brunt immediately seized a spare Bren gun and, going round his forward positions, rallied his men. Then, leaping on a [[Sherman tank]] supporting the Company, he ordered the tank commander to drive from one fire position to another, whilst he sat, or stood, on the turret, directing [[Besa machine gun|Besa]] fire at the advancing enemy, regardless of the hail of small arms fire. Then, seeing small parties of the enemy, armed with bazookas, trying to approach round the left flank, he jumped off the tank and, taking a Bren gun, stalked these parties well in front of the Company positions, killing more and causing the enemy finally to withdraw in great haste leaving their dead behind them.

Wherever the fighting was heaviest, Captain Brunt was always to be found, moving from one post to another, encouraging the men and firing any weapon he found at any target he could see. The magnificent action fought by this Officer, his coolness, bravery, devotion to duty and complete disregard of his own personal safety under the most intense and concentrated fire was beyond praise. His personal example and individual action were responsible to a very great extent for the successful repulse of these fierce enemy counter-attacks.

The next day Captain Brunt was killed by mortar fire.}}

The next morning, having won the battle and the acclaim of his regiment, Captain Brunt was as eager to return to the offensive, keeping alert for more trouble as breakfast was being prepared for the men, their first meal in 48 hours.  He was standing in the doorway of the platoon headquarters, having a mug of tea and chatting with friends, when a stray German mortar bomb landed at his feet, killing him outright.  He had celebrated his 22nd birthday just four days before.<ref name = "All for Valour" />

John Brunt is buried at Faenza War Cemetery in Italy under a [[Commonwealth War Grave]] headstone;<ref>{{cite web|url=http://www.cwgc.org/search/casualty_details.aspx?casualty=2237026|title=Commonwealth War Graves Commission – Casualty details|publisher=[[Commonwealth War Graves Commission]]|accessdate=13 December 2007}}</ref> his Victoria Cross was announced [[Posthumous recognition|posthumously]] in February 1945.<ref name = "All for Valour" />

==Victoria Cross presentation==
On 18 December 1945, [[George VI|King George VI]] presented Brunt's Victoria Cross and Military Cross to his parents at [[Buckingham Palace]]. Brunt's father met [[Field marshal (United Kingdom)|Field Marshal]] [[Harold Alexander, 1st Earl Alexander of Tunis|Sir Harold Alexander]], the [[15th Army Group]] commander for most of the [[Italian Campaign (World War II)|Italian Campaign]], at the ceremony and said to him "I expect that you know many men who should have been awarded this medal", to which Alexander replied "No, because there is always only one who will do the unexpected and that day it was your son."<ref name = "All for Valour" />

==Legacy==
[[File:John Brunt new sign.JPG|thumb|The new sign, unveiled on 9 November 2008]]In 1946, John Brunt's sister Dorothy gave birth to a boy which she named John Brunt Miller, in honour of his heroic uncle.<ref name = "All for Valour" />

On 3 September 1947 the Kent Arms public house in [[Paddock Wood]], Kent, was named the [[John Brunt V.C. (public house)|John Brunt V.C.]] in his honour. In 1997, the pub changed its name to The Hopping Hooden Horse; after local outrage the original name was restored in 2001.<ref name = "All for Valour" /> Behind the pub a small housing development called John Brunt VC Court was built.<ref name=swm2>{{cite book|title=Shropshire War Memorials, Sites of Remembrance|page=39}}</ref>

During his military career, Brunt was awarded the Victoria Cross, Military Cross, [[1939-45 Star]], [[Africa Star]], [[Italy Star]] and the [[War Medal 1939-1945|British War Medal 1939–1945]], all of which are on display in Royal Lincolnshire Regiment and Lincolnshire Yeomanry Collections in the [[Museum of Lincolnshire Life]] in [[Lincoln, Lincolnshire|Lincoln]].{{citation needed|date=March 2015}} In 1951 an altar rail in the Soldiers' Chapel of St George in [[Lincoln Cathedral]] was dedicated to his memory by the regiment.<ref name=swm2 />

On 17 July 1965 [[The Victor (comic)|The Victor comic]] featured a cover story named ''Brunt V.C.'', a two-page strip based on the actions that won Brunt the VC.<ref>{{cite book|title=The Victor|date=July 1965|publisher=D.C. Thompson}}</ref>

A John Brunt Memorial Cricket Pavilion was opened at Ellesmere College in 1970, after funds were raised for it since 1945. The College's Ante Chapel holds a photograph of Brunt with a copy of his VC citation displayed below.<ref name=swm2 />

In May 2004 an outdoor plaque to his memory was unveiled in Priestweston.<ref name=swm2 />

==Notes==
{{Reflist|30em}}

==References==
*{{cite book|last=Laffin|first=John|title=British VCs of World War 2|year=1997}}
*{{cite book|last=Harvey|first=David|title=Monuments to Courage. Victoria Cross Headstones & Memorials|year=1999|isbn=1-84342-356-1|publisher=Naval & Military Press Ltd}}
*{{cite book|title=The Register of the Victoria Cross|year=1997|isbn=0-906324-27-0|publisher=This England}}

==External links==
*[http://www.thelincolnshireregiment.org/vc.shtml Lincolnshire Regiment VCs] ''(Royal Lincolnshire & Royal Anglian Regimental Association (Lincoln Branch))''
{{Good article}}

{{DEFAULTSORT:Brunt, John Henry Cound}}
[[Category:1922 births]]
[[Category:1944 deaths]]
[[Category:People educated at Ellesmere College]]
[[Category:British Army personnel of World War II]]
[[Category:British military personnel killed in World War II]]
[[Category:British World War II recipients of the Victoria Cross]]
[[Category:People from Shropshire]]
[[Category:Recipients of the Military Cross]]
[[Category:Sherwood Foresters officers]]
[[Category:Queen's Own Royal West Kent Regiment soldiers]]
[[Category:British Army recipients of the Victoria Cross]]