__NOTOC__
{|{{Infobox Aircraft Begin
 |name =A-122 Uirapuru
|image =uirapuru_voa.jpg
|caption=T-23A Uirapuru.
}}{{Infobox Aircraft Type
 |type =Primary Trainer
|national origin =[[Brazil]]
|manufacturer =[[Aerotec]]
|designer =
|first flight =2 June 1965
|introduction =
|produced =1968-1977
|number built=155
}}
|}

The '''[[Aerotec]] A-122 Uirapuru''' was a Brazilian military trainer aircraft. It was a low-wing [[monoplane]] with tricycle [[Landing gear|undercarriage]] that accommodated the pilot and instructor side-by-side.  It first flew on 2 June 1965.<ref name="Songbird p13">Pereira 1977, p.13.</ref>

In October 1967, the [[Brazilian Air Force]] ordered 30 aircraft to replace the obsolete [[Fokker S.11]]s and [[Fokker S.12|S.12]]s (T-21s and T-22s) that were operating in the Air Force Academy.<ref name="Songbird p13-4">Pereira 1977, p. 13-14.</ref>  Later, they ordered another 40, and then 30 more.  These were designated '''T-23'''.

The [[Bolivian Air Force]] ordered 36 examples in [[1974 in aviation|1974]], which flew until [[1997 in aviation|1997]], and in 1975 the [[Military of Paraguay|Paraguayan Air Force]] bought 8 aircraft to replace the Fokker T-21 (S.11). In 1986, six more were donated by the FAB. Most of them were withdrawn from service in 1992, replaced by the Enaer T-35 Pillán. As of 2009, only one T-23 is in flying conditions. Thirty others were sold in the civilian market. A total of 155 were built including prototypes by the time production finished in 1977.<ref name="Janes 80p10">J W R Taylor 1980, p.10.</ref>

The T-23 suffered fatal accidents during [[Spin (flight)|spin]] training. The problem was resolved after a crash in which an instructor described his stricken aircraft's responses to his control inputs all the way to the end.  Uirapurus then received a [[fin]] under the rear [[fuselage]] to correct the issue.

In [[1980 in aviation|1980]] interest by the airforce in an improved version led to the development of the [[Aerotec Uirapuru II|Uirapuru II]].

==Variants==
* '''A-122A Uirapuru''' - T-23 - Military trainer
* '''A-122B Uirapuru''' - Civil version<ref name="Janes">{{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=39 }}</ref>
* '''A-122C Uirapuru''' - T-23C 
* '''A-132 Uirapuru II''' - enlarged version with improved canopy and larger vertical tail surfaces.<ref name="Janes"/>

==Operators==
; {{BRA}}
* [[Brazilian Air Force]]
; {{BOL}}
* [[Bolivian Air Force]] - 18 A-122A purchased in 1974.<ref>Siegrist 1987, p. 194.</ref>
; {{PAR}}
* [[Paraguayan Air Force]]  -  14 aircraft (8 in 1975 and 6 in 1986)
* Escuela Nacional de Aeronáutica Civil  -  1 aircraft (early 70s)

==Specifications (T-23)==
[[File:uirapuru 3vistas.jpg|thumb|right|A-122A(Military) A-122C(civil)]]
{{aircraft specifications

<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->

<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). 
If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")" and start a new, fully formatted line beginning with * -->

|ref=Jane's All The World's Aircraft 1971-72 <ref name="Janes71 p10">J W R Taylor 1971,p.10.</ref>

|crew=Two
|capacity=
|length main= 6.60 m
|length alt=21 ft 8 in
|span main=8.50 m
|span alt=27 ft 10¾ in
|height main=2.70 m
|height alt=21 ft 8 in
|area main=13.50 m²
|area alt=145.3 ft²
|airfoil=NACA 43013
|empty weight main=540 kg
|empty weight alt=1,191 lb
|loaded weight main=  
|loaded weight alt=  
|useful load main=  
|useful load alt= 
|max takeoff weight main=840 kg
|max takeoff weight alt=1,825 lb
|more general=

|engine (prop)=[[Lycoming O-320]]-B2B
|type of prop=4-cylinder horizontally opposed air cooled
|number of props=1
|power main=119 kW
|power alt=160 hp
|power original=

|max speed main=225 km/h
|max speed alt=122 knots, 140 mph
|cruise speed main=185 km/h
|cruise speed alt=100 knots, 115 mph
|stall speed main=88 km/h
|stall speed alt=48 knots, 55 mph
|never exceed speed main=307 km/h
|never exceed speed alt=165 knots, 190 mph
|range main=800 km
|range alt=429 nm, 495 mi
|ceiling main=4,500 m
|ceiling alt=17,000 ft
|climb rate main=4.0 m/s
|climb rate alt=787 ft/min
|loading main=  
|loading alt=  
|thrust/weight=
|power/mass main=  
|power/mass alt=  
|more performance=*'''Endurance:''' 4 hours

|armament=

|avionics=

}}

==See also==
{{Portal|Aviation}}
{{aircontent|
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->

|related=*[[Aerotec A-132 Tangará]]<!-- related developments -->

|similar aircraft=<!-- similar or comparable aircraft -->

|sequence=<!-- designation sequence, if appropriate -->

|lists=<!-- related lists -->

|see also=<!-- other relevant information -->

}}

==References==
;Notes
{{reflist}}
;Bibliography
*{{cite magazine|last=Pereira |first= Roberto|authorlink= |coauthors= |date=July 1977|title=Songbird of the Amazon |trans_title= |journal= [[Air International]]|volume= 13 |issue= 1|pages=13–17 |id= |url= |accessdate=|quote= }}
*{{cite magazine|last=Siegrist|first=Martin|title=Bolivian Air Power — 70 Years On|magazine=Air International|date=October 1987|volume=33|issue=4|pages=170–176, 194|issn=0306-5634}}
*{{cite book |title= Jane's All The World's Aircraft 1971-72|last= Taylor|first= John W.R. (editor)|authorlink= |coauthors= |year=1971 |publisher=Sampson Low |location=London |isbn=0-354-00094-2 |page= |pages= |url= }}
* {{cite book |title= Jane's All The World's Aircraft 1980-81|last=Taylor |first= John W R (ed.)|authorlink= |coauthors= |year=1980 |publisher= Jane's Publishing |location= London|isbn= 0-7106-7105-9|page= |pages= |url= }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=39 }}

==External links==
{{commons category-inline|Aerotec A-122 Uirapuru}}

[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Brazilian military trainer aircraft 1960–1969]]
[[Category:Aerotec aircraft|Uirapuru]]