{{Infobox Museum
 | name        = Kyriazis Medical Museum
 | established = {{Start date and age|2011|df=yes}}
 | collection  = approx. 1 thousand objects
 | location    = 35 Karaoli & Demetriou Street, Larnaca, Cyprus
 | coordinates = {{coord|34.92169|33.633903|display=inline}}
 }}
[[File:Larnaca 01-2017 img05 Kyriazis Medical Museum.jpg|thumb|230px|Kyriazis Medical Museum occupies the ground floor.]]
The '''Kyriazis Medical Museum''' in [[Larnaca]], Cyprus was established in 2011.<ref name=established>Politis newspaper 19 October 2011, p. 50</ref>  It displays medical items, books, and framed documents relating to the practice of Cypriot medicine and the [[history of medicine in Cyprus]], from antiquity to the 20th century. It is sometimes referred to as a Medical Museum—the only one in Cyprus.<ref name="established"/>

A sign on the facade says: "Larnaka Cultural Walk - '''Kyriazis Medical Museum''' - A unique in its kind museum in Cyprus which presents the medical, healing and health history of the island." It is located on Karaolis and Dimitriou Street. It is open on Wednesdays and Saturdays from 9 AM to 12.30. Entrance is free.

The Museum organises cultural events, lectures for the public and activities with medical interest. It also functions as a 'health hangout' for healthcare professionals and the general public alike.

==History==
It was opened by Cyprus Health Minister [[Stavros Malas]] in October 2011.<ref>Philelephtheros newspaper, 15 October 2011</ref>

Its founder is [[Marios Kyriazis]],<ref>Prima Scala magazine, p. 44 June 2012</ref> a descendant of four generations of doctors and pharmacists in Larnaca. It is housed in a traditional restored and listed town mansion, donated by Kyriazis.<ref>Iatrikos Kosmos, Journal of the Cyprus medical Association February 2012, p. 12</ref> Kyriazis has donated items including items and books inherited from his grandfather [[Neoclis Kyriazis]] (1878–1956) and from his great grandfather Antonios Tsepis (1843–1905) both of whom practiced in Larnaca. The collections of the museum begun in 1990 but due to lack of suitable space the items remained stored, although occasionally, these were exhibited within other medical or historical exhibitions.
[[File:kmm2.jpg|thumb|The entrance hall]]

==Exhibits==
Documents with old Cypriot poems and [[curse]]s with medical content,<ref>http://www.cyprushighlights.com/en/index.php/page/44/</ref> and a number of virtually forgotten sayings and poems with medical slant of [[Byzantian]], [[Franks|Frankish]], [[Republic of Venice|Venetian]] or [[Ottoman Empire|Ottoman]] origins are exhibited on walls. The sayings and poems were shaped by a history of lack of medical facilities, inadequate treatments and absence of health prevention.

===The Hall===
The walls of the main entrance hall (Iliakos) are lined with framed pictures displaying the history of medicine from antiquity, through the Middle Ages and early 1800s. Examples include depictions of operations without anaesthetic, treatment of cholera or the plague, and treatments in monasteries.

===Room 1===
This is the first room encountered on the left of the main entrance. There are Pharmacist’s cupboards displaying original bottles, tablets, injections, and doctor’s prescriptions. Items include a bullet extractor and a spring-loaded device for [[physiotherapy]] of muscles in the hand (where each finger's movement—of a hand that is closing—is counteracted by the force of separate springs, as the springs are stretched). Among other exhibits are a phallic item believed to have been used in the treatment of female hysteria in ancient times, and a rare find: oil from the Larnaka salt lake which was used in skin wounds and insect bites. In this room there is also a display of many old traditional Cypriot poems or sayings with medical content.

===Room 2===
An [[X-ray machine]],  an electronic [[EKG]] measuring instrument, an ob/gynaecological bench and a surgical table are among exhibits in room two. The display information on the gynaecological bench states that it was used by nearly half of the original population of Larnaka. There is also a pharmacist’s display unit with several medical items from the 1850s including cupping material, leeches, a tonsil extractor, a Victorian magneto-electric device and cautery items.
[[File:One of the rooms- surgical theatre.jpg|thumb|Surgical equipment.]]

===Room 3===
Although there are several medical and pharmaceutical items exhibited, this room is served mainly as a study/research facility. A medical library with books in Greek, Cypriot, French and English is available to students or academic researchers. A pharmaceutical display unit contains items for making hand-made pills.

===Room 4===
Original surgical amputation instruments and a [[trough]] with wood [[shaving]]s (to absorb blood) with a mock amputated hand are among exhibits in room four. There is a replica of the Hippocratic Ladder, a wooden ladder used to treat dislocations of the hip or of the neck. The injured leg was pulled by a ceramic pot full of stones or water.  A medical partition donated by the old Larnaca Hospital serves as a poster display unit with medical cartoons or information about traditional Cypriot therapies. Other items include a doctor's examination couch and equipment for treating tuberculosis.

Books are in all the four rooms. On request, permission to manipulate exhibits can be informally granted.

===Garden===
The garden is a traditional urban residential area where meetings or public events are being held. There is a collection of some Cypriot healing herbs such as mint, basil, lavender, sage, melissa and marjoram. The garden is accessible to the public, as this is used also as a health hub, where herbal and other health drinks are served. A covered area has a further display facility where outdoors exhibitions have taken place.

==Architecture==
The museum has a neoclassic-type façade with blue window shutters, high ceilings, wooden floors in the rooms, and the obligatory decorated ceramic pattern in the ‘iliakos’ floor complete the picture.<ref>Luke Chrysanthou, Sunjet magazine, Cyprus Airways June 2012</ref> The house used to be a private residence, but the street where the museum is located had an unusually high number of doctors, nurses or pharmacists living there. Approximately 50% of all local residents had a medical connection.<ref>https://www.cyprus.gov.cy/MOI/pio/pio.nsf/All/C6505F7E24C4E54DC2257A6A00305199/$file/CyprusToday_12_Small.pdf</ref>

==Philosophy of its management==
The aim is to safeguard the medical cultural heritage of Cyprus for future generations of medical scientists and to identify facts of sociological, scientific, medical or literary interest.<ref>http://www.larnakaregion.com/listing_info.php?id=596</ref> The museum aims to reach any member of the public, of any age, and to facilitate exchange of information, learning and discussion on current health practices based on the past.

==Notable Events==
The Medical Museum has organised the following cultural events:

April 2012: ‘Strange Traditional Cypriot Treatments’, under the auspices of the Mayor of Larnaca, also broadcast on [[Cyprus Broadcasting Corporation]] channel 1 <ref>http://www.cybc-media.com/video/index.php/video-on-demand?task=play&id=6515&sl=cats</ref>

September 2013: ‘World Tourism Day’ events and practical demonstrations

May 2013: ‘2000 years from the death of [[Apollonios of Kition]]’ under the auspices of the Cyprus Health Minister. This event included Ancient Greek wrestling ([[Pankration]]), medical theatre and presentations.

12 September 2013: Exhibition of medical cartoons, under the auspices of the Mayor of Larnaca.

26 September 2013: ‘Medical Saints of Cyprus’ under the auspices of the Bishop of Kition. Presentations from Kalogera Primary School students and other lectures.

22 May 2014: An evening of Cypriot medicine, in association with the Larnaca Doctors Association, and the Larnaca Municipality.

==Ongoing research projects==
Three main research projects are currently being undertaken:

1. A compilation of a Cypriot medical dictionary. The aim is to collect and publish all Cypriot words with a medical meaning. Several interested parties are now collaborating in order to prepare a complete collection of all Cypriot medical terms including anatomy, pharmacology and nursing.

2. A study of ‘strange’ traditional healing practices. This includes anything unusual or weird used in the past in order to heal or ameliorate illness.  Examples include baked lizards and gunpowder used in baldness, donkey manure in infections, and live mice in a variety of health recipes.

3. The collection, display and subsequent publication of medieval, Byzantian or early Cypriot literature with medical content. This includes poems and ballads, wishes and curses, folk stories, and couplets.

==References==
{{reflist}}

[[Category:Museums in Cyprus]]
[[Category:Medical museums]]
[[Category:History of medicine]]
[[Category:Health in Cyprus]]