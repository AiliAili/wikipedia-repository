{{Use mdy dates|date=July 2015}}
{{Infobox United States federal proposed legislation
| name            = Anna Westin Act of 2015
| fullname        = To amend the Public Health Service Act with respect to eating disorders, and for other purposes.
| acronym         =
| nickname        =
| introduced in the = 114th
| sponsored by    =  	[[Ted Deutch]]
| number of co-sponsors = 
| public law url  =
| cite public law =
| cite statutes at large = 
| acts affected = 
| acts repealed   =
| title affected   =
| sections created =
| sections affected = 
| agenciesaffected = 
| authorizationsofappropriations =
| appropriations =
| leghisturl      =
| introducedin    = 
| introducedbill  = 
| introducedby    = 
| introduceddate  = 
| committees      = 
| passedbody1     =
| passeddate1     = 
| passedvote1     = 
| passedbody2     =
| passedas2       =
| passeddate2     =
| passedvote2     =
| conferencedate  =
| passedbody3     =
| passeddate3     =
| passedvote3     =
| agreedbody3     =
| agreeddate3     =
| agreedvote3     =
| agreedbody4     =
| agreeddate4     =
| agreedvote4     =
| passedbody4     =
| passeddate4     =
| passedvote4     =
| signedpresident =
| signeddate      =
| unsignedpresident =
| unsigneddate    =
| vetoedpresident =
| vetoeddate      =
| overriddenbody1 =
| overriddendate1 =
| overriddenvote1 =
| overriddenbody2 =
| overriddendate2 =
| overriddenvote2 =
| amendments      =
| SCOTUS cases    =
}}
The '''Anna Westin Act''' of 2015 ({{USBill|114|H.R.|2515}}, {{USBill|114|S.|1865}}), is a proposed bill which is aimed at training school officials and healthcare professionals on how to identify those with eating disorders and on how to intervene.<ref name=":2" /> 30 million Americans suffer from [[eating disorder]]s.<ref name=":0">Wade, T. D., Keski‐Rahkonen, A., & Hudson, J. I. (2011). Epidemiology of eating disorders. ''Textbook of Psychiatric Epidemiology, Third Edition'', 343-360.</ref> Eating disorders have the highest mortality rate out of any mental illness<ref>Sullivan, P. F., Bulik, C. M., Fear, J. L., & Pickering, A. (1998). Outcome of anorexia nervosa: A case-control study. ''American Journal of Psychiatry''.</ref> and affects women 2.5 times more than men.<ref name=":0" /> Additionally, eating disorders research to improve identification and treatment is limited. Eating disorder research receives some of the lowest amounts of research funding compared to all other diseases at $30 research dollars per person affected versus $188 research dollars for [[Autism]] and $682 research dollars for [[Breast cancer|Breast Cancer]].<ref>Health and Human Services, National Institute of Health (2014). ''Estimates of funding for various research, condition, and disease categories (RCDC).'' https://report.nih.gov/categorical_spending.aspx</ref>

On May 21, 2015, [[Ted Deutch|Congressman Ted Deutch (D-FL-21)]] and Congresswoman [[Ileana Ros-Lehtinen|Ileana Ros-Lehtinen (R-FL-27)]] introduced the bill in the [[United States House of Representatives|House of Representatives]]<nowiki/>.<ref>{{Cite web|title = U.S. Reps. Ted Deutch, Ileana Ros-Lehtinen Introduce New Bipartisan Eating Disorders Legislation|url = http://teddeutch.house.gov/news/documentsingle.aspx?DocumentID=398431|website = US Congressman Ted Deutch|access-date = 2016-02-09}}</ref><ref>{{Cite news|url = http://edcexamplewebsite.weebly.com/uploads/5/8/8/1/58810665/combating_eating_disorders_gains_ample_bipartisan_support_mental_health_weekly_8.24.2015.pdf|title = Mental Health Weekly|last = Canady|first = Valerie|date = August 20, 2015|work = |access-date = |via = }}</ref> On July 27, 2015, [[Amy Klobuchar|Senator Amy Klobuchar (D-MN)]] and [[Kelly Ayotte|Senator Kelly Ayotte (R-NH)]] introduced the bill in the [[United States Senate|Senate]] with support and leadership from Senators [[Tammy Baldwin|Tammy Baldwin (D-WI)]] and [[Shelley Moore Capito|Shelley Moore Capito (R-WV)]].<ref>{{Cite web|title = Ayotte Helps Introduce Legislation to Improve Prevention and Treatment of Eating Disorders  {{!}} Kelly Ayotte {{!}} United States Senator|url = http://www.ayotte.senate.gov/?p=press_release&id=2092|website = www.ayotte.senate.gov|access-date = 2016-02-09}}</ref><ref name=":1">{{Cite web|title = Klobuchar introduces legislation combating eating disorders|url = http://kimt.com/2015/07/29/klobuchar-introduces-legislation-combating-eating-disorders/|website = KIMT 3|access-date = 2016-02-09|first = Katie|last = Huinker}}</ref><ref name=":2">{{Cite web|title = New legislation aimed at preventing, treating eating disorders|url = http://www.news8000.com/news/politics/new-legislation-aimed-at-preventing-treating-eating-disorders/34415570|website = WKBT|access-date = 2016-02-09|last = WKBT}}</ref>

==About==
The Anna Westin Act of 2015 is written to help those affected by eating disorders get the care they need by focusing on improved training and clarity of mental health parity. The bill is designed to have a zero [[Congressional Budget Office|CBO]] score.  In addition, the House of Representatives version of the Anna Westin Act includes the truth in advertising act, a small inter-agency study looking at digitally altered images of humans as they related to fair advertising practices.<ref>{{Cite web|title = Eating Disorders Coalition: Current Initiatives|url = http://www.eatingdisorderscoalition.org/inner_template/our_work/current-initiatives.html|website = www.eatingdisorderscoalition.org|access-date = 2016-02-08}}</ref> The three components of the bill are as follows:
* Training: The Anna Westin Act of 2015 will help prevent eating disorders by using existing [[National Institute of Mental Health|NIMH]] and [[Substance Abuse and Mental Health Services Administration|SAMHSA]] funds to provide training for health professionals and school personnel to identify eating disorders and intervene early when precursory symptoms and behaviors arise.<ref name=":1" /><ref>{{Cite web|title = Sen. Capito discusses bill to combat eating disorders|url = http://www.wvva.com/story/29664218/2015/07/29/sen-capito-discusses-bill-to-combat-eating-disorders|website = www.wvva.com|access-date = 2016-02-09|first = Lindsay|last = Oliver}}</ref><ref>{{Cite web|title = Anna Westin Act Fights for Education, Treatment of Eating Disorders|url = http://www.kfyrtv.com/home/headlines/KFYR-Anna-Westin-Act-Fights-for-Education-Treatment-of-Eating-Disorders-321477971.html|website = www.kfyrtv.com|access-date = 2016-02-09|first = Kellie Meyer, Washington|last = Correspondent}}</ref>
* Clarity of Mental Health Parity: The Anna Westin Act of 2015 aims to provide better health insurance treatment coverage for those affected by eating disorders.The legislation clarifies the intent of former [http://eatingdisorderscoalition.org.s208556.gridserver.com/couch/uploads/file/Letters_Kennedy.pdf Congressman Jim Ramstad (R-MN) and former Congressman Patrick Kennedy (D-RI)] to include [[Residential treatment center|residential treatment services]] in their past bipartisan legislation, the [https://www.cms.gov/CCIIO/Programs-and-Initiatives/Other-Insurance-Protections/mhpaea_factsheet.html Paul Wellstone and Pete Domenici Mental Health Parity and Addiction Equity Act of 2008] (the Parity Law), which required insurance providers to cover people with mental illness equally as those with other health issues.<ref>{{Cite web|title = mhpaea_factsheet|url = https://www.cms.gov/CCIIO/Programs-and-Initiatives/Other-Insurance-Protections/mhpaea_factsheet.html|website = www.cms.gov|date = 2013-12-13|access-date = 2016-02-08|language = en-us}}</ref><ref name=":3">{{Cite web|title = Kitty Westin, Sen. Klobuchar hope eating disorders bill named for Anna Westin will help others|url = https://www.minnpost.com/mental-health-addiction/2015/07/kitty-westin-sen-klobuchar-hope-eating-disorders-bill-named-anna-wes|website = MinnPost|access-date = 2016-02-09}}</ref>
* Truth in Advertising: The House bill (H.R. 2515) requires the [[Federal Trade Commission]] to conduct a small inter-agency study and report on digitally altered images of humans and fair advertising practices.<ref>{{Cite web|title = Why the Anna Westin Act of 2015 matters|url = http://thehill.com/blogs/congress-blog/healthcare/247438-why-the-anna-westin-act-of-2015-matters|website = TheHill|access-date = 2016-02-09|first = Joe|last = Picard}}</ref>

==History==
[[File:Anna Westin 2.png|thumb|Anna Westin]]The Anna Westin Act of 2015 received its name in observance of 21-year-old Anna Westin.  Anna grew up living with her mom, dad and sisters in the small town of [[Chaska, Minnesota]]. When Anna was 16, she developed anorexia. Anna received outpatient treatment for her eating disorder and Anna and her family thought her treatment was successful. Unfortunately, when Anna returned from college her sophomore year, her parents realized she had relapsed and was suffering from severe [[Anorexia nervosa|anorexia]]. Anna’s family took her to the doctor and Anna was told she needed to be hospitalized. When Anna went to be admitted to the hospital, Anna’s insurer refused to cover inpatient treatment. Shortly thereafter, on  February 17, 2000, Anna died from suicide as a direct result of her battle with anorexia.<ref name=":2" /><ref name=":3" /><ref>{{Cite web|title = Remembering Anna|url = http://www.swnewsmedia.com/chaska_herald/remembering-anna/article_60f3d602-76e5-5a4e-9fb9-af2a7236703e.html|website = SWNewsMedia.com|access-date = 2016-02-09}}</ref>

Kitty Westin, Anna's mother, established Minnesota’s The Anna Westin Foundation (now The Emily Program Foundation) and other resources to help people experiencing eating disorders.<ref>{{cite web | url=http://emilyprogramfoundation.org/ | publisher=The Emily Program Foundation | title=The Emily Program Foundation | accessdate=February 9, 2016}}</ref>

==References==

{{reflist}}

* [http://www.salemnews.com/news/local_news/breaking-the-silence/article_41eb67c9-a0d5-52d4-8bca-ec2e515cb2bd.html "Breaking the silence: Daughter's struggle with anorexia prompts mother's advocacy"]
* [http://kstp.com/article/stories/S3960581.shtml?cat=11854 "Minn. Mother Aims to Help Patients with Eating Disorders Across the Country"]
* [http://www.nbc15.com/home/headlines/New-eating-disorder-legislation-seeks-to-hold-insurance-companies-accountable-337736181.html "New eating disorder legislation seeks to hold insurance companies accountable"]
* [http://www.citypages.com/news/kitty-westin-wants-to-save-parents-from-having-to-bury-a-child-7606267 "Kitty Westin Wants to Save Parents from Having to Bury a Child"]
* [http://www.valleynewslive.com/home/headlines/Anna-Westin-Act-Focuses-on-Education-and-Treatment-of-Eating-Disorders-321475591.html "Anna Westin Act Focuses on Education and Treatment of Eating Disorders"]
* [http://www.startribune.com/parents-of-woman-who-died-while-battling-anorexia-push-for-legislation/321322631/ "Parents of woman who died while battling anorexia push for legislation"]
* [http://www.msnbc.com/andrea-mitchell-reports/watch/eating-disorder-legislation-introduced-494262851934 "Anna Westin Act introduced to combat eating disorders"]

[[Category:Eating disorders]]
[[Category:Proposed legislation of the 114th United States Congress]]