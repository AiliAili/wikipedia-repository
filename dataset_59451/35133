{{Use dmy dates|date=January 2012}}
{{Use Australian English|date=January 2012}}
{{good article}}
{{Infobox storm
| name = 1992 Queensland storms
| image = 
| alt = 
| caption = 
| type = Tornado outbreak
| active = 29 November 1992 
| lowest pressure = 
| lowest temperature =
| tornadoes = Two
| fujitascale = F4
| tornado duration = Less than one hour
| highest winds = 
| hail = 
| gusts =
| maximum snow = 
| power outages =
| casualties =  No fatalities
| damages = Total unknown; millions ([[Australian dollar|A$]]) in [[crop]] damage
| affected = [[Queensland]], [[Australia]]
| current advisories =
| enhanced =
| notes = 
}}
The '''1992 Queensland storms''' were a series of [[thunderstorm]]s which struck southeastern [[Queensland]], [[Australia]] on 29 November 1992. The storms produced strong [[wind]]s, [[flash flood]]ing and large [[hail]]stones in the region, including the capital city of [[Brisbane]]. The storms also spawned two of the most powerful [[tornado]]es recorded in Australia, including the only Australian tornado to be given an official 'F4' classification on the [[Fujita scale]].<ref name="WH9">Whitaker (2005), 199.</ref><ref name="CO9">Coenraads (2005), 229.</ref><ref>{{cite web |title = Tornadoes |url = http://www.bom.gov.au/lam/climate/levelthree/c20thc/storm1.htm |publisher  = Bureau of Meteorology |accessdate = 2008-04-06|archiveurl=http://pandora.nla.gov.au/pan/96122/20090317-1643/www.bom.gov.au/lam/climate/levelthree/c20thc/storm1.html|archivedate=17 March 2009
}}</ref>

The [[Meteorology|meteorological]] instability in the region resulted in the formation of at least five [[supercell]] thunderstorms in the space of around three hours. The storms, which spawned progressively further up the coast from [[Brisbane]] to [[Gladstone, Queensland|Gladstone]] as the afternoon progressed, left a trail of damage resulting from hail, rain and wind. The event has been described as ''"one of the most widespread outbreaks of severe thunderstorms recorded"'' by veteran meteorologist [[Richard Whitaker]].<ref name="DAV">Davies ''et al.'' (1992).</ref><ref name="WH0">Whitaker (2005), 200.</ref>

==Climatology and conditions==
November is traditionally the start of the thunderstorm season along the eastern seaboard of Australia, with a rise in average [[humidity]] and warmer ground [[temperature]]s combining with more frequent occurrences of cool air in the [[upper atmosphere]]. These conditions are conducive for producing severe thunderstorms, particularly those which feature hail.<ref name="DAV"/><ref name="WH5">Whitaker (2005), 195.</ref>

The conditions on Sunday, 29 November were extremely unsettled. There were a series of [[thunderstorm]] [[Storm cell|cells]] that formed early in the morning — despite it being more common for thunderstorms to form in the late afternoon in the south-east Queensland region. These storms, which had periodic bursts of severe [[lightning]], cleared quickly.<ref name="WH5"/><ref name="JAM">Chambers (n.d.).</ref>

Thunderstorms began to form again just before midday, as the hot and humid conditions became more acute in the middle part of the day. The [[Bureau of Meteorology]] [[radar]] picked up a series of cells to the north-west of [[Brisbane]], the capital of Queensland, and the data suggested that there was a possibility of large hail. The Bureau immediately issued a Severe Thunderstorm Warning for the coastal region between Brisbane and the [[Sunshine Coast, Queensland|Sunshine Coast]], 100&nbsp;km to the north.<ref name="CO9"/><ref name="WH6">Whitaker (2005), 196.</ref>

The main cell in the thunderstorm system appeared from [[Bureau of Meteorology]] [[radar]] analysis to split into two separate and distinct cells. This development resulted in one part of the major storm to head north, to [[Maroochydore]], while the other part headed south towards [[Brisbane]].<ref name="JAM"/><ref name="WH6"/> The southern cell struck Brisbane just after 1:00pm, with intense [[lightning]] activity and [[hail]]stones the size of [[marbles]] falling. The storm caused a lengthy delay during the First [[Test cricket|Test]] of the series between [[Australia national cricket team|Australia]] and the [[West Indies cricket team|West Indies]], when hail forced play to be stopped at the [[Brisbane Cricket Ground]] around 1:15pm.<ref name="WH7">Whitaker (2005), 197.</ref>

The northern cell continued to intensify throughout the afternoon. The Bureau of Meteorology then recognised it as a [[supercell]], which often bring erratic developments and often last for long periods of time. The storm dropped hailstones which were between eight and ten centimeters around [[Maroochydore]], on the Sunshine Coast, damaging the roofs of around 80 houses in the area. The hail also damaged aircraft at a local airport and dented cars, as well as inflicting injuries to a handful of [[Human swimming|swimmers]] at beaches near Maroochydore.<ref name="DAV"/><ref name="WH8">Whitaker (2005), 198.</ref>

==Tornadoes==
[[File:Bucca Tornado.jpg|thumb|The tornado near Bucca]]
The extreme instability in this area caused at least three more severe [[supercell]]s in the region. Two separate cells both produced a tornado that were recorded as two of the most powerful in Australian history. The third supercell, which formed just after 3:00pm (immediately after the two tornadoes) near [[Gladstone, Queensland|Gladstone]], produced [[golf ball]]-sized hail that caused [[crop]] damage around Gladstone.<ref name="WH0"/><ref name="CO8">Coenraads (2005), 228.</ref> The total damage to crops from the event was placed in the millions ([[Australian dollar|A$]]).<ref>Bureau of Meteorology (2007).</ref>

===Oakhurst tornado===
Early in the afternoon, another [[supercell]] developed around the town of [[Maryborough, Queensland|Maryborough]], around 300&nbsp;km north of [[Brisbane]]. It developed rapidly also, and at 2:30pm a number of reports sent to the [[Bureau of Meteorology]] reported a [[tornado]] had touched down in [[Oakhurst, Queensland|Oakhurst]], a [[rural]] area 10&nbsp;km west of Maryborough.<ref name="WH9"/> However, due to the low population density in the area the reported damage was sparse, with one house destroyed, several others unroofed and hundreds of trees were snapped.<ref>{{cite web |title = Severe Thunder Storms and Bucca Tornado|url = http://hardenup.org/umbraco/customContent/media/596_Gladstone_Tornado_Bucca_1992.pdf |publisher = Harden Up |author = Callaghan, Jeff| work = Bureau of Meteorology |accessdate = 10 February 2012}}</ref>

Upon investigation and analysis of measurements and the damage caused by the tornado, it was given a rating of 'F3' on the [[Fujita scale]]. This was one of the most powerful tornadoes ever recorded in Australia, and the scale indicated the tornado may have produced winds of between 252 and 300 kilometres per hour.<ref name="DAV"/><ref name="WH0"/>

===Bucca tornado===
{{main|Bucca tornado}}
Only minutes after the Oakhurst tornado, another supercell developed to the south-west of [[Bundaberg]], around 400&nbsp;km north of Brisbane and 150&nbsp;km north of the Oakhurst tornado. It strengthened and moved in a north-east direction, causing severe damage to [[Bullyard]] and [[Bucca, Queensland|Bucca]] areas with giant hailstones, described as the size of a "cricket ball".<ref name="WH9"/>

The supercell then spawned a tornado in the Bucca and [[Shire of Kolan|Kolan]] area. According to reports by meteorologists, the tornado was so strong and the effects caused on the area it hit were so extreme that household appliances were displaced, small objects were embedded in trees and house walls, and "a 3-[[tonne]] truck body was carried 300 metres across the ground". However, as with Oakhurst, the rural nature of the area affected limited the damage caused by the tornado.<ref name="WH0"/><ref name="CO8"/>

Examination by a severe weather team from the [[Bureau of Meteorology]] examined the damage in the Bucca and Kolan region and recorded it as an 'F4' on the [[Fujita scale]]. This corresponds to the tornado being able to produce winds between 331 and 417 kilometres per hour and of 'devastating' intensity. This is the first tornado ever to be recorded as an F4 in Australian history.<ref name="DAV"/><ref name="WH0"/>

==See also==
*[[List of Southern Hemisphere tornadoes and tornado outbreaks]]

==Notes==
{{Reflist|colwidth=30em}}

==References==
*{{cite web
 | title=Severe Thunderstorms: Facts, Warnings and Protection
 | url=http://www.bom.gov.au/info/thunder/
 | author=Bureau of Meteorology
 | author-link=Bureau of Meteorology
 | year=2007
 | accessdate=2007-09-08
| archiveurl= http://webarchive.loc.gov/all/20020223000536/http%3A//www%2Ebom%2Egov%2Eau/info/thunder/| archivedate= 23 February 2002 <!--DASHBot-->| deadurl= no}}
*{{cite web
 | author = Chambers, James
 | title = Severe Thunderstorms with devastating tornadoes over Eastern Qld: 29 November 1992
 | url = http://users.bigpond.net.au/thundery/291192report.html
 | year = n.d.
 | accessdate = 2007-11-21
 | publisher = Thundery: Brisbane Storms
}}
*{{cite book
 | author = Coenraads, Robert
 | title = Natural Disasters And How We Cope
 | year = 2006
 | publisher = The Five Mile Press
 | location = Victoria, Australia
 | isbn = 1-74178-212-0
 | pages = 228–9
}}
*{{cite book
 | author = Davies, Bryan
 | author2 = Oates, Sue
 | title = Severe Thunderstorms and Tornadoes in Southeast Queensland
 | year = 1992
 | publisher = Bureau of Meteorology Severe Weather Section
 | location = Brisbane, Australia
}}
*{{cite book
 | author = Whitaker, Richard
 | title = Australia's Natural Disasters
 | year = 2005
 | publisher = Reed New Holland
 | location = Sydney, Australia
 | isbn = 1-877069-04-3
 | pages = 195–200
 | authorlink = Richard Whitaker
}}

{{1992 tornado outbreaks}}

[[Category:Disasters in Queensland]]
[[Category:Natural disasters in Australia]]
[[Category:Tornadoes in Australia]]
[[Category:Tornadoes of 1992|Queensland storms]]
[[Category:1992 in Australia|Queensland storms]]
[[Category:1992 meteorology|Queensland storms]]
[[Category:1992 natural disasters|Queensland storms]]
[[Category:20th century in Queensland]]