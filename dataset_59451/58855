{{Italic title}}
{{Infobox Journal
| title = Mathematics of Operations Research
| abbreviation  = Math. Oper. Res.
| cover = 
| editor = J. G. "Jim" Dai
| discipline = [[Mathematics]]
| language = English
| publisher = [[Institute for Operations Research and the Management Sciences]] (INFORMS)
| frequency = Quarterly
| history = Feb. 1976-present<ref name=Stats>{{cite web|title=Mathematics of Operations Research: Stats & History|url=http://pubsonline.informs.org/page/moor/stats-history|publisher=INFORMS|accessdate=21 November 2013}}</ref>
| impact = 0.924
| impact-year = 2013
| website = http://www.informs.org/Pubs/MOR
| OCLC = 692909490
| LCCN = 76647217
| CODEN = MOREDQ
| ISSN = 0364-765X
| eISSN = 1526-5471
}}
'''''Mathematics of Operations Research''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] first published in February 1976. It focuses on areas of mathematics relevant to the field of operations research such as [[continuous optimization]], [[discrete optimization]], [[game theory]], [[machine learning]], simulation methodology, and [[stochastic models]]. The journal is published quarterly by [[INFORMS]] (Institute for Operations Research and the Management Sciences), which also publishes other journals including ''[[Operations Research (journal)|Operations Research]]'' and ''[[Management Science (journal)|Management Science]]''. ''Mathematics of Operations Research'' is indexed by the ''[[Journal Citation Reports]]''.<ref>[http://stang.sc.mahidol.ac.th/internal/if2004/pdf/OPER.pdf Journal Citation Reports Year 2004 Science Edition]</ref> In 2014, it moved to "Issues in Advance", which publishes the articles online as they become available. The [[H-index]] of the journal is 50.<ref>"Mathematics of Operations Research". SCIMAGO. </ref>

==History==

The first issue of ''Mathematics of Operations Research'' was published in February 1976. The founding editor was then-professor at Stanford University, Arthur F. Veinott Jr. He then served as editor-in-chief until 1980, when the position was taken over by Stephen M. Robinson, who held the position until 1986. [[Erhan Cinlar]] served from 1987 to 1992, and was followed by [[Jan Karel Lenstra]] (1993-1998). Next was [[Gérard Cornuéjols]] (1999-2003) and [[Nimrod Megiddo]] (2004-2009). Finally came Uri Rothblum (2009-2012) and the current editor-in-chief Jim Dai (2012-present).<ref>“Mathematics of Operations Research: Stats and History”. Informs</ref> 

The journal's three initial areas were [[game theory]], stochastic systems, and mathematical programming, edited by [[Robert Aumann]], [[Donald Iglehart]], and R. Rockafellar, respectively.<ref>"Front Matter." Mathematics of Operations Research 1.1 (1976): n. pag. JSTOR. Web. 27 Apr. 2015.</ref> Currently, the journal has four areas: [[continuous optimization]], [[discrete optimization]], [[stochastic models]], and game theory. They are edited by [[Marc Teboulle]], [[Jens Vygen]], [[Adam Shwartz]], and [[Bernhard von Stengel]], respectively.<ref>"Front Matter." Mathematics of Operations Research 40.2 (2015): n. pag. JSTOR. Web. 30 Apr. 2015.</ref>

''Mathematics of Operations Research'' has hosted articles written by four Nobel Prize-winning authors: Robert Aumann (Economics, 2005), Roger B. Myerson (Economics, 2007), Alvin E. Roth (Economics, 2012), and Lloyd S. Shapley (Economics 2012).<ref>“Mathematics of Operations Research: Stats and History”.</ref>

==Notable papers==

The following papers have been cited most frequently:<ref>“Mathematics of Operations Research: Most Cited Articles”. Informs</ref>

1. [[Roger B. Myerson]], “[http://pubsonline.informs.org/doi/abs/10.1287/moor.6.1.58 Optimal Auction Design]”, Mathematics of Operations Research 1981, vol 6:1 , 58-73

2. A. Ben-Tal and [[Arkadi Nemirovski]], “[http://pubsonline.informs.org/doi/abs/10.1287/moor.23.4.769 Robust Convex Optimization]”, Mathematics of Operations Research 1998, vol 23:4 , 769-805

3. M. R. Garey, D. S. Johnson, and Ravi Sethi, “[http://pubsonline.informs.org/doi/abs/10.1287/moor.1.2.117 The Complexity of Flowshop and Jobshop Scheduling]”, Mathematics of Operations Research 1976, vol 1:2 , 117-129

== References ==
{{reflist}}

==External links==
* {{Official website|http://www.informs.org/Pubs/MOR}}

[[Category:Publications established in 1976]]
[[Category:Game theory]]
[[Category:Operations research]]
[[Category:Mathematical optimization]]
[[Category:Systems journals]]
[[Category:Mathematics journals]]
[[Category:INFORMS academic journals]]


{{mathematics-journal-stub}}