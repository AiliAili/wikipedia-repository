{{Other uses|SAB (disambiguation)}}

{{Primary sources|date=July 2006}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Sab
| title_orig    = 
| translator    = 
| image         = 
| caption = 
| author        = [[Gertrudis Gomez de Avellaneda]]
| illustrator   = 
| cover_artist  = 
| country       = [[Cuba]]
| language      = [[Spanish language|Spanish]]
| series        = 
| genre         = 
| publisher     = 
| release_date  = 1841
| english_release_date =
| media_type    = 
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}
'''''Sab''''' is a novel written by [[Gertrudis Gomez de Avellaneda]] in 1841 and published in Madrid. In the story, Sab, a [[mulato]] slave—who is in love with Carlota, the white daughter of his master—is the main character. The pain and struggle of his secret passion for Carlota leads Sab to his own death, which occurs in the same hour as Carlota's wedding with Enrique Otway.  The novel was first published in Cuba in 1914.

''Sab'' is regarded by some scholars as an antislavery novel, and some have also suggested that it criticizes the institution of [[marriage]]. {{Citation needed|date=January 2010}} The novel was written a decade before [[Harriet Beecher Stowe]]'s ''[[Uncle Tom's Cabin]]''. The publishing of ''Sab'', in effect, was a turning point in being a precursor to the antislavery movements. {{Citation needed|date=January 2010}}Thus, the novel was not freely published in [[Cuba]] until 1914.

According to the Spanish literature professor Catherine Davies, ''Sab'' is "the only feminist-abolitionist novel published by a woman in nineteenth-century Spain or its slaveholding colony Cuba."{{sfn|Davies|2001|p=1}}

==Plot==
The novel is set on a sugar plantation located halfway between the city of Santa María de Puerto Príncipe (modern-day Camagüey) and the village of Cubitas. While most of the novel takes places of the plantation, some of it takes place in Puerto Príncipe, some in the Cubitas Mountains, and some in the northern port of Guanaja.{{sfn|Davies|2001|p=2}}

==Background==
Although ''Sab'' was initially published in Madrid in December 1841,{{sfn|Davies|2001|p=202}} Avellaneda began writing the book in Cuba and continued working on it during the two-month journey to Europe in 1836.{{sfn|Davies|2001|p=2}}

==Notes==
{{reflist|2}}

==References==
{{refbegin}}
* {{cite book |last=Davies |first=Catherine |year=2001 |chapter=Introduction |title=Sab |series=Hispanic Texts |language=English |publisher=Manchester University Press |isbn=978-0719057069 |ref=harv}}
{{refend}} 

==External links==
*[http://www.cervantesvirtual.com/servlet/SirveObras/04700625355736673132268/p0000001.htm#1 ''Sab'': Online Edition (Spanish)]

{{Slave narrative}}

[[Category:1841 novels]]
[[Category:Cuban novels]]
[[Category:Novels about slavery]]


{{1840s-novel-stub}}