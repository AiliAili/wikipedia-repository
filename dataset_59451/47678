{{Orphan|date=March 2016}}

{{Infobox musical artist
| honorific_prefix    = 
| name                = Tamar Eisenman
| honorific_suffix    = 
| image               = [[File:071212-tamar eisenman.jpg|center|250px]]
| image_size          = 
| landscape           = <!-- yes, if wide image, otherwise leave blank -->
| alt                 = 
| caption             = 
| background          = solo_singer
| native_name         = 
| native_name_lang    = 
| birth_name          = 
| alias               = 
| birth_date          = {{birth date and age|1980|7|2}}
| birth_place         =[[Jerusalem]] 
| origin              = 
| death_date          = <!-- {{death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date 1st) -->
| death_place         = 
| genre               = [[Folk Music|Folk]], [[Rock Music|rock]], [[Blues Music|blues]], [[Pop Music|pop]]
| occupation          = [[Singer]], [[songwriter]], [[guitarist]], producer
| instrument          = [[Guitar]], vocals, bass guitar
| years_active        = 2001-present
| label               = [[Zipi's Baby]], [[NMC Music]], [[Odacity]] 
| associated_acts     = 
| website             =  {{URL|www.eisenwoman.com}}
| notable_instruments = 
| module              = 
| module2             = 
| module3             = 
}}
'''Tamar Eisenman''' (born July 2, 1980) is a [[blues]]-folk-rock guitarist, singer songwriter and producer, born in [[Jerusalem]].

Eisenman launched her musical career in 2003 in Israel with the release of her self-titled EP "Tamar Eisenman EP". She later released studio albums.<ref>[http://www.allmusic.com/album/tamar-eisenman-mw0001091131 studio albums Allmusic:Tamar Eisenman]</ref>  - "5 feet 4 , Gymnasium",<ref>[http://www.haaretz.com/print-edition/features/why-do-israeli-singers-sing-in-english-1.280434 Why do Israeli Singers Sing in English?]</ref> "Time for Creation",<ref>[http://www.haaretz.com/israel-news/israeli-singer-tamar-eisenman-takes-some-time-for-creation-1.377126 Israeli Singer Tamar Eisenman Takes Some Time for Creation]</ref> and "Limbo".<ref>[http://e.walla.co.il/item/2760009 Odacity]</ref><ref>[http://www.frontview-magazine.be/en/news/new-single-for-tamar-eisenman#.Vlxbn4Rllm8 Frontview Magazine: New Single for Tamar Eisenman]</ref> In 2010 Tamar opened for [[Jeff Beck]]<ref>http://www.mouse.co.il/CM.articles_item,636,209,55024,.aspx</ref> in Caesarea amphitheater and initiated several new projects as [[guitarist]] and producer. Tamar also recorded and performed with: [[Danny Sanderson]],<ref>[http://www.haaretz.com/israel-news/the-kaveret-kid-turns-60-1.329868 The Kaveret Kid Turns 60]</ref> [[Asaf Avidan]],<ref>[http://www.asafavidanmusic.com/discography/# Asaf Avidan]</ref> [[Miki Gavrielov]], [[David Broza]]<ref>[http://www.mako.co.il/music-Magazine/reviews/Article-5a68cdb1bbd3241006.htm David Broza]</ref> and [[Kaki King]].<ref>[http://www.jpost.com/Arts-and-Culture/Music/Royal-strings Kaki King]</ref> Tamar is mainly known for her  [[acoustic guitar]]<ref>[http://3songsbonn.com/2015/04/03/jewel-of-jerusalem-tamar-eisenman Acoustic]</ref><ref>[http://www.harmonie-bonn.de/portals/0/Veranstaltungen2015/04-2015//GA-15-04-04.pdf acoustic guitar]</ref> hand picking [[electric guitar]]<ref>[http://www.nrg.co.il/online/47/ART1/887/093.html electric guitar]</ref> sound integrated with her bluesy pop songwriting.

==Early life==

Tamar was born in Jerusalem to Tzipora and Ya'akov Eisenmann. She graduated from ''Hanisuee'', the experimental open school in Jerusalem, and spent part of her childhood with her family in [[San Francisco]] where she started playing the guitar at the age of six.<ref>[http://www.midnighteast.com/mag/?p=30376 Tamar Eisenman on Music, Language and Limbo]</ref> At the age of twelve, Tamar started  taking guitar lessons with Aharoni Ben Ari<ref>[http://bidur.nana10.co.il/Article/?ArticleID=1110753 תמר אייזנמן: "הקהל בישראל זה בית"]</ref> focusing on [[Blues]] and [[Jazz]]. A few years later she started writing her first songs and playing with local bands as well as participated in the project for young bands of Jerusalem at the ''Yellow Submarine''<ref>[http://yellowsubmarine.org.il/?show=צהריים-בצוללת-עם-פרויקט-ההרכבים fצהריים בצוללת עם: פרויקט ההרכבים]</ref>

==Musical career==

At the age of twenty Tamar started performing with other musicians<ref>http://www.habama.co.il/Pages/Description.aspx?Subj=1&Area=1&ArticleID=542</ref> as a lead guitarist and musical producer. At the same time, she released her self-produced EP in 2003, established her trio band and began performing with her own repertoire.<ref>[http://mooma.mako.co.il/Article.asp?GroupID=1915&itemID=66572&ArtistID=21173 abroad, has a show premiere with distinguished guests	]</ref> In 2005, Tamar released her full-length album, "5 feet 4" – her height with shoes - Including 15 songs written and composed by her and produced by [[Amir Ben Ami]] ([[Hadag Nachash]]). In 2006, Tamar joined [[Danny Sanderson]]’s band as guitarist and vocalist. A year later she moved to Tel Aviv and joined ''The Tonight Band''<ref>http://www.ishim.co.il/p.php?s=תמר+אייזנמן</ref> of [[Lior Schleien]]’s late night TV Tonight ("Ha Layla"), an Israeli version of The Tonight Show, which won the Israeli Academy Award for Best Talk Show.{{citation needed|date=December 2015}}

In 2009 after signing to [[NMC Music]]<ref>http://www.yosmusic.com/yos5510/</ref> Tamar released her first single from ''Gymnasium'', her 3rd album. "Hit Me"<ref>http://www.ynet.co.il/articles/0,7340,L-3723649,00.html</ref> was her first national radio hit. Following her second radio hit ''Sun'',<ref>[http://www.ynet.co.il/articles/0,7340,L-3777293,00.html]</ref> Tamar released her 3rd album and toured constantly across Israel and in venues and festivals around the world. [[CMJ]] festival<ref>https://www.cmj.com/tag/tamar-eisenman/</ref> (NY) [[Sappho]] festival<ref>[http://www.ynet.co.il/articles/0,7340,L-4239672,00.html Sappho festival]</ref> (Greece), Bam Cafe (NY), [[The Water Rats]] ([[London]]) and was chosen to open for [[Jeff Beck]] in Caesarea.{{citation needed|date=December 2015}}

In 2011, Tamar released her 4th album "Time for Creation", 12 tracks  mixed by [[Tamir Muskat]] – as she expanded the trio with a brass section.
Later that year Tamar musically produced a tribute concert<ref>[http://www.jpost.com/Arts-and-Culture/Music/Concert-review-Doing-Dylan-proud Doing Dylan Proud]</ref> celebrating Bob Dylan 70th birthday in Tel Aviv.
In August 2015 Tamar released her latest album "Limbo".<ref>http://www.haaretz.co.il/gallery/music/musicreview/.premium-1.2693923</ref><ref>http://andywaswrong.com/tamar-eisenman-limbo/</ref> Her first full album in Hebrew which included songs written by [[Yankale Rotblit]] and [[David Avidan]] and a tribute Hebrew duet version to the blues song [[The Thrill Is Gone]] ."Tam" was recorded with musician and singer [[Rona Kenan]].
At the end of 2015 Tamar moved to New York.

==Discography==
'''Studio Albums'''
* 2015- Limbo/לימבו (Zipis Baby, [[NMC Music]])
*2011- Time for Creation (Zipis Baby, [[NMC Music]])
*2009- Gymnasium (Zipis Baby, [[NMC Music]])
*2005- 5 Feet 4 (Zipis Baby, The Eighth note)
*2003- Tamar Eisenmann EP (Zipi's Baby)

'''Additional Projects and collaborations'''
*2015- ''Mama Blues, J-Town girl, Give Me Words'' / "Rage against The Eclipse", Hadara Levin Areddy (guitars and music production)
*2013- Puzzle, Ronen Green (Guitars, vocals, music production)
* 2012- בלדה לאישה Israeli women singers anthology 1940-2011 ( "''2 Step Dance''")
*2012- מסע בלוז איתי פרל Itay Pearl (Guitar) 
*2012- Sunrise,<ref>http://www.allaboutjazz.com/sunrise-amit-friedman-origin-records-review-by-dan-mcclenaghan.php|Sunrise</ref> Amit Friedman Sextet (Vocals)
*2010- לא חברה , Movie by Sivan ben Ari. Soundtrack
*2009- Lo Yafrid Davar/לא יפריד דבר [[Danny Sanderson]] (Guitars, vocals)
*2008- משחקי פחד [[Ido Mosseri]] (Guitars)
*2008-  סוג של ורוד [[Tamar Giladi]] (Guitars)
*2008- [[The Reckoning (Asaf Avidan & the Mojos album)|The Reckoning]], [[Asaf Avidan]]
*2007- שאנן סטריט, הבזק אור חולף (Vocals)
*2007- Rock Rimon,<ref>http://www.ishim.co.il/m.php?s=רוקרימון</ref> ''84 Steps'' theme song for TV show 
*2007- Stefan Braun\סטפן בראון ,Movie by Itamar Alkalay. Soundtrack (Guitars, programming)
*2007- חוטים, [[Ronit Rolland]] (Guitars and co- arrangements/musical producer)
*2006- Now that You're leaving" Ep, Asaf Avidan ( Guitars, bass, vocals, co- arrangements)
*2002- Magic Time, Hadara Levin Areddy ( Guitars)
*2002- King O, [[Hadara Levin Areddy]] (Guitars)
*2001- Live at Tmuna Theater, Hadara Levin Areddy (Guitars)
*2001- This Is a True Story/ The Rough Cut, Hadara Levin Areddy (Guitars)

==References==
{{reflist}}

==External links==
*{{Official website|http://www.eisenwoman.com}}

{{Authority control}}

{{DEFAULTSORT:Eisenman, Tamar}}
[[Category:1980 births]]
[[Category:Living people]]
[[Category:Israeli guitarists]]
[[Category:Israeli singer-songwriters]]