{{Infobox recurring event
| name         = <!--Uses page name if omitted-->
| native_name  =
| logo         = [[File:Pacific_Symposium_on_Biocomputing_logo.gif]]
| logo_caption =
| image        =
| imagesize    =
| caption      =
| date         =
| begins       =
| ends         =
| prev         = PSB 2015
| next         = PSB 2016
| frequency    = Annually
| location     = [[Hawaii]], United States
| years_active = {{age|1996|01|03}}<ref name=1996-schedule>{{cite web|title=PSB 1996 Conference Schedule|url=http://psb.stanford.edu/previous/psb96/schedule.html|accessdate=9 February 2015}}</ref>
| first        =
| last         =
| participants =
| attendance   =
| genre        =
| budget       =
| patron       =
| organised    = Tiffany Murray  (2015&nbsp;coordinator)
| people       =
| member       =
| website      = {{URL|http://psb.stanford.edu/}}
| footnotes    =
}}
The '''Pacific Symposium on Biocomputing''' (PSB) is a multidisciplinary [[scientific meeting]] held annually since 1996. The conference is to presentation and discuss research in the theory and application of computational methods for biology. Papers and presentations are [[peer review]]ed and published.<ref name=proc>{{cite web|title=PSB Proceedings|url=http://psb.stanford.edu/psb-online/|accessdate=9 February 2015}}</ref>

PSB brings together researchers from the [[United States|US]], the [[Asia-Pacific|Asian Pacific]] nations, to exchange research results and address open issues in all aspects of [[computational biology]]. PSB is a forum for the presentation of work in databases, algorithms, interfaces, visualization, modeling, and other computational methods, as applied to biological problems, with emphasis on applications in data-rich areas of [[molecular biology]].

The PSB aims for "[[critical mass (sociodynamics)|critical mass]]" in sub-disciplines within [[Bioinformatics|biocomputing]]. For that reason, it is the only meeting whose sessions are defined dynamically each year in response to specific proposals. PSB sessions are organized by leaders in the emerging areas and targeted to provide a forum for publication and discussion of [[research]] in biocomputing's topics.

==References==
{{reflist}}

==External links==
[http://psb.stanford.edu Pacific Symposium on Biocomputing web site]

[[Category:Biology conferences]]
[[Category:Computer science conferences]]