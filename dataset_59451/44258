{{Infobox airport
| name         = Pyramiden Heliport
| nativename   = 
| image        =Mil_Mi-8_at_Pyramiden_helipad.jpg
| image-width  = 300px
| caption      = 
| IATA         =
| ICAO         = ENPY
| type         = Private
| owner        = 
| operator     = [[Arktikugol]]
| city-served  = [[Pyramiden]], [[Svalbard]], [[Norway]]
| location     = 
| metric-elev  = yes
| elevation-f  = 6
| elevation-m  = 2
| website      = 
| coordinates  = {{coord|78|41|N|16|24|E|region:NO|display=inline,title}}
| pushpin_map            = Svalbard
| pushpin_label          = '''ENPY'''
| pushpin_label_position = bottom
| pushpin_map_caption    = Location within [[Svalbard]]
| metric-rwy   = yes
| h1-number    = 
| h1-length-m  = 90
| h1-length-f  = 295
| h1-surface   = Gravel
}}

'''Pyramiden Heliport''' ({{lang-no|Pyramiden helikopterhavn}}; {{airport codes||ENPY|p=n}}) is a [[heliport]] located at [[Pyramiden]] in [[Svalbard]], [[Norway]]. The airport is owned and operated by [[Arktikugol]], who owns the mining town. The [[airport]] consists of a gravel [[runway]] and apron measuring {{convert|90|by|40|m|sp=us}} and a small terminal building. There is capacity for up to three helicopters on the apron. Flights are carried out by [[Spark+]] using two [[Mil Mi-8]] helicopters. Flights are flown to [[Barentsburg Heliport, Heerodden]] and [[Svalbard Airport, Longyear]] at irregular intervals.

The airport opened in 1961 to allow [[Aeroflot]] to commence flights between Barentsburg and Pyramiden using [[Mil Mi-4]] airport. An upgrade was carried out in the late 1970s after the airport in Longyearbyen opened. By then five [[Mil Mi-8]] were stationed on Svalbard. A crash during landing on 27 March 1991 killed two people. Flights were reduced during the 1990s and from 1998 Pyramiden was abandoned, reducing use of the heliport to a minimum.

==History==
Arktikugol commenced flights on Svalbard in 1961, at first operating a shuttle service between their two remaining mining towns, Barentsburg and Pyramiden. Barentsburg received a larger facility and was the base of operations. Flights were initially carried out by Aeroflot using two Mil Mi-4 helicopters with a capacity for eleven passengers.<ref name="Risanger: 319">Risanger: 319</ref> The ''Aviation Act'' applies to Svalbard and from 1961 to 1974 Arktikugol followed this by applying for and receiving a helicopter operating concession from the [[Ministry of Transport and Communications (Norway)|Ministry of Transport and Communications]]. After 1974 the Soviet Union stated that the regulations were in violation of the [[Svalbard Treaty]] allowing free shipping.<ref>Risanger: 317</ref>

[[File:Piramida west livestock buildings IMG 7293.JPG|thumb|left|Location of the airport in relation to the town]]
The Soviet Union agreed in 1971 to allow the construction of Svalbard Airport, Longyear. The condition was that the airport be built with capacity to allow Aeroflot to operate flights to [[Moscow]]. This would again increase the need for the heliport in Pyramiden, as it would be used for fly passengers from Barentsburg to the Longyearbyen. Svalbard Airport, Longyear opened in 1975.<ref name=r260>Risanger: 260</ref> In 1977 the [[Avinor|Civil Aviation Administration]] installed seven navigational lights around [[Billefjorden]], aiding navigation through the [[polar night]]. This was met with some controversy, as one of the lights was placed in [[Gåsøyane Bird Sanctuary]].<ref>Risanger: 330</ref> Arktikugol increased its fleet to five Mil Mi-8 helicopters, all with Aeroflot markings.<ref>Risanger: 316</ref> Each helicopter has a capacity for 28 passengers and a range of {{convert|375|km|sp=us}}.<ref name="Risanger: 319"/>

[[File:Spark+ Mi-8 at Pyramiden.jpg|thumb|[[Mil Mi-8]] operated by [[Spark+]] for [[Arktikugol]]]]
Following the [[dissolution of the Soviet Union]] in 1991, subsidies and resources allocated to Svalbard and Arktikugol were diminished. By 1993 there were only two remaining helicopters, and all crew and airport employees were relocated to live in Barentsburg.<ref>Holm: 18</ref> During the early 1990s, there were about 40 to 60 aircraft movements per month at Pyramiden, with higher frequency during the summer.<ref>Accident Investigation Board Norway (1991): 23</ref> Operation were reduced from 1998 when Arktikugol closed Pyramiden.<ref>Holm: 108</ref> However, Arktikugol retains an operational heliport and continues irregular flights to Pyramiden.<ref name=a4>Accident Investigation Board Norway (2013): 4</ref>

==Facilities==
The heliport is located on the southwestern part of Pyramiden. Situated at an elevation of {{convert|2|m|sp=us}} [[above mean sea level]], it is about {{convert|200|m|sp=us}} from Billefjorden. The airport features a gravel runway and apron measuring {{convert|90|by|40|m|sp=us}}. This allows for two helicopters to land during winter and three during winter. There is a small terminal building which also acts as a control tower. It is manned during flight operations by a representative for the airline and provides [[aerodrome flight information service]]. The terminal building has capacity for both crew and passengers. The heliport is lit to allow for night flights.<ref>Accident Investigation Board Norway (1991): 12</ref>

==Airlines and destinations==
[[File:Spark+ Mil Mi-8 at Pyramiden.jpg|thumb|left|[[Mil Mi-8]] operated by [[Spark+]] for [[Arktikugol]]]]
Arktikugol has two Mil Mi-8 aircraft which are operated by [[Spark+]].<ref name=a14>Accident Investigation Board Norway (2013): 14</ref> They are based at Barentsburg and provide transport services for Arktikugol and the [[Consulate-General of Russia in Barentsburg]] for flights to Barentsburg and to Longyearbyen.<ref name=a4 />

==Accidents and incidents==
On 27 March 1991 an Aeroflot Mi-8 helicopter crashed Mimerbukta, {{convert|800|m|sp=us}} from the airport.<ref>Accident Investigation Board Norway (1991): 1</ref> The helicopter was en route from Longyearbyen to Pyramiden with a crew of three and no passengers with the intent of flying employees to catch an Aeroflot flight to Moscow.<ref>Accident Investigation Board Norway (1991): 2</ref> The pilot lost visual references during the landing, which was carried out during difficult weather conditions.<ref>Accident Investigation Board Norway (1991): 3</ref> Two of the three people on board were killed.<ref>Accident Investigation Board Norway (1991): 5</ref>

{{clear}}

==References==
{{reflist|30em}}

==Bibliography==
{{commons category|Pyramiden Heliport}}
* {{cite web |author=[[Accident Investigation Board Norway]] |title=Rapport om luftfartsulykke den 27. mars 1991 på isen i Mimerbukta 800 m sydøst for landingsplassen ved Pyramiden Svalbard, med helikopter Aeroflot 06155 |url=http://www.aibn.no/rapport-06-1991-pdf?pid=Native-ContentFile-File&attach=1 |year=1991 |language=Norwegian |accessdate=7 April 2014}}
* {{cite web |author=[[Accident Investigation Board Norway]] |title=Report concerning aviation accident on the Cape Heer Heliport, Svalbard, Norway, 30 March 2008 with Mil Mi-8MT, RA-06152, operated by Spark+ Airline Ltd.|url=http://www.aibn.no/Luftfart/Rapporter/2013-06-eng?pid=SHT-Report-ReportFile&attach=1 |year=2013 |accessdate=7 April 2014}}
* {{cite book |last=Holm |first=Kari |title=Longyearbyen – Svalbard: historisk veiviser |year=1999 |url=http://urn.nb.no/URN:NBN:no-nb_digibok_2008090100031 |language=Norwegian |isbn=82-992142-4-6}}
* {{cite book |last=Risanger |first=Otto |title=Russerne på Svalbard |url=http://www.nb.no/nbsok/nb/b733fa7e240161a56203b2d75c3246a0 |location=Longyearbyen |publisher=Sampress |year=1978 |isbn=82-90210-03-5 |language=Norwegian}}

{{Airports in Norway}}
{{Svalbard}}
{{Portal bar|Aviation|Arctic|Norway|Russia|Soviet Union}}

[[Category:Airports in Svalbard]]
[[Category:Heliports in Norway]]
[[Category:Pyramiden]]
[[Category:Airports established in 1961]]
[[Category:1961 establishments in Norway]]
[[Category:Russian airport stubs]]