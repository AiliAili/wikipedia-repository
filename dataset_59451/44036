{|{{Infobox Aircraft Begin
 |name= ASh-73 
 |image = 
 |caption = 
}}
{{Infobox Aircraft Engine
 |type= [[Radial engine]]
 |national origin = [[Soviet Union]]
 |manufacturer= [[Shvetsov]] OKB-19 in Perm'
 |first run= 1946
 |major applications=[[Tupolev Tu-4]], [[Beriev Be-6]]  
 |number built = 14,310
 |program cost = 
 |unit cost = 
 |developed from = [[Shvetsov M-71]]<br>[[Shvetsov M-72]]
 |developed into = 
 |variants with their own articles = 
}}
|}

The '''Shvetsov ASh-73''' was an 18-cylinder, air-cooled, radial [[aircraft engine]] produced between 1947 and 1957 in the [[Soviet Union]]. It was primarily used as the powerplant for the [[Tupolev Tu-4]] heavy bomber, a copy of the American [[Boeing]] [[B-29 Superfortress]].

==Design and development==
The Shvetsov ASh-73 originated in 1938 from a specification for an 18-cylinder, twin-row, development of the [[Shvetsov M-25]], a license-built 9-cylinder, air-cooled, radial [[Wright R-1820|Wright R-1820-F3]] Cyclone engine. Development continued through a series of less than successful engines, before culminating in the ASh-73. Contrary to popular belief the ASh-73 wasn't a reverse engineered copy of the [[Wright R-3350]] Duplex Cyclone: "There was no need to copy the Wright R-3350-23A; the engine that was put into production was the indigenous ASh-73TK - a further development of the M-71 and M-72, which differed in being fitted with twin TK-19 turbosuperchargers (TK = ''toorbokompressor'')." <ref name=g1>Gordon and Rigmant, p. 21</ref> rather the ASh-73 was the product of a similar specification. Since the earlier M-25 engines were a licensed copy of the Wright R-1820, there were similarities and some parts were interchangeable between the Duplex Cyclone and the ASh-73 powerplants. The two engines evolved from a common ancestor and to a similar requirement. "In the late 1930s and the early 1940s OKB-19 evolved two 18-cylinder two-row radials — the 2,000-hp M-71 and the 2,250-hp M-72 — which were similar in their design features and production techniques to the Wright Duplex Cyclone engines powering the B-29."<ref name=g1/>

The progenitor for the ASh-73 was the M-70. It was tested in late 1938 and was a failure because of cracks in the master [[connecting rod]] and the geared [[centrifugal supercharger]]'s impeller. The exhaust valves also burnt through. The M-71 of 1939 was the successor to the M-70 and it too was not a success. It used some components from the [[Shvetsov ASh-62|M-62]] engine, but its development was slowed by the German attack on the Soviet Union in 1941. It passed its State acceptance tests in the autumn of 1942, but was not placed into production as there was not any production capacity available, although it was tested on a number of different prototypes during the war. The M-72 of early 1945 was a boosted version of the M-71 and was superseded by the ASh-73 before production could get underway.<ref name=k9>Kotelnikov, pp. 129–30</ref>

The first prototypes of the ASh-73 were built in 1945 and by the end of 1946 testing had completed successfully. The first models to enter production in 1947 lacked turbo-superchargers. They weighed {{convert|1330|kg|lb|abbr=on}} and had {{convert|2400|hp|kW|abbr=on}} during take-off. The ASh-73TK had two TK-19 [[turbocharger]]s and an [[intercooler]] fitted which were direct copies of the units used on the R-3350. The engine was upgraded over the course of its production. On the fourth series of engines the crankshaft nose was changed, the articulated connecting rods were strengthened and the accessory drive was changed. The middle part of the crankcase and the pistons were strengthened and the ignition was improved in the fifth series. In the sixth series the master connecting rod and the crankshaft cheeks were strengthened, the pistons were lightened and shortened. For the seventh series exhaust valves with floating seats were introduced and the reduction gearing was improved.<ref name=k0/>

A boosted version was developed as the ASh-82TKF that had a rating of {{convert|2720|hp|kW|abbr=on}}. It was bench tested, but not put into production. A further development in 1949 was the ASh-73TKFN with fuel injection that boosted power to {{convert|2800|hp|kW|abbr=on}}, but it too was not built. Another 1949 project was an ASh-73TK with a power-recovery turbine to create a [[turbo-compound engine]], but no other information is known.<ref name=k0>Kay, p. 130</ref>

Factory No. 19 began preparation to build the ASh-73 in 1946, but production did not begin until the next year. Production continued there until 1953. Factory No. 36 in Rybinsk also produced it until 1957. A total of 14,310 ASh-73s were built. A number of these were exported to the [[People's Republic of China]] during the 1950s as spare parts for their Tu-4s.<ref name=k0/>

==Applications==
* [[Beriev Be-6]]
* [[Ilyushin Il-18 (1947)]]
* [[Petlyakov Pe-8]] (in service with [[Aeroflot]], post-war)
* [[Tupolev Tu-4]]
* [[Tupolev Tu-70]]
* [[Tupolev Tu-75]]
* [[Tupolev Tu-80]]

==Specifications (Shvetsov ASh-73TK)==
[[File:Shvetsov.jpg|thumb|Family tree of Shvetsov engines]]
{{pistonspecs
|ref=Gordon and Rigmant, ''Tupolev Tu-4: Soviet Superfortress''
|type=18-cylinder two-row radial engine.
|bore={{convert|155.58|mm|in|abbr=on}}
|stroke={{convert|169.86|mm|in|abbr=on}} 
|displacement=58.122 Liters (3,546.8 cu in)
|length={{convert|2.29|m|ftin|abbr=on}}
|diameter={{convert|1.37|m|ftin|abbr=on}}
|width=
|height=
|weight=1,339 kg (2,951 Ib)
|valvetrain=Pushrod, two valves per cylinder with sodium-cooled exhaust valve.
|supercharger=Two-stage supercharging system with intercooler. First stage consisted of two TK-19 exhaust-driven turbochargers (''toorbokompressor'') operating in parallel (two speeds automatically controlled by an electronic governor). Second stage consisted of a PTsN single-speed engine-driven centrifugal blower (''privodnoytsentrobezhnw nagnetahtel''). Boost was limited to 1.5 Atm with low octane fuel.
|turbocharger=
|fuelsystem=Carburettors with automatic mixture control (last version with direct fuel injection).
|fueltype=92/93 octane fuel, 95 or 100 octane.
|oilsystem=
|coolingsystem=Air-cooled
|power=<br>
*'''With direct fuel injection:'''
*{{convert|2720|hp|kW|abbr=on}} at 2,600 RPM for take-off (Dry)
*{{convert|2360|hp|kW|abbr=on}} at 2,400 RPM maximum continuous
*'''With Carburettors and 92/93 octane fuel:'''
*{{convert|2400|hp|kW|abbr=on}} at 2,600 RPM for take-off (Dry), boost rated at 1.5 Atm. (43.8", 7.35psi)
*{{convert|2200|hp|kW|abbr=on}} at 2,400 RPM at critical altitude of {{convert|8500|m|abbr=on}}
*{{convert|2000|hp|kW|abbr=on}} at 2,400 RPM at {{convert|9000|m|abbr=on}}
|specpower=30.8 kW/l (0.676 hp/in³)
|compression=6.9:1
|fuelcon=
|specfuelcon=350 g/hp•h (0.77 Ib/hp•hr) for take-off, 315-335 g/hp•hr (0.69-0.74 Ib/hp•hr) at nominal power (carburettor version).
|oilcon=
|power/weight=
|reduction_gear= 0.375:1}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* {{cite book |last=Kotelnikov  |first=Vladimir |title=Russian Piston Aero Engines|year=2005 |publisher=Crowood Press|location=Ramsbury, Marlborough|ISBN=1-86126-702-9}}
* {{cite book |last=Yefim Gordon  |first=Vladimir Rigmant |title=Tupolev Tu-4, Soviet Superfortress|year=2002|series=Red Star|volume=7 |publisher=Midland Publishing|ISBN= 1-85780-142-3|location=Hinckley, England}}
{{refend}}

{{Shvetsov aeroengines}}

{{DEFAULTSORT:Shvetsov Ash-73}}
[[Category:Aircraft air-cooled radial piston engines]]
[[Category:Shvetsov aircraft engines]]
[[Category:Aircraft piston engines 1940–1949]]