{{Orphan|date=January 2015}}

'''Terese Coe''' is an American writer, translator, and dramatist.

== Background ==

Terese Coe was born in New York, NY. She received a B.A. in English with a minor in comparative literature from the City College of New York and an M.A. in dramatic literature from the University of Utah.<ref name="wordtech">[http://wordpoetrybooks.com/coe.html Terese Coe bio and book description, Wordtech Communications (publisher)]</ref> Her poems and translations have appeared in US journals including ''[[32 Poems]]'',<ref name="32poems">[http://www.32poems.com/issues 32 Poems]</ref> ''[[Able Muse]]'', ''[[Alaska Quarterly Review]]'',<ref name="newpages">{{cite web|last1=McKenna|first1=Brian|title=Review of the Alaska Quarterly Review Vol. 31|url=http://www.newpages.com/literary-magazine-reviews/2014-05-15/|website=The New Pages}}</ref> ''American Arts Quarterly'',<ref name="aaq">[http://www.nccsc.net/archives/poetry/poems/tompkins-square American Arts Quarterly]</ref> ''The Cincinnati Review'',<ref name="cr">[http://www.cincinnatireview.com/blog/tag/terese-coe/ The Cincinnati Review]</ref> ''The Connecticut Review'', ''[[The Huffington Post]]'',<ref name="huffpost">[http://www.huffingtonpost.com/terese-coe/ The Huffington Post]</ref> ''Measure'', ''[[New American Writing]]'',<ref name="naw">[http://www.newamericanwriting.com/25/borges.htm New American Writing]</ref> ''[[Ploughshares]]'',<ref name="pshares">[http://www.pshares.org/read/author-detail.cfm?intAuthorID=7278 Ploughshares]</ref> ''[[Poetry (magazine)|Poetry]]'',<ref name="poetry">[http://www.poetryfoundation.org/poetrymagazine/browse/185/6#!/20606941/0 Poetry Foundation]</ref> ''[[Smartish Pace]]'', ''The Shakespeare Newsletter'', ''[[Stone Canoe]]'',<ref name="stonecanoe">[http://www.stonecanoejournal.org/SC6OnlineContents/SC6_coe.pdf Stone Canoe]</ref> ''[[Tar River Poetry]],'' and ''[[The Threepenny Review]]'';<ref name="threepenny">[http://www.threepennyreview.com/tocs/132_w13.html The Threepenny Review]</ref> in the UK in ''Agenda'',<ref name="agenda">[http://www.agendapoetry.co.uk/featured-translations.php Agenda]</ref> ''Anon'', ''Interlude'', ''Interpreter’s House'', ''Leviathan Quarterly'', ''[[New Walk]]'', ''[[Orbis (journal)|Orbis]]'', ''[[Poetry Review]]'', the ''[[Times Literary Supplement]]'', and ''Warwick Review''; and in Ireland, in ''Crannog'', ''Cyphers'', and ''[[The Stinging Fly]]''.<ref name="paw">[http://www.pw.org/content/terese_coe Poets & Writers Directory]</ref> The EBSCO research database lists numerous poems and translations by Coe.<ref name="ebsco">{{cite web|title=Citations with the tag: COE, Terese|url=http://connection.ebscohost.com/tag/COE%2C%2BTerese|website=EBSCO}}</ref>

Coe's poem "More" was among those chosen by ''[[Poetry Review]]'' Guest Editor [[George Szirtes]] to be heli-dropped across London as part of the 2012 London Olympics' Poetry Parnassus.<ref name="nyit1">[http://www.nyit.edu/index.php/faculty_staff_updates/faculty_and_staff_achievements9/ "Faculty and Staff Achievements", NYIT]</ref>

Terese Coe's first collection of poems, ''The Everyday Uncommon'', was published in 2005 by Wordtech.<ref name="amazon1">[http://www.amazon.com/Everyday-Uncommon-Terese-Coe/dp/1932339612/ ''The Everyday Uncommon'' at Amazon]</ref> 
Her second collection, ''Shot Silk'', was published in 2015 by White Violet Press.<ref>[http://www.amazon.com/Shot-Silk-Terese-Coe/dp/0692376283]</ref> Her work appears in anthologies such as ''Anthology One'' (from the Alsop Review), ''Grace Notes: Poetry from the Pages of First Things,'' ''Irresistible Sonnets,''<ref name="amazon2">[http://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=terese+coe+in+books&rh=i%3Aaps%2Ck%3Aterese+coe+in+books Terese Coe in Books at Amazon]</ref> and ''Phoenix Rising from the Ashes'' (Canada)<ref name="amazon2" />

Coe has worked as editor and writer for publications including ''The New York Free Press'' and ''Changes'' (NY, 1969); English teacher and director of poetry workshops in Kathmandu, Nepal; director of children’s poetry workshops at the Sun Valley (ID) Center for the Arts; and as editorial consultant for numerous financial publications at investment banks in Manhattan. She is currently an adjunct professor of English writing and literature in NY.<ref name="nyit2">[http://www.nyit.edu/index.php/arts_and_sciences/announcements/terese_coe_published_at_the_poetry_foundation/ "Terese Coe Published at the Poetry Foundation", NYIT]</ref>

== Awards and Scholarships ==

* [[Willis Barnstone Translation Prize]], Finalist, 2009 and 2004 for her translations of Pierre de Ronsard’s “Goodbye to the Green” (from the French) and Rainer Maria Rilke’s “End of Autumn” (from the German), respectively. Evansville, Ohio.<ref name="wordtech" /><ref name="baer_letter">{{cite web|last1=Baer|first1=William|title=Letter confirming Terese Coe was a finalist for the Barnstone Translation Prize in 2004 and 2009|url=http://ramblingrose.com/poetry/Baer_Letter.pdf|website=ramblingrose.com}}</ref><ref name="hypertexts">{{cite web|title=Terese Coe biography|url=http://www.thehypertexts.com/Terese_Coe_Poet_Poetry_Picture%20Bio.htm|website=The Hypertexts|publisher=Michael Burch, Editor}}</ref>
* [[Giorno Poetry Systems]]: Two grants for poetry, 1999 and 2000; John Giorno, New York, NY.<ref name="wordtech" /><ref name="hypertexts" />
* [[Helen Schaible Sonnet Award]] 2008: First Prize for poem, “Book of Changes.”<ref name="hypertexts" /><ref>{{cite journal|last1=William Baer, Barnstone Translation Prize Competition Director & Faculty Director of The Evansville Review|title=Contributors' Notes|journal=Evansville Review|date=Spring 2009|volume=XIX|page=153|url=http://ramblingrose.com/poetry/C70001050.pdf}}</ref><ref name="sphere2">{{cite web|last1=McLean|first1=Susan|title=Helen Schaible Shakespearean/Petrarchan Sonnet Contest|url=http://www.ablemuse.com/erato/showthread.php?t=2282|website=Eratosphere}}</ref>
* The Nimrod/Hardman Prize 2005: Semifinalist for poem, “Letter to Anton Chekhov.” From ''Nimrod International Journal'', Oklahoma.
* Nuyorican Poets Ball, 1992: First Prize in satire, Host Bob Holman.<ref name="wordtech" />
* ''Orbis'': “Lullaby” (translation from Rainer Maria Rilke) and “Saint John’s Bread” received the Orbis Readers’ Poll Honorable Mention for poems in Orbis 131, Spring 2005.
* ''Smartish Pace'': nomination for a [[Pushcart Prize]] for her Ronsard translation, “Beset by War”; Dec. 1, 2006, MD.
* ''Triplopia'': nominations for Pushcart Prize for “Minetta” in 2003; for “Spanish Dancer” in 2004, translated from the German of Rainer Maria Rilke.
* [[West Chester University Poetry Conference|The West Chester Poetry Conference]], West Chester University, PA: scholarships to attend in various years, including 2003 and 2005.
* Wordtech Communications: ''The Everyday Uncommon'' was a finalist in the Word Press Prize in 2004 and was published in 2005.<ref name="wordtech" />

== Critical Reception ==

[[Hayden Carruth]] wrote about ''The Everyday Uncommon'': “It’s clear to me that she knows what she’s doing, she’s doing what she wants to do, and she does it well.”<ref name="wordtech" /> The book has also received critical praise from [[Rhina Espaillat]],<ref name="wordtech" /> [[R. S. Gwynn]],<ref name="wordtech" /> [[David Mason (writer)|David Mason]],<ref name="sphere1">[http://www.ablemuse.com/erato/showthread.php?t=1612 Discussion of ''The Everyday Uncommon'' at Eratosphere]</ref> and [[Deborah Warren]],<ref name="wordtech" /> among others.

Terese Coe's work was discussed in depth by editors of the ''Cincinnati Review'' in 2012,<ref name="soapbox">{{cite web|last1=Ampleman|first1=Lisa|last2=McBride|first2=Matt|last3=Bogen|first3=Dan|title=Collaborative Feature—Soapbox and CR|url=http://www.cincinnatireview.com/blog/tag/for-i-will-consider/|website=The Cincinnati Review}}</ref><ref>{{cite web|last1=Coe|first1=Terese|title=For I Will Consider|url=http://www.soapboxmedia.com/features/012412cincinnatireview1.aspx|website=Soapbox}}</ref> and has been reviewed and discussed by [[Paul Hoover]]<ref name="hoover">{{cite web|last1=Hoover|first1=Paul|title=Clayton Eshleman in conversation with Paul Hoover and Maxine Chernoff|url=http://jacketmagazine.com/36/iv-eshleman-chernoff-hoover.shtml|website=Jacket Magazine}}</ref> and other critics.<ref name="newpages" />

== Works ==

=== Sample poems and translations ===

* Agenda: [http://www.agendapoetry.co.uk/featured-translations.php Translations]
* American Arts Quarterly: [http://www.nccsc.net/archives/poetry/poems/tompkins-square “Tompkins Square”]
* Candelabrum: [http://www.poetrymagazines.org.uk/magazine/record.asp?id=20431 "Ponderosa Pine"] and [http://www.poetrymagazines.org.uk/magazine/record.asp?id=20451 "Only the Road"]
* Christian Century: [http://www.christiancentury.org/contributor/terese-coe Rilke’s “The Angels”]
* Cincinnati Review and Soapbox Media: [http://soapboxmedia.com/features/012412cincinnatireview1.aspx “For I Will Consider”]
* E-Verse Radio: [http://www.everseradio.com/actors-terese-coe/ translation of Heinrich Heine’s “Where?”] (first appeared in Agenda, UK) and [http://www.everseradio.com/actors-terese-coe/ “Actors”]
* Huffington Post: [http://www.huffingtonpost.com/terese-coe/ “In Spate”]
* The Hypertexts: [http://www.thehypertexts.com/Terese_Coe_Poet_Poetry_Picture%20Bio.htm varying poems and translations]
* Kin: [http://wearekin.org/author/tcoe/mollusks “Mollusks”] and [http://wearekin.org/author/tcoe/taking_the_fall “Taking the Fall”] (first appeared in Poetry Nottingham, UK)
* Lilt: [http://ramblingrose.com/lilt/poem.php?pid=andthisiswhat “And This Is What We Have” (post 9-11)], [http://www.ramblingrose.com/lilt/poem.php?pid=behindtheseeyes “Behind These Eyes”] and [http://ramblingrose.com/lilt/poem.php?pid=itwasnt “It Wasn’t”]
* New American Writing: [http://www.newamericanwriting.com/25/borges.htm Translations]
* Poetry Foundation: [http://www.poetryfoundation.org/poetrymagazine/browse/185/6#!/20606941/0 Translation of Pierre de Ronsard’s “Epitaph for François Rabelais”]
* Soundzine: [http://soundzine.org/index.php?option=com_content&task=view&id=288&Itemid=1 “Hide or Pay Heed,” “I Died for War, and “I Am”] (inc. sound files)
* Stone Canoe: [http://www.stonecanoejournal.org/SC6OnlineContents/SC6_coe.pdf “Sponge”]
* Umbrella: [http://www.umbrellajournal.com/winterspring2011-2012/csm/TereseCoe.html “Approaching Salt Lake City from the East”]
* Unsplendid: [http://unsplendid.com/1-2/1-2_ronsardcoe_adieu.htm Translation of Pierre de Ronsard’s “Adieu Cruel Girl”]
* Verse Daily: [http://www.versedaily.org/itlotd.shtml “In the Lee of the Disaster”]

=== Collections ===
*[http://www.amazon.com/Shot-Silk-Terese-Coe/dp/0692376283 ''Shot Silk''], White Violet Press; (February, 2015), ISBN 978-0692376287
*[http://wordpoetrybooks.com/coe.html ''The Everyday Uncommon''], Wordtech; (January, 2005), ISBN 1932339612

=== Essays ===

* Umbrella: [http://www.umbrellajournal.com/summerfall2012/csm/CarmineStreetMetricsAnInt.html Carmine Street Metrics: An Introduction], 2012.

== References ==

{{reflist}}

{{DEFAULTSORT:Coe, Terese}}
[[Category:Living people]]
[[Category:American women poets]]
[[Category:Year of birth missing (living people)]]