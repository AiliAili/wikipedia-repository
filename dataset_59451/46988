{{Campaignbox Golden Age of Piracy}}
The '''Chepo expedition''' was a pirate voyage led by Spanish renegades '''[[Juan Guartem]]''', '''Eduardo Blomar''' and '''[[Bartolomé Charpes]]''' in the [[Spanish Main]] during 1679. Sailing up the Mandingua River, the expedition crossed the [[Isthmus of Panama]] into the Pacific where they raided shipping for several months as well as looting and then burning the town of [[Chepo, Panamá Province|Chepo, Panama]]. They were [[trial in absentia|tried ''in absentia'']] by the [[Viceroy|Viceroy of Bogotá]] and on his orders were [[effigy|burned in effigy]] at [[Santa Fe de Bogotá]]. However, the three continued committing piracy on both coasts of [[Central America]] and were never caught for their crimes. This was the second major expedition following the "Pacific Adventure" led by [[John Coxon (pirate)|John Coxon]] that same year. <ref>Anderson, C.L.G. ''Old Panama and Castilla Del Oro''. Washington: The Sudworth Company, 1911. (pg. 397)</ref> 

In an official communication by the [[Governor of Panama]] Don Dionicio Alceda in 1743, he recalled the incident writing ''"This passage was effected in the year 1679 by the arch pirates Juan Guartem, Eduardo Blomar and Bartolomé Charpes. These freebooters were tried for their crimes by audience of the viceroyalty, and as they could not be in person to suffer the just punishment, they were burned in effigy at Santa Fé (de Bogota), while they were yet ravaging the settlement on both sides the Isthmus"''. <ref>Scruggs, William L. ''The Colombian and Venezuelan Republics: With Notes on Other Parts of Central and South America''. Boston: Little, Brown and Company, 1905. (pg. 13)</ref> 

==References==
{{Reflist}}

==External links==
*[http://www.bruceruiz.net/PanamaHistory/Pirates/juan_guartem.htm Pirates and Buccaneers in Panama - Juan Guartem, Eduardo Blomar and Bartolomé Charpes]
{{Pirates}}
[[Category:Piracy in the Pacific Ocean]]
[[Category:1679]]
[[Category:Piracy in the Caribbean]]