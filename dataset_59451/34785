{{good article}}
{{Infobox hurricane season
|Basin=Atl
|Year=1859
|Track=1859 Atlantic hurricane season summary map.png
|First storm formed=July 1, 1859
|Last storm dissipated=October 29, 1859
|Strongest storm name=[[#Hurricane Six|Six]]
|Strongest storm winds=115
|Strongest storm pressure=938
|Total depressions=8
|Total storms=8
|Total hurricanes=7
|Fatalities=Numerous at sea
|Damages=
|five seasons=[[1857 Atlantic hurricane season|1857]], [[1858 Atlantic hurricane season|1858]], '''1859''', [[1860 Atlantic hurricane season|1860]], [[1861 Atlantic hurricane season|1861]]}}

The '''1859 Atlantic hurricane season''' featured seven hurricanes, the most recorded during an [[Atlantic hurricane season]] until [[1870 Atlantic hurricane season|1870]].<ref name="ACE"/> However, in the absence of modern satellite and other remote-sensing technologies, only storms that affected populated land areas or encountered ships at sea were recorded, so the actual total could be higher. An undercount bias of zero to six [[tropical cyclone]]s per year between 1851 and 1885 has been estimated.<ref>{{cite book|title=Hurricanes and Typhoons: Past, Present and Future|chapter=The Atlantic hurricane database re-analysis project: Documentation for the 1851–1910 alterations and additions to the HURDAT database|author =Christopher W. Landsea|year=2004|publisher=Columbia University Press|location=New York City, New York|isbn=0-231-12388-4|pages=177–221}}</ref> Of the eight known 1859 cyclones, five were first documented in 1995 by Jose Fernandez-Partagás and Henry Diaz, which was largely adopted by the [[National Oceanic and Atmospheric Administration]]'s [[Atlantic hurricane reanalysis]] in their updates to the [[HURDAT|Atlantic hurricane database]] (HURDAT), with some adjustments. HURDAT is the official source for hurricane data such as track and intensity, although due to sparse records, listings on some storms are incomplete.

The first tropical cyclone was a hurricane observed in the [[Tuxpan]] area of [[Veracruz]], [[Mexico]], on July&nbsp;1. Hurricane conditions were observed along the coast and several vessels were lost. On September&nbsp;2, another hurricane struck [[Saint Kitts]] and [[Saint Croix, U.S. Virgin Islands|Saint Croix]], damaging ships on the former. The fifth storm of the season, possibly the most devastating of the season, brought storm surge and hurricane force winds to the [[Florida Panhandle]] and [[Mobile, Alabama]], as well as flooding and wind damage to some areas of the [[Mid-Atlantic states|Mid-Atlantic]]. In early October, the sixth cyclone brought damage to Inagua in the Bahamas. At least 25 boats sunk, with several people drowning after one vessel capsized. Two ships capsized in the Bahamas due to the seventh storm. A ship in the Gulf of Mexico capsized during the eighth and final cyclone, drowning an unknown number of people. The storm became [[Extratropical cyclone|extratropical]] offshore the [[Southeastern United States]] on October&nbsp;29. 

The season's activity was reflected with an [[accumulated cyclone energy]] (ACE) rating of 56.<ref name="ACE">{{cite report|work=[[Hurricane Research Division]]; [[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=[[National Oceanic and Atmospheric Administration]]|date=February 2014|title=Atlantic basin Comparison of Original and Revised HURDAT|accessdate=July 13, 2014|url=http://www.aoml.noaa.gov/hrd/hurdat/comparison_table.html|location=Miami, Florida}}</ref> ACE is, broadly speaking, a measure of the power of the hurricane multiplied by the length of time it existed, so storms that last a long time, as well as particularly strong hurricanes, have high ACEs. It is only calculated for full advisories on tropical systems at or exceeding 39&nbsp;mph (63&nbsp;km/h), which is tropical storm strength.<ref>{{cite report|author =David Levinson|date=August 20, 2008|title=2005 Atlantic Ocean Tropical Cyclones|publisher=National Oceanic and Atmospheric Administration|work=[[National Climatic Data Center]]|accessdate=July 13, 2014|url=http://www.ncdc.noaa.gov/oa/climate/research/2005/2005-atlantic-trop-cyclones.html|location=Asheville, North Carolina}}</ref>

==Systems==
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/07/1859 till:01/12/1859
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/07/1859

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_≥157_mph_(≥252_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:01/07/1859 till:01/07/1859 color:C2 text:One (C2)
  from:17/08/1859 till:19/08/1859 color:C2 text:Two (C2)
  from:02/09/1859 till:02/09/1859 color:C1 text:Three (C1)
  from:12/09/1859 till:13/09/1859 color:C2 text:Four (C2)
  from:15/09/1859 till:18/09/1859 color:C1 text:Five (C1)
  from:02/10/1859 till:06/10/1859 color:C3 text:Six (C3)
  from:16/10/1859 till:18/10/1859 color:TS text:Seven (TS)
  from:24/10/1859 till:29/10/1859 color:C1 text:Eight (C1)

  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/07/1859 till:01/08/1859 text:July
  from:01/08/1859 till:01/09/1859 text:August
  from:01/09/1859 till:01/10/1859 text:September
  from:01/10/1859 till:01/11/1859 text:October
  from:01/11/1859 till:01/12/1859 text:November
TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir–Simpson Hurricane Scale]])"

</timeline>
</center>
===Hurricane One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1859 Atlantic hurricane 1 track.png
|Formed=July 1
|Dissipated=July 1
|1-min winds=90
}}
Little is known about the first observed tropical cyclone of the 1859 season, which was first analyzed in 1995.<ref name="Partagas1">Partagás and Diaz, p. 1</ref> During late June or early July, cities along the coastal [[Mexican state]] of [[Veracruz]] experienced a formidable hurricane, and several ships in the [[Gulf of Mexico]] were lost. Due to a lack of reports, the storm's listing in the Atlantic hurricane database is limited to a single point near [[Tuxpan, Veracruz]], on July&nbsp;1, although this date was chiefly chosen as a placeholder in lieu of definitive data.{{Atlantic hurricane best track}}<ref name="meta">{{cite web|author =Hurricane Research Division|year=2011|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|publisher=National Oceanic and Atmospheric Administration|accessdate=January 2, 2013|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_master.html}}</ref> It is estimated that sustained winds reached 105&nbsp;mph (165&nbsp;km/h), which is equivalent to a Category 2 hurricane on the modern day [[Saffir–Simpson hurricane wind scale]].{{Atlantic hurricane best track}} 
{{clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1859 Atlantic hurricane 2 track.png
|Formed=August 17
|Dissipated=August 19
|1-min winds=90
|Pressure=982
}}
The second known storm of the season was discovered in contemporary reanalysis. It existed in the northwestern Atlantic in the middle of August with the only evidence of the storm being reports from two vessels in the vicinity of bad weather. One vessel, the ''Tornado'', encountered severe winds, starting on August&nbsp;17, which forced her to abandon her easterly course and sail into [[New York City]]. The ''Caure'' also experienced strong winds on August&nbsp;18 and August&nbsp;19 with a [[barometric pressure]] as low as {{convert|982|mbar|inHg|abbr=on|lk=on}}.<ref name="Partagas1"/><ref name="meta"/> A standard wind–pressure relationship model for that value yields winds of {{convert|80|mph|km/h|abbr=on}}. The wind patterns reported by each ship indicate that neither vessel reached the storm's core, where winds are typically at their strongest. As a result, the system is estimated to have attained at least Category 2 intensity.<ref name="meta"/> Its approximate track follows an east-northeasterly trajectory.{{Atlantic hurricane best track}}
{{clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1859 Atlantic hurricane 3 track.png
|Formed=September 2
|Dissipated=September 3
|1-min winds=70
}}
The first of three hurricanes in September was also the first of the season to be identified prior to 1995. It was described by W. H. Alexander in a 1902 publication as a "mild" system which passed over [[St. Kitts]] and later [[St. Croix]] on September&nbsp;2. A short track formulated for the storm in 1995 indicated a path across the northern [[Lesser Antilles]] on September&nbsp;2; the track was shifted slightly toward the south for its inclusion within HURDAT.{{Atlantic hurricane best track}}<ref name="Partagas2"/> The highest sustained winds are estimated to have been 80&nbsp;mph (130&nbsp;km/h).{{Atlantic hurricane best track}}
{{clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1859 Atlantic hurricane 4 track.png
|Formed=September 12
|Dissipated=September 13
|1-min winds=90
}}
Based on reports of strong winds from a ship near 40°N, 50°W, Partagás documented another new storm near that point on September 12.<ref name="Partagas2"/> As with the previous hurricane, no track existed for this storm until the 1995 HURDAT, when more extensive observations from several additional vessels were utilized. At least four ships sustained structural damage or took on water, and the ''Bell Flower'' lost her captain and a crew member to the sea. The severity of the weather encountered by the ships suggested a cyclone of modest hurricane intensity.{{Atlantic hurricane best track}}<ref name="meta"/>  
{{clear}}

===Hurricane Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1859 Atlantic hurricane 5 track.png
|Formed=September 15
|Dissipated=September 18
|1-min winds=70
|Pressure=982
}}
The fifth storm was first mentioned by [[David M. Ludlum]] in 1963 as having affected [[Mobile, Alabama]], on September 15, but with no description of its genesis or impacts.<ref>Ludlum, p.&nbsp;196</ref> Partagás also acknowledged the storm without attempting to reconstruct its track.<ref name="Partagas2">Partagás and Diaz, p. 2</ref><ref name="Partagas4">Partagás and Diaz, p. 4</ref> As a result, the cyclone was initially added to the hurricane database with only a single data point, placing it near Mobile as a Category&nbsp;1 hurricane. However, in 2003, the reanalysis project expanded the track from September&nbsp;15 through September&nbsp;18, using newspaper accounts and reports from both land and sea.<ref name="meta"/> With winds of minimal hurricane strength, the storm moved north-northeastward toward the central [[Gulf Coast of the United States|Gulf Coast]].{{Atlantic hurricane best track}} The cyclone made landfall early on September&nbsp;16 over [[Alabama]] with winds estimated at 80&nbsp;mph (130&nbsp;km/h) and a barometric pressure of {{convert|982|mbar|inHg|abbr=on}}.<ref name="us">{{cite web|author =Hurricane Research Division|year=2012|title=Chronological List of All Continental United States Hurricanes: 1851-2011|publisher=National Hurricane Center|accessdate=January 2, 2013|url=http://www.aoml.noaa.gov/hrd/hurdat/All_U.S._Hurricanes.html}}</ref> It likely weakened into a tropical storm as it pushed inland, and traversed the [[Southeastern United States]] and [[Mid-Atlantic states|Mid-Atlantic region]]. The storm re-emerged into the Atlantic as it continued towards the northeast,{{Atlantic hurricane best track}} and based on ship reports, it is believed to have reattained hurricane intensity prior to passing south of New England and the [[Canadian Maritimes]], before being last noted on September&nbsp;18.<ref name="meta"/>

Hurricane-force winds were reported in the [[Florida Panhandle]].<ref name="us"/> In [[Alabama]], the storm brought strong winds and large waves to the Mobile area. After a wharf was flooded, authorities warned residents to seek higher ground. Businesses also moved their merchandise to the second floors of their buildings. Wharves, bathhouses, bales, barrels, and boxes washed away. The train system was also interrupted for a few days. A few ships, such as the schooner ''W W. Harkness'' and the steamboat ''Crescent'' suffered damage. Winds downed fences, trees, and telegraph lines. Damage reached at least US$10,000. Later, flooding was reported in [[Virginia]] and [[Washington, D.C.]]. The [[Potomac River]] rose considerably in some areas of Virginia and Washington, D.C., especially at [[Georgetown (Washington, D.C.)|Georgetown]], where water reached the wharves. Two bridges nearly swept away. With the storm causing over {{convert|8|in|mm|abbr=on}} of rain, crops, mill dams, and fences were damaged. In [[New York (state)|New York]], strong winds destroyed a five-story warehouse and another adjoining building.<ref name="meta"/>
{{clear}}

===Hurricane Six===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1859 Atlantic hurricane 6 track.png
|Formed=October 2
|Dissipated=October 6
|1-min winds=110
|Pressure=938<ref name="HURDAT"/>
}}
The sixth storm of the season was first documented by Edward B. Garriott in 1900. After being first observed by a ship near [[Jamaica]] on October&nbsp;2, the hurricane tracked northward over the extreme eastern tip of Cuba, according to its reconstructed track. Heavy weather was reported in [[Baracoa]]. The storm later reached southeastern [[Bahamas]], where the cyclone severely impacted the [[Inagua]] region on October 2 and 3, destroying at least 25&nbsp;boats. Several ships underway around Inagua endured rough seas and strong winds; "several of the crew and two soldiers" aboard one vessel wrecked by the hurricane died.<ref name="Partagas4"/> Meteorological reports from ships confirm that the hurricane continued northward, passing about midway between [[Bermuda]] and the [[East Coast of the United States]]. On October&nbsp;6, a vessel near the center of the storm recorded a barometric pressure of {{convert|938|mbar|inHg|abbr=on}}, signalling it was an intense hurricane even after it crossed the [[40th parallel north]].{{Atlantic hurricane best track}}<ref name="Partagas5">Partagás and Diaz, p. 5</ref> This was also the lowest pressure associated with the storm and was used to estimate that maximum sustained winds peaked at 125&nbsp;mph (205&nbsp;km/h).{{Atlantic hurricane best track}}<ref name="meta"/> Also on October&nbsp;6, a ship near [[Sable Island]] encountered the storm, which likely continued to approach the [[The Maritimes|Canadian Maritimes]]. That day, the storm was last noted offshore [[Nova Scotia]].{{Atlantic hurricane best track}}<ref name="Partagas5"/>
{{clear}}

===Tropical Storm Seven===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1859 Atlantic tropical storm 7 track.png
|Formed=October 16
|Dissipated=October 18
|1-min winds=60
|Pressure=
}}
Beginning on October&nbsp;16, ships throughout the central Bahamas experienced squally conditions accompanied by strong winds. Based on those reports, this storm was documented in 1995 as a west-northwestward-moving system. One ship ran aground on [[Paradise Island]] and another suffered a similar fate in the [[Abaco Islands]].<ref name="Partagas5"/> Continuing westward, the storm made landfall near modern day [[Boca Raton, Florida]], with winds of 70&nbsp;mph (110&nbsp;km/h) at 16:00&nbsp;UTC on October&nbsp;17. The system was last noted near present day [[Arcadia, Florida|Arcadia]] early the following day.{{Atlantic hurricane best track}}
{{clear}}

===Hurricane Eight===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1859 Atlantic hurricane 8 track.png
|Formed=October 24
|Dissipated=October 29
|1-min winds=80
|Pressure=974
}} 
A low pressure area developed over the [[Bay of Campeche]] between October&nbsp;23 and October&nbsp;24,<ref name="meta"/> with a tropical storm forming on the latter date. At least four other ships in that portion of the Gulf of Mexico sustained appreciable structural damage. One ship capsized in the storm with all hands lost except one seaman, who was picked up on November 2 by a passing vessel. The survivor said he was stranded on the wreck for five days, indicating that his ship went down on October&nbsp;28. Stormy weather was reported near Bermuda. Partagás and Diaz used these reports to create a track for the storm.<ref name="Partagas6">Partagás and Diaz, p. 6</ref> Initially, the storm drifted north-northeastward and northeastward. By 12:00&nbsp;UTC on October&nbsp;26, it was estimated that the system became a hurricane and intensified slightly further to peak at winds of 90&nbsp;mph (150&nbsp;km/h).{{Atlantic hurricane best track}} Shortly thereafter, the hurricane began accelerating east-northeastward due to a [[cold front]]. At 18:00&nbsp;UTC on October&nbsp;28, the storm made landfall near [[St. Petersburg, Florida]].<ref name="meta"/> A barometric pressure of {{convert|974|mbar|inHg|abbr=on}} was observed,<ref name="meta"/> the lowest in relation to the storm. The storm emerged in the Atlantic less than six hours later and transitioned into an [[extratropical cyclone]] early on October&nbsp;29.{{Atlantic hurricane best track}}
{{clear}}

==See also==
{{Portal|Tropical cyclones}}
* [[List of Atlantic hurricane seasons]]
{{clear}}

==Notes==
{{Reflist|2}}

== References ==
{{commons category|1859 Atlantic hurricane season}}
* {{cite book|author =[[David M. Ludlum|Ludlum, David McWilliams]]|title=Early American hurricanes, 1492–1870|year=1963|publisher=[[American Meteorological Society]]}}
* {{cite web|author1=Partagás, José Fernández  |author2=Diaz, Henry|publisher=National Oceanic and Atmospheric Administration|year=2003|title=A Reconstruction of Historical Tropical Cyclone Frequency in the Atlantic from Documentary and other Historical Sources: Year 1859|accessdate=July 15, 2011|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1858-1864/1859.pdf}}

{{TC Decades|Year=1850|basin=Atlantic|type=hurricane}}

[[Category:1850–1859 Atlantic hurricane seasons| ]]
[[Category:1859 meteorology]]
[[Category:Articles which contain graphical timelines]]