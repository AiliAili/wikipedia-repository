{{Infobox journal
| cover = [[Image:ArchivesofAsianArtvol57.jpg]]
| discipline = [[Art]]
| abbreviation = Arch. Asian Art
| formernames = Archives of the Chinese Art Society of America
| publisher = [[University of Hawaii Press]] on behalf of the [[Asia Society]]
| country = United States
| frequency = Annual
| history = 1945–present
| website = http://www.uhpress.hawaii.edu/journals/asianart/
| link1 = http://muse.jhu.edu/journals/archives_of_asian_art/
| link1-name = Online access at [[Project MUSE]]
| ISSN = 0066-6637
| JSTOR = 00666637
}}
'''''Archives of Asian Art''''' is an annual [[academic journal]] covering [[the arts]] of South, Southeast, Central, and East Asia. Each issue contains articles by [[scholar]]s of [[art]] and a selection of outstanding works of Asian art acquired by North American [[art museum|museums]] during the previous year.

The journal was established in 1945 as the ''Archives of the Chinese Art Society of America''. It obtained its current title with volume 20 in 1966. The journal is owned by the [[Asia Society]], which in 2007 changed its publisher from [[Brepols]] to the [[University of Hawaii Press]].

Volumes 1–55 (1945–2005) are available on [[JSTOR]]; recent volumes are available on [[Project MUSE]].

== External links ==
* {{Official website|http://www.uhpress.hawaii.edu/journals/asianart/}}
* [http://muse.jhu.edu/journals/archives_of_asian_art/ MUSE homepage]
* [http://www.asiasociety.org/ Asian Society homepage]

[[Category:Annual journals]]
[[Category:Asian studies journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1946]]
[[Category:Visual art journals]]