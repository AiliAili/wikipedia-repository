{{EngvarB|date=August 2016}}
{{Use dmy dates|date=August 2016}}

{{italic title|force=true}}
{{Infobox European case
|short_name=Intel v. CPM
|court=ECJ
|SubmitDate=29 May
|SubmitYear=2007
|DecideDate=27 November
|DecideYear=2008
|FullName=Intel Corporation Inc. v. CPM United Kingdom Ltd.
|CelexID=62007CJ0252
|CaseNumber=252/07, ][2009] E.T.M.R. 13
|Chamber=First Chamber
|Nationality=United Kingdom
|Procedural=Court of Appeal (England & Wales) (Civil Division) [2007] EWCA Civ 431
|JudgeRapporteur=Marko Ilešič
|JudgePresident=Peter Jann|Peter Jann
|AdvocateGeneral=Eleanor V. E. Sharpston|Eleanor V. E. Sharpston
|Ruling = In Order to Show Trademark Dilution there must be proof:<br><br>
1.That a link between the marks exists in the minds of the consumer.
2.That use of the later mark would take [[unfair advantage (business)|unfair advantage]] of, or is or would be detrimental to, the distinctive character or the repute of the earlier mark.
|InstrumentsCited=[[Trade Marks Directive|Directive 89/104/EEC]], & [http://eur-lex.europa.eu/Notice.do?val=277903:cs&lang=en&list=277903:cs,&pos=1&page=1&nbl=1&pgs=10&hwords=&checktexte=checkbox&visu=#texte Adidas-Salomon and Adidas Benelux v. Fitnessworld Training LTd. C-408/01]
|LegislationAffecting=Interpreting Article 4(4)(a) of [[Trade Marks Directive|Directive 89/104/EEC]]
|Keywords=Trademark Dilution
}}

'''''Intel Corporation v. CPM United Kingdom Ltd.''''',<ref>Intel Corp. v. CPM United Kingdom, C-252/07, [2009] E.T.M.R. 13</ref> Case 252/2007 was a case of the [[European Court of Justice]] in which the ECJ interpreted the meaning of Article 4 (4)(a) of the EU [[Trade Marks Directive]]. The ECJ considered what elements are required to show that a later mark was causing dilution to an earlier mark. The case laid out a clear basis on which grounds a court can find that trademark dilution has occurred.

== Background ==

[[Intel Corp.]], a company that produces microprocessors had a UK national trademark, as well as various trademarks in other countries, and an EU-wide community trademark consisting of, or including the word 'INTEL'. [[CPM Group|CPM United Kingdom]] had the UK national trademark registration for the word 'INTELMARK' to brand their telemarketing services. Intel claimed that CPM's use of the mark would take unfair advantage of, or be detrimental to, the distinctive character or the repute of the earlier 'INTEL' mark the [[Trade Marks Act 1994]].<ref>''Ibid'', § 12(The Trade Marks Act is the UK adoption of the EU Trade Marks Directive).</ref> Intel brought an action against CPM with the UK Trade Mark Registry, and a Hearing Officer denied the claim on 1 February 2006. Intel appealed the case to the High Court of Justice of England and Wales, and they dismissed the complaint on 26 July 2006. Intel then appealed the decision to the Court of Appeal which then decided that there was a question of European Law and stayed the proceedings while confirming the question about dilution to the ECJ.

== Opinion ==

The Court determined that Article 4(4)(a) of the Directive ensures protection for trade marks with a reputation by protecting the mark from damage to its distinctive character, or to its reputation, and it prohibits the subsequent mark from taking unfair advantage of the distinctiveness or repute of the earlier mark.

The Court concluded that it should interpret Article 4(4)(a) in the same way that it interpreted Article 5(2) in an earlier ruling as the wording of these articles are designed to give trademarks with a reputation the same protection.

The Court determined that under the ''Adidas-Salomon and Adidas Benelux''<ref>''Ibid'', § 30 (citing [http://eur-lex.europa.eu/Notice.do?val=277903:cs&lang=en&list=277903:cs,&pos=1&page=1&nbl=1&pgs=10&hwords=&checktexte=checkbox&visu=#texte Adidas-Salomon and Adidas Benelux v. Fitnessworld Training LTd. C-408/01])</ref> ruling a plaintiff must establish that a link between the marks exists in the minds of the relevant public. The Court further goes on to state that while the establishment of the link is a requirement to show dilution, the presence of the link itself is not sufficient to establish that one of the three types of injury had occurred.

In answering the questions posed by the Court of Appeal regarding the link the ECJ found that a court determining whether there exists such a link must look at all relevant factors and undertake a global analysis. Factors that the court considered relevant include: 1) the degree of similarity between the marks, 2) the nature of the goods or services for which the marks are registered, and the degree of similarity between those categories and the relevant consumers, 3) the strength of the earlier mark's reputation, 4) the degree of the earlier mark's distinctive character, and 5) the existence of the likelihood of confusion.

The Court found that the relevant public should be the consumers of the product of the prior mark if the plaintiff alleges injury to the reputation or distinctive character of the prior mark. If the plaintiff claims that the later mark is gaining an unfair advantage then the relevant public to be considered is the consumers of the good identified by the subsequent mark.

The Court next looked at what a court should consider to determine whether there was an injury. The Court concluded that to show injury by harm to the distinctive character of the mark or to its reputation, a plaintiff has to show that there was a change in the economic behaviour of the average consumer of the goods for which the prior mark was registered. Likewise if a plaintiff wants to show that injury occurred due to the latter marks gaining of an unjust advantage it would have to be shown that use of the subsequent mark caused a change in the economic behaviour of the average consumer of the goods for which the subsequent mark was registered. The Court determined that the group whose economic behaviour changes must be analogous to the group considered to be the relevant public of the marks.

Lastly the Court concluded that the change in economic behaviour must not be actual, but a subsequent mark should also be denied if it can be shown that there is a high likelihood of future harm to the prior mark.

== Judgment ==

The Court concluded that the fact that an earlier mark had a significant reputation, the categories of goods for which the marks were registered were dissimilar, and that the earlier mark was unique, is insufficient as to assume that there is a link between the marks under ''Adidas-Salomon and Adidas Benelux'

That to show injury to the prior mark all relevant factors should be considered. Just because the earlier mark has a reputation, the goods are dissimilar, the prior mark is unique, and that the later mark calls the prior one to mind, is insufficient to show that the later mark causes injury.

Lastly injury can be shown even if the prior mark is not unique. First use of the subsequent mark may cause injury and so likelihood of future harm is sufficient proof, and that evidence of injury requires proof of a change in economic behaviour of the relevant consumers.

== Commentary ==

It has been said that the approach taken by the ECJ in determining dilution adds increased complexity to the subject and makes it more difficult to determine as the Court added the test for a link, and the analysis of economic behaviour.<ref>{{cite journal|last=Middlemiss|first=Susie|author2=Steven Warner|title=The Protection of Marks with a Reputation: Intel v. CPM|journal=European Intellectual Property Review|year=2009|volume=31|issue=6|page=336}}</ref> However they also feel that the ECJ approach may be easier to apply in practice than the more nebulous and undefined concepts of detriment and unfair advantage, and so if realised properly the ECJ could be an advantage to trademark holders.<ref>''Ibid''</ref>

== See also ==

*[[European Union law]]
*[[Trade Marks Directive]]
*[[European Court of Justice]]

== Notes ==

{{reflist|2}}

== References ==

*{{cite journal|last=Fhima|first=Ilanah Simon|title=Exploring the Roots of European Dilution|journal=Intellectual Property Quarterly|year=2012|volume=25|issue=38|ssrn=2111951}}
*{{cite journal|last=Middlemiss|first=Susie|author2=Steven Warner|title=The protection of marks with a reputation: Intel v CPM|journal=European Intellectual Property Review|year=2009|volume=31|issue=6|url=http://www.slaughterandmay.com/media/881026/the_protection_of_marks_with_a_reputation_intel_v_cpm.pdf}}
*{{cite web|last=Kat|first=IP|title=Intel v CPM – ECJ judgment|url=http://ipkitten.blogspot.com/2008/11/intel-v-cpm-ecj-judgment.html|accessdate=16 May 2013}}

== External links ==

* Case 252/07, ''Intel Corporation v. CPM United Kingdom Ltd.'', [2009] E.M.T.R 13 [http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:62007CJ0252:EN:NOT].
{{Intel}}
{{DEFAULTSORT:Intel Corporation Inc. v. CPM United Kingdom Ltd}}
[[Category:Court of Justice of the European Union case law]]
[[Category:Intel Corporation litigation]]