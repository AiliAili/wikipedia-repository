{{Infobox bridge
| bridge_name = Albert Bridge
| image       = Albert Bridge from Battersea.JPG
| image_size  = 250px
| carries     = [[A3031 road (Great Britain)|A3031 road]]
| preceded    = [[Battersea Bridge]]
| followed    = [[Chelsea Bridge]]
| crosses     = [[River Thames]]
| locale      = [[Battersea]] and [[Chelsea, London]]
| designer    = [[Rowland Mason Ordish]], [[Joseph Bazalgette]]
| design      = [[Ordish–Lefeuvre system]], subsequently modified to an Ordish–Lefeuvre system&nbsp;/ [[suspension bridge]]&nbsp;/ [[beam bridge]] hybrid design
| spans       = 4 (3 before 1973)
| pierswater  = 6 (4 before 1973)
| mainspan    = {{unbulleted list | {{convert|384|ft|9|in|m}} (before 1973) | {{convert|185|ft|m}} (after 1973) }}
| length      = {{convert|710|ft|m}}
| width       = {{convert|41|ft|m}}
| height      = {{convert|66|ft|m}}
| below       = {{convert|37|ft|9|in|m|sigfig=3}} at [[lowest astronomical tide]]<ref name="Thames Bridges Heights" />
| traffic     = 19,821 vehicles (2004){{sfn|Cookson|2006|p=316}}
| open        = {{Start date|1873|08|23|df=y}}
| heritage    = [[listed building|Grade II* listed structure]]
<!-- | coordinates = {{coord|51|28|56|N|0|10|00|W|region:GB_type:landmark|display=inline,title}}  deprecated -->
| coordinates = {{coord|51.4823|-0.1667|region:GB_type:landmark|display=inline,title}}
}}

The '''Albert Bridge''' is a road bridge over the [[River Thames]] in [[West (London sub region)|West London]], connecting [[Chelsea, London|Chelsea]] on the north bank to [[Battersea]] on the south bank. Designed and built by [[Rowland Mason Ordish]] in 1873 as an [[Ordish–Lefeuvre system]] modified [[cable-stayed bridge]], it proved to be structurally unsound, so between 1884 and 1887 [[Joseph Bazalgette|Sir Joseph Bazalgette]] incorporated some of the design elements of a [[suspension bridge]]. In 1973 the [[Greater London Council]] added two concrete [[pier (architecture)|piers]], which transformed the central span into a simple [[beam bridge]]. As a result, today the bridge is an unusual hybrid of three different design styles. It is an [[English Heritage]] Grade II* [[listed building]].<ref>{{cite web|url=http://list.historicengland.org.uk/resultsingle.aspx?uid=1065576|title=  Name: ALBERT BRIDGE List entry Number: 1065576 |publisher= Historic England|accessdate = 26 May 2015}}</ref>

Built as a [[toll bridge]], it was commercially unsuccessful. Six years after its opening it was taken into public ownership and the tolls were lifted. The [[Toll house|tollbooths]] remained in place and are the only surviving examples of bridge tollbooths in London. Nicknamed "The Trembling Lady" because of its tendency to vibrate when large numbers of people walked over it, the bridge has signs at its entrances that warned troops to break step whilst crossing the bridge.

Incorporating a roadway only {{convert|27|ft|m}} wide, and with serious structural weaknesses, the bridge was ill-equipped to cope with the advent of the motor vehicle during the 20th century. Despite the many calls for its demolition or pedestrianisation, the Albert Bridge has remained open to vehicles throughout its existence, other than for brief spells during repairs, and is one of only two Thames road bridges in central London never to have been replaced. The strengthening work carried out by Bazalgette and the Greater London Council did not prevent further deterioration of the bridge's structure. A series of increasingly strict traffic control measures have been introduced to limit its use and thus prolong its life, making it the least busy Thames road bridge in London, except for the little-used [[Southwark Bridge]]. The bridge's condition is continuing to degrade as the result of traffic load and severe rotting of the timber [[deck (bridge)|deck]] structure caused by the urine of the many dogs using it as a route to nearby [[Battersea Park]].

In 1992, the Albert Bridge was rewired and painted in an unusual colour scheme designed to make it more conspicuous in poor visibility, and avoid being damaged by ships. At night it is illuminated by 4,000&nbsp;bulbs, making it one of west London's most striking landmarks. In 2010–2011, these were replaced with [[light emitting diode|LEDs]].

==History==
[[File:Stanford 1891 Chelsea bridges.png|thumb|300px|left|Chelsea and Battersea in 1891, showing (left to right) Old Battersea Bridge, Albert Bridge, Victoria (now Chelsea) Bridge and Grosvenor Railway Bridge. Battersea and Albert bridges are less than {{convert|500|yd|m}} apart.]]

The historic industrial town of [[Chelsea, London|Chelsea]] on the north bank of the [[River Thames]] about {{convert|3|mi|km}} west of [[Westminster]], and the rich farming village of [[Battersea]], facing Chelsea on the south bank, were linked by the modest wooden [[Battersea Bridge]] in 1771.{{sfn|Matthews|2008|p=65}} In 1842 the [[Commissioners of Woods and Forests|Commission of Woods, Forests, and Land Revenues]] recommended the construction of an [[Thames Embankment|embankment]] at Chelsea to free land for development, and proposed a new bridge downstream of Battersea Bridge, and the replacement of the latter by a more modern structure.{{sfn|Roberts|2005|p=130}} Work on the [[Chelsea Bridge|Victoria Bridge]] (later renamed Chelsea Bridge), a short distance downstream of Battersea Bridge, began in 1851 and was completed in 1858, with work on the [[Chelsea Embankment]] beginning in 1862.{{sfn|Roberts|2005|p=112}} Meanwhile, the proposal to demolish Battersea Bridge was abandoned.{{sfn|Roberts|2005|p=130}}

The wooden Battersea Bridge had become dilapidated by the mid-19th century. It had grown unpopular and was considered unsafe.{{sfn|Roberts|2005|p=63}} The newer Victoria Bridge, meanwhile, suffered severe congestion. In 1860, [[Albert, Prince Consort|Prince Albert]] suggested that a new [[tollbridge]] built between the two existing bridges would be profitable,{{sfn|Davenport|2006|p=71}} and in the early 1860s the Albert Bridge Company was formed with the aim of building this new crossing.{{sfn|Matthews|2008|p=71}} A proposal put forward in 1863 was blocked by strong opposition from the operators of Battersea Bridge, which was less than {{convert|500|yd|m}} from the proposed site of the new bridge and whose owners were consequently concerned over potential loss of custom.{{sfn|Matthews|2008|p=71}} A compromise was reached, and in 1864 a new Act of Parliament was passed, authorising the new bridge on condition that it was completed within five years.{{sfn|Cookson|2006|p=126}} The Act compelled the Albert Bridge Company to purchase Battersea Bridge once the new bridge opened, and to compensate its owners by paying them £3,000 per annum (about £{{formatnum:{{Inflation|UK|3000|1864|r=-3}}|0}} in {{CURRENTYEAR}}) in the interim.{{sfn|Davenport|2006|p=72}}{{Inflation-fn|UK}}

[[File:Bridge of Franz Joseph I., Prague.jpg|thumb|The 1868 [[Franz Joseph Bridge]] in Prague was built to the proposed design of the future Albert Bridge.]]

[[Rowland Mason Ordish]] was appointed to design the new bridge.{{sfn|Matthews|2008|p=71}} Ordish was a leading architectural engineer who had worked on the [[Royal Albert Hall]], [[St Pancras railway station]], the [[The Crystal Palace|Crystal Palace]] and [[Holborn Viaduct]].{{sfn|Matthews|2008|p=71}} The bridge was built using the [[Ordish–Lefeuvre system]], an early form of [[cable-stayed bridge]] design which Ordish had patented in 1858.{{sfn|Davenport|2006|p=71}} Ordish's design resembled a conventional [[suspension bridge]] in employing a [[parabola|parabolic]] cable to support the centre of the bridge, but differed in its use of 32&nbsp;inclined [[Guy-wire|stays]] to support the remainder of the load.{{sfn|Smith|2001|p=38}} Each stay consisted of a flat [[wrought iron]] bar attached to the bridge deck, and a [[wire rope]] composed of 1,000 {{convert|1/10|in|mm|adj=on|sigfig=2}} diameter wires joining the wrought iron bar to one of the four octagonal support columns.{{sfn|Tilly|2002|p=217}}

==Design and construction==
Although authorised in 1864, work on the bridge was delayed by negotiations over the proposed [[Chelsea Embankment]], since the bridge's design could not be completed until the exact layout of the new roads being built on the north bank of the river had been agreed.{{sfn|Cookson|2006|p=126}} While plans for the Chelsea Embankment were debated, Ordish built the [[Franz Joseph Bridge]] over the [[Vltava]] in [[Prague]] to the same design as that intended for the Albert Bridge.{{sfn|Matthews|2008|p=72}}{{refn|group=n|Damaged during the [[Second World War]], the Franz Joseph Bridge was replaced by a more conventional bridge in the 1950s. The Albert Bridge and the Franz Joseph Bridge were the only significant bridges built using the Ordish–Lefeuvre system; a third, smaller bridge was built in [[Singapore]].}}

[[File:ILN Chelsea Embankment & Albert Bridge construction.jpg|left|thumb|Chelsea Embankment and the Albert Bridge under construction, 1873]]

In 1869, the time allowed by the 1864 Act to build the bridge expired. Delays caused by the Chelsea Embankment project meant that work on the bridge had not even begun, and a new Act of Parliament was required to extend the time limit.{{sfn|Cookson|2006|p=126}} Construction finally got underway in 1870, and it was anticipated that the bridge would be completed in about a year, at a cost of £70,000 (about £{{formatprice|{{Inflation|UK|70000|1869|r=-3}}|0}} in {{CURRENTYEAR}}).{{Inflation-fn|UK}}{{sfn|Matthews|2008|p=72}} In the event, the project ran for over three years, and the final bill came to £200,000 (about £{{formatprice|{{Inflation|UK|200000|1873|r=-3}}|0}} in {{CURRENTYEAR}}).{{sfn|Cookson|2006|p=126}}{{Inflation-fn|UK}} It was intended to open the bridge and the Chelsea Embankment in a joint ceremony in 1874, but the Albert Bridge Company was keen to start recouping the substantially higher than expected costs, and the bridge opened without any formal ceremony on 23 August 1873, almost ten years after its authorisation.{{sfn|Matthews|2008|p=72}} As the law demanded, the Albert Bridge Company then bought Battersea Bridge.{{sfn|Matthews|2008|p=71}}{{sfn|Cookson|2006|p=123}}

Ordish's bridge was {{convert|41|ft|m}} wide and {{convert|710|ft|m}} long, with a {{convert|384|ft|9|in|m|adj=on}} central span.{{sfn|Davenport|2006|p=72}} The deck was supported by 32&nbsp;rigid steel rods suspended from four octagonal [[cast iron]] towers, with the towers resting on cast iron piers.{{sfn|Cookson|2006|p=126}} The four piers were cast at [[Battersea]] and floated down the river into position, at which time they were filled with concrete; at the time they were the largest castings ever made.{{sfn|Cookson|2006|p=126}}{{sfn|Matthews|2008|p=72}} Unlike most other suspension bridges of the time, the towers were positioned outside the bridge to avoid causing any obstruction to the roadway.{{sfn|Matthews|2008|p=72}} At each entrance was a pair of [[toll house|tollbooths]] with a bar between them, to prevent people entering the bridge without paying.{{sfn|Matthews|2008|p=72}}

[[File:Albert Bridge notice.JPG|thumb|upright|Warning to troops]]

The bridge acquired the nickname of "The Trembling Lady" because of its tendency to vibrate, particularly when used by troops from the nearby [[Chelsea Barracks]].{{sfn|Cookson|2006|p=127}} Concerns about the risks of [[mechanical resonance]] effects on suspension bridges, following the 1831 collapse of the [[Broughton Suspension Bridge]] and the 1850 collapse of [[Angers Bridge]], led to notices being placed at the entrances warning troops to break step (i.e. not to march in rhythm) when crossing the bridge;{{sfn|Cookson|2006|p=130}}<ref name="British Pathe 1954-05-24" />{{refn|group=n|The original sign at each end of the Albert Bridge read: "''Officers in command of troops are requested to break step when passing over this bridge''".}} Although the barracks closed in 2008, the warning signs are still in place.{{sfn|Tilly|2002|p=217}}{{refn|group=n|A similar resonance effect caused the temporary closure of the nearby [[Millennium Bridge (London)|Millennium Bridge]] in 2000 shortly after its opening.}}

==Transfer to public ownership==
[[File:Albert Bridge at night, London, UK - Diliff.jpg|thumb|left|300px|The octagonal tollbooths are London's last surviving bridge tollbooths.]]

The Albert Bridge was catastrophically unsuccessful financially. By the time the new bridge opened, the Albert Bridge Company had been paying compensation to the Battersea Bridge Company for nine years, and on completion of the new bridge became liable for the costs of repairing the by then dilapidated and dangerous structure.{{sfn|Pay|Lloyd|Waldegrave|2009|p=70}} The cost of subsidising Battersea Bridge drained funds intended for the building of wide approach roads, making the bridge difficult to reach.{{sfn|Roberts|2005|p=130}} Located slightly further from central London than neighbouring Victoria (Chelsea) Bridge, demand for the new bridge was less than expected, and in the first nine months of its operation only £2,085 (about £{{formatnum:{{Inflation|UK|2085|1874|r=-3}}|0}} in {{CURRENTYEAR}}) were taken in tolls.{{Inflation-fn|UK}}{{sfn|Pay|Lloyd|Waldegrave|2009|p=70}}

In 1877 the [[Metropolis Toll Bridges Act 1877|Metropolis Toll Bridges Act]] was passed, which allowed the [[Metropolitan Board of Works]] to buy all London bridges between [[Hammersmith Bridge|Hammersmith]] and [[Waterloo Bridge|Waterloo]] bridges and free them from tolls.{{sfn|Cookson|2006|p=147}} In 1879, The Albert Bridge, which had cost £200,000 to build, was bought by the Board of Works along with Battersea Bridge for a combined price of £170,000 (about £{{formatprice|{{Inflation|UK|170000|1879|r=-3}}|0}} in {{CURRENTYEAR}}).{{Inflation-fn|UK}}<ref name="Times Freeing" /> The tolls were removed from both bridges on 24 May 1879,{{sfn|Davenport|2006|p=71}} but the octagonal tollbooths were left in place, and today are the only surviving bridge tollbooths in London.{{sfn|Quinn|2008|p=237}}

==Structural weaknesses==
In 1884 the Board of Works' Chief Engineer [[Joseph Bazalgette|Sir Joseph Bazalgette]] conducted an inspection of the bridge and found that the iron rods were already showing serious signs of corrosion.{{sfn|Cookson|2006|p=127}} Over the next three years the staying rods were augmented with steel chains, giving it an appearance more closely resembling a conventional suspension bridge,{{sfn|Tilly|2002|p=217}}{{sfn|Roberts|2005|p=131}} and a new timber deck was laid, at a total cost of £25,000 (about £{{formatprice|{{Inflation|UK|25000|1884|r=-3}}|0}} in {{CURRENTYEAR}}).{{sfn|Davenport|2006|p=71}}{{inflation-fn|UK}} Despite these improvements, Bazalgette was still concerned about its structural integrity and a weight limit of five [[long ton|tons]] was imposed on vehicles using the bridge.{{sfn|Matthews|2008|p=72}}

With a roadway only {{convert|27|ft|m}} wide and subject to weight restrictions from early on, the Albert Bridge was ill-suited to the advent of motorised transport in the 20th century. In 1926 the [[Royal Commission on Cross-River Traffic]] recommended demolition and rebuilding of the bridge to carry four lanes of traffic, but the plan was not carried out because of a shortage of funds in the [[Great Depression]].{{sfn|Roberts|2005|p=132}} It continued to deteriorate, and in 1935 the weight limit was reduced to two tons.{{sfn|Roberts|2005|p=132}}

Because of its ongoing structural weaknesses, in 1957 the [[London County Council]] proposed replacing the Albert Bridge with a more conventional design. A protest campaign led by [[John Betjeman]] resulted in the withdrawal of the proposal, but serious concerns about the integrity of the bridge continued.{{sfn|Cookson|2006|p=127}} In 1964 an experimental [[tidal flow (traffic)|tidal flow]] scheme was introduced, in which only northbound traffic was permitted to use the bridge in the mornings and southbound traffic in the evenings.{{sfn|Matthews|2008|p=72}} The bridge's condition continued to deteriorate however, and in 1970 the [[Greater London Council]] (GLC) sought and obtained consent to carry out strengthening work. In April 1972 the bridge was closed for the work to be carried out.{{sfn|Matthews|2008|p=72}}

===Pedestrianised park proposal===
[[File:Albert Bridge view.JPG|thumb|Concrete central piers were added in 1973, making the bridge an unusual hybrid of a [[cable-stayed bridge]], [[suspension bridge]] and [[beam bridge]].]]

The GLC's solution entailed adding two concrete piers in the middle of the river to support the central span and thus transform the bridge's central section into a [[beam bridge]].{{sfn|Matthews|2008|p=73}} The bridge's main girder was also strengthened, and a lightweight replacement deck was laid. The modifications were intended to be a stopgap measure to extend the bridge's life by five years while a replacement was being considered; in the GLC's estimation the work would last for a maximum of 30&nbsp;years, but the bridge would need to be either closed or replaced well before then.{{sfn|Cookson|2006|p=128}}

In early 1973, the ''[[Architectural Review]]'' submitted a proposal to convert the Albert Bridge into a landscaped public park and pedestrian footpath across the river.{{sfn|Roberts|2005|p=133}} The proposal proved very popular with the area's residents, and a May 1973 campaign led by [[John Betjeman]], [[Sybil Thorndike]] and [[Laurie Lee]] raised a [[petition]] of 2,000&nbsp;signatures for the bridge to be permanently closed to traffic.{{sfn|Matthews|2008|p=73}} Although the GLC reopened the bridge to traffic in July 1973, it also announced its intention to proceed with the ''Architectural Review'' scheme once legal matters had been dealt with.{{sfn|Matthews|2008|p=73}}{{refn|group=n|A modified form of the ''Architectural Review'' design was used in 1999 for the Green Bridge, carrying [[Mile End Park]] over [[Mile End Road]] in East London.}}

The [[Royal Automobile Club]] campaigned vigorously against the pedestrianisation proposal. A publicity campaign fronted by actress [[Diana Dors]] in favour of reopening the bridge was launched, whilst a lobbying group of local residents led by poet [[Robert Graves]] campaigned in support of the GLC's plan.{{sfn|Cookson|2006|p=127}} Graves's campaign collected over a thousand signatures in support, but was vigorously attacked by the [[British Road Federation]], who derided the apparent evidence of public support for the scheme as "sending a lot of students around to [[council flat]]s [where] most people will sign anything without knowing what it is all about".{{sfn|Cookson|2006|p=128}} A [[public enquiry]] of 1974 recommended that the bridge remain open to avoid congestion on neighbouring bridges, and it remained open to traffic with the tidal flow and 2-ton weight limit in place.{{sfn|Matthews|2008|p=73}}

==Present day==
[[File:Albert Bridge columns.JPG|thumb|upright|left|The unusual colour scheme is intended to increase visibility to shipping in poor lighting conditions.]]

In 1990, the tidal flow system was abandoned and the Albert Bridge was converted back to two-way traffic. A [[traffic island]] was installed on the south end of the bridge to prevent larger vehicles from using it. In the early years of the 21st century the Chelsea area experienced a growth in the popularity of large [[four-wheel drive]] cars (so-called [[Chelsea tractor]]s), many of which were over the two-ton weight limit; it was estimated that one third of all vehicles using the bridge were over the weight limit.<ref name="Guardian Tractors" /> In July 2006 the {{convert|27|ft|m|adj=on}} wide roadway was narrowed to a single lane in each direction to reduce the load.<ref name="BBC 2006-07-28" /> Red and white plastic barriers have been erected along the roadway in an effort to protect the structure from damage by cars.<ref name="Builder & Engineer" />

Between 1905 and 1981 the Albert Bridge was painted uniformly green; in 1981 it was repainted yellow. In 1992 it was redecorated and rewired.{{sfn|Roberts|2005|p=135}} Partially as a result, it is now a major West London landmark. The bridge is painted in pink, blue and green to increase visibility in fog and murky light and thus to reduce the risks of ships colliding with the fragile structure during the day.{{sfn|Cookson|2006|p=129}} At night, a network of 4,000 low-voltage [[tungsten-halogen bulb]]s illuminate the bridge. In 1993 the innovative use of long-life low-energy lighting was commended by [[Mary Archer]], at the time Chairwoman of the [[National Energy Foundation]].{{sfn|Cookson|2006|p=130}} Its distinctive and striking current appearance has led to its use as a backdrop for numerous films set in the Chelsea area, such as ''[[A Clockwork Orange (film)|A Clockwork Orange]]'',''[[Absolute Beginners (film)|Absolute Beginners]]'', ''[[Sliding Doors]]'' and ''[[Maybe Baby (2000 film)|Maybe Baby]]''.{{sfn|Roberts|2005|p=133}}

[[File:Albert Bridge, London - Oct 2012.jpg|thumb|350px|4,000 bulbs illuminate the Albert Bridge at night]]

Except for [[Tower Bridge]], built in 1894, the Albert Bridge is the only Thames road bridge in central London never to have been replaced.{{sfn|Cookson|2006|p=126}} Intended as a temporary measure to be removed in 1978, the concrete central piers remain in place,{{sfn|Cookson|2006|p=130}} and although in 1974 its lifespan was estimated at a maximum of 30 years, the bridge is still standing and operational.{{sfn|Cookson|2006|p=128}} The Albert Bridge was protected as a Grade II* [[listed structure]] in 1975, granting it protection against significant alteration without consultation.<ref name="IoE" /> It continues to deteriorate. Although proposals have been drawn up by [[Kensington and Chelsea London Borough Council]] to repair and rescue it,<ref name="Builder & Engineer" /> by March 2008 funds for the repairs were unavailable.<ref name="Paige 2008-03-02" /> As well as structural damage caused by traffic, the timbers underpinning the deck are being seriously rotted by the urine of dogs crossing to and from nearby [[Battersea Park]].{{sfn|Roberts|2005|p=134}}{{refn|group=n|Because of the lack of large open spaces on the north side of the river in this area, large numbers of dogs cross daily to be walked in Battersea Park.}} With multiple measures in place to reduce traffic flow and prolong the life of the bridge, in 2009 it carried approximately 19,000 vehicles per day, the lowest usage of any Thames road bridge in London other than the little-used [[Southwark Bridge]].{{sfn|Pay|Lloyd|Waldegrave|2009|p=71}}

===Refurbishment of 2010–2011===
The bridge was closed to motor vehicles on 15 February 2010 for refurbishment and strengthening. It was originally expected to remain closed for approximately 18 months,<ref name="RBKC" /> but after the condition of the bridge was found to be worse than expected, it was closed for 22 months.<ref name="Osborne 2 December 2011" /> All of the timber in the decking as well as the footway that had rotted away were replaced, with additional timber added for strengthening. Surfaces at the carriageway and pavement decking were replaced. New steel structures were added to strengthen the bridge. All the lightbulbs were changed to more energy-efficient ones. The tollbooths were refurbished. All twelve layers of paint were stripped down until the bare metal was exposed, which was repaired and treated before three new coats of paint were added. The whole project cost £7.2 million of which the [[Royal Borough of Kensington and Chelsea]] provided 25% of the cost and the other 75% was provided by [[Transport for London]].<ref name="RBKC done">{{cite web|url=https://www.rbkc.gov.uk/parking-transport-and-streets/your-streets/streetscape/street-projects/albert-bridge-restoration|title=Albert Bridge restoration|publisher=[[Royal Borough of Kensington and Chelsea]]|accessdate=3 January 2017}}</ref>

It re-opened on 2 December 2011, when two dogs named Prince and Albert, from nearby [[Battersea Dogs and Cats Home]], walked across the bridge. All of the [[Grade II]] features were retained.<ref name="Osborne 2 December 2011" />

==Notes==
{{reflist|group=n}}

==References==
{{reflist|20em
| refs =
<ref name="Thames Bridges Heights">
{{cite web
| title = Thames Bridges Heights
| publisher = Port of London Authority
| url = http://www.pla.co.uk/display_fixedpage.cfm/id/174/site/navigation
| accessdate = 23 August 2011
| archiveurl = https://web.archive.org/web/20110720225414/http://www.pla.co.uk/display_fixedpage.cfm/id/174/site/navigation
| archivedate = 20 July 2011
| deadurl = no
}}
</ref>

<ref name="British Pathe 1954-05-24">
{{cite web
| title = Severn Bridge Model&nbsp;– see 1min16sec into newsreel for a photo of the original 'break step' sign
| date = 24 May 1954
| publisher = British Pathe
| url = http://www.britishpathe.com/record.php?id=47666
| accessdate = 23 August 2011
| archiveurl = https://web.archive.org/web/20110708094854/http://www.britishpathe.com/record.php?id=47666
| archivedate = 8 July 2011
| deadurl = no
}}
</ref>

<ref name="Times Freeing">
{{cite news
| date = 28 June 1880
| title = The Freeing of the Bridges
| newspaper = The Times
| page = 12
}}
</ref>

<ref name="Guardian Tractors">
{{cite news
| last = Temko
| first = Ned
| authorlink = Ned Temko
| date = 20 August 2006
| title = Chelsea choked by its tractors
| newspaper = The Guardian
| location = London
| url = https://www.theguardian.com/news/2006/aug/20/transportintheuk.travelsenvironmentalimpact
| accessdate = 4 June 2009
}}
</ref>

<ref name="BBC 2006-07-28">
{{cite news
| title = Albert Bridge feeling the strain
| date = 28 July 2006
| publisher = BBC News
| url = http://news.bbc.co.uk/2/hi/uk_news/england/london/5225686.stm
| accessdate = 23 August 2011
}}
</ref>

<ref name="Builder & Engineer">
{{cite journal
| title = Albert Bridge undergoes restoration study
| date = 17 March 2008
| journal = Builder & Engineer
| location = London
| url = http://www.builderandengineer.co.uk/news/general/albert-bridge-undergoes-restoration-study-1724.html
| accessdate = 4 June 2009
}}
</ref>

<ref name="IoE">
{{IoE|206969}} Images of England website. Retrieved on 6 June 2009.
</ref>

<ref name="Paige 2008-03-02">
{{cite news
| last = Paige
| first = Elaine
| authorlink = Elaine Paige
| date = 2 March 2008
| title = What's a girl to do against all this blah?
| newspaper = The Daily Telegraph
| location = London
| url = http://www.telegraph.co.uk/comment/personal-view/3555651/Whats-a-girl-to-do-against-all-this-blah.html
| accessdate = 23 August 2011
}}
</ref>

<ref name="RBKC">
{{cite web
| title = Albert Bridge
| work = The Royal Borough of Kensington and Chelsea website
| url = http://www.rbkc.gov.uk/newsroom/councilstatements/albertbridgeclosure.aspx
| accessdate = 23 August 2011
}}
</ref>

<ref name="Osborne 2 December 2011">
{{cite news
| last = Osborne
| first = Lucy
| date = 2 December 2011
| title = Drivers cross the Albert Bridge at last
| newspaper = London Evening Standard
| url = http://www.thisislondon.co.uk/standard/article-24016982-drivers-cross-the-albert-bridge-at-last.do
| accessdate = 9 December 2011
}}
</ref>

}}

'''Bibliography'''
{{refbegin}}
* {{cite book
  | last = Cookson
  | first = Brian
  | year = 2006
  | title = Crossing the River
  | publisher = Mainstream
  | location = Edinburgh
  | isbn = 978-1-84018-976-6
  | oclc = 63400905
  | ref = harv
  }}
* {{cite book
  | last = Davenport
  | first = Neil
  | year = 2006
  | title = Thames Bridges: From Dartford to the Source
  | publisher = Silver Link Publishing
  | location = Kettering
  | isbn = 978-1-85794-229-3
  | ref = harv
  }}
* {{cite book
  | last = Matthews
  | first = Peter
  | year = 2008
  | title = London's Bridges
  | publisher = Shire
  | location = Oxford
  | isbn = 978-0-7478-0679-0
  | oclc = 213309491
  | ref = harv
  }}
* {{cite book
  | last = Pay
  | first = Ian
  | last2 = Lloyd
  | first2 = Sampson
  | last3 = Waldegrave
  | first3 = Keith
  | year = 2009
  | title = London's Bridges: Crossing the Royal River
  | publisher = Artists' and Photographers' Press
  | location = Wisley
  | isbn = 978-1-904332-90-9
  | oclc = 280442308
  | ref = harv
  }}
* {{cite book
  | last = Quinn
  | first = Tom
  | year = 2008
  | title = London's Strangest Tales
  | publisher = Anova Books
  | location = London
  | isbn = 978-1-86105-976-5
  | ref = harv
  }}
* {{cite book
  | last = Roberts
  | first = Chris
  | year = 2005
  | title = Cross River Traffic
  | publisher = Granta
  | location = London
  | isbn = 978-1-86207-800-0
  | ref = harv
  }}
* {{cite book
  | last = Smith
  | first = Denis
  | year = 2001
  | title = Civil Engineering Heritage London and the Thames Valley
  | publisher = Thomas Telford
  | location = London
  | isbn = 978-0-7277-2876-0
  | ref = harv
  }}
* {{cite book
  | last = Tilly
  | first = Graham
  | year = 2002
  | title = Conservation of Bridges
  | publisher = Taylor & Francis
  | location = Didcot
  | isbn = 978-0-419-25910-7
  | ref = harv
  }}
{{refend}}

==Further reading==
{{Commons category|Albert Bridge}}
{{refbegin}}
* {{cite book
  | last = Loobet
  | first = Patrick
  | year = 2002
  | title = Battersea Past
  | pages = 48–49
  | publisher = Historical Publications Ltd
  | isbn = 978-0-948667-76-3
  }}
{{refend}}

{{Crossings navbox
| structure = [[Crossings of the River Thames|Crossings]]
| place = [[River Thames]]
| upstream = [[Battersea Bridge]]
| downstream = [[Chelsea Bridge]]
| bridge = Albert Bridge, London <br /> [[British national grid reference system|Grid reference]]: {{gbmappingsmall|TQ273775}}
}}

{{Featured article}}

[[Category:Beam bridges]]
[[Category:Bridges across the River Thames]]
[[Category:Bridges and tunnels in London]]
[[Category:Bridges completed in 1873]]
[[Category:Former toll bridges]]
[[Category:Grade II* listed bridges]]
[[Category:Grade II* listed buildings in Kensington and Chelsea]]
[[Category:Grade II* listed buildings in Wandsworth]]
[[Category:Road bridges in England]]
[[Category:Suspension bridges in the United Kingdom]]
[[Category:Transport in Kensington and Chelsea]]
[[Category:Transport in the London Borough of Wandsworth]]
[[Category:1873 establishments in England]]