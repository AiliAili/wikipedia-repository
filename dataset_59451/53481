{{Infobox person
| name          = Samuel A. DiPiazza, Jr.
| image         = Image:Samuel DiPiazza 2008.jpg
| alt           = 
| caption       = DiPiazza speaks during a press conference in Tianjin, China 26 September 2008.
| birth_name    = 
| birth_date    =  <!-- {{birth date and age|YYYY|MM|DD}} for living people. For people who have died, use {{Birth date|YYYY|MM|DD}}. -->
| birth_place   =  
| nationality   = 
| citizenship        = 
| education          = [[University of Alabama]]
| alma_mater         = [[University of Houston]]
| other_names   = 
| occupation    = 
| years_active  = 
| home_town          =  
| known_for     = 
| boards     = [[AT&T]] 
| awards             = 
| website            = [http://www.pwc.com/extweb/aboutus.nsf/docid/D160FADBD066CB6C85256E590003044C Official PwC bio]
}}

'''Samuel A. DiPiazza, Jr.''', was appointed [[Chief executive officer|CEO]] of [[PricewaterhouseCoopers]] on January 1, 2002.  Before this he served as the chairman of the PricewaterhouseCoopers [[United States|U.S.]] firm.

==Early life and education==
He received dual degrees in [[accountancy|accounting]] and [[economics]] from the [[University of Alabama]]  where he was a member of [[Theta Chi Fraternity]]. He then went on to get his [[Master of Accountancy]] from the [[University of Houston]].
==Career==
He started work with the firm [[Coopers & Lybrand]], one of the two firms along with Price Waterhouse, that merged in 1998 to form PricewaterhouseCoopers.  DiPiazza joined C&L in 1973 and was named partner six years later in 1979.  Later in his career he was chosen to head first the [[Birmingham, Alabama]], and then the [[Chicago, Illinois|Chicago]] offices.  In 1992 he was appointed managing partner of the [[Midwestern United States|Midwest region]].  In 1994 he moved to a position in the [[New York City|New York]] office, similar to his previous one, and added client service vice president to his resume.  Finally, in 2000 he was elected chairman and senior partner of the United States firm, a role he filled until 2002 when he took over the role of chairman and managing partner for the firm as a whole.

==Boards and memberships==
DiPiazza served a three-year term as a trustee of the Financial Accounting Foundation.  He also serves on the International Advisory Board of Junior Achievement, the Executive Council of the Inner City Scholarship Fund and is President of [[Big Brothers Big Sisters of New York City|Big Brothers Big Sisters New York City]].  He serves on the Executive Committee of the National Corporate Theater Fund and as a board member with the [[New York City Ballet]]. He is also on the board of the [[World Trade Center Memorial Foundation]]. He has stepped down as the Chairman of PwC on June 30, 2009.<ref name="stepsown"/> On October 12, 2005, Mr. DiPiazza was appointed to a second four-year term as chairman of the PricewaterhouseCoopers global, beginning on January 1, 2006. He stepped down as the Global Chairman on June 30, 2009 making way for Dennis Nally.<ref name="stepsown">http://www.mydigitalfc.com/jobs/pwc-global-chairman-step-down-873</ref>

Recently, Mr. DiPiazza has been a member of the [[Committee on Capital Markets Regulation]] which has been mandated to assess the impact of the current U.S. regulatory environment on the competitiveness of the U.S. in the global capital marketplace.

==See also==
*[[List of chief executive officers]]
*[[List of AT&T board members]]

==References==
{{reflist}}

==External links==
*[http://www.pwc.com/extweb/aboutus.nsf/docid/D160FADBD066CB6C85256E590003044C Official PwC bio]
{{AT&T}}
{{DEFAULTSORT:Dipiazza, Samuel}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American businesspeople]]
[[Category:American accountants]]
[[Category:University of Alabama alumni]]
[[Category:University of Houston alumni]]
[[Category:PricewaterhouseCoopers people]]