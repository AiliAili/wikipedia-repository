The '''Cambridge Philosophical Society''' ('''CPS''') is a scientific society at the [[University of Cambridge]].  It was founded in 1819. The name derives from the medieval use of the word [[philosophy]] to denote any research undertaken outside the fields of [[theology]] and [[medicine]]. The society was granted a royal charter by William IV in 1832.

The society has published several [[scientific journal]]s, including ''Biological Reviews'' (established 1926) and ''[[Mathematical Proceedings of the Cambridge Philosophical Society]]'' (formerly entitled ''Proceedings of the Cambridge Philosophical Society'', published since 1843).  ''Transactions of the Cambridge Philosophical Society'' was published between 1821–1928, but was then discontinued.

==Fellow of the Cambridge Philosophical Society (F.C.P.S.)==

Members of the Society are called [[Fellows]] and are permitted to use the 'FCPS' post-nominal title.  Fellows are usually academics or graduate students involved in mathematical or scientific research within the University.  A Fellow must be recommended in writing by both a Fellow of the Society who has been a member for at least three years and a person of appropriate standing, who knows the candidate in a professional capacity.  Approved candidates are elected at open meetings of the Society following proposal at Council Meetings.<ref>{{cite web|url=http://cambridgephilosophicalsociety.org/membership.shtml|title=Membership|work=cambridge philosophical society|date=|accessdate=}}</ref>

The equivalent organisation for philosophers is the [[Cambridge University Moral Sciences Club|Cambridge Moral Sciences Club]].

==References==
{{reflist}}

== Further reading ==
*{{cite book | author=[[Alfred Rupert Hall|A. Rupert Hall]] | title=The Cambridge Philosophical Society: A History 1819-1969 | year=1969 | publisher=Cambridge Philosophical Society | isbn=0-9500348-1-9 }}

== External links ==
{{wikisource|Proceedings of the Cambridge Philosophical Society}}
* [http://www.cambridgephilosophicalsociety.org/ Cambridge Philosophical Society]
* [http://www.cam.ac.uk/ University of Cambridge]

{{University of Cambridge}}

[[Category:1819 establishments in England]]
[[Category:Clubs and societies of the University of Cambridge|Philosophical]]
[[Category:Learned societies of the United Kingdom]]
[[Category:Philosophical societies in the United Kingdom]]
[[Category:Scientific societies]]
[[Category:Organizations established in 1819]]


{{philo-org-stub}}
{{UK-org-stub}}
{{UCambridge-stub}}