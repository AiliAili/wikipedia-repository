{{Orphan|date=January 2017}}

{{Use dmy dates|date=December 2016}}
The '''inverse Warburg theory''' is a molecular and bioenergetic based model of neurodegeneration.,<ref>Demetrius LA & Simon DK. An inverse-Warburg effect and the origin of Alzheimer's disease.  Biogerontology. 13(6):583-94 (2012)</ref><ref>Demetrius LA, Magistretti PJ & Pellerin L.  Alzheimer's disease: the amyloid hypothesis and the Inverse Warburg effect.  Front Physiol 5: 522 (2015).</ref><ref>Demetrius LA & Driver JA.  Preventing Alzheimer’s disease by means of natural selection. J R Soc Interface. 12(102): 20140919 (2015).</ref> which was proposed to explain certain biochemical and epidemiological observations that characterize the sporadic form of Alzheimer’s disease (AD), namely:

(1) The prolonged prodromal phase and the up-regulation of oxidative phosphorylation (OxPhos) in the very early stages of the disease – the [[inverse Warburg effect]].

(2) The incidence of AD is exponential and increases with age<ref>Ziegler-Graham K1, Brookmeyer R, Johnson E, Arrighi HM. Worldwide variation in the doubling time of Alzheimer's disease incidence rates. Alzheimers Dement. 4(5):316-23. (2008)</ref>

(3) The inverse comorbidity of AD with certain forms of cancer,<ref>Driver JA, Beiser A, Au R, Kreger BE, Splansky GL, Kurth T, Kiel DP, Lu KP, Seshadri S & Wolf PA.  Inverse association between cancer and Alzheimer's disease: results from the Framingham Heart Study.  BMJ. 344:e1442 (2012).</ref><ref>Tabarés-Seisdedos R & Rubenstein JL. Inverse cancer comorbidity: a serendipitous opportunity to gain insight into CNS disorders.  Nat Rev Neurosci. 14(4):293–304 (2013).</ref>

The inverse Warburg theory, in sharp contrast to the [[amyloid cascade hypothesis]]<ref>Hardy, J. A. & Higgins, G. A. Alzheimer's disease: the amyloid cascade hypothesis. Science 256, 184–185 (1992).</ref> proposed to explain the rare familial forms of AD, considers mitochondria as critical factors in the origin of the sporadic form of AD in addition to the interaction between neurons and astrocytes as the cell types implicated in the development of the disorder.

The model is a bioenergetic paradigm for age-related neurodegenerative diseases.  The central tenet of the model is that complex neurodegenerative diseases, whose incidence increases with age, can be understood within an energy-based genetic and metabolic system.  Mitochondria are the primary energy producing organelles.  The energy transformation from the environment to other forms of energy for use by the cells and tissues is achieved by OxPhos.  This process regulates a large class of cellular functions and is central to the production of ATP.  Mitochondrial activity produces reactive oxygen species (ROS), which are critical as they act as signal transduction molecules.<ref>
Shadel GS & Horvath TL. Mitochondrial ROS Signaling in Organismal Homeostasis. Cell. 163(3):560-9. (2015).</ref>   The Inverse Warburg theory postulates that the origin of sporadic AD, and the histopathological effects such as neuronal death, are initiated by the molecular disorder and metabolic defects intimately linked to the aging process at the molecular and cellular level.  These events involve both the neurons, which are post-mitotic cells, and the astrocytes which are mitotic and are a source of energy for neuronal activities.  The Inverse Warburg theory thus posits that sporadic AD is driven by the process of aging which induces the following cascade of events:

''(i) Mitochondrial dysregulation'': An age-dependent impairment in the activities of enzymes in the Electron transport chain (ETC) of these energy producing organelles.<ref>Petrosillo G1, De Benedictis V, Ruggiero FM, Paradies G.  Decline in cytochrome c oxidase activity in rat-brain mitochondria with aging. Role of peroxidized cardiolipin and beneficial effect of melatonin.  J Bioenerg Biomembr. 45(5):431-40 (2013).</ref> This biochemical defect may reduce the activity of cytochrome c oxidase enzymes resulting in a decline in the generation of ATP thus compromising neuronal function.

''(ii) Metabolic reprogramming:'' This refers to the upregulation of OxPhos, a key component of the Inverse Warburg effect.  This metabolic alteration occurs as a compensatory response to the decline in energy following impairment in the mitochondria.  Metabolic reprogramming is a response to mitochondrial dysregulation – a stochastic process that entails the emergence of heterogenous populations of neurons '''Figure 1.''' [[File:Figure 1 neuronal energetics.jpg|thumbnail|Neuronal Bioenergetics]]
   
One population of neurons, which remain largely intact, will have slightly decreased OxPhos ('''Type 1 neurons'''), due to age-related protein misfolding and impaired enzyme efficiencies, whereas a second population of neurons, containing mitochondria with large defects, are characterized by increased OxPhos to compensate for reduced energy production ('''Type 2 neurons''').   Type 1 neurons will have minor mitochondrial defects whereas Type 2 neurons will have more pronounced mitochondrial defects.  The two types of neurons ― relatively healthy (with normal OxPhos), and impaired (with increased OxPhos activity due to the compensatory effect) ― must compete for nutrients.  Impaired neurons (Type 2), since they contain some mitochondria with upregulated activity, have an advantage in the competition for resources.  With normal aging, the two populations eventually reach equilibrium, typically with healthy neurons far outnumbering impaired neurons. The temporal duration of this equilibrium condition defines the prodromal phase of AD.  As long as the balance is maintained, disease will not ensue.

== Implications ==

The inverse Warburg theory has implications for the diagnosis and the treatment of AD.

''Diagnosis:'' The model distinguishes between two stages where a significant change in metabolic activity occurs:

(a) ''Hypermetabolic phase:'' This defines an increase in metabolic rate in different cortical regions with predominance in AD-specific regions,<ref>Cohen AD, Price JC, Weissfeld LA, James J, Rosario BL, Bi W, Nebes RD, Saxton JA, Snitz BE, Aizenstein HA, Wolk DA, Dekosky ST, Mathis CA, Klunk WE. Basal cerebral metabolism may modulate the cognitive effects of Abeta in mild cognitive impairment: an example of brain reserve.  J Neurosci. 29(47):14770-8 (2009).</ref><ref>Ashraf A, Fan Z, Brooks DJ, Edison P.  Cortical hypermetabolism in MCI subjects: a compensatory mechanism?  Eur J Nucl Med Mol Imaging. 42(3):447–58 (2015).</ref><ref>Ossenkoppele R, Madison C, Oh H, Wirth M, van Berckel BN, Jagust WJ.  Is verbal episodic memory in elderly with amyloid deposits preserved through altered neuronal function?  Cereb Cortex. 24(8):2210-8 (2014).</ref> This is a compensatory response to the mitochondrial dysregulation which derives from the aging process.  The intensity of this compensatory hypermetabolism and its persistence are predictive of the onset and degree of neurodegeneration.

(i)  A small constant change in metabolic rate corresponds to low risk for AD

(ii) A large change in metabolism which varies in intensity over time corresponds to a high risk for AD

(b) ''Hypometabolic phase:'' This pertains to a reduction in metabolic rate. This condition, which subsequently arises decades after the incidence of the hypermetabolic phase, occurs at a point when the clinical symptoms of AD are present.  The changes in metabolic rate can be assessed by measuring cerebral glucose metabolism using PET scans <ref>Hoffman JM1, Welsh-Bohmer KA, Hanson M, Crain B, Hulette C, Earl N, Coleman RE.  FDG PET imaging in patients with pathologically verified dementia. J Nucl Med. 41(11):1920-8. (2000)</ref>

== Therapeutic strategy ==

The central thesis of the theory is that the transition towards AD requires an external factor which can disrupt the dynamic metabolic equilibrium thus inducing the shift from normal to pathological aging. Patients with type 2 diabetes (T2DM) are insulin resistant and are at a significantly increased risk of developing AD.<ref>Craft S, Baker LD, Montine TJ, et al. Intranasal insulin therapy for Alzheimer disease and amnestic mild cognitive impairment: a pilot clinical trial. Arch Neurol. 69(1):29–38 (2012)</ref>  These observations are consistent with the [[Inverse Warburg Effect]] where insufficient ability to transport glucose within the CNS would render hypermetabolic neurons at risk.  It is feasible that diabetes disrupts the equilibrium between healthy and impaired neurons.   Therefore, viable therapeutic strategies would involve the administration of metabolites to maintain the dynamical equilibrium state, or agents which enhance glucose uptake.

Metabolic interventions that have been proposed are:

(a) '''Lactate therapy:''' The introduction of lactate to ensure that intact healthy neurons have a selective advantage.  In the presence of increased lactate, impaired Type 2 neurons with increased OxPhos activity will have decreased viability.

(b) '''Other metabolic therapies:'''
  
(i) '''Intranasal insulin administration''' – Several clinical trials have recently revealed that intranasal insulin improves both memory performance and metabolic integrity of the brain in patients with AD or exhibiting mild cognitive impairment,.<ref>Craft S, Baker LD, Montine TJ, et al. Intranasal insulin therapy for Alzheimer disease and amnestic mild cognitive impairment: a pilot clinical trial. Arch Neurol. 69(1):29–38 (2012)</ref><ref>Freiherr J, Hallschmid M, Frey WH 2nd, Brünner YF, Chapman CD, Hölscher C, Craft S, De Felice FG, Benedict C.  Intranasal insulin as a treatment for Alzheimer's disease: a review of basic research and clinical evidence. CNS Drugs. 27(7):505-14 (2013)</ref>  Intranasal administration can quickly deliver insulin to the central nervous system across the olfactory epithelium.  Whether improved cognition following intranasal insulin administration is mediated by increased insulin signalling or improved glucose uptake is still unresolved.

(ii) '''Mimetics of glucagon-like peptide 1 (GLP-1)'''- Research in diabetes has shown that mimetics of GLP-1, including exedin-4, liraglutide and lixisenatide, can improve insulin sensitivity in T2DM.  GLP-1 is a gut-derived incretin hormone that enhances glucose-stimulated insulin secretion.  Treatment of transgenic animal models of AD with GLP-1 mimetics resulted in neuroprotective effects and improved memory.<ref>Yarchoan M & Arnold SE. Repurposing Diabetes Drugs for Brain Insulin Resistance in Alzheimer Disease.  Diabetes. 63 (7): 2253–2261(2014).</ref>  It is currently unknown if the protective effects associated with GLP-1 mimetic administration are mediated by signalling processes or improved metabolite processing.  Clinical trials are currently underway testing GLP-1 mimetics in AD patients. 
 
(iii) '''Metformin.'''  
Metformin is a biguanide that suppresses glucose production in the liver (hepatic gluconeogenesis), increases insulin sensitivity, and enhances glucose uptake by reducing GLUT4 endocytosis.<ref>Yang J and Holman GD. Long-term metformin treatment stimulates cardiomyocyte glucose transport through an AMP-activated protein kinase-dependent reduction in GLUT4 endocytosis.  Endocrinology. 147(6):2728-36. (2006).</ref>  Patients with T2DM and AD that receive metformin, have a lower rate of cognitive impairment than untreated patients.<ref>
Domínguez RO, Marschoff ER, González SE, Repetto MG, Serra JA. Type 2 diabetes and/or its treatment leads to less cognitive impairment in Alzheimer’s disease patients. Diabetes Res Clin Pract. 98(1):68–74 (2012).</ref>  It is currently unknown if metformin exerts a beneficial effect on memory via signalling or improved insulin sensitivity/glucose uptake.

<big>'''The Amyloid cascade hypothesis versus the inverse Warburg theory:'''</big>

The [[amyloid cascade hypothesis]] postulates that the production of amyloid is the cause of AD and that removal of amyloid will increase memory and enhance cognition.   However, epidemiological studies have consistently shown a poor correlation between amyloid plaque load and the degree of dementia in AD patients.<ref>Price JL, McKeel DW Jr, Buckles VD, Roe CM, Xiong C, Grundman M, Hansen LA,
Petersen RC, Parisi JE, Dickson DW, Smith CD, Davis DG, Schmitt FA, Markesbery WR, Kaye J, Kurlan R, Hulette C, Kurland BF, Higdon R, Kukull W, Morris JC. Neuropathology of nondemented aging: Presumptive evidence for preclinical Alzheimer disease. Neurobiol Aging 30:1026–1036 (2009)</ref><ref>Morris GP, I Clark IA, Vissel B. Inconsistencies and Controversies Surrounding the Amyloid Hypothesis of Alzheimer's Disease.  Acta Neuropathol Commun. 2: 135 (2014).</ref> These studies suggest that three categories of relations between amyloid and disease incidence can be distinguished:

(a) Individuals with high amyloid load and no dementia

(b) Individuals with no amyloid load and with mild or moderate dementia

(c) Individuals with amyloid load and dementia.

The amyloid cascade model is only applicable to class (c), which may indeed be a very small group.
The inverse Warburg theory posits that AD is due to a collapse in the stability of the neuronal population thus generating a shift from healthy aging to pathological aging.
Therapeutic strategies involve metabolic interventions which will maintain the homeostatic conditions which characterizes the process of healthy aging. These interventions do not depend on the existence of a correlation between amyloid load and disease severity. Metabolic interventions will be applicable to the three classes of populations.
Metabolic interventions recognize the heterogeneity of the disease and the multifactorial nature of its etiology.  Anti-amyloid therapies based on immunological interventions ignore the heterogeneity of the disease. Such therapies may only be applicable to a small subset of the elderly population.  However, metabolic interventions, based on the tenets of the Inverse Warburg Theory may hold great promise in treating the vast majority of sporadic cases of AD.

==References==
{{reflist}}

[[Category:Chemistry]]