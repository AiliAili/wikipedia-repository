{{Infobox airport
| name         = Bergen Airport, Sandviken
| nativename   =
| image        = Bergen Airport, Sandviken 1.jpg
| image-width  = 300px
| caption      =
| IATA         =
| ICAO         =
| type         = Private
| owner        = Bergen Municipality
| operator     = [[Fonnafly]]
| city-served  = [[Bergen]], [[Norway]]
| location     = Kristiansholm, [[Sandviken, Norway|Sandviken]], Bergen
| metric-elev  = yes
| elevation-f  = 0
| elevation-m  = 0
| coordinates  = {{coord|60.4095|N|005.3180|E|region:NO|display=inline,title}}
| pushpin_relief         = yes
| pushpin_map            = Norway
| pushpin_mapsize        = 300
| pushpin_label          = Sandviken
| pushpin_label_position = right
| pushpin_map_caption    =
| metric-rwy   = yes
| r1-number    = 03/21
| r1-length-m  =
| r1-length-f  =
| r1-surface   = Water
| r2-number    = 14/32
| r2-length-m  =
| r2-length-f  =
| r2-surface   = Water
}}

'''Bergen Airport, Sandviken''' ({{lang-no|Bergen sjøflyplass, Sandviken}}) is a [[water airport]] and [[heliport]] situated in the [[Sandviken, Norway|Sandviken]] neighborhood of [[Bergen]], [[Norway]]. The [[aerodrome]] is located on the artificial peninsula of Kristiansholm. It is currently serving [[seaplane]] and helicopters operated by [[Fonnafly]] aimed at [[air taxi]] services at cruise ship tourists. The airport is owned by Bergen Municipality through [[Bergen Port Authority]] and is part of the [[Bergen Port]]. Operations are carried out by Fonnafly.

The airport opened in 1935, although it was not connected to land until 1938. Services were first carried out by [[Norwegian Air Lines]] on a coastal route to [[Oslo]]. From 1936 also a north-bound service was introduced. Sandviken was taken over by [[Luftwaffe]] during the [[Second World War]], but little used. Norwegian Air Lines resumed services in 1946 and the following year [[West Norway Airlines]] was established with their base at Sandviken. The airlines moved their scheduled flights to [[Bergen Airport, Hjellestad]] from 1948 to 1951. West Norway Airlines resumed scheduled flights from Sandviken in 1952, until they ceased operations in 1957. [[Bergen Airport, Flesland]] opened in 1955, thus removing most of the traffic from Sandviken.

==History==
The first air route in Norway was established in 1920 by [[Det Norske Luftfartsrederi]]. Using a [[Supermarine Channel]], it received state subsidies to operate a route from Bergen via Haugesund to Stavanger. The service lasted from 16 August to 15 September.<ref>Nerdrum (1986): 29–30</ref> In Bergen the airline used a water airport in Sandviken.<ref>Østerbø: 21</ref>

Norwegian Air Lines announced the establishment of a seaplane route from Oslo and around the coast to Bergen ahead of the 1935 season. The city selected Breiviken as its preferred location, situated further out than the ultimate location. However, this site would require much work and a temporary location was needed. For this Kristiansholm was selected. A molo had been built there in 1920; this created difficulties for ship traffic in the area, but was a suitable location to anchor up a seaplane. The location was also suitable due to the vicinity of the city. Funding was granted from Bergen Municipality and a burned down lot at Sandvikstorget was selected.<ref>Haaland: 165</ref>

The route commenced on 11 June 1935, using a Junkers Ju 52. Flights were only flown during summer and they relied on [[visual flight rules]] and could not fly in the dark or in poor weather.<ref>Nerdrum: 75–80</ref> Bergen was the terminus during the first season, but from 1936 a second leg was introduced, extending services northwards to [[Tromsø]].<ref>Nerdrum: 87–90</ref> The airport's location was popular due to its central location and the municipality therefore decided to make it permanent. Holmsundet, a shallow sound, was covered with a {{convert|100|m|sp=us|adj=on}} molo, making the airport fixed to land. This hindered ship traffic in the area and caused several docks and companies to have to relocate to still handle sea traffic.<ref>Haaland: 166</ref>

The airport at Sandviken was taken over by [[Luftwaffe]], the German air force, during the [[German occupation of Norway]] between 9 April 1940 and 8 May 1945. They were primarily interested in using [[Herdla Airport]] which they constructed on [[Askøy]], and Sandviken was little used. In 1942 and 1944 it saw use by a few detachments of [[Arado Ar 196]] reconnaissance aircraft.<ref>Hafsten: 319</ref>

After the war, air services commenced in 1946. The first year it consisted of a route to Oslo and to Stavanger, operated by Norwegian Air Lines using Junkers Ju 52.<ref>Nerdrum: 152–156</ref> Herdla Airport became the site of the [[air traffic control]] for the Bergen after the war.<ref name=a38>Aarsand: 38</ref>  West Norway Airlines was established at Sandviken in 1947. Operating a fleet dominated by the [[Republic RC-3 Seabee]], they operated mostly [[air taxi]] and [[air ambulance]] services.<ref>Østerbø: 22</ref>

From 1948 Norwegian Air Lines introduced the 37-seat [[Short Sandringham]] on their Oslo routes. This required a much long runway, which was not possible without interfering with ship traffic in the port. Bergen Municipality therefore started looking for a suitable site further away from town. Both Herdla and Flatøy were discarded due to distances and the need for ferry transport, and [[Hjellestad]] was found the most suitable site. [[Bergen Airport, Hjellestad]] therefore served as Bergen's main airport from 23 August 1948 to the end of the 1951 season.<ref name=a38 />

By then Norway Air Lines' successor [[Scandinavian Airlines]] had dropped the direct route to Oslo and the sole airline serving Bergen was [[West Norway Airlines]] operating its [[Short Sealand]]s.<ref>Aarsand: 41</ref> From the 1952 season it operated two routes out of Bergen. One was southwards to Haugesund and Stavanger,<ref name=r15>Reitan: 15</ref> and one northwards, to Ålesund, Molde, Kristiansund and Trondheim.<ref name=v105>Vik: 105</ref>

The 1955 opening of [[Bergen Airport, Flesland]] caused the airline to terminate its Stavanger route.<ref>Hagby: 59</ref> However, flights to Haugesund continued until 29 September 1956.<ref name=r15 /> Northward the route lasted until the end of the 1957 summer season. The need for a coastal seaplane route fell away the following year with the opening of [[Ålesund Airport, Vigra]].<ref>Hjelle: 14</ref>

During a storm in October 2003 one of the piers at the airport sank. It took with it two aircraft, which were subject to severe damages, but remaired. The damage amounted to NOK&nbsp;2 million.<ref>{{cite news |url=http://www.ba.no/nyheter/article916048.ece |title=Kommunen kreves for to millioner |date=23 March 2004 |last=Ottesen |first=Ken Andre |language=Norwegian |work=[[Bergensavisen]]}}</ref> A debate arose in 2006 concerning the construction of a helipad at Sandviken. Belma AS, which owns two of Fonnafly's aircraft, intended to anchor a barge and use it as a helipad. The proposal was met by opposition from neighbors, who wished to keep down noise pollution the mixed commercial and residential area of Sandviken. Belma stated that vicinity to cruise ships was necessary to conduct their business.<ref>{{cite news |title=Fly forbannet på ny landingsplass  |last=Mæland |first=Pål |work=[[Bergens Tidende]] |date=12 November 2006 |page=8 |language=Norwegian}}</ref> The helipad was however given permission to operate.<ref name=horing>{{cite web |url=http://www3.bergen.kommune.no/BKSAK_filer/bksak%5C2013%5CBR2%5C2013174507-3929728.pdf |title=Høring – søknad om fornyelse av konsesjon for Bergen sjøflyplass, Sandviken og Bergen helikopterplass, Sandviken |publisher=Bergen Municipality |date=4 June 2013 |language=Norwegian |format=PDF}}</ref>

In the last half of the 2000s there was a political discussion about converting the Kristiansholm to either a cruise ship terminal, or a recreational area. The latter position was argued due to it being the only site in the part of Sandviken which could grant seaside access to the public. However, nothing came of the plans.<ref>{{cite news |title=Friområde på Kristiansholm |work=[[Bergens Tidende]] |date=12 June 2008 |page=2 |language=Norwegian}}</ref>

==Facilities==
[[File:Sandviken Airport.jpg|thumb|The airport and molo]]
The airport is located on Kristiansholm in the Sandviken neighborhood of Bergen, situated X km from the city center. Legally the site consists of two aerodromes, a water airport and a heliport. Both are owned by Bergen Municipality through Bergen Port Authority, although the airport is operated by Fonnafly. Kristiansholm is a peninsula sticking into Byfjorden. The water airport has two runways, aligned 03/21 and 14/32, respectively.<ref name=horing />

Operations are mostly targeted at cruise ship tourists visiting Bergen. The proximity to the cruise ship port is an advantage for Fonnafly. However, the airport is located in a mixed commercial and residential area and there is a high degree of noise pollution impact due to this. Fonnafly operates about 250 annual aircraft movements at the water airport and 450 for the heliport. However, the concession permits 900 and 800, respectively.<ref name=horing />

==References==
{{reflist|30em}}

==Bibliography==
{{commons category|Bergen Airport, Sandviken}}
* {{cite book |last=Aarsand |first=Knut |last2=Bakka |first2=Dag |title=Hjellestad sjøflyhavn 1948–1952 |work=Årsskrift 2004–2005 |publisher=Fana Historielag |issn=0809-5256 |pages=38–42 |language=Norwegian}}
* {{cite book |last=Hafsten |first=Bjørn |last2=Larsstuvold |first2=Ulf |last3=Olsen |first3=Bjørn |last4=Stenersen |first4=Sten |title=Flyalarm: Luftkrigen over Norge 1939–1945 |year=1991 |publisher=Sem & Stenersen |location=Oslo |language=Norwegian |isbn=82-7046-058-3 |url=http://www.nb.no/nbsok/nb/824921f23d8278cf9650ee046c3209f8}}
* {{cite book |last=Haaland |first=Anders |title=Bergen havn gjennom 900 år – II: Perioden 1900–1945 Knutepunkthavnen |publisher=Bergen Port Authority |location=Bergen |year=2005 |isbn=82-7128-387-1 |language=Norwegian}}
* {{cite book |last=Hjelle |first=Bjørn Owe |title=Ålesund lufthavn Vigra |location=Valderøya |year=2007 |isbn=978-82-92055-28-1 |language=Norwegian}}
* {{cite book |last=Nerdrum |first=Johan |title=Fugl fønix: En beretning om Det Norske Luftfartselskap  |year=1986 |publisher=Gyldendal Norsk Forlag |location=Oslo |isbn=82-05-16663-3 |language=Norwegian}}
* {{cite book |last=Østerbø |first=Kjell |title=Da Bergen tok av |year=2005 |publisher=Avinor |language=Norwegian |isbn=82-303-0495-5}}
* {{cite book |last=Reitan |first=Sverre Utne |title=Luftfarten på Haugalandet fra 1914 til 2004 |year=2003 |publisher=Eget Forlag |location=Karmøy |language=Norwegian}}
* {{cite book |last=Vik |first=Knut L. |title=Strinda den gang da |chapter=Trondheim lufthavn Jonsvatnet 1935–1939 |publisher=Strinda historielag |location=Trondheim |year=2003 |language=Norwegian |issn=1502-2315 |isbn=82-92357-02-5 |pages=85–107 |url=http://www.strindahistorielag.no/jonsvatnet-lufthavn-1935-39-aarbok2003.pdf |archiveurl=https://web.archive.org/web/20131218133504/http://www.strindahistorielag.no/jonsvatnet-lufthavn-1935-39-aarbok2003.pdf |archivedate=18 December 2013 |accessdate=18 December 2013}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway}}

[[Category:Water aerodromes in Norway]]
[[Category:Heliports in Norway]]
[[Category:Airports in Hordaland]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Buildings and structures in Bergen]]
[[Category:1935 establishments in Norway]]
[[Category:Airports established in 1935]]
[[Category:Military installations in Bergen]]