<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Lloyd C.II–C.IV
 | image=WW1 aircraft Lloyd C.II.jpg
 | caption=Lloyd C.II
}}{{Infobox Aircraft Type
 | type=[[Reconnaissance aircraft]]
 | national origin=[[Austria-Hungary]]
 | manufacturer=[[Ungarische Lloyd Flugzeug und Motorenfabrik AG]] / [[Magyar Lloyd Repülőgép és motorgyár Részvény-Társaság]]
 | designer=
 | first flight=1915
 | introduced=
 | retired=
 | status=
 | primary user=[[KuKLFT]]
 | number built=100 × C.II<br>ca. 50 × C.III<br>ca. 40 × C.IV
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Lloyd C.II''' and its derivatives, the '''C.III''' and '''C.IV''' were reconnaissance aircraft produced in Austria-Hungary during the First World War.<ref name="Jane's">Taylor 1989, 581</ref>  They were based on the Lloyd company's pre-war [[Lloyd C.I|C.I]] design, and like it, were conventional biplanes with swept-back wings.

==Design and development==
After the outbreak of World War I, the original aircraft was refined somewhat by Lloyd designers Wizina and von Melczer,<ref name="Gunston">Gunston 2003, 184</ref> featuring a reduced wingspan and wing area but increased weight.<ref name="Grosz">Grosz 2002</ref> An 8&nbsp;mm [[Schwarzlose machine gun]] was added on a semi-circular mount for an observer.<ref name="Murphy">Murphy 2005, 108</ref>

Beginning in 1915, one hundred examples of this type were built – fifty by Lloyd at their plant in [[Aszód]], and another fifty by [[Wiener Karosserie- und Flugzeugfabrik|WKF]] in [[Vienna]].

Apart from their service with the [[KuKLFT|Austro-Hungarian flying service]], ten C.IIs saw service with Poland. These were captured in [[Malopolska]] in November 1918 and were used as trainers until being withdrawn from service in 1920.

The '''C.III''' was almost identical except for the use of a 120&nbsp;kW (160&nbsp;hp) [[Austro-Daimler]] engine, which increased the top speed to 133&nbsp;km/h (83&nbsp;mph).<ref name="Grosz" /> Production again was by both Lloyd and WKF, with total production amounting to 50-60 machines.

The '''C.IV''' also used the Austro-Daimler engine, and small batches were produced by both Lloyd and WKF.<ref name="Grosz" />

==Variants==
* '''C.II''' with Heiro engine an 14.00 m wingspan (100 built)
* '''C.III''' with Austro-Daimler engine and 14.00 m wingspan (8 or 16 built by Lloyd, 43 by WKF)
* '''C.IV''' with Austro-Daimler engine produced by Lloyd with 14.52 m (47&nbsp;ft 8 in) wingspan (47 built, plus one converted)

==Operators==
;{{flag|Austria-Hungary}}
*[[KuKLFT]]

==Specifications (C.II)==
{{Aircraft specs
|ref=Grosz 2002, German & Austro-Hungarian Aircraft Manufacturers 1908 - 1918<ref name=Treadwell>{{cite book|last=Treadwell|first=Terry C.|title=German & Austro-Hungarian Aircraft Manufacturers 1908 - 1918|year=2010|publisher=Amberley Publishing|location=Stroud|isbn=978 1 4456 0102 1|pages=268–271}}</ref> 
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=8.8
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|upper span m=14.8
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=13.8
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=3.15
|height ft=
|height in=
|height note=
|wing area sqm=38.0
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=900
|empty weight lb=
|empty weight note=
|gross weight kg=1,329
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hiero (Mar)]]
|eng1 type=6-cyl. water-cooled in-line piston engine
|eng1 kw=108<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=128
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=400
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=3,000
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=5.6
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=One {{convert|8|mm|in|abbr=on|3}} [[Schwarzlose machine gun]] in the observers position, mounted on a rail.
*{{convert|90|kg|lb|abbr=on|0}} of bombs
|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
* {{cite book |last= Grosz |first= Peter M. |title=Austro-Hungarian Army Aircraft of World War One |year=2002 |publisher=Flying Machine Press |location= Colorado |pages= }}
* {{cite book |last= Gunston |first= Bill |title=World Encyclopedia of Aircraft Manufacturers |year=1993 |publisher=Naval Institute Press |location= Annapolis |pages= }}
* {{cite book |last= Murphy |first= Justin D. |title=Military Aircraft: Origins to 1918 |year=2005 |publisher=ABC-Clio |location= Santa Barbara |pages= }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}
*{{cite book|last=Treadwell|first=Terry C.|title=German & Austro-Hungarian Aircraft Manufacturers 1908 - 1918|year=2010|publisher=Amberley Publishing|location=Stroud|isbn=978 1 4456 0102 1|pages=268–271}}

<!-- ==External links== -->

{{Lloyd aircraft}}
{{KuKLFT C-class designations}}

[[Category:Austro-Hungarian military reconnaissance aircraft 1910–1919]]
[[Category:Lloyd aircraft]]