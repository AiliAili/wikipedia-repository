{{Infobox Officeholder
|name        = Thomas E. Bramlette
|image       = Thomas E. Bramlette (Kentucky).jpg
|order1      = 23rd
|office1     = Governor of Kentucky
|term_start1 = September 1, 1863
|term_end1   = September 3, 1867
|lieutenant1 = [[Richard Taylor Jacob|Richard T. Jacob]]
|predecessor1= [[James Fisher Robinson|James F. Robinson]]
|successor1  = [[John L. Helm]]
|office2     = Member of the [[Kentucky General Assembly]]
|term2       = 1841
|birth_date  = {{birth date|1817|1|3|mf=y}}
|birth_place = [[Cumberland County, Kentucky]]
|death_date  = {{death date and age |1875|01|12|1817|01|03}}
|death_place = [[Louisville, Kentucky]]
|restingplace = [[Cave Hill Cemetery]]
|party       = [[Whig Party (United States)|Whig]]<br/>[[Democratic Party (United States)|Democrat]]
|spouse      = Sallie Travis<br/>Mary E. Adams
|profession  = [[Lawyer]]
|religion    =
|signature   = Thomas-E-Bramlette-sig.jpg
|signature_alt = Thos. E. Bramlette
|allegiance   = [[United States]] [[Union (American Civil War)|Union]]
|branch       = [[Union Army]]
|serviceyears = 1861{{spaced ndash}}1862
|rank         = [[File:Union Army colonel rank insignia.png|35px]] [[Colonel]]
|unit         = [[3rd Regiment Kentucky Volunteer Infantry|3rd Kentucky Infantry]]
|battles      = [[American Civil War|Civil War]]
}}
'''Thomas Elliott Bramlette''' (January 3, 1817 &ndash; January 12, 1875) was the [[List of Governors of Kentucky|23rd]] [[Governor of Kentucky]]. He was elected in 1863 and guided the state through the latter part of the [[American Civil War|Civil War]] and the beginning of [[Reconstruction era of the United States|Reconstruction]]. At the outbreak of the war, Bramlette put his promising political career on hold and enlisted in the [[Union Army]], raising and commanding the [[3rd Regiment Kentucky Volunteer Infantry|3rd Kentucky Infantry]]. In 1862, [[President of the United States|President]] [[Abraham Lincoln]] appointed him [[United States District Attorney|district attorney]] for Kentucky. A year later, he was the Union Democrats' nominee for governor. Election interference by the Union Army gave him a landslide victory over his opponent, [[Charles A. Wickliffe]]. Within a year, however, federal policies such as recruiting Kentucky [[African-American|Negroes]] for the Union Army and suspending the writ of ''[[habeas corpus]]'' for Kentucky citizens caused Bramlette to abandon his support of the Lincoln administration and declare that he would "bloodily baptize the state into the [[Confederate States of America|Confederacy]]".

After the war, Bramlette issued a general [[pardon]] for most ex-Confederates in the state. He opposed ratification of the [[Fourteenth Amendment to the United States Constitution|Fourteenth]] and [[Fifteenth Amendment to the United States Constitution|Fifteenth]] Amendments and the establishment of the [[Freedmen's Bureau]] in Kentucky. Among his accomplishments not related to the war and its aftermath were the reduction of the state's debt and the establishment of the Kentucky Agricultural and Mechanical College (now the [[University of Kentucky]]). Following his term as governor, Bramlette returned to his legal practice in [[Louisville, Kentucky|Louisville]]. He died January 12, 1875 and was buried in [[Cave Hill Cemetery]].

==Early life==
Thomas E. Bramlette was born on January 3, 1817, at Elliott's Cross Roads in [[Cumberland County, Kentucky|Cumberland]] (now [[Clinton County, Kentucky|Clinton]]) County, Kentucky.<ref name=allen106>Allen, p. 106</ref> He was the son of [[Colonel]] Ambrose S. and Sarah (Elliott) Bramlette.<ref name=harrison112>Harrison, p. 112</ref> His father served two terms in the [[Kentucky Senate]] and several terms in the [[Kentucky House of Representatives]].<ref name=allen106/>

Bramlette studied law, was admitted to the [[Bar association|bar]] in 1837 at the age of 20, and began practicing in [[Louisville, Kentucky]].<ref name=powell/><ref name=nga>NGA Bio</ref> In September of that year, Bramlette married Sallie Travis, the first of his two wives.<ref name=powell>Powell, p. 56</ref> The couple had two children, Thomas and Corinne.<ref name=webb93>Webb, p. 93</ref>

Bramlette's political career began in 1841, when he was elected to represent Clinton County in the [[Kentucky General Assembly|General Assembly]].<ref name=powell/> In 1848, Governor [[John J. Crittenden]] appointed Bramlette [[Commonwealth's Attorney]].<ref name=powell/> He resigned the position in 1850 to continue his legal practice, relocating to [[Columbia, Kentucky]] in 1852.<ref name=powell/> He was the [[Whig Party (United States)|Whig]] nominee for his district's seat in the [[United States House of Representatives|House of Representatives]] in 1853, but was defeated by [[Democratic Party (United States)|Democrat]] [[James Chrisman]].<ref name=allen106/> In 1856, he was elected as a judge in Kentucky's 6th Judicial District, serving with distinction for five years.<ref name=powell/>

==Military service==
Bramlette resigned his judgeship and accepted a commission as a [[Colonel (United States)|colonel]] in the [[Union Army]] on August 7, 1861.<ref name=kyng>Trowbridge, "Kentucky's Military Governors"</ref> In violation of Kentucky's agreement to remain neutral in the [[American Civil War|Civil War]], he raised and commanded the [[3rd Regiment Kentucky Volunteer Infantry|3rd Kentucky Infantry]].<ref name=harrison112/><ref name=powell/> On September 19, the 3rd Kentucky marched on [[Lexington, Kentucky|Lexington]] to forestall a peace conference scheduled there on September 21 and to arrest the state's junior [[United States Senate|Senator]], [[John C. Breckinridge]].<ref name=rawley39>Rawley, p. 39</ref> A delay allowed Breckinridge to escape before the arrest was made, and he enlisted in the [[Confederate States Army|Confederate Army]] shortly thereafter.<ref name=rawley39/>

Bramlette resigned his military commission on July 13, 1862 at [[Decherd, Tennessee]].<ref name=kyng /> He returned to Louisville to accept [[President of the United States|President]] [[Abraham Lincoln]]'s offer to become [[United States District Attorney]] for Kentucky.<ref name=eok>''Encyclopedia of Kentucky''</ref> During his tenure in this position, he vigorously enforced Kentucky's wartime laws against [[Confederate States of America|Confederates]] and Confederate sympathizers.<ref name=powell />

==Governor of Kentucky==
Union Democrats chose [[Joshua Fry Bell]] as their candidate for governor of Kentucky in 1863, but Bell was skeptical of Kentucky's future with the Union and withdrew his name from consideration.<ref name=powell/> The party's central committee chose Bramlette to replace Bell, and Bramlette declined a commission as a [[brigadier general]] in the Union Army to make the race.<ref name=powell/><ref name=kyng /> During the election, Union forces intimidated and jailed supporters of Bramlette's opponent, former governor [[Charles A. Wickliffe]].<ref name=powell/> As a result, Bramlette carried the election by a margin of nearly 4-to-1.<ref>"Kentucky's Governors: 1851{{spaced ndash}}1879"</ref> During his term, he turned down an offered seat in the [[United States House of Representatives|U.S. House of Representatives]] as well as a nomination to become the Democratic candidate for [[Vice-President of the United States|Vice-President]] in 1864.<ref name=nga/>

===Civil War===
In December 1863, Bramlette addressed the General Assembly, declaring that the state had fulfilled its quota of soldiers for the Union army.<ref name=webb94>Webb, p. 94</ref> January 4 of the following year, he proclaimed that rebel sympathizers would be held responsible for all [[guerrilla]] raids in the state, and specified stiff fines and imprisonment for anyone found to be aiding the guerrillas.<ref name=webb94/>

Although Bramlette assumed the governorship as a staunch supporter of the Union cause, within a year he issued a proclamation that he would "bloodily baptize the state into the Confederacy".<ref name=powell/> The reasons for Bramlette's reversal were many. He took issue with General [[Stephen Burbridge]]'s decision to enlist [[African-American|Negroes]] from Kentucky for military service, asking that this measure only be taken if Kentucky failed to meet her quota.<ref name=webb94/> The situation worsened when on July 5, 1864, President Lincoln suspended the writ of ''[[habeas corpus]]'' for citizens of the Commonwealth.<ref name=harrison113>Harrison, p. 113</ref> Burbridge continually menaced Kentucky's citizens, interfering with the [[United States presidential election, 1864|presidential election of 1864]], and banishing [[Lieutenant Governor of Kentucky|Lieutenant Governor]] [[Richard T. Jacob]] from the state.<ref name=webb94/> When the General Assembly re-convened in January 1865, Bramlette continued to voice his opposition to the Union's tactics. Nevertheless, he urged passage of the [[Thirteenth Amendment to the United States Constitution|Thirteenth Amendment]], maintaining that the institution of slavery was "irrevocably doomed".<ref name=webb95>Webb, p. 95</ref>

===Reconstruction era===
Despite his disagreements with the Lincoln administration, Bramlette proclaimed a day of [[fasting]] and [[prayer]] upon receiving news of Lincoln's assassination.<ref name=webb95/> The General Assembly petitioned new president [[Andrew Johnson]] to call an end to [[martial law]] in the state.<ref name=webb95/> The tension between the state and federal governments remained, however. Bramlette announced that every "white male citizen" twenty-one years of age who had resided in the Commonwealth for at least two years would be eligible to vote.<ref name=webb95/> Spurred on by the Democratic governor's actions, Kentucky gave control of both houses of the General Assembly and five of its nine congressional seats to Democrats.  President Johnson received the message, ending martial law and restoring ''habeas corpus'' in Kentucky.<ref name=webb95/>

When the General Assembly convened in December 1865, Bramlette sought to restore harmony in the state by issuing [[clemency|pardons]] to most ex-Confederates.<ref name=webb95/> He and the majority of the General Assembly opposed passage of the [[Fourteenth Amendment to the United States Constitution|Fourteenth]] and [[Fifteenth Amendment to the United States Constitution|Fifteenth]] amendments, and Bramlette protested the establishment of the [[Freedmen's Bureau]] in the Commonwealth.<ref name=harrison113/>

Bramlette was very proud of those of his accomplishments not related to the Civil War, including the reduction of the state's debt and the establishment of the Agricultural and Mechanical College (later, the [[University of Kentucky]]).<ref name=harrison113/> He supported the construction of [[turnpikes]] financed by government bonds, the development of natural resources, and encouraged immigration to obtain adequate labor to support reconstruction efforts.<ref name=harrison113/>

==Later life and death==
<!-- Deleted image removed: [[File:ThomasBramletteGrave.jpg|thumb|240px|right|Governor Bramlette's grave in [[Cave Hill Cemetery]]]] -->
Following his term as governor, Bramlette conducted a failed campaign to become a [[United States Senate|U.S. Senator]].<ref name=powell/> He married Mary E. Graham Adams in 1874, two years after the death of his first wife.<ref name=powell/><ref name=eok/> He returned to his law practice in Louisville, and became a patron of many charitable organizations.<ref name=harrison113/>

Bramlette died in Louisville on January 12, 1875 following a brief illness.<ref name=webb96>Webb, p. 96</ref> He is buried at [[Cave Hill Cemetery]] in Louisville.<ref name=powell/>

==See also==
{{Portal|United States Army|American Civil War|Kentucky}}
*[[Kentucky in the Civil War]]

==References==
{{reflist|colwidth=30em}}
{{commons category}}

===Bibliography===
*{{cite book |last=Allen |first=William B. |title=A History of Kentucky: Embracing Gleanings, Reminiscences, Antiquities, Natural Curiosities, Statistics, and Biographical Sketches of Pioneers, Soldiers, Jurists, Lawyers, Statesmen, Divines, Mechanics, Farmers, Merchants, and Other Leading Men, of All Occupations and Pursuits |publisher=Bradley & Gilbert |year=1872 |url=https://books.google.com/?id=s_wTAAAAYAAJ |accessdate=2008-11-10}}
*{{cite book |title=The Encyclopedia of Kentucky |publisher=Somerset Publishers |location=[[New York City]], [[New York (state)|New York]] |year=1987 |isbn=0-403-09981-1}}
*{{cite book |last=Harrison |first=Lowell H. |authorlink=Lowell H. Harrison |editor=Kleber, John E. |others=Associate editors: [[Thomas D. Clark]], [[Lowell H. Harrison]], and James C. Klotter |title=The Kentucky Encyclopedia |year=1992 |publisher=The University Press of Kentucky |location=[[Lexington, Kentucky]] |isbn=0-8131-1772-0}}
*{{cite web |url=http://www.nga.org/cms/home/governors/past-governors-bios/page_kentucky/col2-content/main-content-list/title_bramlette_thomas.html |title=Kentucky Governor Thomas Elliott Bramlette |publisher=National Governors Association |accessdate=2012-04-03}}
*{{cite web |url=http://www.kdla.ky.gov/resources/Governors%20of%20Kentucky_pg3.htm#Bramlette |archive-url=https://archive.is/20110528125115/http://www.kdla.ky.gov/resources/Governors%20of%20Kentucky_pg3.htm |dead-url=yes |archive-date=2011-05-28 |title=Kentucky's Governors: 1851 – 1879 |publisher=Kentucky Department of Libraries and Archives |accessdate=2007-05-06}} 
*{{cite book |last=Powell |first=Robert A. |title=Kentucky Governors |publisher=Bluegrass Printing Company |location=[[Danville, Kentucky]] |year=1976 |oclc=2690774}}
*{{cite book |last=Rawley |first=James A. |title=Turning Points in the Civil War |publisher=Lincoln University of Nebraska Press |year=1989 |isbn=978-0-8032-8935-2}}
*{{cite web|last=Trowbridge |first=John M. |title=Kentucky's Military Governors |work=Kentucky National Guard History e-Museum |publisher=Kentucky National Guard |url=http://kynghistory.ky.gov/people/Kentucky+Military+Governors.htm |accessdate=2010-04-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20100527181343/http://kynghistory.ky.gov/people/Kentucky+Military+Governors.htm |archivedate=2010-05-27 |df= }}
*{{cite book |last=Webb |first=Ross A. |title=Kentucky's Governors |editor=[[Lowell H. Harrison]] |publisher=The University Press of Kentucky |location=[[Lexington, Kentucky]] |year=2004 |isbn=0-8131-2326-7}}

==Further reading==
*{{cite journal |title=Governor Thomas E. Bramlette |journal=The Register of the Kentucky Historical Society |volume=5 |date=January 1907 |pages=27–28}}

==External links==
* {{Find a Grave|6860277}}
* [http://www.rootsweb.com/~kygenweb/kybiog/jefferson/bramlette.te.txt Biography from Lawyers and Lawmakers of Kentucky]

{{s-start}}
{{s-off}}
{{succession box|title=[[Governor of Kentucky]]|before=[[James Fisher Robinson|James F. Robinson]]|after=[[John L. Helm]]|years=1863&ndash;1867}}
{{s-end}}
{{Governors of Kentucky}}

{{good article}}

{{Authority control}}

{{DEFAULTSORT:Bramlette, Thomas E.}}
[[Category:1817 births]]
[[Category:1875 deaths]]
[[Category:Burials at Cave Hill Cemetery]]
[[Category:District attorneys]]
[[Category:Governors of Kentucky]]
[[Category:Kentucky Commonwealth's Attorneys]]
[[Category:Kentucky Democrats]]
[[Category:Kentucky lawyers]]
[[Category:Kentucky state court judges]]
[[Category:Kentucky Whigs]]
[[Category:19th-century American politicians]]
[[Category:Members of the Kentucky House of Representatives]]
[[Category:People from Clinton County, Kentucky]]
[[Category:Politicians from Louisville, Kentucky]]
[[Category:People of Kentucky in the American Civil War]]
[[Category:Southern Unionists in the American Civil War]]
[[Category:Union Army colonels]]
[[Category:Union state governors]]
[[Category:Democratic Party state governors of the United States]]