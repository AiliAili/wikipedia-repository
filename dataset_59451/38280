{{good article}}
{{Infobox University Boat Race
| name= 106th Boat Race
| winner = Oxford
| margin =  1 and 1/4 lengths
| winning_time=  18 minutes 59 seconds
| date= 2 April 1960
| umpire =  [[Kenneth Payne]]<br>(Cambridge)
| prevseason= [[The Boat Race 1959|1959]]
| nextseason= [[The Boat Race 1961|1961]]
| overall =58&ndash;47
}}
The 106th [[The Boat Race|Boat Race]] took place on 2 April 1960. Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]]. Oxford went into the race as reigning champions having won the [[The Boat Race 1959|previous year's race]].  In a race umpired by former Cambridge rower [[Kenneth Payne]] and attended by [[Princess Margaret, Countess of Snowdon]], it was won by Oxford by one-and-a-quarter lengths in a time of 18 minutes 59 seconds, their second consecutive victory, which took the overall record in the event to 58&ndash;47 in Cambridge's favour.<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited | title = Boat Race – Results| accessdate = 17 July 2014}}</ref>

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 12 July 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race takes place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 12 July 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities; it is followed throughout the United Kingdom and, as of 2014, broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=9 July 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 12 July 2014}}</ref>  Oxford went into the race as reigning champions, having won the [[The Boat Race 1959|1959 race]] by six lengths,<ref name=results/> while Cambridge led overall with 58 victories to Oxford's 46 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 12 July 2014}}</ref>

[[File:Princess Margaret 1965.jpg|right|thumb|[[Princess Margaret, Countess of Snowdon]] ''(pictured in 1965)'' was the first member of the [[British Royal Family]] to attend the Boat Race in forty years.<ref name=royal/>]]
Cambridge were coached by Chris Addison (who rowed for Cambridge in the [[The Boat Race 1939|1939 race]]), [[James Crowden]] (who rowed for the Light Blues in the [[The Boat Race 1951|1951]] and [[The Boat Race 1952|1952 races]]), A. T. Denby ([[The Boat Race 1958|1958]] Blue), [[David Jennens]] (who rowed three times between 1949 and 1951) and J. R. Owen ([[The Boat Race 1959|1959]] race).  Oxford's coaching team comprised [[Hugh Edwards (rower)|Jumbo Edwards]] (who rowed for Oxford in [[The Boat Race 1926|1926]] and [[The Boat Race 1930|1930]]), J. L. Fage (an Oxford Blue in 1958 and 1959) and L. A. F. Stokes (who rowed for the Dark Blues in the [[The Boat Race 1951|1951]] and [[The Boat Race 1952|1952]] races).<ref>Burnell, pp. 110&ndash;111</ref>  The Oxford crew had opted to use {{convert|7.5|lb|kg|adj=on}} oars, {{convert|2|lb|kg|1}} lighter than normal.<ref name=best/>

The main race was umpired for the seventh and penultimate time by the former British Olympian [[Kenneth Payne]] who had rowed for Cambridge in the [[The Boat Race 1932|1932]] and [[The Boat Race 1934|1934 races]].<ref>Burnell, pp. 49, 74</ref><ref>{{Cite web | url = http://www.sports-reference.com/olympics/athletes/pa/kenneth-payne-1.html | publisher = [[Sports Reference]] | title = Kenneth Payne Bio, Stats, and Results| accessdate = 28 December 2014}}</ref>  [[Antony Armstrong-Jones, 1st Earl of Snowdon|Antony Armstrong-Jones]], who had coxed Cambridge to victory in the [[The Boat Race 1950|1950 race]],<ref>{{Cite web | url = https://books.google.co.uk/books?id=cVUEAAAAMBAJ&pg=PA36 | work = [[Life (magazine)|Life]] |  title = Introduction to Tony, the Princess's fiance | page = 36 | date = 7 March 1960}}</ref> and his fiance [[Princess Margaret, Countess of Snowdon|Princess Margaret]] were spectators on board the Cambridge [[Launch (boat)|launch]] ''Amaryllis''.<ref name=royal>{{Cite news | title = The Princess Sees Oxford Victory | work = [[The Guardian]] | date = 3 April 1960 | page =1}}</ref>

==Crews==
Both crews weighed an average of 12&nbsp;[[Stone (unit)|st]] 9&nbsp;[[Pound (mass)|lb]] (80.1&nbsp;kg).  Cambridge's crew contained three rowers who had taken part in the 1959 race, [[Bow (rowing)|bow]] J. R. Owen, G. H. Brown and John Beveridge.  Oxford saw two rowers return from the previous year's race, Alexander Lindsay and D. C. Rutherford.<ref name=burn79/>  Former Olympic gold medallist [[Jack Beresford]], writing in ''[[The Observer]]'' said the Dark Blues "are good watermen: their co-ordination is precise, their boat control admirable".<ref name=best>{{Cite news | work = [[The Observer]] | first = Jack |last = Beresford|authorlink = Jack Beresford | title = It's one of Oxford's best crews | date = 27 March 1961 | page = 31}}</ref>  Beresford suggested that the Cambridge crew were "not fully under control ... there is a definite jerkiness and hurry about their work."<ref name=best/>

Only one participant in this year's race was registered as non-British in American Townsend Swayze, the former [[Harvard University]] captain.<ref>Burnell, p. 39</ref><ref>{{Cite news | work = [[The Miami News]] | date = 26 March 1961 | page = 4 | title = Yanks invade British spectacle |url = https://news.google.com/newspapers?nid=2206&dat=19610326&id=DZIyAAAAIBAJ&sjid=X-oFAAAAIBAJ&pg=4027,4776002}}</ref>
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || R. C. I. Bate || [[St Edmund Hall, Oxford|St Edmund Hall]] || 12 st 5 lb || J. R. Owen || [[St John's College, Cambridge|St John's]] || 11 st 8 lb
|-
| 2 || R. L. S. Fishlock ||  [[St Edmund Hall, Oxford|St Edmund Hall]]  || 12 st 0 lb ||  S. R. M. Price || [[Trinity College, Cambridge|1st & 3rd Trinity]] || 12 st 0 lb
|-
| 3 || T. S. Swayze|| [[Wadham College, Oxford|Wadham]] || 13 st 2 lb || F. P. T. Wiggins || [[Lady Margaret Boat Club]] || 12 st 12.5 lb
|-
| 4 || A. T. Lindsay || [[Magdalen College, Oxford|Magdalen]] || 12 st 10 lb || J. Parker || [[Lady Margaret Boat Club]] || 12 st 8 lb
|-
| 5 || I. L. Elliottt || [[Keble College, Oxford|Keble]] || 13 st 8 lb || G. H. Brown (P) || [[Trinity Hall, Cambridge|Trinity Hall]] || 13 st 11.5 lb
|-
| 6 || D. C. Rutherford (P)  || [[Magdalen College, Oxford|Magdalen]]  || 12 st 12 lb || J. Beveridge || [[Jesus College, Cambridge|Jesus]] || 13 st 2 lb
|-
| 7 || J. R. Chester || [[Keble College, Oxford|Keble]] || 12 st 5 lb || E. T. C. Johnstone || [[Lady Margaret Boat Club]] || 12 st 12 lb
|-
| [[Stroke (rowing)|Stroke]] || C. M. Davis || [[Lincoln College, Oxford|Lincoln]] || 12 st 6 lb || P. W. Holmes || [[Lady Margaret Boat Club]] || 12 st 5 lb
|-
| [[Coxswain (rowing)|Cox]] || P. J. Reynolds || [[St Edmund Hall, Oxford|St Edmund Hall]]  || 9 st 1 lb || R. T. Weston || [[Selwyn College, Cambridge|Selwyn]] || 9 st 1 lb
|-
!colspan="7"|Source:<ref name=burn79>Burnell, p. 79</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 51&ndash;52</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the [[coin flipping|toss]] and elected to start from the Middlesex station, handing the Surrey side of the river to Cambridge.<ref name=burn79/>  The Dark Blues made the quicker start and held a [[Glossary of rowing terms#Equipment / parts of the boat|canvas-length]] lead after ten strokes, extending to half a length after the first minute.  With the bend of the river in their favour, Oxford pulled further ahead and were three-quarters of a length up by [[Craven Cottage]].  Passing the Mile Post in a record time (beating that set in the [[The Boat Race 1934|1934]]), Oxford spurted at [[Harrods Furniture Depository]] to hold held a clear water advantage, and were two lengths ahead by the time the crews passed below [[Hammersmith Bridge]].<ref name=bitter/>  

As the Oxford [[Coxswain (rowing)|cox]], P. J. Reynolds steered the Dark Blues towards the Middlesex shore, allowing Cambridge to start to reduce the deficit.  Difficult conditions between Chiswick and [[Barnes Railway Bridge|Barnes Bridge]] slowed the pace of the race, but a spurt just before Barnes allowed Cambridge to close the gap to four seconds.  Aggressive steering from Cambridge's Roger Weston was held off by Oxford who took advantage of the tide, and in a sprint finish,<ref name=bitter/> Oxford won by one-and-a-quarter lengths in a time of 18 minutes 59 seconds.<ref name=burn79/>  It was Oxford's second consecutive victory for just the second time since the First World War.  It was also the first time since the [[The Boat Race 1921|1921 race]] that the event was witnessed by a member of the [[British Royal Family]].<ref name=royal/><ref>{{Cite news | title = Boat Race coxswains show their imagination | date = 4 April 1960 | work = [[The Times]] | page =14 | issue = 54737}}</ref>

Ian Thomson, writing in ''[[The Observer]]'', described the event as "a really titanic struggle ... an exceptionally gallant race."<ref name=bitter>{{Cite news | title = Fought out to the bitter end | first = Ian | last = Thomson | date = 3 April 1960 | page = 32 | work = [[The Observer]]}}</ref>  The rowing correspondent for ''[[The Guardian]]'' stated that the race saw "one of the most gallant finishing efforts by a crew two lengths in arrears at the half-way mark."<ref>{{Cite news | work = [[The Observer]] | title = Cambridge endurance not enough to overhaul Oxford | page = 14 | date = 4 April 1960}}</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1960}}
[[Category:1960 in English sport]]
[[Category:1960 in rowing]]
[[Category:The Boat Race]]
[[Category:April 1960 sports events]]