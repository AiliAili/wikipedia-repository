{{Use dmy dates|date=April 2012}}
[[File:Edward Teshmaker Busk.jpg|thumb|right|Edward Teshmaker Busk]]
[[File:Edward Teshmaker Busk Grave.jpg|thumb|150px|right|The grave of Edward Teshmaker Busk in [[Aldershot Military Cemetery]]]]
Lieutenant '''Edward Teshmaker Busk''', [[London Electrical Engineers]]. RE(T) (8 March 1886 – 5 November 1914) was an [[England|English]] pioneer of early aircraft design, and the designer of the first full-sized efficient inherently stable aeroplane.

He was the son of Thomas Teshmaker Busk (1852–1894) and Mary Busk née Acworth (1854–1935), of Hermongers, [[Rudgwick]], Sussex. After attaining First Class Honours in Mechanical Sciences at [[Cambridge]] in June 1912 he became Assistant Engineer at the newly formed Royal Aircraft Factory, Farnborough, later the [[Royal Aircraft Establishment]]. Here he devoted much of his time to the mathematics and dynamics of stable flight.

In the early years of powered flight inherent stability in an aircraft was a most important quality. Busk took his theories into the air and tried them out in practice. In 1913 this work  was used in the R.E.1 (Reconnaissance Experimental), claimed as the first inherently stable aeroplane, and resulted in the development of the [[B.E.2c]].

The remarkable feature of this design was that there was no single device that was the cause of the stability. The stability resulted from detailed design of each part of the aircraft, with due regard to its relation to, and effect on, other parts in the air. Weights and areas were so arranged that under most conditions the machine would tend to right itself.

Busk was killed on 5 November 1914 while flying a [[B.E.2]] which caught fire at Laffans Plain (now Farnborough Airfield), near Aldershot. He was buried at [[Aldershot Military Cemetery]] with full military honours.<ref>[http://www.cwgc.org/search/casualty_details.aspx?casualty=359187 CWGC entry]</ref>

His genius and his courage were recognised by the posthumous award of the Gold Medal of the [[Royal Aeronautical Society#History|Aeronautical Society of Great Britain]], and amongst the many letters of condolence received by his mother was one from King George V.

His youngest brother, Hans Acworth Busk (b. 1894), was reported missing on 6 January 1916, last seen flying a heavy bombing aeroplane against the Turks at Gallipoli. They were both survived by their mother (author of "E.T. Busk, a pioneer in flight"), by sister Mary Agnes Dorothea Morse (1888–1960) and by brother Henry Gould Busk (1890–1956).

== Memorials ==
* Busk Crescent, Farnborough.
* The Busk Memorial, R.A.E. Farnborough, a small lily pond and fountain with memorial plaque.
* Holy Trinity Church, Rudgwick.
* The Busk Studentship in Aeronautics at Cambridge University

== References ==
{{Reflist}}
* ''[[Jerome Clarke Hunsaker|Hunsaker, Jerome C.]] Dynamical Stability of Aeroplanes, U. S. Navy and Massachusetts Institute of Technology''
* ''Cross & Cockade International, Volume 14 - 1983, Number 3. E. T. Busk, an Irreparable Loss : Marvin L Skelton''

{{DEFAULTSORT:Busk, Edward Teshmaker}}
[[Category:1886 births]]
[[Category:1914 deaths]]
[[Category:Royal Engineers officers]]
[[Category:English aviators]]
[[Category:Aviators killed in aviation accidents or incidents in the United Kingdom]]
[[Category:Burials at Aldershot Military Cemetery]]
[[Category:Accidental deaths in England]]
[[Category:British Army personnel of World War I]]