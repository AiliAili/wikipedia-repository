{{multiple issues|
{{Refimprove|date=November 2009}}
{{tone|date=April 2014}}
}}

{{Infobox person
| name        = Albert Caquot
| image       = Albert Caquot à Polytechnique.jpg
| image_size  = 250x350px
| caption     = Albert Caquot, wearing a dark suit in the foreground, in [[École Polytechnique]] premises, Paris, ca 1900.
| birth_date  = 1 July 1881
| birth_place = [[Vouziers]], [[Ardennes (department)|Ardennes]], France
| death_date  = {{death date and age|1976|11|28|1881|7|1|df=y}}
| death_place = [[Paris]], France
| nationality = France
| occupation  = engineer and inventor 
| education   = [[École Polytechnique]], 1899<br/>[[Ponts et Chaussées]]
| prizes      = [[Wilhelm Exner Medal]], 1962
}}

'''Albert Irénée Caquot''' (1 July 1881 – 28 November 1976) was considered as the "best living French engineer"<ref>Necrologic note by Maurice Roy, reports of the Academy of Science, number 193398-77, meeting of 19 January 1977.</ref> during half a century. He received the “[[Croix de guerre 1914–1918 (France)]]” (military honor) and was [[Légion d'honneur|Grand-croix of the Légion d’Honneur]] (1951). He was a member of the [[French Academy of Sciences]] from 1934 till his death. In 1962, he was awarded the [[Wilhelm Exner Medal]].<ref>Editor, ÖGV. (2015). Wilhelm Exner Medal. Austrian Trade Association. ÖGV. Austria.</ref>

==Biography==
His parents, Paul Auguste Ondrine Caquot and wife Marie Irma (born Cousinard)<ref name="Polytech.Fiche"/> owned a family farm in [[Vouziers]], in the [[Ardennes]], near the Belgian border. His father taught him modernism, by installing at their place electricity and telephone as early as 1890. One year only after high school, at eighteen years old, he was admitted at the [[École polytechnique|Ecole Polytechnique]]<ref name="Polytech.Fiche">[http://www.bibliotheque.polytechnique.edu/jsp/accueil.jsp?CODE=64303226&LANGUE=1 Website of the library of the École Polytechnique], thumb index « ''BCX Catalogs –> Polytechnicien family'' », search for « Albert Caquot », you get : « Caquot, Albert Irénée (X 1899 ; 1881-1976) » ; you can then choose to click on "Fiche matricule" for more detailed information.</ref> ("year" 1899). Six years later, he graduated in the [[Corps of Bridges and Roads (France)|“Corps des Ponts et Chaussées”]].

===The scientist and designer===
From 1905 to 1912, he was a project manager in [[Troyes]] (Aube), and was pointed out for major civil work improvements he undertook with the city sewer system. This protected the city from the centennial flood of the [[:fr:Crue de la Seine de 1910|River Seine in 1910]]. In 1912, he joined a leading structural engineering firm where he applied his unique talent of structure designer.

Albert Caquot conducted outstanding research that was immediately applied in construction. His major contributions include:

* [[reinforced concrete]] design, and structural engineering in a broader sense. In 1930, he defined the intrinsic curve and explained why the elasticity theory was not sufficient any more for modern structures design.
* [[geotechnics]] and [[foundation (architecture)|foundation]] design. He stated the corresponding states theorem (CST). In 1933, his publication on the stability of pulverulent and coherent material received an admiring report from the [[French Academy of Sciences]], where he was elected life member in 1934. In 1948, with [[Jean Kérisel]] (1908–2005), his son-in-law and disciple, he developed an advanced theory extremely important for passive earth pressure (LINK) where there is soil-wall friction. This principle has been broadly applied ever since for the design of ground engineering structures such as retaining walls, tunnels and foundation piles.
* the revival of [[cable-stayed bridges]] with reinforced concrete (Donzère Mondragon bridge, 1952), which he envisioned with long spans, even crossing the English Channel. In 1967, he designed a conceptual double-deck bridge of this type with 810 m-wide spans and two 25 m-wide decks stages accommodating 8 lanes for cars, 2 for rail and 2 for skytrain.
[[File:Ponkail.JPG|200px|thumb|right|The new La Caille bridge near [[Annecy]], 1928.]]
In the course of his life, Albert Caquot taught mechanical science for a long time in three of the most prominent French engineering schools in Paris: [[École nationale supérieure des mines de Paris|Écoles nationales supérieures des Mines]], des [[École nationale des ponts et chaussées|Ponts]] et de [[École nationale supérieure de l'aéronautique et de l'espace|l’Aéronautique]].

In the course of his career, as both a highly creative designer and a tireless calculator, he designed more than 300 bridges and facilities among which several were world records at the time:
* the [[:fr:Pont Général-Audibert|La Madeleine Bridge]], in Nantes (1928), a concrete cantilever bridge over the River Loire,
* the [[:fr:Pont La-Fayette (Paris)|Lafayette Bridge]] crossing the tracks of the Gare de l’Est in Paris (1928). This is a struss bridge in reinforced concrete, where concrete vibrators using compressed air were used for the first time in history,
* the [[:fr:pont de la Caille|new La Caille Bridge]] (1928), on the ravine of Usses, in the Alps, close to Annecy. This is a 140-m-span concrete arc bridge,
* the great [[Louis Joubert Lock|Louis Joubert]] [[dry dock]] in the port of [[Saint-Nazaire]] (1933), [[Image:Barrage de la Girotte.jpg|200px|thumb|right|Dam of La Girotte (Alps, France), 1949.]]
* the [[:fr:Barrage de la Girotte|La Girotte Dam]] (1944–49),
* the [[:fr:Bollène|Bollène lock]], on the left side (navigating downwards) of the [[Donzère-Mondragon Dam]] (built on the [[:fr:Canal de Donzère-Mondragon|Donzère-Mondragon Canal]], lateral to the [[Rhône]] river), the world’s tallest [[Lock (water transport)|lock]] (1950),
* the Bildstock tunnel (1953–1955),
* the world’s largest [[Rance Tidal Power Station|tidal power plant on the River Rance]], in Brittany (1961–1966). In his eighties, Albert Caquot made a critical contribution to the construction of the dam, designing an enclosure in order to protect the construction site from the 12-m-high ocean tides and the strong streams.
[[File:Cristo Redentor Rio de Janeiro 4.jpg|200px|thumb|right|The [[Corcovado]] Christ, the internal structure of which was built by Caquot, 1931.]]
Two prestigious achievements made him famous internationally: the internal structure of the [[Christ the Redeemer (statue)|statue of Christ the Redeemer]] in [[Rio de Janeiro]] (Brazil) at the peak of [[Christ the Redeemer (statue)|Corcovado Mountain]] (1931) and the [[George V Bridge, Glasgow|George V Bridge on the Clyde River]] in [[Glasgow]] (Scotland) for which the Scottish engineers asked for his assistance.

In his late eighties, he developed a gigantic tidal power project to capture the tide energy in Mont St Michel bay, in Normandy.

===The aeronautical engineer ===
During the course of his life, he committed himself alternatively to structural and aeronautical engineering following the rhythm imposed by the First and Second World Wars. Albert Caquot’s contributions to aeronautics are priceless, from the design of the “Caquot dirigible” to the launching of technical innovations at the new French Aviation Ministry, where he created several Fluid Mechanics Institutes that still exist today. [[Marcel Dassault]], who was charged by Albert Caquot to develop several major aeronautical projects at the beginning of his career, wrote about him: "He was one of the best engineers that aeronautics ever had. He was visionary and ahead of his time. He led aeronautical innovations for forty years".

As early as 1901, already visionary, he performed his military service in an [[airship]] unit of the French army. At the beginning of First World War, he was mobilised with the 40e Compagnie d'Aérostiers equipped with ''Drachen'' type [[airship]]s as first lieutenant. He noticed the poor wind behavior of these sausage shaped captive balloons, which were ineffective except in calm conditions.

[[File:Type R Observation Balloon.jpg|thumb|Caquot dirigible]]
In 1914, he designed a new sausage-shaped dirigible equipped with three air-filled lobes spaced evenly around the tail as stabilizers, and moved the inner air balloonette from the rear to the underside of the nose, separate from the main gas envelope. The Caquot was able to hold in 90&nbsp;km/h winds and remain horizontal. During three years, France manufactured "Caquot dirigibles" for all the allied forces, including English and United States armies. The United States also manufactured nearly a thousand "Caquot R balloons" in 1918-1919. This balloon gave to France and its allies an advantage in military observation which significantly contributed to the allies’ supremacy in aviation and eventually to the final victory. In January 1918, [[Georges Clemenceau|Georges Clémenceau]] named him technical director of the entire military aviation.
In 1919, Albert Caquot proposed the creation of the French aeronautical museum (today called [[Musée de l'Air et de l'Espace]], in Le Bourget). This museum is the oldest aeronautical museum in the world.

In 1928, Albert Caquot became the first executive director of the new Aviation ministry. He implemented a policy of research, prototypes and mass production which contributed quickly to France leadership in the aeronautical industry. His main accomplishments are:
* the development of fluid mechanics research and education. He created in 1928 [[École nationale supérieure de l'aéronautique et de l'espace|the Ecole Nationale Supérieure d’Aéronautique]] (Sup' Aero), the leading engineering school in aeronautics that contributed to French scientific excellence in aeronautics and led to the creation of several institutions like [[ONERA]] (National Office of Aerospace Studies and Research) in 1946 and the [[CNES]] (National Center of Space Studies) in 1952. The school still exists today.
* the construction of the gigantic '''Chalais-Meudon Wind Tunnel''' in 1929<ref>The large wind tunnel in Meudon, S1Ch, http://www.onera.fr/meudon/histoire/aviation.php.</ref> (120 m-long and 25 m-high) allowing to test an aircraft in real conditions, with engine running and the pilot on board.<ref>[https://books.google.com/books?id=QdsDAAAAMBAJ&pg=PA94&dq=Popular+Science+1935+plane+%22Popular+Mechanics%22&hl=en&ei=QIs_TpjpHOPJsQKo4uC_Bw&sa=X&oi=book_result&ct=result&resnum=2&ved=0CCwQ6AEwATgU#v=onepage&q&f=true "Man Made Hurricane Tests Full Size Planes" ''Popular Mechanics'', January 1936, pp.94-95]</ref> This wind tunnel was the largest of the world at the time and it was used to test the [[Dassault Mirage III]], the [[Sud Aviation Caravelle]] and the [[Concorde]], but also cars like the Peugeot 4 CV and the VW Beetle.
In 1933, after a budget cut which prevented him from carrying forward his projects, he resigned and went back to structural engineering for several years.

In 1938, under the threat of the war, Albert Caquot was brought back to manage all the national aeronautical businesses. He resigned in January 1940.

===The man===
He always had a great independence of mind and an incredible selflessness. The numerous honors he received from multiple countries, for example the dignity of [[Légion d'honneur|Grand-croix de la Légion d’Honneur in France]] (1951), pay tribute to his exceptional merits. He was a member of the [[French Academy of Sciences]] for 41 years and served as their president from 1952 to 1961. During more than twenty years, he chaired numerous French scientific organizations, like the [[:fr:CNISF|National Council of the French Engineers]] ([[:fr:CNISF|CNISF]]), and was on the board of [[Électricité de France|EDF]] (Électricité de France), the main electricity generation and distribution company in France, during more than ten years. In 1961, at 80 years old, Albert Caquot resigned from all the presidencies which he had always assured voluntarily. Warm-hearted, attentive and available, he loved to withdraw within his family.

===Homage===
On 2 July 2001, a [[:fr:Timbres de France 2001|4.5-FRF (0.69-€) stamp]] was issued in France to celebrate Albert Caquot’s legacy on the 120th anniversary of his birth and the 25th anniversary of his death. A [[airship|“Caquot dirigeable”]] and the [[:fr:pont de la Caille|bridge of La Caille]], two of his creations, surround his picture on the stamp.

Since 1989, the [[Prix Albert Caquot]] is awarded annually by the [[French Association of Civil and Structural Engineering]].

==See also==
* [[Airship]]
* [[French Academy of Science]]
* [[École polytechnique]], France
* [[École des Ponts ParisTech]]
* [[Musée de l'air et de l'espace]], Le Bourget

==Notes==
<references />

==Bibliography==
* [http://www.amazon.fr/dp/2859783431 « Albert Caquot 1881-1976 - Savant, soldat et bâtisseur »], Jean Kérisel – August 2001
* [http://www.sabix.org/bulletin/sabixb28.htm, Bulletin of the SABIX, special number 28 about Albert Caquot], July 2001
* Le Curieux Vouzinois, "Hyppolyte Taine and Albert Caquot", by Jean Kerisel, Vouziers (the Ardennes), 25 March 2001
* Sciences Ouest, numero 112, "L'Ecole Polytechnique et la Bretagne. Le barrage et l'usine maremotrice de la Rance", June 1995
* L'Union, "Une journee particuliere en hommage a Albert Caquot", Vouziers (the Ardennes), 25 March 1995
* La Jaune et la Rouge, "Albert Caquot (X 1899)", by Robert Paoli (X 1931), November 1993

==External links==
{{Commons category|Albert Caquot}}
* {{Structurae person|id=d000005|name=Albert Caquot}}
* [http://www.enpc.fr/fr/enpc/historique/bio_caquot.htm Biography on the Ecole Nationale des Ponts et Chaussees website (in French)]
* [http://www.annales.org/archives/x/caquot.html Biography on the Ecole Nationale Superieure des Mines de Paris website (in French)]
* [http://www.ville-vouziers.fr/page2.asp?rec=156 Biography on the Vouziers city website (in French)]
* [http://www.planete-tp.com/jsp/room.jsp?PEGA_HREF_3194877_0_0_showArticle=showArticle&PEGAHTML_TEXT_NAME_22222222222222222_attribute=mainArticle_ARTICLE_222%7c%7challFolder_TOPIC_102%7c%7chall_TOPIC_248%7c%7croom_TOPIC_326%7c%7croom_ARTICLE_null Biography on the planete-TP website (in French)]
* [http://www.afgc.asso.fr/v3/home.html List of Albert Caquot awards (AFGC) since 1989]

{{Authority control}}

{{DEFAULTSORT:Caquot, Albert}}
[[Category:1881 births]]
[[Category:1976 deaths]]
[[Category:French bridge engineers]]
[[Category:Corps des ponts]]
[[Category:École des Ponts ParisTech alumni]]
[[Category:École Polytechnique alumni]]
[[Category:Électricité de France people]]
[[Category:French aerospace engineers]]
[[Category:French civil engineers]]
[[Category:Geotechnical engineers]]
[[Category:Grand Croix of the Légion d'honneur]]
[[Category:Officers of the French Academy of Sciences]]
[[Category:People from Vouziers]]
[[Category:Recipients of the Croix de guerre 1914–1918 (France)]]
[[Category:Structural engineers]]