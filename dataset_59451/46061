{{Infobox person
|name          = Ah Ken
|image         = 
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = 
|birth_place   = [[Guangzhou]], [[China]]
|death_date    = 
|death_place   = 
|death_cause   = 
|nationality   = [[Chinese American]]
|other_names   = Ah Kam 
|known_for     = First [[Chinese American]] to settle in [[Chinatown, Manhattan]]. 
|occupation    = Businessman
}}

'''Ah Ken''' ([[floruit|fl.]] 1858–1896) was a [[Chinese American]] businessman and popular figure in [[Chinatown, Manhattan]] during the mid-to late 19th century. The first man to permanently immigrate to Chinatown, although [[Quimbo Appo]] is claimed to have arrived in the area during the 1840s, Ah Ken resided on [[Mott Street]] and eventually founded a successful [[cigar store]] on [[Park Row (Manhattan)|Park Row]].<ref name="Moss">Moss, Frank. ''The American Metropolis from Knickerbocker Days to the Present Time''. London: The Authors' Syndicate, 1897. (pg. 403)</ref><ref>[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 278-279) ISBN 1-56025-275-8</ref><ref name="Harlow">Harlow, Alvin F. ''Old Bowery Days: The Chronicles of a Famous Street''. New York and London: D. Appleton & Company, 1931. (pg. 392)</ref><ref>Worden, Helen. ''The Real New York: A Guide for the Adventurous Shopper, the Exploratory Eater and the Know-it-all Sightseer who Ain't Seen Nothin' Yet''. Indianapolis: Bobbs-Merrill, 1932. (pg. 140)</ref><ref name="Hemp">Hemp, William H. ''New York Enclaves''. New York: Clarkson M. Potter, 1975. (pg. 6) ISBN 0-517-51999-2</ref><ref>Wong, Bernard. ''Patronage, Brokerage, Entrepreneurship, and the Chinese Community of New York''. New York: AMS Press, 1988. (pg. 31) ISBN 0-404-19416-8</ref><ref>Lin, Jan. ''Reconstructing Chinatown: Ethnic Enclave, Global Change''. Minneapolis: University of Minnesota Press, 1998. (pg. 30-31) ISBN 0-8166-2905-6</ref><ref>Taylor, B. Kim. ''The Great New York City Trivia & Fact Book''. Nashville: Cumberland House Publishing, 1998. (pg. 20) ISBN 1-888952-77-6</ref><ref>Ostrow, Daniel. ''Manhattan's Chinatown''. Charleston, South Carolina: Arcadia Publishing, 2008. (pg. 9) ISBN 0-7385-5517-7</ref> He first arrived in New York around 1858 where he was ''"probably one of those Chinese mentioned in gossip of the sixties as peddling 'awful' cigars at three cents apiece from little stands along the City Hall park fence - offering a paper spill and a tiny oil lamp as a lighter"'' according to author Alvin Harlow in ''Old Bowery Days: The Chronicles of a Famous Street'' (1931).<ref name="Harlow"/>

Later immigrants would similarly find work as "cigar men" or carrying billboards and Ah Ken's particular success encouraged cigar makers William Longford, John Occoo and John Ava to also ply their trade in Chinatown eventually forming a monopoly on the cigar trade.<ref>Tchen, John Kuo Wei. ''New York Before Chinatown: Orientalism and the Shaping of American Culture, 1776-1882''. Baltimore: Johns Hopkins University Press, 2001. (pg. 82-83) ISBN 0-8018-6794-0</ref> It has been speculated that it may have been Ah Ken who kept a small [[boarding house]] on lower Mott Street and rented out bunks to the first Chinese immigrants to arrive in Chinatown. It was with the profits he earned as a landlord, earning an average of $100 a month, that he was able to open his Park Row smoke shop around which modern-day Chinatown would grow.<ref name="Moss"/><ref name="Hemp"/><ref name="FWP">Federal Writers' Project. ''New York City: Vol 1, New York City Guide''. Vol. I. American Guide Series. New York: Random House, 1939. (pg. 104)</ref><ref>Marcuse, Maxwell F. ''This Was New York!: A Nostalgic Picture of Gotham in the Gaslight Era''. New York: LIM Press, 1969. (pg. 41)</ref><ref>Chen, Jack. ''The Chinese of America''. New York: Harper & Row, 1980. (pg. 258) ISBN 0-06-250140-2</ref><ref>Hall, Bruce Edward. ''Tea That Burns: A Family Memoir of Chinatown''. New York: Simon and Schuster, 2002. (pg. 37) ISBN 0-7432-3659-9</ref>

==References==
{{Reflist}}

==Further reading==
*"New York's First Chinaman". ''Atlanta Constitution''. 22 Sep 1896
*Crouse, Russel. ''Murder Won't Out''. New York: Doubleday, Doran & Company, 1932.
*Dunshee, Kenneth Holcomb. ''As You Pass By''. New York: Hastings House, 1952.
*Ramati, Raquel. ''How to Save Your Own Street''. Garden City, Doubleday and Co., 1981. ISBN 0-385-14814-3

{{DEFAULTSORT:Ah, Ken}}
[[Category:1858 births]]
[[Category:1896 deaths]]
[[Category:Qing Dynasty emigrants to the United States]]
[[Category:19th-century American businesspeople]]
[[Category:People from Manhattan]]
[[Category:American people of Cantonese descent]]
[[Category:Chinatown, Manhattan]]
[[Category:Businesspeople from Guangzhou]]