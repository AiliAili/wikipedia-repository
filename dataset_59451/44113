{{Infobox airport
| name = Amakusa Airfield
| nativename =
| nativename-a = {{lang|ja|天草飛行場}}
| nativename-r =
| image = Amakusa-airport.JPG
| image-width = 300
| caption = Amakusa Airfield terminal building
| IATA = AXJ
| ICAO = RJDA
| type = Public
| owner = Kumamoto Prefecture government
| operator =
| city-served = [[Amakusa, Kumamoto|Amakusa]]
| location =
| elevation-f = 340
| elevation-m = 104
| coordinates = {{coord|32|28|56|N|130|09|32|E|region:JP|display=inline,title}}
| pushpin_map = Japan
| pushpin_label = RJDA
| pushpin_map_caption = Location in Japan
| website = [http://www.pref.kumamoto.jp/soshiki/137/amakusa120323.html pref.kumamoto.jp]
| metric-rwy =Y
| r1-number = 13/31
| r1-length-f = 3,281
| r1-length-m = 1,000
| r1-surface = [[Asphalt concrete]]
| stat-year = 2014
| stat1-header = Passengers
| stat1-data = 62,759
| stat2-header = Cargo (metric tonnes)
| stat2-data = 2
| stat3-header = 
| stat3-data = 
| footnotes =Source: Japanese [[Aeronautical Information Publication|AIP]] at [[Aeronautical Information Service|AIS Japan]]<ref name="AIP">[https://aisjapan.mlit.go.jp/ AIS Japan]</ref> <br>Osaka Ministry of Land, Infrastructure and Transport Civil Aviation Bureau <ref>{{Cite press release |title=Amakusa Tanegashima Airport Statistics|publisher=Osaka Ministry of Land, Infrastructure and Transport Civil Aviation Bureau |date=
|url=http://ocab.mlit.go.jp/about/total/report/pdf/h26syuukei.pdf|format=PDF |accessdate= 8 July 2016}}</ref>
}}

{{nihongo|'''Amakusa Airfield'''|天草飛行場}} is an airport located {{convert|2.3|NM|abbr=on|lk=in}} northwest<ref name="AIP"/> of [[Amakusa, Kumamoto]], [[Japan]], on the [[Amakusa|Amakusa Islands]] {{airport codes|AXJ|RJDA}}. Locals often referred to the airfield as '''Amakusa Airport'''. It is located on the northern side of the Amakusa Islands, north west of Amakusa city. Only one airline, [[Amakusa Airlines]], uses this airfield.

==History==
[[File:Amakusa-airline-Dash 8-100.JPG|thumb|250px|DHC-8-103 of [[Amakusa Airlines]] taxiing at Amakusa Airfield]]
On September 6, 1982, the governor of [[Kumamoto Prefecture]] announced plans for a small airport during a regular press conference. On December 26, 1990, the [[Ministry of Land, Infrastructure, Transport and Tourism (Japan)|Ministry of Land, Infrastructure and Transport]] approved the construction of the airfield. Construction began in 1992. The first plane that landed at this airport was a [[DHC-8]] of [[Amakusa Airlines]] on November 19, 1999. The airfield was opened for public use on March 23, 2000.

In the spring of 2000, it had round trips between Amakusa Islands and Kumamoto twice a day.<ref>{{cite web |url=http://www.bombardiertransport.com/en/3_0/3_1/pdf/RUfeb99.pdf |format=PDF |title=Poised for certification |publisher=Bombardier |date=September 1999}}</ref> Starting from December 1, 2005, the operating time was extended from (8:00 am ～ 7:00 pm) to (7:40 am ～ 8:30 pm).<ref name="AIP"/> It now has three round trips between Amakusa Airfield and [[Fukuoka Airport]] and one round trip between Amakusa Airfield and [[Kumamoto Airport]] (also one round trip between Kumamoto Airport and [[Kobe Airport]]) per day while using the same aircraft (aircraft registration number JA81AM).<ref>{{cite web |url=http://vacant.amx-web.com/diary3.cgi |title=Timetable of Amakusa Airlines |language=Japanese |accessdate=2009-07-18}}</ref> Since this airfield is served only by Amakusa Airlines and this airline only has one aircraft, the [[DHC-8]] (pictured on the right) is the only regular, scheduled, aircraft that uses this airfield.<ref>{{cite web |url=http://www.ocab.mlit.go.jp/about/total/report/pdf/riyou_h20.pdf |title=Statistics of airports in Osaka region in 2008 |language=Japanese |format=PDF |publisher=Osaka Regional Civil Aviation Bureau}}</ref><ref>{{cite web |url=http://www.ocab.mlit.go.jp/about/total/report/pdf/yusou_h20.pdf |title=Statistics of airlines in western Japan in 2008 |language=Japanese |format=PDF |publisher=Osaka Regional Civil Aviation Bureau}}</ref>

==Airlines and destinations==
===Passenger===
{{Airport-dest-list
| [[Amakusa Airlines]] | [[Fukuoka Airport|Fukuoka]], [[Kumamoto Airport|Kumamoto]], [[Osaka International Airport|Osaka-Itami]]
}}

==Airport communications==
* Air / Ground (130.775&nbsp;MHz)<ref name ="AIP"/>

==Runway information==
{| border=1 align=right cellpadding=4 cellspacing=0 width="25%" style="margin: 0 0 1em 1em; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
!align="center" bgcolor="#0099FF" colspan="4"|''Operations and Statistics''<ref>{{cite web |url=http://www.ocab.mlit.go.jp/about/total/report/ |title=大阪航空局_大阪航空局のご案内_統計情報_利用実績速報 (Statistics of airports within Osaka Region) |publisher=Osaka Regional Civil Aviation Bureau |language=Japanese}}</ref>
|-
!colspan="4" bgcolor="lightgrey"|<small>Passenger movements</small>
|-
|<small>2001</small>
|<small>83,654</small>
|<small>2007</small>
|<small>73,410</small>
|-
|<small>2002</small>
|<small>83,868</small>
|<small>2008</small>
|<small>60,086</small>
|-
|<small>2003</small>
|<small>76,161</small>
|<small>2009</small>
|<small>70,138</small>
|-
|<small>2004</small>
|<small>76,123</small>
|<small>2010</small>
|<small>70,138</small>
|-
|<small>2005</small>
|<small>72,866</small>
|<small>2011</small>
|<small></small>
|-
|<small>2006</small>
|<small>74,847</small>
|
|
|-
!colspan="4" bgcolor="lightgrey"|<small>Airfreight movements in tonnes</small>
|-
|<small>2001</small>
|<small>0</small>
|<small>2007</small>
|<small>6</small>
|-
|<small>2002</small>
|<small>0</small>
|<small>2008</small>
|<small>4</small>
|-
|<small>2003</small>
|<small>3</small>
|<small>2009</small>
|<small>2</small>
|-
|<small>2004</small>
|<small>3</small>
|<small>2010</small>
|<small></small>
|-
|<small>2005</small>
|<small>3</small>
|<small>2011</small>
|<small></small>
|-
|<small>2006</small>
|<small>7</small>
|
|
|-
!colspan="4" bgcolor="lightgrey"|<small>Aircraft movements</small>
|-
|<small>2001</small>
|<small>1,914</small>
|<small>2007</small>
|<small>2,033</small>
|-
|<small>2002</small>
|<small>1,850</small>
|<small>2008</small>
|<small>1,896</small>
|-
|<small>2003</small>
|<small>1,842</small>
|<small>2009</small>
|<small>1,559</small>
|-
|<small>2004</small>
|<small>1,723</small>
|<small>2010</small>
|<small></small>
|-
|<small>2005</small>
|<small>1,679</small>
|<small>2011</small>
|<small></small>
|-
|<small>2006</small>
|<small>1,859</small>
|
|
|}

Landings are made using [[VHF omnidirectional range|VOR]]/[[Distance measuring equipment|DME]] approach on runway 13/31.<ref>{{cite web |url=http://www.casas.or.jp/amakusa.html |script-title=ja:天草飛行場 |language=Japanese |publisher=(Japan) Aerodrome　Support　and　Aeronautical　Service}}</ref> The airport has a single runway, 13/31, which is {{convert|1000|x|30|m|abbr=on|0}} and is constructed of [[asphalt concrete]]. The lighting systems on runway 13/31 are [[High Intensity Runway Lights]], [[Runway End Identification Lights]], and [[Precision Approach Path Indicator]] (PAPI).<ref name="AIP"/>

==Airport facilities==
* 1st Floor
** Information Office
** Departure
** Arrival Lobby
** Amakusa Airlines Headquarters
** Washrooms
** Breast-feeding room
** Tourist Information
** Shop
** Taxi bus
* 2nd Floor
** Observation deck
** Administration Office

==Time zone==
* UTC+9 [[Japan Standard Time]]

==Ground transport==
All ground transport is located on the first floor.

===Bus===
{{main|List of bus operating companies in Japan (west)}}
[[List of bus operating companies in Japan (west)#Kumamoto Prefecture|Kyushu Sanko Bus]] ([[:ja:九州産交バス|九州産交バス]]) operate bus routes from airport to Amakusa city.

===Road===
The airport is connected by Kumamoto Prefecture Amakusa Highway No.334 and Itsuwa Road No.47.

==References==
{{reflist}}

==External links==
* [http://www.pref.kumamoto.jp/soshiki/137/amakusa120323.html Amakusa Airfield]

{{Japanese airports}}

{{Portal|Aviation}}

[[Category:Airports in Japan]]
[[Category:Transport in Kumamoto Prefecture]]
[[Category:Buildings and structures in Kumamoto Prefecture]]