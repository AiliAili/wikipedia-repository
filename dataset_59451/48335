{{Infobox person
| image       = 
| imagesize   =
| name        = Don Haig
| birth_place = [[Winnipeg]], [[Manitoba]]
| birth_date  = {{Birth date|1933|07|22}}
| birth_name  = Donald George Haig
| death_place = [[Toronto]], [[Ontario]]
| death_date  = {{Death date and age|2002|03|02|1933|07|22}}
| nationality = [[Canadian]]
| occupation  = [[Film producer]]<br>[[Film editor]]
| yearsactive = [[1955 in film|1955]] - [[1998 in film|1998]]
| awards      = [[Governor General's Awards]]
}}

'''Don Haig''' (22 July 1933 — 2 March 2002) was a [[Canadians|Canadian]] filmmaker, editor, and producer.

His work in film and television spanned nearly five decades. Over the course of his career, he won [[Academy Awards|Academy]], [[Genie Awards|Genie]],<ref name=topalovich>{{cite book|last=Topalovich|first=Maria|title=And the Genie Goes To... Celebrating 50 Years of the Canadian Film awards|year=2000|publisher=Stoddart Publishing Company Ltd.|location=Toronto|isbn=0-7737-3238-1|pages=249}}</ref> and [[Gemini Awards|Gemini]] awards, and the [[Governor General's Performing Arts Award]].

Don was known as the "godfather of Canadian film" for nurturing young talent and producing many award-winning films. He is recognized by some as "the most important person on the Canadian film scene," helping create over 500 films.<ref>{{cite web|title=Don Haig |url=http://tiff.net/CANADIANFILMENCYCLOPEDIA/content/bios/don-haig |work=Canadian Film Encyclopedia |accessdate=29 November 2012 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

==Career==
As a young man, Haig began his career in film distribution in Winnipeg. In the early years, he would travel to small prairie towns with films and a projector, and charge five cents or less to see a movie, as "another pair of eyes doesn't cost much".  After the early years repairing film for MGM in Winnipeg, he later moved to the [[Canadian Broadcasting Corporation]] where he remained until 1962.

In 1963, with the support of filmmakers [[Allan King]] and [[Beryl Fox]], he started his company Film Arts. Haig received film editing awards for the [[Beryl Fox]] documentaries including ''[[Summer in Mississippi]]'' and ''[[The Mills of the Gods: Viet Nam]]''. He also edited her ''[[Fields of Endless Day]]''.

In 1970, Haig co-founded the Canadian Film Editors Guild in 1970. He was chairman of the Canadian Film and Television Association in 1972.

Among the films he helped produce was ''[[Artie Shaw: Time Is All You've Got.]]'', which was written and directed by Brigitte Berman in 1985. [[Oprah Winfrey]] presented Berman and Haig with an [[Academy Award|Oscar]] at the [[59th Academy Awards]].

After his retirement and sale of Film Arts to Film House, Haig joined the [[National Film Board of Canada]] in 1992 and became head of English production. He was noted for aiding young talent with funding, guidance, and editing their films. He retired in 1998 after completing many NFB productions.

He died in 2002 at his home in Toronto.  The Don Haig Foundation was established to see his legacy of supporting young filmmakers continue.

In 2004, the Don Haig Foundation began recognizing aspiring documentary filmmakers with an annual award. On February 6, 2006, the award committee began a partnership with the [[Hot Docs Canadian International Documentary Festival]] to ensure a home for the Don Haig Award.

==Personal life==
Haig was [[gay]].<ref name=waugh>[[Thomas Waugh|Waugh, Thomas]], ''Romance of Transgression in Canada: Queering Sexualities, Nations, Cinemas''. [[Carleton University Press]], 2006. ISBN 978-0773530690. p. 426.</ref> His life partner was Bill Schultz.<ref name=waugh/>

==Filmography==
In addition to the films listed below, there are an estimated 500 films he has contributed as producer or editor, and Library and Archives Canada<ref>{{cite web|title=Haig Film Arts |url=http://www.collectionscanada.gc.ca/ |work=Library and Archives Canada |accessdate=29 November 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20140102221451/http://www.collectionscanada.gc.ca/ |archivedate=2 January 2014 |df= }}</ref> has an archival collection of over 800 film reels  associated with Haig or his companies including Film Arts Ltd.

===Documentary films===
* ''[[Summer in Mississippi]]'' (1964)
* ''[[The Mills of the Gods: Viet Nam]]'' (1965)

===Feature films and telefilms===
* ''[[125 Rooms of Comfort]]'' (1974) 
* ''[[Who Has Seen the Wind? (1965 film)|Who Has Seen the Film]]'' (1977)
* ''[[Summer’s Children]]'' (1979)
* ''[[Silence of the North]]'' (1981)
* ''[[Alligator Shoes]]'' (1981) 
* ''[[I've Heard the Mermaids Singing]]'' (1987)

===Television===
* ''This Hour Has Seven Days'' (1966)
* ''[[W5 (TV series)]]'' (1966?-?)
* ''[[The Fifth Estate (TV)|The Fifth Estate]]'' (1975?-?)

===National Film Board Documentary films===
Haig headed production of English-language film at the National Film Board of Canada. He acted as producer, co-producer, and provided guidance, funding, and editing assistance to many filmmakers while there.
* ''[[A Further Glimpse of Joey]]'' (1966)
* ''[[Fields of Endless Day]]'' (1978)
* ''[[K.C.I.: Beyond the Three R's]]'' (1982)
* ''[[Blockade]]'' (1994)
* ''[[The Voyage of the St. Louis]]'' (1995)
* ''[[Body Politics]]'' (1997)
* ''[[The Battle of Vimy Ridge]]'' (1997)
* ''[[The Double Shift]]'' (1997)
* ''[[The Gender Tango]]'' (1997)
* ''[[The Need to Know]]'' (1997)
* ''[[A Place in the World (1997 film)|A Place in the World]]'' (1997)
* ''[[Postcards from the Future]]'' (1997)
* ''[[The Power Game]]'' (1997)
* ''[[The Street: A Film with the Homeless]]'' (1997)

==Lifetime awards==
* At the 1985 Genies, the Air Canada Award for Outstanding Contributions to the Canadian Film Industry
* 1993 Gemini award - the [[Donald Brittain]] Award 
* 1987 Ontario Film Institute’s Achievement Award
* 1991 City of Toronto Arts Award
* 1993 Haig received the [[Governor General's Performing Arts Award]].
* In 1993, Haig received an honorary doctorate (D.Litt) from [[York University]].

== References ==
{{Reflist}}

== External links ==
* [http://www.onf-nfb.gc.ca/eng/collection/result.php?type=credit&pid=6936&nom=Don+Haig National Film Board of Canada film collection]
* [http://tiff.net/CANADIANFILMENCYCLOPEDIA/content/bios/don-haig Canadian Film Encyclopedia]{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
* [https://web.archive.org/web/20120716054414/http://www.hotdocs.ca:80/conference/documentarys_don_haig_award Don Haig Award at Hot Docs]
* [http://www.donhaig.org/ The Don Haig Foundation]

{{DEFAULTSORT:Haig, Don}}
[[Category:1933 births]]
[[Category:2002 deaths]]
[[Category:Articles created via the Article Wizard]]
[[Category:Canadian documentary filmmakers]]
[[Category:Canadian film producers]]
[[Category:Canadian television producers]]
[[Category:Canadian film editors]]
[[Category:Governor General's Performing Arts Award winners]]
[[Category:Gay men]]
[[Category:LGBT people from Canada]]
[[Category:LGBT producers]]
[[Category:People from Winnipeg]]
[[Category:People from Toronto]]
[[Category:Canadian Broadcasting Corporation people]]
[[Category:National Film Board of Canada people]]