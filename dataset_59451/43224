{{Redirect|MD-12|the state highway|Maryland Route 12}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= MD-12
 |image= File:Md-12-2.png
 |caption= A computer graphics rendering of the proposed MD-12, a full [[double-deck aircraft|double-decker]] configuration concept
 |alt=
}}{{Infobox aircraft type
 |type= [[double-deck aircraft|Double-decker]] [[wide-body aircraft]]
 |national origin= [[United States]]
 |manufacturer= [[McDonnell Douglas]]
 |designer=
 |first flight=
 |introduction=
 |retired=
 |status= Design study, canceled
 |primary user= 
 |more users= <!--This field is limited to THREE (3) TOTAL. Please separate with <br/>.-->
 |produced=
 |number built=
 |unit cost=
 |developed from=
 |variants with their own articles=
}}
|}

The '''McDonnell Douglas MD-12''' was an aircraft design study undertaken by the [[McDonnell Douglas]] company in the 1990s for a "[[superjumbo]]" aircraft, first conceived as a larger [[trijet]], then stretched to a 4-engine airliner.  It was to be similar in size to the [[Boeing 747]], but with more passenger capacity.  However, the MD-12 received no orders and was canceled.  McDonnell Douglas then studied larger [[McDonnell Douglas MD-11|MD-11]] derivatives named ''MD-XX'' without proceeding.

==Design and development==

===Background===
McDonnell Douglas studied improved, stretched versions of the trijet [[McDonnell Douglas MD-11|MD-11]], named MD-12X<ref name=MDC_brochure>[http://md-eleven.net/MD11-MD12-undeveloped-models "MDC brochures for undeveloped versions of the MD-11 and MD-12."] ''md-eleven.net.'' Retrieved: April 14, 2008.</ref> with a possible lower-front passenger deck with panoramic windows.<ref>[http://www.airliners.net/info/stats.main?id=112 "MD-11 page."] ''Airliners.net''. Retrieved: October 18, 2007.</ref><ref name=Arthur_p92>{{citation|last=Steffen|first=Arthur|title=McDonnell Douglas MD-11: A Long Beach Swansong|publisher=Hinckley|location=UK: Midland|year=2002|isbn=1-85780-117-2|pages=92–94}}</ref> The MDC board of directors agreed in October 1991 to offer the MD-12X design to airlines.  MD-12X had a length of {{convert|237|ft|11|in|m|abbr=on|sigfig=3}} and wingspan of {{convert|212|ft|6|in|m|abbr=on|sigfig=3}}.  In November 1991, McDonnell Douglas and Taiwan Aerospace Corporation signed a Memorandum of Understanding to form a company to produce the new design.  The new company would have McDonnell Douglas as the majority shareholder (51%) with Taiwan Aerospace (40%) and other Asian companies (9%) having the remaining shares.<ref name=Arthur_p92/>

===MD-12===
[[File:McDonnell Douglas MD-12-b.png|thumb|Artist's impression of the aircraft]]

In late 1991, McDonnell Douglas made a move to separate its civil and military divisions in a bid to raise the estimated $4 billion development costs needed to develop the MD-12X trijet. Separating the costly military [[Boeing C-17 Globemaster III|C-17 airlifter]] development, which had been a drain on the company's resources, from the profit-making production of the MD-80 and MD-11 airliners would make it easier to attract foreign investors for the MD-12X.<ref>[http://www.flightglobal.com/pdfarchive/view/1991/1991%20-%202935.html "MD-12 divides Douglas."] ''Flight International,'' November 13–19, 1991.</ref>

The design grew into the much larger MD-12 with four engines and two passenger decks extending the length of the [[fuselage]].  The length of the main MD-12 variants was {{convert|208|ft|m|abbr=on|sigfig=3}} with a wingspan of {{convert|213|ft|m|abbr=on|sigfig=3}}.  The fuselage was {{convert|24|ft|3|in|m|2|abbr=on|sigfig=3}} wide by {{convert|27|ft|11|in|m|abbr=on|sigfig=3}} high.<ref name=Arthur_p92/>

McDonnell Douglas unveiled its MD-12 design in April 1992.<ref name=Arthur_p92/> The design was similar in concept to the [[Airbus A380|Airbus A3XX]] and [[Boeing NLA]], and it would have been larger than the [[Boeing 747]] with which it would have directly competed.  [[Douglas Aircraft Company|Douglas Aircraft]] had also studied a smaller double-decker design in the 1960s.<ref>Berek, D. [http://rides.webshots.com/photo/1131353000048918155rOFmxM "Proposed double deck DC-10 design in 1965."] ''webshots.com,'' April 4, 2004. Retrieved: July 15, 2011.</ref><ref>Waddington, Terry. ''McDonnell Douglas DC-10''. Miami, Florida: World Transport Press, 2000. ISBN 1-892437-04-X.</ref>

The first flight of the MD-12 was to take place in late 1995, with delivery in 1997.<ref name=Arthur_p92/>  Despite aggressive marketing and initial excitement, especially in the aviation press, no orders were placed for the aircraft. MDC lacked the resources after Taiwan Aerospace left the project.<ref name=Arthur_p92/> Some sceptics believed MDC launched the project to lure Boeing into paying a higher price for the company.<ref>{{cite web|last1=Knowlton|first1=Brian|last2=International Herald Tribune|title=Boeing to Buy McDonnell Douglas|url=http://www.nytimes.com/1996/12/16/news/boeing-to-buy-mcdonnell-douglas.html|website=New York Times|publisher=Arthur Ochs Sulzberger, Jr.|accessdate=23 March 2017}}</ref><ref>{{cite web|last1=Bean|first1=Bransom|title=FAILURE TO LAUNCH: THE LEGACY OF THE MCDONNELL DOUGLAS MD-12 PROGRAM|url=http://www.avgeekery.com/failure-to-launch-the-legacy-of-the-mcdonnell-douglas-md-12-program/|website=AV Geekery|publisher=AV Geekery|accessdate=23 March 2017}}</ref>{{better source|date=March 2017}} A new [[Double-deck aircraft|double deck]] widebody has proved to be extremely expensive and complex to develop, even for the remaining aerospace giants Boeing and Airbus, although the massive [[Airbus A380]], a similar concept to the MD-12, was later brought to fruition.<ref>[http://business.timesonline.co.uk/tol/business/industry_sectors/engineering/article659591.ece "Airbus will lose €4.8bn because of A380 delays."] ''Times Online,'' October 4, 2006.</ref><ref>{{citation|url=http://money.cnn.com/magazines/fortune/fortune_archive/2007/03/05/8401277/index.htm|title=Big plane, big problems|first=Nelson D.|last=Schwartz|website=''[[CNN]]''|date=March 5, 2007}}</ref>

===MD-XX===
With the MD-12 program over, McDonnell Douglas focused on 300 to 400–seat MD-11 derivatives.  At the 1996 Farnborough International Air Show, the company presented plans for a new trijet with high-seating and long-range named "MD-XX".<ref>[http://www.boeing.com/news/releases/mdc/96-221.html "McDonnell Douglas Unveils New MD-XX Trijet Design."]  ''McDonnell Douglas,'' September 4, 1996.</ref>  The MD-XX was offered in two variants; MD-XX Stretch with a longer fuselage and MD-XX LR for longer range.  Both MD-XX variant designs had {{convert|213|ft|m|abbr=on|sigfig=3}} wingspan, the same as MD-12. The MD-XX Stretch was lengthened {{convert|32|ft|m|abbr=on|sigfig=2}} over the MD-11 and had seating for 375 in a typical 3-class arrangement and 515 in all-economy seating.  Its range was to be {{convert|7020|nmi|mi km|abbr=on|sigfig=3}}. The MD-XX LR was the same length as the MD-11, had seating for 309 in a typical 3-class arrangement and featured a range of {{convert|8320|nmi|mi km|abbr=on|sigfig=3}}. However, the MDC board of directors decided to end the MD-XX program in October 1996, stating the financial investment for the program was too large for the company.<ref name=Arthur_p92/>

==Variants==
The MD-12 was offered in a few proposed variants listed below.<ref name=Arthur_p92/>

*MD-12 HC (High Capacity)
*MD-12 LR (Long Range)
*MD-12 ST (Stretch)
*MD-12 Twin (two engine version)

==Specifications (MD-12 High Capacity design)==
[[File:Md-12.png|right|300px|Three views]]

{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?=plane
|jet or prop?=jet
|ref=McDonnell Douglas promotional materials<ref name=MDC_brochure/>
|crew=2 (pilot and co-pilot)
|capacity= Long range: 430 passengers (3-class) / High-capacity: 511 passengers (3-class)
|length main=208 ft 0 in
|length alt=63.40 m
|span main=213 ft 0 in
|span alt=64.92 m
|height main=74 ft 0 in
|height alt=22.55 m
|area main=5,846 ft²
|area alt=543.1 m²
|empty weight main=402,700 lb
|empty weight alt=187,650 kg
|loaded weight main=
|loaded weight alt=
|max takeoff weight main=949,000 lb
|max takeoff weight alt=430,500 kg
|engine (jet)=[[General Electric CF6|General Electric CF6-80C2]] 
|type of jet= high-bypass turbofans
|number of jets=4
|thrust main=61,500 lbf 
|thrust alt=274 kN
|thrust original=
|max speed main=Mach 0.85
|max speed alt= 565 [[knots]], 1,050 km/h
|max speed more=
|cruise speed main= 
|cruise speed alt= 
|cruise speed more=
|range main=7,170 nmi
|range alt=9,200 mi, 14,825 km
|ceiling main=
|ceiling alt=
|climb rate main=
|climb rate alt=
|loading main=162.3 lb/ft²
|loading alt=792.7 kg/m²
|thrust/weight=
}}

==See also==
{{aircontent
|see also=
|related=
* [[McDonnell Douglas MD-11]]
|similar aircraft=
* [[Airbus A380]]
* [[Boeing NLA]]
* [[Boeing 747#747X and 747X Stretch|Boeing 747X]]
* [[Sukhoi KR-860]]
|lists=
}}

==References==
;Notes
{{Reflist}}

==External links==
{{Commons category|McDonnell Douglas MD-12}}
* [http://md-eleven.net/MD11-MD12-undeveloped-models Undeveloped MD-11/MD-12 models page on MD-Eleven.net]
* [https://query.nytimes.com/gst/fullpage.html?res=9E0CE3DB1538F935A25750C0A964958260 "McDonnell May Build A Larger Jet", The New York Times, March 16, 1992]

{{Douglas airliners}}

[[Category:Abandoned civil aircraft projects of the United States]]
[[Category:Quadjets]]
[[Category:McDonnell Douglas aircraft|MD-012]]
[[Category:Low-wing aircraft]]
[[Category:McDonnell Douglas MD-11]]