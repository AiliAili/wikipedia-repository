{{Infobox coin
| Country             = United States
| Denomination        = Franklin half dollar
| Value               = 50 cents (0.50
| Unit                = [[United States dollar|US dollars]])
| Mass                = 12.50{{sfn|''Coin World'', Franklin half dollar}}
| Diameter            = 30.61{{sfn|''Coin World'', Franklin half dollar}}
| Silver_troy_oz      = 0.36169{{sfn|''Coin World'', Franklin half dollar}}
| Diameter_inch       = 1.21{{sfn|''Coin World'', Franklin half dollar}}
| Edge                = [[reeding|reeded]]{{sfn|''Coin World'', Franklin half dollar}}
| Composition =
  {{plainlist |
* 90% [[Silver|Ag]]
* 10% [[Copper|Cu]]{{sfn|''Coin World'', Franklin half dollar}}
  }}
| Years of Minting    = 1948–1963{{sfn|''Coin World'', Franklin half dollar}}
| Mint marks = [[Denver Mint|D]], [[San Francisco Mint|S]].  On reverse, above yoke of bell and below letter E in STATES.  [[Philadelphia Mint]] coins struck without mint mark.
| Obverse             = Franklin Half 1963 D Obverse.png
| Obverse Design      = [[Benjamin Franklin]]
| Obverse Designer    = [[John R. Sinnock]]
| Obverse Design Date = 1948
| Reverse             = Franklin Half 1963 D Reverse.png
| Reverse Design      = [[Liberty Bell]]
| Reverse Designer    = [[John R. Sinnock]]
| Reverse Design Date = 1948
}}

The '''Franklin half dollar''' is a [[United States coinage|coin]] that was [[Coining (mint)|struck]] by the [[United States Mint]] from 1948 to 1963.  The [[Half dollar (United States coin)|fifty-cent piece]] pictures [[Founding Fathers of the United States|Founding Father]] [[Benjamin Franklin]] on the [[obverse and reverse|obverse]] and the [[Liberty Bell]] on the reverse.  A small [[Eagle (heraldry)|eagle]] was placed to the right of the bell to fulfill the legal requirement that half dollars depict the figure of an eagle.  Produced in 90&nbsp;percent silver with a [[reeding|reeded edge]], the coin was struck at the [[Philadelphia Mint|Philadelphia]], [[Denver Mint|Denver]], and [[San Francisco Mint|San Francisco]] [[Mint (facility)|mints]]. At the end of April 2016 the metal value of the $0.50 coin was approximately $6.48, an increase of approximately 1300%.

Mint director [[Nellie Tayloe Ross]] had long admired Franklin, and wanted him to be depicted on a coin.  In 1947, she instructed her chief engraver, [[John R. Sinnock]], to prepare designs for a Franklin half dollar. Sinnock's designs were based on his earlier work, but he died before their completion. The designs were completed by Sinnock's successor, [[Gilroy Roberts]].  The Mint submitted the new designs to the [[Commission of Fine Arts]] ("Commission") for its advisory opinion. The Commission disliked the small eagle and felt that depicting the crack in the Liberty Bell would expose the coinage to jokes and ridicule.  Despite the Commission's disapproval, the Mint proceeded with Sinnock's designs.

After the coins were released in April 1948, the Mint received accusations that Sinnock's initials "JRS" on the cutoff at Franklin's shoulder were a tribute to Soviet dictator [[Joseph Stalin]] (Stalin did not have a middle  name that began with an 'R').  No change was made, with the Mint responding that the letters were simply the artist's initials (The same accusation was made after the release of the Sinnock designed [[Roosevelt Dime]] in 1946).  The coin was struck regularly until 1963; beginning in 1964 it was replaced by the [[Kennedy half dollar]], issued in honor of the [[Assassination of John F. Kennedy|assassinated]] President [[John F. Kennedy]].  Though the coin is still [[legal tender]], its value to collectors or as silver both greatly exceed its face value.

== Background and selection ==
Mint Director Nellie Tayloe Ross had long been an admirer of Benjamin Franklin, and wished to see him on a coin.{{sfn|Taxay|1983|p=376}} In 1933, Sinnock had designed a medal featuring Franklin, which may have given her the idea.{{sfn|Taxay|1983|p=376}}  Franklin had opposed putting portraits on coins;{{sfn|AP/''The Portsmouth Times''|1948-05-13}} he advocated proverbs about which the holder could profit through reflection.{{sfn|Tomaska|2002|p=3}} In a 1948 interview, Ross noted that Franklin only knew of living royalty on coins, and presumably would feel differently about a republic honoring a deceased founder.{{sfn|AP/''The Portsmouth Times''|1948-05-13}}  Indeed, Franklin might have been more upset at the reverse design: as [[numismatic]] writer Jonathan Tepper noted, "Had Benjamin Franklin known that he would be appearing on a half dollar with an eagle, he most likely would have been quite upset.  He detested the eagle, and numismatic lore has it that he often referred to it as a scavenger. Given the practical man that he was, Franklin proposed the wild turkey as our national bird."{{sfn|Tepper|April 1990}}

[[File:Sesquicentennial american independence half dollar commemorative reverse.jpg|thumb|left|200px|The Sesquicentennial half dollar. Its reverse was used as the basis for the Franklin half dollar's reverse.]]
An 1890 statute forbade the replacement of a coin design without congressional action, unless it had been in service for 25&nbsp;years, counting the year of first issuance.  The [[Walking Liberty half dollar]] and [[Mercury dime]] had been first issued in 1916; they could be replaced without congressional action from and after 1940.{{sfn|Breen|1988|pp=413, 416}}  Mint officials considered putting Franklin on the dime in 1941, but the project was shelved due to heavy demands on the Mint for coins as the United States entered World War II.{{sfn|Tomaska|2011|p=21}}  During the war, the Mint contemplated adding one or more new denominations of coinage; Sinnock prepared a Franklin design in anticipation of a new issue, which did not occur.{{sfn|Tomaska|2002|pp=4–5}}  The dime was redesigned in 1946 to depict fallen President [[Franklin Roosevelt]], who had been closely associated with the [[March of Dimes]].{{sfn|Tomaska|2011|p=21}}  The Walking Liberty design seemed old-fashioned to Mint officials, and the only other coin being struck which was eligible for replacement was the [[Lincoln cent]].  [[Abraham Lincoln]] remained a beloved figure, and Ross did not want to be responsible for removing him from the coinage.{{sfn|Lange|2006|p=172}}

In 1947, Ross asked Sinnock to produce a design for a half dollar featuring Franklin.  The chief engraver adapted his earlier work for the obverse.{{sfn|Taxay|1983|p=376}}  He had designed the medal from a bust of Franklin by [[Jean-Antoine Houdon]].{{sfn|Tomaska|2011|p=21}} Sinnock based his design for the reverse on the 1926 commemorative half dollar for the sesquicentennial (150th anniversary) of American Independence.{{sfn|Taxay|1983|p=376}} Numismatic writer [[Don Taxay]] later discovered that Sinnock had based his Liberty Bell (as depicted on both the Sesquicentennial half dollar and the Franklin half) on a sketch by John Frederick Lewis.{{sfn|''Coin Community Family''}} Sinnock died in May 1947, before finishing the reverse design, which was completed by the new chief engraver, [[Gilroy Roberts]].{{sfn|Tomaska|2011|p=21}}  Similar to Sinnock's work for the Roosevelt dime, the portrait is designed along simple lines, with Franklin depicted wearing a period suit.  The small eagle on the reverse was added as an afterthought, when Mint officials realized that the [[Coinage Act of 1873]] required one to be displayed on all coins of greater value than the dime.{{sfn|Lange|2006|p=172}}

The Mint sought comments on the designs from the [[Commission of Fine Arts]], which was provided with a lead striking of the obverse and a view of the reverse; Taxay suggests they were shown a plaster model.  On December 1, 1947, Commission chairman [[Gilmore Clarke]] wrote to Ross saying that they had no objection to the obverse, in which they recognized Sinnock's "good workmanship".{{sfn|Taxay|1983|pp=376–377}}  As for the reverse,

{{quote |
The eagle shown on the model is so small as to be insignificant and hardly discernible when the model is reduced to the size of a coin.  The Commission hesitate to approve the Liberty Bell as shown with the crack in the bell visible; to show this might lead to puns and to statements derogatory to United States coinage.

The Commission disapprove the designs.{{sfn|Taxay|1983|pp=376–377}}
}}

Numismatist Paul Green later noted, "Over the years there would probably have been even more puns and derogatory statements if there had been an attempt to depict the bell without a crack."{{sfn|Tomaska|2002|p=xi}}  The Commission suggested a design competition under its auspices.{{sfn|Taxay|1983|p=377}}  Its recommendations, which were only advisory,{{sfn|Tepper|April 1990}} were rejected by the Treasury Department and the coin was approved by Treasury Secretary [[John W. Snyder]], which Taxay ascribes to an unwillingness to dishonor Sinnock.{{sfn|Taxay|1983|p=377}}

== Release and production ==
[[File:Franklin medal.jpeg|thumb|[[John R. Sinnock]]'s medal of Franklin served as the basis of his obverse for the half dollar.]]
On January 7, 1948, the Treasury issued a press release announcing the new half dollar.  The Commission's disapproval went unreported; instead the release noted that the design had been Ross's idea and had received Secretary Snyder's "enthusiastic approval".{{sfn|United States Department of the Treasury|1948-01-07}}  The release noted Franklin's reputation for thrift, and expressed hope that the half dollar would serve as a reminder that spare cash should be used to purchase [[savings bond]]s and [[savings stamp]]s.  Franklin became the fifth person and first non-president to be honored by the issuance of a regular-issue US coin, after Lincoln, Roosevelt, [[George Washington]] and [[Thomas Jefferson]].{{sfn|United States Department of the Treasury|1948-01-07}}

In a speech given when she unveiled the design in January 1948, Ross indicated that she had been urged to put Franklin on the cent because of his association with the adage "a penny saved is a penny earned" (in Franklin's original, "A penny saved is twopence dear".){{sfn|''Coin Community Family''}}  Ross stated, "You will agree, I believe, that the fifty-cent piece, being larger and of silver, lends itself much better to the production of an impressive effect."{{sfn|''Coin Community Family''}}  On April 29, 1948, the day before the coin's public release, Ross held a dinner party for 200 at the [[Franklin Institute]] in Philadelphia; each guest received a Franklin half dollar in a card signed by Ross.{{sfn|Tomaska|2002|p=3}}{{sfn|Breen|1988|p=416}}

The new half dollars first went on sale at noon on April 30, 1948, the anniversary of [[George Washington]]'s [[First inauguration of George Washington|1789 inauguration as President]].  They were sold from a booth on the steps of the Sub-Treasury Building in New York, by employees of the Franklin Savings Bank dressed in Revolutionary-era garb.{{sfn|''The New York Times''|1948-05-01}}

[[File:Nellie Tayloe Ross medal.jpeg|thumb|left|[[Nellie Tayloe Ross]], seen in this medal by Sinnock, favored placing Franklin on the half dollar.]]
The [[Roosevelt dime]] had been designed by Sinnock, and had provoked complaints by citizens viewing Sinnock's initials "JS" on the coin as those of [[Joseph Stalin]], placed there by some [[Kremlin]] infiltrator within the Mint. Even though Sinnock's initials (placed on the cutoff of Franklin's bust) were expressed "JRS", the Mint still received similar complaints, to which they responded with what numismatic historian [[Walter Breen]] termed "outraged official denials".{{sfn|Tomaska|2011|p=21}}{{sfn|Breen|1988|p=416}}  According to ''[[The New York Times]]'', "People wrote in demanding to know how the Bureau of the Mint had discovered that Joe Stalin had a middle name."{{sfn|''The New York Times''|1959-02-15}} Another rumor was that the small "o" in "of" was an error, and that the coins would be recalled.  This claim died more quickly than the Stalin rumor.{{sfn|Breen|1988|p=416}}

After the [[assassination of John F. Kennedy]] on November 22, 1963, Congress and the Mint moved with great speed to authorize and produce a half dollar in tribute to him.  With the authorization of the [[Kennedy half dollar]] on December 30, 1963, the Franklin half dollar series came to an end.{{sfn|Breen|1988|p=418}}  Breen reports rumors of 1964 Franklin half dollars, produced possibly as trial strikes to test 1964-dated dies, but none has ever come to light.{{sfn|Breen|1988|p=416}}  A total of 465,814,455 Franklin half dollars were struck for circulation; in addition, 15,886,955 were struck in proof.{{sfn|Guth|Garrett|2005|p=96}}
{{clear}}

== Collecting ==
The Franklin half dollar was struck in relatively small numbers in its first years,{{sfn|Lange|2006|p=172}} as there was limited demand due to a glut of Walking Liberty halves.  No half dollars were struck at Denver in 1955 and 1956 due to a lack of demand for additional pieces.{{sfn|''Daytona Beach Sunday News-Journal''|1975-03-16}}  The San Francisco Mint closed in 1955; it did not reopen until 1965.{{sfn|AP/''Ocala Star-Banner''|1965-08-22}}  In 1957, with improved economic conditions, demand for the pieces began to rise.{{sfn|''Daytona Beach Sunday News-Journal''|1975-03-16}}  They were struck in much greater numbers beginning in 1962, which saw the start of the greatly increased demand for coins which would culminate in the great coin shortage of 1964.{{sfn|Lange|2006|p=172}}  No Franklin half dollar is rare today, as even low-mintage dates were widely saved.  [[Proof coin]]s were struck at the Philadelphia Mint from 1950.  "Cameo proofs", with frosted surfaces and mirror-like fields, were struck in small numbers and carry a premium.  Just under 498&nbsp;million Franklin half dollars, including proofs, were struck.{{sfn|''Coin Community Family''}}

[[File:1955-Franklin-Half-Dollar-Bugs-Bunny-Variety-Close-Up.jpg|thumb|Close-up of the "Bugs Bunny" variety of the Franklin half dollar]]
There are only 35&nbsp;different dates and mintmarks in the series, making it a relatively inexpensive collecting project.{{sfn|''Daytona Beach Sunday News-Journal''|1975-03-16}}  A widely known variety is the 1955 "[[Bugs Bunny]]" half.  This variety was caused by a die clash between an obverse die and a reverse die.  The impact of the eagle's wings on the other die caused a marking outside of Franklin's mouth which, according to some, resembles buck teeth.{{sfn|Tomaska|2011|p=68}}  The quality of half dollars struck by the Mint decreased in the late 1950s, caused by deterioration of the master die from which working dies were made for coinage.{{sfn|Tomaska|2011|p=84}}

In an initial attempt to improve the quality of the pieces, the Mint made slight modifications to the designs, though both the old (Type I) and new (Type II) were struck in 1958 and 1959.  One obvious difference between the types is the number of long tail feathers on the eagle—Type I half dollars have four tail feathers, Type II only three.  Approximately 20% of the 1958 Philadelphia coinage is Type II, struck from dies which were first used to strike the 1958 proofs.  About 70% of the 1959 half dollars struck at Philadelphia are Type II; all 1958-D and 1959-D half dollars are Type I.{{sfn|Tepper|April 1990}} The Mint recut the master die before beginning the 1960 coinage, improving quality.{{sfn|Tomaska|2011|p=84}}

An especially well-struck Franklin half dollar is said to have full bell lines. To qualify, the seven parallel lines making up the bottom of the bell must be fully visible, and the three wisps of hair to the right of Franklin's ear on the obverse must also fully show, and not blend together.{{sfn|''Coin World'', Franklin half dollar}} Many Franklins have been damaged by "roll friction": the tendency of pieces in a loose coin roll to rub together repeatedly, causing steel-gray abrasions, usually on Franklin's cheek and on the center of the Liberty Bell.{{sfn|Tepper|May 1990}}

By mintages, the key dates in this series are the 1948, 1949-S, 1953 and 1955. Franklin half dollars have been extensively melted for their silver, and many dates are rarer than the mintage figures indicate.{{sfn|''Coin World'', Franklin half dollar}} For example, although more than nine million 1962 halves were struck for circulation, and an additional three million in proof, the coin was more valuable as bullion than in any condition [[Nelson Bunker Hunt#Silver Thursday|when silver prices reached record levels]] in 1979–1980. In 2010, the 1962 half in [[Mint state|MS]]-65 condition sold for about US$145, second only to the 1953-S in price in that grade.{{sfn|Green|2010-07-14}}

=== Mintage figures ===

[[File:Numismatist June 1948.jpeg|thumb|The release of the Franklin half dollar was front page news in the coin collecting world, as seen by the June 1948 ''[[The Numismatist]]''.]]

{| class="wikitable plainrowheaders"
|+ Mintage figures
|-
! scope="col" | Year
! scope="col" | [[Philadelphia Mint]]{{sfn|Breen|1988|pp=417–418}}
! scope="col" | [[Denver Mint]]{{sfn|Breen|1988|pp=417–418}}
! scope="col" | [[San Francisco Mint]]{{sfn|Breen|1988|pp=417–418}}
|-
! scope="row" | 1948
| 3,006,814
| 4,028,500
|
|-
! scope="row" | 1949
| 5,614,000
| 4,120,500
| 3,744,000
|-
! scope="row" | 1950
| 7,793,509 (51,386)
| 8,031,600
|
|-
! scope="row" | 1951
| 16,859,602 (57,500)
| 9,475,200
| 13,696,000
|-
! scope="row" | 1952
| 21,274,073 (81,980)
| 25,395,600
| 5,526,000
|-
! scope="row" | 1953
| 2,796,820 (128,800)
| 20,900,400
| 4,148,000
|-
! scope="row" | 1954
| 13,421,502 (233,300)
| 25,445,580
| 4,993,400
|-
! scope="row" | 1955
| 2,876,381 (378,200)
|
|
|-
! scope="row" | 1956
| 4,701,384 (669,384)
|
|
|-
! scope="row" | 1957
| 6,361,952 (1,247,952)
| 19,966,850
|
|-
! scope="row" | 1958
| 4,917,652 (875,652)
| 23,962,412
|
|-
! scope="row" | 1959
| 7,349,291 (1,149,291)
| 13,053,750
|
|-
! scope="row" | 1960
| 7,715,602 (1,691,602)
| 18,215,812
|
|-
! scope="row" | 1961
| 11,318,244 (3,028,244)
| 20,276,442
|
|-
! scope="row" | 1962
| 12,932,019 (3,218,019)
| 35,473,281
|
|-
! scope="row" | 1963
| 25,239,645 (3,075,645)
| 67,069,292
|
|}

'''Note:''' Numbers in parentheses represent coins which were distributed in proof sets, which are also included in the totals.

== References ==
{{reflist|20em}}

== Bibliography ==

{{refbegin}}
* {{cite book
  | last = Breen
  | first = Walter
  | authorlink = Walter H. Breen
  | year = 1988
  | title = Walter Breen's Complete Encyclopedia of U.S. and Colonial Coins
  | publisher = Doubleday
  | location = New York
  | isbn = 978-0-385-14207-6
  | ref = harv
  }}
* {{cite book
  | last1 = Guth
  | first1 = Ron
  | last2 = Garrett
  | first2 = Jeff
  | year = 2005
  | title = United States Coinage: A Study by Type
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-1782-4
  | ref = harv
  }}
* {{cite book
  | last = Lange
  | first = David W.
  | year = 2006
  | title = History of the United States Mint and its Coinage
  | publisher = Whitman Publishing
  | location = Atlanta
  | isbn = 978-0-7948-1972-9
  | ref = harv
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | authorlink = Don Taxay
  | year = 1983
  | origyear = 1966
  | title = The U.S. Mint and Coinage
  | publisher = Sanford J. Durst Numismatic Publications
  | location = New York
  | edition = reprint
  | isbn = 978-0-915262-68-7
  | ref = harv
  }}
* {{cite book
  | last = Tomaska
  | first = Rick
  | year = 2002
  | title = A Complete Guide to Franklin Half Dollars
  | publisher = DLRC Press
  | location = Virginia Beach, Va.
  | isbn = 978-1-880731-68-0
  | ref = harv
  }}
* {{cite book
  | last = Tomaska
  | first = Rick
  | year = 2011
  | title = A Guide Book of Franklin & Kennedy Half Dollars
  | publisher = Whitman Publishing
  | location = Atlanta
  | isbn = 978-0-7948-3243-8
  | ref = harv
  }}

'''Other sources'''

* {{cite news
  | agency = Associated Press
  | newspaper = The Portsmouth (Ohio) Times
  | date = May 13, 1948
  | title = Benjamin Franklin opposed putting portraits on coins
  | page = 28
  | url = https://news.google.com/newspapers?id=McJQAAAAIBAJ&sjid=RtAMAAAAIBAJ&pg=7494,3392083
  | accessdate = April 4, 2011
  | ref = {{sfnRef|AP/''The Portsmouth Times''|1948-05-13}}
  }}
* {{cite news
  | agency = Associated Press
  | newspaper = Ocala Star-Banner
  | date = August 22, 1965
  | title = Minting of coins with less silver to start on Monday
  | page = 24
  | url = https://news.google.com/newspapers?id=BWJPAAAAIBAJ&sjid=9gQEAAAAIBAJ&pg=2396,223726
  | accessdate = April 25, 2011
  | ref = {{sfnRef|AP/''Ocala Star-Banner''|1965-08-22}}
  }}
* {{cite web
  | publisher = Coin Community Family
  | title = 1948–63 Half Dollar Franklin History
  | url = http://www.coincommunity.com/coin_histories/half_dollar_1948_franklin.asp
  | accessdate = April 4, 2011
  | ref = {{sfnRef|''Coin Community Family''}}
  }}
* {{cite web
  | work = [[Coin World]]
  | title = Franklin half dollar
  | publisher = Amos Press
  | url = http://www.coinworld.com/coinvalues/half-dollar/franklin-half-dollar.html
  | accessdate = November 10, 2014
  | ref = {{sfnRef|''Coin World'', Franklin half dollar}}
  }}
* {{cite journal
 |last=Green 
 |first=Paul M. 
 |date=July 14, 2010 
 |title=1962 Franklin Half Dollars Melted, Now Valuable 
 |journal=Coin Collecting News 
 |volume= 
 |issue= 
 |url=http://www.coincollectingnews.org/1962-franklin-half-dollars-melted-now-valuable/1012538 
 |accessdate=April 22, 2011 
 |ref={{sfnRef|Green|2010-07-14}} 
 |deadurl=bot: unknown 
 |archiveurl=https://web.archive.org/web/20111006025433/http://www.coincollectingnews.org/1962-franklin-half-dollars-melted-now-valuable/1012538 
 |archivedate=October 6, 2011 
 |df= 
}} <!-- http://www.numismaster.com/ta/numis/Article.jsp?ad=article&ArticleId=12234 -->
* {{cite news
  | newspaper = [[The New York Times]]
  | date = May 1, 1948
  | title = New half dollar for museum archives
  | url = http://select.nytimes.com/gst/abstract.html?res=FB0914FE3D54157B93C3A9178ED85F4C8485F9&scp=13&sq=Franklin+half+dollar&st=p
  | accessdate = April 5, 2011
  | ref = {{sfnRef|''The New York Times''|1948-05-01}}
  }} {{subscription required}}
* {{cite news
  | newspaper = Daytona Beach Sunday News-Journal
  | date = March 16, 1975
  | last = Restifo
  | first = Harriett
  | title = The Franklin half
  | page = 10C
  | url = https://news.google.com/newspapers?id=SEwfAAAAIBAJ&sjid=g9EEAAAAIBAJ&pg=1601%2C204937
  | accessdate = April 25, 2011
  | ref = {{sfnRef|''Daytona Beach Sunday News-Journal''|1975-03-16}}
  }}
* {{cite news
  | last = Shuster
  | first = Alvin
  | date = February 15, 1959
  | newspaper = [[The New York Times]]
  | title = Change of a penny
  | page = 15 (Magazine)
  | url = http://select.nytimes.com/gst/abstract.html?res=F7061EFE3A5B107B93C7A81789D85F4D8585F9&scp=5&sq=%22franklin+half+dollar%22&st=p
  | accessdate = April 5, 2011
  | ref = {{sfnRef|''The New York Times''|1959-02-15}}
  }} {{subscription required}}
* {{cite journal
  | last = Tepper
  | first = Jonathan
  | date = April 1990
  | journal = [[Numismatist (magazine)|The Numismatist]]
  | publisher = [[American Numismatic Association]]
  | title = Circulation strike Franklin half dollars, Part I
  | volume = 
  | issue = 
  | pages = 544–546, 617
  | ref = {{sfnRef|Tepper|April 1990}}
  }}
* {{cite journal
  | last = Tepper
  | first = Jonathan
  | date = May 1990
  | journal = [[Numismatist (magazine)|The Numismatist]]
  | publisher = [[American Numismatic Association]]
  | title = Circulation strike Franklin half dollars, Part II
  | volume = 
  | issue = 
  | pages = 736–739, 771–772
  | ref = {{sfnRef|Tepper|May 1990}}
  }}
* {{cite web
  | publisher = United States Department of the Treasury
  | date = January 7, 1948
  | title =  Press service no. S-589
  | url = https://docs.google.com/viewer?a=v&q=cache:sdM3AIQBaT8J:www.usmint.gov/historianscorner/index.cfm?action%3DDocDL%26doc%3Dpr545.doc+the+story+of+the+new+benjamin+franklin+half-dollar+ross&hl=en&gl=us&pid=bl&srcid=ADGEESgGxCsNlAAwpzwf0pO7d8K2LS31Oo5yaJvwJxd3OJlFUhFPRBc2U_SmgxCIOt9teP1y1JsFBZlN0OsLiylEUQ5y3Uh0iwyLPo3dKb96-1_4VGay3tn6as1ULApT-1UBzeegYQQ3&sig=AHIEtbSO_O43NRIFrHNLp2ol9jFfA9xpLg&pli=1
  | format = DOC
  | accessdate = April 5, 2011
  | ref = {{sfnRef|United States Department of the Treasury|1948-01-07}}
  }}
{{refend}}

== External links ==
*[http://www.coincommunity.com/us_coin_links/us_halves_franklin.asp US Franklin Half Dollar by year.] Histories, photos, and more.
*[http://www.coinpage.com/franklin%20half-pictures.html Franklin Half Dollar Pictures]

{{s-start}}
{{succession box
| title  = [[Half dollar (United States coin)]] (1948–1963)
| before = [[Walking Liberty half dollar]]
| after  = [[Kennedy half dollar]]
| years =
}}
{{s-end}}

{{Benjamin Franklin}}
{{Coinage (United States)|state=expanded}}

{{Featured article}}
[[Category:1948 introductions]]
[[Category:Coins of the United States]]
[[Category:United States silver coins]]
[[Category:Fifty-cent coins]]
[[Category:Works about Benjamin Franklin]]