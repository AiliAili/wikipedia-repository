{{ Infobox book
|
| image     = The Black Gauntlet.jpg
| caption = ''First edition title page''
| name          = The Black Gauntlet: A Tale of Plantation Life in South Carolina
| author        = Mary Howard Schoolcraft (as Mrs. Henry Rowe Schoolcraft)
| illustrator   =
| cover_artist  =
| country       = United States
| language      = English
| genre         = [[Anti-Tom literature|Plantation literature]]
| publisher     =
| release_date  = [[1860 in literature|1860]]
| media_type    = Print ([[Hardcover]] and [[Paperback]]) & [[E-book]]
| pages         = c. 100 (May change depending on the publisher and the size of the text)
}}

'''''The Black Gauntlet: A Tale of Plantation Life in South Carolina''''' (also known as simply '''''The Black Gauntlet''''') is an [[Anti-Tom literature|anti-Tom]] novel written in [[1860 in literature|1860]] by Mary Howard Schoolcraft, published under her married name of Mrs. [[Henry Rowe Schoolcraft]].

== Background ==
Mary Howard (d. 1878) was born into the [[sowing|planter]] slaveholding elite of [[South Carolina]]. She was the second wife of the widower and [[ethnologist]] [[Henry Rowe Schoolcraft]], who was 53 when they married in 1846. They lived in [[Washington, DC]] and after their deaths were each buried in the [[Congressional Cemetery]].<ref name=Howard>{{FAG|8080807|Mary ''Howard'' Schoolcraft}}</ref>

''The Black Gauntlet'' is an example of the pro-slavery plantation literature genre that was written in response to the anti-slavery novel ''[[Uncle Tom's Cabin]]'' ([[1852 in literature|1852]]) by [[Harriet Beecher Stowe]].  Critics accused Stowe of exaggerating (or inaccurately depicting) Southern society, slaveholders, slaves and the institution of slavery in the South.<ref>[http://www.enotes.com/uncle-toms/ "Uncle Tom's Cabin"], ENotes</ref>

''The Black Gauntlet'' is unusual as a late example, as the majority were written and published soon after ''Uncle Tom's Cabin'' in 1852. The competing novels were part of the public, rhetorical arguments between North and South in the years of rising political and social tensions before the [[American Civil War]].

== Plot ==
Unlike other anti-Tom novels, ''The Black Gauntlet'' does not have a discernible narrative. It is essentially a collection of speeches by characters who argue in favor of [[Slavery in the United States|American slavery]] as an institution. Some of the speeches were created by Schoolcraft.  In other cases, she refers to quotations from other published works, including [[Bible|the Bible]] and ''Uncle Tom's Cabin''.<ref>[http://www.iath.virginia.edu/utc/proslav/schoolhp.html  "''The Black Gauntlet''"], Stephen Railton, Pro-Slavery Responses, ''Uncle Tom's Cabin & American Culture'', University of Virginia, accessed 3 April 2011</ref>

== In other works ==
Schoolcraft's work used quotes which had also appeared in ''[[Aunt Phillis's Cabin]]'' ([[1852 in literature|1852]]) by [[Seth and Mary Eastman|Mary Henderson Eastman]], a native Virginian.<ref>Preface of ''Aunt Phillis's Cabin'' - M.H. Eastman (1852)</ref>

== References ==
{{reflist}}

== External links ==
{{Portal|United States|Novels}}
* [https://archive.org/details/blackgauntletat00schogoog ''The Black Gauntlet''], text online
* {{OCLC|742997047|123764}}

{{Uncle Tom's Cabin}}

{{DEFAULTSORT:Black Gauntlet}}
[[Category:1860 novels]]
[[Category:Anti-Tom novels]]
[[Category:19th-century American novels]]
[[Category:Novels set in South Carolina]]