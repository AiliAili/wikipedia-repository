<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name = KAL-2
 | image = Japan 300316 Tokorozawa KAL-2 01.jpg
 | caption =
}}{{Infobox Aircraft Type
 | type = Four/ five seat cabin aircraft
 | national origin = [[Japan]]
 | manufacturer = [[Kawasaki Heavy Industries Aerospace Company|Kawasaki]]
 | designer =
 | first flight = 25 November 1954
 | introduced =
 | retired =
 | status =
 | primary user =
 | more users = <!--Limited to three in total; separate using <br> -->
 | produced = <!--years in production-->
 | number built = 2
 | program cost = <!--Total program cost-->
 | unit cost = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from = 
 | variants with their own articles =
}}
|}

The '''Kawasaki KAL-2''' is a [[Japan]]ese four/five seat, single engine aircraft, designed for both military and civil markets in the mid-1950s. Only two were completed.

==Design and development==
The '''KAL-2''' was a successor to the '''KAL-1''', an earlier cabin monoplane and Kawasaki's first post-[[World War II|war]] design.  It had more in common with its Kawasaki contemporary, the [[Kawasaki KAT-1|KAT-1]] primary [[training aircraft|trainer]] in layout and in shared components.  The major difference between the two types was accommodation, the KAL-2 seating up to five in two rows in a broad cabin whereas the KAT-1 has two seats in tandem in a narrower [[fuselage]].<ref name=JAWA56/>

The KAL-2 is a [[cantilever]] [[monoplane#Types of monoplane|low wing monoplane]].  Its wing is of  blunt [[wing tip|tipped]], approximately [[trapezoidal]] plan but with slightly greater sweep on the centre section [[leading edge]]s than outboard.  It is constructed from two metal spars and stressed aluminium skin.  Inboard of the [[aileron]]s, which are [[aircraft fabric covering|fabric covered]] over aluminium alloy frames and [[aileron#Mass balance weights|mass balanced]], there are hydraulically operated [[flap (aircraft)#Types of flaps|split flaps]]. The horizontal tail, mounted on the top of the [[fuselage]], is also straight tapered with blunt tips but the [[fin]] and [[rudder]] are more rounded, with a [[Dorsal (anatomy)|dorsal]] [[fillet (aircraft)|fillet]].  All the rear surfaces have alloy frames and fabric covering.  The rudder and [[Elevator (aeronautics)|elevator]] are both statically and aerodynamically balanced and carried [[trim tab]]s.<ref name=JAWA56/>

The fuselage of the KAL-2 is a stressed skin alloy [[monocoque|semi-monocoque]] with a 240&nbsp;hp (179&nbsp;kW) [[Lycoming GO-435]] [[flat-six]] engine installed in the nose, driving a two blade [[propeller (aircraft)#Variable pitch|variable-pitch propeller]].  The military prototypes had the -435-C2 engine variant but civil KAT-2s were intended to have a -435-C2B. The [[tandem#Side-by-side seating|side-by-side]] seating in the KAL-2 required a wider fuselage than that of the KAT-1; unlike the KAL-1, where the rear cabin roof merged into the upper fuselage line,<ref name=JapReg/> the transparent roof of the KAL-2s cabin drops down aft to a lower rear fuselage. There are two seats in front, fitted with dual control and a bench seat for two or three passengers behind.  Those at the front have an explosively released starboard side door for emergency escape, rear seat occupants leaving via canopy roof panels.  The interior has sound-proofing and air-conditioning. The KAL-2 has a retractable [[tricycle undercarriage]] with oleo-pneumatic [[shock absorbers]] and [[hydraulic]] brakes.<ref name=JAWA56/>

==Operational history==
Two prototypes were flown. One prototype served with the [[Japan Air Self-Defense Force]], the other with the [[Japan Maritime Self-Defense Force]].<ref name=JAWA56/>

==Aircraft on display==
*[[Tokorozawa Aviation Museum]]:<ref name=Toko/> KAL-2 serial ''20001''.
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (civil version)==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1956/57<ref name=JAWA56/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|capacity=Four to five
|length m=8.820
|length note=
|span m=11.924
|span note=
|height m=2.694
|height note=
|wing area sqm=19.6
|wing area note=
|aspect ratio=7.2
|airfoil=[[NACA]] 2R1 16.5 at root, [[NACA]] 2410 at tip 
|empty weight kg=1110
|empty weight note=
|gross weight kg=16000
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=300 L (66 Imp gal; 79 US gal)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Lycoming GO-435]]-C2B
|eng1 type=6-cylinder [[horizontally opposed]] air-cooled
|eng1 hp=240
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=[[Hartzell Propeller]]
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=7
|prop dia in=6
|prop dia note=metal, variable pitch

<!--
        Performance
-->
|perfhide=

|max speed kmh=293
|max speed note=at sea level
|cruise speed kmh=210
|cruise speed mph=
|cruise speed kts=
|cruise speed note=40% power at 2,135 m (7,000 ft)
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1600
|range note=maximum, at 40% power at 2,135 m (7,000 ft)
|endurance=<!-- if range unknown -->
|ceiling m=4500
|ceiling note=service
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=4.7
|climb rate note=initial
|time to altitude=
|wing loading kg/m2=81.5
|wing loading note=

|power/mass=91 W/kg (0.058 hp/lb)

|more performance=
*'''Take-off run:''' 350 m (1,150 ft) at sea level, no wind
*'''Landing run:''' 430 m 1,4100 ft at sea level, no wind
*'''Landing speed:''' 87 km/h (54 mph) with flaps extended

}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|refs=

<ref name=JAWA56>{{cite book |title= Jane's All the World's Aircraft 1956-57|last= Bridgman |first= Leonard |coauthors= |edition= |year=1956|publisher= Jane's All the World's Aircraft Publishing Co. Ltd|location= London|isbn=|page=183 }}</ref>

<ref name=JapReg>{{cite web |url=http://ksa.axisz.jp/RS-0003-FirstJA-Numbers3001.htm#074|title=The first 100 JA registrations of single engine aircraft - the first KAL-1 |author= |date= |work= |publisher= |accessdate=14 September 2012}}</ref>

<ref name=Toko>{{cite web |url=http://www.aviationmuseum.eu/World/Asia/Japan/Tokorozawa/Tokorozawa_Aviation_Museum.htm|title=Tokorozawa Aviation Museum |author= |date= |work= |publisher= |accessdate=23 May 2012}}</ref>

}}
<!-- ==Further reading== -->

==External links==
{{commons category|Kawasaki KAL-2}}
*{{cite web |url=http://cdn-www.airliners.net/aviation-photos/photos/4/9/4/1134494.jpg|title=KAL-2 ''20001'' at Tokorozawa Aviation Museum |author= |date= |work= |publisher= |accessdate=15 September 2012}}

<!-- Navboxes go here -->
{{Kawasaki aircraft}}

[[Category:Japanese aircraft 1950–1959]]
[[Category:Kawasaki aircraft|KAL-2]]