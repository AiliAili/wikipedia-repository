{{Infobox astronaut
| name          =Daniel W. Bursch
| other_names   =Daniel Wheeler Bursch
| image         =Daniel Bursch.jpg
| type          =[[NASA]] [[Astronaut]]
| status        =Retired
| nationality   =American
| birth_date    ={{Birth date and age|1957|07|25}}
| birth_place   =[[Bristol, Pennsylvania]], U.S.
| occupation    =[[Engineer]]
| alma_mater    =[[United States Naval Academy|USNA]], B.S. 1979<br>[[Naval Postgraduate School|NPS]], M.S. 1991
| rank          =[[Captain (naval)|Captain]], [[United States Navy|USN]]
| selection     =[[List of astronauts by selection#1990|1990 NASA Group 13]]<ref name=SF-bio/>
| time          =226d 22h 16m<ref name=SF-bio>{{cite web|url=http://www.spacefacts.de/bios/astronauts/english/bursch_daniel.htm|title=Astronaut Biography: Daniel Bursch|publisher=spacefacts.de}}</ref>
| mission       =[[STS-51]], [[STS-68]], [[STS-77]], [[Expedition 4]] ([[STS-108]] / [[STS-111]])
| insignia      =[[File:Sts-51-patch.png|40px]] [[File:Sts-68-patch.png|40px]] [[File:Sts-77-patch.png|40px]] [[File:STS-108 Patch.svg|40px]] [[File:Expedition 4 insignia.png|40px]] [[File:Sts-111-patch.png|40px]]
}}
'''Daniel Wheeler Bursch''' (born July 25, 1957<ref name=NASA-bio>{{cite web|url=http://www.jsc.nasa.gov/Bios/htmlbios/bursch.html|title=Astronaut Bio: Dan Bursch|publisher=NASA|date=July 2007}}</ref>) is a former [[NASA]] [[astronaut]], and [[Captain (naval)|Captain]] of the [[United States Navy]]. He had four [[spaceflight]]s, the first three of which were [[Space Shuttle]] missions lasting 10 to 11 days each. His fourth and final spaceflight was a long-duration stay aboard the [[International Space Station]] as a crew member of [[Expedition 4]], which lasted from December 2001 to June 2002. This 196-day mission set a new record for the [[List of spaceflight records|longest duration spaceflight]] for an American astronaut, a record simultaneously set with his crew mate [[Carl E. Walz|Carl Walz]].<ref name=NASA-bio/> This record has since been broken, and as of 2010 it is held by [[Michael Lopez-Alegria]], who had a 215-day spaceflight as Commander of [[Expedition 14]].

==Education==
Graduated from [[Vestal Senior High School]], [[Vestal, New York]], in 1975; received a [[Bachelor of Science]] degree in [[Physics]] from the [[United States Naval Academy]] in 1979, and a [[Master of Science]] degree in [[Engineering Science]] from the U.S. [[Naval Postgraduate School]] in 1991.<ref name=NASA-bio/>

==Navy career==
Bursch graduated from the U.S. Naval Academy in 1979, and was designated a [[Naval Flight Officer]] in April 1980 at [[Naval Air Station Pensacola]], [[Florida]].<ref name=NASA-bio/> After initial training as an [[A-6 Intruder|A-6E Intruder]] bombardier/navigator (B/N), he reported to [[VFA-34|VA-34]] in January 1981, and deployed to the [[Mediterranean]] aboard the [[aircraft carrier]] {{USS|John F. Kennedy|CV-67|6}}, and to the [[North Atlantic]] and [[Indian Ocean]]s aboard {{USS|America|CV-66|6}}.<ref name=NASA-bio/> He attended the [[United States Naval Test Pilot School|U.S. Naval Test Pilot School]] in January 1984. Upon graduation in December he worked as a project test flight officer flying the A-6 Intruder until August 1984, when he returned to the U.S. Naval Test Pilot School as a [[flight instructor]].<ref name=NASA-bio/> In April 1987, Bursch was assigned to the Commander, [[Cruiser-Destroyer Group 1]], as Strike Operations Officer, making deployments to the Indian Ocean aboard the [[cruiser]] {{USS|Long Beach|CGN-9|6}} and the carrier {{USS|Midway|CV-41|6}}. Redesignated an Aeronautical Engineering Duty officer (AEDO), he attended the U.S. Naval Postgraduate School in [[Monterey, California]], from July 1989 until his selection to the astronaut program.<ref name=NASA-bio/>

He has flown over 3,430 flight hours in more than 35 different aircraft.<ref name=NASA-bio/>

==Astronaut career==
Selected by NASA in January 1990, Bursch became an astronaut in July 1991. His technical assignments to date include: Astronaut Office Operations Development Branch, working on controls and displays for the Space Shuttle and Space Station; Chief of Astronaut Appearances; spacecraft communicator ([[CAPCOM]]) in mission control. A veteran of four space flights, Bursch has logged over 227 days in space. He was a mission specialist on [[STS-51]] (1993), [[STS-68]] (1994) and [[STS-77]] (1996), and served as flight engineer on [[International Space Station]] [[Expedition 4]] (2001–2002). Dan Bursch and fellow astronaut [[Carl Walz]] completed one of the longest U.S. space flights of 196 days in space. In January 2003, Bursch reported to the Naval Postgraduate School for a two-year assignment as an instructor in the Space Systems Academic Group.<ref name=NASA-bio/>

He left NASA in May 2005, and later retired from active duty in July 2005 after 26 years of service in the U.S. Navy. Bursch joined [[The Aerospace Corporation]] in July 2005 and is currently serving as the [[National Reconnaissance Office]] (NRO) Chair at the Naval Postgraduate School.<ref name=NASA-bio/>

===STS-51===
[[STS-51]] launched from the [[Kennedy Space Center]], Florida, on September 12, 1993. During the ten-day mission the crew of five aboard the {{OV|103}} deployed the U.S. [[Advanced Communications Technology Satellite]] (ACTS), and the [[Shuttle Pallet Satellite]] (SPAS) with NASA and German scientific experiments aboard. Following a [[spacewalk]] by two crew members to evaluate [[Hubble Space Telescope]] repair tools, the crew initiated rendezvous burns and Bursch recovered the SPAS using the [[Remote Manipulator System]] (RMS). The mission concluded on September 22, 1993, with the first night landing at the Kennedy Space Center. Mission duration was 236 hours and 11 minutes.<ref name=NASA-bio/>

===STS-68===
[[STS-68]], Space Radar Lab-2 (SRL-2), launched from the Kennedy Space Center, Florida, on September 30, 1994. As part of NASA’s [[Mission to Planet Earth]], SRL-2 was the second flight of three advanced radars called SIR-C/X-SAR (Spaceborne Imaging Radar-C/X-Band Synthetic Aperture Radar), and a carbon-monoxide pollution sensor, MAPS (Measurement of Air Pollution from Satellites). SIR-C/X-SAR and MAPS operated together in [[Space Shuttle Endeavour|''Endeavour'']]{{'}}s cargo bay to study Earth’s surface and atmosphere, creating radar images of Earth’s surface environment and mapping global production and transport of carbon monoxide pollution. Real-time crew observations of environmental conditions, along with over 14,000 photographs aided the science team in interpreting the SRL data. The SRL-2 mission was a highly successful test of technology intended for long-term environmental and geological monitoring of planet Earth. Following 183 [[orbit]]s of the Earth, the eleven-day mission ended with Space Shuttle ''Endeavour'' landing at [[Edwards Air Force Base]], [[California]], on October 11, 1994. Mission duration was 269 hours and 46 minutes.<ref name=NASA-bio/>

===STS-77===
[[STS-77]] launched from the Kennedy Space Center on May 19, 1996. It included the fourth Spacehab module flight as a scientific laboratory, designated SPACEHAB-4. It consisted of 12 separate materials processing, fluid physics and biotechnology experiments, with an emphasis on commercial space product development. STS-77 completed a record four rendezvous in support of two satellites sponsored by the [[Goddard Space Flight Center]], and the SPARTAN 207/Inflatable Antenna Experiment (IAE) and the Passive Aerodynamically stabilized Magnetically damped Satellite/Satellite Test Unit (PAMS/STU). Following 160 orbits of the Earth, the ten-day mission ended with Space Shuttle ''Endeavour'' landing at the Kennedy Space Center on May 29, 1996. Mission duration was 240 hours and 39 minutes.<ref name=NASA-bio/>

===Expedition 4===
The [[Expedition 4]] crew launched on December 5, 2001 aboard [[STS-108]] and docked with the International Space Station on December 7, 2001. During a 6½ month stay aboard the Space Station, the [[Expedition 4]] crew of three (two American astronauts and one Russian cosmonaut) performed flight tests of the station hardware, conducted internal and external maintenance tasks, and developed the capability of the station to support the addition of science experiments. The crew spent 196 days in space establishing a U.S. space flight endurance record for Dan Bursch and crew mate [[Carl Walz]]. Wearing the Russian [[Orlan space suits|Orlan]] spacesuit, Bursch logged 11 hours and 48 minutes of EVA time in two separate spacewalks. The Expedition-Four crew returned to Earth aboard [[STS-111]], with ''Endeavour'' landing at Edwards Air Force Base, California, on June 19, 2002.<ref name=NASA-bio/>

==Special honors==
Awarded the [[Defense Superior Service Medal]], [[NASA Space Flight Medal]]s, the [[Navy Commendation Medal]] and the [[Navy Achievement Medal]]. Distinguished graduate, U.S. Naval Academy and U.S. Naval Test Pilot School.<ref name=NASA-bio/>

==Personal life==
Bursch was born in [[Bristol, Pennsylvania]], but considers [[Vestal, New York]], to be his hometown.<ref name=NASA-bio/> Single.<ref name=NASA-bio/> Four children, two boys and two girls. One grandchild.

==References==
{{Portal|Biography|Spaceflight}}
{{Include-NASA}}
{{reflist}}

==External links==
*[http://www.danbursch.com Dan Bursch's Personal Website]

{{NASA Astronaut Group 13}}

{{Use American English|date=January 2014}}

{{DEFAULTSORT:Bursch, Daniel W.}}
[[Category:1957 births]]
[[Category:Living people]]
[[Category:American astronauts]]
[[Category:United States Navy astronauts]]
[[Category:Crew members of the International Space Station]]
[[Category:People from Bristol, Pennsylvania]]
[[Category:People from Vestal, New York]]
[[Category:United States Naval Academy alumni]]
[[Category:United States Naval Test Pilot School alumni]]
[[Category:Naval Postgraduate School alumni]]
[[Category:Naval Postgraduate School faculty]]
[[Category:United States Navy officers]]
[[Category:United States Naval Flight Officers]]
[[Category:United States Naval Aviators]]
[[Category:Recipients of the Defense Superior Service Medal]]