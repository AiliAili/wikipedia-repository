{{For|the airport in Addison, Alabama|Addison Municipal Airport}}
{{Infobox airport
| name    =Addison Airport
| image   =Addison Airport aerial.jpg
| IATA    =ADS
| ICAO    =KADS
| FAA     = ADS
| type    =Public
| owner   =[[Addison, Texas|City of Addison]]
| operator   =
| city-served=[[Dallas, Texas]]
| location   =[[Addison, Texas]]
| hub        = 
<div>
*[[Ameristar Air Cargo]]
*[[Ameristar Jet Charter]]
*[[Flight Express]]
*[[GTA Air]]
*[[Martinaire]]
</div>
| elevation-f=644
| elevation-m=196.3
| coordinates={{Coord|32|58|07|N|096|50|11|W|region:US_type:airport_scale:10000|display=inline,title}}
| website  =[http://www.addisonairport.net/ www.addisonairport.net]
| pushpin_map            = USA Texas#USA
| pushpin_relief         = yes
| pushpin_map_caption    = Location of airport in Texas / United States
| pushpin_label          = '''ADS'''
| pushpin_label_position = bottom
| r1-number =15/33
| r1-length-f=7,202
| r1-length-m=2,195
| r1-surface =[[Concrete]]
| stat-year =2006
| stat1-header=Aircraft operations
| stat1-data =133,557
| stat2-header=Based aircraft
| stat2-data =774
| footnotes =Source: [[Federal Aviation Administration]]<ref name=FAA>{{FAA-airport|ID=ADS|use=PU|own=PU|site=23710.3*A}}, effective 2007-12-20</ref>
}}
[[Image:ADS - FAA airport diagram.png|thumb|FAA airport diagram]]
[[File:Addison airport.jpg|thumb|right|Runway 15 with Dallas in the background]]

'''Addison Airport''' {{airport codes|ADS|KADS|ADS}} is a public [[airport]] in [[Addison, Texas|Addison]], in [[Dallas County, Texas|Dallas County]], [[Texas]]. It is nine miles (14&nbsp;km) north of downtown Dallas.<ref name=FAA />

The airport opened in 1954 and was purchased by the town of Addison in 1976.<ref>[http://www.addisonairport.net/management/about_us.aspx Addison Airport: History] {{webarchive |url=https://web.archive.org/web/20080601101729/http://www.addisonairport.net/management/about_us.aspx |date=June 1, 2008 }}</ref> The airport is home to the [[Cavanaugh Flight Museum]].

The [[Addison Airport Toll Tunnel]] allows east-west traffic to cross the airport under the runway and was completed in 1999.

==Facilities==
Addison Airport covers {{convert|368|acre}}; its one runway, 15/33, is {{convert|7202|x|100|ft|adj=on}} [[concrete]]. In 2006 the airport had 133,557 aircraft operations, average 365 per day: 88% [[general aviation]], 12% [[air taxi]], <1% airline and <1% military. 774 aircraft were then based at the airport: 49% single-engine, 24% multi-engine, 24% jet and 3% [[helicopter]].<ref name=FAA />

Three fixed-base operators are on the field, [[Atlantic Aviation]], [[Landmark Aviation]], and [[Million Air]].

Charter services are available from a variety of companies, with [[Business Jet Solutions]] and [[Bombardier FlexJet]] having large operations at the field.

The airport is the headquarters of [[Ameristar Air Cargo]], [[GTA Air]], and [[Martinaire]], and also has scheduled freight flights from [[AirNet]], [[Flight Express]], and [[Flight Development]].

The airport is a training hub, with primary to advanced flight instruction available from [[American Flyers (flight school)|American Flyers]], [[BAe ATP|ATP]], [[Monarch Air]], [[PlaneSmart!]] and Lone Star Flyers.

== Cargo ==
{{Airport destination list
|Charter and Transport|[[Lubbock Preston Smith International Airport|Lubbock]], [[San Antonio International Airport|San Antonio]] 
|[[AirNet Express]]|[[MidAmerica St. Louis Airport|Belleville (IL)]], [[Rickenbacker International Airport|Columbus-Rickenbacker]]
|Flight Development|[[MidAmerica St. Louis Airport|Belleville (IL)]]
|[[Flight Express]]|[[George Bush Intercontinental Airport|Houston-Intercontinental]], [[Will Rogers World Airport|Oklahoma City]], [[Shreveport Regional Airport|Shreveport]], [[Tulsa International Airport|Tulsa]], [[Tyler Pounds Regional Airport|Tyler]]
|GTA Air|[[Abilene Regional Airport|Abilene]], [[Austin–Bergstrom International Airport|Austin]], [[El Paso International Airport|El Paso]], [[Laredo International Airport|Laredo (TX)]], [[Tyler Pounds Regional Airport|Tyler]], [[Mid Valley Airport|Weslaco]]
|[[Martinaire]]|[[Abilene Regional Airport|Abilene]], [[Dallas/Fort Worth International Airport|Dallas/Fort Worth]], [[George Bush Intercontinental Airport|Houston-Intercontinental]]
}}

==Accidents and incidents==
The following involved flights departing or arriving at the airport:
*July 19, 1986: All 4 occupants of a [[Cessna 421]], registration number ''N6VR'', were killed when the aircraft suffered an apparent right-hand engine failure, rolled over, and dived into a vacant lot immediately after takeoff from Addison Airport.<ref>{{cite news |author=Jim Zook|title=Addison Air Crash Kills 4|work=[[The Dallas Morning News]] |date=1986-07-20}}</ref> The post-crash investigation revealed that the right-hand engine did not show any obvious signs of failure and its controls were not set to deliver full takeoff power. The crash was attributed to incorrect engine control operation; the pilot had recently purchased the Cessna 421 but had not been formally trained to fly it, and most of his twin-engined experience had been in an airplane with engine controls that operated in the reverse direction of those in the Cessna.<ref>{{cite news|url=http://www.ntsb.gov/ntsb/brief.asp?ev_id=20001213X34171&key=1|title=NTSB Report FTW86FA133|publisher=National Transportation Safety Board|accessdate=2010-04-23}}</ref>
*June 20, 1992: The pilot of a [[Piper J-3|Piper J3C-65 Cub]], registration number ''N3128M'', reported trouble and attempted to return to Addison Airport soon after taking off to test a newly installed engine. While turning to line up with the runway, the airplane suddenly lost altitude, rolled upside down, and crashed in the middle of nearby Beltway Drive, killing the pilot and his passenger. The crash was attributed to breakage of the left-hand [[Elevator (aircraft)|elevator]] control tube due to corrosion.<ref>{{cite news |author=Nancy St. Pierre|title=2 killed as plane crashes|work=[[The Dallas Morning News]] |date=1992-06-21}}</ref><ref>{{cite news|url=http://www.ntsb.gov/ntsb/GenPDF.asp?id=FTW92FA165&rpt=fi|title=NTSB Report FTW92FA165|publisher=National Transportation Safety Board|accessdate=2010-04-23}}</ref>
*January 1, 2004: The pilot and passenger of a [[Bellanca 17-30|Bellanca 17-30A Super Viking]], registration number ''N4104B'', died when the aircraft struck houses in the [[Preston Hollow, Dallas, Texas|Preston Hollow]] neighborhood of nearby Dallas, Texas after departing from Addison Airport bound for [[Amarillo, Texas]]. An intense post-crash fire destroyed two houses and the remains of the Bellanca, but an elderly resident of one house escaped injury after being dragged out of the burning structure by his caregiver, who was also unhurt.<ref>{{cite news |author=Tanya Eiserer|title=Man rescued after plane hits two N. Dallas houses - Caregiver pulls disabled doctor from fire; 2 fliers killed|work=[[The Dallas Morning News]] |date=2004-01-02}}</ref> The crash was attributed to [[spatial disorientation]] in densely clouded [[Instrument flight rules|IFR]] conditions; the pilot had reported a partial instrument panel failure, after which [[radar]] data indicated that he was making left turns instead of right turns as directed by [[air traffic controller]]s.<ref>{{cite news|url=http://www.ntsb.gov/ntsb/brief.asp?ev_id=20040106X00018&key=1|title=NTSB Report FTW04FA052|publisher=National Transportation Safety Board|accessdate=2010-04-22}}</ref>

==References==
{{Reflist|35em}}

==External links==
{{Portal|Dallas|Aviation}}
*[http://www.addisonairport.net/ Addison Airport], official site
*{{FAA-diagram|00768}}
*{{US-airport-ga|ADS}}
**{{FAA-procedures|ADS}}
*[http://www.cavanaughflightmuseum.com/ Cavanaugh Flight Museum]
*[https://web.archive.org/web/20090416175817/http://www.addisontx.gov:80/departments/fire_dep// Addison Fire Department]
*[http://www.dfwinstruments.com/ DFW Instrument Corporation]
{{Addison, Texas}}
{{Dallas airports}}

[[Category:Airports in the Dallas–Fort Worth metroplex]]
[[Category:Buildings and structures in Dallas County, Texas]]
[[Category:Transportation in Dallas County, Texas]]
[[Category:Airports established in 1954]]
[[Category:Airports in Texas]]