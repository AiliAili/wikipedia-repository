{{Infobox journal
| title = Administrative Law Review
| cover = [[File:Administrative Law Review.jpg|180px]]
| discipline = [[Administrative law]], [[Jurisprudence|Legal studies]]
| abbreviation = Admin. L. Rev.
| editor = Student editors at Washington College of Law
| publisher = [[Washington College of Law]] and [[American Bar Association]] Section of Administrative Law & Regulatory Practice
| country = United States
| frequency = Quarterly
| history = 1948–present
| openaccess =
| license =
| impact = 1.115
| impact-year = 2010
| website = http://www.administrativelawreview.org
| link1 = http://digitalcommons.wcl.american.edu/alr/
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 01461100
| LCCN = sf82003051
| CODEN =
| ISSN = 0001-8368
| eISSN =
}}
The '''''Administrative Law Review''''' ([[Bluebook]] abbreviation: ''Admin. L. Rev.'') was established in 1948 and is the official [[law review|law journal]] of the [[American Bar Association]] Section of Administrative Law & Regulatory Practice.

==Overview==
The journal is a quarterly publication that is managed and edited by approximately 80 students at the [[Washington College of Law]]. The 2016–2017 [[Editor-in-Chief]] is Ross Handler.  The journal has been cited by the [[United States Court of Appeals for the District of Columbia Circuit]]<ref>''See, e.g.'', National Mining Ass'n v. Mine Safety and Health Admin., 512&nbsp;F.3d 696, 700 (D.C. Cir. 2008); Central Texas Telephone Co-op., Inc. v. FCC, 402&nbsp;F.3d 205, 210-11 (D.C. Cir. 2005).</ref> (which is known as the administrative law circuit), and since 2000 has been cited by the Second,<ref>Sweet v. Sheahan, 235&nbsp;F.3d 80, 88 (2d Cir. 2000).</ref> Fourth,<ref>U.S. v. Duke Energy Corp., 411&nbsp;F.3d 539, 548 n.6 (4th Cir. 2005).</ref> Fifth,<ref>Walton v. Rose Mobile Homes LLC, 298&nbsp;F.3d 470, 490 (5th Cir. 2005).</ref> Ninth,<ref>U.S. v. Kriesel, 508&nbsp;F.3d 941, 945 (9th Cir. 2007).</ref> and Tenth [[United States courts of appeals|Circuit Courts of Appeal]].<ref>Dalton v. U.S. Dep't of Labor, 58 Fed. App'x 442, 445 (10th Cir. 2003).</ref> It was also cited by the [[Supreme Court of the United States]].<ref>[[Immigration and Naturalization Service v. Chadha]], 462 U.S. 919, 955 n.19 (1983).</ref>

==Admissions==
The Administrative Law Review selects staff members based on a competitive exercise that tests candidates on their editing skills, research skills, legal analysis skills, and legal writing ability. There is not a preset number of accepted candidates each year; recent classes of new editors have ranged from about 45 to 50. The candidate "write-on" exercise is distributed to candidates during their second semester at the law school.  An optional "grade-on" process allows students to become staff members based solely on their grades.  Transfer students are also eligible for admission through a fall write-on process.

==References==
{{reflist}}

==External links==
* {{Official website|http://www.administrativelawreview.org}}
* [http://www.abanet.org/adminlaw/home.html American Bar Association: Section of Administrative Law & Regulatory Practice]

{{American University}}
{{Law}}
{{Washington, D.C.}}

[[Category:American law journals]]
[[Category:American Bar Association]]
[[Category:American University]]
[[Category:Administrative law journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1948]]
[[Category:English-language journals]]
[[Category:Law journals edited by students]]