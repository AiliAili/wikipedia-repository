{{Infobox military person
| name           = Harald Auffarth
| image          = Harald Auffarth.jpg
| caption        = 
| birth_date     = ~1896
| birth_place    = 
| death_date     = {{Death date|df=yes|1946|10|12}}
| death_place    = [[Eschenbach (Göppingen)|Eschenbach]], [[Kingdom of Württemberg]]
|allegiance= {{flag|German Empire}}, {{flag|Nazi Germany}}
|branch= [[Luftstreitkräfte]], [[Luftwaffe]]
|serviceyears= 1914-1918, 1943-1945
|rank= [[Oberstleutnant]]
|battles 	= [[World War I]], [[World War II]]
| awards=[[Pour le Mérite]] (Recommended but not approved), [[Iron Cross]], [[Royal House Order of Hohenzollern]], [[Hanseatic Cross]], Silver [[Wound Badge]]
}}

'''Eduard Florus Harald Auffarth''' (also Auffahrt) (~1896&nbsp;– 12 October 1946) [[Royal House Order of Hohenzollern]],<ref name="Aerodrome2">{{cite web|work=The Aerodrome|url=http://www.theaerodrome.com/medals/germany/prussia_rhoh.php|accessdate=8 October 2008|title=Royal House Order of Hohenzollern}}</ref> [[Iron Cross]],<ref name="Aerodrome4">{{cite web|work=The Aerodrome|url=http://www.theaerodrome.com/medals/germany/prussia_ic.php|accessdate=8 October 2008|title=Iron Cross}}</ref> Silver [[Wound Badge]],<ref name="Aerodrome5">{{cite web|work=The Aerodrome|url=http://www.theaerodrome.com/medals/germany/wb.php|accessdate=13 November 2013|title=Wound Badge}}</ref> [[Hanseatic Cross]],<ref name="Aerodrome6">{{cite web|work=The Aerodrome|url=http://www.theaerodrome.com/medals/germany/hamburg_hc.php|accessdate=13 November 2013|title=Hanseatic Cross}}</ref> was a [[World War I]] [[Germany|German]] [[flying ace|fighter ace]] credited with 29 victories.<ref name="Aerodrome">{{cite web|work=The Aerodrome|url=http://www.theaerodrome.com/aces/germany/auffarth.php|accessdate=8 October 2008|title=Harald Auffarth}}</ref> After the war, he ran an aviation training school that covertly supported establishment of the ''[[Luftwaffe]]''.<ref name="Aerodrome3">{{cite web|work=The Aerodrome|url=http://www.theaerodrome.com/forum/people/17652-harold-auffarth.html|accessdate=8 October 2008|title=Harold Auffarth}}</ref>{{better source|date=November 2013}}

==Early life==
Eduard Florus Harald Auffarth's date and place of birth are unknown. However, German youths were generally not accepted for military duty before their 18th birthday. Based on Auffarth's service records, this would seem to indicate a birth date at least prior to 1899, with 1896 or earlier more likely. His later award of a [[Hanseatic Cross]]<ref name="Aerodrome6"/> from Bremen would indicate his birth in [[Bremen]], [[Bremerhaven]], or the vicinity; the various kingdoms of the German Empire generally awarded medals only to their subjects. Germany's supreme award, the Prussian ''[[Pour le Merite]]'', was an exception to this rule.<ref>{{cite web|work=pourlemerite.org|title=Order of the Pour le Merite|url=http://www.pourlemerite.org/}}</ref>

==World War I Flying Service==
A typical career for a German fighter pilot of World War I began with service in a ground unit; wounds and awards for bravery (typically an [[Iron Cross]]<ref name="Aerodrome4"/>) could lead to approval of requests for aviation training. After training, the new flier usually served in a reconnaissance unit flying two-seated aircraft. Auffarth served in two of these units. He first served in ''Feldflieger Abteilung'' 27. He then transferred to ''Flieger Abteilung Artillerie'' 266,<ref name="Aerodrome"/><ref name=Lines>Franks et al 1993, pp. 63-64.</ref> which would have entailed the pilot flying while the observer was spotting and correcting artillery fire upon the enemy. Typically, the observer was the senior man in the crew.

Auffarth transferred to flying fighters with Prussian ''[[Jagdstaffel 18]]'' and recorded his first victory with them on 16 September 1917.<ref name="Aerodrome"/> On the last day of the month, he became an ace.<ref name="Aerodrome"/> He then transferred to Prussian ''[[Jagdstaffel 29]]''.<ref name=Lines/>

He was appointed [[Staffelführer]] (commanding officer) on either 11 or 19 November 1917 (depending on source),<ref name=Lines/> as an [[Oberleutnant]] and would lead the squadron for almost a year. At any rate, he scored his first victory with them on 13 November and would eventually score a quarter of his new squadron's victories,<ref name="theaerodrome.com">{{cite web|work=The Aerodrome|url=http://www.theaerodrome.com/services/germany/jasta/jasta29.php|title=Jasta 29|accessdate=13 November 2013}}</ref> flying a succession of [[Albatros Flugzeugwerke|Albatros]]es, [[Fokker D.VII]]s, and a rare [[Pfalz D.VIII]], all in his personal colour scheme of yellow nose and green fuselage with a stylized eight-pointed comet on the side.<ref>{{cite web|work=History in Illustration|first=Bob|last=Pearson|url=http://www.cbrnp.com/profiles/quarter1/pfalzd8.htm|accessdate=8 October 2008|title=Pfalz D.VIII}}</ref>

He accumulated victories erratically until his total stood at 13 as of 27 March 1918. Auffarth received the [[House Order of Hohenzollern]] on 11 April 1918. He scored again on 17 May 1918. On 28 May 1918, he was wounded in the hand. The wound would keep him off duty until 21 July 1918. The day after his return, he shot down a [[Royal Aircraft Factory SE.5]]a over Loos. When his victory count reached 20 on 3 September 1918, as was customary, he was recommended for the ''Pour le Merite''. It would not be approved before [[Kaiser Wilhelm II]]'s abdication.<ref name="Aerodrome2"/><ref name=Lines/>

Auffarth was promoted to command of ''[[Jagdgruppe Nr. 3 (German Empire)|Jagdgruppe Nr. 3]]'' on 28 September 1918. His new fighter wing consisted of [[Jagdstaffel 14]], [[Jagdstaffel 16]], and [[Jagdstaffel 56]], as well as Jasta 29. He seemed to fly with the latter, as he continued to credit his victories to Jasta 29.<ref name="theaerodrome.com"/> He then had his finest month as a fighter pilot. He downed an [[Airco DH.9]] on 5 October, being downed himself in the process. He scored two victories on 9 October and three each on 14 and 30 October, bringing his total to 29.<ref name="Aerodrome"/><ref name=Lines/>

==Post World War I==
He survived the war despite at least three wounds, which was the minimum required to win the Silver [[Wound Badge]].<ref name="Aerodrome5"/> He established ''Fliegerschule'' Auffarth (Auffarth's Flying School) in Munster in 1924. This was one of ten flying schools that surreptitiously supported covert military aviation training in Germany in violation of the Treaty of Versailles. The school was closed in 1929,<ref name="Aerodrome3"/>{{better source|date=November 2013}} just before Germany began training military pilots deep within Russia.

Auffarth returned to service for [[World War II]]. He was stationed in [[Eschenbach (Göppingen)|Eschenbach]] in May 1943 as an ''[[Oberstleutnant]]'' or [[lieutenant colonel]]. He died there on 12 October 1946.<ref name="Aerodrome"/>

==Endnotes==
{{Reflist|2}}

==References==
* ''Above the Lines: The Aces and Fighter Units of the German Air Service, Naval Air Service and Flanders Marine Corps, 1914–1918''. [[Norman Franks]], Frank W. Bailey, Russell Guest. Grub Street, 1993. ISBN 0-948817-73-9, ISBN 978-0-948817-73-1.
* ''Fokker D VII Aces of World War 1: Part 2''. Norman Franks, et al. Osprey Publishing, 2004.

{{wwi-air}}

{{DEFAULTSORT:Auffarth, Harald}}
[[Category:1946 deaths]]
[[Category:German World War I flying aces]]
[[Category:Knights of the House Order of Hohenzollern]]
[[Category:Recipients of the Hanseatic Cross (Bremen)]]
[[Category:1896 births]]
[[Category:Luftwaffe personnel of World War II]]
[[Category:Luftstreitkräfte personnel]]