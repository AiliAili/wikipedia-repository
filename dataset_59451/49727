{{Infobox book | 
| name          = Looking Backward: 2000&ndash;1887
| title_orig    = 
| translator    = 
| image         = Looking Backward.jpg
| image_size    = 200px
| caption = <small>Cover of the Ticknor & Co. first edition of ''Looking Backward, 2000-1887.''</small>
| author        = [[Edward Bellamy]]
| illustrator   = 
| cover_artist  = 
| country       = United States
| language      = English
| series        = 
| genre         = [[Science fiction]]<br>[[Utopian novel]]
| publisher     = • Ticknor & Co.<br>(Jan. 1888)<br>• Houghton Mifflin <br>(Sept. 1889)
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = vii, 470
| isbn          = <!-- NA -->
| preceded_by   = 
| followed_by   = [[Equality (book)|Equality]] ''(1897)
}}

'''''Looking Backward: 2000&ndash;1887''''' is a [[utopia]]n [[science fiction]] novel by [[Edward Bellamy]], a journalist and writer from [[Chicopee Falls, Massachusetts]]; it was first published in [[1888 in literature|1888]]. 

It was the third-largest bestseller of its time, after ''[[Uncle Tom's Cabin]]'' and ''[[Ben-Hur (novel)|Ben-Hur: A Tale of the Christ]]''.<ref name = "qlfjpt">Edward Bellamy, ''Looking Backward 2000-1887'', with a Foreword by [[Erich Fromm]], Signet, 1960. ISBN 0-451-52412-8</ref> It influenced a large number of intellectuals, and appears by title in many of the major [[Marxism|Marxist]] writings of the day. "It is one of the few books ever published that created almost immediately on its appearance a political mass movement".<ref>See Fromm's Foreword to ''Looking Backward'', p. vi.</ref>

In the United States alone, over 162 [[Nationalist Clubs|"Bellamy Clubs"]] sprang up to discuss and propagate the book's ideas.<ref>{{cite book|last=Bellamy|first=Edward|title=Looking backward: 2000-1887|year=2000|publisher=Signet|page=Introduction}} Walter James Miller confirms "more than 162 Bellamy Clubs".</ref> Owing to its commitment to the [[nationalization]] of private property and the desire to avoid use of the term ''socialism'', this political movement came to be known as Nationalism — not to be confused with the political concept of [[nationalism]].<ref>Edward Bellamy. "What 'Nationalism' Means". ''The Eclectic Magazine of Foreign Literature (1844&ndash;1898)''; Vol. 52, No. 3 (September 1890); p. 289.</ref> The novel also inspired several [[Commune (intentional community)|utopian communities]].

==Publication history==

The decades of the 1870s and the 1880s were marked by economic and social turmoil, including the [[Long Depression]] of 1873-1879, a series of [[recession]]s during the 1880s, the rise of [[trade union|organized labor]] and [[strike action|strikes]], and the 1886 [[Haymarket affair]] and its controversial aftermath.<ref name=Bowman87>Sylvia E. Bowman, ''The Year 2000: A Critical Biography of Edward Bellamy.'' New York: Bookman Associates, 1958; pp. 87-89.</ref> Moreover, American capitalism's tendency towards concentration into ever larger and less competitive forms — [[monopoly|monopolies]], [[oligopoly|oligopolies]], and [[trust (business)|trusts]] — began to make itself evident, while emigration from Europe expanded the labor pool and caused wages to stagnate.<ref name=Bowman87 /> The time was ripe for new ideas about economic development which might ameliorate the current social disorder.

[[Edward Bellamy]] (1850-1898), a relatively unknown [[New England]]-born novelist with a history of concern with social issues,<ref>Bowman, ''The Year 2000,'' pg. 96.</ref> began to conceive of writing an impactful work of visionary fiction shaping the outlines of a [[utopian socialism|utopian]] future, in which production and society were ordered for the smooth production and distribution of commodities to a regimented labor force. In this he was not alone — between 1860 and 1887 no fewer than 11 such works of fiction were produced in the United States by various authors dealing fundamentally with the questions of economic and social organization.<ref>Bowman, ''The Year 2000,'' pg. 107.</ref>

Bellamy's book, gradually planned throughout the 1880s, was completed in 1887 and taken to [[Boston]] publisher Benjamin Ticknor, who published a first edition of the novel in January 1888.<ref name=Bowman115>Bowman, ''The Year 2000,'' pg. 115.</ref> Initial sales of the book were modest and uninspiring, but the book did find a readership in the Boston area, including enthusiastic reviews by future Bellamyites [[Cyrus Field Willard]] of the ''[[Boston Globe]]'' and [[Sylvester Baxter]] of the ''Boston Herald.''

Shortly after publication, Ticknor's publishing enterprise, Ticknor and Company, was purchased by the larger Boston publisher, [[Houghton, Mifflin & Co.]], and new publishing plates were created for the book.<ref name=Bowman115 /> Certain "slight emendations" were made to the text by Bellamy for this second edition, released by Houghton Mifflin in September 1889.<ref>Bowman, ''The Year 2000,'' pp. 115-116.</ref>

In its second release, Bellamy's futuristic novel met with enormous popular success, with more than 400,000 copies sold in the United States alone by the time Bellamy's follow-up novel, ''Equality,'' was published in 1897.<ref name=Bowman121>Bowman, ''The Year 2000,'' pg. 121.</ref> Sales would top 532,000 in the USA by the middle of 1939.<ref name=Bowman121 /> The book gained an extensive readership in [[United Kingdom|Great Britain]] as well, with more than 235,000 copies sold there between the time of its first release in 1890 and 1935.<ref name=Bowman121 />
<!--Andolfatto has all of the Chinese characters for the Chinese names-->
The first version of the novel published in China, heavily edited for the tastes of Chinese readers, was titled ''Huitou kan jilüe'' (回頭看記略). This text was later retitled ''Bainian Yi Jiao'' (百年一覺 ), or "A Sleep of 100 Years" and in 1891-1892 this version was serialized in ''[[Wanguo gongbao]]'';<ref name=Andolfatto>Andolfatto, Lorenzo. "[http://transtexts.revues.org/619 Productive distortions: On the translated imaginaries and misplaced identities of the late Qing utopian novel]" ([http://transtexts.revues.org/pdf/619 PDF version]). ''Transtext(e)s Transcultures'' (跨文本跨文化), 10, 2015. DOI: [http://dx.doi.org/10.4000/transtexts.619 10.4000/transtexts.619].</ref> the organization Guangxuehui (廣學會; Society for Promoting Education) published these pieces in a book format. This first translation, the first piece of science fiction from a Western country published in [[Qing Dynasty]] China, was done in an abridged format by [[Timothy Richard]].<ref name=DDWangTranslatingp310>[[David Der-wei Wang|Wang, David D. W.]] "Translating Modernity." In: Pollard, David E. (editor). ''Translation and Creation: Readings of Western Literature in Early Modern China, 1840-1918''. [[John Benjamins Publishing]], 1998. ISBN 9027216282, 9789027216281. Start: p. [https://books.google.co.in/books?id=Ni88Ddi_S2cC&pg=PA303 303]. CITED: p. [https://books.google.co.in/books?id=Ni88Ddi_S2cC&pg=PA310 310].</ref> The novel was again serialized in China in 1898, in ''Zhongguo guanyin baihua bao'' (中國官音白話報);<ref name=Andolfatto/> and in 1904, under the title ''Huitou kan'' (Looking Backward), within ''Xiuxiang xiaoshuo'' (繡像小說; Illustrated Fiction).<ref name=DDWangTranslatingp310/>

The book remains in print in multiple editions, with one publisher alone having reissued the title in a printing of 100,000 copies in 1945.<ref>Joseph Schiffman, "Introduction" to Edward Bellamy: ''Selected Writings on Religion and Society.'' New York: Liberal Arts Press, 1955; pg. xxxviii.</ref>

==Synopsis==

Bellamy's novel tells the story of a hero figure named Julian West, a young American who, towards the end of the 19th century, falls into a deep, hypnosis-induced sleep and wakes up one hundred and thirteen years later. He finds himself in the same location ([[Boston, Massachusetts]]), but in a totally changed world: It is the year 2000 and, while he was sleeping, the United States has been transformed into a [[socialist]] utopia. The remainder of the book outlines Bellamy's thoughts about improving the future. The major themes include problems associated with capitalism, a proposed socialist solution of a nationalization of all industry, the use of an "industrial army" to organize production and distribution, as well as how to ensure free cultural production under such conditions.

The young man readily finds a guide, Doctor Leete, who shows him around and explains all the advances of this new age; including drastically reduced working hours for people performing menial jobs and almost instantaneous, Internet-like delivery of goods. Everyone retires with full benefits at age 45, and may eat in any of the public kitchens. The productive capacity of the United States is nationally owned, and the goods of society are equally distributed to its citizens. A considerable portion of the book is dialogue between Leete and West wherein West expresses his confusion about how the future society works and Leete explains the answers using various methods, such as metaphors or direct comparisons with 19th-century society.

Although Bellamy's novel did not discuss technology or the economy in detail, commentators frequently compare ''Looking Backward'' with actual economic and technological developments. For example, Julian West is taken to a store which (with its descriptions of cutting out the middleman to cut down on waste in a similar way to the [[consumers' cooperative]]s of his own day based on the ''[[Rochdale Principles]]'' of 1844) somewhat resembles a modern [[warehouse club]] like BJ's, Costco, or Sam's Club. He additionally introduces a concept of "credit" cards in chapters 9, 10, 11, 13, 25, and 26, but these actually function like modern [[debit card]]s. All citizens receive an equal amount of "credit." Those with more difficult, specialized, dangerous or unpleasant jobs work fewer hours (in contrast to the real-world practice of paying them more for their efforts of, presumably, the same hours). Bellamy also predicts both sermons and music being available in the home through [[cable radio|cable "telephone"]] (already demonstrated but commercialized only in 1890 as [[Théâtrophone]] in France). Bellamy labeled the philosophy behind the vision "nationalism", and his work inspired the formation of more than 160 Nationalist Clubs to propagate his ideas.

Despite the "ethical" character of his socialism (though he was initially reluctant to use the term "socialism"), Bellamy's ideas somewhat reflect classical [[Marxism]]. In Chapter 19, for example, he has the new legal system explained. Most civil suits have ended in socialism, while crime has become a medical issue. The idea of [[atavism]], then current, is employed to explain crimes not related to inequality (which Bellamy thinks will vanish with socialism). Remaining criminals are medically treated. One professional judge presides, appointing two colleagues to state the prosecution and defense cases. If all do not agree on the verdict, then it must be tried over. Chapter 15 and 16 have an explanation of how free, independent public art and news outlets could be provided in a more [[libertarian socialist]] system. In one case Bellamy even writes "the nation is the sole employer and capitalist".

== Precursors ==

Though Bellamy tended to stress the independence of his work, ''Looking Backward'' shares relationships and resemblances with several earlier works&nbsp;— most notably the anonymous ''[[The Great Romance]]'' (1881), [[John Macnie]]'s ''[[The Diothas]]'' (1883),<ref>Arthur E. Morgan, ''Edward Bellamy'', New York, Cloumbia University Press, 1944.</ref> [[Laurence Gronlund]]'s ''The Co-operative Commonwealth'' (1884), and [[August Bebel]]'s ''Woman in the Past, Present, and Future'' (1886).<ref>Arthur E. Morgan, ''Plagiarism in Utopia: A Study of the Continuity of the Utopian Tradition With Special Reference to Edward Bellamy's "Looking Backward",'' Yellow Springs, Ohio, privately printed, 1944.</ref> For example, in ''The True Author of Looking Backward'' (1890) J.B. Shipley argued that Bellamy's novel was a repeat of Bebel's arguments, whilst literary critic R. L. Shurter went so far as to argue that "''Looking Backward'' is actually a fictionalized version of ''The Co-operative Commonwealth'' and little more".<ref>Robert L. Shurter, ''The Utopian Novel in America, 1865–1900'', New York, AMS Press, 1975; p. 177.</ref> However, Bellamy's book also bears resemblances to the early socialist theorists or 'utopian socialists' [[Étienne Cabet|Etienne Cabet]], [[Charles Fourier]], [[Robert Owen]] and [[Henri Saint-Simon]], as well as to the 'Associationism' of [[Albert Brisbane]] whom Bellamy had met in the 1870s.<ref>Carl J. Guarneri, ''The Utopian Alternative, Fourierism in Nineteenth-Century America'', (Cornell University Press, Ithaca, 1991), p.368, p.401</ref>

== Reaction and sequels ==
{{main|List of sequels to Looking Backward}}

In 1897 Bellamy wrote a sequel, ''[[Equality (book)|Equality]]'', dealing with women's rights, education and many other issues. Bellamy wrote the sequel to elaborate and clarify many of the ideas merely touched upon in ''Looking Backward''.

The success of ''Looking Backward'' provoked a spate of sequels, parodies, satires, dystopian, and 'anti-utopian' responses.<ref>Jean Pfaelzer,''The Utopian Novel in America, 1886–1896: The Politics of Form'', Pittsburgh, University of Pittsburgh Press; pp. 78&ndash;94 and 170&ndash;3.</ref> A partial list of these follows.<ref>This list was derived from G. Claeys ''Late Victorian Utopias: A Prospective'', (Pickering and Chatto, London, 2008), J. Pfaelzer, ''The Utopian Novel in America 1886&ndash;1896: The Politics of Form'', (University of Pittsburgh Press, Pittsburgh, 1984), K. Roemer, ''The Obsolete Necessity: America in Utopian Writings, 1888&ndash;1900'', (Kent State University Press, Kent, 1976), K. Roemer, ''Utopian Audiences, How Readers Locate Nowhere'', (University of Massachusetts Press, Amherst, 2003), C.J. Rooney, ''Dreams and Visions: a study of American utopias, 1865&ndash;1917'' (1997), F. Shor, ''Utopianism and radicalism in a reforming America, 1888&ndash;1918'', (Greenwood Press, Westport Connecticut, 1997), and especially L.T. Sargent ''British and American Utopian Literature, 1516&ndash;1985: An Annotated, Chronological Bibliography'' (Garland Publishing, New York, 1988).</ref>

Directly 'anti-Bellamy' responses:

* Bachelder, J. ''A.D. 2050. Electrical Development at Atlantis'' (1890)
* Harris, G. ''Inequality and Progress'' (1897) [which assumes Bellamy advocated an absolute equality of goods]
* Michaelis, R.C. ''Looking Further Forward: An Answer to "Looking Backward" by Edward Bellamy'' (1890)
* Morris, William, ''[[News from Nowhere]]'' (1890)
* Roberts, J.W. ''Looking Within: The Misleading Tendencies of "Looking Backward" Made Manifest'' (1893)
* Sanders, G.A. ''Reality: or Law and order vs. Anarchy and Socialism, A Reply to Edward Bellamy’s'' Looking Backward ''and'' Equality (1898)
* Satterlee, W.W. ''Looking Backward and What I Saw'' (1890)
* Vinton, A.D. ''Looking Further Backward'' (1890)
* West, J. [pseud.] ''My Afterdream'' (1900)
* Wilbrant, C. ''Mr. East's Experiences in Mr. Bellamy's World'' (1891)

Direct and positive utopian responses / unofficial sequels:

* Berwick, E. 'Farming in the Year 2000, A.D.', ''Overland Monthly'' (1890) 
* Bellamy, C.J. ''An Experiment in Marriage. A Romance'' (1889) [Bellamy's brother]
* Chavannes, A. ''In Brighter Climes, or Life in Socioland'' (1895) by A. Chavannes
* Chavannes, A. ''The Future Commonwealth'' (1892)
* Claflin, S.F. ''Nationalism. Or a System of Organic Unity'' (189x)
* 'Crusoe, R.' ''Looking Upwards; or Nothing New'' (1892)
* Emmens, S.H. ''The Sixteenth Amendment'' (1896)
* Flower, B.O. ''Equality and Brotherhood'' (1897) [A positive response to Bellamy’s ''Equality''; see also 'The Latest Social Vision', ''Arena'' v.18, pp.&nbsp;517–34]
* Flower, B.O. ''The New Time'' (1894)
* Fuller, A.M. ''A.D. 2000'' (1890)
* Geissler, L.A. ''Looking Beyond'' (1891)
* Giles, F.S. ''The Industrial Army'' (1896)
* Gillette, K.C., ''[[The Human Drift]]'' (1894)
* Griffin, C.S. ''Nationalism'' (1889)
* Gronlund, L. ''Our Destiny. The Influence of Nationalism on Morals and Religion'' (1890) [first syndicated ''[[The Nationalist (United States)|The Nationalist]]'', (March–September 1890)]
* Hayes, F.W. ''The Great Revolution of 1905: Or, The story of the Phalanx'' (1893)
* Hertzka, T. ''Freeland, a Social Anticipation'' (1890)
* Howard, E. ''To-Morrow: A Peaceful Path to Real Reform'' (1898)
* McCowan, A. ''Philip Meyer’s Scheme'' (1892)
* Moffat, W. White, G., and White J., ''What’s the World Coming To?'' (1893)
* Porter, L.B. ''Speaking of Ellen'' (1890) [not a utopia]
* Salisbury, H.B. 'The Birth of Freedom', ''[[The Nationalist (United States)|The Nationalist]]'' (November 1890, Mar-Apr 1891)
* Schindler, S. 'Dr. Leete's Letter to Julian West', ''[[The Nationalist (United States)|The Nationalist]]'' (September 1890)
* Schindler, S. ''Young West: A Sequel to Edward Bellamy's Celebrated Novel "Looking Backward"'' (1894)
* Stone, C.H. ''One of Berrian's Novels'' (1890)
* Worley, F.U. ''Three Thousand Dollars a Year'' (1890) [a gradualist utopia]
* Hillman, H.W. ''Looking Forward'' (1906)

The result was a "battle of the books" that lasted through the rest of the 19th century and into the 20th. The back-and-forth nature of the debate is illustrated by the subtitle of Geissler's 1891 ''Looking Beyond'', which is "A Sequel to 'Looking Backward' by Edward Bellamy and an Answer to 'Looking Forward' by Richard Michaelis".

The book was translated into [[Bulgarian language|Bulgarian]] in 1892. In 1900 Bellamy personally approved a request by Bulgarian author [[Iliya Yovchev]] to make an "adapted translation" based on the realities of [[Bulgaria]]n social order. The resulting work, titled ''The Present as Seen by Our Descendants And a Glimpse at the Progress of the Future'' ("Настоящето, разгледано от потомството ни и надничане в напредъка на бъдещето"), generally followed the same plot. The events in Yovchev's version take place in a [[environmentally friendly]] [[Sofia]] and describe the country's unique path of adapting to the new social order. It is considered by local critics to be the first Bulgarian utopian work.<ref>{{cite web|url=http://chitanka.info/text/20938-nachaloto|title=The Beginning|author=Ivaylo Runchev|publisher=Narodna Mladezh|year=1985|accessdate=1 July 2013|quote=Фактически налице е произведение, отличаващо се от оригинала дотолкова, че следва да се говори за нов роман, първия ни български утопичен роман. ("Basically this work differs from the original to such an extent, that we can consider it a new novel, the first Bulgarian Utopian novel.)}}</ref>

[[William Morris]]'s [[1890 in literature|1890]] utopia ''[[News from Nowhere]]'' was partly written in reaction to Bellamy's utopia, which Morris did not find congenial.

Beyond the purely literary sphere, Bellamy's descriptions of utopian [[urban planning]] had a practical influence on [[Ebenezer Howard]]'s founding of the [[garden city movement]] in England, and on the design of the [[Bradbury Building]] in [[Los Angeles]].
[[Image:Edward Bellamy 2000 1887.jpg|thumb|German Reclam edition 1919]]
During the [[Great railroad strike of 1877|Great Strikes of 1877]], [[Eugene V. Debs]] opposed the strikes and argued that there was no essential necessity for the conflict between capital and labor. Debs was influenced by Bellamy's book to turn to a more [[socialist]] direction. He soon helped to form the [[American Railway Union]]. With supporters from the [[Knights of Labor]] and from the immediate vicinity of Chicago, workers at the Pullman Palace Car Company went on strike in June 1894. This came to be known as the [[Pullman Strike]].

The book had a specific and intense reception in Wilhelminian Germany including various parodies and sequels, from [[Eduard Loewenthal]], [[Ernst Müller]] and [[Philipp Wasserburg]] till [[Konrad Wilbrandt]] and [[Richard Michaelis]].<ref>(Edward Bellamy, ''Ein Rückblick aus dem Jahre 2000 auf das Jahr 1887'', Translation Georg von Gizycki, editor Wolfgang Biesterfeld. Philipp Reclam jun., Stuttgart 1983 ISBN 3-15-002660-1 (Universal-Bibliothek 2660 [4]), Afterword of Biesterfeld, p.301f.)</ref>

== Legacy and later responses ==
''Looking Backward'' influenced the novel ''[[Future of a New China]]'' by [[Liang Qichao]].<ref name=DDWangTranslatingp309>[[David Der-wei Wang|Wang, David D. W.]] "Translating Modernity." In: Pollard, David E. (editor). ''Translation and Creation: Readings of Western Literature in Early Modern China, 1840-1918''. [[John Benjamins Publishing]], 1998. ISBN 9027216282, 9789027216281. Start: p. [https://books.google.co.in/books?id=Ni88Ddi_S2cC&pg=PA303 303]. CITED: p. [https://books.google.co.in/books?id=Ni88Ddi_S2cC&pg=PA309 309].</ref>

''Looking Backward'' was rewritten in 1974 by American science fiction writer [[Mack Reynolds]] as ''Looking Backward from the Year 2000''. [[Matthew Kapell]], a historian and anthropologist, examined this re-writing in his essay, "Mack Reynolds' Avoidance of his own [[18 Brumaire|Eighteenth Brumaire]]: A Note of Caution for Would-Be Utopians".

In 1984, Herbert Knapp and Mary Knapp's ''[[Red, White and Blue Paradise|Red, White and Blue Paradise: The American Canal Zone in Panama]]'' appeared. The book was in part a memoir of their careers teaching at fabled [[Balboa High School (Panama)|Balboa High School]], but also a re-interpretation of the [[Panama Canal Zone|Canal Zone]] as a creature of turn-of-the-century Progressivism, a workers' paradise. The Knapps used Bellamy's ''Looking Backward'' as their heuristic model for understanding Progressive ideology as it shaped the Canal Zone.

A one-act play, ''Bellamy's Musical Telephone,'' was written by Roger Lee Hall and premiered at Emerson College in Boston in 1988 on the centennial year of the novel's publication.  It was released as a DVD titled, "The Musical Telephone."

== See also ==
* [[Equality Colony]]

== Footnotes ==
{{Reflist|2}}

== Further reading ==

* Edward Bellamy, [https://archive.org/details/lookingbackward200bell ''Looking Backward, 2000-1887.''] Boston: Ticknor and Co., 1888. <small>—First edition.</small>
* Edward Bellamy, [https://archive.org/details/lookingbackward01bellgoog ''Looking Backward, 2000-1887.''] Boston: Houghton, Mifflin & Co., 1889. <small>—Second edition.</small>
* Edward Bellamy, [https://archive.org/details/HowICameToWriteLookingBackward-may1889 "How I Came to Write ''Looking Backward,"''] ''The Nationalist'' (Boston), vol. 1, no. 1 (May 1889), pp.&nbsp;1–4.

== External links ==
{{wikisource|Looking Backward From 2000 to 1887}}
* {{FadedPage|id=20080503|name=Looking Backward 2000-1887}}
* [http://www.gutenberg.org/ebooks/624 Full text on gutenberg.org]
* Anna Simon (reader), [https://archive.org/details/lookingbackward_0909_librivox ''Looking Backward, 2000-1887'' audiobook], LibriVox.
* "The Musical Telephone" [http://www.americanmusicpreservation.com/TheMusicalTelephone.htm - a play based on a chapter in Edward Bellamy's ''Looking Backward, 2000-1887'']

{{Authority control}}

[[Category:1888 novels]]
[[Category:Novels set in Boston]]
[[Category:Utopian novels]]
[[Category:American novels adapted into plays]]
[[Category:Bellamyism]]
[[Category:19th-century American novels]]
[[Category:1880s science fiction novels]]
[[Category:American science fiction novels]]
[[Category:Boston in fiction]]
[[Category:Social science fiction]]
[[Category:2000 in fiction]]
[[Category:Novels set in the 2000s]]
[[Category:Novels set in the future]]