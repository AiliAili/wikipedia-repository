{{Infobox military conflict
|conflict    = Battles of the Kinarot Valley
|partof      = the [[1948 Arab–Israeli War]]
|image       = [[File:Deganiatank1.jpg|300px]]
|caption     = Preserved [[Renault R35]] tank destroyed by Israel at Degania. The PIAT's hit can be seen at the top of the turret.
|date        = May 15–21, 1948
|place       = [[Israel]], [[Syria]] ([[Golan Heights]])
|result      = Tactical stalemate, perceptual decisive Israeli victory
|combatant1  = {{flagcountry|Israel}} ([[Haganah]])
|combatant2  = [[File:Syria-flag 1932-58 1961-63.svg|22px]] [[Syrian Republic (1930–1958)|Syria]]
|commander1  = {{flagicon|Israel}} [[Moshe Dayan]]
|commander2  = [[File:Syria-flag 1932-58 1961-63.svg|22px]] [[Anwar Bannud]]<br />[[File:Syria-flag 1932-58 1961-63.svg|22px]] Abdullah Wahab el-Hakim<br />[[File:Syria-flag 1932-58 1961-63.svg|22px]] [[Husni al-Za'im]]
|strength1   = ~70 (Degania Alef)<ref name="wallach14" /><ref name="idfwebsite" /><br />
~80 (Degania Bet)<ref name="morris254" />
|strength2   = Infantry brigade,<br />Tank battalion,<br />Two armored vehicle battalions,<br />Artillery battalion<ref name="gelber141" />
|casualties1 = 8 (Degania Alef)<ref name="yitzhaki97" /><br />54 (Tzemah)<ref name="morris254" />
|casualties2 = 45 (Israeli estimate)
|}}

The '''Battles of the Kinarot Valley''' ({{lang-he-n|הַמַּעֲרָכָה בְּבִקְעַת כִּנָּרוֹת}}, ''HaMa'arakha BeBik'at Kinarot''), is a collective name for a series of military engagements between the [[Haganah]] and the [[Military of Syria|Syrian army]] during the [[1948 Arab–Israeli War]], fought between May 15–22, 1948 in the [[Kinarot Valley]]. It includes two main sites: the Battle of [[Degania Alef|Degania]]–[[Samakh, Tiberias|Samakh]] (Tzemah), and battles near [[Masada (kibbutz)|Masada]]–[[Sha'ar HaGolan]]. The engagements were part of the battles of the [[Jordan Valley (Middle East)|Jordan Valley]], which also saw fighting against [[Jordan|Transjordan]] in the area of [[Gesher, Israel|Gesher]].

The battles began shortly after the [[Declaration of Independence (Israel)|Israeli declaration of independence]], when Syria shelled [[Ein Gev]] on the night of May 15–16. They were the first military engagement between Israel and Syria. On May 18, Syria attacked the Israeli forward position in Samakh (Tzemah), and on May 20 attacked [[Degania Alef]] and occupied Masada and Sha'ar HaGolan. The attack on Degania Alef was a failure, after which the Syrian forces attempted to capture [[Degania Bet]]. After reaching a stalemate, they retreated to their initial position in [[Tel al-Qasr]], where they remained until the end of the war.

The campaign was perceived as a decisive Israeli victory, causing reorganizations in the Syrian high command and the birth of heroic tales in Israel. However, Syria made a small territorial gain and certain actions were criticized within Israel, such as the retreat from Masada and Sha'ar HaGolan.

==Background==
The first stage of the 1948 War, referred to as the [[1947–1948 Civil War in Mandatory Palestine]], started following the ratification of [[United Nations Partition Plan for Palestine|UN Resolution 181]] on November 29, 1947, which granted [[Israel]] the mandate to declare independence.<ref name="morris04-12" /> This was [[Declaration of Independence (Israel)|declared]] on May 14, 1948 and the next night, the armies of a number of Arab states invaded Israel and attacked Israeli positions.<ref name="gelber138" /><ref name="globalsecurity" />

The Arab states surrounding the [[British Mandate of Palestine|Mandate of Palestine]] started to prepare themselves a few weeks before May 15. According to the Arab plan, the Syrian army was to attack the new state from southern [[Lebanon]] and capture [[Safed]].<ref name="gelber134" /> As such, the Syrians massed their forces in that area; however, after they found out that Lebanon did not wish to actively participate in combat, their plans changed to an attack from the southern [[Golan Heights]] on [[Samakh, Tiberias|Samakh]] (Tzemah) and later [[Tiberias]].<ref name="gelber136" /> The Syrian force assembled in [[Qatana]] on May 1. It moved on May 12 to [[Beirut]] and to [[Sidon]] on May 13, after which it headed to [[Bint Jbeil]]. After the sudden plan change, the force moved to [[Nabatieh]], and proceeded around the [[Finger of the Galilee]] to [[Banias]] and [[Quneitra]], from which the eventual attack was staged.<ref name="gelber141" />

The [[Syrian Army]] was meant to consist of two brigade-sized units, but there was no time to prepare them, thus only the 1st Brigade was in a state of readiness by May 15. It had about 2,000 soldiers in two infantry battalions, one armored battalion, and 4–6 artillery batteries.<ref name="morris251" />

==Prelude==
[[File:Kinarot valley topo.png|thumb|right|Map of the battle site on the evening of 14 May 1948]]
According to plan, the Syrians attacked from the southern Golan Heights, just south of the [[Sea of Galilee]] through [[al-Hama]] and the [[Yarmouk River]], hitting a densely populated Jewish area of settlement. This came as a surprise to the [[Haganah]],<ref name="gelber136" /> which expected an attack from south Lebanon and [[Mishmar HaYarden, Palestine|Mishmar HaYarden]].<ref name="gelber135" /> The Jewish villages on the original confrontation line were [[Ein Gev]], [[Masada, Emek HaYarden|Masada]], [[Sha'ar HaGolan]] and both Deganias.

On Friday, May 14, the [[Syrian 1st Infantry Brigade]], commanded by Colonel Abdullah Wahab el-Hakim, was in Southern Lebanon, positioned to attack [[Malkia]]. That day Hakim was ordered to return to Syria, move south across the Golan and enter Palestine south of the Sea of Galilee through Samakh (Tzemah). He began to advance at 9:00 AM on Saturday and had only two of his battalions, where the soldiers were already exhausted.<ref name="dupuy47" />
[[File:Deganya Israel.jpg|left|thumb|The Deganias and Tzemah in 2003.]]

At the onset of the invasion, the Syrian force consisted of a reinforced infantry brigade, supplemented by at least one armored battalion (including [[Renault R35]] tanks) and a [[field artillery]] battalion.<ref name="wallach14" /><ref name="gelber141" /> The troops moved to [[Kafr Harib]] and were spotted by Haganah reconnaissance, but because the attack was not expected, the Israeli troops did not attack the invaders.<ref name="gelber136" /> At night between 15 and 16 May, the bulk of the Syrian forces set up camp in [[Tel al-Qasr]] in the southwestern Golan. One company with armored reinforcements split up to the south to proceed to the Jewish water station on the [[Yarmouk River|Yarmouk]] riverbank.<ref name="wallach14" />

The Haganah forces in the area consisted of several units from the Barak (2nd) Battalion of the [[Golani Brigade]], as well as the indigenous villagers, including a reduced [[Guard Corps (Haganah)|Guard Corps]] (HIM) company at the Samakh (Tzemah) police station.<ref name="wallach14" /> This force was headed by the battalion commander's deputy, who was killed in action in the battle.<ref name="idfhistory169" /> On May 13, the battalion commander declared a state of emergency in the area from May 15 until further notice. He authorized his men to seize all necessary arms from the settlements and urged them to dig in and build fortifications as fast as possible, and to mobilize all the necessary work force to do so.<ref name="lorch170-171" />

==Battles==
[[File:Kinarot valley firstbattle.png|thumb|right|Breakdown of the movements and battles up to May 16]]
On Saturday night, May 15, the observation posts reported many vehicles with full lights moving along the Golan ridge east of the Sea of Galilee.<ref name="lorch171" /> The opening shots were fired by Syrian artillery on [[kibbutz]] Ein Gev at approximately 01:00 on May 16. At dawn, Syrian aircraft attacked the Kinarot valley villages. The following day, a Syrian company which split from the main force attacked the water station with heavy weaponry, where every worker was killed except one.<ref name="etzioni165" />

An Israeli reserve unit was called in from [[Tiberias]]. It arrived after twenty minutes and took positions around the town. At that point, Samakh (Tzemah) was defended by three platoons from the Barak battalion and reinforcements from neighboring villages.<ref name="lorch171" /> They entrenched in the actual village,<ref name="wallach14" /> which had been abandoned by the residents in April 1948, with British escort.<ref name="gelber101" /> Positions in the village included the [[Tegart fort|police station]] in the west, the cemetery in the north, the Manshiya neighborhood in the south, and the railway station.<ref name="etzioni165" /> The Syrians set up their positions in an abandoned British military base just east of the village and in an animal quarantine station to the southeast.<ref name="wallach14" />

Two Israeli sappers were sent to mine the area of the quarantine station, but did not know that it was already under Syrian control. Their vehicle was blown up, but they managed to escape alive.<ref name="etzioni165" /> On the same day, the Syrian company that attacked the water station from Tel ad-Dweir proceeded towards Sha'ar HaGolan and Masada. Its advance was halted by the village residents as well as a platoon of reinforcements armed with [[20 mm caliber|20 mm]] cannons. The company retreated to its position and commenced artillery fire on the two kibbutzim.<ref name="wallach14" />

[[File:Fighter at Shaar Hagolan.jpg|thumb|left|widthpx|A member of Sha'ar HaGolan in a stand overlooking the border]]

This development gave the Israeli forces time to organize their defenses at Samakh (Tzemah). During the course of May 16, Israeli [[gunboat]]s harassed the Syrian positions on the southeastern [[Sea of Galilee]] shore, trenches were dug, and roadblocks were set up. Meanwhile, Syrian aircraft made bombing runs on Masada, Sha'ar HaGolan, [[Degania Bet]] and [[Afikim]].<ref name="wallach14" /> The attack on Samakh (Tzemah) resumed before dawn on May 17—the Syrians attacked the village's northern positions, but their armor stayed behind. The infantry thus could not advance into the concentrated Israeli fire from the village itself, despite severe ammunition shortages on the Israeli side.<ref name="etzioni165" />

Meanwhile, the defenders of Tiberias believed their town would be targeted next, and built barricades and fortifications. Ben-Gurion told the cabinet that "The situation is very grave. There aren't enough rifles. There are no heavy weapons".<ref name="morris253-254" /> Aharon Israeli, a platoon leader, commented that there was also a severe lack of experienced field commanders—he himself was hastily promoted on May 15, despite not having sufficient knowledge or experience.<ref name="etzioni171" /> Also on May 16, the [[Syrian President]], [[Shukri al-Quwatli]], visited the front with his Prime Minister, [[Jamil Mardam]], and his Defense Minister, [[Taha al-Hashimi]]. He told his forces "to destroy the Zionists".<ref name="morris253-254" />

At night, A Syrian force attempted to surround the Israelis by crossing the [[Jordan River]] to the north of the Sea of Galilee, but encountered a minefield in which a senior Syrian officer was wounded.<ref name="wallach14" /> This was spotted and reported by the Israelis at [[Tabgha]],<ref name="idfhistory" /> and the additional reprieve allowed the Kinarot Valley villages to evacuate the children, elderly and sick, as well as conduct maneuvers which feigned massive reinforcements in the [[Poria Illit|Poria]]-[[Alumot]] region.<ref name="wallach14" /> In the panic of surprise, many men also tried to flee the frontal villages, but blockposts were set up near [[Afula]] and [[Yavne'el]] by the [[Military Police Corps (Israel)|Military Police Service]]'s northern command, under Yosef Pressman, who personally stopped buses and allowed only the women and children to proceed to safety.<ref name="harel15" />

===Samakh (Tzemah)===
[[File:Kinarot valley tzemah.png|thumb|right|Breakdown of the battles on May 18–19 (Battle of Tzemah)]]
At about 04:30 on May 18,<ref name="etzioni166" /> the [[Syrian 1st Brigade]], now commanded by Brigadier General [[Husni al-Za'im]] and consisting of about 30 vehicles, including tanks, advanced west towards Samakh (Tzemah) in two columns—one across the coast, and another flanking from the south. A contingent was allocated further south, in order to secure the safety of the main force by flanking Sha'ar HaGolan and Masada from the west. It entered a stalemate with a new Israeli position northwest of the two villages.<ref name="wallach14" /><ref name="dupuy48" />

The coastal column shelled the Israeli positions and inflicted enormous damage; the Israelis were either dug in within shallow trenches made for infantry warfare with no head cover, or in Samakh's clay houses that were vulnerable to heavy weapons. The Israelis were eventually forced to abandon their posts and concentrate in the police station, where they brought the wounded. The deputy commander of the Golani Brigade, Tzvika Levkov, also arrived at the station, and called reinforcements from Sha'ar HaGolan and Tiberias, which did not manage to arrive on time.<ref name="etzioni166" />

A soldier who participated in the battle reported that only 20 uninjured troops were left to defend the police station as the second Syrian column reached Samakh (Tzemah). The only heavy weapon the defenders possessed was ineffective against Syrian armor.<ref name="etzioni166" /> Fearing their forces would be completely cut off, an order was given by the Haganah to retreat and leave the wounded, Tzvika Levkov among them. The retreat was disorganized and heavy Israeli casualties were recorded as Samakh's police station fell.<ref name="wallach14" /><ref name="dupuy48" /> Reinforcements from the Deganias, commanded by Moshe Cohen, arrived but were immediately hit by the Syrians and did not significantly affect the battle.<ref name="wallach14" /> Aharon Israeli, a platoon commander in these reinforcements, wrote that it was clear as soon as they arrived that the battle was over. Cohen would not hear of a retreat initially, but when the force saw Levkov fall into a trench, they hastily withdrew.<ref name="etzioni172" />
[[File:Palmachnik at Tzemach.jpg|thumb|left|upright|A Palmach fighter at Samakh]]

On the same day, Syrian aircraft bombed the Israeli village [[Kvutzat Kinneret|Kinneret]] and the regional school [[Beit Yerah]], on the southwestern shore of the Sea of Galilee. By evening, Samakh (Tzemah) had fallen and a new Israeli defensive line was set up in the Deganias, facing the Syrian counterparts.<ref name="wallach14" /> At night, a [[Palmach]] company from Yiftach's 3rd Battalion attempted to recapture Samakh's police station. They stealthily reached the school next to the station, but the assault on the actual fort was warded off.<ref name="etzioni168" /> On the morning of May 19, a message was sent from Sha'ar HaGolan and Masada that they were preparing for an evacuation, although when the order was given to stay put, the villages had already been abandoned, mostly to Afikim.<ref name="wallach14" /> In the morning, when the villagers carried out an order to return to their positions, local Arabs were already present at the location.<ref name="etzioni168" /> The Syrian troops then captured the villages without a fight,<ref name="dupuy48" /> and proceeded to loot and destroy them.<ref name="haaretz-massada"/> Aharon Israeli wrote that an order was given not to disclose the flight of Masada and Sha'ar HaGolan's residents, but this became clear as fire and smoke rose from the villages, and hurt the morale of the Israelis making defensive preparations in the Deganias.<ref name="etzioni176" />

The counterattack on the police station failed but delayed the Syrian attack on the Deganias by twenty-four hours. In the evening of May 19, a delegation from the Deganias arrived in Tel Aviv to ask for reinforcements and heavy weapons. One of its members later wrote that [[David Ben-Gurion]] told them he could not spare them anything, as "The whole country is a front line". He also wrote that [[Yigael Yadin]], the Chief Operations Officer of the Haganah, told him that there was no alternative to letting the Arabs approach to within twenty to thirty meters of the gates of Degania and fight their tanks in close combat.<ref name="lorch174" /> Yadin prepared reinforcements, and gave an order: "No point should be abandoned. [You] must fight at each site". He and Ben-Gurion argued over where to send the Yishuv's only battery of four pre-[[World War I]] 65&nbsp;mm mountain guns (nicknamed "[[Napoleonchik]]im"), which had no proper sights. Ben-Gurion wanted to send them to Jerusalem, but Yadin insisted that they be sent to the Kinarot valley, and Ben-Gurion eventually agreed.<ref name="morris255" />

On the night of May 18–19, a platoon departed from Ein Gev by sea to [[Al-Samra|Samra]] and raided the Syrian contingent in [[Tel al-Qasr]]. The raid failed, but may have delayed the Syrian attack on Degania, thus giving its defenders twenty-four hours to prepare. A second raid, by a Yiftach company, crossed the Jordan and struck the Syrian camp at the Customs House, near the main [[Bnot Yaakov Bridge]]. After a short battle, the Syrian defenders (one or two companies) fled. The Palmachniks destroyed the camp and several vehicles, including two armored cars, without losses.<ref name="morris254-255">Morris (2008), pp. 254–255</ref>

===Degania Alef===
[[File:Kinarot valley degania.png|thumb|right|Breakdown of the battles on May 20–22 (Battle of Degania)]]
After the fall of Tzemah, the Haganah command realized the importance of the campaign in the region, and made a clear separation between the Kinarot Valley, and the [[Gesher, Israel#1948 Israeli-Arab war|Battle of Gesher]] fought against Transjordan<ref name="Tal2004p202">{{cite book|author=David Tal|title=War in Palestine, 1948: Israeli and Arab Strategy and Diplomacy |url=https://books.google.com/books?id=C_F8YXt3EKQC&pg=PA205|date=31 January 2004|publisher=Routledge|isbn=978-0-203-49954-2|pages=202}}</ref> and Iraq to the south.<ref name="morris254" /> On May 18, [[Moshe Dayan]], who had been born in Degania, was given command of all forces in the area, after having been charged with creating a commando battalion in the [[8th Armored Brigade (Israel)|8th Brigade]] just a day before.<ref name="teveth137" /><ref name="teveth139" /> A company of reinforcements from the [[Gadna (Israel)|Gadna]] program was allocated, along with 3 [[PIAT]]s. Other reinforcements came in the form of a company from the [[Yiftach Brigade]] and another company of paramilitaries from villages in the [[Lower Galilee]] and the [[Jezreel Valley]]. The Palmach counterattack on the police station on the night of May 18 gave the Israeli forces an additional day to prepare defense and attack plans.<ref name="wallach14" />

The Israelis called the reinforcements assuming this was the main Syrian thrust. The Syrians were not intending to carry out any further operation south of the Sea of Galilee and planned to make their main effort further north, near the Bnot Ya'akov bridge. On May 19, the Iraqis were about to drive west through [[Nablus]] toward [[Tulkarm]], and asked the Syrians to make a diversion in the Degania area to protect their right flank. The Syrians complied, their main objective being to seize the bridge across the river north of Degania Alef, thus blocking any Israeli attack from [[Tiberias]] against the Iraqi line of communications.<ref name="dupuy48" />

Heavy Syrian shelling of Degania Alef started at about 04:00 on May 20 from the Samakh police station, by means of 75&nbsp;mm cannons, and 60 and 81&nbsp;mm mortars.<ref name="idfhistory" /> The barrage lasted about half an hour.<ref name="morris255" /> At 04:30 on May 20, the Syrian army began its advance on the Deganias and the bridge over the Jordan River north of Degania Alef.<ref name="yitzhaki97" /> Unlike the attack on Samakh (Tzemah), this action saw the participation of nearly all of the Syrian forces stationed at Tel al-Qasr, including infantry, armor and artillery.<ref name="wallach14" /> The Israeli defenders numbered about 70 persons (67 according to Aharon Israeli's head count<ref name="etzioni176" />), most of them not regular fighters, with some Haganah and Palmach members. Their orders were to fight to the death. They had support from three 20&nbsp;mm guns at [[Beit Yerah]], deployed along the road from Samakh to Degania Alef. They also had a [[Davidka]] mortar, which exploded during the battle, and a PIAT with fifteen projectiles.<ref name="morris255" />

At night, a Syrian expeditionary force attempted to infiltrate Degania Bet, but was caught and warded off, which caused the main Syrian force to attack [[Degania Alef]] first.<ref name="wallach14" /> At 06:00, the Syrians started a frontal armored attack, consisting of 5 tanks, a number of armored vehicles and an infantry company.<ref name="yitzhaki97" /> The Syrians pierced the Israeli defense, but their infantry was at some distance behind the tanks. The Israelis knocked out four Syrian tanks and four armored cars with 20&nbsp;mm cannons, PIATs and [[Molotov cocktail]]s.<ref name="pollack452" /> Meanwhile, other defenders kept small arms fire on the Syrian infantry, who stopped in citrus groves a few hundred meters from the settlements. The surviving Syrian tanks withdrew back to the Golan.<ref name="dupuy48" /> At 07:45, the Syrians halted their assault and dug in, still holding most of the territory between Degania Alef's fence and Samakh's police fort.<ref name="morris255" /> They left behind a number of lightly damaged or otherwise inoperable tanks that the Israelis managed to repair.<ref name="pollack453" />

===Degania Bet===
Despite the Syrian superiority in numbers and equipment, the destruction of a multitude of armored vehicles and the infantry's failure to infiltrate Degania Alef was the likely cause for the retreat of the main Syrian force to Samakh (Tzemah). A less-organized and sparsely numbered armored and infantry force forked off to attack [[Degania Bet]].<ref name="wallach14" /> Eight tanks, supported by mortar fire, moved within 400 yards of the settlement defense, where they stopped to provide fire support for an infantry attack. The Syrians made two failed attempts to breach the Israeli small arms fire defense and gave up the attempt.<ref name="dupuy49" /> Against this force, the Israelis had about 80 people and one [[PIAT]].<ref name="morris256" /> The defenses in Degania Bet were disorganized and there were not enough trenches. They also had no communication link to the command, so Moshe Dayan sent one of his company commanders to assess the situation.<ref name="teveth141" />
[[File:Degania after battle.jpg|thumb|left|widthpx|A house in Degania after the Syrian bombardment]]
While the battle was taking place, the [[Canon de 65 M (montagne) modele 1906|65&nbsp;mm artillery]], four ''Napoleonchik canons'', reached the front in the middle of the day and were placed on the Poria–Alumot [[ridge]].<ref name="wallach14" /> It was the first Israeli artillery to be used in the war.<ref name="dupuy49" /> At 13:20, they began to fire at the Syrians, and about 40 rounds the latter began to retreat.<ref name="teveth142" /> The Israelis also fired into Samakh, where the Syrian officers, who had until then believed that the Israelis had nothing that could hit their headquarters, took shelter. One projectile hit the Syrian ammunition depot in the village, and others ignited fires in the dry fields.<ref name="morris256" /> While the soldiers who operated the cannons (still lacking sights) were not proficient in handling them, an acceptable level of accuracy was achieved after practice shots into the Sea of Galilee. In all, the artillery fire took the Syrian army by complete surprise, and the latter decided to regroup and retreat to [[Tel al-Qasr]], also recalling the company at Sha'ar HaGolan and Masada.<ref name="wallach14" /> A total of 500 shells were fired by the Israeli artillery.<ref name="teveth141" /> Syrian officers may have shot some of their fleeing soldiers.<ref name="morris256" />

There were two other reasons for the Syrian withdrawal. The 3rd Battalion from the Palmach's Yiftach Brigade had been sent by boat during the previous night across the sea to Ein Gev, planning to assault and capture [[Kafr Harib]]. It was, noticed and shelled by the Syrians, but one of the companies managed to climb up the Golan. It carried out a smaller raid at dawn, bombing water carriers and threatening the Syrian 1st Brigade's line of communications.<ref name="dupuy49" /><ref name="kadish138" /> The second reason was that they were running out of ammunition: [[Husni al-Za'im]] had been promised replenishment, and attacked Degania short of ammunition. Za'im ordered a withdrawal when his troops ran out of ammunition. The replenishment was instead sent to the 2nd Brigade further north. The Israelis were not aware of this, and attributed the Syrian withdrawal to surprise at the Israeli artillery fire.<ref name="dupuy49" />

==Aftermath and effects==
[[File:War Memorial in Samakh, Israel.jpg|thumb|right|War memorial in Tzemah]]
On May 21, Haganah troops returned to Samakh (Tzemah) and set up fortifications,<ref name="wallach14" /> The damaged tanks and armored cars were gathered and taken to the rear. The settlers returned that night to identify the bodies of their comrades in the fields and buried them in a common grave in Degania.<ref name="lorch176" /> At dawn on May 21, the Golani staff reported that the enemy was repelled but that they were expecting another attack. The full report read:
{{quote|Our forces repelled yesterday a heavy attack of tanks, armored vehicles and infantry that lasted about 8 hours. The attack was repelled by the brave stand of our men, who used Molotov cocktails and their hands against the tanks. 3" mortars and heavy machinery took their toll on the enemy. Field cannons caused a panicked retreat of the enemy, who yesterday left Tzemah. This morning our forces entered Tzemah and took a large amount of booty of French ammunition and light artillery ammunition. We have captured 2 tanks and an armored vehicle of the enemy. The enemy is amassing large reinforcements. We are expecting a renewal of the attack.<ref name="idfhistory" />}}
[[File:Water tower at Shaar Hagolan.jpg|thumb|left|Sha'ar HaGolan's water tower after the battle]]
On May 22, villagers returned to Masada and Sha'ar HaGolan, which had been largely destroyed.<ref name="wallach14" /> Expecting another attack, reinforcements from the [[Carmeli Brigade]] took up positions in the two villages.<ref name="idfhistory" /> Many of the participants of the battles were sent to Tiberias to rest and recuperate, and the units that lost soldiers were reorganized.<ref name="etzioni170" />

In the wake of the fall of [[Gush Etzion]], news of Degania's successful stand (as well as [[Battles of Kfar Darom|that of Kfar Darom]]) provided a morale boost for other Israeli villages.<ref name="lorch177" /> The battle also influenced British opinion on the balance of power in the war.<ref name="morris257" /> The success of the Napoleonchik field cannons prompted the Israeli high command to re-use two of them in [[Battle of Latrun|attempts to capture Latrun]].<ref name="idfhistory" /> The flight from Masada and Sha'ar HaGolan, on the other hand, stirred controversy in the young state, fueled by news of the [[Kfar Etzion massacre]] just days before, and the Palmach issued a newsletter accusing them of abandoning national assets, among other things. These accusations were subsequently repeated in media and in a play by [[Yigal Mossensohn]], and a campaign was started by the villagers to clear their name.<ref name="haaretz-massada" />

The battles of the Kinarot Valley were the first and last of the major ground engagements between Israel and Syria to the south of the Sea of Galilee, although minor patrol skirmishes continued until the [[1948 Arab–Israeli War#First truce: 11 June – 8 July 1948|first ceasefire]].<ref name="idfhistory" /> The campaign, combined with the [[Battle of Gesher]], was possibly the only coordinated attack between two or more Arab countries in the northern front.<ref name="kadish138" /> At the end, the Syrians held Tel al-Qasr, which was part of the [[British Mandate of Palestine]] and the Jewish state according to the UN partition of 1947. Despite the above, the offensive was considered a decisive Syrian defeat by both sides. The Syrian defense minister [[Ahmad al-Sharabati]] and Chief of Staff [[Abdullah Atfeh]] blamed each other, the latter resigning and the former being dismissed by the prime minister as a result of the battle.<ref name="gelber141" /> As reasons for their defeat, they gave their low level of preparedness and the strength of the Israeli defenses, as well as their lack of coordination with the Iraqis (according to one Syrian historian, the Iraqis were supposed to assist them in the Deganias). After the battle, British observers became convinced that the Arabs were not going to win the war, and compared the battle to the [[Luftwaffe]]'s failure in the [[Battle of Britain]] in 1940, which showed that [[Germany]] was not going to win the air war. The observers said that "A greater edge than the [Syrians] enjoyed at Degania they won't have again".<ref name="morris257" />

===First tank kill controversy===
The first Syrian tank damaged near Degania Alef's gates, which has been preserved on the location, was the subject of a historiographic dispute when Baruch "Burke" Bar-Lev, a retired [[Israel Defense Forces|IDF]] colonel and one of Degania's native defenders at the time, claimed that he was the one who stopped the tank with a [[Molotov cocktail]].<ref name="haaretz-barlev" /> However, his account was rebutted by an IDF [[Ordnance Corps (Israel)|Ordnance Corps]] probe, which in 1991 determined that a [[PIAT]] shot had killed the tank's crew. Shlomo Anschel, a [[Haifa]] resident who also participated in the battle, told [[Haaretz]] in 2007 that the tank was hit by PIAT fire from a Golani soldier, and that the Molotov cocktail could not possibly have hit the crew.<ref name="haaretz-anschel" />

==References==
{{reflist|2|refs=
<ref name="globalsecurity">{{cite web|url=http://www.globalsecurity.org/military/world/war/israel-inde.htm|title=Israeli War of Independence|publisher=GlobalSecurity.org|accessdate=2009-01-28| archiveurl= https://web.archive.org/web/20090118072325/http://www.globalsecurity.org/military/world/war/israel-inde.htm| archivedate= 18 January 2009 <!--DASHBot-->| deadurl= no}}</ref>
<ref name="haaretz-anschel">{{cite web|publisher=[[Haaretz]]|url=http://blog.tapuz.co.il/anschel/images/2444848_50.JPG|title=The Battle of Degania|date=2007-08-16|accessdate=2009-01-28|language=he}}</ref>
<ref name="haaretz-barlev">{{cite web|url=http://www.haaretz.co.il/hasite/pages/ShArt.jhtml?more=1&itemNo=888373&contrassID=2&subContrassID=2&sbSubContrassID=0|title=The Tank Crewman who Spoke of Idi Amin's Heart|author=Dromi, Uri|publisher=[[Haaretz]]|accessdate=2009-01-28|language=he}}</ref>
<ref name="haaretz-massada">{{cite web|url=http://www.haaretz.com/hasen/objects/pages/PrintArticleEn.jhtml?itemNo=703004|title=Zionist Mythology Destroys its Children|publisher=[[Haaretz]]|author=Ashkenazi, Eli|date=6 April 2006|accessdate=2009-01-28}}</ref>
<ref name="idfwebsite">{{cite web|publisher=[[Israel Defense Forces]] |url=http://dover.idf.il/IDF/About/history/40s/1948/052001.htm |accessdate=2009-01-28 |title=The Battle for Degania |language=he |deadurl=yes |archiveurl=https://web.archive.org/web/20080915071704/http://dover.idf.il:80/IDF/About/history/40s/1948/052001.htm |archivedate=2008-09-15 |df= }}</ref>
<ref name="dupuy47">[[#refDupuy|Dupuy (2002)]], p. 47</ref>
<ref name="dupuy48">[[#refDupuy|Dupuy (2002)]], p. 48</ref>
<ref name="dupuy49">[[#refDupuy|Dupuy (2002)]], p. 49</ref>
<ref name="etzioni165">[[#refEtzioni|Etzioni (1951)]], pp. 165–166</ref>
<ref name="etzioni166">[[#refEtzioni|Etzioni (1951)]], pp. 166–168</ref>
<ref name="etzioni168">[[#refEtzioni|Etzioni (1951)]], p. 168</ref>
<ref name="etzioni170">[[#refEtzioni|Etzioni (1951)]], p. 170</ref>
<ref name="etzioni172">[[#refEtzioni|Etzioni (1951)]], pp. 172–176</ref>
<ref name="etzioni176">[[#refEtzioni|Etzioni (1951)]], pp. 176–177</ref>
<ref name="etzioni171">[[#refEtzioni|Etzioni (1951)]], pp. 171–172</ref>
<ref name="gelber101">[[#refGelber|Gelber (2006)]], p. 101</ref>
<ref name="gelber134">[[#refGelber|Gelber (2006)]], p. 134</ref>
<ref name="gelber135">[[#refGelber|Gelber (2006)]], p. 135</ref>
<ref name="gelber136">[[#refGelber|Gelber (2006)]], p. 136</ref>
<ref name="gelber138">[[#refGelber|Gelber (2006)]], pp. 138–142</ref>
<ref name="gelber141">[[#refGelber|Gelber (2006)]], p. 141</ref>
<ref name="harel15">[[#refHarel|Harel (1982)]], p. 15</ref>
<ref name="idfhistory">[[#refIDF|IDF History (1978)]], pp. 166–173</ref>
<ref name="idfhistory169">[[#refIDF|IDF History (1978)]], p. 169</ref>
<ref name="kadish138">Hadari, Danny in ed. [[#refKadish|Kadish, Alon (2005)]], pp. 138–141</ref>
<ref name="lorch170-171">[[#refLorch|Lorch (1968)]], pp. 170–171</ref>
<ref name="lorch171">[[#refLorch|Lorch (1968)]], p. 171</ref>
<ref name="lorch174">[[#refLorch|Lorch (1968)]], p. 174</ref>
<ref name="lorch176">[[#refLorch|Lorch (1968)]], p. 176</ref>
<ref name="lorch177">[[#refLorch|Lorch (1968)]], p. 177</ref>
<ref name="morris04-12">[[#refMorris2004|Morris (2004)]], pp. 12–13</ref>
<ref name="morris251">[[#refMorris2008|Morris (2008)]], p. 251</ref>
<ref name="morris253-254">[[#refMorris2008|Morris (2008)]], pp. 253–254</ref>
<ref name="morris254">[[#refMorris2008|Morris (2008)]], p. 254</ref>
<ref name="morris255">[[#refMorris2008|Morris (2008)]], p. 255</ref>
<ref name="morris256">[[#refMorris2008|Morris (2008)]], p. 256</ref>
<ref name="morris257">[[#refMorris2008|Morris (2008)]], p. 257</ref>
<ref name="pollack452">[[#refPollack|Pollack (2002)]], p. 452</ref>
<ref name="pollack453">[[#refPollack|Pollack (2002)]], p. 453</ref>
<ref name="teveth137">[[#refTeveth|Teveth (1972)]], p. 137</ref>
<ref name="teveth139">[[#refTeveth|Teveth (1972)]], p. 139</ref>
<ref name="teveth141">[[#refTeveth|Teveth (1972)]], p. 141</ref>
<ref name="teveth142">[[#refTeveth|Teveth (1972)]], p. 142</ref>
<ref name="wallach14">[[#refWallach|Wallach et al. (1978)]], pp. 14–15</ref>
<ref name="yitzhaki97">[[#refYitzhaki|Yitzhaki (1988)]], pp. 97–101</ref>
}}

===Bibliography===
* <span class="citation" name="refDupuy">{{cite book
| publisher = Military Book Club
| isbn = 0-9654428-0-2
| last = Dupuy
| first = Trevor N.
| authorlink = Trevor N. Dupuy
| title = Elusive Victory: The Arab–Israeli Wars, 1947–1974
| year = 2002
}}</span>
* <span class="citation" name="refEtzioni">{{cite book|editors=Etzioni, Binyamin|year=1951|title=Tree and Dagger – Battle Path of the Golani Brigade|publisher=Ma'arakhot Publishing|language=he}}</span>
**<span class="citation" name="refBroshi">Broshi, Yitzhak: ''[http://ynhockey.net/materials/etzioni-binyamin-pp162-177.pdf Battles of the Kinarot Valley]'', pp.&nbsp;162–170 {{he icon}}</span>
**<span class="citation" name="refIsraeli">Israeli, Aharon: ''[http://ynhockey.net/materials/etzioni-binyamin-pp162-177.pdf In the Campaign]'', pp.&nbsp;171–177 {{he icon}}</span>
* <span class="citation" name="refGelber">{{cite book
| edition = 2nd
| publisher = Sussex Academic Press
| isbn = 1-84519-075-0
<!--| pages = 436 pages-->
| last = Gelber
| first = Yoav
| authorlink = Yoav Gelber
| title = Palestine 1948: War, Escape And The Emergence Of The Palestinian Refugee Problem
|date=April 2006
}}</span>
* <span class="citation" name="refHarel">{{cite encyclopedia|title=Military Police Corps (Vol. 16)|encyclopedia=IDF in Its Corps: Army and Security Encyclopedia|last=Harel|first=Zvi|editor=Yehuda Schiff|publisher=Revivim Publishing|year=1982|language=he}}</span>
* <span class="citation" name="refIDF">{{cite book|title=History of the War of Independence|author=Israel Defense Forces History General Staff Historiography Branch|edition=20th|id=S/N 501-202-72|publisher=Ma'arakhot Publishing|origyear=First published in 1959|location=Israel|year=1978|language=he}}</span>
* <span class="citation" name="refKadish">{{cite book|title=Israel's War of Independence 1948–49: Revisited|year=2005|editor=Kadish, Alon|edition=2nd|origyear=First published in 2004|publisher=[[Ministry of Defense (Israel)|Ministry of Defense]]|isbn=965-05-1251-9|language=he}}</span>
* <span class="citation" name="refLorch">{{cite book
| publisher = Massada
| last = Lorch
| first = Netanel
| title = The Edge of the Sword: Israel's War of Independence, 1947–1949
| location = Jerusalem
| year = 1968
}} </span>
* <span class="citation" name="refMorris2004">{{cite book
| publisher=[[Cambridge University Press]]
| last=Morris
| first=Benny
| authorlink=Benny Morris
| title=The Birth of the Palestinian Refugee Problem Revisited
| year=2004
| isbn=0-521-00967-7
| url=https://books.google.com/books?id=uM_kFX6edX8C&pg=PR3&source=gbs_selected_pages&cad=0_1
| accessdate=2009-01-28
}}</span>
* <span class="citation" name="refMorris2008">{{Cite book
| publisher = Yale University Press
| isbn = 0-300-15112-8
| last = Morris
| first = Benny
| title = 1948: A History of the First Arab-Israeli War
| date = 2008
}}</span>
* <span class="citation" name="refPollack">{{cite book
| publisher = Bison Books
| isbn = 0-8032-8783-6
<!-- | pages = 717 pages -->
| last = Pollack
| first = Kenneth M.
| authorlink = Kenneth Pollack
| title = Arabs at War: Military Effectiveness, 1948–1991
| date = 2004-09-01
| url=https://books.google.com/books?id=sSHYdGR_xvoC&pg=PA16&dq=nirim&ei=nJ5DS_-ZHKrSyQT0o-jJBw&cd=3#v=onepage&q=nirim&f=false
| accessdate=2010-01-05
}}</span>
* <span class="citation" name="refTeveth">{{cite book|author=Teveth, Shabtai|year=1972|title=Moshe Dayan. The Soldier, the Man, the Legend|publisher=Widenfeld & Nicolson|isbn=0-297-99522-7}}</span>
* <span class="citation" name="refWallach">{{cite encyclopedia|encyclopedia=Carta's Atlas of Israel|year=1978|publisher=[[Carta (Publisher)|Carta]]|location=[[Jerusalem]], [[Israel]]|editor=Evyatar Nur|author=Wallach, Jeuda|author2=Lorch, Netanel|author3= Yitzhaki, Aryeh|title=Battles of the Jordan Valley|volume=Volume 2 - The First Years 1948–1961|language=he}}</span>
* <span class="citation" name="refYitzhaki">{{cite book
|title=A Guide to War Monuments and Sites in Israel (English Title), Volume 1 - North
|publisher=Barr Publishers
|author=Yitzhaki, Aryeh
|authorlink=Aryeh Yitzhaki
|year=1988
|location=[[Tel Aviv]], Israel
<!--|pages=152 pages-->
|language=he}}</span>

{{coord|32|41|47.24|N|35|35|28.46|E|display=title}}

{{good article}}

[[Category:Battles and operations of the 1948 Arab–Israeli War|Kinarot Valley]]
[[Category:May 1948 events]]