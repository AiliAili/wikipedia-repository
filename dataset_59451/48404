{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}

'''Ann Hawkshaw''' (14 October 1812 – 29 April 1885) was an English poet. She published four volumes of poetry between 1842 and 1871.

==Early life==
Ann Hawkshaw (née Jackson) was born on 14 October 1812, third child of the Reverend James Jackson, dissenting Protestant minister of the Green Hammerton Independent Chapel in the West Riding of Yorkshire, and his wife Mary (née Clarke). There were fourteen children in total, with only seven surviving into early adulthood. The Clarke family had worked the land in Green Hammerton for over three hundred years and Ann lived here until she was fourteen when she left to board at the Moravian School in Little Gomersal, about forty miles from the family home.

==Family and connections==
During the 1820s Ann met [[John Hawkshaw]]. They were married on 20 March 1835 in Whixley, moving to Salford shortly after. Whilst in Manchester the Hawkshaws mixed socially with the Unitarian community, including [[John Relly Beard]], William and [[Elizabeth Gaskell]] and their close friends the Dukinfield Darbishires, and [[Catherine Winkworth]]. John's election to the [[Manchester Literary and Philosophical Society]] in 1839 brought the Hawkshaws into contact with many of Manchester's prominent thinkers including [[Richard Cobden]] and [[John Dalton]].

Ann and John had six children: Mary Jane Jackson (1838), Ada (1840), [[John Clarke Hawkshaw|John Clarke]] (1841), Henry Paul (1843), Editha (1845), and Oliver (1846). Ada died of hydrocephalus in 1845. Oliver died in 1856 having contracted typhoid fever whilst the family were holidaying in Pitlochry, Scotland.
In 1850 John Hawkshaw set up as a consulting engineer in Great George Street, Westminster, and the family moved to London. From the early 1850s the Hawkshaws employed a governess, Mary Pugh, who was later employed by [[Charles Darwin]] at [[Down House]].

On 24 June 1862 the eldest of Hawkshaws' children, Mary, married [[Godfrey Wedgwood]], with her brother John Clarke marrying Godfrey's sister Cecily in 1865. Mary and Godfrey's first child, [[Cecil Wedgwood]], was born on 28 March 1863 and fifteen days later Mary died from puerperal mania. Three of the Hawkshaws' six children had now died.

In 1865 the Hawkshaws purchased the four thousand acre Hollycombe estate near Liphook, Hampshire and spent their time between here and London. Visitors to their country home included [[Charles Darwin|Charles]] and [[Emma Darwin]], [[Joseph Dalton Hooker]], [[Henry James]], [[Anne Thackeray]] and [[Alfred Tennyson]]. The Hawkshaws built a school at nearby Wardley Green in memory of their three dead children and commissioned a stained-glass window depicting a mother and three children with the motif '‘Fides, Spes et Caritas'’ (Faith, Hope and Charity). The window has since been replaced with a commemorative stone 'To the Memory of Ada, Oliver and Mary'.

==Writing career==
Hawkshaw's first volume of poetry  '''Dionysius the Areopagite' with other poems'' was published in London and Manchester in November 1842. The collection of twenty-two poems includes the long narrative title poem which retells the biblical story of [[Dionysius the Areopagite]], an elected member of the Areopagus in Athens who is briefly mentioned in the New Testament (Acts 17:34). Hawkshaw's poem offers an imaginative reconstruction of Dionysius's personal journey towards Christianity and his decision to choose Christian faith over romantic love. The collection was favourably received by Manchester's poetic community, most notably by [[Samuel Bamford]] who mentions Hawkshaw's work in the preface to his '' Poems''  in 1843.<ref>Samuel Bamford, preface to ''Poems'' (Manchester: published by the author, 1843).</ref> In January 1844 John Hawkshaw forwarded a copy of the volume to [[Thomas Carlyle]] who in turn forwarded the book to his mother for her perusal.<ref>From ''The Carlyle Letters Online'', TC TO MARGARET A. CARLYLE ; 24 January 1844; {{doi|10.1215/lt-18440124-TC-MAC-01}}; CL 17: 248–249.</ref> Two of the collections short lyric poems 'Why am I a Slave?' and 'The Mother to her Starving Child' are included in ''Nineteenth-Century Women Poets: An Oxford Anthology'' (1996).<ref>''Nineteenth-Century Women Poets: an Oxford Anthology'', ed. by Isobel Armstrong, Joseph Bristow, and Cath Sharrock (Oxford: Clarendon Press, 1996), pp.346–8.</ref>

''Poems for My Children'' was published in London and Manchester in July 1847. Six of the collection’s twenty-seven poems are addressed to the Hawkshaw children – including Ada, who had died in 1845. Several of the poems in the collection celebrate nature, whilst others are set firmly in Manchester’s urban landscape. The series of five poems in the collection retelling aspects of British history anticipate Hawkshaw’s ambitious retelling of Anglo-Saxon history which was published seven years later.

''Sonnets on Anglo-Saxon History'' was published in London by [[John Chapman (publisher)|John Chapman]] in November 1854. The sequence of one hundred sonnets retells the history of Britain up to the [[Norman Conquest]] with each sonnet faced on the page by a short prose extract. The extracts include quotations from the work of prominent contemporary Anglo-Saxon historians, such as [[Sharon Turner]] (''The History of the Anglo-Saxons from the Earliest Period to the Norman Conquest'' (1799–1805)), Francis Palgrave (''History of the Anglo-Saxons'' (1831)) and J.M. Kemble (''The Saxons in England'' (1849)), and from translations of [[The Anglo-Saxon Chronicle]] and [[Bede]]'s ''Ecclesiastical History''. In her sonnet response Hawkshaw interacts with the historians and presents alternative perspectives on aspects of Anglo-Saxon history, challenging the traditions of historiography by noting its limitations and filling in the gaps.
 
Hawkshaw's final collection, ''Cecil's Own Book'', was published for private circulation in 1871. The collection of three short stories and ten poems was written to amuse her young grandson Cecil Wedgwood, the surviving son of Ann's daughter Mary who had died shortly after childbirth in 1863. The collection is dedicated to her memory. Of particular note is the collection's final poem, 'In Memoriam', a touching elegy on childhood death which traces the loss of the Hawkshaws' three children.

Several twentieth-century anthologies of children's poetry connect Hawkshaw with the pseudonym 'Aunt Effie', author of ''Aunt Effie's Rhymes for Little Children'' (1852) and ''Aunt Effie's Gift to the Nursery'' (1854). These collections were written by Jane Euphemia Saxby (née Browne), who also wrote a volume of sacred poetry, ''The Dove on the Cross'' (1849).

==Death==
Ann Hawkshaw died on 29 April 1885 at her London home at the age of seventy-two. The cause of death was a stroke. The ''Manchester Guardian'' published an obituary on 1 May 1885.<ref>''Manchester Guardian'', 1 May 1885, p.8.</ref> The now Lady Hawkshaw was buried at St Mary-the-Virgin Church, Bramshott, a few miles from the family estate of Hollycombe. Sir John Hawkshaw commissioned a stained glass window set in the nave of Bramshott church to commemorate the life of his wife.

==Bibliography==
*''Dionysius the Areopagite'' with other poems (London: Jackson and Walford; Manchester: Simms and Dinham, 1842)
*''Poems for My Children'' (London: Simpkin and Marshall; Manchester: Simms and Dinham, 1847)
*''Sonnets on Anglo-Saxon History'' (London: John Chapman, 1854)
*''Cecil’s Own Book'' (printed for private circulation, 1871)

==Reviews==
*''North of England Magazine: a monthly journal of literature, politics, science, and art'', vol.2, issue 11. December 1842, pp.&nbsp;121–122 (review of  '''Dionysius the Areopagite' with other poems'')
*''The Gentleman’s Magazine'', vol.174. January–June 1843, p.621 (review of ''‘Dionysius the Areopagite' with other poems'')
*''Court Magazine and Monthly Critic'', June 1843, pp.60–61 (review of ''‘Dionysius the Areopagite' with other poems'')
* Bamford, Samuel, "Preface" to ''Poems'', Manchester:  published by the author, 1843 (discusses ‘Dionysius the Areopagite')
*''The Athenaeum'', 15 January 1848, p.57 (review of ''Poems for My Children'')
*''Manchester Guardian'', 8 November 1854, p.10 (review of ''Sonnets on Anglo-Saxon History'')
*''The Living Age'', vol.44, issue 554, 6 January 1855, p.142 (review of ''Sonnets on Anglo-Saxon History'')
*''The Athenaeum'', 20 January 1855, pp.76–77(review of ''Sonnets on Anglo-Saxon History'')
*''The Monthly Christian Spectator'', vol.5, January–December 1855, p.55 (review of ''Sonnets on Anglo-Saxon History'')
*''The Eclectic Review'', series 5, vol.10, July–December 1855, p.376 (review of ''Sonnets on Anglo-Saxon History'')

==References==
{{Reflist}}

==Further reading==
*Bark, Debbie, ‘Ann Hawkshaw’, ''British Writers Supplement XVIII'', ed. Jay Parini (Charles Scribner’s Sons, 2012), 127–143.
*Ann Hawkshaw, ''The Collected Works of Ann Hawkshaw'', ed. Debbie Bark (London: Anthem Press, 2014).

{{authority control}}

{{DEFAULTSORT:Hawkshaw, Ann}}
[[Category:1812 births]]
[[Category:1885 deaths]]
[[Category:Writers from Manchester]]
[[Category:Darwin–Wedgwood family]]
[[Category:Victorian poets]]
[[Category:Victorian women writers]]
[[Category:English women poets]]
[[Category:19th-century English poets]]
[[Category:19th-century English writers]]
[[Category:19th-century women writers]]