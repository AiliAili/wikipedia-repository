{{Multiple issues|
{{primary sources|date=December 2006}}
{{plot|date=October 2008}}
{{cleanup|date=October 2008}}
}}

{{Infobox character
|series=[[24 (TV series)|24]]
|image=[[Image:Mike Novick.jpg|200px]]
|caption= [[Jude Ciccolella]] as Mike Novick
|name=Mike Novick
|portrayer=[[Jude Ciccolella]]
|lbl1=Days
|first=[[24 (season 1)|Day 1 – Episode 6]]
|last=[[24 (season 5)|Day 5 – Episode 24]]
|data1=[[24 (season 1)|1]], [[24 (season 2)|2]], [[24 (season 4)|4]], [[24 (season 5)|5]]
}}

'''Mike Novick''' is a [[fictional character]] played by [[Jude Ciccolella]] as part of the television series, ''[[24 (TV series)|24]]''. He has appeared more than any other recurring character, having appeared in 58 episodes over 4 seasons, and has been given a promotional photo with the same background as the starring cast members, but has never been credited as part of the main cast.

==Appearances==

===24: Season 1===

Novick had been a close advisor and staff member of [[David Palmer (24 character)|David Palmer]] since Palmer's first Senate run.  Whereas Palmer tended to be bold and idealistic, Novick was more cautious and [[wikt:pragmatic|pragmatic]], sometimes even a bit sneaky, though Palmer nonetheless valued his counsel.  When Palmer sought nomination as a presidential candidate, Novick stayed alongside the Senator as his chief of staff.

The day of the California presidential primary would prove to be an especially daunting one for the Palmer campaign for a number of reasons.  First was the troubling revelation that Palmer's son Keith had killed the man who raped Palmer's daughter Nicole.  Even more troubling was the revelation that Palmer's wife [[Sherry Palmer|Sherry]] had concealed this from David, supposedly to protect him.

When Palmer began investigating he found that certain men with a vested interest in seeing Palmer get elected were determined not to let this scandal come out at any cost.  Things took a startling turn when Keith's therapist, Doctor Ferragamo, with whom Keith had confided about what he had done, was killed in a fire shortly after speaking with Senator Palmer.  Novick, however, insisted that they had no proof that these individuals were responsible.

Keith, however, would later acquire damning evidence against these men when he secretly recorded a conversation between himself and Carl Webb, a man working for them.  Sherry insisted that nothing good could come from revealing the contents of the tape to the public and urged Palmer to destroy it.  Novick, on the other hand, suggested Palmer should keep it as leverage against these men once he was in the White House.  In the end, Palmer rejected both options and revealed the entire story to the press.  Amazingly, polls taken shortly after the announcement revealed that the public was not disillusioned by the scandal but impressed by Palmer's honesty.

The second threat to the Palmer campaign that day came in the form of a threat on Palmer's life. Novick told Palmer that a man named Jack Bauer was in Secret Service custody for attempting to assassinate Palmer. Palmer knew the name Jack Bauer but couldn't remember from where. Several hours later, Novick discovered the connection between Bauer and Palmer. They were both involved in a covert operation called "Operation Nightfall" which resulted in the death of all of Bauer's men.

Though Palmer would go on to win the nomination, his wife's manipulations during the day caused his faith in her to deteriorate to the point where he announced he was leaving her.  Evidently, this did not disrupt the campaign significantly either.  After winning the nomination, Palmer would go on to win the election.  Novick stayed on as Palmer's [[White House Chief of Staff]].

===24: Season 2===
Over a year after the primary, Palmer's administration was faced with an immense threat to national security: word came that a nuclear bomb was going to be detonated by terrorists in [[Los Angeles]] within a day.  Upon hearing the news, Novick, Palmer's [[White House Chief of Staff|Chief of Staff]] immediately got on a plane to reach the President.  He was surprised to find Palmer's ex-wife Sherry there when he arrived, as she had apparently convinced the President to let her help in gathering information.  Given her deception in the past, Novick was unsure if she could be trusted and told Palmer so.

When evidence surfaced that [[Roger Stanton (24 character)|Roger Stanton]], the head of [[NSA]], may have been hindering the search for the bomb, Novick urged Palmer to relieve Stanton of duty and have him interrogated.  Palmer did so and was surprised when Stanton implicated Sherry Palmer as well.

Eventually the bomb was located and detonated a safe distance from the city.  In addition, a recording was found implicating three Middle Eastern countries in collaborating with the terrorists.  Accordingly, retaliatory strikes were immediately planned against the three countries.  Palmer, however, suspended the attack when [[Jack Bauer]] contacted him with tentative evidence that the recordings were false.

Despite his long-time loyalty to Palmer, Novick strongly opposed the President's decision to suspend the attacks based on only the faintest of evidence that the recordings were false.  So strong was his opposition that Novick conspired with [[Vice President of the United States|Vice-President]] [[Jim Prescott]] to invoke the [[Twenty-fifth Amendment to the United States Constitution|25th Amendment]] to remove Palmer from office and appoint Prescott as [[Acting President of the United States|Acting President]].  Novick was forced to have Palmer's chief aide [[Lynne Kresge]] detained when she learned of this.  He was horrified when Kresge was severely injured attempting to escape but did not waver.

After presenting Palmer's apparently questionable actions to the Cabinet, they voted, by a narrow margin, to invoke the 25th Amendment.  Once Prescott was sworn in as President, he immediately ordered preparation for the attack to resume. Palmer was furious at his long-time friend's betrayal.  Novick, for his part, seemed to become more disillusioned as the hours passed.

After learning that the entire situation may have been orchestrated by a businessman named [[Peter Kingsley (24 character)|Peter Kingsley]], Novick finally took action.  He ordered Ryan Chappelle of CTU to assist Jack Bauer, who had gone rogue in attempting to find evidence the recording was false.  With the assistance of CTU and Sherry Palmer, Bauer was able to obtain a recording in which Kingsley implicated himself.  Prescott immediately aborted the attack and restored the presidency to Palmer.

Prescott and the cabinet members who voted against Palmer offered their resignations, though Palmer did not accept them.  Palmer believed that they realized their mistakes.  Novick's betrayal was far more personal to Palmer, however.  Though he had come through in the eleventh hour, Palmer could not forget Novick's prior actions and relieved him of his post. An emotional Novick accepted the President's decision.

Although it was revealed in an interview with the producers that Kresge survived her injuries, there is no indication that Novick was punished for detaining her. Novick was later succeeded as Chief of Staff by [[Wayne Palmer]], the President's younger brother&nbsp;– and did not appear in Day 3.

===24: Season 4===
At some point, Novick switched parties and joined the staff of [[Charles Logan (24 character)|Charles Logan]] who served as Vice-President under Palmer's successor, President [[List of minor characters in 24#24: Season 4|John Keeler]]. As with David Palmer, Novick had known Logan for many years prior. When Keeler was critically injured in a terrorist attack on [[Air Force One]], Novick once again had the cabinet invoke the 25th Amendment to install Logan.  Once again the President and Novick faced a grave crisis when a terrorist leader named [[Habib Marwan]] stole several activation codes for the nation's nuclear arsenal from the wreckage of Air Force One, eventually assembling and launching a nuclear missile.

Unfortunately, Logan proved a nervous and ineffective leader. When an order from Logan jeopardized the hunt for Marwan, Logan began to question his own ability to govern during the crisis.  Novick suggested bringing in former President Palmer as an advisor. Novick, remembering his deception of Palmer during Day 2, offered to leave if his presence made things awkward for him. Though Palmer reiterated his viewpoint that it was a fact Novick had betrayed him, he decided it best to put the past behind them. The two shook hands and got to work. 

Palmer worked closely with Novick and became directly involved in executive decision making while Logan became increasingly isolated. Logan eventually became wary of Palmer's actions, particularly the authorization of government agents infiltrating the [[China|Chinese]] consulate in Los Angeles, which led to the accidental death of the Chinese [[consul]]. Despite several setbacks and close calls, CTU and military officials were able to find and destroy the missile before it hit its target.

A second crisis emerged almost immediately thereafter, when Chinese officials linked Jack Bauer to the infiltration of the consulate and demanded his [[extradition]]. With no other options, President Logan appeared to concede their demands. Soon after, Novick overheard [[Walt Cummings]], Logan's chief of security, instructing the secret service agent sent to pick Bauer up to kill him instead, apparently concerned at the idea of a man with Bauer's information falling into the hands of the Chinese. Logan had previously rejected Cummings' proposal to dispose of Bauer though Novick was certain that was what he wanted all along.

Novick warned Palmer, who in turn warned Jack. Jack then staged an escape attempt and, with the help of some of his CTU friends, faked his own death and went into hiding. Logan was satisfied to have this last loose end apparently tied up. Only Palmer and a handful of other people inside CTU would know the truth.

===24: Season 5===

Mike was still on the staff of President Logan eighteen months later at the Presidential retreat in Los Angeles during the signing of an anti-terrorism defense pact with the Russian President Yuri Suvarov. Along with [[First Lady of the United States|First Lady]] [[Martha Logan]], he was one of the few people in Logan's inner circle close to David Palmer and, when informed of Palmer's assassination, was moved to tears.

When Jack Bauer, who had emerged from hiding to find out who had ordered Palmer's assassination, discovered Walt Cummings (now Logan's Chief of Staff) was involved, he contacted Mike to arrange a secret meeting to warn him of Cummings' involvement. Cummings, however, monitored their phone call and had both men detained before Jack could tell Mike anything.

After Jack managed to convince [[United States Secret Service|Secret Service]] agent [[Aaron Pierce (24 character)|Aaron Pierce]] to let him speak with the President, he was able to reveal the truth to Logan. In response, Logan ordered Cummings arrested and Mike released.

Mike and President Logan, along with Martha, soon discussed how the situation with Cummings would be publicly addressed. Mike suggested a more secretive approach, while Martha suggested Logan admit responsibility but promise that Cummings would be brought to justice. Logan ultimately chose the latter option, though it became moot when Cummings was later found dead, apparently having committed [[suicide]] by hanging himself.

It is at this point where Mike becomes Chief of Staff to President Logan, regaining a position he had not held in over 4 years.

Mike was present when the President was faced with an agonizing dilemma.  Eastern European terrorists led by [[Vladimir Bierko]], who were in the possession of a deadly [[nerve gas]], contacted President Logan and demanded to know the route that President Suvarov and his wife were taking as their motorcade left Los Angeles.  Bierko threatened to use the nerve gas to kill thousands of American civilians if Logan did not cooperate.  The President, unwilling to allow any further American deaths, ultimately choose to give in to his demands.

Mike was sympathetic to the President's situation, particularly when Martha Logan, aware of the situation, joined the Suvarovs in their motorcade. In a moment of self-pity, Logan asked Mike to pray with him.  Mike was initially uncomfortable with this, possibly because he considered it a personal matter, or perhaps because of differing religious beliefs, but nonetheless indulged the President.

Ultimately the President's decision was made for him when CTU intercepted communications that suggested a possible attack on the motorcade.  Secret Service was warned just as Bierko's men launched the attack.  Though motorcade personnel suffered heavy casualties, they were able to repel the attack and prevent the deaths of the First Lady and the Suvarovs. Consequently, however, Logan was contacted by Bierko, warning him that attacks on American targets would soon commence.

Following Bierko's threat, Mike was surprised by the sudden arrival of [[Hal Gardner]], the Vice-President, at the retreat.  Gardner proposed a stronger military presence in the threatened area, to the extent of effectively declaring [[martial law]]. Mike was concerned by the suggestion of such drastic measures, particularly without consulting Congress, though Logan appeared at least somewhat swayed by Gardner's suggestion.

Hoping to convince the President otherwise, Mike sought out assistance from Martha (following Logan's failure to recall the motorcade, the two had once again become estranged). Mike attempted to explain the President's situation in the hopes that the two would reconcile. He appeared to be at least partially successful as she later conceded her husband's responsibilities extended far beyond her own safety.

Mike was puzzled when, hours later, he learned that the President had ordered the arrest of Jack Bauer, claiming new evidence implicated him in the assassination of David Palmer. Mike was further confused when Logan stated he had transferred responsibility of apprehending Bauer from CTU to the military. When Mike contacted the military commander in the area, he claimed to have received no such order. At this point, Mike was unaware that Logan himself was complicit in Palmer's assassination, as well as the entire day's horrific events, and that Logan was attempting to prevent Bauer from coming forward with evidence implicating him.  Mike's suspicions about the President's behavior were further reinforced when [[Karen Hayes]], interim Special Agent in Charge of CTU-Los Angeles, voiced her concerns about Logan's task delegations as well.

Mike had the unenviable task of informing Logan of Bierko's escape from CTU custody. He and Logan monitored the situation until its conclusion. Mike was congratulatory toward the president, but still appeared to suspect that the truth was being kept from him. Shortly thereafter, Martha Logan and Aaron Pierce enlisted Mike's aid in bringing the president to justice. The three of them orchestrated Logan's abduction at the hands of Jack Bauer.

After Bauer's failure to obtain a confession, Mike appeared grudgingly informed Martha that Jack wasn't able to force her husband to admit his guilt. He then appeared alongside the president during his eulogy over the body of David Palmer. During the speech, Mike was clearly disgusted with Logan's insincerely positive comments regarding Palmer. (Unbeknownst to Logan, Jack had placed a listening device in his pen). While Logan was arrested, he glared at Mike and Martha, who smirked triumphantly. This would be the last time Mike would be seen in the series.

==References==
{{reflist}}
{{24 (TV series)}}

{{DEFAULTSORT:Novick, Mike}}
[[Category:24 (TV series) characters]]
[[Category:Fictional lawyers]]
[[Category:Fictional advisors]]
[[Category:Fictional Democrats (United States)]]
[[Category:Fictional White House Chiefs of Staff]]
[[Category:Fictional characters introduced in 2001]]