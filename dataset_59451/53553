{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox person
| name        = Sir Philip Hampton
| image       = <!-- just the filename, without the File: or Image: prefix or enclosing [[brackets]] -->
| alt         = 
| caption     = 
| birth_name  = 
| birth_date  = {{Birth date and age|df=yes|1953|10|05}}
| birth_place = United Kingdom
| death_date  = 
| alma_mater  = [[Lincoln College, Oxford]]
| death_place = 
| nationality = 
| other_names = 
| occupation  = Chairman, [[GlaxoSmithKline]]
| known_for   = 
}}

'''Philip Roy Hampton''' (born 5 October 1953) is a British businessman, the chairman of [[GlaxoSmithKline]]. Originally he joined RBS as the deputy chairman on 19 January 2009 and was promoted to chairman on 3 February the same year.  He has previously held the chairman role at [[J Sainsbury]] plc, parent company of the Sainsbury's chain of supermarkets, and the role of group finance director of a number of blue chip companies including 
[[Lloyds Banking Group]] plc, [[BT Group]] plc, [[BG Group]] plc, [[British Gas plc]] and [[British Steel plc]].<ref>[http://www.rbs.com/about-rbs/g1/our-board.ashx RBS Board]</ref> before becoming the first chairman of UK Financial Investments Limited in 2008, the post he left to join RBS. 

==Biography==
Hampton gained an MA in English from [[Lincoln College, Oxford]], in 1975.<ref name="HM Treasury"/> He joined the accounting firm [[Coopers & Lybrand]] the same year, qualified as a [[chartered accountant]] in 1978, and then trained as an [[auditor]], working in London and west Africa.<ref name="HM Treasury">{{cite web|url=http://www.hm-treasury.gov.uk/hampton_biography.htm|title=Philip Hampton|publisher=[[HM Treasury]]|accessdate=19 January 2009}}</ref> He took an [[MBA]] from [[INSEAD]] from 1980 to 1981,<ref name="HM Treasury"/> and returned to join [[Lazard]]s [[investment bank]] for nine years.<ref>{{cite news| url=http://business.timesonline.co.uk/tol/business/industry_sectors/retailing/article2492703.ece | work=The Times | location=London | title=Business big shot Sir Philip Hampton | first=Sarah | last=Butler | date=20 September 2007 | accessdate=22 May 2010}}</ref> Working on mergers, acquisitions, business restructurings and [[capital markets]]; in 1986 he was seconded to [[Lazard Freres]], New York, and also worked extensively with Lazard Freres in Paris.<ref name="HM Treasury"/>

Since then Hampton has been:
*[[British Steel plc]] – group finance director from 1990 to 1995; the company were a former client at Lazards
*[[British Gas plc]] – group finance director from 1995 to 1997; oversaw double-demerger of [[Centrica]] and then [[Lattice Group]]<ref>{{cite news| url=http://www.independent.co.uk/news/business/news/philip-hampton-to-quit-struggling-bt-within-year-615887.html | work=The Independent | location=London | title=Philip Hampton to quit struggling BT within year | date=5 November 2001 | accessdate=22 May 2010}}</ref>
*[[BG Group]] – group finance director from 1997 to 2000
*[[British Telecom]] – group finance director from 2000 to 2002
*[[Lloyds TSB]] – group finance director from 2002 to 2004

Hampton was appointed chairman of [[J Sainsbury]] plc on 19 July 2004 to replace Sir [[Sir Peter Davis|Peter Davis]]. His appointment came at a significant time for the retailer; Davis had been forced out as chairman by shareholders due to an extremely generous bonus package despite his dubious performance as chairman and previously as CEO, another shareholder revolt in February 2004 had caused the company to abandon the appointment of [[Ian Prosser|Sir Ian Prosser]] as chairman and [[Justin King (businessman)|Justin King]] had been appointed chief executive in March 2004. Hampton was described by then BBC Business Editor [[Jeff Randall (reporter)|Jeff Randall]] as ''"a well-respected City figure"'' and a ''"safe pair of hands".'' Under the King/Hampton leadership Sainsbury's regained some market share and in June 2006 reported its highest sales increase in four years.<ref>http://today.reuters.co.uk/news/newsArticle.aspx?type=businessNews&storyID=2006-06-21T085610Z_01_LAD002533_RTRUKOC_0_UK-RETAIL-SAINSBURY.xml&archived=False</ref> In November 2009, [[David Tyler (businessman)|David Tyler]] took over as chairman of Sainsbury's.

In the 2004 Budget, Hampton was asked to lead a review of regulatory inspection and enforcement.  This review produced the [[Hampton Report]], which led to the [[Regulatory Enforcement and Sanctions Act 2008]]. On 3 November 2008, Philip Hampton was appointed as chairman of [[UK Financial Investments Limited]], the firm set up to manage the UK government's shareholding in banks subscribing to its recapitalisation fund. He resigned to join RBS in January 2009.

Hampton was knighted in the [[New Year Honours 2007]].

===RBS Board revolts===
In December 2009, the board of [[Royal Bank of Scotland|RBS]] warned their major shareholder, the British public, that they would resign unless they were permitted to pay bonuses of £1.5bn to staff in its investment arm.<ref>{{cite news| url=http://news.bbc.co.uk/1/hi/business/8392147.stm | work=BBC News | title=RBS board could quit over bonuses | date=3 December 2009 | accessdate=22 May 2010}}</ref> The matter received heavy criticism because it followed a £42bn taxpayer bailout of the banking system.

In January 2012, the board of RBS paid a bonus of £963,000 to Stephen Hester, the then chief executive. The Treasury permitted the payment because they feared Mr Hester and much of the board would have quit if the payment had been vetoed by the government as the majority shareholder.<ref>http://www.bbc.co.uk/news/business-16751691 | date=27 January 2009</ref><ref>{{cite news| url=http://www.bbc.co.uk/news/business-16752392 | work=BBC News | title=Treasury feared Hester and board would quit | date=26 January 2012}}</ref><ref>{{cite news| url=https://www.theguardian.com/business/2012/jan/27/david-cameron-stephen-hester-bonus?newsfeed=true | location=London | work=The Guardian | first1=Nicholas | last1=Watt | first2=Larry | last2=Elliott | first3=Jill | last3=Treanor | title=Stephen Hester bonus puts David Cameron under pressure | date=27 January 2012}}</ref><ref>{{cite news| url=http://www.telegraph.co.uk/finance/newsbysector/banksandfinance/9046377/Sir-Philip-Hampton-waives-1.4m-share-award.html | location=London | work=The Daily Telegraph | first=Richard | last=Tyler | title=Sir Philip Hampton waives £1.4m share award | date=28 January 2012}}</ref><ref>{{cite news| url=http://www.reuters.com/article/2012/01/28/rbs-idUSWLA205920120128 | work=Reuters | title=RBS chairman waives share-based bonus | date=28 January 2012}}</ref><ref>{{cite news| url=http://www.dailymail.co.uk/debate/article-2092614/David-Cameron-failed-Borgen-test-Hesters-bonus.html?ito=feeds-newsxml | location=London | work=Daily Mail | first=Tim | last=Shipman | title=David Cameron has failed the Borgen test on Hester's bonus | date=27 January 2012}}</ref><ref>http://www.metro.co.uk/news/888045-ed-miliband-david-cameron-must-block-stephen-hesters-bonus</ref><ref>http://www.ft.com/cms/s/0/a4ba3c32-4843-11e1-941c-00144feabdc0.html#axzz1klLJdjwt</ref> Hampton made the decision to turn down his £1.4m bonus before the uproar over Stephen Hester's bonus.<ref>{{cite news| url=http://www.bbc.co.uk/news/uk-politics-16772525 | work=BBC News | title=RBS chairman rejects £1.4m bonus | date=28 January 2012}}</ref>

===Personal life===

==References==
{{reflist}}

==External links==
* [http://www.worldwhoswho.com/views/entry.html?id=sl1128782 HAMPTON, Philip] International Who's Who. accessed 3 September 2006.

{{DEFAULTSORT:Hampton, Philip}}
[[Category:1953 births]]
[[Category:Living people]]
[[Category:Alumni of Lincoln College, Oxford]]
[[Category:INSEAD alumni]]
[[Category:British accountants]]
[[Category:Chairmen of Royal Bank of Scotland Group]]
[[Category:Knights Bachelor]]
[[Category:British chairmen of corporations]]