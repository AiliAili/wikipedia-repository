{{good article}}
{{Infobox hurricane season
| Basin=SWI
| Year=1984
| Track=1983-1984 South-West Indian Ocean cyclone season summary map.png
| First storm formed=December 5, 1983
| Last storm dissipated=April 16, 1984
| Strongest storm name=Andry, Bakoly, Jaminy, Kamisy
| Strongest storm pressure=927
| Strongest storm winds=105
| Average wind speed=10
| Total depressions=11, 3 unofficial
| Total storms=11, 2 unofficial
| Total hurricanes=4
| Fatalities=356 total
| Damages=496
| five seasons=[[1981–82 South-West Indian Ocean cyclone season|1981–82]], [[1982–83 South-West Indian Ocean cyclone season|1982–83]], '''1983–84''', [[1984–85 South-West Indian Ocean cyclone season|1984–85]], [[1985–86 South-West Indian Ocean cyclone season|1985–86]]
| Australian season=1983–84 Australian region cyclone season
| South Pacific season=1983–84 South Pacific cyclone season
}}
The '''1983–84 South-West Indian Ocean cyclone season''' featured above normal activity and several deadly storms. There was steady storm activity from December through April due to favorable conditions, such as warm [[sea surface temperature]]s and an active [[monsoon trough|monsoon]]. The first [[tropical cyclone naming|named storm]] &ndash; Andry &ndash; was tied for the strongest with Bakoly, Jaminy, and Kesiny. Cyclone Andry passed near [[Agaléga]] island within [[Mauritius]], damaging or destroying every building there and killing one person. It later struck [[Madagascar]], the first of three storms to strike the nation within two months, which collectively caused $25&nbsp;million in damage{{#tag:ref|Damage totals are in 1984&nbsp;[[United States dollar]]s unless otherwise stated.|group="nb"}} and 42&nbsp;deaths. The third of these storms, [[Tropical Storm Domoina]], caused deadly flooding in southeastern Africa that killed 242&nbsp;people and caused $199&nbsp;million in damage. The storm destroyed more than 50&nbsp;small dams in Madagascar and caused the worst flooding in [[Swaziland]] in 20&nbsp;years. In addition three of the first storms affecting Madagascar, Cyclone Bakoly in December left $21&nbsp;million in damage on [[Mauritius]].

Less than two weeks after Domoina caused severe flooding in South Africa, Tropical Storm Imboa produced additional rainfall and high seas in the country, killing four people. The final storm of the season was [[Cyclone Kamisy]], which caused $250&nbsp;million in damage and 68&nbsp;deaths when it made landfalls in northern and northwestern Madagascar. The cities near landfall were largely destroyed, and about 100,000&nbsp;people were left homeless. The penultimate storm, Jaminy, was tied for the strongest storm in the [[tropical cyclone basins|basin]] after it crossed from the Australian region, where it was named Annette. Cyclone Fanja in January also crossed from the Australian region, where it was named Vivienne.

==Season summary==
[[File:05S Nov 23 1983 0315Z.png|left|thumb|Satellite image of the unofficial tropical storm in November]]
During the season, the Réunion Meteorological Service tracked storms in the basin, using the [[Dvorak technique]] to estimate tropical cyclone intensities via satellite imagery.<ref name="mad"/> The agency later became [[Météo-France]]'s meteorological office at [[Réunion]] (MFR). At the time, the basin extended from the east coast of Africa to [[80th meridian east|80° E]].<ref>{{cite report|author=Philippe Caroff|date=June 2011|title=Operational procedures of TC satellite analysis at RSMC La Reunion|publisher=World Meteorological Organization|accessdate=2013-07-30|url=http://www.wmo.int/pages/prog/www/tcp/documents/RSMCLaReunionforIWSATC.pdf|format=PDF|display-authors=etal}}</ref> Eleven storms were named by the Mauritius Meteorological Service and the Madagascar Meteorological Service.<ref name="mad"/> The rest of the naming list was Lalao, Monja, Nora, Olidera, Pelazy, Rija, Saholy, Tsira, Vaosolo, Wilfredy, Yannika, and Zozo.<ref>{{cite report|author=Secretariat of the World Meteorological Organization|title=Tropical cyclone operational plan for the South-West Indian Ocean|date=1983-12-01|publisher=World Meteorological Organization|accessdate=2013-07-30|url=https://books.google.com/books?id=qMARAQAAIAAJ&q=cyclone+vaosolo+wilfredy&dq=cyclone+vaosolo+wilfredy&hl=en&sa=X&ei=HPv3UZrQIJau4APwmoHwDg&ved=0CC8Q6AEwAA}}</ref> The 11&nbsp;named storms were slightly above the normal of 9, most of which formed in January and February. There were four intense tropical cyclones, which is twice the average. The increased activity of the season was in part due to enhanced [[easterlies]], a strong [[monsoon trough]], and warm water temperatures around {{convert|28|°C|°F|abbr=on}} which extended to 25°&nbsp;S.<ref name="soest">{{cite journal|author=Mark R. Jury|author2=Beenay Pathack|author3=Bin Wang|author4=Mark Powell|author5=Nirivololona Raholijao|title=A Destructive Cyclone Season in the SW Indian Ocean: January-February 1984|journal=South African Geographical Journal|year=1993|volume=95|publisher=University of Hawaii at Manoa School of Ocean and Earth Science and Technology|accessdate=2013-07-21|url=http://www.soest.hawaii.edu/MET/Faculty/bwang/bw/paper/wang27.pdf|format=PDF}}</ref>

In addition to the 11&nbsp;named storms, there were two additional storms in the season, classified by the [[Joint Typhoon Warning Center]] (JTWC).{{#tag:ref|The [[Joint Typhoon Warning Center]] is a joint [[United States Navy]]&nbsp;– [[United States Air Force]] task force that issues tropical cyclone warnings for the region.<ref>{{cite web|publisher=Joint Typhoon Warning Center|title=Joint Typhoon Warning Center Mission Statement|year=2011|accessdate=2012-07-25|url=https://metocph.nmci.navy.mil/jtwc/menu/JTWC_mission.html|archiveurl=https://web.archive.org/web/20070726103400/https://metocph.nmci.navy.mil/jtwc/menu/JTWC_mission.html|archivedate=2007-07-26}}</ref> |group="nb"}} The first developed in July in the Australian basin, and briefly crossed into the south-west Indian Ocean on July&nbsp;14. Soon after it re-entered the Australian basin and dissipated.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 HSK0184 (1983192S05089)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-29|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1983192S05089}}</ref> The other formed just southeast of [[Diego Garcia]] on November&nbsp;20. It tracked to the southwest, and the JTWC estimated peak 1&nbsp;minute sustained winds of 85&nbsp;km/h (50&nbsp;mph). The storm dissipated on November&nbsp;25 northeast of Mauritius.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 HSK0484 (1983324S08073)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-29|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1983324S08073}}</ref> In addition, Cyclone Daryl, which formed in the Australian basin on March&nbsp;6, crossed into the south-west Indian Ocean on March&nbsp;16 as a weakening storm without being renamed. Two days later it re-entered the Australian basin before dissipating.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Daryl (1984067S09101)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-30|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984067S09101}}</ref>

In December and January, three storms &ndash; Andry, Caboto, and Domoina &ndash; struck Madagascar in short succession. Collectively they dropped heavy rainfall, and some areas of the country reported precipitation totals that were 220% above normal. The storms damaged roads, bridges, dams, and croplands, wrecking 10,000&nbsp;tons of rice. Damage from the three storms was estimated at $25&nbsp;million, and 13,560&nbsp;people were left homeless. The storms cumulatively killed 42&nbsp;people.<ref name="fy">{{cite report|publisher=ReliefWeb|author=Office of US Foreign Disaster Assistance|title=Annual Report for FY 1984|accessdate=2013-07-28|url=http://reliefweb.int/sites/reliefweb.int/files/resources/EA042C733C19D854C125777E004C45A9-AR1984.pdf|format=PDF}}</ref> After seven cyclones struck or affected the country, causing 23.9&nbsp;billion [[Malagasy franc]]s ($200&nbsp;million 1984&nbsp;USD){{#tag:ref|Original currency in 1984&nbsp;[[Malagasy franc]]s, converted to [[United States dollar]]s via FXTOP.com.<ref>{{cite web|publisher=FXTOP.com|accessdate=2013-07-29|title=Currency Converter from Malagasy Francs to United States dollars|url=http://fxtop.com/en/currency-converter-past.php?A=23.9&C1=MGF&C2=USD&DD=30&MM=06&YYYY=1984&B=1&P=&I=1&btnOK=Go%21}}</ref> |group="nb"}} in crop damage, the [[African Development Bank]] approved a loan of 559&nbsp;million Malagasy francs ($1.35&nbsp;million 1989&nbsp;USD){{#tag:ref|Original currency in 1989&nbsp;[[Malagasy franc]]s, converted to [[United States dollar]]s via FXTOP.com.<ref>{{cite web|publisher=FXTOP.com|accessdate=2013-07-29|title=Currency Converter from Malagasy Francs to United States dollars|url=http://fxtop.com/en/currency-converter-past.php?A=100&C1=MGF&C2=USD&DD=01&MM=01&YYYY=1989&B=1&P=&I=1&btnOK=Go%21}}</ref> |group="nb"}} to rebuild the damaged water infrastructure. The program lasted until December 22, 1993, and consisted of repairing irrigation systems and dams.<ref>{{cite report|publisher=African Development Bank Group|date=January 1995|title=Madagascar Emergency Irrigation Infrastructure Repairs Project|accessdate=2013-07-29|url=http://www.afdb.org/fileadmin/uploads/afdb/Documents/Project-and-Operations/ADF-BD-IF-97-137-EN-SCANNEDIMAGE.215.PDF|format=PDF}}</ref>

==Systems==

===Intense Tropical Cyclone Andry===
{{Infobox hurricane small
|Basin=SWI
|Image=Cyclone Andry 7 Dec 1983 1137z.png
|Track=Andry 1983 track.png
|Formed=December 5
|Dissipated=December 14
|10-min winds=92
|1-min winds=130
|Pressure=927
}}
On December&nbsp;5, an area of convection persisted between [[Agaléga]] and Diego Garcia, which corresponded to a satellite-derived [[Dvorak technique|Dvorak rating]] of T2.0;<ref name="mad"/> on this basis, MFR assessed the system as a tropical disturbance, and later that day, JTWC also initiated advisories.<ref name="abt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Andry (1983339S10065)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-27|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1983339S10065}}</ref> The Réunion Meteorological Service named the system Andry. On December&nbsp;7, the storm intensified into a tropical cyclone,<ref name="mad"/> the same day that the JTWC upgraded Andry to the equivalent of a minimal hurricane.<ref name="abt"/> After having moved to the west, the cyclone turned more to the west-southwest, and while doing so it passed just south of the Agaléga islands, producing wind gusts of {{convert|174|km/h|mph|abbr=on}}.<ref name="mad"/> The storm damaged or destroyed every house on the island, leaving the 350&nbsp;residents without power, food, water, or shelter. Andry also downed most of the coconut trees on the island, which was the source of employment for most residents. High waves flooded wells and contaminated the water supply.<ref name="fy"/> The cyclone injured 30&nbsp;people,<ref name="mad"/> and killed one. The Mauritius government later evacuated residents to structures that were not destroyed. Following the storm, various countries donated to the country to assist, including France who sent crews from Réunion to set up shelter and provide care for the residents.<ref name="fy"/> The island was largely rebuilt after about two years.<ref>{{cite news|author=Indradev Curpen|date=2013-03-01|title=Louis Hervé da Sylva : "Inhabitants of Agaléga should participate in the integral development of their islands"|newspaper=Le Defi Media Group|accessdate=2013-07-29|url=http://www.defimedia.info/defi-quotidien/dq-interview/item/27695-louis-herve-da-sylva-inhabitants-of-agalega-should-participate-in-the-integral-development-of-their-islands.html}}</ref>

Cyclone Andry reached peak winds of December&nbsp;9, when MFR estimated winds of 170&nbsp;km/h (105&nbsp;mph). The next day, JTWC estimated 1&nbsp;minute winds of 240&nbsp;km/h (150&nbsp;mph).<ref name="abt"/> Around that time, Andry was passing just north of the northernmost tip of Madagascar at [[Antsiranana|Diego-Suarez]], where the storm produced wind gusts of {{convert|250|km/h|mph|abbr=on}}.<ref name="mad"/> The cyclone weakened while curving to the southwest and later to the south,<ref name="abt"/> making landfall on western Madagascar near [[Mahajanga|Majunga]] with wind gusts of {{convert|198|km/h|mph|abbr=on}}. While over land and turning to the southeast, Andry rapidly weakened into a tropical depression, which later passed near the capital [[Antananarivo]]. The storm emerged back into the Indian Ocean on December&nbsp;14, by which time the system was disorganized.<ref name="mad"/> That day, MFR estimated that Andry dissipated, although the JTWC assessed that the system re-intensified slightly and turned sharply southwestward before dissipating over Madagascar on December&nbsp;16.<ref name="abt"/>
{{clear}}

===Intense Tropical Cyclone Bakoly===
{{Infobox hurricane small
|Basin=SWI
|Image=Bakoly Dec 23 1983 1141Z.png
|Track=Bakoly 1983 track.png
|Formed=December 19
|Dissipated=December 30
|10-min winds=92
|1-min winds=90
|Pressure=927
}}
On December&nbsp;19, a tropical disturbance formed near Diego Garcia, which initially tracked to the south-southeast before turning to the southwest. Later that day, the system intensified to moderate tropical storm status, prompting the Mauritius Meteorological Service to name it Bakoly. The storm gradually intensified into an intense tropical cyclone, reaching peak winds of 170&nbsp;km/h (105&nbsp;mph) on December&nbsp;23. After maintaining that intensity for about 12&nbsp;hours, Bakoly weakened below cyclone status as it turned to the south-southeast. On December&nbsp;25, the storm passed between Réunion and Mauritius, and later resumed its south-southwest trajectory. After executing a small loop, Bakoly turned to the southeast and dissipated on December&nbsp;30.<ref name="mad"/><ref name="bbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Bakoly (1983353S08070)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-29|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1983353S08070}}</ref>

On Mauritius, Bakoly produced {{convert|197|km/h|mph|abbr=on}} wind gusts and heavy rainfall, reaching {{convert|507|mm|in|abbr=on}} at [[Midlands, Mauritius|Midlands]]. The high winds caused roof damage, and eight people were injured on the island.<ref name="mad"/> Bakoly caused power outages and damaged 4% of the telephone network. Damage was estimated at RS300&nbsp;million ($21&nbsp;million USD).{{#tag:ref|Original currency in 1984&nbsp;[[Mauritian rupee]]s, converted to [[United States dollar]]s via FXTOP.com.<ref>{{cite web|publisher=FXTOP.com|accessdate=2013-07-30|title=Currency Converter from Mauritius rupees to United States dollars|url=http://fxtop.com/en/currency-converter-past.php?A=300000000&C1=MUR&C2=USD&DD=30&MM=06&YYYY=1984&B=1&P=&I=1&btnOK=Go%21}}</ref> |group="nb"}}<ref>{{cite journal|title=Quarterly Economic Review of Madagascar, Mauritius, Seychelles, Comoros|journal=Economist Intelligence Unit Limited|number=1|year=1984|accessdate=2013-07-30|url=https://books.google.com/books?id=4PUbAQAAMAAJ&q=cyclone+bakoly&dq=cyclone+bakoly&hl=en&sa=X&ei=dvj3UeyfKY_J4AO5loCYBQ&ved=0CE8Q6AEwCA}}</ref> Passing within 100&nbsp;km (60&nbsp;mi) of Réunion, Bakoly produced {{convert|100|km/h|mph|abbr=on}} winds and dropped {{convert|300|mm|in|abbr=on}} of rainfall.<ref name="mad"/>
{{clear}}

===Moderate Tropical Storm Caboto===
{{Infobox hurricane small
|Basin=SWI
|Image=Caboto Jan 5 1984 1223Z.png
|Track=Caboto 1984 track.png
|Formed=January 4
|Dissipated=January 10
|10-min winds=36
|Pressure=991
}}
MFR began tracking a tropical disturbance in the Mozambique channel on January&nbsp;4. The next day, the agency estimated the system intensified into a moderate tropical storm,<ref name="cbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Caboto (1984005S15044)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-27|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1983339S10065}}</ref> prompting the Madagascar Meteorological Service to name it Caboto.<ref name="mad"/> The storm moved southward along Madagascar's western coast, reaching peak winds of about 65&nbsp;km/h (40&nbsp;mph).<ref name="cbt"/> Caboto made landfall on January&nbsp;7 to the north of the mouth of the [[Mangoky River]], and crossed the southern portion of the country, emerging near [[Farafangana]] into the Indian Ocean. Winds associated with the storm reached {{convert|43|km/h|mph|abbr=on}} at [[Morondava]] on the west coast and {{convert|63|km/h|mph|abbr=on}} at Farafangana on the east coast. A developing ridge caused Caboto to slow after it reached open waters, executing a partial loop southwest of Réunion before turning to the south and dissipating on January&nbsp;10.<ref name="mad"/>
{{clear}}

===Severe Tropical Storm Domoina===
{{Infobox hurricane small
|Basin=SWI
|Image=Domoina jan 28 1984 0900Z.jpg
|Track=Domoina 1984 track.png
|Formed=January 19
|Dissipated=January 30
|10-min winds=52
|1-min winds=55
|Pressure=976
}}
{{Main|Tropical Storm Domoina}}
Domoina developed on January&nbsp;16 off the northeast coast of Madagascar. With a ridge to the north,<ref name="mad">{{cite journal|title=La Saison Cyclonique 1983-1984 A Madagascar|date=September 1984|author=La Météorlogie, Service de la Réunion|page=146|journal=Madagascar: Revue de Géographie|accessdate=2013-07-17|language=French|url=http://madarevues.recherches.gov.mg/IMG/pdf/rev-geo43_9_.pdf|format=PDF|volume=43|issue=Juil-Déc 1983}}</ref> the storm tracked generally westward and later southwestward. On January&nbsp;21, Domoina struck eastern Madagascar. After crossing the country, Domoina strengthened in the Mozambique channel to peak winds of 95&nbsp;km/h (60&nbsp;mph). On January&nbsp;28, the storm made [[landfall (meteorology)|landfall]] in southern Mozambique, and slowly weakened over land. Domoina crossed into Swaziland and later eastern South Africa before dissipating on February&nbsp;2.<ref name="Domoniabt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Domonia (1984016S15073)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-17|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984016S15073}}</ref>

In Mozambique, Domoina dropped heavy rainfall in the capital [[Maputo]] that accounted for 40% of the annual total.<ref name="fy"/> Floods in the country destroyed over 50&nbsp;small dams and left widespread crop damage just before the summer harvest.<ref name="fy"/><ref name="mwl">{{cite journal|pages=182–183|title=Hurricane Alley|author=Dick DeAngelis|journal=Mariners Weather Log|date=Summer 1984|volume=28|number=3|publisher=United States Department of Commerce}}</ref>

Later, the rains caused the worst flooding in over 20&nbsp;years in Swaziland,<ref name="spok">{{cite news|newspaper=The Spokesman-Review|date=1984-01-31|title=Hurricane Hits Swaziland|accessdate=2013-07-24|url=https://news.google.com/newspapers?id=rFdWAAAAIBAJ&sjid=7e4DAAAAIBAJ&pg=6608,7625158&dq=mozambique&hl=en}}</ref> which damaged or destroyed more than 100&nbsp;bridges.<ref name="nyt">{{cite news|newspaper=The New York Times|date=1984-02-05|title=Swazi Storm Toll Rises to 39|agency=Reuters|accessdate=2013-07-21|url=https://www.nytimes.com/1984/02/05/world/swazi-storm-toll-rises-to-39.html}}</ref> Disrupted transport left areas isolated for several days.<ref name="rw">{{cite report|date=February 1984|author=United Nations Department of Humanitarian Affairs|title=Swaziland Floods Feb 1984 UNDRO Situation Reports 1-6|publisher=ReliefWeb|accessdate=2013-07-19|url=http://reliefweb.int/report/swaziland/swaziland-floods-feb-1984-undro-situation-reports-1-6}}</ref> In South Africa, rainfall peaked at 950&nbsp;mm (37&nbsp;in),<ref name="dwaf2">{{cite report|title=Documentation of the 1984 Domoina Floods|url=http://www.dwaf.gov.za/iwqs/reports/tr/TR_122_1984_Domoina_floods.pdf|publisher=Department of Water Affairs (South Africa)|author=Z.P. Kovács|author2=D.B. Du Plessis|author3=P.R. Bracher|author4=P. Dunn|author5=G.C.L. Mallory|date=May 1985}}</ref> which flooded 29&nbsp;river basins, notably the [[Pongola River]] which altered its course after the storm.<ref name="dwaf">{{cite report|title=The effects of the Domoina floods and releases from the Pongolapoort Dam on the Pongolo floodplain|url=http://www.dwa.gov.za/iwqs/reports/tr/B-N3_0704_Domoina%20floods%20and%20the%20Pongolo%20floodplain.pdf|publisher=Department of Water Affairs (South Africa)|author=J.N. Rossouw|date=January 1985}}</ref> Flooding caused the [[Pongolapoort Dam]] to reach 87% of its capacity; when waters were released to maintain the structural integrity, additional flooding occurred in Mozambique, forcing thousands to evacuate.<ref name="moz">{{cite report|title=Coping with Floods - The Experience of Mozambique|author=Álbaro Carmo Vaz|publisher=1st WARFSA/WaterNet Symposium: Sustainable Use of Water Resources|date=November 2000|accessdate=2013-07-24|url=http://www.docstoc.com/docs/50875552/COPING-WITH-FLOODS---THE-EXPERIENCE-OF-MOZAMBIQUE1-1-THE-FLOOD}}</ref> Throughout the region, Domoina caused widespread flooding that damaged houses, roads, and crops, leaving about $199&nbsp;million in damage. There were 242&nbsp;deaths in southeastern Africa.<ref name="fy"/><ref name="mwl"/><ref name="usaid">{{cite report|author=Office of U.S. Foreign Disaster Assistance|title=Significant Data on Major Disasters Worldwide 1900-present|date=August 1993|accessdate=2013-07-19|url=http://pdf.usaid.gov/pdf_docs/PNABP986.pdf|format=PDF}}</ref>
{{Clear}}

===Moderate Tropical Storm Edoara===
{{Infobox hurricane small
|Basin=SWI
|Image=Edoara Jan 21 1984 1047Z.png
|Track=Edoara 1984 track.png
|Formed=January 21
|Dissipated=January 25
|10-min winds=36
|1-min winds=35
|Pressure=991
}}
A circulation developed south of Diego Garcia on January&nbsp;20, and the next was classified as a tropical disturbance by MFR. Given the name Edoara by the Mauritius Meteorological Service, it quickly intensified into a moderate tropical storm, although it never strengthened beyond winds of 65&nbsp;km/h (40&nbsp;mph). While maintaining a southwest track, Edoara passed southeast of Rodrigues, Mauritius, and Réunion. On Rodrigues, the storm produced wind gusts of {{convert|131|km/h|mph|abbr=on}}, and heavy rainfall reaching {{convert|253|mm|in|abbr=on}} at [[Baie aux Huîtres]]. After moving away from the islands, Edoara dissipated on January&nbsp;25.<ref name="mad"/><ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Edoara (1984020S11077)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-29|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984020S11077}}</ref>
{{clear}}

===Moderate Tropical Storm Vivienne-Fanja===
{{Infobox hurricane small
|Basin=SWI
|Image=Vivienne Jan 26 1984 0945Z.png
|Track=Vivienne-Fanja 1984 track.png
|Formed=January 27 (entered basin)
|Dissipated=January 30
|10-min winds=44
|1-min winds=75
|Pressure=984
}}
The origins of Vivienne-Fanja are unclear as a result of sparseness of data, due to a disruption in satellite imagery coverage. It is estimated that a tropical low formed on January&nbsp;23 west of [[Christmas Island]] in the Australian basin. The [[Bureau of Meteorology]] named the storm Vivienne, which gradually intensified while moving to the west.<ref name="Tropical Cyclone Vivienne">{{cite report|title=Tropical Cyclone Vivienne|publisher=Bureau of Meteorology|accessdate=2013-07-29|url=http://www.bom.gov.au/cyclone/history/vivienne84.shtml}}</ref> On January&nbsp;27, the cyclone crossed 80°&nbsp;E into the southwest Indian Ocean, at which time it was renamed Fanja. While in the basin, the storm reached peak winds of 80&nbsp;km/h (50&nbsp;mph). It continued moving to the southwest before dissipating on January&nbsp;30.<ref name="mad"/><ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Fanja:Vivienne (1984022S08086)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-29|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984022S08086}}</ref>
{{clear}}

===Moderate Tropical Storm Galy===
{{Infobox hurricane small
|Basin=SWI
|Image=Galy Feb 1 1984 1153Z.png
|Track=Galy 1984 track.png
|Formed=January 29
|Dissipated=February 4
|10-min winds=36
|Pressure=991
}}
On January&nbsp;29, a circulation developed between Agaléga and Tromelin island.<ref name="mad"/> Initially the system moved to the southwest, followed by a turn to the southeast. Given the name Galy, the storm attained winds of 65&nbsp;km/h (40&nbsp;mph) on January&nbsp;30, but weakened into a tropical disturbance the next day. By that time, Galy turned to the west toward the Madagascar coastline, and on February&nbsp;1 re-intensified into a moderate tropical storm.<ref name="gbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Galy (1984025S14073)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-27|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984025S14073}}</ref> The next day, Galy made landfall near Mananjary, but soon after recurved to the southeast and emerged into the Indian Ocean near [[Tôlanaro|Fort Dauphin]]. On February&nbsp;4, the storm dissipated in a polar trough. While over land, Galy dropped light rainfall of around {{convert|44.5|mm|in|abbr=on}}.<ref name="mad"/>
{{clear}}

===Severe Tropical Storm Haja===
{{Infobox hurricane small
|Basin=SWI
|Image=Haja Feb 17 1984 1157Z.png
|Track=Haja 1984 track.png
|Formed=February 7
|Dissipated=February 19
|10-min winds=52
|1-min winds=45
|Pressure=976
}}
A tropical depression formed on February&nbsp;7 south of Diego Garcia. For about a week, the system remained weak and changed directions several times; after an eastward movement, it turned to the northwest, curved to the southeast, and later began a steady track to the southwest. On February&nbsp;13, it intensified into a moderate tropical storm, and quickly attained peak winds of 95&nbsp;km/h (60&nbsp;mph). Given the name Haja, the storm passed southeast of Rodrigues and Réunion. Haja approached the southeast coast of Madagascar, but turned to the southeast and weakened, dissipating on February&nbsp;19.<ref name="mad"/><ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Haja (1984038S12073)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-29|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984038S12073}}</ref>
{{clear}}

===Severe Tropical Storm Imboa===
{{Infobox hurricane small
|Basin=SWI
|Image=Imboa Feb 13 1984 1247Z.png
|Track=Imboa 1984 track.png
|Formed=February 10
|Dissipated=February 19
|10-min winds=52
|1-min winds=45
|Pressure=976
}}
On February&nbsp;10, MFR began tracking a tropical disturbance in the Mozambique channel near [[Juan de Nova Island]]. The system tracked generally south-southwestward, gradually intensifying. Given the name Imboa, the storm reached peak winds of 95&nbsp;km/h (60&nbsp;mph) on February&nbsp;13 while passing near [[Europa Island]]. After executing a small loop, Imboa turned toward the southeastern African coastline and approached the eastern coasts of Mozambique and South Africa as a weakened system. A ridge caused the storm to turn to the east and northeast, dissipating on February&nbsp;19.<ref name="mad"/><ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Imboa (1984041S17042)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-29|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984041S17042}}</ref>

Early in its duration, Imboa produced winds of {{convert|111|km/h|mph|abbr=on}} at [[Maintirano]] while passing off the west coast of Madagascar.<ref name="mad"/> While offshore South Africa, Imboa dropped heavy rainfall along the coast just weeks after Domoina flooded the region, reaching over {{convert|350|mm|in|abbr=on}} in some locations. The rains caused flooding along the [[Mhlatuze River|Mhlatuze]] and Mfuluzone rivers,<ref name="dwaf2">{{cite report|title=Documentation of the 1984 Domoina Floods|url=http://www.dwaf.gov.za/iwqs/reports/tr/TR_122_1984_Domoina_floods.pdf|publisher=Department of Water Affairs (South Africa)|author=Z.P. Kovács|author2=D.B. Du Plessis|author3=P.R. Bracher|author4=P. Dunn|author5=G.C.L. Mallory|date=May 1985}}</ref> which destroyed a temporary bridge along the [[Umfolozi River]] built after Domoina.<ref name="mwl"/> Along the coast, Imboa produced high tides that caused beach erosion.<ref name="lucia">{{cite book|title=Ecology and Conservation of Estuarine Ecosystems: Lake St Lucia as a Global Model|editor1=Renzo Perissinotto |editor2=Derek D. Stretch |editor3=Ricky H. Taylor |year=2013|publisher=Cambridge University|ISBN=978-1-107-01975-1|location=New York|accessdate=2013-07-24|url=https://books.google.com/books?id=KcC37CogsSoC&pg=PA38&dq=domoina&hl=en&sa=X&ei=OurvUfeHNo2A9QSZioDQCA&ved=0CDMQ6AEwAQ#v=onepage&q=domoina&f=false}}</ref> There were four deaths in the country.<ref name="fy"/>
{{clear}}

===Intense Tropical Cyclone Annette-Jaminy===
{{Infobox hurricane small
|Basin=SWI
|Image=Jaminy Feb 16 1984 1028Z.png
|Track=Annette 1984 track.png
|Formed=February 16 (entered basin)
|Dissipated=February 24
|10-min winds=92
|1-min winds=100
|Pressure=927
}}
Cyclone Annette developed simultaneously with Cyclone Willy in the Australian basin and Cyclone Haja in the south-west Indian. On February&nbsp;3, a tropical low formed northeast of the [[Cocos Islands]]. Steered by a ridge to the south, it moved generally southwestward and intensified into Tropical Cyclone Annette, named by the Bureau of Meteorology. After executing a loop, Annette crossed 80&nbsp;E into the south-west Indian Ocean on February&nbsp;16.<ref name="Tropical Cyclone Vivienne"/> Upon crossing into the basin, the storm was renamed Jaminy by the Mauritius Meteorological Service.<ref name="mad"/> Around that time, the cyclone attained peak winds of 170&nbsp;km/h (105&nbsp;mph). Jaminy moved generally southwestward and weakened below cyclone status on February&nbsp;20. The next day, it turned to the southeast, later dissipating on February&nbsp;24.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Annette:Jaminy (1984035S09101)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-29|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984035S09101}}</ref>
{{Clear}}

===Intense Tropical Cyclone Kamisy===
{{Infobox hurricane small
|Basin=SWI
|Image=Kamisy Apr 8 1984 0401Z.png
|Track=Kamisy 1984 track.png
|Formed=April 3
|Dissipated=April 16
|10-min winds=92
|1-min winds=100
|Pressure=927
}}
{{main|Cyclone Kamisy}}
A tropical disturbance formed near Diego Garcia on April&nbsp;3 and subsequently moved westward, intensifying into a moderate tropical storm two days later. Given the name Kamisy, the storm gradually intensified into an intense tropical cyclone by April&nbsp;9. Kamisy reached winds of 170&nbsp;km/h (105&nbsp;mph) before making landfall in extreme northern Madagascar near Diego Suarez. It weakened upon entering the Mozambique channel, but briefly re-intensified on April&nbsp;10. That day while passing near Mayotte, the cyclone turned to the southeast, striking Madagascar again near Majunga. Kamisy quickly crossed the country and quickly weakened into a tropical disturbance. After emerging into the Indian Ocean off the east coast of Madagascar, the system re-intensified into a moderate tropical storm before dissipating on April&nbsp;16.<ref name="mad"/><ref name="kbt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1984 Kamisy (1984094S10080)|publisher=Bulletin of the American Meteorological Society|accessdate=2013-07-28|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1984094S10080}}</ref>

In northern Madagascar, Kamisy produced wind gusts of {{convert|250|km/h|mph|abbr=on}}, which destroyed 80% of the city of Diego Suarez. About 39,000&nbsp;people were left homeless in the area, and there were five deaths.<ref name="mad"/> In western Madagascar, the cyclone dropped {{convert|232.2|mm|in|abbr=on}} of rainfall in 24&nbsp;hours in Majunga, which damaged rice fields in the region after causing widespread river flooding.<ref name="mad"/><ref name="fy"/> The storm destroyed about 80% of Majunga where the storm struck. Throughout the country, Kamisy caused $250&nbsp;million in damage and 68&nbsp;deaths,<ref name="fy"/> with 215&nbsp;people injured and 100,000&nbsp;left homeless.<ref name="usaid"/> Kamisy also affected Mayotte with winds of over {{convert|100|km/h|mph|abbr=on}}, which left about 25,000&nbsp;homeless and left widespread damage.<ref name="fy"/> One death was reported on the island.<ref name=UPI1>{{cite news|title=Cyclone devastates Indian Ocean islands|date=1984-04-13|agency=United Press International}} {{subscription required|via=Lexis Nexis}}</ref>
{{Clear}}

==See also==
*Atlantic hurricane seasons: [[1983 Atlantic hurricane season|1983]], [[1984 Atlantic hurricane season|1984]]
*Eastern Pacific hurricane seasons: [[1983 Pacific hurricane season|1983]], [[1984 Pacific hurricane season|1984]]
*Western Pacific typhoon seasons: [[1983 Pacific typhoon season|1983]], [[1984 Pacific typhoon season|1984]]
*North Indian Ocean cyclone seasons: [[1983 North Indian Ocean cyclone season|1983]], [[1984 North Indian Ocean cyclone season|1984]]

==Notes==
{{Reflist|group=nb}}

==References==
{{reflist|2}}

{{TC Decades|Year=1980|basin=South-West Indian Ocean|type=cyclone|shem=yes}}

{{DEFAULTSORT:1983-84 South-West Indian Ocean cyclone season}}
[[Category:1983–84 South-West Indian Ocean cyclone season]]