{{Use dmy dates|date=November 2013}}
{{Use British English|date=November 2013}}
[[File:COAST logo.jpg|thumb|COAST logo]]
The '''Community of Arran Seabed Trust (COAST)''' is a registered Scottish charity based on the [[Isle of Arran]], the largest island in the [[Firth of Clyde]].<ref name="COAST">[http://www.arrancoast.com ArranCOAST]</ref> Established in 1995 by two local divers (Howard Wood and Don McNeish) COAST has been a registered charity since 2011 and is run mostly by Arran residents, who all share the same aims to protect the health of the marine environment around the Isle of Arran and within the Firth of Clyde from indiscriminate commercial fishing methods.<ref name="BBC">[http://www.bbc.co.uk/news/uk-scotland-glasgow-west-13165228 BBC News - Marine life 'flourishing' at Lamlash reserve]</ref> COAST values the marine biodiversity around the shoreline of Arran and aims to regenerate and protect it for future generations. 

COAST was the driving force behind the creation of Scotland’s first [[No Take Zone]] (NTZ) in [[Lamlash]] Bay in 2008. In May 2012 COAST submitted a proposal for a [[Marine Protected Area]] (MPA) around the south of Arran which, now that the MPA has been designated, is part of the Scottish MPA network. The South Arran MPA is one of the only Scottish MPA's which has been motivated and developed by the local community.   

== Vision and purpose ==

COAST’s vision is for healthy seas around Arran and in the Clyde. The purpose is to ensure shared responsibility for the protection and restoration of the marine environment and the diversity of life it supports in this area.<ref name="Aljazeera">[http://www.aljazeera.com/programmes/earthrise/2012/07/2012727113556327127.html AlJazeera Arran No Take Zone]</ref>

=== Main objectives ===

COAST's charitable objectives are:
# To identify and secure, areas of seabed around the Isle of Arran for the community to promote and protect all forms of marine life; 
# To protect and improve the surrounding waters adjacent to the project for the advancement of environmental protection to promote sustainable marine stocks; 
# To educate the community, individuals and organisations in relation to the need for marine conservation.

=== Strategy ===

COAST will work collaboratively to research the social, economic and environmental benefits of restoring and protecting Clyde seas, to promote widespread awareness of the findings and actively campaign for the protection and restoration of this public resource.

=== Research ===

COAST is conducting long-term research and monitoring programmes in Arran’s waters. It is an active participant in the [http://www.seasearch.org.uk Seasearch initiative] and also hosts and supports researchers from the [[University of Glasgow]], [[University of York]], [[Edinburgh Napier]] and [[Herriot Watt]]. The impacts of the No Take Zone (NTZ) are monitored annually to obtain data on the development of species such as scallops and crustaceans and the regeneration of the seabed within and outside of the protected area. Marine research underpins the South Arran MPA proposal and baseline studies were carried out prior to designation in July 2014 and have continued since, both by academic research institutes and government agencies.

=== Additional aims ===

* Ensure COASTs work is underpinned by rigorous and independent scientific investigation.
* Improve the marine environment in our own "backyard", determining success through rigorous monitoring.
* Reverse the decline of local fish stocks.
* Inform and advise others committed to marine conservation of our experience of community based action.
* Contribute to the development of national legislation to improve the health of our seas.

=== Funders ===

COAST is currently funded mainly by grants from [[Esmée Fairbairn Foundation]] and the John Ellerman Foundation and has previously received support from The Underwood Trust [[Scottish Natural Heritage]], European Commission Directorate General for the Environment, [[Marine Conservation Society]] and initial start-up funding from the Lottery Awards for All.

==History of the Firth of Clyde ecosystem and the need for conservation measures ==

The Firth of Clyde in South West Scotland, an area of 3650 square miles, is the most southerly fjord in the northern hemisphere and extends over 100&nbsp;km into Scotland’s west coast. Once comprising a highly diverse and rich ecosystem the Firth of Clyde marine environment has seen dramatic alterations throughout the last 200 years.<ref name="McIntyre">McIntyre F, Fernandes PG and Turrell WR (2012) Clyde Ecosystem Review. Scottish Marine and Freshwater Science Vol 3 No 3</ref>  During the 19th century, diverse mixed fisheries existed in the Firth of Clyde for [[herring]] (''Clupea harengus''), [[cod]] (''Gadus morhua''), [[haddock]] (''Melanogrammus aeglefinus''), [[turbot]] (''Psetta maxima'') and a variety of other species.<ref name="McIntyre" /><ref name="Thurstan">Thurstan RH, Roberts CM (2010) Ecological Meltdown in the Firth of Clyde, Scotland: Two Centuries of Change in a Coastal Marine Ecosystem. PLoSONE 5(7): e11767. doi:10.1371/journal.pone.0011767</ref>

The 19th century saw increased demand for fish, which encouraged more indiscriminate methods of fishing such as [[bottom trawling]] to also catch [[demersal fish]] (fish living and feeding mainly on the seabed). During the 1880s, fish landings began to decline, and upon the recommendation of local fishers and scientists, the Firth of Clyde was closed to large trawling vessels in 1889. This closure remained in place until 1962 when bottom trawling for [[Norway lobster]] (''Nephrops norvegicus'') was approved in areas more than three nautical miles (nm) from the coast. During the 1960s and 1970s, landings of bottomfish increased as trawling intensified. The trawl closure within three nm of the coast was repealed in 1984 under pressure from the industry. Thereafter, bottomfish landings went into terminal decline, with all species collapsing to zero or near zero landings by the early 21st century. Herring fisheries collapsed in the 1970s as more efficient mid-water trawls and fish finders were introduced.

Between the 1960s and the early 2000s populations of cod and haddock have dropped by 95 - 99%.<ref name="Thurstan" /> A population is termed 'collapsed' if there is a decline of over 90% from the peak catch.  This collapse of commercial populations in the Clyde is often compared to the similar collapse of [[cod fishing in Newfoundland]] in the 1990s due to intense overfishing.<ref name="Thurstan" /> Today there is now no viable fishery of those species. Instead invertebrate species are targeted, such as the Norway lobster and [[scallops]] (''Pecten maximus''). For this usually otter trawls and dredges are used, which are dragged along the seabed to disturb bottom-dwelling fish, a practice which damages seabed habitats.<ref name="McIntyre" /><ref name="Thurstan" />

In July 2011, a paper was published indicating a major ecological impact of fishing on the ecosystems of the Clyde.  Fishing is thought to have completely altered the mix and size distribution of fish species in the Clyde. Once dominated by large predatory species (e.g. [[spurdog]], [[hake]] and cod), now 70% of species found are small whiting and other small fish.<ref name="Heath">Heath MR and Speirs DC (2011) Changes in species diversity and size composition in the Firth of Clyde demersal fish community (1927–2009) Proc. R. Soc. Vol. 279 no. 1728 543-552</ref> Such a simplification of the ecosystem can possibly lead to a decline in diversity, thus threatening fish stocks and fishery of the future.<ref name="Howard">Howard LM, Roberts CM, Thurstan RH, Stewart BD (2013) The unintended consequences of simplifying the sea: making the case for complexity. Fish and Fisheries, DOI: 10.1111/faf.12041</ref> A report by Marine Scotland suggests that these changes might have been, amongst others, caused by high by-catch rates found in ''Nephrops'' fisheries using trawling and dredging equipment.<ref name="McIntyre" /> The habitat destruction caused by these methods is also thought to be a major factor influencing species composition and size.<ref name="Thurstan" /> 
Other scientists have concluded that the marine ecosystems in the Clyde are nearing the endpoint of overfishing<ref name="Thurstan" /> but, as the Clyde used to be a highly productive system, there is potential for recovery if appropriate measures are taken.<ref name="McIntyre" />

In partnership with a wide range of stakeholders, the Scottish Government announced the Clyde 2020 initiative, which will involve research and practical measures to improve the Firth of Clyde marine ecosystem. In April 2014, the Clyde summit was held, where a wide range of stakeholders were brought together to determine the focus of and develop an action plan for the initiative.<ref name="Clyde2020">http://www.scotland.gov.uk/Topics/marine/marine-environment/Clyde2020</ref>

== No Take Zone ==

[[File:View of NTZ.JPG|thumb|View of the No Take Zone in Lamlash Bay and information board]]
Scotland’s first No Take Zone (NTZ) was designated by the Scottish Government on 20 September 2008 at the north end of Lamlash Bay on the east of the Isle of Arran, after 13 years of campaigning by COAST. It was established to protect [[Maerl]] beds and to promote natural regeneration of all marine life. It covers an area of 2.67 km<sup>2</sup> of sea and seabed from which no marine life can be removed by any method. This was achieved after over a decade of campaigning by the island community.<ref name="COAST" />

=== Benefits of a NTZ ===

No take zones are likely to economically benefit fisheries, tourism and local communities, and have done so in many areas of the world.<ref name="Roberts">Roberts CM,  Bohnsack JA, Gell F, Hawkins JP, Goodridge R (2001) Effects of Marine Reserves on Adjacent Fisheries. Science Vol 294, 1920 - 1923</ref> By protecting a certain area from external disturbances such as damaging fishing methods, biomass and diversity can flourish and improve within a short time. Nearby fisheries and ecosystem can benefit from enhanced recruitment within the protected area when individuals leave the protected zone ([[‘spillover effect’]]). Long term, the establishment of a network of protected zones can create optimal harvesting areas.<ref name="Grafton">Grafton RQ, Komas P, Van Ha P (2006) The Economic Payoffs from Marine Reserves: Resource Rents in a Stochastic Environment*. The Economic Record Vol. 82, No. 259, pp. 469–480</ref> The UK's first NTZ around [[Lundy]] Island in the Bristol Channel, Devon, established in 2003 is an example for the beneficial effects of a protected marine area. Five years after its creation, scientific surveys of the NTZ's ecology have found an increase in abundance and average size of landable-sized lobsters, both within and adjacent to the protected area; as well as an increase in numbers of undersized lobsters within the NTZ’s boundary. Additionally the local tourism sector recorded an increase of divers, anglers and sightseers thanks to increased publicity surrounding Lundy and the marine reserve as a result of its designation, and the resulting improvement of marine biodiversity, including seabirds.<ref name="IUCN">IUCN, 2009. Marine protected areas case studies, Lundy Island NTZ</ref>

A study conducted by researchers from the University of York in 2013, provides evidence that the creation of the NTZ in Lamlash bay has supported the regeneration of the seabed and overall biodiversity of the area. Animals which attach to the seabed, such as maerl and hydrozoans were found to be twice as abundant inside the NTZ than outside. In addition, within the NTZ, the average size of scallops and lobsters were larger. Scallops were more abundant, larger and older within the NTZ, while higher numbers of larger lobsters were found inside the NTZ, which is thought to increase their reproductive potential.<ref name="LBNTZ">http://www.arrancoast.com/pdf2013/Summary_of_York_University_research_within_Lamlash_Bay_No_Take_Zone_2013.pdf></ref>  The majority of species found across all habitats in the NTZ were all bottom dwelling, opportunistic invertebrates (such as crabs and starfish), which suggests there is still some recovery to go.   

=== Maerl ===

[[File:ARRAN SEASCENES 013.jpg|thumb|Maerl, a corraline algae]]
A significant marine feature in the Lamlash bay NTZ are the [[Maerl]] beds. Maerl is also present in the South Arran MPA. Maerl is a very slow-growing (1 mm per year) coral-like calcareous red algae and is an important Scottish species.<ref>http://www.snh.gov.uk/about-scotlands-nature/species/algae/marine-algae/maerl/</ref> Maerl beds are reservoirs of biodiversity, important both as nursery grounds for young scallops and young fish. Studies show that organic waste from fish farms significantly reduces live maerl and that scallop dredging has profound and long lasting impacts. Scallop dredging on a maerl bed has been found to kill over 70% of live maerl, with no discernible recovery over the following four years. Recovery of maerl beds would be expected to require many years without disturbance.<ref name="Hughes">Hughes D & Nickell T (2009) Recovering Scotland’s Marine Environment, Report to Scottish Environment LINK</ref>

== Scotland's Marine Protected Area Network ==

Marine Protected Areas (MPAs) are geographically defined marine zones, used as a management tool used to protect the marine environment. MPAs are recognised as a specific means to achieve 'good environmental status' in the marine environment.  In 2011 Scottish Natural Heritage (SNH) proposed a network of 33 MPAs for designation to the Scottish government. As of July 2014, 30 MPAs were designated; 17 of which are in Scotlands inshore zone (within 12 nm), and 13 are offshore.<ref>http://www.scotland.gov.uk/Topics/marine/marine-environment/mpanetwork</ref>

The United Kingdom  has a duty to create an ecologically coherent network of marine protected areas by 2012, as promised under several agreements including the [[OSPAR Convention]], [[World Summit on Sustainable Development]], [[Convention on Biological Diversity]],<ref>http://jncc.defra.gov.uk/default.aspx?page=4549</ref> [[Water Framework Directive]] and most recently the [[Marine Strategy Framework Directive]] (MSFD). The European MSFD was transposed into UK legislation on 15 July 2010 and requires member states to prepare national strategies to manage their seas to achieve or maintain Good Environmental Status by 2020.<ref>[http://www.scotland.gov.uk/Topics/marine/seamanagement/msfd Marine Strategy Framework Directive]</ref>  In Annex I, 11 qualitative descriptors were set up to determine ‘good environmental status’. Amongst others, they require the maintenance of biological diversity (descriptor 1), populations of all commercially exploited fish and shellfish to be within safe biological limits exhibiting a population age and size distribution that is indicative of a healthy stock (descriptor  3) and to ensure that sea-floor integrity is at a level that ensures that the structure and functions of the ecosystems are safeguarded and benthic ecosystems, in particular, are not adversely affected (descriptor 6).

The [[Marine and Coastal Access Act 2009]] provides executive devolution to Scottish Ministers of the new marine planning and conservation powers in the offshore region (12-200 nautical miles) of Scotland and in particular, the power to designate MPAs. This will provide greater flexibility for Ministers to use area-based measures to conserve marine biodiversity.<ref>[http://www.scotland.gov.uk/Topics/marine/seamanagement/marineact/ukbill Marine and Coastal Access Act 2009]</ref> Marine Protected Areas are essential for healthy, functioning and resilient ecosystems which help to deliver the Government’s vision of a clean, healthy, safe, productive and biologically diverse oceans and seas. As some human activities can damage or cause disturbance to marine habitats and their species, a MPA is designed to manage or restrict such activities. 

Case studies from Scottish waters support the general scientific consensus that use of mobile fishing gear, particularly bottom-trawls and dredges, is the most pervasive form of human disturbance to the seabed environment and the most significant cause of ecosystem degradation in coastal seas. Other processes, such as discharge of organic waste and water-borne chemical contaminants can be locally important, but are of much lower overall impact than the use of mobile fishing gear.<ref name="Hughes" />

== South Arran MPA ==

In 2012 COAST submitted their proposal to [[Scottish Natural Heritage]] (SNH) for a designated Marine Protected Area (MPA) around the South of Arran under the Marine Scotland Act 2010. On 24 July 2014 the South Arran MPA was created. Though management measures are still to be determined, COAST is advocating all active/towed gear (trawls, dredged and hydraulic methods) should be excluded from the entire proposed South Arran MPA. This will allow regeneration of the seabed and increase the social and economic benefit to the Clyde. COAST recommends that only sustainable methods such as creeling, hand diving for scallops and angling should be allowed inside the MPA. Currently, the management for the South Arran MPA has not been designated and all activities in the area are continuing as normal. [[Marine Scotland]] has proposed options which allow for the continuation of trawling and dredging in the MPA. <ref>http://www.arrancoast.com/campaigns/south-arran-marine-protected-area</ref>

The South of Arran MPA will:
* Promote sustainable fishing such as creeling, hand diving and sea angling 
* Create an area protected from bottom trawling and dredging 
* Protect important habitats and fish nursery grounds
* Effectively reinstate part of the three-mile limit around South Arran that was stopped in 1984.


The  MPA will not:
* Be a no take zone
* Stop anyone from sea angling, yachting, diving, kayaking or swimming in the area

== External links ==
* [http://www.arrancoast.com ArranCOAST]
* [http://www.mcsuk.org MSC UK]
* [http://www.ukmpas.org UK Marine Protected Areas Centre]
* [http://www.seasearch.org.uk SeaSearch]
* [http://www.lundymcz.org.uk Lundy No Take Zone]
* [http://www.esmeefairbairn.org.uk Esmée Fairbairn Foundation]
* [http://www.snh.org.uk Scottish National Heritage Trust]

== References ==

{{reflist}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box.  Just press "Save page". -->

[[Category:Marine conservation organizations]]
[[Category:Fisheries conservation organizations]]