{{notability|date=March 2015}}
{{unreferenced|date=March 2015}}
{{Infobox journal
| title         = Journal of Legislation
| cover         = 
| editor        = 
| discipline    = 
| peer-reviewed = 
| language      = 
| former_names  = 
| abbreviation  = 
| publisher     = [[Notre Dame Law School]]
| country       = 
| frequency     = Semiannual
| history       = 
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0360-4209
| eISSN         = 
| boxwidth      = 
}}
'''''Journal of Legislation''''' is a major [[law review|scholarly journal]] published by [[Notre Dame Law School]].

The ''Journal of Legislation'' (''JLEG'') is a legislative law review which focuses on analysis and reform of public policy. The primary purpose of the Journal is to provide a forum for debate of timely issues of state, national, and international stature. The ''Journal'' differs from traditional law reviews by concentrating primarily on statutory, regulatory, and public policy matters rather than on case law.

Prominent public figures and legal scholars contribute major pieces of legal analysis to the Journal in the form of "articles". We also publish substantial student-written pieces called "notes" and commentaries on recent legislation in our "legislative reform" section. The staff chooses topics for article solicitation, note writing, and legislative reform pieces based upon the current legislative and administrative policies of state, national, and international bodies. The staff solicits articles authors from among those public figures who influence politics. 

The ''Journal'' believes in the open debate of all political ideologies and philosophical points of view. The main philosophy behind the ''Journal'''s solicitation effort is that viewpoints and recommendations of public leaders are more likely to effect policy change than are technical analyses by lesser known experts. Therefore, the ''Journal'' has traditionally solicited legislators, judges, administrators, and prominent attorneys, as well as recognized experts from beyond the legal world.

The ''Journal'' is also dedicated to helping the individual student learn to think critically, research thoroughly, and write with precision and style. For this reason the Journal is proud to uphold the tradition of publishing student notes. Writing, editing, cite checking, and proofreading provide members with intense training in the art of legal writing. The Editorial Staff is available to help student staff members and contributors achieve these goals.
 
The ''Journal'' is financially self-sufficient, independently organized and completely student-run.

==Notable contributors==
*U.S. Sen. [[Christopher Dodd]], "A Proposal for Making Product Liability Fair, Efficient and Predictable"
*U.S. Rep. [[Richard A. Gephardt]], "Tax Reform and Capital Gains: The War Against Unfair Taxes is Far From Over"
*U.S. Rep. [[Jack Kemp]], "Early Phased Deployment of SDI as a National Insurance Policy"
*Gov. [[Michael S. Dukakis]], "The Problem is Not the Phone Calls; It’s the Special Interest Money"
*U.S. Rep. [[Bob Barr]], "A Tyrant’s Toolbox: Technology and Privacy in America"
*U.S. Sen. [[Rick Santorum]], "A Compassionate Conservative Agenda: Addressing Poverty for the Next Millennium"

==External links==
*{{official website|1=http://scholarship.law.nd.edu/jleg}}

{{University of Notre Dame}}


{{law-journal-stub}}

[[Category:American law journals]]
[[Category:Notre Dame Law School]]
[[Category:University of Notre Dame]]