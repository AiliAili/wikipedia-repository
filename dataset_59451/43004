<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Meteor
 | image=Ikarus Meteor 57 (YU-4103) glider.png
 | caption=Meteor 57, probably at Jeżów Sudecki airstrip, Poland, c.a. 1958
}}{{Infobox Aircraft Type
 | type=Single seat competition [[glider (sailplane)|sailplane]]
 | national origin=[[Yugoslavia]]
 | manufacturer=
 | designer=Boris Cijan, Stanko Obad and Miho Mazovec
 | first flight=1955
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built= probably only 2
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Ikarus Meteor''' is a long-span, all-metal [[glider (sailplane)|sailplane]] designed and built in [[Yugoslavia]] in the 1950s. It competed in [[World Gliding Championships]] (WGC) between 1956 and 1968 and was placed fourth in 1956; it also set new triangular-course world speed records.

==Design and development==
The Meteor was designed by the same duo, Boris Cijan and Stanko Obad, who had produced the [[Cijan-Obad Orao|Orao]] glider which had achieved third place in the 1950 WGC.  They were aided by Miho Mazovec and generously funded by the Yugoslav government. The result was "... without doubt the most advanced sailplane of its time."<ref name=SimonsII>{{cite book |title=Sailplanes 1945-1965 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9807977 4 0|pages=182}}</ref> In 1955 almost all gliders had wooden structures, with perhaps a few unstressed [[glass reinforced plastic|GRP]] parts; in contrast the Meteor was all-metal, with a large, 20&nbsp;m (65&nbsp;ft&nbsp;7&nbsp;in) span and an [[aspect ratio (wing)|aspect ratio]] of 25, values beyond the reach of wood.  The optimum glide ratio of greater than 40:1 was good, but the high inter-thermal glide speed was exceptional.<ref name=SimonsII/>

The Meteor was one of a group of mid-1950s gliders to use the [[NACA]] 6 series [[laminar flow]] [[airfoil]] first adopted by the [[Ross-Johnson RJ-5]], which required careful attention to profile control and surface finish.  In plan the wings are straight tapered with unswept [[leading edge]]s and forward sweep on the [[trailing edge]].  The [[wing tip]]s carries small, elongated bodies termed "salmons" to dampen tip vortices, as on the slightly earlier [[Bréguet Br 901 Mouette|Bréguet Mouette]] and has a constant 2° of [[dihedral (aircraft)|dihedral]].  Completely metal-skinned, the wing is built around a box spar within which the thickened skin is internally stiffened with span-wise stringers.  The whole trailing edge carries control surfaces; the outer quarter with conventional [[aileron]]s, and the rest roughly equally divided between narrower inboard ailerons which droop together when the final inboard section of [[Camber (aerodynamics)|camber]] changing flaps are depressed through as much as 20° for low speed flight.  These flaps can be raised by 11°, reducing the camber for high speed flight.  [[Schempp-Hirth]] type [[air brake (aircraft)|airbrakes]] are fitted at mid-chord, just aft of the box spar at about one third span.<ref name=SimonsII/><ref name=JAWA60>{{cite book |title= Jane's All the World's Aircraft 1960-61|last= Taylor |first= John W R |edition= |year=1960|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|page=411 }}</ref>

The Meteor has a pod-and-boom style [[fuselage]], with a long, single-piece [[canopy (aircraft)|canopy]] above the upper fuselage line, forward of the wing, under which the pilot sits in a reclined position.  The under-wing, retractable [[Landing gear#Gliders|monowheel undercarriage]] is assisted by a short, retractable, forward skid.  Retraction is manual; there is a wheel brake.  Behind the wing the fuselage has a circular cross-section.  There is a long, shallow fillet or sub fin between fuselage and fin, both straight edged, and both rudder and fin extend well below the lower fuselage line to form a tail bumper. The rudder is [[balanced rudder|horn balanced]]. Later revisions slightly increased rudder area. The high aspect ratio, straight tapered tailplane is mounted above the fuselage on the fillet forward of the fin's leading edge.  The [[elevator (aircraft)|elevators]] are also aerodynamically balanced and originally carried a ground adjustable [[trim tab]], later removed.<ref name=SimonsII/><ref name=JAWA60/>

==Operational history==

The Meteor was competitively long lived; its first WGC was in 1956 and its last in 1968.  During that time glider construction moved decisively from wood to composites, but the [[glider competition classes|open class]] metal Meteor was not out-classed.<ref name=SimonsII/>  It was fourth in 1956<ref name=SimonsII/> and 1958,<ref name=JAWA60/> fifth in 1965 and competed again in 1968,<ref name=SimonsII/> though only placed 27th.<ref name=S&G>{{cite journal |last= |first= |authorlink= |date=August–September 1968  |title=Final results WGC 1968|journal=[[Sailplane & Gliding]]|volume=19 |issue=4 |pages=294 |id= |url= http://www.lakesgc.co.uk/mainwebpages/Sailplane%20&%20Gliding%201961%20to%201970/Volume%2019%20No%204%20Aug-Sept%201968.pdf |accessdate= |quote= }}</ref> In 1958 the Meteor established two new international closed circuit speed records over 100&nbsp;km and 300&nbsp;km.<ref name=SimonsII/>

Meteor 57 appears on the [[Croatia]]n civil register in 2010.<ref name=EuReg>{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0|page=526}}</ref>

Meteor 60 ''YU-4103'' is under reconstruction at the [[Museum of Aviation (Belgrade)]] in [[Serbia]] but is not on public display.<ref name=Ogden>{{cite book |title=Aviation Museums and Collections of Mainland Europe |last= Ogden |first=Bob |edition= |year=2009|publisher= Air Britain (Historians) Ltd|location= |isbn=978 0 85130 418 2}}</ref>

==Variants==
;Meteor: Original 1955 version.
;Meteor 57: 1957 version.
;Meteor 60: 1960 version: refined canopy-fuselage junction and rear fuselage.

==Specifications (Meteor 60) ==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1960-61<ref name=JAWA60/> The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde<ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=8.05
|length note=
|span m=20.0
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=16.0
|wing area note=
|aspect ratio=25.0
|airfoil=[[NACA airfoil|NACA 63<sub>2</sub>-616.5]]
|empty weight kg=376
|empty weight note=
|gross weight kg=505
|gross weight note=
|max takeoff weight kg=505
|max takeoff weight lb=
|max takeoff weight note=

|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=67
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=250
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=<br/>
*'''Rough air speed max:''' {{convert|125|km/h|mph kn|abbr=on|0}}
*'''Aerotow speed:''' {{convert|150|km/h|mph kn|abbr=on|0}}
*'''Winch launch speed:''' {{convert|90|km/h|mph kn|abbr=on|0}}
*'''Terminal velocity:''' with full air-brakes at max all-up weight {{convert|230|km/h|mph kn|abbr=on|0}}
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+4 +1 at  {{convert|252|km/h|mph kn|abbr=on|0}} +5 at  {{convert|153|km/h|mph kn|abbr=on|0}} -2.5 at  {{convert|144|km/h|mph kn|abbr=on|0}}
|roll rate=<!-- aerobatic -->
|glide ratio=40:1 at {{convert|90|km/h|mph kn|abbr=on|0}}
|sink rate ms=0.60
|sink rate note= best
|lift to drag=
|wing loading kg/m2=31.5
|wing loading note=
|more performance=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=[[Slingsby Skylark 3]]
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==

*{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}
*{{cite book |title=Aviation Museums and Collections of Mainland Europe |last= Ogden |first=Bob |edition= |year=2009|publisher= Air Britain (Historians) Ltd|location= |isbn=978 0 85130 418 2}}
*{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0|page=526}}
*{{cite book |title=Sailplanes 1945-1965 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9807977 4 0|pages=182}}
*{{cite book |title= Jane's All the World's Aircraft 1960-61|last= Taylor |first= John W R |edition= |year=1960|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|page=411 }}
*{{cite journal |last= |first= |authorlink= |date=August–September 1968  |title=Final results WGC 1968|journal=[[Sailplane & Gliding]]|volume=19 |issue=4 |pages=294 |id= |url= http://www.lakesgc.co.uk/mainwebpages/Sailplane%20&%20Gliding%201961%20to%201970/Volume%2019%20No%204%20Aug-Sept%201968.pdf |accessdate= |quote= }}

<!-- ==Further reading== -->

==External links==
{{commons category|Ikarus aircraft}}
*[http://www.lakesgc.co.uk/mainwebpages/Sailplane%20&%20Gliding%201961%20to%201970/Volume%2019%20No%204%20Aug-Sept%201968.pdf Sailplane & Gliding]

<!-- Navboxes go here -->
{{Ikarus aircraft}}

[[Category:Yugoslavian sailplanes 1950–1959]]