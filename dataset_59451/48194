{{Infobox themed area
| image       = Gold Coast Parklands sign.JPG
| image_width = 220
| caption     = Original Gold Coast Parklands sign removed from venue in 2013 prior to demolition of complex.
| opened      = {{Start date|1988|12|07}}
| closed      = 2013
}}
[[File:Gold Coast Light Rail - Broadwater Parklands Station.jpg|thumb|Gold Coast Light Rail - Broadwater Parklands Station]]
The '''Gold Coast Parklands''' was a complex serving the grey hound and harness racing industries on the [[Gold Coast, Queensland]], Australia.<ref>Parklands Gold Coast</ref> It opened on 7 December 1988 on the corner of Smith Street and Parklands Drive, Southport on the Gold Coast, Queensland at a cost of nine million dollars.<ref>{{cite news | newspaper = The Gold Coast Bulletin | location = Southport | page = 56 | date = 25–26 August 2007}}</ref>

==History==
Plans to build the complex began in 1983 when the Racing Minister for Queensland, Mr Russell Hinze, visited the Gold Coast.  He was reportedly disappointed by trotting and grey hound racing facilities at Owen Park, Southport.<ref>{{Citation | last = Gleeson | first = Peter | title = Politicians paw over Parklands showgrounds | newspaper = The Gold Coast Bulletin | page = 58 | date = 25 August 2007}}</ref> The decision was made to provide improved facilities at another location and State Government land and was selected opposite [[Griffith University]].
 
The complex was managed by the Parklands Trust and was the home for a number of organisations<ref>{{cite web | last = Kane | first = Charmaine | last2 = Lewis | first2 = David | title = Games athletes' village to oust community groups | publisher = Australian Broadcasting Corporation | date = 5 April 2013  | url = http://www.abc.net.au/news/2013-04-05/games-athletes-village-to-oust-community-groups/4611386 | accessdate = 3 March 2014 }}</ref> including the [[Gold Coast Show Society]],<ref>{{cite web|author=February 21st, 2013 by admin. |url=http://www.goldcoastshow.com.au |title=Home 2014 |publisher=Gold Coast Show |date=2013-02-21 |accessdate=2014-03-31}}</ref> [[Gold Coast Greyhound Racing]]<ref>{{Citation | last = Wilson | first = Terry  | title = Avid fans in mourning for final race of greyhounds : It's a dog gone shame | newspaper = The Gold Coast Bulletin | page = 89 | date = 30 July 2007 }}</ref> the [[Parklands Indoor Sports Centre]]<ref>{{cite web|url=http://www.parklandsindoor.com/index.htm |title=Parklands Indoor Sports Centre |publisher=Parklandsindoor.com |date= |accessdate=2014-03-31}}</ref> and the [[Gold Coast Harness Racing Club]].<ref>{{cite web|url=http://www.harnessracingqueensland.com/clubs/gold-coast.aspx |title=Harness Racing QLD |publisher=Harnessracingqueensland.com |date=2014-02-13 |accessdate=2014-03-31}}</ref><ref>{{cite news | last = Roots | first = Chris | title = Parklands closure offers a new dawn for Coast | newspaper = The Sun Herald Sydney | location = Sydney | page = 51 | date = 29 September 2013}}</ref><ref>{{Citation | last = Gleeson | first = Peter | title = Greys dogged by rough trot | newspaper = The Gold Coast Bulletin | page = 47 | date = 3 June 2008}}</ref>
Between 1993 and 2013 it was also the venue for the annual [[Big Day Out]] concert.<ref>{{cite news | newspaper = The Gold Coast Bulletin | location = Southport | page = 13 | date = 10 August 2005}}</ref><ref>{{Citation  | title = Parklands fears losing its biggest earner | newspaper = The Gold Coast Bulletin | date = 24 August 2008}}</ref>
In 2007, The State Government announced plans to build a new hospital adjacent to the complex.<ref>{{cite news | newspaper = The Gold Coast Bulletin | location = Southport | page = 56 | date = 25–26 August 2007}}</ref>
This resulted in the departure of [[Gold Coast Greyhound Racing]] from Parklands in 2009.<ref>{{Citation | last = Hickson | first = Kate  | title = Anger as greyhound track goes to the dogs | newspaper = The Gold Coast Sun | page = 9 | date = 22 September 2010}}</ref><ref>{{Citation | last = Cameron | first = Peter | title = Patter of greyhounds may yet be heard at Parklands | newspaper = The Gold Coast Bulletin | page = 19 | date = 29 March 2011}}</ref>
On 1 February 2013 the remaining part of the site was formally designated as the athletes' village for the [[2018 Commonwealth Games]]<ref>{{cite web | title = Planning commences for Commonwealth Games Village | publisher = The Gold Coast 2018 Commonwealth Games Corporation (GOLDOC) | date = 1 February 2013 | url = http://www.gc2018.com/?name=Planning-commences-for-Commonwealth-Games-Village | accessdate = 3 March 2014 }}</ref><ref>{{cite news | last = Hurst | first = Daniel | title = Games tick: new board, same decision | newspaper = The Brisbane Times | location = Brisbane | publisher = Fairfax Media | date = 29 May 2012 | url = http://www.brisbanetimes.com.au/queensland/games-tick-new-board-same-decision-20120529-1zg60.html | accessdate = 3 March 2014}}</ref> and was declared a Priority Development Area (PDA) by the [[Government of Queensland]].<ref>[http://www.dsdip.qld.gov.au/commonwealth-games-village/economic-development-queensland/commonwealth-games-village.html Commonwealth Games Village]</ref>
In August 2013 a call for stories, memories and photographs from the community to commemorate the history of Parklands was launched.<ref>[http://www.mygc.com.au/article/entertainment/entertainment-news/38966-help-save-25-years-of-parklands-memories.php Help Save 25 Years of Parklands Memories]</ref><ref>[https://www.facebook.com/pages/Parklands-Gold-Coast/295067050507175 Parklands Gold Coast]</ref> These stories and ephemera relating to the complex were donated to the City of Gold Coast Local Studies Library.

Demolition of the buildings on the site commenced in late 2013 and in December 2013 [[Grocon]] won the contract to build the village.<ref>{{Citation | last = Wilmot | first = Ben | title = Grocon wins contract to build Games village | newspaper = The Australian  | date = 17 December 2013 | url = http://www.theaustralian.com.au/business/property/grocon-wins-contract-to-build-games-village/story-fnko7zi0-1226784457864# | accessdate = 6 March 2014}}</ref>

==See also==
{{Portal|Gold Coast|Queensland}}
* [[Sports on the Gold Coast, Queensland]]

==References==
{{reflist}}

==Bibliography==
* {{Citation | newspaper = The Gold Coast Bulletin | page = 56 | date = 25–26 August 2007 }}
* {{cite web | last = Kane | first = Charmaine | last2 = Lewis | first2 = David | title = Games athletes' village to oust community groups | publisher = Australian Broadcasting Corporation | date = 5 April 2013  | url = http://www.abc.net.au/news/2013-04-05/games-athletes-village-to-oust-community-groups/4611386 | accessdate = 3 March 2014}}
* {{cite news | newspaper = The Gold Coast Bulletin | location = Southport | page = 13 | date = 10 August 2005}}
* {{cite news | last = Hurst | first = Daniel | title = Games tick: new board, same decision | newspaper = The Brisbane Times | location = Brisbane | publisher = Fairfax Media | date = 29 May 2012 | url = http://www.brisbanetimes.com.au/queensland/games-tick-new-board-same-decision-20120529-1zg60.html | accessdate = 3 March 2014}}
* {{Citation | last = Staun | first = Fiona | title = 100 years : history of the Gold Coast Show | publisher = Gold Coast Show Society | year = 2006 }}
* {{Citation | title = Parklands still a groundswell after 24 years years of memories showstopper | newspaper = The Gold Coast Bulletin | page = 49 | date = 31 August 2013 }}
* {{Citation | last = Telforo | first = Simon  | title = Rodders Paradise | journal = Street Machine | volume = 33 | issue = 6 | pages = 96–102 | date = 1 June 2013}}
* {{Citation | last = Redmond | first = Renee  | title = Hospital pass is end of an era : Howls of sadness at greyhound's clubs last night | newspaper = The Gold Coast Bulletin | page = 42 | date = 31 July 2008 }}
* {{Citation | last = Gleeson | first = Peter | title = Big Day Out, showgrounds and harness racing track to stay put : Parklands keeps rocking | newspaper = The Gold Coast Bulletin | date = 27 August 2007 }}
* {{Citation | last = Grant | first = Dwayne | title = No winners as site sacrificed : harness racing fraternity bids solemn farewell to Parklands track  | newspaper = The Gold Coast Bulletin | page = 6 | date = 27 September 2013}}

==External links==
* [http://www.harnessracingqueensland.com/clubs/gold-coast.aspx Harness Racing Gold Coast]
* [http://www.goldcoastshow.com.au Gold Coast Show]
* [http://www.gc2018.com Gold Coast 2018 Commonwealth Games]
* [http://www.bigdayout.com/goldcoast Gold Coast Big Day Out]
* [http://www.dsdip.qld.gov.au/commonwealth-games-village/economic-development-queensland/commonwealth-games-village.html Commonwealth Games Village]
* [http://www.grocon.com/ Grocon]

{{coord missing|Queensland}}

[[Category:Demolished buildings and structures in Australia]]
[[Category:Defunct greyhound racing venues in Australia]]
[[Category:Harness racing in Australia]]
[[Category:Harness racing venues in Australia]]
[[Category:Sports venues completed in 1988]]
[[Category:Sports venues demolished in 2013]]
[[Category:Sports venues on the Gold Coast, Queensland]]
[[Category:1988 establishments in Australia]]