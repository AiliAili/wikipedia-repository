<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Lom-61 Favorit
 | image=Lommatzsch Favorit.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=High performance single seat [[glider (sailplane)|glider]]
 | national origin=[[German Democratic Republic]]
 | manufacturer=[[Volkseigener Betrieb|VEB]] Apparatebau [[Lommatzsch]]
 | designer=Hans Wegerich, Hans Hartung and Wolfgang Heintze
 | first flight=c.1961
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=5
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Lommatzsch Lom-61 Favorit''' is a single seat, high performance [[glider (sailplane)|glider]] designed and built in the [[German Democratic Republic]] in the early 1960s. A small number were used by clubs.

==Design and development==
The [[Lommatzsch]] factory of the Volkseigener Betrieb Flugzeug (Association of the People's Aircraft Enterprises) was responsible for glider design and production in the [[German Democratic Republic|DDR]].<ref name=JAWA60/>  The Lom-61 was principally designed by Hans Wegerich, Hans Hartung and Wolfgang Heintze.

The Favorit has a [[monoplane#Types of monoplane|high wing]] with a [[NACA airfoil#6-series|6-series NACA laminar flow airfoil]], straight tapered in plan.  This was built in two pieces around a single spar at 40% [[chord (aircraft)|chord]] with pine and honeycomb supported [[plywood]] skin, linked by a central section integral with the [[fuselage]]. There are [[Schempp-Hirth]] [[air brake (aircraft)|airbrakes]] mounted further aft than usual, at 70% chord. The [[ailerons]] are gapless and split into pairs on either side.<ref name=Schmidt/>

The wooden fuselage of the Favorit is slender, with a maximum cross section of 0.38 m², and tapers rearwards only gently.  The tail surfaces are straight tapered, with marked sweep on the [[fin]] but none on the [[rudder]], which extends to the keel.  The horizontal surfaces are mounted on top of the fuselage, forward enough for the rudder hinge to be ahead of the [[elevator (aircraft)|elevator]] [[trailing edge]].  Forward of the wing, the cockpit is under a long, low, single piece [[canopy (aircraft)|canopy]]. The Favorit lands on a fixed [[Landing gear#Gliders|monowheel]], partially enclosed in the fuselage, rubber sprung and fitted with brakes, assisted by a rubber sprung nose skid.<ref name=Schmidt/>

The Lom-61 first flew, as its name suggests, around 1961.  Though variants for 1962-4 were discussed, they were not built.

==Operational history==
Only five Favorits were built, flying competitively in the DDR until 1980. One set a national record of 107&nbsp;km/h&nbsp;(67&nbsp;mph) over a 100&nbsp;k&nbsp;(62&nbsp;mi) triangle; another was the first to fly a German 50&nbsp;km&nbsp;(310&nbsp;mi) triangle.

<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Aircraft on display==
[[German sailplane museum]], [[Wasserkuppe]]:''DM-2704''

==Specifications==
{{Aircraft specs
|ref=Segelflugzeuge<ref name=Schmidt/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=6.74
|length note=
|span m=15.00
|height m=2.00
|height note=
|wing area sqm=12.40
|wing area note=
|aspect ratio=18.15
|airfoil=[[NACA]] 65<sub>2</sub>-615.5
|empty weight kg=225
|empty weight note=
|gross weight kg=335
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=20
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=38:1 at 95 km/h (59 mph; 51 kn)
|sink rate ms=0.60
|sink rate note=minimum, at 76 km/h (47 mph; 41 kn)
|lift to drag=
|wing loading kg/m2=27.0
|wing loading note=
|more performance=
*'''Landing speed:''' 70 km/h (44 mph; 38 kn)

}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|refs=

<ref name=Schmidt>{{cite book |title= Aerotyp Segelflugzeuge|last= Schmidt |first=A.F. |year=1969|publisher=Transpress VEB|location=Berlin|isbn=|pages=38–9 }}</ref>

<ref name=JAWA60>{{cite book |title= Jane's All the World's Aircraft 1960-61|last= Taylor |first= John W R |coauthors= |edition= |year=1960|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|pages=173–4 }}</ref>

}}
<!-- ==Further reading== -->
<!--==External links==
*[http://www.ae.illinois.edu/m-selig/ads/afplots/naca612615.gif NACA  airfoil]-->
<!-- Navboxes go here -->

[[Category:Aircraft manufactured in East Germany]]