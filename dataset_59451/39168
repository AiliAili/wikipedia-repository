{{good article}}
{{Infobox road
|state=CA
|type=SR
|route=177
|alternate_name=Desert Center Rice Road
|section=477
|maint=[[California Department of Transportation|Caltrans]]
|map_notes=SR 177 highlighted in red
|length_mi=27.024
|length_round=3
|length_ref=<ref name=trucklist/>
|direction_a=South
|terminus_a={{jct|state=CA|I|10}} near [[Desert Center, California|Desert Center]]
|direction_b=North
|terminus_b={{jct|state=CA|SR|62}} near [[Rice, California|Rice]]
|previous_type=SR
|previous_route=175
|next_type=SR
|next_route=178
|counties=[[Riverside County, California|Riverside]]
}}
'''State Route 177''' ('''SR 177''') is a short [[state highway (US)|state highway]] in the [[U.S. state]] of [[California]] in [[Riverside County, California|Riverside County]]. The route links [[Interstate 10 (California)|Interstate 10]] (I-10) midway between the [[Coachella Valley]] and [[Blythe, California|Blythe]] on the [[California]]–[[Arizona]] border, to [[State Route 62 (California)|SR 62]] near [[Rice, California|Rice]]. SR 177 travels along the eastern portion of the [[Joshua Tree National Park]]; like the eastern {{convert|100|mi|km}} of SR 62, it passes through some of the most desolate areas of the [[Mojave Desert]]. The southernmost portion of SR 177 near I-10 runs concurrently with [[County Route R2 (California)|County Route R2]] (CR R2).

==Route description==
SR 177 begins at [[I-10 (CA)|I-10]] near [[Desert Center, California|Desert Center]] and briefly travels north, intersecting with [[CR R2 (CA)|CR R2]]. At this intersection, SR 177 turns northeast and travels across the [[Mojave Desert]], through the [[Chuckwalla Valley]]. It passes near the [[Desert Center Airport]] and comes near [[Palen Lake]], a dry lake. The highway passes along the southeastern boundary of [[Joshua Tree National Park]] before turning due north and crossing the desert for several miles. SR 177 briefly turns northeast again before intersecting with [[SR 62 (CA)|SR 62]] and defaulting onto SR 62 eastbound.<ref name="sbdtg">{{cite map|year=2008|title=Riverside County Road Atlas|publisher=Thomas Brothers}}</ref>

The route is two lanes wide for its entire length.<ref name=tcr>{{cite web|title=SR&nbsp;177 Transportation Concept Report|url=http://dot.ca.gov/hq/tpp/corridor-mobility/D8_docs/TCRs/sr-177.pdf|publisher=California Department of Transportation|accessdate=December 21, 2014|author=Staff|format=PDF|year=1998|pages=3, 5}}</ref> SR 177 is not part of the [[National Highway System (United States)|National Highway System]],<ref name=fhwa-nhs>{{FHWA NHS map|region=californiasouth}}</ref> a network of highways that are essential to the country's economy, defense, and mobility.<ref name=NHS-FHWA>{{FHWA NHS}}</ref> In 2013, SR&nbsp;177 had an [[annual average daily traffic]] (AADT) of 1,200 at the northern terminus with SR 62, and 3,700 at the southern terminus with I-10, the latter of which was the highest AADT for the highway.<ref name=traffic>{{Caltrans traffic|year=2013|start=164|end=178}}</ref>

==History==
The [[Metropolitan Water District]] built a road from Desert Center that ran north before turning east to [[Earp, California|Earp]] along with portions of the [[California Aqueduct]] from February 20 to August 4 in 1933. This was part of a road system that was built in order to support the construction of the aqueduct by connecting the camps that construction workers resided in during the project. This road cost $389,600 (about ${{Formatprice|{{Inflation|US-NGDPPC|390600|1933|r=-6}}}} in {{inflation-year|US-NGDPPC}} dollars){{Inflation-fn|US-NGDPPC}} to construct; the roads in the system were paved. <ref>{{cite book | url=http://www.mwdh2o.com/mwdh2o/pages/about/AR/AR1928/Construction/Chapter-7.pdf | title=History & First Annual Report, Commemorative Edition | publisher=Metropolitan Water District | author=Staff | year=1939 | pages=141–145}}</ref> SR 177 was added to the state highway system in 1972 by the [[California State Legislature]].<ref>{{cite CAstat|year=1972|ch=1216}}</ref> As of 1998, Caltrans had no plans to expand the highway, considering it to be "maintain only" through 2015.<ref name="tcr" />

==Major intersections==
{{CAinttop|post-1964=yes|county=Riverside|post_ref=<br><ref name=bridgelog>{{Caltrans bridgelog}}</ref><ref name=traffic /><ref name=trucklist />
}}
{{CAint
|location=none
|postmile=0.00
|road={{Jct|state=CA|I|10|city1=Blythe|city2=Indio}}
|notes=Interchange; south end of SR 177; I-10 exit 192
}}
{{CAint
|location=none
|postmile=0.26
|road=Kaiser Road ([[County Route R2 (California)|CR R2]]) – [[Kaiser Mine, California|Kaiser Mine]]
|notes=
}}
{{CAint
|location=none
|postmile=27.02
|road={{Jct|state=CA|SR|62|name1=[[Twentynine Palms Highway]], Desert Center Rice Road|city1=Rice|city2=Twentynine Palms}}
|notes=North end of SR 177
}}
{{Jctbtm}}

==See also==
*{{portal-inline|California Roads}}

==References==
{{reflist|30em}}

==External links==
{{Attached KML|display=inline,title}}
{{commonscat}}
*[http://www.aaroads.com/california/ca-177.html California @ AARoads.com - State Route 177]
*[http://www.dot.ca.gov/hq/roadinfo/sr177 Caltrans: Route 177 highway conditions]
*[http://www.cahighways.org/177-184.html#177 California Highways: Route 177]

[[Category:State highways in California|177]]
[[Category:Roads in Riverside County, California|State Route 177]]