{{Notability|date=June 2014}}
{{NPOV|date=June 2014}}

'''''FLUIDEX''''' is an online [[bibliographic database]] that covers fluids behavior, applications, and engineering in [[civil engineering]] and [[process engineering]]. It is published by Elsevier.

Indexing includes more than 900,000 past and current records pertaining to relevant [[trade magazine]]s and [[scientific journal]]s from 1966–present. Further topical coverage includes research and technology in [[fluid dynamics]], [[separation processes]], [[offshore platform]] issues, and new [[hydraulic]] and [[pneumatic]] equipment applications and operations. Abstracts summarize 98% of all documents.

Global literature coverage includes over 400  trade and peer reviewed publications. Additionally, archives contain several hundred cataloged trade, peer reviewed, and book titiles. The database is annually updated with more than 40,000 [[Abstract (summary)|abstracts]] and [[citations]]. [[Ovid Technologies|OVID]]  and [[Dialog (online database)|Dialog]] are online access points. This database is also published in a printed paper format. Hence, the print counterparts are "Fluid Abstracts: Process Engineering" ({{issn|0962-7162}}), and "Fluid Abstracts: Civil engineering" ({{issn|0962-7170}}).<ref name=lewis>
{{cite journal
 | last = Lewis
 | first = Sandy
 | last2 =Bob
 | first2 = Heyer-Gray
 | last3 = Shafer
 | first3 = Sharon
 | title =Fluidex: The Fluid Engineering Abstracts Database
 | journal =Issues in Science and Technology Librarianship
 | volume = 27
 | issue = Database Reviews & Reports
 | pages =
 | publisher =[[Association of College and Research Libraries]] (ACRL). [[American Library Association]]
 | location =
 | date =Summer 2000
 | language =
 | url =http://www.istl.org/00-summer/databases.html
 | jstor =
 | issn =
 | doi =
 | accessdate =2013-08-26}}</ref><ref name=ovid>
{{cite web
 | last =[[Ovid Technologies|OVID]]
 | first =
 | title =FLUIDEX Engineering Codes
 | publisher = [[Wolters Kluwer]]
 | url =http://resourcecenter.ovid.com/site/products/fieldguide/elfx/FLUIDEX_Engineering_Codes.jsp
 | format = web page
 | accessdate =2013-08-26 
 | archiveurl =http://resourcecenter.ovid.com/site/products/fieldguide/elfx/FLUIDEX_Engineering_Codes.jsp 
 | archivedate =2013-08-26 }}</ref><ref name=fluidex>
{{cite web
 | last = Database description
 | first =
 | title =FLUIDEX  
 | publisher = [[Engineering Information, Inc.]]. [[Elsevier]].   
 | url =http://www.elsevier.com/bibliographic-databases/fluidex
 | format =web page
 | accessdate =2013-08-26 
 | archiveurl =http://www.elsevier.com/bibliographic-databases/fluidex 
 | archivedate =2013-08-26 }}</ref><ref name=dialog>
{{cite web
 | last = Database description
 | first =
 | title =FLUIDEX (Fluid Engineering Abstracts) 
 | publisher =[[Dialog (online database)|Dialog Bluesheets]] 
 | url =http://library.dialog.com/bluesheets/html/bl0096.html
 | format =web page
 | accessdate =2013-08-26 
 | archiveurl =http://library.dialog.com/bluesheets/html/bl0096.html
 | archivedate =2013-08-26}}</ref>

==References==
{{reflist}}

==External links==
* [http://www.elsevier.com/journals/fluid-abstracts-process-engineering/0962-7162 Fluid Abstracts: Process Engineering]. Elsevier. 2013.
* [http://www.elsevier.com/journals/fluid-abstracts-civil-engineering/0962-7170 Fluid Abstracts: Civil Engineering]. Elsevier. 2013.
* [http://www.elsevier.com/elsevier-products/fluidex FLUIDEX]. Elsevier
* [https://www.facebook.com/Fluidex FACEBOOK]. Facebook


[[Category:Bibliographic databases in engineering]]
[[Category:Elsevier]]
[[Category:Online databases]]


{{database-stub}}