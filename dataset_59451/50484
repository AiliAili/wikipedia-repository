{{Infobox organization
| name          = American Society of Ichthyologists and Herpetologists
| image         =
| size          = <!-- default 200px -->
| alt           = <!-- alt text; see [[WP:ALT]] -->
| caption       =
| formation     = {{Start date|1915}}
| founder       = [[John Treadwell Nichols]]
| type          = [[learned society]]
| headquarters  = [[Lawrence, Kansas]]
| location      =
| coords        = <!-- Coordinates of location using {{Coord}} -->
| leader_title  = President
| leader_name   = {{plainlist|
* H. Bradley Shaffer<ref name="officers">http://www.asih.org/about/officiers</ref>
* Larry G. Allen (elect)<ref name="officers" />
}}
| leader_title2 = Treasurer
| leader_name2  = F. Douglas Martin
| leader_title3 = Secretary
| leader_name3  = Maureen A. Donnelly
| website       = {{url|www.asih.org}}
}}

The '''American Society of Ichthyologists and Herpetologists''' (ASIH) is an international [[learned society]] devoted to the scientific studies of [[ichthyology]] (study of fish) and [[herpetology]] (study of [[reptile]]s and [[amphibian]]s). The primary emphases of the society are to increase knowledge about these organisms, to communicate that knowledge through publications, conferences, and other methods, and to encourage and support young scientists who will make future advances in these fields. The programs of the American Society of Ichthyologists and Herpetologists are part of a global effort to interpret, understand, and conserve the Earth's natural diversity and to contribute to the wise use of natural resources for the long-term benefit of humankind.<ref>{{cite web|author=American Society of Ichthyologists and Herpetologists |title=Mission Statement |work=About ASIH |url=http://www.asih.org/info/endowpam.html |accessdate=May 27, 2006 |archiveurl=https://web.archive.org/web/20060501062435/http://www.asih.org/info/endowpam.html |archivedate=May 1, 2006 |deadurl=no |df= }}</ref>

==History==
On December 27, 1913, [[John Treadwell Nichols]] published the first issue of ''[[Copeia]]'', a [[scientific journal]] dedicated to the knowledge of fish, [[reptiles]], and [[amphibians]]. Nichols named ''Copeia'' to commemorate [[Edward Drinker Cope]], a prominent 19th-century ichthyologist and herpetologist. The first edition of ''Copeia'' was four pages in length and comprised five articles.

In an effort to increase the publication of ''Copeia'' and communication among ichthyologists and herpetologists, Nichols met with [[Henry Weed Fowler]] and [[Dwight Franklin]] in New York City. Together, the three men founded the American Society of Ichthyologists and Herpetologists; however, this achievement is often given to Nichols exclusively.

By 1923, the Society accommodated around 50 members. Furthermore, the length of ''Copeia'' extended to 120 pages and an editorial staff established by the society assumed responsibility for the mass publication and expansion of this quarterly journal. Presently, the society has more than 2,400 members and ''Copeia'' features 1,200 pages of informative content and is found in over 1,000 institutional libraries..<ref>{{cite web|author=American Society of Ichthyologists and Herpetologists |title=Beginnings |work=About ASIH |url=http://www.asih.org/info/endowpam.html |accessdate=May 27, 2006 |archiveurl=https://web.archive.org/web/20060501062435/http://www.asih.org/info/endowpam.html |archivedate=May 1, 2006 |deadurl=no |df= }}</ref>

<!-- Deleted image removed: [[File:ASIH 1932.jpg|300px|thumb|The American Society of Ichthyologists and Herpetologists in 1932: [[M. Graham Netting]], Secretary and future President, is in the center of the first row, the shorter man in a dark suit.]] -->

==Affiliated organizations==
{{clarify|date=June 2012}}
{{columns-list|colwidth=30em|
* American Elasmobranch Society
* American Fisheries Society
* American Society of Ichthyologists and Herpetologists
* ''Asociación Herpetologica Espanola''
* [[Association of Systematics Collections]]
* Canadian Association of Aquarium Clubs
* European Snake Society
* Herpetologists' League
* [[International Union for Conservation of Nature]]
* Kansas Herpetological Society
* Native Fish Conservancy
* Neotropical Ichthyological Association
* [[North American Native Fishes Association]]
* [[Integrative and Comparative Biology]]
* Society for Northwestern Vertebrate Biology
* Society for the Study of Amphibians and Reptiles
* [[Society for the Preservation of Natural History Collections]]
* Southeastern Fishes Council
* Southwestern Association of Naturalists
}}

==References==
{{Reflist}}

==External links==
*[http://www.asih.org Official website]
*[http://siarchives.si.edu/collections/siris_arc_255813 American Society of Ichthyologists and Herpetologists, Records, c. 1978-1983] from the [[Smithsonian Institution Archives]]

[[Category:Ichthyology organizations]]
[[Category:Herpetology organizations]]
[[Category:Organizations established in 1915]]
[[Category:Scientific organizations based in the United States]]
[[Category:Natural Science Collections Alliance members]]