{{Infobox terrorist attack
|title=Abole oil field raid
|image=Ethiopia-Somali.png
|caption=Map of Ethiopia highlighting the Somali region.
|location=[[Abole]], [[Somali Region]], Ethiopia
|target=Abole oil field
|date=April 24, 2007
|time=6:00&nbsp;am<ref name=ChineseEmbassy>{{cite web |url= http://www.china-embassy.org/eng/xw/t314025.htm|title= China strongly condemns attack on Chinese oil company site in Ethiopia|publisher=Embassy of People's Republic of China in the United States of America|date=2007-04-25|accessdate=2007-04-25}}</ref>
|timezone=[[UTC]]+3
|fatalities=65 Ethiopian workers<br>9 Chinese workers<br>7 Chinese workers taken hostage.
|injuries=Unknown
|perps=[[Image:Flag of Ogaden National Liberation Front.svg|20px]] [[Ogaden National Liberation Front]]
}}
The '''Abole oil field raid''' occurred in the early morning of April 24, 2007, when gunmen of the [[Ogaden National Liberation Front]] (ONLF) attacked a [[People's Republic of China|Chinese]] oil company's premises in the town of [[Abole]], {{convert|30|km|mi|abbr=on}} northwest of [[Degehabur]], in the [[Somali Region]] of [[Ethiopia]]. 74 Ethiopian soldiers, including nine Chinese workers working for the Zhongyuan Petroleum Exploration Bureau under the [[China Petroleum & Chemical Corporation]] (Sinopec), were killed.<ref>{{cite news|url=http://www.mfa.gov.et/Press_Section/Newsletter.php?Page=Press_Statements/Letter_BBC_May_04_2007.htm |title=A letter sent to BBC by Ministry of Foreign Affairs of FDRE|publisher=Ministry of Foreign Affairs of Ethiopia|date=2007-04-30|accessdate=2007-06-19| archiveurl= https://web.archive.org/web/20070630020646/http://www.mfa.gov.et/Press_Section/Newsletter.php?Page=Press_Statements/Letter_BBC_May_04_2007.htm| archivedate= 30 June 2007 <!--DASHBot-->| deadurl= no}}</ref><ref name="BBC News">{{cite news|url=http://news.bbc.co.uk/2/hi/africa/6588055.stm|title=Scores die in Ethiopia oil attack |publisher=[[BBC News]]|date=2007-04-24|accessdate=2007-04-25| archiveurl= https://web.archive.org/web/20070428132331/http://news.bbc.co.uk/2/hi/africa/6588055.stm| archivedate= 28 April 2007 <!--DASHBot-->| deadurl= no}}</ref><ref>{{cite news|url=http://news.xinhuanet.com/english/2007-04/24/content_6022126.htm|title=9 Chinese workers killed by gunmen in Ethiopia's Somali state|publisher=[[Xinhua News Agency]]|date=2007-04-24|accessdate=2007-04-24| archiveurl= https://web.archive.org/web/20070511035646/http://news.xinhuanet.com/english/2007-04/24/content_6022126.htm| archivedate= 11 May 2007 <!--DASHBot-->| deadurl= no}}</ref> 

The attack shocked the public, and many became worried it might push away foreign investors from coming to the country. Most of the dead were soldiers but it included some members of the Ethiopian security officials. This was one of the largest single attack committed by ONLF.<ref name="Garowe Online 1">{{cite news|url=http://www.garoweonline.com/artman2/publish/Africa_22/Ethiopia.shtml|title= Ethiopia:Oil companies suspend operations |publisher=[[Garowe Online]]|date=2007-04-28|accessdate=2007-04-28}}</ref>

The Abole attack came just as Ethiopian forces in [[Mogadishu]] were involved in [[Battle of Mogadishu (March – April 2007)|fierce fighting]] with Somali insurgents.<ref name=HRW1>{{cite web |url= http://hrw.org/reports/2008/ethiopia0608/9.htm#_Toc200167135|title= Collective Punishment: War Crimes and Crimes against Humanity in the Ogaden area of Ethiopia's Somali Regional State|publisher=[[Human Rights Watch]]|date=June 2008|accessdate=2008-10-01}}</ref>

== See also ==
* [[Ogaden Basin]]
* [[Insurgency in Ogaden]]
* [[Ogaden War]]

==References==
{{Reflist|2}}

== External links ==
*{{cite web |url= http://english.aljazeera.net/news/africa/2007/04/200852514432951375.html|title= China oilfield in Ethiopia attacked|accessdate=2007-04-25 |date=2007-04-24|work=Aljazeera.net|publisher=[[Al Jazeera English]] }}
*{{cite web|url=http://www.onlf.org/pressAug062006.htm |title=ONLF Statement On Military Operatio Against Illegal Oil Facility In Ogaden |accessdate=2007-04-25 |date=2007-04-24 |work=Onlf.org |publisher=[[Ogaden National Liberation Front]] |archiveurl=https://web.archive.org/web/20070429194251/http://www.onlf.org/pressAug062006.htm |archivedate=2007-04-29 |deadurl=no |df= }}

{{DEFAULTSORT:Abole Oil Field Raid}}
[[Category:Insurgency in Ogaden]]
[[Category:2007 in Ethiopia]]
[[Category:April 2007 events]]