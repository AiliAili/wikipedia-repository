{{Infobox scientist
| name              =  Robert L. "Rob" Last
| image             = 
| image_size        = 
| alt               = 
| caption           = 
| birth_date        = <!-- {{birth year and age|}} {{Birth date and age|YYYY|MM|DD}} -->
| birth_place       = 
| death_date        = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place       = 
| resting_place             = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline,title}} -->
| residence         = 
| citizenship       = 
| nationality       = 
| fields            = 
| workplaces        = [[Ohio Wesleyan University]]<br>[[Carnegie-Mellon University]]<br>[[Whitehead Institute for Biomedical Research]]<br>[[Cornell University]]<br>[[Michigan State University]]
| alma_mater        = 
| thesis_title      = Characterization of RNA splicing components of the Baker’s Yeast Saccharomyces cerevisiae
| thesis_url        = 
| thesis_year       = 
| doctoral_advisor  = John L. Woolford
| academic_advisors = 
| doctoral_students = 
| notable_students  = 
| known_for         = 
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences        = 
| influenced        = 
| awards            = Barnett Rosenberg Professor, Michigan State University<br>University Distinguished Faculty, Michigan State University <br>[[Fellow of the American Association for the Advancement of Science]]<br>Fellow of American Society of Plant Biologists<br>NSF Presidential Young Investigator Award
| signature         = <!--(filename only)-->
| signature_alt     = 
| website           = {{URL|https://bmb.natsci.msu.edu/faculty/robert-l-last/}} 
| footnotes         = 
| spouse            = 
}}

'''Robert L. Last''' is a plant biochemical [[Genomics|genomicist]] who studies metabolic processes that protect plants from the environment and produce products important for animal and human nutrition. His research has covered (1) production and breakdown of [[essential amino acid]]s, (2) the synthesis and protective roles of [[Vitamin C]] (ascorbic acid) and [[Vitamin E]] (tocopherols) as well as identification of mechanisms that protect [[photosystem II]] from damage, and (3) synthesis and biological functions of plant protective specialized metabolites (plant [[secondary metabolite]]s). Four central questions are: (i) how are leaf and seed amino acids levels regulated, (ii.) what mechanisms protect and repair photosystem II from stress-induced damage, (iii.) how do plants produce protective metabolites in their glandular secreting trichomes (iv.) and what are the evolutionary mechanisms that contribute to the tremendous diversity of specialized metabolites that protect plants from insects and pathogens and are used as therapeutic agents.

== Education and training ==
Last obtained a BA in Chemistry with minors in Biology in 1980 from [[Ohio Wesleyan University]]. He received his PhD in 1986 from [[Carnegie-Mellon University]] for research conducted in the Biological Sciences Department. His thesis research on the RNA genes of the Baker's yeast ''[[Saccharomyces cerevisiae]]'' was carried out under the direction of Professor John Woolford. 

== Professional experience ==
Last spent three years as an NSF Plant Biology Postdoctoral Fellow at the [[Whitehead Institute for Biomedical Research]] working with Professor Gerald R. Fink. Starting in 1989 he worked through the ranks to Scientist at the [[Boyce Thompson Institute for Plant Research]], and Adjunct Professor of Genetics and Development at Cornell University. Starting in 1998, he worked for four years at Cereon Genomics in Cambridge, MA as a founding science director. A highlight of this work was shotgun sequencing of the ''[[Arabidopsis thaliana]]'' Landsberg ''erecta'' genome.<ref name=":4">{{cite journal|last1=Jander|first1=G|last2=et. al.|title=Arabidopsis map-based cloning in the post-genome era|journal=Plant Physiology|date=2002|volume=129|page=440-450}}</ref> He served for 1.5 years as a program officer in the US National Science Foundation Plant Genome Research Program before moving to Michigan State University, where he is Barnett Rosenberg Professor, with appointments in the Departments of Plant Biology and Biochemistry and Molecular Biology. During this time he established the MSU Plant Genomics Research Experiences for Undergraduates Summer Training Program (in 2006) and serves as founding Program Director of the NIH-funded Plant Biotechnology for Health and Sustainability graduate training program. He has had sabbatical appointments at the [[Max Planck Institute for Chemical Ecology]] and the [[Weizmann Institute of Science]]. 

Last has served in a variety of editorial roles including as a founding Associate Editor of [[Science Advances]], Associate and Monitoring Editor of [[Plant Physiology (journal)|Plant Physiology]] and Editor-in Chief of The Arabidopsis Book. He was chair of the board of directors of the [[iPlant Collaborative]] (now CyVerse) during its first three years.

== Research ==
Last studies how plants produce metabolites that are important for their survival in the environment and either are essential for human health or contribute to the well-being of humans and other primary consumers of plants. His research integrates genetics, genomics, analytical chemistry, biochemistry and evolutionary biology to identify and characterize the proteins that perform these functions. Significant accomplishments related to primary metabolism in plants include identification of the first genetically-transmitted amino acid requiring mutants of plants leading to characterization of the tryptophan biosynthetic pathway,<ref>{{cite journal|last1=Last|first1=RL|last2=Fink|first2=GR|title=Tryptophan-requiring mutants of the plant ''Arabidopsis thaliana''|journal=Science|date=1988|volume=240|page=305-310}}</ref><ref>{{cite journal|last1=Radwanski|first1=ER|last2=Last|first2=RL|title=Tryptophan biosynthesis and metabolism: Biochemical and molecular genetics|journal=Plant Cell|date=1995|volume=7|page=921-934}}</ref> branched chain amino acid metabolic networks,<ref>{{cite journal|last1=Gu|first1=L|title=Metabolite profiling reveals broad metabolic phenotypes associated with a plant amino acid catabolism mutant|journal=Plant Journal|date=2010|volume=61|page=579-590}}</ref> and molecular genetic dissection of the Vitamins C and E biosynthetic pathways.<ref name=":4" /><ref>{{cite journal|last1=Van Eenennaam|first1=AL|title=Engineering improved vitamin E quality: from Arabidopsis mutant to soy oil|journal=Plant Cell|date=2003|volume=15|page=3007-3019}}</ref> Notable accomplishments related to plant environmental adaptation include characterization of plant UV-B sensing, protective and repair mechanisms,<ref>{{cite journal|last1=Li|first1=J|last2=et. al.|title=Arabidopsis flavonoid mutants are hypersensitive to UV-B irradiation|journal=Plant Cell|date=1993|volume=5|page=171-179}}</ref><ref>{{cite journal|last1=Landry|first1=LG|last2=et. al.|title=An Arabidopsis photolyase mutant is hypersensitive to ultraviolet-B radiation|journal=Proc. Natl. Acad. Sci. USA|date=1997|volume=94|page=328-332}}</ref><ref>{{cite journal|last1=Kliebenstein|first1=DJ|last2=et. al.|title=The Arabidopsis RCC1 homologue UVR8 mediates UV-B signal transduction and tolerance|journal=Plant Physiology|date=2002|volume=130|page=234-243}}</ref> PSII protection and repair,<ref>{{cite journal|last1=Lu|first1=Y|last2=et. al.|title=A small zinc finger thylakoid protein plays a role in maintenance of photosystem II|journal=Plant Cell|date=2011|volume=23|page=1861-1875}}</ref> and detailed analysis of the biosynthetic and evolutionary mechanisms that contribute to metabolic diversity in glandular secreting trichomes of cultivated tomato (''[[Solanum lycopersicum]]'') and its relatives in the [[Solanaceae]] (nightshade) family.<ref>{{cite journal|last1=Schilmiller|first1=AL|last2=Schauvinhold|first2=I|last3=et. al.|title=Monoterpenes in the glandular trichomes of tomato are synthesized via a neryl diphosphate intermediate rather than geranyl diphosphate|journal=Proc. Natl. Acad. Sci. USA|date=2009|volume=106|page=10865-70|doi=10.1073/pnas.0904113106}}</ref><ref>{{cite journal|last1=Milo|first1=R|last2=Last|first2=RL|title=Achieving diversity in the face of constraints - lessons from metabolism|journal=Science|date=2012|volume=336|page=1663-1667|doi=10.1126/science.1217665}}</ref><ref>{{cite journal|last1=Schilmiller|first1=AL|last2=et. al.|title=Identification of a BAHD acetyltransferase that produces protective acyl sugars in tomato trichomes|journal=Proc. Natl. Acad. Sci. USA|date=2012|volume=109|page=16377-16382|doi=10.1073/pnas.1207906109}}</ref><ref>{{cite journal|last1=Liu|first1=J|last2=Last|first2=RL|title=. A land plant-specific thylakoid membrane protein contributes to photosystem II maintenance in ''Arabidopsis thaliana''|journal=Plant Journal|date=2015|volume=82|page=731–743|doi=10.1111/tpj.12845}}</ref><ref>{{cite journal|last1=Fan|first1=P|last2=et. al.|title=''In vitro'' reconstruction and analysis of evolutionary variation of the tomato acylsucrose metabolic network|journal=Proc. Natl. Acad. Sci. USA|date=2016|volume=113|page=E239-E248|doi=10.1073/pnas.1517930113}}</ref>

== References ==
{{reflist|30em}}

== External links ==
*[https://plantmetabolism.natsci.msu.edu/ Plant Biotechnology for Health and Sustainability]

{{authority control}}
{{DEFAULTSORT:Last, Robert L.}}
[[Category:American biochemists]]
[[Category:Fellows of the American Association for the Advancement of Science]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]