<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Ki-11
 |image= Nakajima Ki-11.jpg
 |caption=
}}{{Infobox Aircraft Type
 |type= [[prototype]] [[fighter aircraft]]
 |manufacturer= [[Nakajima Aircraft Company]]
 |designer=
 |first flight= 1934
 |introduced=
 |retired=
 |status=
 |primary user= [[Imperial Japanese Army Air Force|IJA Air Force]]
 |more users=
 |produced=
 |number built= 4
 |variants with their own articles=
}}
|}
The {{nihongo|'''Nakajima Ki-11'''|キ11 (航空機) |Ki-jyuichi Kokūki}} was an unsuccessful attempt by [[Nakajima Aircraft Company]] to meet a 1935 requirement issued by the Japanese government for a modern single-seat [[monoplane]] [[Fighter aircraft|fighter]] suitable to meet the needs of both the [[Imperial Japanese Army Air Force]] and [[Imperial Japanese Navy Air Service]]

==Design and development==
Development of the Ki-11 began as a private venture in 1934, based on a [[Flying wires|wire]]-braced low-wing monoplane, inspired by the [[Boeing P-26 Peashooter]]. The [[fuselage]] wing center section and [[Landing gear|undercarriage]] were constructed in [[duralumin]], while the wings and tail were of wood and canvas. The aircraft was powered by a single {{convert|410|kW|hp|-1|abbr=on}} [[Nakajima Kotobuki]] Ha-1-3 [[radial engine]]. Proposed armament consisted of twin 7.7&nbsp;mm (.303&nbsp;in) [[machine gun]]s firing from between the engine cylinders.<ref>{{Harvnb|Wieliczko and Szeremeta|2004|pp=9–10.}}</ref>

The Ki-11 was entered into competition with the [[Kawasaki Ki-10]] [[biplane]] design. Although technically more advanced and faster than the [[Kawasaki Heavy Industries Aerospace Company|Kawasaki]] design, the [[Imperial Japanese Army]] command was split between supporters of "maneuverability" and supporters of "speed". The supporters of the "maneuverability" scheme won, and the Ki-10 became the main army fighter until 1937.<ref>{{Harvnb|Wieliczko and Szeremeta|2004|pp=10–11.}}</ref> Nakajima continued to refine the Ki-11 design, and it re-emerged in the form of the [[Nakajima Ki-27]] "Nate" several years later.<ref>{{Harvnb|Mikesj and Abe|1990|p=218.}}</ref>

Nakajima later sold the fourth prototype as '''AN-1 Communications Aircraft''' to the ''[[Asahi Shimbun]]'' newspaper, who registered it as '''J-BBHA''' and used it as a [[Liaison aircraft|liaison]] and courier plane, and for [[reconnaissance]] and news-gathering flights.<ref>{{Harvnb|Mikesj and Abe|1990|p=219.}}</ref>

==Variants==
* '''Nakajima Ki-11'''
: initial prototype (4 built); #4 with enclosed [[cockpit]]

==Operators==
*''[[Asahi Shimbun]]''

==Specifications (Ki-11)==
{{aerospecs
|ref=''Famous Airplanes of the World, first series, #76: Army Experimental Fighters (1)''<ref>{{Harvnb|FAOW|1976|p=2.}}</ref>
|met or eng?= met
|crew= 1
|capacity=
|length m= 6.89 
|length ft= 22
|length in= 7
|span m= 10.88 
|span ft= 35
|span in= 8
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m= 3.33
|height ft= 10
|height in= 11
|wing area sqm= 19.1
|wing area sqft= 205.6
|empty weight kg= 1,269
|empty weight lb= 2,798
|gross weight kg= 1,560
|gross weight lb= 3,440
|eng1 number= 1
|eng1 type= [[Nakajima Kotobuki|Nakajima Ha-1-3 Kotobuki]] air-cooled radial engine
|eng1 kw= 410
|eng1 hp= 550
|max speed kmh= 420
|max speed mph= 262
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km= 410
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m= 10,200
|ceiling ft=
|climb rate ms=
|climb rate ftmin=
|armament1= 2 × 7.7 mm (.303 in) [[Type 89 machine gun]]s
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=
* [[P-26 Peashooter]]
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
===Citations===
{{reflist|2}}
===Bibliography===
{{refbegin}}
* {{cite book|last= Francillon, Ph.D.|first= René J.|year= 1979|title= Japanese Aircraft of the Pacific War|publisher= Putnam & Company Ltd.|location= London|isbn= 0-370-30251-6}}
* {{cite book|last= Mikesh|first= Robert C.|year= 1990|author2=Shorzoe Abe|title= Japanese Aircraft, 1910-1941|publisher= Naval Institute Press |location = Annapolis, MD|isbn= 1-55750-563-2}}
* Unknown author. ''Famous Airplanes of the World, first series, #76: Army Experimental Fighters (1)''. Tokyo: Bunrin-Do, August 1976.
* Unknown author. ''Famous Airplanes of the World, second series, #24: Army Experimental Fighters''. Tokyo: Bunrin-Do, September 1990.
* Wieliczko, Leszek A. and Zygmunt Szeremeta. ''Nakajima Ki 27 Nate'' (bilingual Polish/English). Lublin, Poland: Kagero, 2004. ISBN 83-89088-51-7.
{{refend}}

==External links==
{{commons category|Nakajima military aircraft}}
* [http://www.airwar.ru/enc/fww2/ki11.html Russian site]

{{Nakajima aircraft}}
{{Japanese Army Aircraft Designation System}}

[[Category:Propeller aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Japanese fighter aircraft 1930–1939|Ki-11, Nakajima]]
[[Category:Japanese experimental aircraft 1930–1939]]
[[Category:Abandoned military aircraft projects of Japan]]
[[Category:Nakajima aircraft|Ki-011]]