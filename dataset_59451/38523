[[Image:BWER on radar data.PNG|right|thumb|Vertical cross-section through a supercell showing the BWER.]]
The '''bounded weak echo region''', also known as a '''BWER''' or a vault, is a [[Weather radar|radar]] signature within a [[thunderstorm]] characterized by a local minimum in radar [[reflectivity]] at low levels which extends upward into, and is surrounded by, higher reflectivities aloft. This feature is associated with a strong [[updraft]] and is almost always found in the inflow region of a thunderstorm. It cannot be seen visually.<ref name="NWS">{{cite web|url=http://www.weather.gov/glossary/index.php?letter=b |work=Meteorological Glossary |author=[[National Weather Service]] |publisher=[[National Oceanic and Atmospheric Administration]] |title= Bounded Weak Echo Region|accessdate=2008-02-08}}</ref> The BWER has been noted on radar imagery of severe thunderstorms since 1973 and has a [[lightning]] detection system equivalent known as a ''lightning hole''.<ref name="Martin">Martin J. Murphy and Nicholas W. S. Demetriades. [http://ams.confex.com/ams/pdfpapers/84505.pdf An Analysis of Lightning Holes in a DFW Supercell Storm Using Total Lightning and Radar Information.] Retrieved on 2008-01-08.</ref>

== Description and attributes ==
<noinclude>[[Image:Aug242006tornadoBWER.jpg|thumb|right|BWER associated with a [[August 24, 2006 tornado outbreak|tornadic supercell in 2006]], as seen from different elevation angles. The lower angles (upper left) shows a weaker area of reflectivity but not at higher levels.]]</noinclude>
The BWER is a nearly vertical channel of weak radar echo, surrounded on the sides and top by significantly stronger echoes. The BWER, sometimes called a vault, is related to the strong updraft in a severe convective storm that carries newly formed atmospheric particulates, called [[hydrometeor]]s, to high levels before they can grow to radar-detectable sizes. BWERs are typically found at mid-levels of convective storms, {{convert|3|km|mi}} to {{convert|10|km|mi}} above the ground, and are a few kilometers in horizontal diameter.<ref name="AMS">{{cite web|url=http://amsglossary.allenpress.com/glossary/search?id=bounded-weak-echo-region1 |work=Meteorological Glossary |publisher=[[American Meteorological Society]] |title= Bounded Weak Echo Region|accessdate=2008-02-08}}</ref> Identifying the location of the updraft region is important because it is linked to locations where [[severe weather]] normally occurs.<ref>Advanced Warning Operations Course. [https://wdtb.noaa.gov/courses/awoc/documentation/screen/Supplemental_Lessons/ICSvr3-I-B_shearedstorm.pdf IC 3-I-B: 1. Storm Interrogation.] Retrieved on 2008-01-08.</ref> The presence of a BWER has been part of a method to diagnose thunderstorm strength as part of the [[Lemon technique]] since 1977.<ref name="Lemon technique">Leslie R. Lemon. New severe thunderstorm radar identification techniques and warning criteria: a preliminary report. Techniques Development Unit, [[National Severe Storms Forecast Center]], Kansas City, Missouri, July 1977.</ref> The updraft strength within the BWER supports the growth of large [[hail]]stones just above the vault, which can be displaced slightly into the direction of motion of the parent [[supercell]] storm.<ref>William R. Cotton and Roger A. Pielke. [https://books.google.com/books?id=cmjnapjueiAC&pg=PA36&lpg=PA36&dq=bounded+weak+echo+region&source=web&ots=NqvlFB9GF2&sig=saoE1KjvAdN_otZSHjQP32rOJtk#PPA40,M1 Human Impacts on Weather and Climate.] Retrieved on 2008-01-08.</ref>

=== Detection ===
{{see also|Weather radar}}
[[File:Bounded weak echo region.gif|thumb|left|upright=0.75|Radar schematics of the BWER]]
The bounded weak echo region (BWER) is a region of low radar reflectivity bounded above by an area of higher radar reflectivity which shows evidence of a strong updraft within mesocyclones.  Radar analysts have recognized this phenomenon since at least 1973,<ref>Richard Jason Lynn. [http://www.spc.noaa.gov/publications/lynn/wdss2.pdf The WDSS-II Supercell Identification and Assessment Algorithm.] Retrieved on 2008-01-08</ref> using different elevation scans. Methods of objectively corroborating that a BWER is associated with a [[mesocyclone]] is done by using a [[weather radar]] with [[Doppler effect]] to obtain the precipitations velocities. This have been available operationally in [[United States]] since 1997 with the [[NEXRAD]] network.<ref>Kenneth Falk and William Parker. [http://www.srh.noaa.gov/shv/shear.htm ROTATIONAL SHEAR NOMOGRAM FOR TORNADOES.] Retrieved on 2008-03-08. {{webarchive |url=https://web.archive.org/web/20050821234207/http://www.srh.noaa.gov/shv/shear.htm |date=August 21, 2005 }}</ref>  When using the lightning detection system, lightning holes (uncovered in 2004) correspond to where a BWER would be seen on radar.<ref name="Martin"/>

A cross-section of the three-dimensional reflectivity of a thunderstorm shows the vault better. Algorithms were developed by the [[J.S. Marshall Radar Observatory]] of [[McGill University]] in [[Canada]] to locate the overhang region in a thunderstorm by the late 1980s.<ref>{{Cite web|author=Frédéric Fabry|url=http://www.radar.mcgill.ca/severe_weather.html |title=McGill S-band radar severe weather algorithms |accessdate= 2010-06-14 |publisher= [[Wayback Machine]]|date =2007-08-14|work= [[McGill University]] |archiveurl = https://web.archive.org/web/20070814192806/http://www.radar.mcgill.ca/severe_weather.html |archivedate = 2007-08-14}}</ref><ref>{{Cite conference
|last=Duncan
|first=M.R. |author2=A. Bellon |author3=A. Kilambi |author4=G.L. Austin |author5=H.P. Biron
|year=1992
|title=PPS and PPS jr:  A distribution network for weather radar products, severe warnings and rainfall forecasts.
|conference=8th International Conf. on interactive information and processing systems for Meteorology, Oceanography and Hydrology,
|booktitle=Preprint
|location=Atlanta, Georgia
|pages=67–74}}</ref><ref>{{cite conference
 | first = G.L.
 | last = Austin
 |author2=A. Kilambi |author3=A. Bellon |author4=N. Leoutsarakos |author5=A. Hausner |author6=L. Trueman |author7=M. Ivanich
  | year = 1986
 | title = Rapid II: An operational, high speed interactive analysis and display system for intensity radar data processing
 | conference = 23rd Conf. on Radar Meteor. & Conf. on Cloud Physics
|booktitle=Preprint
 | editor = American Meteorological Society
 | location = Snowmass, Colorado
 | pages = 79–82
}}</ref><ref>{{Cite conference
|first=J.
|last=Halle
|author2=A. Bellon
|year=1980
|title=Operational use of digital radar products at the Quebec Weather Centre of the Atmospheric Environment Service, Canada
|conference=19 th Radar Meteor. Conf.
|booktitle=Preprint
|editor = [[American Meteorological Society]]
|location=Miami, Florida
|pages=72–73}}</ref> Its radar uses 24 angles, giving it good vertical resolution.<ref>{{Cite web|author=Frédéric Fabry|url=http://www.radar.mcgill.ca/facilities/mcgill-radar.html|title= McGill S-band radar characteristics |accessdate= 2010-06-14|publisher= [[McGill University]]}}</ref> In United States, fewer scanning angles are made within the WSR-88D radar which makes it more difficult to detect the overhang.<ref>Advanced Warning Operations Course. [http://www.wdtb.noaa.gov/courses/awoc/documentation/screen/Supplemental_Lessons/ICSvr3-I-B_shearedstorm.pdf 1. Storm Interrogation.] Retrieved on 2008-03-08.</ref><ref>Rhonda Scott, Randy M. Steadham, and Rodger A. Brown. [http://www.roc.noaa.gov/app/pdfs/iips%20P1_22.pdf New Scanning Strategies for the WSR-88D.] Retrieved on 2008-03-08.</ref>  Once the overhang is located, it is possible to make a cross-section to view if it is related with a BWER.<ref>Leslie R. Lemon. [http://ams.allenpress.com/perlserv/?request=get-document&doi=10.1175%2F1520-0434(1998)013%3C0327%3ATRTBSS%3E2.0.CO%3B2 The Radar “Three-Body Scatter Spike”: An Operational Large-Hail Signature.] Retrieved on 2008-03-08.</ref> However, since 1997 algorithms have been developed by the National Weather Service to determine regions of reflectivity gradient in three dimensions and the presence of BWER in convection.<ref>Valliappa Lakshmanan. [http://www.cimms.ou.edu/~lakshman/Papers/ga/node2.html The Bounded Weak Echo Region Algorithm.] Retrieved on 2008-01-08.</ref>

The development of a pronounced BWER can lead to tropical cyclone-like radar signatures over land when located with a low angle [[plan position indicator]] (PPI).<ref>[[Storm Prediction Center]]. [http://www.spc.noaa.gov/coolimg/nc_storm/ncloop.htm North Carolina "Tornadocane" from 1999.] Retrieved on 2008-01-08.</ref><ref>David M. Roth. [http://www.wpc.ncep.noaa.gov/research/roth/landcane.html MCS with Eye - July 21, 2003.] Retrieved on 2008-01-08.</ref> When using the lightning detection system, lightning holes (uncovered in 2004) correspond to where a BWER would be seen on radar.<ref name="Martin"/>

{{clr}}
== See also ==
* [[Tornado]] and [[tornadogenesis]]
* [[Hook echo]]
* [[Three body scatter spike|Hail spike]]
* [[Convective storm detection]]

== References ==
{{reflist|2}}

== External links ==
* [http://www.spc.noaa.gov/coolimg/nc_storm/ncloop.htm North Carolina "Tornadocane" from 1999 - SPC]
* [http://www.wpc.ncep.noaa.gov/research/roth/landcane.html MCS with Eye - July 21, 2003 - HPC]
{{good article}}

[[Category:Severe weather and convection]]
[[Category:Radar meteorology]]