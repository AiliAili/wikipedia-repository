{{Infobox airport 
| name         = RAF Coleby Grange
| nativename   = [[Image:Ensign of the Royal Air Force.svg|90px]]
| nativename-a = 
| nativename-r = 
| image        = RAF Coleby control tower.jpg
| image-width  = 
| caption      =The original airfield control tower at RAF Coleby Grange still stands
| type         = Military
| built        = 1938
| used         = 1939-1963
| occupants    = 
| owner        = [[Ministry of Defence (United Kingdom)|Ministry of Defence]]
| operator     = [[Royal Air Force]]
| city-served  = 
| location     = [[Coleby, North Kesteven]], [[Lincolnshire]]
| elevation-f  = 200
| elevation-m  = {{Convert|200|ft|disp=output number only|0}}
| coordinates  = {{coord|53|07|47|N|000|29|55|W|type:airport_region:GB|display=inline,title}}
| pushpin_map            = Lincolnshire
| pushpin_label          = RAF Coleby
| pushpin_map_caption    = Location in Lincolnshire
| website      =  
| metric-rwy   =
| r1-number    = NE/SW
| r1-length-f  =6,000
| r1-length-m  = 1,800
| r1-surface   =Grass
| r2-number    = NW/SE
| r2-length-f  =4,200
| r2-length-m  = 1,280
| r2-surface   =Grass
| r3-number    = W/E
| r3-length-f  = 2,898
| r3-length-m  = 880
| r3-surface   =Grass
| stat-year    = 
| stat1-header =Battles/Conflicts
| stat1-data   = [[World War II]]<br>[[Cold War]]
| stat2-header =
| stat2-data   =
| footnotes    = The stations ID during flying operations was '''CG''' and was later redesignated '''WC2''' during its IRBM role. The airfield was returned to agriculture when decommissioned. The site now contains a roadside café, several farm buildings and the derelict control tower.
}}
'''Royal Air Force Station Coleby Grange''' or more simply '''RAF Coleby Grange''' was a [[Royal Air Force]] [[Royal Air Force station|station]] situated alongside the western edge of the [[A15 road (England)|A15]] on open heathland between the villages of [[Coleby, North Kesteven|Coleby]] and [[Nocton|Nocton Heath]] and lying {{Convert|7.4|mi|abbr=on}} due south of the county town [[Lincoln, England|Lincoln]], [[Lincolnshire]], England.

Opened in 1939 and operated as a fighter and night fighter airfield during [[World War II]], occupied at various times by UK, US, Canadian and Polish fighter squadrons, the station briefly switched to a training role post-war before being placed on a care and maintenance basis.

Reopened in 1959 as an [[RAF Bomber Command]] [[Intermediate Range Ballistic Missile]] (IRBM) launch facility and placed on a high [[DEFCON|DEFCON 2]] launch alert during the [[Cuban Missile Crisis]], the station was finally closed and decommissioned in 1963.  The site has been returned to agricultural use and now has little evidence of its former use, other than several lengths of perimeter track and the original air traffic control tower.

==History==

===World War II===
The station was constructed during late 1938 and opened early in 1939 initially as a relief landing ground (RLG) for the training facility at [[RAF Cranwell]] although quite quickly.  In early 1940, two squadrons [[No. 253 Squadron RAF]] and [[No. 264 Squadron RAF]] took up residence at Coleby Grange.<ref>[http://www.raf-lincolnshire.info/colebygrange/colebygrange.htm Coleby Grange at RAF Lincolnshire]</ref>

The station was destined never to be upgraded with concrete or tarmac runways and throughout its operational life used three grass runways.  Aircraft remained parked outdoors on permanent flight readiness and initially only a single Type T1 hangar was constructed for use during aircraft repairs.  Much later one blister type hangar and seven extended over-blister hangars were added.<ref>[http://www.controltowers.co.uk/C/ColebyGrange.htm Hangars]</ref>

The station’s technical and communal accommodation sites were located on the northern rim of the station with a vehicle access from Heath Road and the headquarters site was on the eastern edge adjacent to the A15 Lincoln to Sleaford road.
 
The B1202 Heath Lane on the southern boundary was closed to traffic and became part of the airfield’s perimeter track.  With only a few exceptions the buildings were of the temporary [[Nissen hut|Nissen]] or [[Quonset hut|Quonset]] hutting type and the station never developed the air of permanence achieved by many other RAF stations.  The nearby Coleby Hall, built in 1628,<ref>[http://www.genuki.org.uk/big/eng/LIN/Coleby/ Coleby Hall]</ref> was requisitioned by the Air Ministry for the duration of the war and adopted as the station’s officers’ mess.  Living accommodation on the station was graded for 1,800 RAF and WAAF personnel including officers.<ref>[http://www.raf-lincolnshire.info/colebygrange/colebygrange.htm Accommodation]</ref>
[[File:Mosquito 600pix.jpg|thumb|left|A de Havilland Mosquito.  This type was flown by several squadrons at Coleby Grange]]

In May 1941 the station was transferred to [[No. 12 Group RAF]] and severed its link with RAF Cranwell. Instead Coleby Grange became a satellite field of nearby [[RAF Digby]] and was occupied in turn by [[No. 402 Squadron RCAF]], [[No. 409 Squadron RCAF]], [[No. 410 Squadron RCAF]] and [[No. 307 Polish Night Fighter Squadron]]<ref>[http://www.raf-lincolnshire.info/colebygrange/colebygrange.htm Resident squadrons]</ref>

In 1751 a {{Convert|100|ft|abbr=on}} high landmark and former inland lighthouse known as the [[Dunston Pillar]] had been erected less than a mile north of the station on Tower Road to aid travellers crossing the wild heathland south of Lincoln.  As the tower was within the flying circuit of the new airfield {{Convert|40|ft|abbr=on}} was removed from the tower's height and its top-piece statue of King George III was removed to Lincoln Castle, where it remains today.<ref>[http://www.macla.co.uk/dunston/pillar.htm Dunston Pillar]</ref>

Until 1943 RAF Coleby Grange formed only part of a ring of fighter stations around Lincoln but, when the German daylight offensive wound down, RAF Digby shifted to a non-flying radar calibration role, RAF Kirton in Lindsey re-roled as a training establishment and RAF Hibaldstow closed.  For the remainder of the war Coleby Grange remained as the only local station still operating in the night fighter role across Lincolnshire.<ref>[http://www.raf-lincolnshire.info/colebygrange/colebygrange.htm Coleby stands alone]</ref>

During [[Operation Overlord|the D-Day landings]] RAF Coleby Grange was used as a fighter station by the [[425th Fighter Squadron]] US Army Air Force, flying [[Northrop P-61 Black Widow]]s and [[Douglas A-20 Havoc|P70 Havoc]]s in support of the [[9th Armored Division (United States)|US 9th Armored Division]].<ref>[http://www.raf-lincolnshire.info/colebygrange/colebygrange.htm D-Day operations]</ref> The squadron was under the command of the US [[Ninth Air Force]] from its headquarters at [[St Vincents Hall|St Vincents]], a large mansion in the centre of [[Grantham]].

===Post war years===
Immediately after the war the RAF mounted an annual series of air displays to commemorate the Battle of Britain.  The first of these displays in Lincolnshire took place in September 1946 at RAF Coleby Grange, as the only remaining fighter station in the county amid all the many bomber stations.<ref>[http://www.raf-lincolnshire.info/colebygrange/colebygrange.htm First Battle of Britain display]</ref>

When the war came to a close the control of Coleby Grange was returned to No. 17 Flying Training School at RAF Cranwell and the station became home to No. 1515 Beam Approach Training Flight flying [[Airspeed Oxford]]s<ref>[http://www.airfieldinformationexchange.org/community/archive/index.php/t-267.html 1515 Flight]</ref> and  No. 107 Elementary Glider School. No. 1515 BAT Flight left for [[RAF Spitalgate]] in 1946 and the glider squadron relocated to [[RAF Barkston Heath]] in 1947.

The station was mothballed and placed on a care and maintenance basis from 1947 until 1958 when it was reactivated as an IRBM missile facility.

===Cold War===
[[File:Thor RAF.jpg|thumb|British [[PGM-17 Thor]] missile]]
In January 1956 [[RAF Hemswell]] just north of Lincoln was established as an [[RAF Bomber Command]] missile unit, maintaining and operating nine mobile mounted [[PGM-17 Thor|Thor Intermediate Range Ballistic Nuclear Missile]] launchers of [[No. 97 Squadron RAF|No 97(Strategic Missile) Squadron RAF]].   Each missile with a range of {{Convert|1500|mi|abbr=on}} was tipped with a 1.44 megaton nuclear warhead, jointly controlled by the [[Royal Air Force]] and the [[United States Air Force]] under the so-called ''"dual-key arrangements"''.<ref>[http://www.history.ac.uk/resources/e-journal-international-history/twigge-paper Thor and dual key arrangements]</ref> 

In 1959 RAF Hemswell became the headquarters for the ''"No 5 (Lincolnshire) Missile Dispersal Sites"'' located at [[RAF Bardney]], [[RAF Caistor]], [[RAF Ludford Magna]] and RAF Coleby Grange.  The missiles at Coleby Grange were maintained and operated by [[No. 142 Squadron RAF]].<ref>[http://www.raf.mod.uk/history/142squadron.cfm 142 Squadron]</ref>

The [[Cuban Missile Crisis]] brought the entire UK based Thor missile force to maximum strategic alert and readiness for a ten-day period during October and November 1962.  On 26 October 1962 the [[NATO]] alert level was raised to [[DEFCON|DEFCON 2]] and the missiles were made ready for launching, on a phased-hold leaving the missiles eight minutes from launch in the vertical unfuelled condition or two minutes from launch in the fuelled position.  Several Lincoln residents can remember the Coleby Grange missiles standing erect on their mobile launchers and ready to fire.  Politically, the following day came to be referred to as "Black Saturday" and was very tense until a negotiated stand-down by both sides was reached<ref>[http://www.history.ac.uk/resources/e-journal-international-history/twigge-paper Black Saturday]</ref>

===The station closes===
RAF Coleby Grange was decommissioned and closed in 1963. In 1964 and 1965 the land was sold at auction and returned to agricultural use.  Unfortunately most of the buildings have been demolished with a small number adapted to alternate uses in farm complexes.  The original air operations control tower and part of a Thor blast wall still stand in view of the A15 in derelict conditions.  The control tower is reputed locally to be haunted.<ref>[http://www.freewebs.com/paranormal-investigation/colebygrangewatchtower.htm Sale and watch tower]</ref>

The graves of many airmen that died while serving at the station can be found in the graveyard at nearby [[Scopwick]].<ref>[http://www.macla.co.uk/scopwick/graves.php Scopwick war Graves]</ref> In the same graveyard is the final resting place of the poet [[John Gillespie Magee, Jr.]], author of the classic aviation poem "High Flight".  Magee was flying from nearby RAF Wellingore when his [[Supermarine Spitfire|Spitfire]] collided in mid-air with an [[Airspeed Oxford]] from RAF Cranwell.<ref>[http://www.macla.co.uk/scopwick/magee.php John G Magee Jr]</ref>

The long distance footpath known as the [[Viking Way]] passes less than a mile from the Coleby Grange site.<ref>[http://microsites.lincolnshire.gov.uk/Countryside/section.asp?catId=7776 Viking Way]</ref>

==Station timeline and resident units==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 90%;" 
|- bgcolor="#B0C4DE" align="center" 
| Date
| Event or Unit
| Notes
|-
| 1938
| '''Airfield and accommodation site construction commences'''
|
|-
|Spring 1939
|'''RAF Coleby Grange opened as a relief landing ground (RLG) for [[RAF Cranwell]] <br>Station Identity Code: CG'''
|
|-
|May 1940
|[[No. 253 Squadron RAF]]
|Operating [[Hawker Hurricane]] Mark 1s and relocated from [[RAF Cranwell]].  Left the station in July 1940 and relocated to [[RAF Turnhouse]]
|-
|May 1940
|[[No. 264 Squadron RAF]]
|Flying [[Boulton Paul Defiant]]s and training for a night fighter role. Relocated to [[RAF Colerne]] near Bristol.
|-
|May 1941
|'''Control of RAF Coleby Grange switched to [[No. 12 Group RAF]]
|The station became a satellite field for RAF Digby
|-
|May 1941
|[[No. 402 Squadron RCAF]]
|Operating [[Hawker Hurricane]] Mark IIs relocated from [[RAF Digby]]. Squadron re-equipped with [[Supermarine Spitfire]] Mark Vbs and left for [[RAF Colerne]] in March 1942
|-
|26 July 1941
|[[No. 409 Squadron RCAF]]
|Operating [[Boulton Paul Defiant]]s and arrived from RAF Digby. Re-equipped in August 1941 with [[Bristol Beaufighter]] Mark IIfs and in June 1942 with Mark VIs. Relocated to [[RAF Acklington]] on 23 February 1943.
|-
|February 1943
|[[No. 410 Squadron RCAF]]
|Operating de Havilland Mosquito and arrived from RAF Acklington. Departed in October 1943 for [[RAF West Malling]]
|-
|March 1943
|[[No. 288 Squadron RAF]]
|288 Squadron formed at RAF Digby on 18 November 1941 from No. 12 Group AAC Flight. It continued to provide anti-aircraft cooperation training to ground based gun crews, towing targets with a variety of aircraft, mainly the [[Miles Martinet]]. Squadron departed for Yorkshire in November 1943
|-
|21 November 1943
|[[No. 264 Squadron RAF]]
|Returned from night defence duties over the Bristol Channel ports from the base at RAF Colerne and re-equipped with [[de Havilland Mosquito]]s in November 1943. Now operating as a night defence force for bomber operations.
|-
|19 December 1943
|[[No. 409 Squadron RCAF]]
|Squadron returned from RAF Acklinton until 5 February 1944 when they relocated to [[RAF Hunsdon]]
|-
|February 1944
|[[No. 68 Squadron RAF]]
|Operating [[Bristol Beaufighter]]s and arrived from [[RAF High Ercall]] in Shropshire. Relocated to [[RAF Coltishall]] in March 1944
|-
|1 March 1944
|[[No. 2882 Flight, RAF Regiment]] (LAA Squadron)
|The RAuxAF unit of Light Anti Aircraft gunners for airfield defence formed at Coleby Grange
|-
|March 1944
|[[No. 307 Polish Night Fighter Squadron]]
|Operating [[de Havilland Mosquito]] Mark NFXIIs in a night intruder unit over enemy airfields in occupied France. Squadron was disbanded on 2 January 1947
|-
|20 March 1944
|[[No. 17 SFTS]] RAF
|The Service Flight Training School moved to Coleby Grange from RAF Cranwell and remained until March 1945 when it relocated to [[RAF Spitalgate]] in Grantham
|-
|May 1944
|[[425th Fighter Squadron]] USAAF
|Flying [[Northrop P-61 Black Widow]]s and [[Douglas A-20 Havoc|P70 Havoc]]s in support of the [[9th Armored Division (United States)|US 9th Armored Division]] during D-Day and early operations during the European campaign. The squadron departed for operations from captured airfields in France soon after the D-Day invasion.
|-
|February 1945
|'''Control of RAF Coleby Grange reverted to [[RAF Cranwell]]
|
|-
|February 1945
|[[No. 1515 BAT Flight RAF]]
|Operating [[Airspeed Oxford]]s. The Beam Approach Training flight had moved to Shropshire's [[RAF Peplow]] in January 1945, but there were difficulties caused by beam approach conflict at Peplow and nearby [[RAF Hinstock]] as the beams were almost parallel. Both stations were taken over by the Fleet Air Arm as the twin HMS Godwit carrier landing training facilities and the Navy were given precedence, so 1515 Flight were only able to operate when the cloud base was above {{convert|1000|ft|m}}. The conflicting needs were only solved a month later when 1515 Flight was relocated to Coleby Grange. The flight relocated to [[RAF Spitalgate]] and was disbanded on 9 January 1946
|-
|February 1945
|[[No. 107 EGS]]
|The Elementary Gliding School was relocated from RAF Cranwell and remained at Coleby Grange until it moved to [[RAF Syerston]] where it still exists, renumbered as No. 643 Volunteer Gliding School
|-
| May 1946
|'''Operational flying ceased from RAF Coleby Grange. Station placed on a care and maintenance basis'''
|
|-
|September 1946
|'''Battle of Britain Air Display mounted at Coleby Grange'''
|
|-
| 1959
|'''Control of RAF Coleby Grange was handed to [[RAF Bomber Command]]. Station ID code WC2'''
|RAF Coleby Grange formed part of ''"No 5 (Lincolnshire) Missile nDispersal Sites"'' with missiles also rotated around [[RAF Hemswell]], [[RAF Bardney]], [[RAF Caistor]] and [[RAF Ludford Magna]]
|- 
|1959
|[[No. 142 Squadron RAF]]
|Operating the 3 mobile Thor Intermediate Range Ballistic Missiles while they were on site. When the UK Thor IRBM Force was stood down in 1963 the missiles were returned to the US and their warheads removed before the launch vehicles were reused in the space programme - predominantly launching communications satellites.
|-
|1963
|'''RAF Coleby Grange was decommissioned and closed. The land was sold by auction in 1964/65 and returned to agricultural uses.'''
|
|}

==Gallery==
<Gallery>
File:Northrop P-61 green airborne.jpg|A Northrop P-61 Black Widow used during the invasion of Europe during 1944 in a ground support role 
File:The former control tower at RAF Coleby Grange - geograph.org.uk - 139755.jpg|Former control tower and watch office at RAF Coleby Grange
File:A Thor ICBM blast wall on RAF Coleby Grange - geograph.org.uk - 139761.jpg|Remains of the Thor IRBM blast wall at Coleby
File:Boothby Graffoe Heath - geograph.org.uk - 95729.jpg|View looking north over the heathland that was RAF Coleby Grange, photographed from Boothby Graffoe
File:Thor IRBM.jpg|A Thor missile ready to launch, with its nuclear warhead replaced by a communications satellite payload
</Gallery>

==See also==
* [[List of former RAF stations]]

==References==

===Citations===
{{reflist|3}}

===Bibliography===
*[[Bruce Barrymore Halpenny]] ''Action Stations: Wartime Military Airfields of Lincolnshire and the East Midlands v. 2'' (ISBN 978-0850594843)

==External links==
{{Commons category|RAF Coleby Grange}}
* [http://airfields.fotopic.net/c1317145.html RAF Coleby Grange photos]
* [http://www.controltowers.co.uk/C/ColebyGrange.htm Coleby Grange at Controltowers.co.uk]
* [http://www.bing.com/maps/?cp=53.12781772471~-0.506950642430786&lvl=14&style=a&FORM=MMREDIR Aerial photograph of the current site]

{{Royal Air Force}}
{{RAF stations in Lincolnshire}}

{{DEFAULTSORT:Coleby Grange}}
[[Category:Royal Air Force stations in Lincolnshire]]
[[Category:Royal Air Force stations of World War II in the United Kingdom]]
[[Category:Military units and formations established in 1939]]
[[Category:Military units and formations disestablished in 1963]]