The '''Copyright Society of the U.S.A.''' is the primary scholarly society dedicated to the study of [[copyright law in the United States]].<ref>See generally F. Jay Dougherty, "A Story of Two Anniversaries: Nimmer and the Bulletin/Journal of the Copyright Society", 60 ''Journal of the Copyright Society U.S.A.'' 149 (Winter 2013).</ref>

The Copyright Society of the USA was established in 1953, by a number of copyright scholars and lawyers including [[Charles B. Seton]] (1910-2005).  

The Society publishes a long-running journal, the ''Journal of the Copyright Society of the USA''.<ref>[http://www.csusa.org/?About_us "About Us"], CSUSA website (last visited March 14, 2014).</ref> The Society also hosts annual and midwinter meetings, as well as a variety of educational sessions in its regional chapters. The organization has approximately eleven chapters throughout the country, and is headquartered in New York.<ref>[http://www.csusa.org/?page=ChapterAll "Chapters"], CSUSA website (last visited March 14, 2014).</ref> The Society hosts the annual "Donald C. Brace Memorial Lecture" (named after [[Donald Brace]], one of the founders of the [[Harcourt, Brace & Co.]] publishing company),<ref>[http://www.csusa.org/?page=Brace "Donald C. Brace Memorial Lecture"], CSUSA website (last visited March 14, 2014).</ref> and presents the annual "Seton Award" for scholarship by a young lawyer (under 40).<ref>[http://www.csusa.org/?page=Award_Seton "Seton Award"], CSUSA website (last visited March 14, 2014).</ref>   

==Notes==
{{reflist}}

[[Category:Organizations established in 1953]]
[[Category:Copyright law organizations]]
[[Category:1953 establishments in the United States]]
[[Category:Organizations based in New York City]]

{{US-org-stub}}