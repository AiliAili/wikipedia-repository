{{For|the railway station in New Jersey|Elizabeth (NJT station)}}
{{Use dmy dates|date=September 2014}}
{{Use Australian English|date=September 2014}}
{{Infobox station
| name                = <small>Elizabeth Interchange</small>
| image               = ElizabethTrainStation02042012.jpg
| caption             = 
| address             = Mountbatten Square
| borough             = [[Elizabeth, South Australia|Elizabeth]]
| coordinates         = 
| owned               = 
| operator            = 
| line                = [[Gawler Central railway line|Gawler Central Line]]
| distance            = 25.8&nbsp;km from [[Adelaide railway station|Adelaide]]
| platforms           = 2
| tracks              = 
| structure           = 
| parking             = Yes
| bicycle             = Yes
| disabled            = 
| bus_routes          = 
| code                = 
| zone                = 
| website             = 
| opened              = 1960
| closed              = 
| rebuilt             = 1999 & 2012
| services            = 
{{s-rail|title=TransAdelaide}}
{{s-line|system=TransAdelaide|line=Gawler Central|previous=Elizabeth South|next=Womma}}
}}

'''Elizabeth railway station''' is located on the [[Gawler Central railway line|Gawler Central line]].<ref>[http://www.adelaidemetro.com.au/var/metro/storage/original/application/8a768a6fdea5fb9418c2b174b850e308.pdf Gawler Central timetable] {{webarchive |url=https://web.archive.org/web/20140210204945/http://www.adelaidemetro.com.au/var/metro/storage/original/application/8a768a6fdea5fb9418c2b174b850e308.pdf |date=10 February 2014 }} Adelaide Metro 4 February 2013</ref> Situated on the border of the northern [[Adelaide]] suburbs of [[Elizabeth, South Australia|Elizabeth]] and [[Edinburgh, South Australia|Edinburgh]], it is 25.8 kilometres from [[Adelaide railway station|Adelaide station]]. Elizabeth is one of the busiest stations on the [[Railways in Adelaide|Adelaide suburban system]].{{citation needed|date=April 2012}}

==History==
Elizabeth station opened in 1960, being rebuilt in 1999 with one island platform that was accessible by an underground pedestrian subway. It included a 40–50 metre roof.

==Redevelopment 2012==
In March 2012, the station was rebuilt in a $15 million project. The work involved demolition of the island platform, construction of two side platforms, the track re-aligned, and other changes made in preparation for the aborted electrification of the [[Gawler Central railway line|Gawler Central line]].<ref>[http://anthonyalbanese.com.au/major-upgrades-to-revamp-northern-suburbs-rail-travel Major upgrades to revamp Northern suburbs rail travel] Anthony Albanese MP</ref><ref>[http://www.knoxconstructions.com/elizabeth-station-upgrade.php Elizabeth station upgrade] Knox Constructions</ref><ref>[http://www.yorkcivil.com.au/project/elizabeth-station-upgrade/ Elizabeth Station Upgrade] York Civil</ref><ref>[http://www.infrastructure.sa.gov.au/stations_upgrade_program Stations Upgrade Program] Department of Planning, Transport & Infrastructure</ref><ref>[http://www.adelaidenow.com.au/news/south-australia/federal-government-pulls-plug-on-76m-gawler-line-electrification/story-fni6uo1m-1226745622546 Federal Government pulls plug on $76m Gawler Line electrification] ''[[Adelaide Advertiser]]'' 24 October 2013</ref>

As some peak-hour services were to terminate at Elizabeth, the rebuild included the addition of a siding just north of the station to allow trains to stable clear of the main line. The western platform is accessed both by a level crossing and an overpass.

==Transport links==
{{AdelaideMetroBusTable/Header
|stopname=Stand A (Elizabeth Station)}}
{{AdelaideMetroBusTable/Body
|busnumber=440
|colour=#007698
|textcolour=#ffffff
|details=[[Munno Para railway station|Munno Para station]] via [[Smithfield railway station, Adelaide|Smithfield Interchange]] & Hamblyn Road<ref>[https://www.adelaidemetro.com.au/routes/440 Route 440] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=441
|colour=#5a3f99
|textcolour=#ffffff
|details=Smithfield Interchange via Yorketown Road<ref>[https://www.adelaidemetro.com.au/routes/441 Route 441] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=442
|colour=#00502f
|textcolour=#ffffff
|details=Smithfield Interchange via Blair Park Drive<ref>[https://www.adelaidemetro.com.au/routes/442 Route 442] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=443
|colour=#55b145
|textcolour=#ffffff
|details=Elizabeth North Loop via Smithfield Interchange & Munno Para Loop<ref>[https://www.adelaidemetro.com.au/routes/443 Route 443] Adelaide Metro</ref>}}
|}

{{AdelaideMetroBusTable/Header
|stopname=Stand B (Elizabeth Station)}}
{{AdelaideMetroBusTable/Body
|busnumber=451
|colour=#9c8dc3
|textcolour=#ffffff
|details=Munno Para Shopping Centre via [[Andrews Farm, South Australia|Andrews Farm]], [[Davoren Park]] & [[Smithfield railway station, Adelaide|Smithfield Interchange]] and Curtis Road<br/><ref>[https://www.adelaidemetro.com.au/routes/451 Route 451] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=452
|colour=#f26649
|textcolour=#ffffff
|details=Munno Para Shopping Centre via Mark Oliphant College<ref>[https://www.adelaidemetro.com.au/routes/452 Route 452] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=900
|colour=#bf311a
|textcolour=#ffffff
|details=[[Salisbury railway station, Adelaide|Salisbury Interchange]] via Peachey Road, [[Virginia, South Australia|Virginia]] & Waterloo Corner Road<ref>[https://www.adelaidemetro.com.au/routes/900 Route 900] Adelaide Metro</ref>}}
|}

{{AdelaideMetroBusTable/Header
|stopname=Stand C (Elizabeth Station)}}
{{AdelaideMetroBusTable/Body
|busnumber=400
|colour=#a587be
|textcolour=#ffffff
|details=[[Salisbury North]] via [[Salisbury railway station|Salisbury Interchange]]<ref>[https://www.adelaidemetro.com.au/routes/400 Route 400] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=430
|colour=#ffe4b8
|textcolour=#000000
|details=Salisbury Interchange & [[Main North Road]]<ref>[https://www.adelaidemetro.com.au/routes/430 Route 430] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=224
|colour=#f7b2bf
|textcolour=#000000
|details=[[Adelaide city centre|City]] via Salisbury Interchange & [[Mawson Interchange]]<ref>[https://www.adelaidemetro.com.au/routes/224 Route 224] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=560
|colour=#fdd256
|textcolour=#000000
|details=[[Tea Tree Plaza Interchange]]<ref>[https://www.adelaidemetro.com.au/routes/560 Route 560] Adelaide Metro</ref>}}
|}

{{AdelaideMetroBusTable/Header
|stopname=Stand D (Elizabeth Station)}}
{{AdelaideMetroBusTable/Body
|busnumber=C1
|colour=#c78d86
|textcolour=#ffffff
|details=[[Adelaide city centre|City]] via [[O-Bahn Busway|O-Bahn]], limited stop service<ref>[https://www.adelaidemetro.com.au/routes/C1 Route C1] Adelaide Metro</ref>}}
{{AdelaideMetroBusTable/Body
|busnumber=500
|colour=#d4a67c
|textcolour=#ffffff
|details=City via [[Salisbury railway station, Adelaide|Salisbury Interchange]], Bridge Road & O-Bahn<br><small>Transit Link limited stop service</small><ref>[https://www.adelaidemetro.com.au/routes/500 Route 500] Adelaide Metro</ref>}}
|}

==Gallery==
<gallery>
File:ElizabethRailwayStationAdelaide.jpg|Station before the 2012 upgrade
File:Concept Image Elizabeth Interchange.jpeg|Artist's impression of the new station
File:Concept Image Elizabeth Interchange 2.jpeg|Artist's impression of the new station
</gallery>

==References==
{{Reflist|2}}

==External links==
*{{commons category-inline}}
*[https://www.flickr.com/photos/baytram366/sets/72157628677030895/ Flick gallery]

{{coord|-34.7173|138.6647|format=dms|display=title|region:AU-SA_type:railwaystation}}

{{Gawler Central railway line, Adelaide}}

[[Category:Railway stations in Adelaide]]
[[Category:Railway stations opened in 1960]]