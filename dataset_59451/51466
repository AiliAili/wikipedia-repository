A '''hijacked journal''' is a legitimate [[academic journal]] for which a bogus website has been created by a malicious third party for the purpose of fraudulently offering academics the opportunity to rapidly publish their research online for a [[Article processing charge|fee]].<ref name=Butler/>

==Background==
In 2012,  [[Computer crime|cyber criminals]] began hijacking print-only journals by registering a [[domain name]] and creating a fake website under the title of the hijacked journals.<ref>{{cite journal |url=http://wjst.wu.ac.th/index.php/wjst/article/view/1004 |title=Hijacked Journals and Predatory Publishers: Is There a Need to Re-Think How to Assess the Quality of Academic Research?  |journal=Walailak Journal of Science and Technology |year=2014 |volume=11 |number=5 |pages=389–394 |first1=Mehrdad |last1=Jalalian |first2=Hamidreza |last2=Mahboobi}}</ref>

The first journal to be hijacked was the Swiss journal ''[[Archives des Sciences]]''. In 2012 and 2013, more than 20 academic journals were hijacked.<ref name=Butler>{{cite journal |last1=Butler |first1=Declan |title=Sham journals scam authors |journal=Nature |date=27 March 2013 |volume=495 |issue=7442 |pages=421–422 |doi=10.1038/495421a |pmid=23538804}}</ref> In some cases, forgers find their victim in [[conference proceedings]], extracting authors' emails from papers and sending them fake calls for papers.<ref>Mehdi Dadkhah and Aida Quliyeva, "Social Engineering in Academic World", Journal of Contemporary Applied Mathematics (Invited Paper), 4(2), pp. 3-5, 2015.</ref>

There have also been instances of hijacking journals by taking over their existing domain names after the journal publisher neglected to pay the domain name registration fees on time.<ref>{{citation|url=http://retractionwatch.com/2015/11/19/can-journals-get-hijacked-apparently-yes/|title=Can journals get hijacked? Apparently, yes|first=Alison|last=McCook|date=November 19, 2015|work=[[Retraction Watch]]}}.</ref><ref>{{citation|url=http://news.sciencemag.org/scientific-community/2015/11/feature-how-hijack-journal|journal=[[Science (journal)|Science]]|title=Feature: How to hijack a journal|first=John|last=Bohannon|date=November 19, 2015|doi=10.1126/science.aad7463}}.</ref>

==See also==
*[[Confidence trick]]
*[[Passing off]]
*[[Predatory open access publishing]]
*Other journals that were the victim of hijacking:
**''[[Sylwan]]''
**''[[The Veliger]]''
**''[[Wulfenia (journal)|Wulfenia]]''
**''[[Journal of Natural Products]]''

==References==
{{Reflist}}

==External links==
*American librarian [[Jeffrey Beall]]'s [https://web.archive.org/web/20151207233414/http://scholarlyoa.com/other-pages/hijacked-journals/ hijacked journal list]
*Iranian journalist Mehrdad Jalalian's [http://www.mehrdadjalalian.com hijacked journal list]

[[Category:Internet fraud]]
[[Category:Academic publishing]]


{{Internet-stub}}