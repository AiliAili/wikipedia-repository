{{Infobox Magazine
| title           = Alabama Heritage
| editor          = Donna Cox Baker
| editor_title    = Editor-in-Chief
| frequency       = Quarterly
| circulation     = 10,000
| category        = [[History]], [[Alabama]]
| publishers       = [[University of Alabama]], [[University of Alabama at Birmingham]], and [[Alabama Department of Archives and History]]
| firstdate       = Summer 1986<ref name="WheatJack">Wheat, Jack. Award Puts Alabama Heritage in Tall Cotton, The Tuscaloosa News "Award Puts Alabama Heritage in Tall Cotton". Published 1987-07-19</ref>
| country         = [[United States]]
| based           = [[Tuscaloosa, Alabama]]
| language        = English
| website         = {{URL|http://www.AlabamaHeritage.com}}
| issn            = 0887-493X
}}

'''''Alabama Heritage''''' ({{ISSN|0887-493X}}) is an award-winning, nonprofit educational quarterly history magazine first published during the summer of 1986. It is published by the [[University of Alabama]], the [[University of Alabama at Birmingham]], and the [[Alabama Department of Archives and History]]. The magazine was conceived with a broad conception of "heritage," incorporating more than traditional history. Issues include articles about archaeology, architecture, anthropology, religion, folk arts, literature, and music. ''Alabama Heritage,'' through support from Blue Cross and Blue Shield, is available in every school in the state of Alabama.

== History ==

* Bill Barnard, chair of the University of Alabama's history department, first broached the idea of a state history magazine and recruited Suzanne Wolfe to run with it. The first issue rolled out in the summer of 1986.<ref>LeComte, Richard. ''UA’s Alabama Heritage Magazine Marks 25 Years, 101 Issues'', UA News [http://uanews.ua.edu/2011/08/uas-alabama-heritage-magazine-marks-25-years-101-issues/ "UA’s Alabama Heritage Magazine Marks 25 Years, 101 Issues"]. Published 2011-08-09</ref>
* In the mid-1990s, proration caused severe strains on the state’s education budgets. Out of money and in danger of an issue not being delivered, the community came to the magazine's rescue.
* Currently the magazine is funded by [[the University of Alabama]], [[the University of Alabama at Birmingham]], and the [[Alabama Department of Archives and History]], as well as a "Friends of Alabama Heritage," which has been a key pillar of the magazine's long-term survival.
* The magazine was formerly housed at the Kilgore House, which was part of the Bryce Hospital in Tuscaloosa, Alabama, and considered to be haunted. The editors of the magazine invited paranormal experts to record activity.<ref>Tuscaloosa Paranormal Research Group. ''Kilgore House'', Tuscaloosa Paranormal Research Group [http://www.tuscaloosaparanormal.com/index.php?option=com_content&view=article&id=114 "Kilgore House"]. Published 2009-05</ref>

== Community Impact ==

''Alabama Heritage's'' award-winning feature writing has made an impact in historic preservation (see "Places in Peril" below) and also in politics. Alabama Governor Robert Bentley signed the Scottsboro Boys Act in 2013, which finally and officially exonerated the [[Scottsboro Boys]] (nine African-American teens, wrongfully convicted of raping two white women in 1931. Only one was pardoned before his death.) The ''Alabama Heritage'' feature on the Scottsboro Boys -- "Awaiting Justice: The Improbable Pardon of 'Scottsboro Boy' Clarence Norris" by Tom Reidy (Issue 105, Summer 2012) [http://www.alabamaheritage.com/issue-105-summer-2012.html]—was handed to Gov. Bentley with a plea for a pardoning from Sheila Washington, director of Scottsboro Boys Museum and Cultural Center.<ref>McWhorter, Andy, “Years Later, Scottsboro Boys to be Pardoned,” The Crimson White, http://cw.ua.edu/2013/07/03/years-later-scottsboro-boys-to-be-pardoned/, Published July 3, 2013</ref><ref>Wallingsford, Danielle, “Exoneration Movement for Scottsboro Boys Underway,” The Clarion, http://www.theclarion.org/index.php?option=com_content&view=article&id=4853:exoneration-movement-for-scottsboro-boys-underway&catid=42:clarion-rotation-stories&Itemid=142, Published September 19, 2012</ref> Also, Sherman W. White Jr., one of the [[Tuskegee Airmen]] was finally honored after an article in ''Alabama Heritage'' magazine prompted his hometown of Montgomery, Alabama, to create a historic marker in his honor.<ref>Alabama Blackbelt History. ''Tuskegee Airman Finally gets his Due'' [http://www.alabamablackhistory.com/news/news-120315-TuskegeeAirman.htm "Tuskegee Airman Finally gets his Due"].</ref>

== Becoming Alabama ==

In 2010, ''Alabama Heritage''—in partnership with the Summersell Center for Study of the South, the University of Alabama Department of History, and the Alabama Tourism Department—created a regular department in ''Alabama Heritage'' magazine as a part of the statewide "Becoming Alabama" initiative—a cooperative venture of state organizations to commemorate Alabama’s experiences related to the Creek War, the Civil War, and the civil rights movement. Quarter by quarter, the Becoming Alabama [http://www.alabamaheritage.com/becoming-alabama.html] stories detail the corresponding seasons 200, 150, and 50 years ago—sometimes describing pivotal events, sometimes describing daily life, but always illuminating a world in flux.<ref>Alabama Department of Archives and History, http://www.archives.alabama.gov/BA/press_release_101006.pdf</ref>

== Places in Peril ==

Inspired by the National Trust's yearly listing of "America's Most Endangered Historic Places," a handful of Alabama preservationists decided to develop a similar roster for their own state. The initial list of ten sites, then referred to as "Alabama's Most Endangered Properties," came out in 1994, first as a media release, then as a full article in the fall issue of ''Alabama Heritage'' magazine. Annually since then—in a joint undertaking between the Alabama Historical Commission, the Alabama Trust, and ''Alabama Heritage''—more threatened landmarks have been highlighted in the feature now called "Places in Peril." [http://www.alabamaheritage.com/places-in-peril] These 216 properties represent a broad array of places depicting Alabama's story from prehistoric times to the civil rights struggles of the 1960s. Of the 216 properties listed as Places in Peril in the past twenty years, approximately 59 are no longer endangered, and 35 have been destroyed. The verdict is still out on the remaining 122 sites. Some of the success stories may have had a connection to being listed. Locust Hill in Tuscumbia, for example, was on the market when it was included in Places in Peril in 2004. The media attention given to the site inspired a couple to purchase the building as their home. In 1998 the Queen City Pool in Tuscaloosa was abandoned with an uncertain future. Today, the property has been restored as the Mildred Westervelt Warner Transportation Museum. The annual list has encouraged property owners, city officials, local organizations, and potential buyers to preserve these properties and keep them viable.<ref>Gamble, Robert S. ''Historic Architecture in Alabama: A Guide to Styles and Types, 1810-1930'', [[University of Alabama Press]] (1990), 978-0817311346.</ref>

== Awards ==

* 1987 -  Gold Medal Award, CASE National Competition, "University Magazines."  The judges—a panel from Newsweek—evaluated 90 entries in this competition and awarded 10 Gold Medals.  "The judges were very impressed with a new magazine, Alabama Heritage.  They felt that the content, design, and writing worked beautifully together to effectively meet the magazine's goal--to present the history of the state of Alabama in its diversity."  Other gold medal winners:  Harvard University, Duke University, Cornell University, Johns Hopkins University, the University of Pennsylvania, the University of Portland, Stanford University, George Mason University.<ref name="WheatJack" />
* 1987 -  Best of Category, CASE District III, "University Magazines."  [District III includes North Carolina, South Carolina, Kentucky, Georgia, Florida, Tennessee, and Alabama.]  
* 1989 -  Bronze Medal Award, CASE National Competition, "University Magazines."  This competition was judged at Swarthmore College.
* 1989 -  Grand Award, CASE District III, "Other Magazines."  ["Other Magazines" means non-alumni magazines.]  This competition was judged at Duke University.
* 1989 -  Bronze Medal Award, CASE National Competition, “University Magazines."  The judges at Pomona College, California, considered 125 entries.
* 1989 -  Grand Award, CASE District III, "Institutional Publications."  This competition was judged at Georgia Institute of Technology.
* 1990 -  Honor Award, Alabama Preservation Alliance: "In recognition of outstanding contributions to the people of Alabama in the preservation and conservation of the historical resources of our state."
* 1990 -  Bronze Medal Award, CASE National Competition, "University Magazines."
* 1990 -  Grand Award, CASE District III, "Institutional Publications."
* 1990 -  Grand Award, CASE District III, "University Magazines."
* 1991 -  Award of Excellence, CASE District III,  "Institutional Publications." 
* 1991 -  Certificate of Commendation, Mobile Historic Development Commission, Mobile, Alabama, for "excellence in publishing a high quality magazine on the history of the state." 
* 1991 -  Gold Medal Award, CASE National Competition "Special Interest Magazines."  This competition was judged at Oregon State University.  The judges considered 73 entries and awarded two Gold Medals.
* 1991 -  Gold Medal Award, CASE National Competition, "Top Ten Magazines."  This competition was judged by a panel of editors from Newsweek.  Other top ten winners included magazines published by Dartmouth College, Duke University, the University of Notre Dame, Johns Hopkins University, Radcliffe College, Tulane University, the University of Pittsburgh, Arizona State University, and Southern Methodist University.
* 1992 -  Gold Medal Award, CASE National Competition, "Special Interest Magazines."  Sixty-one entries were considered and one Gold Medal was awarded.  The competition was judged at Washington State University.
* 1992 -  Grand Award, CASE District III, "Other Magazines."  This competition was judged at Duke University.
* 1992 -  Special Merit Award, CASE District III, "Institutional  Publications."  This competition was judged at the University of Kentucky.
* 1993 -  Award of Excellence, CASE District III, "Other Magazines."  This competition was judged at Memphis State University.
* 1993 -  Award of Special Merit, CASE District III, "Institutional Publications."  Judged at Auburn University. 
* 1994 -  Bronze Medal Award, CASE National Competition, "Visual Design in Print."  The judges at Marietta College, Ohio, considered 297 entries.
* 1994 -  Award of Excellence, CASE District III, "Special Interest Magazines."
* 1995 -  Award of Excellence, CASE District III, "Institutional Publications."  This competition was judged at The University of Alabama in Birmingham.
* 1995 - Award of Excellence, CASE District III, "Other Magazines."  This competition was judged at Clark Atlanta University. 
* 1995 - Heritage Education Award, Tuscaloosa County Preservation Society "for the magazine's ongoing contributions to publicizing the plight of not only Tuscaloosa County's historic resources, but those scattered throughout Alabama."
* 1995 - Alabama Humanities Award, presented by the Alabama Humanities Foundation to Suzanne Wolfe, editor of Alabama Heritage, for "exemplary contributions to public understanding and appreciation of the humanities in Alabama." 
* 1996 - Silver Medal Award, CASE National Competition, "Special Interest Magazines."  This competition—sponsored by Newsweek magazine, The Chronicle of Higher Education, and Ford Motor Company Fund—was judged at the University of Kansas.  The judges considered 48 entries.
* 1997 - Award of Excellence, CASE District III, “Other Magazines.” This competition was judged at Mississippi State University.
* 1997 - Gold Medal Award, CASE National Competition, “Special Interest Magazines."  This competition—sponsored by Newsweek magazine, The Chronicle of Higher Education, and Ford Motor Company Fund—was judged at the University of Notre Dame.  The judges considered 42 magazines from across the country.
* 1997 - Southeast Society of Architectural Historians Award for Best Article, given for Delos D. Hughes’s article “Jefferson’s ‘Academical Village’” which was published in the Spring, 1997 issue of Alabama Heritage. 
* 1998 - Award of Excellence, CASE District III, “Other Magazines.” This competition was judged at Auburn University.
* 1999 - Bronze Medal Award, CASE National Competition, “Special Interest Magazines."  This competition, judged at the University of Portland, considered more than 50 magazines from across the country.
* 1999 - Finalist, Southern Environmental Law Center Award for Outstanding Writing on the Southern Environment. Todd Keith’s article, “Alabama’s Heart River: The Cahaba,” (Alabama Heritage, Fall 1998) was a top-contender in this regional competition recognizing the South’s best writing about the environment.
* 2000 – Certificate of Commendation from the American Association for State and Local History, a prestigious national organization whose award program extends throughout North America. Only three AASLH awards have been given to Alabama projects in the last eleven years.
* 2001 – Award of Excellence, CASE District III Advancement Awards, “Other Magazines” category. This competition, judged by the development office of the Episcopal Diocese of Virginia, offered comments from the judges: “Great look. It could probably beat ‘Smithsonian’ in a competition.” “I like the writing. It is warm and informative. The editor is doing a good job.” “This is an excellent example of cooperation between two major universities.”
* 2002 – The Grand Award, CASE District III Advancement Awards, “Other Magazines” category. Selected for this award from a total of eighteen other entries, Alabama Heritage was heralded as a “beautiful magazine! The photographs are outstanding and the content is excellent.” 
* 2003 - Award of Excellence, CASE District III Advancement Awards, “Other Magazines” category. The Communications Office at the University of Georgia judged the competition among 24 entries.
* 2004 - The Grand Award, CASE District III Advancement Awards, “Other Magazines” category. Selected for this award from a total of twenty-six other entries, Alabama Heritage was regarded as “a superb publication and an excellent reflection on the state of Alabama.”  Magazines were judged on content, writing, editing, design, photography, and printing.
* 2005 - Bronze Medal Award, CASE National Competition, “Special Interest Magazines."  This competition, judged at Dartmouth College, considered more than 40 magazines from colleges and universities across the country including the medical schools at Harvard and Yale.

== References ==

{{reflist}}

== Further reading ==

* U.S. Department of Transportation Federal Highway Administration. ''Innovative Financing Tip for Communities at Their Wit's End'', [http://www.fhwa.dot.gov/infrastructure/tip.cfm "Innovative Financing Tip for Communities at Their Wit's End"]. Published 2011-04-07
* Hollis, Tim. ''See Alabama First: The Story of Alabama Tourism'', The History Press (2013), 978-1609494889.
* Higdon, David and Brett J. Talley. ''Haunted Tuscaloosa'', The History Press (2012), 978-1609495732.
* Birmingham Genealogy. ''Alabama Heritage article: Brookside: A Wild West Town"'', Birmingham Genealogical Society [http://birminghamgenealogy.wordpress.com/2007/07/24/alabama-heritage-article-brookside-a-wild-west-town/ "Alabama Heritage article: 'Brookside: A Wild West Town'"]. Published 2013-07-24
* Beitelman, T.J. ''Wait, wait: Who Does This "Jennifer Horne" Think She Is?'' [http://tjbeitelman.com/2011/10/10/wait-wait-who-does-this-jennifer-horne-think-she-is/ "Wait, wait: Who Does This 'Jennifer Horne' Think She Is?"]. 
* Duncan, Andy. Alabama Curiosities: Quirky Characters, Roadside Oddities & Other Offbeat Stuff, Globe Pequot; Second edition (2009), 978-0762749317.
* Geological Survey of Alabama. ''State Fossil of Alabama'' [http://www.gsa.state.al.us/gsa/fossil_alabama.html "State Fossil of Alabama"]. 
* Patterson, Michael Robert. ''Henry Crommelin, Vice Admiral'', Arlington National Cemetery Website [http://www.arlingtoncemetery.net/henry-crommelin.htm "Henry Crommelin, Vice Admiral"]. Published 2004-05-23
* Auburn University Economic & Community Development Institute. ''Alabama Economic Development Resource Directory'', Auburn University & Alabama Cooperative Extension System [http://www.auburn.edu/outreach/ecdi/documents/cultural_heritage.pdf "Alabama Economic Development Resource Directory"]. 
* BhamWiki. ''Charles Kilgore residence'', [[Auburn University]] & Alabama Cooperative Extension System [http://www.bhamwiki.com/w/Charles_Kilgore_residence "Charles Kilgore residence"]. 
* Dialog. ''Campus Landmark: The Kilgore House'', [[The University of Alabama]] [http://dialog.ua.edu/2012/08/campus-landmark-the-kilgore-house/ "Charles Kilgore residence"]. Published 2012-08-13
* Sledge, John S. ''A Legendary Penman: John S. Sledge catches up with one of the Gulf Coast’s most prolific authors, the illustrious Winston Groom'', Mobile Bay Magazine [http://www.mobilebaymag.com/Mobile-Bay/May-2013/A-Legendary-Penman/ "A Legendary Penman: John S. Sledge catches up with one of the Gulf Coast’s most prolific authors, the illustrious Winston Groom"]. Published 2013-05
* Perazzo, Peggy. ''Alabama Stories Tombstones'', Stone Quarries and Beyond [http://quarriesandbeyond.org/states/al/alabama.html "Alabama Stories Tombstones"]. Published 2013-06-08
* ''Places in Peril'', Alabama Trust for Historic Preservation [http://www.alabamatrust.info/about.php "Places in Peril"]. Published 2013
* Armstrong, Meredith. ''Kilgore House demolition underway in Tuscaloosa Monday'', WVTM-TV [http://www.alabamas13.com/story/22235099/kilgore-house-demolition-underway-in-tuscaloosa "Kilgore House demolition underway in Tuscaloosa Monday"]. Published 2013-05-20
* Reynolds, Kelvin. ''Oldest building on UA's campus to be demolished'', WSFA-12 [http://www.wsfa.com/story/22239274/oldest-building-on-uas-campus-to-be-demolished "Oldest building on UA's campus to be demolished"]. Published 2013-05-20
* Alabama Women's Hall of Fame. ''Tallulah Brockman Bankhead'' [http://www.awhf.org/bankhead.html "Tallulah Brockman Bankhead"]. 
* ''"Bridging the Gulf -- The Alabama-Cuba Connection"'' Society Mobile-La Habana [http://www.havana-mobile.com/society_history.htm "Bridging the Gulf -- The Alabama-Cuba Connection"].
* Brock, John Brightman. ''New Archives director wants Alabamians involved in their history'' Alabama Living Magazine [http://alabamaliving.coop/article/new-archives-director-wants-alabamians-involved-in-their-history/ "New Archives director wants Alabamians involved in their history"].

== External links ==
*[http://www.alabamaheritage.com www.AlabamaHeritage.com]

[[Category:Articles created via the Article Wizard]]
[[Category:American history magazines]]
[[Category:American quarterly magazines]]
[[Category:Magazines established in 1986]]
[[Category:Magazines published in Alabama]]