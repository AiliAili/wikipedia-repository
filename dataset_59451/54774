{{italic title}}
[[File:PerkinWarbeck.jpg|right|thumb|Title page from an 1857 edition of ''Perkin Warbeck'']]

'''''The Fortunes of Perkin Warbeck:  A Romance''''' is an 1830 [[historical fiction|historical novel]] by [[Mary Shelley]] about the life of [[Perkin Warbeck]].  The book takes a Yorkist point of view and proceeds from the conceit that Perkin Warbeck died in childhood and the supposed impostor was indeed [[Richard of Shrewsbury]].  [[Henry VII of England]] is repeatedly described as a "fiend" who hates [[Elizabeth of York]], his wife and Richard's sister, and the future [[Henry VIII]], mentioned only twice in the novel, is a vile youth who abuses dogs.  Her preface establishes that records of the [[Tower of London]], as well as the histories of [[Edward Hall]], [[Raphael Holinshed]], and [[Francis Bacon]], the letters of [[John Ramsay, 1st Lord Bothwell|Sir John Ramsay]] to Henry VII that are printed in the Appendix to [[John Pinkerton]]'s History of Scotland<ref>[https://books.google.co.uk/books?id=0dI_AAAAcAAJ&source=gbs_navlinks_s Pinkerton, John, ''History of Scotland'', vol.2 (1791), 438-441]</ref> establish this as fact. Each chapter opens with a quotation.  The entire book is prefaced with a quotation in French by  [[Georges Chastellain]] and [[Jean Molinet]].

==Plot and themes==
In this novel, Mary Shelley returned to ''[[The Last Man]]''{{'}}s message that an idealistic political system is impossible without an improvement in human nature.<ref>Frank, "''Perkin Warbeck''".</ref> This [[historical novel]], influenced by those of [[Walter Scott|Sir Walter Scott]],<ref>Spark, 201; Lynch, 135-41. Mary Shelley consulted Scott while writing the book.</ref> fictionalises the exploits of Perkin Warbeck, a pretender to the throne of [[Henry VII of England|King Henry VII]] who claimed to be [[Richard of Shrewsbury, 1st Duke of York|Richard, Duke of York]], the second son of [[Edward IV of England|King Edward IV]]. Shelley believed that Warbeck really was Richard and had escaped from the [[Tower of London]].<ref>"It is not singular that I should entertain a belief that Perkin was, in reality, the lost Duke of York ... no person who has at all studied the subject but arrives at the same conclusion." Mary Shelley, Preface to ''Perkin Warbeck'', vi–vii, quoted in Bunnell, 131.</ref> She endows his character with elements of Percy Shelley, portraying him sympathetically as "an [[angel]]ic essence, incapable of wound", who is led by his sensibility onto the political stage.<ref>Bunnell, 132; Brewer, "''Perkin Warbeck''".</ref> She seems to have identified herself with Richard's wife, Lady Katherine Gordon, who survives after her husband's death by compromising with his political enemies.<ref>Wake, 246–47; Brewer, "''Perkin Warbeck''".</ref> Lady Gordon stands for the values of friendship, domesticity and equality; through her, Mary Shelley offers a female alternative to the masculine power politics that destroy Richard, as well as the typical historical narrative which only relates those events.<ref>Bunnell, 132; Lynch, 143-44.</ref>  She also creates a strong female character in the round-faced, half-Moor, half-Fleming, Monina de Faro, Richard's adoptive sister, whom Robin Clifford demands as his wife.  Monina is a versatile young lady who acts as decoy, messenger, and military organizer, in addition to her close friendship with both Richard and Katherine.  Robin Clifford epitomizes mixed loyalties—an old friend descended from Lancastrians, who is constantly divided against himself.  Stephen Frion, secretary to Henry VII and betrayed by him, is an elder foil, whose loyalties shift back and forth dependent on Henry's grace, whereas Clifford's wavering is based on genuine emotion.

The book opens immediately after the [[Battle of Bosworth]] on August 22, 1485 (a scanning error in the [[Dodo Press]] 2000 edition gives the date as 1415).  Three knights are fleeing from the battle, Sir Henry Stafford, Lord Lovel, and Edmund Plantagent, although the latter two are not identified until they split from Stafford and arrive at a church.  All three are members of the defeated Yorkist contingency.  With the aid of John de la Poole, the Earl of Lincoln, Lovel and Edumund are involved in spiriting away Richard, Duke of York into the hands of Mynheer Jahn Warbeck, a Flemish moneylender who had previously housed him and pretended that Richard was his deceased son, Perkin Warbeck.  This is not considered safe enough for the youth at the present time, so it is arranged for Richard to go with Madeline de Faro, Warbeck's 25-year-old sister.  Madeline is married to mariner Hernan de Faro, and the two have a daughter named Monina, and Richard and Monina develop a strong sibling bond, Richard aware he could never marry a commoner.  It is she who rescues and nurses him back to health after his first taste of battle in the [[Granada War]].

==Characters==

*[[Richard of Shrewsbury, 1st Duke of York]], son of King Edward IV and nephew of King Richard III 
*[[Perkin Warbeck]], deceased son of Mynheer Jahn Warbeck, and alias of Richard
*[[Lady Catherine Gordon|Lady Katherine Gordon]], Richard's wife, and cousin of James, daughter of Lord Huntley
*[[Monina de Faro]], adoptive sister of Richard and close friend to Lady Katherine
*[[Edmund, Earl of Rutland|Edmund Plantagenet]], bastard son of Richard III, cousin and close ally of Richard
*[[Stephen Frion]], French-born secretary of Henry VII and opportunistic enemy/ally of Richard
*Sir Robert "Robin" Clifford, alternate friend/betrayer of Richard
*[[James IV of Scotland]], friend to Richard
*[[Madeline Warbeck de Faro]], wife of Hernan de Faro, mother of Monina, adoptive mother of Richard, and sister of Mynheer Jahn Warbeck
*[[Hernan de Faro]], a [[Moors|Moor]]ish sailor converted to [[Christianity]], husband of Madeline, father of Monina, adoptive father of Richard
*[[Henry VII of England]], Earl of Richmond and first [[Tudor dynasty|Tudor]] King of England
*[[Elizabeth of York]], wife of Henry VII and sister of Richard
*[[Elizabeth Woodville]], mother of Richard and former queen: widow of Edward IV
*[[Jane Shore]], mistress of Edward IV, Richard's father
*[[Edward Plantagenet, 17th Earl of Warwick]], son of George, Duke of Clarence, prisoner of Henry VII
*[[John de la Pole, 1st Earl of Lincoln|John de la Poole, Earl of Lincoln]]
*[[Lady Margaret Brampton]], ally of Richard
*[[Sir Edward Brampton]], her husband
*[[Arthur, Prince of Wales]], eldest son of Henry VII and Elizabeth of York
*[[Margaret Tudor]], eldest daughter of Henry and Elizabeth
*[[Henry VIII of England|Prince Harry]], second son of Henry and Elizabeth
*[[Thomas Grey, 1st Marquess of Dorset]], son of Elizabeth Woodville by her first marriage 
*[[Thomas Stanley, 1st Earl of Derby]] 
*[[John de Vere, 13th Earl of Oxford]]
*[[Henry Stafford, 2nd Duke of Buckingham]]
*[[Francis Lovell, 1st Viscount Lovell|Lord Lovel]]
*[[John Morton (bishop)|John Morton]], [[Bishop of Ely]], close ally of Henry VII
*[[Richard Foxe|Richard Fox]], Bishop of Exeter, Bath and Wells, Durham, and Winchester, ally of Henry VII 
*[[Christopher Urswick]]
*[[Richard Simon (priest)|Richard Simon]]
*[[Lambert Simnel]]
*[[Mynheer Jahn Warbeck]], father of Perkin Warbeck
*[[Charles the Bold]]
*[[Isabella I of Castile]]
*[[Ferdinand II of Aragon]]
*[[Louis XI of France]]
*[[Jasper Tudor, 1st Duke of Bedford]] 
*[[Sir Thomas Broughton]]
*[[Mary of Burgundy]]
*[[Lord Barry]], ally of Richard
*[[William Stanley (Battle of Bosworth)|Sir William Stanley]], ally of Richard
*[[Meiler Trangmar]], assassin disguised as a monk
*[[Maurice FitzGerald, 9th Earl of Desmond]], ally of Richard
*[[John Lavallan]], Lord Mayor of Dublin and ally of Richard
*[[John O'Water]], previous and subsequent Lord Mayor of Dublin, ally of Richard
*[[Edward Stafford, 3rd Duke of Buckingham]]
*[[George Gordon, 2nd Earl of Huntly|Lord Huntley]], father of Katherine
*[[John Ramsay, 1st Lord Bothwell]], Laird of Kilmaine and spy of Henry VII at the court of James IV
*[[Alexander Stewart, 2nd Earl of Buchan]], ally of Ramsay
*[[Lord Broke]]
*[[Charles the Rash of Burgundy]]
*[[Margaret of York]], Richard's aunt
*[[Thomas Geraldine, Earl of Kildare]]
*[[Martin Swartz]]
*[[René of Anjou]]
*[[John Radcliffe, 9th Baron FitzWalter]]
*[[Don Rodrigo Ponce de Leon, Marquess of Cadiz]]
*[[Bartholomew Diaz]]
*[[Sire de Beverem]]
*[[Boabdil el Chico]]
*[[El Zagal]]
*[[El Zogoybi]]
*[[Count de Tendilla]]
*[[Almoradi Gomelez]]
*[[Charles VIII of France]]
*[[Anne of Brittany]]
*[[Hubert Burgh]]
*[[Sir James Keating]], prior of Kilmainham and ally of Richard 
*[[Richard Fitzroy]]
*[[Sir Simon Mountford]]
*[[Sir Thomas Thwaites]]
*[[Sir Robert Ratcliffe]]
*[[Sir Richard Lessey]]
*[[William Worseley, Dean of St. Paul's]]
*[[Master William Barley]]
*[[George Nevill, 5th Baron Bergavenny|Baron George Neville]]. ally of Richard
*[[Maximilian I, Holy Roman Emperor]]
*[[Adam Floyer]]
*[[Sir William Daubeny|Lord William Dawbenny]]
*[[Thomas Cressenor]]
*[[Thomas Astwood]]
*[[William Richford]]
*[[Thomas Poyns]]
*[[Doctor William Sutton]]
*[[Robert Langborne]]
*[[Sir William Lessey]],
*[[Gilbert Daubeny|Gilbert Dawbenny]], brother of William
*[[Sir Edward Lisle]]
*[[John Tate (mayor)|John Tate]], [[Lord Mayor of London]]
*[[Thomas Howard, 3rd Duke of Norfolk, 2nd Earl of Surrey]]
*[[Sir John Digby]], Lieutenant of the Tower of London
*[[Sir John Peachy]]
*[[Lord Astley]] 
*[[Sir Patrick Hamilton of Kincavil]], ally of Richard
*[[Mary Boyd]], suitor of James 
*[[Lady Jane Kennedy]], suitor of James
*[[James Tuchet, 7th Baron Audley|Lord Audley]]
*[[Anne de Mowbray, 8th Countess of Norfolk]]
*[[John de Mowbray, 4th Duke of Norfolk]]
*[[William Hay, 3rd Earl of Erroll|Earl of Errol]]
*[[James Douglas, 9th Earl of Douglas and 3rd Earl of Avondale|Earl of Douglas]]
*[[Sir Thomas Todd]]
*[[Sir Roderick-de-Lalane]]
*[[Andrew Stewart, Bishop of Moray]]
*[[Master Heron]], lieutenant of Richard chosen by Monina de Faro
*[[Master Skelton]], lieutenant of Richard chosen by Monina de Faro
*[[Master Treireife]], lieutenant of Richard chosen by Monina de Faro
*[[William Courtenay, 1st Earl of Devon]], ally of Henry VII
*[[Adam Wicherly]]
*[[Mat Oldcraft]]
*[[John de Vere, 15th Earl of Oxford]]
*[[Empson]]
*[[Garthe]]
*[[John Cheyne, Baron Cheyne|John Cheney]] 
*[[Sir Harry de Vere]]
*[[Clim of Tregothius]]
*Swartz (son of Martin)
*[[Clym of the Lyn]], a forester and ally of Richard
*[[Sir Hugh Luttrell]], Lancastrian ordered to take Richard prisoner
*[[Long Roger]], prisoner in the Tower of London who aids in Edward and Richard's escape attempt
*[[Dame Madge]], Long Roger's wife ([[unseen character]])
*[[Abel Blewet]], prisoner in the Tower of London who aids in Edward and Richard's escape attempt, a murderous near-dwarf
*[[Mat Strangeways]], prisoner in the Tower of London who aids in Edward and Richard's escape attempt, a drunk
*[[Master Astwood]], prisoner in the Tower of London who aids in Edward and Richard's escape attempt, a miser

===in flashbacks===
*[[Richard III of England]], Richard's paternal uncle, who allegedly orchestrated his murder
*[[Anthony Woodville, 2nd Earl Rivers]], Richard's maternal uncle, whose death was orchestrated by Richard III
*[[Edward V of England]], Richard's older brother
*[[George Plantagenet, 1st Duke of Clarence]], Richard's paternal uncle, whose death was orchestrated by Richard III
*[[Sir James Tyrrel|Sir James Tirell]], vassal of Richard III whom he was alleged to have hired to kill Richard 
*[[John Dighton]], servant of Tirell and alleged murderer of Richard
*[[James III of Scotland]], father of James IV
*[[Thomas de Mowbray, 1st Duke of Norfolk]]
*[[Roger de Clifford, 5th Baron de Clifford]]
*[[Lady Maud Clifford]]
*[[Mistress Margery]], Richard's governess

==Quotations==
Each chapter opens with a quotation, sometimes two.  The quotations come from the following authors:

*[[Edmund Spenser]], (I: 1, 5, 6, 15; II: 15; III: 10, 13, 15, 20)
*[[William Shakespeare]] (usually spelled "Shakspeare"), (I: 2, 3, 4, 11, 13, 17; II: 2, 3, 4, 6, 8, 9, 10, 13, 17, III: 3, 5, 6, 7, 8, 9, 12, 16, 17, Conclusion)
*[[Percy Bysshe Shelley]], (I: 5, 12; II: 5, 9, III: 2, 21)
*[[Francis Beaumont]] and [[John Fletcher (playwright)|John Fletcher]], (I: 7)
*Old Ballad, (I: 8, 9; III: 9)
*[[Lord Byron]], (I: 9; III: 18)
*[[Homer]]'s [[Homeric Hymns|Hymn]] to Mercury, (I: 10)
*[[The Cyclops (Percy Bysshe Shelley)|The Cyclops]] [ [[Percy Bysshe Shelley]] ], (I: 10)
*[[Thomas Moore]], (I: 12; III: 4)
*[[Geoffrey Chaucer]], (I: 14)
*[[Samuel Taylor Coleridge]], (I: 16, 18)
*[[John Ford (dramatist)|John Ford]], (I: 17; II: 9, 14, 18; III: 1, 6)
*[[The Heir of Linne|The Heir of Lynne]], <!--Shelley's spelling--> (II: 1)
*''[[Two Noble Kinsmen]]'', [John Fletcher and William Shakespeare] (II: 7, III: 14, 19) 
*[[Ballad of Jane Shore]], (II: 8)
*[[Ben Jonson]],  (II: 16)
*[[Friedrich Schiller]]'s ''[[Wallenstein (play)|Wallenstein]]'' (III: 1, 8)

==Notes==
{{reflist}}

==Bibliography==
*Bennett, Betty T. "The Political Philosophy of Mary Shelley's Historical novels: ''Valperga'' and ''Perkin Warbeck''". ''The Evidence of the Imagination''. Eds. Donald H. Reiman, Michael C. Jaye, and Betty T. Bennett. New York: New York University Press, 1978.
*Brewer, William D. "[http://findarticles.com/p/articles/mi_qa3708/is_199904/ai_n8844841/pg_1 William Godwin, Chivalry, and Mary Shelley's ''The Fortunes of Perkin Warbeck'']".  ''Papers on Language and Literature'' 35.2 (Spring 1999): 187-205. Rpt. on bnet.com. Retrieved on 20 February 2008.
*Bunnell, Charlene E. ''"All the World's a Stage": Dramatic Sensibility in Mary Shelley's Novels.'' New York: Routledge, 2002. ISBN 0-415-93863-5.
*Garbin, Lidia. "Mary Shelley and Walter Scott: ''The Fortunes of Perkin Warbeck'' and the Historical Novel". ''Mary Shelley's Fiction: From Frankenstein to Falkner''. Eds. Michael Eberle-Sinatra and Nora Crook. New York: Macmillan; St. Martin's, 2000.
*Hopkins, Lisa. "The Self and the Monstrous". ''Iconoclastic Departures: Mary Shelley after "Frankenstein": Essays in Honor of the Bicentenary of Mary Shelley's Birth''. Eds. Syndy M. Conger, Frederick S. Frank, and Gregory O'Dea. Madison, NJ: Fairleigh Dickinson University Press, 1997. 
*Lynch, Deidre. "Historical novelist". ''The Cambridge Companion to Mary Shelley''. Ed. Esther Schor. Cambridge: Cambridge University Press, 2003. ISBN 0-521-00770-4.
*Sites, Melissa. "Chivalry and Utopian Domesticity in Mary Shelley's ''The Fortunes of Perkin Warbeck''". ''European Romantic Review'' 16.5 (2005): 525-43.
*[[Muriel Spark|Spark, Muriel]]. ''Mary Shelley''. London: Cardinal, 1987. ISBN 0-7474-0318-X.
*Wake, Ann M Frank. "Women in the Active Voice: Recovering Female History in Mary Shelley's ''Valperga'' and ''Perkin Warbeck''". ''Iconoclastic Departures: Mary Shelley after "Frankenstein". Essays in Honor of the Bicentenary of Mary Shelley's Birth.'' Ed. Syndy M. Conger, Frederick S. Frank, and Gregory O'Dea. Madison, NJ: Farleigh Dickinson University Press, 1997. ISBN 0-8386-3684-5.

==External links==
*[https://archive.org/details/fortunesofperkin02sheluoft ''The Fortunes of Perkin Warbeck'' (1830), Volume II] from the [[Internet Archive]]
*[https://archive.org/details/fortunesofperkin00sheluoft ''The Fortunes of Perkin Warbeck'' (1830), Volume III] from the Internet Archive
*[https://books.google.com/books?id=na4BAAAAQAAJ&dq=inauthor:Mary+inauthor:Shelley&lr=&as_brr=1 ''The Fortunes of Perkin Warbeck'' (1857)] from [[Google Books]]

{{Mary Shelley}}

{{DEFAULTSORT:Fortunes of Perkin Warbeck, The}}
[[Category:1830 novels]]
[[Category:Historical novels]]
[[Category:Novels by Mary Shelley]]
[[Category:19th-century British novels]]