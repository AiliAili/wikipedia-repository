{{Infobox journal
| title         = Human Molecular Genetics
| cover         = [[File:Human Molecular Genetics.gif]]
| editor        = Kay Davies, Anthony Wynshaw-Boris, and Joel Hirschhorn
| discipline    = [[Molecular genetics]]
| peer-reviewed = 
| language      = [[English language|English]] 
| abbreviation  = Hum. Mol. Gen., Hum. mol. genet., HMG
| publisher     = [[Oxford University Press]]
| country       = Great Britain 
| frequency     = Semi-monthly 
| history       = 1992-present
| openaccess    =some
| license       = 
| impact        = 5.985
| impact-year   = 2015
| website       = http://hmg.oxfordjournals.org/
| link1         = http://hmg.oxfordjournals.org/cgi/search?pubdate_year=&firstpage=&DOI=&author1=&author2=&title=&andorexacttitle=and&titleabstract=&andorexacttitleabs=phrase&fulltext=Open+Access+article+distributed+under&andorexactfulltext=phrase&fmonth=Jan&fyear=2005&ttyear=2010&fdatedef=1+January+1985&tdatedef=31+December+2010&flag=&RESULTFORMAT=1&hits=10&hitsbrief=25&sortspec=date&sortspecbrief=relevance&sendit=Search 
| link1-name    =Open access articles
| link2         = 
| link2-name    = 
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 39511714 
| LCCN          = 92643389  
| CODEN         = 
| ISSN          = 0964-6906
| eISSN         = 1460-2083
| boxwidth      = 
}}

'''''Human Molecular Genetics''''', first published in 1992, is a semimonthly [[peer reviewed]],  [[scientific journal]], published by The [[Oxford University Press]]. The journal's focus is [[research papers]] on all topics related to [[Genetics|human molecular genetics]]. In addition, two "''special review''" issues are published each year. There are three professors who share the title of  [[Executive Editor]] for this journal: Professor Kay Davies from the [[University of Oxford]], Professor Anthony Wynshaw-Boris from the [[University of California]] in San Francisco, and Professor Joel Hirschhorn  from [[Harvard Medical School]].<ref name=home>[http://hmg.oxfordjournals.org/ Home page. Oxford Journals. July 2010.]</ref>

This journal was first published as Volume 1 Number 1 in April 1992 by IRL Press of Oxford England.<ref name=LCCN>[http://lccn.loc.gov/92643389 Human molecular genetics]. Library of Congress. 2010.</ref>

==Impact factor==
The impact factor for 2006 was 8.099, for 2007 it was 7.806, for 2008 it was 7.249. The current impact factor for 2014 is 6.393.<ref name=about>[http://www.oxfordjournals.org/our_journals/hmg/about.html  About the journal]. Oxford Journals.</ref>

==Indexing==
''Human Molecular Genetics'' is indexed in:<ref name=about/> 
{{columns-list|2|
*[[Abstracts in Anthropology]]
*[[Agbiotech News and Information]]
*[[Animal Breeding Abstracts]]
*[[Biological Abstracts]]
*[[Biotechnology Citation Index]]
*[[BIOSIS Previews]]
*[[CAB Abstracts]]
*[[Calcium and Calcified Tissue Abstracts]]
*[[Chemical Abstracts]]
*[[Current Contents]]/Life Sciences
*[[EMBASE]]
*[[Genetics Abstracts]]
*[[Global Health]]
*[[Helminthology Abstracts]]
*[[Journal Citation Reports]]/Science Edition
*[[Proquest Database]]
*[[Science Citation Index Expanded]] ([[SciSearch]])
*[[Science Citation Index]]
*[[The Standard Periodical Directory]]
*[[Tropical Diseases Bulletin]]
*[[Weed Abstracts]]
}}

==References==
{{Reflist}}

==External links==
*[http://hmg.oxfordjournals.org/ Official Website]

[[Category:Oxford University Press academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1972]]
[[Category:English-language journals]]
[[Category:Genetics journals]]

{{biology-journal-stub}}