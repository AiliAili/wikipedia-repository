{{Infobox journal
| title = Biochimica et Biophysica Acta
| cover =
| editor =
| discipline = [[Biochemistry]]
| abbreviation = Biochim. Biophys. Acta
| publisher = [[Elsevier]]
| country =
| frequency = 100/year
| history = 1947-present
| openaccess = Hybrid
| impact =
| impact-year =
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/506062/description#description
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC =
| LCCN =
| CODEN =
| ISSN = 0006-3002
| eISSN =
}}

'''''Biochimica et Biophysica Acta''''' ('''''BBA''''') is a [[peer review|peer-reviewed]] [[scientific journal]] in the field of [[biochemistry]] and [[biophysics]] that was established in 1947. The journal is published by [[Elsevier]] with a total of 100 annual issues in nine specialised sections.

==History==
===Early years===
''Biochimica et Biophysica Acta'' was first published in 1947 and was the first international journal to be devoted to the joint fields of [[biochemistry]] and [[biophysics]].<ref name=Elsevier_history>[http://wayback.archive-it.org/all/20081217175944/http://shop.elsevier.de/sixcms/media.php/792/ElsevierHistory.pdf A short history of Elsevier (Elsevier; 2005)] (accessed 12 December 2008)</ref> Published by [[Elsevier]] in cooperation with [[John Wiley & Sons|Interscience]], it was the first international journal to be launched by Elsevier.<ref name=Elsevier_history /> The journal first made a profit in 1951.<ref name=Elsevier_history />

Early papers were published in English, French, and German, with summaries in all three languages.<ref>[http://www.ncbi.nlm.nih.gov/sites/entrez?Db=nlmcatalog NLM Catalog] (search on "0217513[NlmId]") (accessed 12 December 2008)</ref> The majority of papers in the first volume originated in northern and western Europe, with a minority from the USA and elsewhere; contributors included [[William Astbury]], [[Jean Brachet]], [[Hubert Chantrenne]], [[Pierre Desnuelle]], [[Claude Fromageot]], [[Heinz Holter]], [[Raymond Jeener]], [[Felix Haurowitz]], [[Edgar Lederer]], [[Kaj Ulrik Linderstrøm-Lang|Kaj Linderstrøm-Lang]], [[Roger Vendrely]], [[Jean-Marie Wiame]], and [[Ralph Walter Graystone Wyckoff|Ralph W.G. Wyckoff]].<ref>[http://www.sciencedirect.com/science?_ob=PublicationURL&_tockey=%23TOC%2311521%231947%23999989999%23370474%23FLP%23&_cdi=11521&_pubType=J&_auth=y&_acct=C000050221&_version=1&_urlVersion=0&_userid=10&md5=71ed5abbd350785a14b908ccaf8c6670 ''Biochimica et Biophysica Acta'' Vol. 1] (accessed 12 December 2008)</ref>{{Or|date=May 2011}}

Important papers from these early years include "Studies on the structure of ribonucleic acids" by [[Boris Magasanik]] and [[Erwin Chargaff]] (1951),<ref>Magasanik B, Chargaff E. (1951) Studies on the structure of ribonucleic acids. ''Biochem Biophys Acta'' 7: 396–412 ([http://www.sciencedirect.com/science?_ob=ArticleURL&_udi=B73G9-47DVBY9-3X&_user=10&_coverDate=12%2F31%2F1951&_rdoc=43&_fmt=high&_orig=browse&_srch=doc-info(%23toc%2311521%231951%23999929999%23370440%23FLP%23display%23Volume)&_cdi=11521&_sort=d&_docanchor=&_ct=82&_acct=C000050221&_version=1&_urlVersion=0&_userid=10&md5=08271f538e4b33bcc78ef71719e07538 abstract])</ref> part of the evidence on which [[Molecular Structure of Nucleic Acids: A Structure for Deoxyribose Nucleic Acid|Watson and Crick's model]] of the structure of [[DNA]] was based, and "Enzymic synthesis of deoxyribonucleic acid" by [[Arthur Kornberg]] and colleagues (1956),<ref>Kornberg A, Lehman IR, Bessman MJ, Simms ES. (1956) Enzymic synthesis of deoxyribonucleic acid. ''Biochem Biophys Acta'' 21: 197–198</ref> an early report on the isolation of [[DNA polymerase I]].<ref name=Elsevier_history />

===Diversification===
[[File:BBA cover.gif|thumb|Early cover of the ''General Subjects'' section]]
''Biochimica et Biophysica Acta'' was published as a single title until 1962, when additional sections began to be published alongside the main journal: first ''Specialized Section on Nucleic Acids and Related Subjects'' and then, from 1963, ''Specialized Section on Enzymological Subjects'' and ''Specialized Section on Lipids and Related Subjects''.<ref name=ohsu>[http://www.ohsu.edu/library/ejournals/bba2revised.shtml OHSU Library: BBA Decoder] (accessed 10 December 2008)</ref>

In 1964, the main journal became ''Biochimica et Biophysica Acta (BBA) – General Subjects'', and was published alongside the three established sections plus ''Specialized Section on Biophysical Subjects'' and ''Specialized Section on Mucoproteins and Mucopolysaccharides''. In 1965, the specialist sections were renamed, becoming ''Biophysics including Photosynthesis'', ''Nucleic Acids and Protein Synthesis'', ''Enzymology and Biological Oxidation'', ''Lipids and Lipid Metabolism'' and ''Mucoproteins and Mucopolysaccharides'' (ceased in 1965). In 1967, ''Biophysics including Photosynthesis'' split into ''Bioenergetics'' and ''Biomembranes'', and ''Enzymology and Biological Oxidation'' split into ''Enzymology'' and ''Protein Structure''; the latter pair rejoined in 1982 to become ''Protein Structure and Molecular Enzymology''. Further sections were ''Molecular Cell Research'', launched in 1982, and ''Molecular Basis of Disease'', launched in 1990.<ref name=ohsu />

In addition to the specialised research sections, three review sections were launched in the early 1970s: ''Reviews on Biomembranes'' (1972–2000), ''Reviews on Bioenergetics'' (1973–87) and ''Reviews on Cancer'' (from 1974). The former two were later incorporated into the respective research sections.<ref name=ohsu />

Further name changes are given in the table in the following section.

==Modern journal==
As of 2008, ''Biochimica et Biophysica Acta'' encompasses nine specialised sections with a total of 100 annual issues in ten volumes. Over 16,000 pages were published in 2011. The journal sections are published separately, with one annual volume per section (two for ''Reviews on Cancer''), but form part of the volume numbering for ''Biochimica et Biophysica Acta''.<ref name=ohsu /> Sections are available individually or as part of a combined subscription. All papers are in English.<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/506062/authorinstructions Elsevier: ''BBA – Biochimica et Biophysica Acta'': Guide for Authors] (accessed 13 December 2008)]</ref> The overall [[editor-in-chief]] is [[Ulrich Brandt]] ([[Goethe University Frankfurt]], Germany).<ref>[http://www.elsevier.com/wps/find/journaleditorialboard.cws_home/506062/editorialboard Elsevier: ''BBA – Biochimica et Biophysica Acta'': Editorial Board] (accessed 12 December 2008)</ref>

The sections published in 2014 were as follows:<ref name=ohsu />

{| class="wikitable" style="text-align:left"
|-
! Name
! ISSN
! Annual issues
! [[Impact factor]]<ref name=WoS>{{cite web |url=http://isiwebofknowledge.com |title=Web of Science |year=2014 |accessdate=2014-07-08}}</ref>
! Notes
|-
|''BBA – Bioenergetics''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/506063/description#description Elsevier: ''BBA – Bioenergetics''] (accessed 21 April 2011)</ref>
|{{ISSN|0005-2728}}
|12
|5.353
|Commenced 1967; formerly part of ''BBA – Biophysics including Photosynthesis'' (1965–1966). Incorporates ''BBA – Reviews on Bioenergetics''
|-
|''BBA – Biomembranes''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/601272/description#description Elsevier: ''BBA – Biomembranes''] (accessed 21 April 2011)</ref>
|{{ISSN|0005-2736}}
|12
|3.836
|Commenced 1967; formerly part of ''BBA – Biophysics including Photosynthesis'' (1965–6). Incorporates ''BBA – Reviews on Biomembranes''
|-
|''BBA – Gene Regulatory Mechanisms''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/713381/description#description Elsevier: ''BBA - Gene Regulatory Mechanisms''] (accessed 21 April 2011)</ref>
|{{ISSN|1874-9399}}
|12
|6.332
|Commenced 2008; continuation of ''BBA – Gene Structure and Expression'' (0167-4781; 1982–2007), ''BBA – Nucleic Acids and Protein Synthesis'' (1963–1981) and ''BBA – Specialized Section on Nucleic Acids and Related Subjects'' (1962–4)
|-
|''BBA – General Subjects''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/506066/description#description Elsevier: ''BBA – General Subjects''] (accessed 21 April 2011)</ref>
|{{ISSN|0304-4165}}
|12
|4.381
|Commenced 1964; continuation of ''Biochimica et Biophysica Acta''
|-
|''BBA – Molecular and Cell Biology of Lipids''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/506067/description#description Elsevier: ''BBA – Molecular and Cell Biology of Lipids''] (accessed 21 April 2011)</ref>
|{{ISSN|1388-1981}}
|12
|5.162
|Commenced 1998; continuation of ''BBA – Lipids and Lipid Metabolism'' (1965–98) and ''BBA – Specialized Section on Lipids and Related Subjects'' (1963–4)
|-
|''BBA – Molecular Basis of Disease''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/506068/description#description Elsevier: ''BBA – Molecular Basis of Disease''] (accessed 21 April 2011)</ref>
|{{ISSN|0925-4439}}
|12
|4.882
|Commenced 1990
|-
|''BBA – Molecular Cell Research''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/506069/description#description Elsevier: ''BBA – Molecular Cell Research''] (accessed 21 April 2011)</ref>
|{{ISSN|0167-4889}}
|12
|5.019
|Commenced 1982
|-
|''BBA – Proteins and Proteomics''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/635707/description#description Elsevier: ''BBA – Proteins and Proteomics''] (accessed 21 April 2011)</ref>
|{{ISSN|1570-9639}}
|12
|3.016
|Commenced 2002; continuation of ''BBA – Protein Structure and Molecular Enzymology'' (1982–2002), ''BBA – Enzymology'' (1967–81), ''BBA – Protein Structure'' (1967–81), ''BBA – Enzymology and Biological Oxidation'' (1965–66), ''BBA – Specialized Section on Enzymological Subjects'' (1963–4)
|-
|''BBA – Reviews on Cancer''<ref>[http://www.elsevier.com/wps/find/journaldescription.cws_home/506072/description#description Elsevier: ''BBA – Reviews on Cancer''] (accessed 21 April 2011)</ref>
|{{ISSN|0304-419X}}
|4
|7.845
|Commenced 1974
|-
|}

===Indexing and online journal===
''BBA'' is abstracted and indexed by [[BIOSIS Previews|BIOSIS]], [[Chemical Abstracts Service]], [[Current Contents]]/Life Sciences, [[EMBASE]], [[EMBiology]], [[Index Chemicus]], [[MEDLINE]]/[[Index Medicus]], [[Science Citation Index]], and [[Sociedad Iberoamericana de Informacion Cientifica]].<ref>[http://www.elsevier.com/wps/find/journalabstracting.cws_home/506062/abstracting#abstracting Elsevier: ''Biochimica et Biophysica Acta'': Abstracting and Indexing] (accessed 10 December 2008)</ref>

Articles are available online as [[Portable Document Format|PDF]]s and [[HTML]]; access is largely limited to subscribers, with a small number of sponsored [[Open access (publishing)|open-access]] articles.

==References==
{{reflist|2}}

==Further reading==
*{{cite book
 |author=E.C. Slater
 |year=1986
 |title=Biochimica et Biophysica Acta: The Story of a Biochemical Journal
 |publisher=[[Elsevier]]
 |isbn=0-444-80769-1
}}

==External links==
* {{Official website|1=http://www.elsevier.com/wps/find/journaldescription.cws_home/506062/description#description}}
* [http://www.elsevier.com/wps/find/L03.cws_home/bba_si_overview ''BBA Special Issue Overview'' website]
{{Use dmy dates|date=May 2011}}

[[Category:Elsevier academic journals]]
[[Category:Publications established in 1947]]
[[Category:Biochemistry journals]]
[[Category:English-language journals]]
[[Category:Journals more frequent than weekly]]