{{good article}}
{{Use mdy dates|date=August 2016}}
{{Italic title|reason=[[:Category:Vietnamese words and phrases]]}}
{{Infobox clothing type
| name          = Áo dài
| image_file    = KOCIS Korea Hanbok-AoDai FashionShow 03 (9766157012).jpg
| image_size    =
| caption       = 
| type          = Dress
| material      = Silk
| location      = Vietnam
| manufacturer  =
| url           =
}}
{{Infobox Chinese
|vie = áo dài
|hn = {{linktext|襖|𨱾}}
}}

The '''''áo dài''''' is a [[Vietnam]]ese [[national costume]], now most commonly worn by women but can also be worn by men. In its current form, it is a tight-fitting [[silk]] tunic worn over [[Trousers|pants]]. The word is pronounced {{IPA-vi|ʔǎːw zâːj|}} in the North and {{IPA-vi|ʔǎːw jâːj|}} in the South. ''Áo'' classifies as shirt.<ref name="AmerHerit">''Áo'' is derived from a [[Middle Chinese]] word meaning "padded coat". "[http://www.oxforddictionaries.com/definition/english/ao-dai ao dai]", ''definition of ao dai in Oxford dictionary (British & World English)''. Retrieved November 3, 2014.</ref> ''Dài'' means "long".<ref name="Tuttle">Phan Van Giuong, ''Tuttle Compact Vietnamese Dictionary: Vietnamese-English English-Vietnamese'' (2008), p. 76. "'''dài''' ''adj.'' long, lengthy."</ref>

The word "ao dai" was originally applied to the outfit worn at the court of the [[Nguyễn lords|Nguyễn Lords]] at [[Huế]] in the 18th century. This outfit evolved into the ''áo ngũ thân'', a five-paneled [[Aristocracy (class)|aristocratic]] [[gown]] worn in the 19th and early 20th centuries. Inspired by Paris fashions, Nguyễn Cát Tường and other artists associated with [[Hanoi University]] redesigned the ''ngũ thân'' as a modern dress in the 1920s and 1930s.<ref name="Ellis">{{cite web | last = Ellis | first = Claire | title = Ao Dai: The National Costume | newspaper = Things Asian | year = 1996 | url = http://www.thingsasian.com/stories-photos/1083 | accessdate=August 2, 2008}}
</ref> The updated look was promoted by the artists and magazines of [[Tự Lực văn đoàn]] (Self-Reliant Literary Group) as a national costume for the modern era. In the 1950s, [[Saigon]] designers tightened the fit to produce the version worn by [[Women in Vietnam|Vietnamese women]] today.<ref name="Ellis">{{cite web | last = Ellis | first = Claire | title = Ao Dai: The National Costume | newspaper = Things Asian | year = 1996 | url = http://www.thingsasian.com/stories-photos/1083 | accessdate=August 2, 2008}}
</ref> The dress was extremely popular in [[South Vietnam]] in the 1960s and early 1970s. On [[Tết]] and other occasions, Vietnamese men may wear an ''[[áo gấm]]'' ([[brocade]] [[robe]]), a version of the ao dai made of thicker fabric.

Academic commentary on the ao dai emphasizes the way the dress ties feminine beauty to Vietnamese nationalism, especially in the form of "Miss Ao Dai" pageants, popular both among [[overseas Vietnamese]] and in Vietnam itself.<ref name="Lieu">{{Cite journal | last = Lieu | first = Nhi T. | title = Remembering "the Nation" through pageantry: femininity and the politics of Vietnamese womanhood in the "Hoa Hau Ao Dai" contest | journal = [[Frontiers: A Journal of Women's Studies]] | volume = 21 | issue = 1-2 | pages = 127–151 | publisher = University of Nebraska Press via JSTOR | doi = 10.2307/3347038 | date = 2000 | url = http://dx.doi.org/10.2307/3347038 | ref = harv | postscript = .}}</ref> "Ao dai" is one of the few Vietnamese words that appear in English-language dictionaries.{{efn|"Ao dai" appears in the ''Oxford English Dictionary'', the ''American Heritage Dictionary'' (2004), and the ''Random House Unabridged Dictionary'' (2006). Other Vietnamese words that appear include "[[Tết|Tet]]", "[[Vietminh]]", "[[Vietcong]]", and "[[pho]]" (rice noodles).[http://www.signonsandiego.com/news/world/20070919-1502-britain-newwords.html]}}

== Parts of dress ==
[[File:Aodai.PNG|thumb|Diagram that shows the parts of an ao dai.]]
''Tà sau'': back flap
:''Nút bấm thân áo'': hooks used as fasteners and holes
:''Ống tay'': sleeve
:''Đường bên'': inside seam
:''Nút móc kết thúc'': main hook and hole
''Tà trước'': front flap
:''Khuy cổ'': collar button
:''Cổ áo'': collar
:''Đường may'': seam
:''Kích (eo)'': waist
The ao dai can be worn with a ''nón lá'' (conical leaf hat), a style associated with Huế. On weddings and other formal occasions, a circular headgear called a ''khăn đóng'' is worn.

== History ==

=== Before the Nguyen Dynasty ===
[[File:Lord Nguyen Phuc Thuan.jpg|thumb|right|Portrait of Prince [[Tôn Thất Hiệp]] from the 17th century. He is dressed in a cross-collared robe (''[[áo giao lĩnh]]'') which was commonly worn by all social castes of Vietnam before the 19th century]]

For centuries, peasant women typically wore a halter top ({{lang|vi|''[[yếm]]''}}) underneath a blouse or overcoat, alongside a skirt (''váy'').<ref name="Leshkowich89">Leshkowich, p. 89.</ref> Aristocrats, on the other hand, favored a cross-collared robe called ''[[áo giao lĩnh]]'', which bore resemblance to the Chinese [[Hanfu]], Korean [[Hanbok]], and the Japanese [[kimono]].<ref name="THUYVU">{{cite web | last = Vu | first = Thuy | title = Đi tìm ngàn năm áo mũ | newspaper = Tuoi Tre | year = 2014 | url = http://tuoitre.vn/tin/chinh-tri-xa-hoi/phong-su-ky-su/20141007/di-tim-ngan-nam-ao-mu/654967.html | accessdate=June 16, 2015}}</ref><ref name="TVAN">{{cite web | last = Unknown | first = T.Van | title = Ancient costumes of Vietnamese people | newspaper = Vietnamnet | year = 2013 | url = http://english.vietnamnet.vn/fms/vietnam-in-photos/78314/ancient-costumes-of-vietnam.html | accessdate = June 16, 2015}}</ref> In 1744, Lord [[Nguyễn Phúc Khoát]] of [[Huế]] decreed that both men and women at his court wear trousers and a gown with buttons down the front.<ref name="Ellis" />{{efn|A court historian described the dress in Huế as follows: "Outside court, men and women wear gowns with straight collars and short sleeves. The sleeves are large or small depending on the wearer. There are seams on both sides running down from the sleeve, so the gown is not open anywhere. Men may wear a round collar and a short sleeve for more convenience." ("Thường phục thì đàn ông, đàn bà dùng áo cổ đứng ngắn tay, cửa ống tay rộng hoặc hẹp tùy tiện. Áo thì hai bên nách trở xuống phải khâu kín liền, không được xẻ mở. Duy đàn ông không muốn mặc áo cổ tròn ống tay hẹp cho tiện khi làm việc thì được phép…") (from ''Đại Nam Thực Lục'' [''Records of Đại Nam''])}} Writer [[Lê Quý Đôn]] described the newfangled outfit as an ''áo dài'' (long garment).{{efn|[[Lê Quý Đôn]], ''[http://openlibrary.org/b/OL16898757M/Phủ-biên-tạp-lục Phủ Biên Tạp Lục]'' [Frontier Chronicles] (1775–1776), "Lord Nguyễn Phúc Khoát wrote the first page in the history of the áo dài." "Chúa Nguyễn Phúc Khoát đã viết những trang sử đầu cho chiếc áo dài như vậy."}} The members of the southern court were thus distinguished from the courtiers of the [[Trịnh Lords]] in Hanoi, who wore ''áo giao lĩnh'' with long skirts.<ref name="THUYVU" />

Chinese Ming style clothing was forced on Vietnamese people by the [[Nguyễn dynasty]].<ref name="Woodside1971 3">{{cite book|author=Alexander Woodside|title=Vietnam and the Chinese Model: A Comparative Study of Vietnamese and Chinese Government in the First Half of the Nineteenth Century|url=https://books.google.com/books?id=0LgSI9UQNpwC&pg=PA134&dq=vietnamese+skirt+trousers+tunics#v=onepage&q=vietnamese%20skirt%20trousers%20tunics&f=false |year=1971|publisher=Harvard Univ Asia Center|isbn=978-0-674-93721-5|pages=134–}}</ref><ref>{{cite book|title=Globalization: A View by Vietnamese Consumers Through Wedding Windows |url=https://books.google.com/books?id=4bmbP5Pg5jAC&pg=PA34&dq=vietnamese+skirt+trousers+tunics#v=onepage&q=vietnamese%20skirt%20trousers%20tunics&f=false |year=2008 |publisher=ProQuest |isbn=978-0-549-68091-8 |pages=34–}}</ref><ref>http://angelasancartier.net/ao-dai-vietnams-national-dress</ref><ref>http://beyondvictoriana.com/2010/03/14/beyond-victoriana-18-transcultural-tradition-of-the-vietnamese-ao-dai/</ref><ref>http://fashion-history.lovetoknow.com/clothing-types-styles/ao-dai</ref><ref>http://www.tor.com/2010/10/20/ao-dai-and-i-steampunk-essay/</ref> The tunics and trouser clothing of the Han Chinese on the Ming and Qing tradition was worn by the Vietnamese. However, Han-Chinese clothing is assembled by several pieces of clothing including both pants and skirts called Qun (裙) or chang (裳) which is a part of [[Hanfu]] garments throughout the history of Han Chinese clothing. The Ao Dai was created when [[Tuck (sewing)|tucks]] which were close fitting and compact were added in the 1920s to this Chinese style.<ref name="Reid2015">{{cite book|author=Anthony Reid|title=A History of Southeast Asia: Critical Crossroads |url=https://books.google.com/books?id=cETJBgAAQBAJ&pg=PA285&dq=vietnamese+skirt+trousers+tunics#v=onepage&q=vietnamese%20skirt%20trousers%20tunics&f=false |date=June 2, 2015|publisher=John Wiley & Sons|isbn=978-0-631-17961-0|pages=285–}}</ref> The Chinese clothing in the form of trousers and tunic were mandated by the Vietnamese Nguyen government. It was up to the 1920s in Vietnam's north area in isolated hamlets where skirts were worn.<ref name="Rambo2005">{{cite book|author=A. Terry Rambo|title=Searching for Vietnam: Selected Writings on Vietnamese Culture and Society|url=https://books.google.com/books?id=ippuAAAAMAAJ&q=In+their+place,+the+court+demanded+the+use+of+tunic+and+trousers+copied+from+the+then+current+fashion+in+the+Chinese+court.+...+the+wearing+of+skirts+their+use+continued+in+remote+villages+in+the+Northern+part+of+Vietnam+until+the+1920s.1+In+the+less+...&dq=In+their+place,+the+court+demanded+the+use+of+tunic+and+trousers+copied+from+the+then+current+fashion+in+the+Chinese+court.+...+the+wearing+of+skirts+their+use+continued+in+remote+villages+in+the+Northern+part+of+Vietnam+until+the+1920s.1+In+the+less+...&hl=en&sa=X&ved=0ahUKEwjDt9uK7LrNAhWIQyYKHdNIDj0Q6AEIHjAA|year=2005|publisher=Kyoto University Press|isbn=978-1-920901-05-9|page=64}}</ref> The Chinese Ming dynasty, Tang dynasty, and Han dynasty clothing was referred to be adopted by Vietnamese military and bureaucrats by the Nguyen Lord [[Nguyễn Phúc Khoát]] (Nguyen The Tong).<ref name="WernerWhitmore2012">{{cite book|author1=Jayne Werner|author2=John K. Whitmore|author3=George Dutton|title=Sources of Vietnamese Tradition|url=https://books.google.com/books?id=ZHD4Asj0FagC&pg=PA295&dq=vietnamese+skirt+trousers+tunics&hl=en&sa=X&ved=0ahUKEwiV2LO26brNAhWPZiYKHcUuDwYQ6AEINjAA#v=onepage&q=vietnamese%20skirt%20trousers%20tunics&f=false|date=August 21, 2012|publisher=Columbia University Press|isbn=978-0-231-51110-0|pages=295–}}</ref> Chinese clothing started influencing Vietnamese dress in the Ly dynasty. The current Ao Dai was introduced b the Nguyen Lords.<ref>http://english.vietnamnet.vn/fms/special-reports/151794/vietnamese-ao-dai--from-dong-son-bronze-drum-to-int-l-beauty-contests.html https://www.vietnambreakingnews.com/2016/02/vietnamese-ao-dai-from-dong-son-bronze-drum-to-intl-beauty-contests/</ref>

=== 19th century ===
The ''áo ngũ thân'' had two flaps sewn together in the back, two flaps sewn together in the front, and a "baby flap" hidden underneath the main front flap. The gown appeared to have two-flaps with slits on both sides, features preserved in the later ao dai. Compared to a modern ao dai, the front and back flaps were much broader and the fit looser and much shorter. It had a high collar and was buttoned in the same fashion as a modern ao dai. Women could wear the dress with the top few buttons undone, revealing a glimpse of their ''yếm'' underneath.

<gallery widths="154px" heights="154px" perrow="4" caption="Vietnamese garments throughout the centuries:">
File:Trần Nhân Tông TLĐSXSCĐ.png|[[Tran dynasty]] robes as depicted in a section of a 14th-century scroll
File:Giảng học đồ.png|"Giảng học đồ" (Teaching), 18th century, Hanoi museum of National History. Scholars and students wear cross-collared gowns (''[[áo giao lĩnh]]'') - unlike the buttoned áo dài
File:Femme annamite coiffure tonkin crop 2.jpg|The ''[[áo tứ thân]]'' as worn in the North, 1800s
File:Ao ngu than on postcard dated 1904.JPG|Two women wear áo ngũ thân, the form of the ao dai worn in the nineteenth and early twentieth centuries
</gallery>

=== 20th century ===

==== Modernization of style ====
[[File:VN Phat Diem tango7174.jpg|thumb|Our Lady, [[Phat Diem Cathedral]]]]

[[Huế]]'s Đồng Khánh Girl's High School, which opened in 1917, was widely praised for the ao dai uniform worn by its students.<ref name="kauffner">Kauffner, Peter. "[http://visions-of-indochina.com/latestnews/wp-content/uploads/2010/07/ao-dai-article.pdf Ao dai: The allure and grace of Vietnam's traditional dress] {{webarchive |url=https://web.archive.org/web/20130522194437/http://visions-of-indochina.com/latestnews/wp-content/uploads/2010/07/ao-dai-article.pdf |date=May 22, 2013 }}", ''Asia Insights: Destination Asia'', September–October 2010</ref> The first modernized ao dai appeared at a Paris fashion show in 1921. In 1930, Hanoi artist Cát Tường, also known as Le Mur, designed a dress inspired by the ''áo ngũ thân'' and by Paris fashions. It reached to the floor and fit the curves of the body by using darts and a nipped-in waist.<ref name="Leshkowich91">Leshkowich p. 91.</ref> When fabric became inexpensive, the rationale for multiple layers and thick flaps disappeared. Modern texile manufacture allows for wider panels, eliminating the need to sew narrow panels together. The ''áo dài Le Mur'', or "trendy" ao dai, created a sensation when model Nguyễn Thị Hậu wore it for a feature published by the newspaper ''Today'' in January 1935.<ref name="ninh">{{cite web|title=A Fashion Revolution |newspaper=Ninh Thuận P&T |url=http://www.ninhthuanpt.com.vn/english/Fashion1/index.htm |accessdate=August 2, 2008 |archiveurl=https://web.archive.org/web/20080623224038/http://www.ninhthuanpt.com.vn/English/Fashion1/index.htm |archivedate=June 23, 2008 }}. For a picture of the ''áo dài Le Mur'', see [http://bichvygaugi.spaces.live.com/photos/cns!1CDCF9B5594AB224!1049/ ''Ao Dai — The Soul of Vietnam'']{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}.</ref> The style was promoted by the artists of Tự Lực văn đoàn ("Self-Reliant Literary Group") as a national costume for the modern era.<ref name="aodai4">{{cite web | title = Vietnamese Ao dai history | newspaper = Aodai4u | url = http://www.aodai4u.com/aboutaodai.html | accessdate=August 2, 2008}}</ref> The painter Lê Phô introduced several popular styles of ao dai beginning in 1934. Such Westernized garments temporarily disappeared during World War II (1939–45).

In the 1950s, Saigon designers tightened the fit of the ao dai to create the version commonly seen today.<ref name="Ellis"/> Trần Kim of Thiết Lập Tailors and Dũng of Dũng Tailors created a dress with [[raglan sleeve]]s and a diagonal seam that runs from the collar to the underarm.<ref name="Ellis"/> [[Madame Nhu]], first lady of South Vietnam, popularized a collarless version beginning in 1958. The ao dai was most popular from 1960 to 1975.<ref name="Elmore">{{cite news |last=Elmore |first=Mick |title=Ao Dai Enjoys A Renaissance Among Women : In Vietnam, A Return to Femininity |work=[[International New York Times|International Herald Tribune]] |date=September 17, 1997 |url = https://www.nytimes.com/1997/09/17/news/17iht-saodai.t.html}}<!-- http://www.iht.com/articles/1997/09/17/saodai.t.php --></ref> A brightly colored ''áo dài hippy'' was introduced in 1968.<ref>Bich Vy-Gau Gi, ''[http://bichvygaugi.spaces.live.com/photos/cns!1CDCF9B5594AB224!1049/ Ao Dai — The Soul of Vietnam]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}''. Retrieved on 2 July 2008.</ref> The ''áo dài mini'', a version designed for practical use and convenience, had slits that extended above the waist and panels that reached only to the knee.<ref name="Leshkowich91">Leshkowich p. 91.</ref>

==== Communist period ====
The ao dai has always been more common in the South than in the North. The communists, who gained power in the North in 1954 and in the South in the 1975, had conflicted feelings about the ao dai. They praised it as a national costume and one was worn to the [[Paris Peace Accords|Paris Peace Conference]] (1968–73) by [[Vietcong]] negotiator [[Nguyễn Thị Bình]].<ref name="Overland" /> Yet Westernized versions of the dress and those associated with "decadent" Saigon of the 1960s and early 1970s were condemned.<ref name="Leshkowich92">Leshkowich p. 92.</ref> Economic crisis, famine, and war with Cambodia combined to make the 1980s a fashion low point.<ref name="Valverde">{{cite web | last = Valverde | first = Caroline Kieu | title = The History and Revival of the Vietnamese ''Ao Dai'' | newspaper = NHA magazine | year = 2006 | url = http://www.nhamagazine.com/back_issue/issue_0506/ac_p1.shtml | accessdate=August 2, 2008}}</ref> The ao dai was rarely worn except at weddings and other formal occasions, with the older, looser-fitting style preferred.<ref name="Leshkowich92"/> Overseas Vietnamese, meanwhile, kept tradition alive with "Miss Ao Dai" pageants (''Hoa Hậu Áo Dài''), the most notable one held annually in [[Long Beach, California]].<ref name="Ellis"/>

The ao dai experienced a revival beginning in late 1980s, when state enterprise and schools began adopting the dress as a uniform again.<ref name="Ellis"/> In 1989, 16,000 Vietnamese attended a Miss Ao Dai Beauty Contest held in Ho Chi Minh City.<ref>{{Cite web | last1 = Vu | first1 = Lan | title = Ao Dai Viet Nam | newspaper = Viettouch | year = 2002 | url = http://www.viettouch.com/aodai/aodai-changes.htm | accessdate=July 3, 2008}}</ref> When the Miss International Pageant in Tokyo gave its "Best National Costume" award to an ao dai-clad Trường Quỳnh Mai in 1995, ''Thời Trang Trẻ'' (New Fashion Magazine) claimed that Vietnam's "national soul" was "once again honored."<ref name="Leshkowich79">Leshkowich p. 79.</ref> An "ao dai craze" followed that lasted for several years and led to wider use of the dress as a school uniform.<ref name="Leshkowich97">Leshkowich p. 97.</ref>

== Present day ==
[[File:Ao-dai-xu-Hue-2.jpg|thumbnail|No longer deemed politically controversial, ao dai fashion design is supported by the Vietnamese government]]
No longer deemed politically controversial, ao dai fashion design is supported by the Vietnamese government.<ref name="Valverde"/> It is often called ''áo dài Việt Nam'' to link it to patriotic feelings. Designer Le Si Hoang is a celebrity in Vietnam and his shop in Saigon is the place to visit for those who admire the dress.<ref name="Valverde"/> In Hanoi, tourists get fitted with ao dai on Luong Van Can Street.<ref>{{cite news|title=Traditional ''ao dai'' grace foreign bodies |url=http://vietnamnews.vnagency.com.vn/2004-12/18/Stories/33.htm |work=VNS |date=December 20, 2004 |deadurl=yes |archiveurl=https://web.archive.org/web/20041224075607/http://vietnamnews.vnagency.com.vn/2004-12/18/Stories/33.htm |archivedate=December 24, 2004 |accessdate=August 24, 2016}}</ref> The elegant city of [[Huế]] in the central region is known for its ao dai, ''nón lá'' (leaf hats), and well-dressed women.

The ao dai is now standard for weddings, for celebrating Tết and for other formal occasions. It's required uniform for female teacher (mostly from high school to below) and female student in common high school in the South; no require about color or pattern for teacher when student use plain white with some small patterns like flowers for [[school uniform]]. Companies often require their female staff to wear uniforms that include the ao dai, so [[flight attendant]]s, [[receptionist]]s, bank female staff, restaurant staff, and hotel workers in Vietnam may be seen wearing it.

[[File:Ao dai le hoi.JPG|thumbnail|left|Girls wearing ao dai]]
The most popular style of ao dai fits tightly around the wearer's upper [[torso]], emphasizing her bust and curves. Although the dress covers the entire body, it is thought to be provocative, especially when it is made of thin fabric. "The ao dai covers everything, but hides nothing", according to one saying.<ref name="Overland">{{cite web |url = http://www.overlandclub.jp/en/info/vn_aodai.html |title = Vietnamese AoDai |work=Overlandclub |accessdate = July 2, 2008 |archiveurl = https://web.archive.org/web/20080319082307/http://www.overlandclub.jp/en/info/vn_aodai.html |archivedate = March 19, 2008}}</ref> The dress must be individually fitted and usually requires several weeks for a tailor to complete. An ao dai costs about $200 in the United States and about $40 in Vietnam.<ref name="Bhudsabourg">{{cite web |url=http://www.nhamagazine.com/012008/feature/aodai.shtml |title=Ao Dai Couture |work=Nha magazine. |accessdate= August 12, 2008}}</ref>

"Symbolically, the ao dai invokes nostalgia and timelessness associated with a gendered image of the homeland for which many Vietnamese people throughout the diaspora yearn," wrote Nhi T. Lieu, an assistant professor at the University of Texas at Austin.<ref name="Lieu"/> The difficulties of working while wearing an ao dai link the dress to frailty and innocence, she wrote.<ref name="Lieu"/> Vietnamese writers who favor the use of the ao dai as a school uniform cite the inconvenience of wearing it as an advantage, a way of teaching students feminine behavior such as modesty, caution, and a refined manner.<ref name="Leshkowich97">Leshkowich p. 97.</ref>

The ao dai is featured in an array of Vietnam-themed or related movies. In ''[[Good Morning, Vietnam]]'' (1987), [[Robin Williams]]'s character is wowed by ao dai-clad women when he first arrives in Saigon. The 1992 films ''[[Indochine (film)|Indochine]]'' and ''[[The Lover (film)|The Lover]]'' inspired several international fashion houses to design ao dai collections,<ref name="Wakefield">{{cite web |url = http://articles.getacoder.com/Ao_Dai_-_Vietnamese_Plus_Size_Fashion_Statement_808529x1200042917.htm |title = Ao Dai — Vietnamese Plus Size Fashion Statement |accessdate=July 14, 2008}}</ref> including [[Prada]]'s SS08 collection and a [[Georgio Armani]] collection. In the [[Cinema of Vietnam|Vietnamese film]] ''[[The White Silk Dress]]'' (2007), an ao dai is the sole legacy that the mother of a poverty-stricken family has to pass on to her daughters.<ref>{{cite web |url = http://vietq.wordpress.com/2006/10/16/vietnam-send-ao-lua-ha-dong-to-pusan-film-festl/ |title= Vietnam send Ao Lua Ha Dong to Pusan Film Festival |work=VietNamNet Bridge |year=2006 |accessdate= July 13, 2008}}</ref> The Hanoi City Complex, a 65-story building now under construction, will have an ao dai-inspired design.<ref>{{cite news |title="Nóc nhà" Hà Nội sẽ cao 65 tầng|author=Tuấn Cường|newspaper = [[Tuoi Tre]] |language=Vietnamese |accessdate=April 26, 2009 |url = http://www.tuoitre.com.vn/Tianyon/Index.aspx?ArticleID=192066&ChannelID=3}}</ref> Vietnamese designers created ao dai for the contestants in the [[Miss Universe]] beauty contest, which was held July 2008 in [[Nha Trang]], Vietnam.<ref>{{Cite web
 |title=Miss Universe contestants try on ao dai 
 |newspaper=Vietnam.net Bridge 
 |year=2008 
 |url=http://english.vietnamnet.vn/lifestyle/2008/06/789353/ 
 |accessdate=August 2, 2008 
 |archiveurl=https://web.archive.org/web/20080701025340/http://www.english.vietnamnet.vn/lifestyle/2008/06/789353/ 
 |archivedate=July 1, 2008
 |deadurl=no 
 |df=mdy 
}}
</ref>

The most prominent annual Ao Dai Festival outside of Vietnam is held each year in San Jose, California, a city that is home to a large Vietnamese American community.<ref>http://aodaifestival.com</ref> This event features an international array of designer ao dai under the direction of festival founder, Jenny Do.

== Gallery ==
<gallery widths="154px" heights="154px" caption="Ao dai">
File:Áo dài, Hồ Gươm 2.jpg|: Young woman in a white áo dài.
File:Aodai in Purple.JPG |Dancing high school students
File:Images24696 nusinh070904.jpg |Graduation
File:Aodai-nonla-crop.jpg|A schoolgirl in a white ao dai and a ''[[nón lá]]'' (leaf hat). This ensemble is associated with the central city of [[Huế]]
File:Ao dai.jpg|A light blueish ao dai
File:AodaiVN SGU.JPG| Vietnamese students, Saigon University
File:Bicycle of Ho Chi Minh City.jpg|Two highschool girls in áo dài in HCMC
File:Áo dài hồ Gươm.jpg|Young girls in ao dai by the Hoan Kiem Lake.
</gallery>

== See also ==
{{Portal|Vietnam|Culture|Fashion}}
* [[Áo giao lĩnh]]
* [[Áo tứ thân]]
* [[Culture of Vietnam]]
* [[History of Vietnam]]
* [[Qipao]]
* [[Vietnamese clothing]]
* [[Hanfu]]
* [[Women in Vietnam]]

== Notes ==
{{Notelist|30em}}

== References ==
; Citations 
{{Reflist|30em}}

== External links ==
{{Commons category|Áo dài}}
* [http://wrile.com/long-dress-ao-dai-vietnam History of the Vietnamese Long Dress].
* [http://www.lahava.vn/ao-dai-cuoi.html].
* [https://www.flickr.com/photos/prime85/ Creative Pictures of Ao Dai].
* [https://web.archive.org/web/20110622101508/http://www.ci.seattle.wa.us/helpinglink/StudentWeb/webproject/html/fashion.htm The Evolution of the Ao Dai Through Many Eras], Gia Long Alumni Association of Seattle, 2000.
* [https://www.youtube.com/watch?v=ej7WATu_klE Vietnam: Mini-Skirts & Ao-Dais]. A video that shows what the women of Saigon wore in 1968.
* [http://myvietnamhandbook.com/ao-dai/ Ao Dai: Clothing with a History], an article about Ao Dai at myvietnamhandbook.com - a blog about Vietnamese culture.

{{Folk costume}}

{{DEFAULTSORT:Ao Dai}}
[[Category:Dresses]]
[[Category:History of Asian clothing]]
[[Category:National costume]]
[[Category:Vietnamese clothing]]
[[Category:Vietnamese words and phrases]]