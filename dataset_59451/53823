Analysis of sound and acoustics plays a role in such engineering tasks as product design, production test, machine performance, and process control. For instance, product design can require modification of sound level or noise for compliance with standards from [[American National Standards Institute|ANSI]], [[International Electrotechnical Commission|IEC]], and [[International Organization for Standardization|ISO]]. The work might also involve design fine-tuning to meet market expectations. Here, examples include tweaking an automobile door latching mechanism to impress a consumer with a satisfying click or modifying an exhaust manifold to change the tone of an engine's rumble.  Aircraft designers are also using acoustic instrumentation to reduce the noise generated on takeoff and landing.

Acoustical measurements and instrumentation range from a handheld sound level meter to a 1000-microphone phased array.  Most of the acoustical measurement and instrumentation systems can be broken down into three components:
<br>1) Sensors
<br>2) Data Acquisition
<br>3) Analysis

== Sensors ==
The most common sensor used for acoustic measurement is the [[microphone]].  Measurement-grade microphones are different from typical recording-studio microphones because they can provide a detailed calibration for their response and sensitivity.  Other sensors include [[hydrophones]] for measuring sound in water or [[accelerometers]] for measuring vibrations causing sound.  The three main groups of microphones are pressure, free-field, and random-incidence, each with their own correction factors for different applications.<ref>[http://www.gras.dk/ G.R.A.S. Sound & Vibration] has a [http://www.gras.dk/media/MiscFiles/SalesMat/GRASProductCatalogue_WEB.pdf selection guide] detailing the difference between microphones.</ref> Well-known microphone suppliers include [[PCB Piezotronics]], [[Brüel & Kjær]] and [[GRAS]].

== Data acquisition ==
Data acquisition hardware for acoustic measurements typically utilizes 24-bit [[analog-to-digital converter]]s (ADCs), [[anti-aliasing filter]]s, and other signal conditioning.  This signal conditioning may include amplification, filtering, sensor excitation, and input configuration.  Another consideration is the frequency range of the instrumentation.  It should be large enough to cover the frequency range of signal interest, taking into account the range of the sensor.  To prevent aliasing, many devices come with antialiasing filters, which cut the maximum frequency range of the device to a little less than one-half the maximum sampling rate, as prescribed by the Nyquist sampling theorem. [[Dynamic range]] is a common way to compare performance from one instrument to another.  Dynamic range is a measure of how small you can measure a signal relative to the maximum input signal the device can measure. Expressed in decibels, the dynamic range is 20 log (Vmax/Vmin). For example, a device with an input range of ±10 V and a dynamic range of 110&nbsp;dB will be able to measure a signal as small as 10 µV. Thus, the input range and the specified dynamic range are important for determining the needs of your instrumentation system.  Some well known vendors include OROS,<ref>http://www.oros.com/3970-acoustics-analysis-from-octave-to-sound-power.htm</ref> [[HEAD acoustics]], [[Norsonic]], [[Brüel & Kjær]], [[Prosig]], [[National Instruments]], [[LMS International]] and [[GRAS]] has a [http://www.gras.dk/media/MiscFiles/SalesMat/GRASProductCatalogue_WEB.pdf selection guide] detailing the difference between microphones..

== Analysis ==
Audio and acoustic analysis includes: fractional-octave analysis, sound-level measurements, power spectra, frequency response measurements, and transient analysis.  Results are viewed on waterfall displays, colormap displays, and octave graphs.
{{Gallery
|width=120
|height=115
|lines=5
|File:Octave_Analysis_Graph.JPG|Third Octave Analysis
|File:FFT_Graph.JPG|FFT
|File:FRF_Graph.JPG|Frequency Response Function
|File:Spectral_Map_Graph.JPG|Color Map
|File:Active_crossover_traces_from_Smaart.png|Dual-FFT frequency response of an active crossover as measured using [[Smaart]] software
}}

== References ==
{{Reflist}}

{{DEFAULTSORT:Acoustical Measurements And Instrumentation}}
[[Category:Acoustics]]