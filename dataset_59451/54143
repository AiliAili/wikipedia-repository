{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type     = suburb
| name     = Clarence Park
| city     = Adelaide
| state    = sa
| image    = Churchill Avenue, Clarence Park.JPG
| caption  = Churchill Avenue, looking south towards Cross Road
| est      = 1892<ref name="established">{{Cite web|url=http://www.slsa.sa.gov.au/manning/pn/c/c7.htm#clarenceP |title=Manning Index of South Australian History}}</ref>
| lga      = City of Unley
| postcode = 5034
| pop      = 2,492| pop_year = {{CensusAU|2011}}
| pop_footnotes = <ref name=Census2011>{{Census 2011 AUS | id=SSC40109 | name=Clarence Park (State Suburb) | quick=on | accessdate=23 June 2014}}</ref>
| pop2     = 2,499| pop2_year = {{CensusAU|2006}}
|pop2_footnotes = <ref name=Census2006>{{Census 2006 AUS | id=SSC41256 | name=Clarence Park (State Suburb) | quick=on | accessdate=26 May 2008}}</ref>
| area     =
| stategov = [[Electoral district of Ashford|Ashford]]
| fedgov   = [[Division of Adelaide|Adelaide]]
| near-nw  = [[Black Forest, South Australia|Black Forest]]
| near-n   = [[Black Forest, South Australia|Black Forest]] and [[Millswood, South Australia|Millswood]]
| near-ne  = [[Unley Park, South Australia|Unley Park]]
| near-e   = [[Kings Park, South Australia|Kings Park]]
| near-se  = [[Westbourne Park, South Australia|Westbourne Park]]
| near-s   = [[Cumberland Park, South Australia|Cumberland Park]]
| near-sw  = [[South Plympton, South Australia|South Plympton]]
| near-w   = [[Black Forest, South Australia|Black Forest]]
| dist1    = 4
| location1= Adelaide
}}

'''Clarence Park''' (formerly '''Goodwood South''')<ref name=established/> is an inner southern suburb of [[Adelaide]], [[South Australia]] in the [[City of Unley]]. Its borders are Mills Street (north), [[Goodwood Road, Adelaide|Goodwood Road]] (east), [[Cross Road, Adelaide|Cross Road]] (south) and the [[Seaford railway line]] (north-west).<ref>{{cite book|title=2003 Adelaide Street Directory, 41st Edition |publisher=UBD (A Division of Universal Press Pty Ltd) |year=2003 |isbn=0-7319-1441-4}}</ref>

==History==
''Goodwood Park'' Post Office opened on 27 December 1877, was renamed ''Goodwood South'' in 1884, ''Clarence Park'' in 1910 and was replaced by a new [[Cumberland Park, South Australia|Cumberland Park]] office in 1993.<ref name = "Post Office">{{Cite web  | last = Premier Postal History  | title = Post Office List  | publisher = Premier Postal Auctions | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=SA&country= | accessdate = 26 May 2011}}</ref>

==Facilities==
The Clarence Park Community Centre<ref>[http://www.unley.sa.gov.au/site/page.cfm?u=897 Clarence Park Community Centre], City of Unley.</ref> is located just over the train line on East Avenue, in the suburb of [[Black Forest, South Australia|Black Forest]]. The centre caters for many clubs, provides courses, and is adjacent to the Clarence Park Biodiversity Garden.<ref>[http://www.unley.sa.gov.au/site/page.cfm?u=773&c=5307 Clarence Park Biodiversity Garden], City of Unley</ref> Clarence Park Community Kindergarten<ref>[https://www.facebook.com/ClarenceParkCK/]</ref> is located on Parker Terrace in Clarence Park, offering kindergarten and playgroup to the local community.

==References==
{{Reflist}}

==External links==
* [http://www.unley.sa.gov.au/site/page.cfm City of Unley website]

{{City of Unley suburbs}}
{{Coord|34.963|S|138.586|E|format=dms|type:city_region:AU-SA|display=title}}

[[Category:Suburbs of Adelaide]]
[[Category:Populated places established in 1892]]


{{adelaide-geo-stub}}