{{Infobox park
| name = Anna Scripps Whitcomb Conservatory
| alt_name = Belle Isle Conservatory
| photo = Belle Isle Whitcomb Conservatory.jpg
| coords = {{coord|42|20|32|N|82|58|46|W|region:US-MI|format=dms|display=inline,title}}
| type = [[Botanical garden]] and [[greenhouse]]
| location = [[Belle Isle Park (Michigan)|Belle Isle Park]]<br>[[Detroit]], [[Michigan]]
| area = {{convert|13|acre|ha}}
| opened = {{Start date|1904|8|18}}
| founder =
| designer = [[George D. Mason]] & [[Albert Kahn (architect)|Albert Kahn]] 
| etymology = In April 1953, Anna Scripps Whitcomb gave her 600 orchids collection to the conservatory
| operator = State of Michigan
| budget =
| visitation_num =
| visitation_year =
| visitation_ref =
| status = Open all year
| awards =
| open = 10 a.m. to 5 p.m. ([[Eastern Time Zone|EST]])
| publictransit =
| plants =
| species =
| website = [http://www.belleisleconservancy.org/ belleisleconservancy.org]
}}

The '''Anna Scripps Whitcomb Conservatory''' (commonly and locally known as the '''Belle Isle Conservatory''') is a [[greenhouse]] and a [[botanical garden]] located on [[Belle Isle Park (Michigan)|Belle Isle]], a 982-acre island park nestled in the [[Detroit River]] between [[Detroit]] and the [[Canada–United States border]]. The park itself consists of 13 acres of preserved land for the conservatory and its botanical garden.<ref name="Austin"/>

Opened in 1904, the Anna Scripps Whitcomb Conservatory is the oldest continually-running conservatory in the United States.<ref name="Austin"/><ref name="Conservatory"/> It is named for Anna Scripps Whitcomb, who left her collection of 600 [[Orchidaceae|orchids]] to Detroit in 1955.<ref>{{cite book|title=Belle Isle Visitors Guide|location=White House of Belle Isle|pages=11}}</ref>

==History and architecture==
The Belle Isle Conservatory, along with the neighboring [[Belle Isle Aquarium]], was (according to the September 1, 1901 edition of the Detroit Free Press) designed by the noted firm of George D. Mason and Albert Kahn, and built between 1902 and 1904.<ref name="Conservatory">{{cite web|title=Anna Scripps Whitcomb Conservatory|url=http://www.belleisleconservancy.org/#!anna-scripps-whitcomb-conservatory/iiafv|publisher=Belle Isle Conservancy|accessdate=17 July 2016}}</ref><ref>{{cite book|title=Belle Isle Visitors Guide|location=White House of Belle Isle|pages=6}}</ref> Kahn modeled the building after architectural exhibitions and garden pavilions of the mid- to late-nineteenth century, most notably [[The Crystal Palace]] and the Palm House at [[Kew Gardens]].<ref name="aia">{{Cite book|last1=Hill|first1=Eric J.|last2=Gallagher|first2=John|year=2002|url=https://books.google.com/books?id=sZGskamYzjUC&redir_esc=y|title=AIA Detroit: The American Institute of Architects Guide to Detroit Architecture|location=Detroit|publisher=[[Wayne State University Press]]|isbn=0-8143-3120-3}} P. 264.</ref><ref name="sharoff">{{Cite book|last=Sharoff|first=Robert|authorlink= Robert Sharoff|year=2005|url=https://books.google.com/books?id=_quVubxm6EMC&dq|title=American City: Detroit Architecture, 1845-2005|location=Detroit|publisher=Wayne State University Press|isbn=0-8143-3270-6}} P. 21.</ref>

Located in the center of the 982-acre [[Belle Isle Park (Michigan)|Belle Isle Park]], the conservatory covers 13 acres, with a lily pond on its north side and perennial gardens on the west;<ref name="Austin"/> the gardens home the Levi L. Barbour Memorial Fountain, designed by sculptor [[Marshall Fredericks]].<ref name="Conservatory"/> The Conservatory building is divided into five distinct sections: the Palm house (inside the domed center), the Tropical house (South Wing), the Cactus house and [[Fernery]] (North Wing), and the Show house (East Wing).<ref name="Conservatory"/> The main feature of the building is the {{convert|85|ft|m}}<ref name="Conservatory"/> high central dome, which houses [[Arecaceae|palm trees]] and other tropical plants.<ref name="Austin"/>

[[File:Horticultural building, Belle Isle, Michigan (64793).jpg|thumb|left|250px|Old postcard of the conservatory, showing the original wooden structure]]
The conservatory, originally known as the Horticultural Building,<ref name="Conservatory"/> opened on August 18, 1904, and is currently "the oldest, continually operating conservatory in the United States."<ref name="Austin">{{cite web|last=Austin|first=Dan|title=Anna Scripps Whitcomb Conservatory|url=http://historicdetroit.org/building/anna-scripps-whitcomb-conservatory/|accessdate=22 January 2013}}</ref> Soon after its opening, it became a popular attraction, reaching an annual average of 1.5 million visitors in the mid-1920s.<ref name="Austin"/> In 1953, the original wooden frame of the central dome was replaced by one made of iron and aluminum; the operation cost circa $500,000.<ref name="Austin"/>

In April 1955 Anna Scripps Whitcomb, daughter of ''[[The Detroit News]]'' founder [[James E. Scripps]], gave her 600 orchids collection to the conservatory, which was therefore renamed in her honor on April 6, 1955.<ref name="Conservatory"/> Many of these exotic orchids had been saved from Britain throughout [[World War II]].<ref name="Austin"/> Because of that contribution, the conservatory became "the largest municipally owned orchid collection in the country."<ref name="Austin"/>

==Botany==
The Palm house, maintained at 70&nbsp;°F, is housed under the dome,<ref>{{cite web | url=http://westbloomfield.localstew.com/businesses/belle-isle | title=Belle Isle | publisher=Local Stew: West Bloomfield | accessdate=16 July 2014}}</ref> and contains a variety of [[tropical]] [[tree]]s and [[Arecaceae|palms]], including the Chinese fan palm (''[[Livistona chinensis]]''), the fishtail palm (''[[Caryota]] mitis''), the umbrella tree (''[[Schefflera actinophylla]]'') and the Canary Island date palm (''[[Phoenix canariensis]]''). When a tree reaches the top of the dome it must be cut down, as they cannot be pruned to height.<ref name="belleisleconservancy">{{cite web|title=Fun Facts|url=http://www.belleisleconservancy.org/learn/fun-facts/|publisher=Belle Isle Conservancy|accessdate=20 January 2013}}</ref> The Tropical house, 70&nbsp;°F, contains fruiting plants and trees such as the [[common fig]], [[calamondin orange]] and bloodleaf banana, and tropical flowering plants such as the pink powderpuff (''[[Calliandra brevipes]]'') and peace lily (''[[Spathiphyllum]]''). The Cactus house, 62&nbsp;°F, is lined in [[tufa]] rock and holds a variety of [[cactus|cacti]] and [[Succulent plant|succulents]], including jellybeans (''[[Sedum]] pachyphyllum''), the silver dollar cactus (''[[Opuntia robusta]]''), "old man" cactus (''[[Cephalocereus senilis]]'') and jade plant (''[[Crassula ovata]]''). The [[Fernery]] is sunken to provide cooler conditions and more humidity.<ref name="belleisleconservancy"/> [[Fern]]s such as the [[Cyathea sect. Alsophila|alsophila]] can be found here, as well as a small waterfall located in the center. The Show house changes its display many times throughout the year and is also used for the conservatory's many flower shows and special events. The perennial gardens are located outside to the north of the vestibule.

===Lily pond===
The lily pond is located between the conservatory building and the [[Belle Isle Aquarium]]. It was not part of the original design, but constructed in 1936.<ref name="Conservatory"/> The rocky walls were created with 200 tons of moss-covered limestone boulders that were brought from the construction of the Livingstone Channel in the [[Detroit River]] near [[Amherstburg]], [[Ontario]].<ref name="Conservatory"/> The pond is home to Japanese [[koi]] that are maintained by volunteers and are held in the aquarium basement during winter.<ref name="belleisleconservancy"/>

===Greenhouses===
Located to the west of the conservatory building are 20 [[greenhouse]] structures.<ref>{{Cite book|last1=Rodriguez|first1=Michael|last2=Featherstone|first2=Thomas|year=2003|url=https://books.google.com/books?id=JezkahudWUUC&dq|title=Detroit's Belle Isle: Island Park Gem|publisher=[[Arcadia Publishing]]|isbn=0738523151}} P. 51.</ref> 15 of the structures are used to care for the orchid, bromeliad, cactus, amaryllis and tropical plant collections. Five of the greenhouses are used by the Golightly Career and Technical Center's [[Agriscience]] Program for high school students in the metro Detroit area. These greenhouses help train for post-graduation jobs as green's keepers, [[florists]], [[arborists]], nursery owners and related fields.<ref>{{cite web|title=Activities and Attractions|url=http://www.belleisleconservancy.org/explore-the-park/activities-attractions/|publisher=Belle Isle Conservancy|accessdate=20 January 2013}}</ref> The first greenhouse was built on Belle Isle in 1903.

==Non-profit involvement & volunteerism==
The Anna Scripps Whitcomb Conservatory is opened to the public free of charge, and operates mostly on the budget designated by local and state Governments. However, Belle Isle has long history of support through private organizations and donors. Volunteerism plays an important role for all of Belle Isle. Specifically, four major volunteer organizations have been vital to the livelihood of the island park. Friends of Belle Isle, a non-profit [[grassroots]] [[environmental movement|environmental]] organization, was founded in 1972 and is dedicated to the upkeep and preservation of Belle Isle through cleaning and the ridding of invasive species on the island. In 1988, the Belle Isle Botanical Society began raising money for projects to improve the Anna Scripps Whitcomb Conservatory. The Belle Isle Botanical society offered many services to the conservatory including volunteer tour guides and gardeners, and was also involved in raising funds to maintain and support the conservatory operation through repairs, equipment, purchase of plant material, etc. In 2004, the Belle Isle Women's Committee was created and its first project was to upgrade Sunset Point. The non-profit Friends of the Belle Isle Aquarium was formed in 2005 with the goal of restoring Belle Isle's historic [[Belle Isle Aquarium|aquarium]].

In 2009, the four organizations put a plan in action to form a single organization that could pool all their efforts and ideas into a larger and more effective non-profit dedicated to improvement projects. Together, receiving assistance from the Cultural Alliance for Southeastern Michigan and the Michigan Nonprofit Association, they have joined forces to form a single all-volunteer organization named the [[Belle Isle Conservancy]].<ref name="Belle Isle Conservancy">{{cite web|title=Who We Are|url=http://www.belleisleconservancy.org/about-us/who-we-are/|accessdate=20 January 2013}}</ref> [[The Kresge Foundation]], based in suburban Detroit, has played a key role in the merger. Kresge gave the four organizations $100,000, in 2009, and in 2010 another $100,000.<ref>{{cite web|last=Cohen|first=Rick|title=A Four-Group Merger Creates New Conservancy in Metro Detroit|url=http://www.nonprofitquarterly.org/index.php/policysocial-context/147-news/17026-a-four-group-merger-createsnew-conservancy-in-metro-detroit|publisher=Non Profit Quarterly|accessdate=20 January 2013}}</ref>
Currently, there are three employees and a supervisor from the City of Detroit General Services Department that make up the [[Floriculture]] Unit at the Belle Isle Conservatory. The staff members are responsible for caring for all the plants in the Conservatory and greenhouses. The Conservatory relies on volunteers for all its additional needs and workers.<ref name="belleisleconservancy"/>

==Gallery==
<gallery>
File:Whitcomb Conservatory.jpg|Anna Scripps Whitcomb Conservatory
File:The_Henry_A._Johnson_Memorial_Gardens2013.JPG|The Henry A. Johnson Memorial Gardens
File:Conservatory2013.JPG|Pathway through the Tropical house
File:Show House Entrance.JPG|Entrance to the Show house
File:Show Room2013.JPG|Show house (East Wing)
File:Palm Tree Belle Isle2013.JPG|Palm Tree
File:Ferns Belle isle2013.JPG|Ferns
</gallery>

==References==
{{reflist}}

{{commons category|Anna Scripps Whitcomb Conservatory}}

==External links==
* {{official website|http://www.belleisleconservancy.org/}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:Buildings and structures in Detroit]]
[[Category:Tourist attractions in Detroit]]
[[Category:Botanical gardens in Michigan]]
[[Category:Greenhouses in Michigan]]