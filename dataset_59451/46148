{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox person
| name =Mavis Amankwah
| image = Mavis Amankwah Photoshoot.jpg
| image_size = 200px
| caption = 
| birth_date  = {{Birth date and age|df=yes|1974|8|28}}
| birth_place = [[East London]], England
| death_date  =
| death_place =
| occupation = Business owner
| parents    =
| spouse     = {{marriage|Richmond Agyekum|1996}}
| children  = 2
| nationality  = British-Ghanaian
| religion   = 
| salary     =
| networth   =
| website    ={{url|mavisamankwah.com/}}
}}

'''Mavis Amankwah''' (born 28 August 1974) is a British-Ghanaian entrepreneur, [[businesswoman]] and diversity ambassador, specialising in marketing, [[public relations]], diversity communications and business start up & sustainability.

In 2002, Amankwah set up the [[communications]] arm of Rich Visions after noticing a gap between large corporations/companies and traditionally hard-to-reach [[ethnic communities]] in the UK. As well as Rich Visions, Amankwah has several other businesses to her name.

Amankwah has been listed in [[PR Week]]'s PowerBook<ref>{{cite web|url=http://www.prweek.com/powerbook/person/495f10e3-8e12-4a3d-ae95-99687288006b/mavis-amankwah |title=Mavis Amankwah's 2015 entry in PR Week's Powerbook |publisher=PR Week |accessdate=2013}}</ref> for six consecutive years between 2009–2015.

== Early life and education ==

Amankwah was born in [[Newham]], East London to a [[Ghanaian]] family. Mavis has one son aged 21 and one daughter aged 16.

Having completed secondary school in 1990 at [[St Angela's Usruline Convent School]] in East London, Amankwah went on to study IT at the [[College of North East London]] where she qualified as an [[IT Technician]].

== Career ==

=== St Leonard's Primary Care Trust ===

Amankwah worked as an IT Technician at St Leonard's Primary Care Trust in London for six years. During that time, she spent her spare time organising community events targeting hard-to-reach communities. After working on a few events, Amankwah became the Event Organiser for Miss Ghana UK.<ref>{{cite web|url=http://themissghanauk.com/ |title=Miss Ghana UK |publisher=Miss Ghana UK |accessdate=2001}}</ref>

=== Sleek International ===

Amankwah joined makeup company, Sleek International, as their ICT manager. As ICT Manager, Amankwah was responsible for developing the IT infrastructure and implementing strategies in line with the company's corporate standards. As well as managing her own team, Amankwah assisted the marketing and PR team with their publicity campaigns that specifically targeted niche audiences in Europe, Africa, Asia and the USA.

=== Sankofa African Caribbean Organisation ===

Whilst still working at Sleek, Amankwah decided to set up a social enterprise, [[Sankofa]] [[African-Caribbean]] Organisation in 2003.<ref>{{cite web|url=http://www.nvsc.org.uk/groups/database/fulldetails.php?org=3460&searchfor=&newstyle= |title=Sankofa African Caribbean Organisation |publisher=Newham Voluntary Sector Consortium |accessdate=2003}}</ref> The aim of the organisation was to aid young people to fulfill their dreams by working on their personal development, education, career and enterprise skills. Sankofa ceased operations in 2008.

=== Rich Visions ===

After leaving Sleek International, Amankwah went on to start up marketing and PR communications agency. Rich Visions is an ethnic communications agency which engages and sustains relationships with hard to reach communities. They have created campaigns for [[ASDA]], [[Comic Relief]],<ref>{{cite web|url=http://www.prweek.com/uk/news/1055880/Comic-Relief-appoints-Rich-Visions-raise-awareness-projects-Africa/ |title=Comic Relief appoints Rich Visions to raise awareness of projects in Africa |publisher=PR Week |accessdate=21 February 2011}}</ref> [[HMRC]], [[Metropolitan Police]], [[HSBC]], [[MoneyGram]] and for the [[Digital UK]] TV switchover.<ref>{{cite web|url=http://www.prweek.com/uk/news/1068600/Digital-UK-appoints-Rich-Visions-prepare-ethnic-consumers-digital-switchover/ |title=Digital UK appoints Rich Visions to prepare ethnic consumers for digital switchover |publisher=PR Week |accessdate=5 May 2011}}</ref>

Rich Visions previously assisted the social enterprise Positive Inclusions,<ref>{{cite web|url=http://positiveinclusions.org.uk/ |title=Positive Inclusions |publisher=Positive Inclusions |accessdate=2013}}</ref> the delivery partners on the government-backed initiative, the [[Start Up Loans Scheme]].<ref>{{cite web|url=http://www.startuploans.co.uk/ |title=Start Up Loans |publisher=Start Up Loans |accessdate=2013}}</ref> The scheme offers loans, business support and [[mentoring]] to aspiring entrepreneurs aged 18–30 who want to start or grow their enterprises.

=== Redbridge Homes and Surrey County Council ===

In July 2009, after studying the value of Equality and Diversity, Amankwah worked in consulting as an Equality and Diversity (E&D) Manager for Redbridge Homes and [[Surrey County Council]].

=== Rich Visions Small Business ===

In 2009, Amankwah launched another arm of the agency. Rich Visions Small Business (RVSB) provides professional Business Support (for start-ups and existing businesses), Funding Assistance, Marketing and PR services to Small and Medium Enterprises (SMEs), Entrepreneurs, Sole Traders and Organisations that want to start, grow or expand their businesses and enterprises.

=== Rich Visions Digital ===

In January 2012, the digital arm of the company, Rich Visions Digital, was formed.

=== Author ===

Amankwah wrote a [[self-help book]] for businesses and entrepreneurs called ''44 Ways to Grow Your Business or Brand: a step-by-step guide to increase profit''.<ref>{{cite web|url=http://www.amazon.co.uk/Ways-Grow-Your-Business-Brand/dp/0956666523/ |title=44 Ways to Grow Your Business or Brand: a step-by-step guide to increase profits – book listing on Amazon |publisher=Amazon |accessdate=September 2011}}</ref>

== Business Ambassador ==

In January 2013, Amankwah was appointed as the Business Ambassador<ref>{{cite web|url=http://positiveinclusions.org.uk/blog/mavis-amankwah-becomes-positive-inclusions-business-ambassador/ |title=Mavis Amankwah becomes Positive Inclusions Business Ambassador |publisher=Positive Inclusions |accessdate=January 2013}}</ref> for a [[social enterprise]] called Positive Inclusions. The organisation works with young people from hard-to-reach communities to improve their personal, social, career and business skills. Positive Inclusions were working on the [[Start Up Loans Scheme]] as one of the official "Delivery Partners<ref>{{cite web | url=http://www.startuploans.co.uk/partners/delivery-partners/ | title=Start Up Loans Delivery Partners Network | publisher=Start Up Loans | date=October 2013 }}</ref>" on the project delivering the scheme. Amankwah, as Business Ambassador for Positive Inclusions, is working on promoting this scheme to budding entrepreneurs and raising awareness as well as offering personal mentoring and business plan/cashflow forecast assistance.

== Young UpStarts ==
Mavis was featured on a programme called 'Young UpStarts' which was aired on satellite TV channel London Live on 15 June 2014. Mavis was seen mentoring and guiding a young lady on her journey into the world of business ownership.<ref>http://www.londonlive.co.uk/programmes/8-debate/cf9d3e06</ref>

== Public speaking ==

Amankwah is a public speaker<ref>{{cite web|url=http://trumpetmediagroup.com/the-trumpet/news/in-celebration-of-women/ |title=Mavis Amankwah to speak at Women Mentoring Women event |publisher=The Trumpet |accessdate=16 March 2013}}</ref> who has spoken at various conferences and seminars over the years. She was a [[keynote speaker]] at the prestigious Business 2012 event, alongside [[Stedman Graham]] ([[Oprah Winfrey]]'s partner), [[Lord Alan Sugar]] and [[Sir Richard Branson]].

Amankwah holds workshops and one-to-one mentoring programmes. She launched business club, Diva Visions and Women Like Me in or female business owners, career professionals and aspiring entrepreneurs from diverse backgrounds. It currently consists of 100 members.

== Media coverage ==

Amankwah has been featured in over 200 press titles including<ref>{{cite web|url=http://www.ventures-africa.com/2012/06/%E2%80%98wonder-woman-extraordinaire%E2%80%99-mavis-amankwah-rich-visions/ |title='Wonder Woman Extraordinaire': Mavis Amankwah, Rich Visions |publisher=Ventures Africa |accessdate=3 June 2012}}</ref><ref>{{cite web|url=http://www.wearethecity.com/inspirational-women-mavis-amankwah-managing-director-rich-visions/ |title=Inspirational Women – Mavis Amankwah – Managing Director – Rich Visions |publisher=We Are The City |accessdate=20 May 2010}}</ref><ref>{{cite web|url=http://www.divascribe.com/DivaProfilesOn/Inspirational-PR-Guru-Mavis-Amankwah-Reveals-Her-Childhood-Traumas-a-230.html |title=Inspirational PR Guru, Mavis Amankwah Reveals Her Childhood Traumas |publisher=Diva Scribe |accessdate=2012}}</ref><ref>{{cite web|url=http://mefirighana.com/mavis-amankwah-ghanas-pr-guru/ |title=Mavis Amankwah: Ghana’s PR Guru… |publisher=Me Firi Ghana |accessdate=24 February 2012}}</ref><ref>{{cite web|url=http://www.reconnectafrica.com/5-minute-interview/5-minute-interview-mavis-amankwah-md-rich-visions-2.html |title=Profile: Mavis Amankwah, MD, Rich Visions |publisher=ReConnect Africa |accessdate=2010}}</ref> 
[[The Daily Telegraph|The Telegraph]], [[Virgin Group|Virgin]], [[The Drum (http://www.thedrum.com/)|The Drum]], [[The Guardian]],<ref>{{cite news|url=https://www.theguardian.com/society/2010/apr/14/mavis-amankwah-rich-visions/ |title='There was a gap in the market for a PR agency to build communications between public sector organisations and hard-to-reach communities' |publisher=The Guardian |accessdate=10 April 2010 |location=London |first=Jane |last=Dudman}}</ref> [[The Voice (newspaper)|The Voice]]<ref>{{cite web|url=http://www.voice-online.co.uk/article/ten-years-top-pr-chain |title=Ten Years at the Top of the PR Chain |publisher=The Voice |accessdate=21 May 2012}}</ref> to PR Week.<ref>{{cite web|url=http://www.prweek.com/uk/news/1008819/Industry-fails-value-diversity-background-research-finds/ |title=Industry fails to value diversity of background, research finds |publisher=PR Week |accessdate=10 June 2010}}</ref><ref>{{cite web|url=http://www.prweek.com/uk/news/931172/Profile-Mavis-Amankwah-managing-director-Rich-Visions/ |title=Profile: Mavis Amankwah, managing director, Rich Visions |publisher=PR Week |accessdate=1 September 2009}}</ref><ref>{{cite web|url=http://www.prweek.com/uk/news/945530/29-29-Framing-stars-future/ |title=29 under 29: Framing the stars of the future |publisher=PR Week |accessdate=14 October 2009}}</ref> She has been included in PR Week's Powerbook for five consecutive years (2009–2015).

== Accolades ==

Amankwah has won 12 business and community awards and been featured in over 200 media publications.

In 2012, she won two awards out of four nominated categories at the first annual Women4Africa Awards (Business Woman of the Year and Entrepreneur of the Year).<ref>{{cite web|url=http://www.women4africa.com/women4africa-2012-winners/ |title=Women4Africa's 2012 Winners |publisher=Women4Africa |accessdate=2012}}</ref>

Winner of the ‘Innovation Award’ in 2007 GAB Awards – Media category
Winner of the 2008 Black Business Awards – Innovation category
Winner of the 2010 ‘Outstanding Achievement in Business’ Women In Enterprise award
Winner of the 2011 ‘Outstanding Career Achievement ’ Powersis Award
Women4Africa Awards in 2012 – ‘Business Woman of the Year’
Women4Africa Awards in 2012 – ‘Entrepreneur of the Year’
Winner of the Junior Chamber of Commerce ‘Outstanding Young Persons of The World’ 2013
Finalist of the Great British Entrepreneur Award 2013
African Business Excellence Award 2014
Ebony Business Recognition (EBR) Awards 2014 – Finalist ‘Women’s Business Coach of the Year’ and ‘Motivational & Inspirational Business Women of the Year’
Winner of the 2015 ‘Speaker of the Year’ iWOP
Winner of 2015 100 Outstanding Africans Making a Difference
Finalist ‘Entrepreneur Of The Year’ National Diversity Award 2015
Winner of 2015 ‘Inspirational Award’ Back2Black Award

In 2012, Amankwah judged for the [[Ghana UK-Based Achievement Awards]] (GUBA).<ref>{{cite web|url=http://www.gubaawards.co.uk/the-awards-2/judges/2012-judges/ |title=2012 JUDGES |publisher=GUBA Awards.co.uk |accessdate=9 November 2012}}</ref>

http://www.telegraph.co.uk/sponsored/business/sme-home/news/12062424/would-your-sme-survive-without-top-staff-member.html
http://www.prweek.com/article/931172/profile-mavis-amankwah-managing-director-rich-visions
https://www.virgin.com/entrepreneur/why-entrepreneurs-should-embrace-failure-in-2016
http://www.theguardian.com/society/2010/apr/14/mavis-amankwah-rich-visions

==References==
{{Reflist}}

== External links ==
*[http://www.mavisamankwah.com/ Mavis Amankwah official Website]
*[http://www.richvisions.co.uk/ Rich Visions official Website]
*[http://www.richvisionssb.co.uk/ Rich Visions Small Business official Website]
*[http://www.divavisions.co.uk/ Diva Visions official Website]
*[http://www.richvisionsdigital.com/ Rich Visions Digital official Website]
*[https://twitter.com/mavisamankwah/ Mavis Amankwah Twitter Page]

{{DEFAULTSORT:Amankwah, Mavis}}
[[Category:English businesspeople]]
[[Category:British people of Ghanaian descent]]
[[Category:Living people]]
[[Category:1974 births]]
[[Category:Corporate directors]]