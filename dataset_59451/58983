{{Infobox journal
| title = Molecular Physics
| cover = [[File: Molecular Physics.jpg]]
| discipline = [[Physical chemistry]]
| abbreviation = Mol. Phys.
| language = 
| publisher = [[Taylor & Francis]]
| country =
| frequency= Biweekly
| history = 1958-present
| impact = 1.837
| impact-year = 2015
| website = http://www.tandfonline.com/action/journalInformation?journalCode=tmph20
| link1 = http://www.tandfonline.com/toc/tmph20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/tmph20
| link2-name = Online archive
| ISSN = 0026-8976
| eISSN = 1362-3028
| OCLC = 49725826
| LCCN = 59016795
| CODEN = MOPHAM
}}
'''''Molecular Physics''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering research on the interface between [[chemistry]] and [[physics]], in particular [[chemical physics]] and [[physical chemistry]]. It covers both theoretical and experimental molecular science, including [[electron configuration|electronic structure]], [[molecular dynamics]], [[spectroscopy]], [[chemical kinetics|reaction kinetics]], [[statistical mechanics]], [[condensed matter physics|condensed matter]] and [[surface science]]. The journal was established in 1958 and is published by [[Taylor & Francis]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 1.670.<ref name=WoS>{{cite book |year=2013 |chapter=Molecular Physics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

The current [[editor-in-chief]] is [[Timothy P. Softley]] ([[University of Oxford]]). A reprint of the first editorial and a full list of editors since its establishment can be found in the issue celebrating 50 years of the journal.<ref>{{cite journal |journal=Molecular Physics |volume=106 |issue=16-18 |pages=1959–2253 |publisher=Taylor & Francis |year=2008|bibcode = 2008MolPh.106.1959. |doi = 10.1080/00268970802509787 }}</ref>

== See also ==
* [[List of scientific journals in physics]]
* [[List of scientific journals in chemistry]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.tandfonline.com/action/journalInformation?journalCode=tmph20}}

[[Category:Publications established in 1958]]
[[Category:Physical chemistry journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Biweekly journals]]
[[Category:English-language journals]]