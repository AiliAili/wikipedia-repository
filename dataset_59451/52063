{{Infobox journal
| title = Journal of Social Archaeology
| cover = [[File:Journal of Social Archaeology front cover image.jpg|200px]]
| editor = Lynn Meskell
| discipline = [[Archaeology]]
| former_names = 
| abbreviation = J. Soc. Archaeol.
| publisher = [[SAGE Publications]]
| country =
| frequency = Triannually
| history = 2001-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.sagepub.com/journals/Journal201500
| link1 = http://jsa.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jsa.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 48009098
| LCCN = 2001223309
| CODEN = 
| ISSN = 1469-6053 
| eISSN = 1741-2951 
}}
The '''''Journal of Social Archaeology''''' is a triannual [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[archaeology]], in particular with regard to social interpretations of the past. Its [[editor-in-chief]] is Lynn Meskell ([[Stanford University]]), who established the journal in 2001. It is currently published by [[SAGE Publications]].

== Scope ==
The ''Journal of Social Archaeology'' covers research on social approaches in archaeology, including [[feminism]], [[queer theory]], [[postcolonialism]], [[social geography]], [[literary theory]], politics, [[anthropology]], [[cognitive studies]], and [[behavioural science]], from prehistoric to present-day contexts. The journal also covers contemporary politics and heritage issues, and aims to break down the "arbitrary and hegemonic boundaries between North American, Classical, Near Eastern, Mesoamerican, European and Australian archaeologies – and beyond".<ref name=editorial>{{cite journal |title=Editorial Statement |journal=Journal of Social Archaeology |year=2001 |volume=1 |issue=1 |pages=5–12 |doi=10.1177/146960530100100101 }}</ref>

== Contents ==
The ''Journal of Social Archaeology'' is published in February, June, and October. Each issue contains 5-7 articles, occasionally featuring interviews with social theorists considered "prominent" by the editors.<ref name=editorial />

== Abstracting and indexing ==
The ''Journal of Social Archaeology'' is abstracted and indexed in:
* [[Academic Search Premier]]
* [[Arts & Humanities Citation Index]]
* [[Current Contents]]/Arts & Humanities
* [[Educational Research Abstracts Online]]
* [[Scopus]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://jsa.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Archaeology journals]]
[[Category:Publications established in 2001]]
[[Category:Triannual journals]]