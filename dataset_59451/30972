{{Use dmy dates|date=December 2016}}
{{Infobox album | <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Fantasy Black Channel
| Type        = [[Album]]
| Artist      = [[Late of the Pier]]
| Cover       = FBCLOTP2.jpg
| Alt         = Album cover collage with an artistic blue mountain range in the background and a brown/grey rocky landscape containing a guitar, a drum, two keyboards, and lengths of electrical wire in the foreground.
| Released    = 30 July 2008
| Recorded    = 2006−2008 at various locations
| Genre       = [[Dance-punk]], [[electroclash]], [[glam punk]]
| Length      = 42:47
| Label       = [[Parlophone]]
| Producer    = [[Erol Alkan]], Sam Eastgate
| Last album  = 
| This album  = '''''Fantasy Black Channel'''''<br />(2008)
| Next album  = 
| Misc        = {{Singles
  | Name           = Fantasy Black Channel
  | Type           = album
  | single 1       = [[Bathroom Gurgle]]
  | single 1 date  = 10 September 2007 <br/> Re-released: 2 October 2008
  | single 2       = [[The Bears Are Coming]]
  | single 2 date  = 2 March 2008
  | single 3       = [[Space and the Woods]]" / "[[Focker]]
  | single 3 date  = 19 May 2008
  | single 4       = [[Heartbeat (Late of the Pier song)|Heartbeat]]
  | single 4 date  = 4 August 2008 }} 
}}

'''''Fantasy Black Channel''''' is the debut and only studio album by British [[dance-punk]] band [[Late of the Pier]]. It was released on 30 July 2008 in Japan through [[Toshiba EMI]] and on 4 August 2008 in the British Isles on [[Parlophone]], the band's primary label. Five tracks had already been released as [[single (music)|singles]] in the United Kingdom: "[[Bathroom Gurgle]]", "[[The Bears Are Coming]]", "[[Space and the Woods]]" and "[[Focker]]" as a [[double A-side]], and "[[Heartbeat (Late of the Pier song)|Heartbeat]]". The record peaked at number 28 on the [[UK Albums Chart]], but failed to chart in the United States.

The album was recorded in [[frontman]] Sam Eastgate's bedroom in [[Castle Donington]], England, and at several locations in London. It went through a fractured creative process that lasted for more than two years. It was eventually produced by Eastgate and [[DJ]] [[Erol Alkan]] between 2007 and 2008. ''Fantasy Black Channel'' does not contain a unifying musical or lyrical theme; rather, it is a [[collage]] of all the ideas, genres, and studio effects that fascinated the band members and Alkan, especially during live recording sessions. The record was very well received by critics. It was often treated as one of the best British albums of 2008 because of its eclecticism and spirit of invention. Late of the Pier did not record any further albums following the death of [[drummer]] Ross Dawson in 2015.

== Origins and recording ==

Having officially formed a band under the name Late of the Pier in 2004,<ref name=BBCB>{{cite web |url=http://www.bbc.co.uk/berkshire/content/articles/2008/01/21/pier_210108_feature.shtml|title=Late Of The Pier at Plug 'n' Play|publisher=[[BBC]]|date=21 January 2008|accessdate=10 February 2009}}</ref> childhood friends Sam Eastgate, Andrew Faley, Sam Potter, and the late Ross Dawson initially developed the sound of their first album by listening to the [[alternative dance]] music of British ensemble [[The Prodigy]] and the [[grunge|grunge music]] of American band [[Nirvana (band)|Nirvana]].<ref name=LL>{{cite web |url=http://www.leftlion.co.uk/articles.cfm?id=2300|title=Late of the Pier Interview|work=[[LeftLion]]|author=Wilson, Jared|date=4 November 2008|accessdate=10 February 2009}}</ref><ref name=CM>{{cite web |url=http://www.clickmusic.com/articles/7185/Interview-Late-Of-The-Pier.html|title=Interview: Late Of The Pier |publisher=Click Music|author=Duke, Samuel|accessdate=10 February 2009}}</ref> They soon branched out into listening to diverse genres from the last 40 years of music, including [[Motown]] and [[soul music|soul]].<ref name=CM/><ref name=SCUL/> Potter has treated their conception of ''Fantasy Black Channel'' as a reaction to "mediocre, complacent [[indie music|indie]]-schmindie bands who find a sound and stick to it; whose songs sound exactly the same",<ref name=BBCC>{{cite web |url=http://www.bbc.co.uk/dna/collective/A27585787|title=Indie schmindie (Interview)|publisher=[[Collective (BBC)|Collective]]|author=Turnbull, Stuart|date=4 October 2007|accessdate=10 February 2009}}</ref> while lead writer and composer Eastgate has pointed out that they wanted to "take people past their own limits". The nascent recording stages took place in Eastgate's bedroom, where unconventional [[time signatures]] and experimental [[chord (music)|chords]] were performed because, at the time, no band member could play an instrument properly.<ref name=LL/>

Late of the Pier started using the album [[working title]] ''Interesting Adventure'' in 2006 after practising in Eastgate's bedroom for about a year and previewed their new material at the Liars Club in Nottingham.<ref name=noize>{{cite web|url=http://www.noizemakesenemies.co.uk/2008/02/late-of-pier-interview.html |title=Late Of The Pier – Interview |publisher=Noize Makes Enemies |author=Austin, Charlotte |accessdate=10 February 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20080607091736/http://www.noizemakesenemies.co.uk/2008/02/late-of-pier-interview.html |archivedate=7 June 2008 }}</ref> After receiving contract offers from Parlophone and [[Atlantic Records]], the band members signed to Parlophone because the label gave them total autonomy over the recording process without pressuring them to be commercially successful immediately.<ref name=LL/> The record deal was followed by the recording of an [[EP (music)|EP]] titled ''[[Zarcorp Demo]]'', from which a [[demo (music)|demo]] single, "Space and the Woods", was released in March 2007.<ref>{{cite web |url=http://drownedinsound.com/releases/9313|title=Late Of The Pier: Space And The Woods|publisher=[[Drowned in Sound]]|accessdate=29 May 2009}}</ref> Eastgate has claimed that the band members were influenced by the [[1980s in music|music of the 1980s]] during these formative stages of ''Fantasy Black Channel'' even though none of them were born before 1986.<ref>{{cite web |url=http://www.interviewmagazine.com/music/late-of-the-pier/|title=Late Of The Pier (Interview)|work=[[Interview Magazine|Interview]]|author=Rachel, T. Cole|accessdate=10 February 2009}}</ref>

{{quote box|quote=Sam [Eastgate] will ... make a computerised machine track. Then from there we learn what he's put down in a human way... After that process you get a recording of it like a demo, then from that we'll gig the track and it changes again. Our music goes through lots of filters: it's a really strange process and I don't think many bands do it.<ref name=SCUL>{{cite web |url=http://www.subba-cultcha.com/article_feature.php?id=5697|title=Late Of The Pier – Sam (Potter)|publisher=Subba-Cultcha|author=Holloway, Will|accessdate=15 May 2009}}</ref>|source=Sam Potter, on Late of the Pier's methods of creating ''Fantasy Black Channel''|width=20%|align=right}}

In late 2007, Late of the Pier formally met renowned DJ Erol Alkan after seeing him play a [[dj set|set]] at the Liars Club.<ref name=LL/> Alkan called them "THE most exciting band around" and offered the band members help in the recording process of ''Fantasy Black Channel''.<ref name=LL/><ref name=skinny>{{cite web |url=http://www.theskinny.co.uk/article/43887-late-of-the-pier-band-o-the-times|title=Late Of The Pier: Band O' The Times|work=[[The Skinny (magazine)|The Skinny]]|author=Mitchell, Nick|date=29 September 2008|accessdate=15 May 2009}}</ref> They accepted and made him producer for the album because of the immediate rapport that developed between the two parties.<ref name=LL/> Dawson has explained the choice of producer by suggesting that Alkan is famous for playing broad genres of music and that he understands the properties and crossover of [[dance music|dance]] and guitar music.<ref name=fused>{{cite web |url=http://www.fusedmagazine.com/2009/02/12/late-of-the-pier/|title=Music: Late Of The Pier|work=[[Fused Magazine|Fused]]|author=McNaney, Luke|date=12 February 2009|accessdate=15 May 2009}}</ref> Alkan fully embraced the band members' ideas and immediately understood what they were trying to achieve.<ref name=LL/> "Bathroom Gurgle" was recorded by the new collaboration and was released as a limited edition single in September 2007.<ref>{{cite web |url=http://www.moshimoshimusic.com/releases/late-of-the-pier/late-of-the-pier|title=Late Of The Pier|publisher=[[Moshi Moshi Records]]|accessdate=15 May 2009}}</ref>

The production process for ''Fantasy Black Channel'' gathered pace around December 2007.<ref name=PX>{{cite web |url=http://www.prefixmag.com/features/late-of-the-pier/interview/25112/|title=Late of the Pier: Interview|publisher=Prefix|author=Zipf, Jen|date=11 February 2009|accessdate=15 May 2009}}</ref> Late of the Pier usually proceeded by taking bedroom recordings into the studio, where they were refined by Alkan into "a more presentable package".<ref name=skinny/> They tried unconventional techniques in the style of [[avant garde]] producer [[Joe Meek]] during the live studio recording sessions, including stamping in baths and [[reamping]] guitars through air vents.<ref name=SCUL/> When tracks were [[audio mixing (recorded music)|mixed]] after being recorded, the band members, Alkan, and [[audio engineer|engineer]] Jimmy Robertson worked in tandem and unanimously decided when a track had finished undergoing the studio effects process.<ref name=PX/><ref name=inlay/> No songs changed after this point, even when one of the parties had further ideas.<ref name=PX/> At the time, in an interview with the band, Stuart Turnbull of the [[BBC Collective]] indicated that Alkan managed to "channel Late of the Pier's sonic attack into something more focused yet still undeniably different".<ref name=BBCC/>

== Promotion and release ==

[[File:LOTPBristolDot.jpg|thumb|right|200px|alt=An electric guitarist and a bassist are on stage beside a "JUNO-60 Roland" synthesiser keyboard, which is being played by the guitarist. A drum kit is in the background.|Late of the Pier in concert at the 2007 [[Dot to Dot Festival]] in Bristol]]

The record was nearly completed by the end of January 2008.<ref name=PX/> Late of the Pier stopped recording to embark on a [[headliner|headlining]] UK tour during the month of February, in which they previewed new material. "The Bears Are Coming" was released as a [[vinyl record|vinyl]] single in March 2008.<ref name=DIST>{{cite web |url=http://www.drownedinsound.com/articles/2672798|title=Updated: Late of the Pier tour in February|publisher=[[Drowned in Sound]]|author=Kharas, Kev|date=6 December 2007|accessdate=10 February 2009}}</ref> The band toured until the end of April 2008 to promote another single—a double A-side containing a reworked "Space and the Woods" and "Focker"—from the as-yet-unreleased ''Fantasy Black Channel''.<ref name=clash>{{cite web |url=http://www.clashmusic.com/news/late-the-pier-new-single-and-tour|title=Late of the Pier New Single and Tour|work=[[Clash (magazine)|Clash]]|author=Nowak, Sarah|date=9 April 2008|accessdate=10 February 2009}}</ref> Studio work with Alkan restarted in May 2008 to put the finishing touches on the album; the final version was christened as "a compilation of hits" and "anti-pop [[pop music|pop]]". The album was recorded during six months in different studios and the band members have admitted that the variety in venues partially explains why it sounds disjointed. The idea of having a single 45-minute track instead of a track list of songs was mooted at the end of May 2008, but was not followed through.<ref name=PX/>

''Fantasy Black Channel'''s track listing and UK CD release date of 11 August 2008 were confirmed on 11 June.<ref name=track>{{cite web |url=http://www.nme.com/news/late-of-the-pier/37259|title=Late Of The Pier announce album details – exclusive|work=[[NME]]|date=11 June 2008|accessdate=10 February 2009}}</ref> Faley has claimed that "the album fell into place itself, like the songs basically told us where they wanted to go on the album".<ref name=PX/> Late of the Pier picked the record's name at random after first contemplating using ''Peggy Patch and Her Sequenced Dress''.<ref name=skinny/> The [[cover art]] was designed by a friend of the band members from [[Brighton]], Jon Bergman.<ref name=noize/><ref name=inlay/> A final EP containing material already released in 2007 and 2008, ''Echoclistel Lambietroy'', was released in July 2008 as part of the marketing campaign for the forthcoming album.<ref name=EL>{{cite web|url=http://www.amazon.com/Echoclistel-Lambietroy-EP-Explicit/dp/B001B6J7HY|title=Echoclistel Lambietroy EP|publisher=[[Amazon.com|Amazon]]|accessdate=10 February 2009}}</ref> Potter has summed the record's conception by concluding, "I think in the three years to build up to the album, we never actually thought of a track list, and we never really kind of considered the fact that it should sound like an album. I think we recorded all the songs and then they were there and it was like, 'Oh, we have to kind of stitch this together and make it sound like one piece.'"<ref name=PX/>

== Content ==

===Lyrics===
The writing process for ''Fantasy Black Channel'' was largely unplanned. The verses of "Broken" were conceived late at night and are inspired by [[insomnia]]. It took Eastgate a long time to describe how he was feeling "in a way that sounded original" for such a common theme in pop music. The lyrics in the chorus are about the first time Late of the Pier drove to London and got lost. "Space and the Woods" tries to weigh up what is more important: a person or an inanimate object, or an absence of anything,<ref name=TBT>{{cite web |url=http://drownedinsound.com/in_depth/3976584-disection--late-of-the-pier-debut-album-track-by-track|title=DiSection: Late of the Pier debut album track-by-track|publisher=[[Drowned in Sound]]|author= Adams, Sean|date=10 September 2008|accessdate=20 November 2009}}</ref> while "The Bears Are Coming" concerns "a silent threat".<ref name=noize/> The idea for the lyric "A heartbeat, a flicker, a line" in "Heartbeat" came to Eastgate while recording the song and hearing its time signature always changing.<ref name=TBT/>

The lyricist cannot remember writing "White Snake" and did not know there was a [[Whitesnake|band of the same name]] in existence. "Focker" evolved out of a demo, called "6/8 Focker", Eastgate sang through a [[guitar amp]] with initially indecipherable lyrics. "The Enemy Are the Future" gets its name from a sentence in a promotional leaflet for the [[indie rock]] band [[The Enemy (UK band)|The Enemy]]. Eastgate has explained that for "Mad Dogs and Englishmen" he randomly "shouted lots of short phrases" and then tried to make sense of what he said. The process led to stylised lines like "Falling over aeroplanes and wanting to be a derelict".<ref name=TBT/>

===Composition===
''Fantasy Black Channel'' is largely built on sonic experiments and the use of studio and computer techniques. The opening track, "Hot Tent Blues", is an [[instrumental]] short which contains seven layers of electric guitar run through a [[Zoom Corporation|Zoom]] 506 [[effects units|bass guitar pedal]]. "Broken" was conceived during a jam session between Eastgate's guitar [[riff]]s and Dawson's drumming, while a demo of "Space and the Woods" similar to the Nirvana song "[[All Apologies]]" was reworked and remixed in the studio. Eastgate credits Storm Mortimer, who used to drive Late of the Pier around London, as the muse for "The Bears Are Coming". She often sang impromptu and the frontman eventually borrowed a page of her songs and added an [[afrobeat]]-inspired rhythm to it. Eastgate and Dawson wanted to create a track that evoked the idea of "coming out of the jungle" and credit varied musical acts like [[Slagsmålsklubben]], [[Prince (musician)|Prince]], [[The Beatles]], Mr Flash, FrYars, and [[Lutricia McNeal]] as inspiration.<ref name=TBT/>

{{Listen
 |filename     = HeartbeatLOTP.ogg
 |title        = "Heartbeat"
 |description  = "Heartbeat" is typical of Late of the Pier's unorthodox arrangements and genre fusion. The song is based on an acid house track and features a guitar solo played on the sampler.<ref name=TBT/>
}}
"Random Firl" was one of Alkan's favourites and he persuaded Late of the Pier to re-record it like a [[classical music|classical]] piece of music rather than in a rock-oriented way. The [[vocal harmonies]] at the end of the song were added during one of the last days of recording when the pressure to complete the album was high. "Heartbeat" is based on an old [[acid house]] demo track that Eastgate created during his childhood using a [[Roland TB-303]] electronic [[synthesiser]]-[[music sequencer|sequencer]]. The track features complex time signature progressions and a [[guitar solo]] played by Potter on the [[sampler (musical instrument)|sampler]] at the end. "Whitesnake" was inspired by the music of [[Devo]], [[Roxy Music]], and [[Dandi Wind]], while "VW" is based on a 2001 demo recorded in Eastgate's attic. In the studio, Late of the Pier hired [[brass section|brass player]]s to perform the [[horn section]]s and create new [[scale (music)|scales]]. Once the instrumentals were recorded, the band members purposefully decided to mix their own playing lower in certain sections of the song so that listeners could hear the [[French horn|horns]] more clearly.<ref name=TBT/>

"Focker" was created using a [[loop (music)|looped]] riff from "6/8 Focker". Eastgate then conceived a drum solo and remixed and rearranged the last 20 seconds of the song. "The Enemy Are the Future" was created during a [[hungover]] jam session, where all band members played individually and not in tandem. Dawson performed around 40 different beats one after another all in different timings. The session was recorded using a [[tape player]] and a track was moulded in the studio. Four different endings were recorded and three were spliced together at the end. "Mad Dogs and Englishmen", whose riff is based on an old French [[samba]] song, was borne out of an interplay between Eastgate on the guitar and Faley on the bass. The last track on the album, "Bathroom Gurgle", includes "a squelchy grind of the riff" purpose-built for the Liars Club audience during the band's first ever gigs. Eastgate has explained, "I had these two themes, which looped round on each other which I sang very exasperated [[Annie Lennox]] style vocals over." The fast [[tempo]] of the first half of the song was juxtaposed against a section of "slow drama".<ref name=TBT/>

== Critical reception ==
{{Album ratings
| MC = 81/100<ref name=MC/>
| rev1 = [[AllMusic]]
| rev1Score = {{Rating|4|5}}<ref name=AMG/>
| rev2 = ''[[Alternative Press (music magazine)|Alternative Press]]''
| rev2Score = 4/5<ref>{{cite magazine|magazine=[[Alternative Press (music magazine)|Alternative Press]]|page=102|date=February 2009|title=Review|author=Anon.}}</ref>
| rev3 = ''[[Blender (magazine)|Blender]]''
| rev3Score = {{Rating|3.5|5}}<ref name=blender/>
| rev4 = ''[[The Guardian]]''
| rev4Score = {{Rating|5|5}}<ref name=TG/>
| rev5 = ''[[The Irish Times]]''
| rev5Score = {{Rating|3|5}}<ref>{{cite web|last=Ortiz|first=Deanna|date=1 August 2008|url=http://www.irishtimes.com/culture/rock-pop-1.925377|title=Rock & Pop|work=[[The Irish Times]]|accessdate=16 December 2016}}</ref>
| rev6 = ''[[NME]]''
| rev6Score = {{Rating|4|5}}<ref name=NME/>
| rev7 = ''[[Pitchfork (website)|Pitcthfork]]''
| rev7Score = 6.7/10<ref name=pitch/>
| rev8 = ''[[PopMatters]]''
| rev8Score = 8/10<ref name=popm/>
| rev9 = ''[[Spin (magazine)|Spin]]''
| rev9Score = {{Rating|3|5}}<ref>{{cite magazine|last=Young|first=Jon|date=January 2009|page=90|url=https://books.google.com/books?id=s_G9SEfNaboC&pg=PA90|title=Reviews|magazine=[[Spin (magazine)|Spin]]|accessdate=16 December 2016}}</ref>
| rev10 = ''[[Uncut (magazine)|Uncut]]''
| rev10Score = {{Rating|4|5}}<ref>{{cite magazine|magazine=[[Uncut (magazine)|Uncut]]|title=Review|page=90|date=September 2008|author=Anon.}}</ref>
}}
Media response to ''Fantasy Black Channel'' was highly favourable; aggregating website [[Metacritic]] reports a [[standard score|normalised]] rating of 81% based on 18 critical reviews.<ref name=MC>{{cite web |url=http://www.metacritic.com/music/artists/lateofthepier/fantasyblackchannel|title=Fantasy Black Channel by Late Of The Pier |publisher=[[Metacritic]]|accessdate=6 February 2009}}</ref> [[AllMusic]]'s K. Ross Hoffman described the album as "glorious and galvanising" and labelled it "a convoluted construction crammed with so many immediately gratifying moments that it takes multiple listens to extricate them all".<ref name=AMG>{{cite web |url=http://www.allmusic.com/album/fantasy-black-channel-r1408336/review|title=Fantasy Black Channel: Late of the Pier|author=Hoffman, K. Ross|publisher=[[Allmusic]]|accessdate=10 February 2012}}</ref> John Burgess of ''[[The Guardian]]'' explained that a new favourite song, riff, or wayward moment can be found every time that the album is listened to.<ref name=TG>{{cite web |url=https://www.theguardian.com/music/2008/aug/22/rockreview.lateofthepier|title=Rock review: Late of the Pier, Fantasy Black Channel|author=Burgess, John|work=[[The Guardian]]|date=22 August 2008|accessdate=10 February 2009}}</ref> Tim Chester of ''[[NME]]''  concluded, "It takes a certain kind of mind to make this. While others whinge about living and dying in [[cul-de-sac]] towns shackled by cul-de-sac imaginations, LOTP's vision is housed on [[Dubai]]'s '[[The World (archipelago)|The World]]' island-creation project."<ref name=NME>{{cite web |url=http://www.nme.com/reviews/late-of-the-pier/9835|title=Late Of The Pier: Fantasy Black Channel|author=Chester, Tim|work=[[NME]]|date=8 August 2008|accessdate=10 February 2009}}</ref>

Some reviewers classified ''Fantasy Black Channel'' into the British New Rave genre and musical scene.<ref name=dis>{{cite web |url=http://www.drownedinsound.com/releases/13581/reviews/3727733|title= Late Of The Pier: Fantasy Black Channel|publisher=[[Drowned in Sound]]|author=Diver, Mike|date=25 July 2008|accessdate=10 February 2009}}</ref><ref name=pitch/> Others claimed that they could hear impressions of Roxy Music frontman [[Bryan Ferry]] and musician [[Gary Numan]] in the recording.<ref name=blender>{{cite web|url=http://www.blender.com/guide/new/55379/fantasy-black-channel.html |title=Late of the Pier: Fantasy Black Channel |work=[[Blender Magazine|Blender]] |author=Dolan, Jon |date=13 January 2009 |accessdate=10 February 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20090821132036/http://www.blender.com/guide/new/55379/fantasy-black-channel.html |archivedate=21 August 2009 }}</ref><ref name=URB>{{cite web |url=http://www.urb.com/reviews/cd/feature.php?ReviewId=964|title=Late of the Pier: Fantasy Black Channel|work=[[URB Magazine|URB]]|author=Cuellar, Jorge|date=13 January 2009|accessdate=10 February 2009}}</ref> [[Pitchfork Media]]'s Adam Moerder commented that it contains several New Rave compositions which are aesthetically dubious.<ref name=pitch>{{cite web |url=http://pitchfork.com/reviews/albums/12555-fantasy-black-channel/|title=Late of the Pier: Fantasy Black Channel|author=Moerder, Adam|publisher=[[Pitchfork Media]]|date=14 February 2009|accessdate=25 March 2009}}</ref> Chris Baynes of [[PopMatters]] claimed that the band members "wear their influences pretty much inked onto their sleeves", but concluded that the album is exciting and excitable in equal measure.<ref name=popm>{{cite web |url=http://www.popmatters.com/pm/review/69592-late-of-the-pier-fantasy-black-channel/|title=Late of the Pier: Fantasy Black Channel|author=Baynes, Chris|publisher=[[PopMatters]]|date=17 February 2009|accessdate=10 February 2009}}</ref> According to Late of the Pier, comparisons to [[Klaxons]] and New Rave are the results of lazy journalism, especially for a scene the band has never been in.<ref name=CM/> Eastgate has explained that musical influences are often subconscious, even though the band members tried not to listen to anything but their own work while recording the album.<ref name=SCUL/> Nick Mitchell of ''[[The Skinny (magazine)|The Skinny]]'' pointed out that the record is "an unrestrained, unclassifiable, unexpectedly triumphant romp through blaring influences and genres, from the 70s camp rock of [[Queen (band)|Queen]] and [[David Bowie|Bowie]] to the primitive [[electronic music|electronics]] of Gary Numan, with echoes of 90s computer games and snatches of modern [[house music|house]]".<ref name=skinny/>

''Fantasy Black Channel'' figured in several publications' end-of-year best album lists for 2008, notably, at number three by ''[[Clash (magazine)|Clash]]'',<ref>{{Cite news |author = ''Clash'' staff|title=The 40 Best Albums of 2008|publisher=''[[Clash (magazine)|Clash]]''|page=71|date=December 2008}}</ref> at number five by [[Fact Magazine (United Kingdom)|''FACT'']],<ref>{{cite web |url=http://www.factmagazine.co.uk/index.php?option=com_content&task=view&id=1579&Itemid=27&limit=1&limitstart=1|title=20 best: albums of 2008|publisher=[[Fact Magazine (United Kingdom)|''FACT'']]|author=''FACT'' staff|date=19 December 2008|accessdate=25 July 2009}}</ref> and at number 18 by ''[[NME]]''.<ref name>{{Cite news |title=Best Albums Of 2008|author=''NME'' staff|publisher=''[[NME]]''|page=50|date=13 December 2008}}</ref> It was ranked at number 16 in [[HMV's Poll of Polls]] for 2008, which aggregates the votes of prominent British critics to decide the commercial group's Album Of The Year.<ref name=DT>{{cite web |url=http://www.telegraph.co.uk/culture/culturenews/4014628/Elbow-wins-critics-album-of-the-year.html|title=Elbow wins critics' album of the year |work=[[The Daily Telegraph]]|date=29 December 2008|accessdate=10 February 2009}}</ref> Earlier in 2008, Rory Carroll of ''[[Artrocker]]'' claimed that the record would have been a late entrant for the [[Mercury Music Prize]], and would have probably won the award, if nominations had been made in August rather than July 2008.<ref name=AR>{{cite web |url=http://www.artrocker.com/node/1829|title=Late Of The Pier – Fantasy Black Channel|work=[[Artrocker]]|author= Carroll, Rory|accessdate=10 February 2009}}</ref> Because of their work on the album, Late of the Pier received a nomination for Best New Band at the 2009 [[NME Awards|''NME'' Awards]]. Two tracks were also nominated: "Bathroom Gurgle" and "Heartbeat", in the Best Dancefloor Filler and Best Video categories respectively.<ref name=NMEA>{{cite web |url=http://www.nme.com/news/shockwaves-nme-awards-2008-big-gig/42311|title=Oasis, Alex Turner, Killers: Shockwaves NME Awards 2009 nominations|work=[[NME]]|date=26 January 2009|accessdate=10 February 2009}}</ref> In 2009, ''Clash'' placed ''Fantasy Black Channel'' at number 40 in its list of the 50 Greatest Albums, 2004–2009,<ref>{{cite web|url=http://www.clashmusic.com/feature/clash-essential-50-42-39|work=[[Clash (magazine)|Clash]]|author=Diver, Mike|title=Clash Essential 50 – 42–39|date=14 April 2009|accessdate=17 November 2009}}</ref> while ''FACT'' included it at number 99 in its editorial staff list of the 100 Best: Albums of the Decade.<ref>{{cite web|url=http://www.factmagazine.co.uk/index.php?option=com_content&task=view&id=4095&Itemid=103&limit=1&limitstart=9|work=[[FACT (United Kingdom magazine)|FACT]]|author=''FACT'' staff|title=100 best: Albums of the Decade|date=30 November 2009|accessdate=13 December 2009}}</ref>

== Track listing ==

All songs written and arranged by Sam Eastgate, unless otherwise stated.
{{tracklist
| title1   = Hot Tent Blues
| note1    = 
| length1  = 1:16
| title2   = Broken
| note2    = 
| length2  = 4:11
| title3   = [[Space and the Woods]]
| note3    = 
| length3  = 3:44
| title4   = [[The Bears Are Coming]]
| note4    = Sam Eastgate, Storm Mortimer
| length4  = 3:23
| title5   = Random Firl
| note5    = Sam Eastgate, Andrew Faley
| length5  = 2:14
| title6   = [[Heartbeat (Late of the Pier song)|Heartbeat]]
| note6    = 
| length6  = 3:02
| title7   = Whitesnake
| note7    = 
| length7  = 3:01
| title8   = VW
| note8    = Sam Eastgate, Richard Eastgate
| length8  = 2:27
| title9   = [[Focker]]
| note9    = 
| length9  = 3:14
| title10  = The Enemy Are the Future
| note10    = Sam Eastgate, Andrew Faley, Sam Potter, Ross Dawson
| length10 = 6:01
| title11  = Mad Dogs and Englishmen
| note11    = 
| length11 = 3:00
| title12  = [[Bathroom Gurgle]]
| note12    = Sam Eastgate, Andrew Faley, Sam Potter, Ross Dawson
| length12 = 7:14
}}
* A [[hidden track]], "No Time", begins at 4:50 of "Bathroom Gurgle".

=== Bonus tracks ===
* "Dose A" – 1:47 – track 13 on the [[iTunes]] version
* "Very Wav" – 4:44 – track 13 on the Japanese and U.S. editions
* "Focker (Rolmops [[Remix]])" – 3:15 – track 14 on the Japanese edition
* "The Bears Are Coming (Emperor Machine Remix)" – 9:22 – track 14 on U.S. edition and track 15 on the Japanese edition

=== Vinyl ===
The 2008 UK LP version of ''Fantasy Black Channel'' comprised a standard black vinyl copy in a [[gatefold]] picture sleeve. It was released a week earlier than the CD version and had the following track changes:<ref name=LPEIL>{{cite web |url=http://eil.com/shop/moreinfo.asp?catalogid=441465|title=Fantasy Black Channel UK LP|publisher=Esprit International |accessdate=17 May 2009}}</ref> 
* "Space and the Woods ([[Cenzo Townsend]] Mix)" instead of "Space and the Woods"
* "The Bears Are Coming (Original Version)" instead of "The Bears Are Coming"
* "Heartbeat (Cenzo Townsend Version)" instead of "Heartbeat"
The 2009 U.S. LP version was released concurrently with the CD version.<ref name=AMG/>

== Personnel ==
<ref name=inlay>{{cite AV media notes|title=Fantasy Black Channel|others=[[Late of the Pier]]|year=2008|type=CD booklet (pages 6–7) and case back cover|publisher=[[Parlophone]]|location=London}}</ref>

{{col-begin}}
{{col-2}}
'''Band'''
* Sam Eastgate – [[singing|lead vocals]], [[electric guitar]], [[acoustic guitar]], [[drum machine|drum sequencing]], [[synthesiser]]
* Andrew Faley – [[bass guitar]], [[singing|backing vocals]], bass synthesiser
* Sam Potter – [[sampler (musical instrument)|sampler]], synthesiser, backing vocals
* Ross Dawson – [[drum kit|drum]]s

'''Additional musicians'''
* Giselle Kennedy – backing vocals (track 2)
* Erol Alkan – backing vocals (track 5)
* Nik Carter – [[tenor|tenor vocals]], [[baritone|baritone vocals]] (track 8)
* Jack Birchwood – [[trumpet]], [[flugel horn]] (track 8)

{{col-2}}
'''Production'''
* Erol Alkan – [[music producer|producer]]; [[audio mixing (recorded music)|mixing]] (except tracks 1, 2, 3)
* Sam Eastgate – producer (tracks 1, 2, 4, 9, 12); arrangements
* Jimmy Robertson – [[sound engineer|engineering]]; mixing (except tracks 1, 2, 3)
* Mark Alloway – assistant engineer (tracks 1, 2, 3, 7, 9, 10, 11)
* Al Lawson – assistant engineer (track 1)
* Ben Jackson – assistant engineer (track 7)
* Oli Wright – assistant engineer (track 12)
* Cenzo Townsend – mixing (tracks 1, 2, 3)
* Neil Comber – assistant mixer (track 1, 2, 3)
* Darren Simpson – assistant mixer (track 4)
* Daniel Rejmer – assistant mixer (tracks 5, 11)
* Lee Slaney – assistant mixer (tracks 6, 7, 8, 9, 10, 12)
* Mike Marsh – [[audio mastering|mastering]] (except track 4)
* Nilesh Patel – mastering (track 4)

'''Artwork'''
* Jon Bergman – [[cover art]]
* Late of the Pier – [[design]]
* Traffic – design
{{col-end}}

== Recording and release details ==

''Fantasy Black Channel'' was recorded between 2006 and 2008 in Sam Eastgate's bedroom in Castle Donington and at the following studios in London.<ref name=inlay/>
{| class="wikitable"
|-
! Track
! Location
|-
|"Hot Tent Blues"
|Sam Eastgate's bedroom; Sofa Sound
|-
|"Broken"
|Sam Eastgate's bedroom; Sofa Sound
|-
|"Space and the Woods"
|The [[Miloco]] Garden
|-
|"The Bears Are Coming"
|Sam Eastgate's bedroom; The Miloco Garden
|-
|"Random Firl"
|Sofa Sound
|-
|"Heartbeat"
|Sofa Sound
|-
|"White Snake"
|The Miloco Garden; [[EMI]] Publishing Studios
|-
|"VW"
|The Miloco Garden
|-
|"Focker"
|Sam Eastgate's bedroom; The Miloco Garden
|-
|"The Enemy Are the Future"
|The Garden
|-
|"Mad Dogs and Englishmen"
|The Miloco Garden
|-
|"Bathroom Gurgle"
|Sam Eastgate's bedroom; The Miloco Garden; Miloco [[Hoxton Square]]; Strongroom; Miloco Musikbox
|}

The album's release history is as follows:
{|class="wikitable"
! Region
! Date
! Label
! Format(s)
! Catalog
|-
| Japan
| 30 July 2008
| Toshiba EMI
| CD
| TOCP-66797<ref name=JP>{{cite web |url=http://eil.com/shop/moreinfo.asp?catalogid=444290|title=Fantasy Black Channel Japan CD|publisher=Esprit International |accessdate=17 May 2009}}</ref>
|-
| rowspan="2"|United Kingdom and Ireland
| 4 August 2008
| rowspan="2"|Parlophone
| LP
|228 0331<ref name=LP>{{cite web |url=http://eil.com/shop/moreinfo.asp?catalogid=441465|title=Fantasy Black Channel UK LP Record|publisher=Esprit International |accessdate=17 May 2009}}</ref>
|-
| 11 August 2008
| CD, [[Music download|digital download]]
|228 0342<ref name=inlay/>
|-
| France
| 4 September 2008
| [[Because Music]]
| CD
| BEC 5772361<ref name=inlayfr>{{cite AV media notes|title=Fantasy Black Channel|others=[[Late of the Pier]]|year=2008|type=CD case back cover|publisher=[[Because Music]]|location=London / Paris}}</ref>
|-
| rowspan="2"|United States
| rowspan="2"|13 January 2009
| rowspan="2"|Astralwerks
| LP
| ASW 28033<ref name=AMG/>
|-
| CD, digital download
| ASW 37034<ref name=US>{{cite web |url=http://eil.com/shop/moreinfo.asp?catalogid=475225|title=Fantasy Black Channel USA Promo CD Album|publisher=Esprit International|accessdate=28 May 2009}}</ref>
|-
|}

== Chart positions ==

{{col-begin}}
{{col-2}}
'''Album'''
{| class="wikitable sortable"
|-
! Chart (2008)
! Peak
|-
|[[UK Albums Chart]]<ref>{{cite web |url=http://www.thenorthernecho.co.uk/uk_national_entertainment/3601482.print/|title=Perry at number one for second week|work=[[The Northern Echo]]|date=17 August 2008|accessdate=17 May 2009}}</ref>
|align="center"| 28
|-
| [[Ultratop|Belgian Albums Chart]] ([[Flemish Region|Flanders]])<ref name=BEL>{{cite web |url=http://www.ultratop.be/nl/showitem.asp?interpret=Late+Of+The+Pier&titel=Fantasy+Black+Channel&cat=a|title=Late Of The Pier – Fantasy Black Channel|publisher=[[Ultratop]]|accessdate=17 May 2009|language=nl}}</ref>
|align="center"| 68
|-
| Belgian [[alternative music|Alternative]] Albums<ref name=BEL/>
|align="center"| 30
|}

{{col-2}}
'''Singles'''
{| class="wikitable"
! rowspan="2"| Song
! colspan="4"| Peak
|-
! width="50"|<small>[[UK Singles Chart|UK]]<br /></small>
|-
|-
|"[[Bathroom Gurgle]]"
|align="center"|—
|-
|"[[The Bears Are Coming]]"
|align="center"|—
|-
|"[[Space and the Woods]]" / "[[Focker]]"
|align="center"|—
|-
|"[[Heartbeat (Late of the Pier song)|Heartbeat]]"<ref>{{cite journal|title = The Official UK Singles Chart: For the week ending 16 August 2008|journal = [[ChartsPlus]]|issue = 364|page=2|publisher = Musiqware|location = Milton Keynes}}</ref>
|align="center"|98
|-
|"Bathroom Gurgle" <small>(re-release)</small>
|align="center"|—
|-
|}
<small>"—" denotes releases that did not chart.</small><br>
{{col-end}}

== References ==
{{Reflist|colwidth=30em}}

== External links ==
*[https://web.archive.org/web/20090228185257/http://www.lateofthepier.com:80/ ''Fantasy Black Channel'' music player] at Late of the Pier official site
*[http://www.lateofthepier.com/main/lyrics.php ''Fantasy Black Channel'' lyrics] at Late of the Pier official site
*[http://www.metacritic.com/music/artists/lateofthepier/fantasyblackchannel ''Fantasy Black Channel'' critical reviews] at [[Metacritic]]

{{Late of the Pier}}

{{featured article}}

[[Category:2008 debut albums]]
[[Category:Parlophone albums]]
[[Category:Late of the Pier albums]]