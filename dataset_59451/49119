{{Infobox musical artist  <!-- See Wikipedia:WikiProject Musicians -->
| name                = Szymon Krzeszowiec
| image                 =
| image_size            = 
| landscape           = yes
| caption            =  
| background          = classical_ensemble
| origin              = [[Tychy]], [[Katowice]], Poland
| occupation          = [[violin]]ist
| genre               = [[contemporary]], [[classical music|classical]]
| label               = [[Dux Records|DUX]], [[Sony Classical]]
| website                 = 
}}
'''Szymon Krzeszowiec''' (pronunciation: [[Wikipedia:IPA for Polish|ʂɨmɔn kʐɛʂɔviɛts]] (born April 20, 1974) is a Polish [[violin]]ist, [[chamber musician]] and [[pedagogue]]. Musician of the [[Silesian String Quartet]] and member of the Trio Aristos.<ref>[http://www.culture.pl/web/english/resources-music-full-page/-/eo_event_asset_publisher/eAN5/content/szymon-krzeszowiec%5D culture.pl]</ref>

== Education ==

Szymon Krzeszowiec was born in [[Tychy]], Poland.  He started his musical education in the Complex of State Music Schools in [[Katowice]], with Urszula Szygulska. Later, he went to the [[Karol Szymanowski]] Secondary Musical School in Katowice and studied there with prof. Paweł Puczek. As a pupil of this school, he won first prizes on all-Poland competitions and auditions. He took part in summer music academies in [[Łańcut]] and [[Żagań]].

Between 1993 and 1997, he studied in the violin class of prof. Roman Lasocki in the [[Karol Szymanowski Academy of Music|Karol Szymanowski Academy of Music in Katowice]] (diploma with distinction). Later, he studied at the [[Conservatorium van Amsterdam]] with prof. [[Herman Krebbers]]. He participated in masterclasses with such artists as, among others: [[Glenn Dicterow]], Dmitri Ferschtman, Paweł Głombik, [[Yair Kless]], Krzysztof Węgrzyn and Tadeusz Wroński.<ref>[http://www.rmfclassic.pl/informacje/muzyka,trio-aristos-zwyciezylo-w-konkursie-muzyki-kameralnej-radia-dunskiego,1955.html RMF Classic Poland]</ref>

== Competitions ==

Szymon Krzeszowiec is a laureate of violin competitions in [[Poznań]] (Ogólnopolski Konkurs Skrzypcowy im. Z. Jahnkego), [[Warsaw]] (Konkurs na Skrzypce Solo im. T. Wrońskiego) and [[Brescia]] ([[Italy]]), of [[chamber music]] competition in [[Łódź]] (Konkurs Muzyki Kameralnej im. Kiejstuta Bacewicza).

== Artistic activity ==

Most recently, the artistic activity of Szymon Krzeszowiec is dominated by chamber performances, but his solo career still develops.

=== Soloist ===

As a soloist, he performed with such orchestras as : [http://www.nospr.org.pl/index.xml National Polish Radio Symphony Orchestra], [http://www.sinfoniavarsovia.org/sv.php?ref=%2F Sinfonia Varsovia], [[Silesian Philharmonic]] (Symphony Orchestra), [http://www.aukso.pl/ AUKSO Chamber Orchestra ] and many others. He played under the baton of conductors: Mirosław Jacek Błaszczyk, Jacek Boniecki, [[Leo Brouwer]], Tomasz Bugaj, Szymon Bywalec, Sławomir Chrzanowski, Michał Dworzyński, Czesław Grabowski, Jan Wincenty Hawel, Michał Klauza, Jerzy Kosek, Marek Moś, Michał Nesterowicz, [[Grzegorz Nowak (conductor)|Grzegorz Nowak]], Charles Olivieri-Munroe, Janusz Powolny, Ahmed el Saedi, Jerzy Salwarowski, [[Robert Satanowski]], Tadeusz Strugała, Jerzy Swoboda, Tomasz Tokarczyk, Tadeusz Wicherek, Piotr Wijatkowski, [[Tadeusz Wojciechowski]], Christoph Wyneken and Jan Miłosz Zarzycki.

=== Cooperation with pianists ===

For many years, Szymon Krzeszowiec has played with eminent [[pianists]] : Maria Szwajger-Kułakowska and Wojciech Świtała. With the latter, Krzeszowiec recorded his first CD, containing the three Piano & Violin [[Sonata]]s by [[Johannes Brahms]]. It was published in 2001 by the [[Sony Classical]] label. This album was nominated to the « [[Fryderyk]] » award as the best chamber music recording.

In 2011, he recorded another violin & piano CD, with a leading [[Sweden|Swedish]] pianist, [[Niklas Sivelöv]]. This album, named « Fratres », contains pieces inspired by [[baroque]] music, written by [[20th-century classical music|20th century composers]]. It has been published by the [[Dux Records|DUX]] label.<ref>[http://www.dux.pl/catalogue/results/details/?pid=553 DUX]</ref>

=== Silesian String Quartet ===

Since 2001, Szymon Krzeszowiec has been the first violinist of the [[Silesian String Quartet]]. Rehearsals, recordings and performances as a member of this ensemble have become his most important activities. As a chamber musician, Krzeszowiec played with such artists as, among others : Max Artved, Detlef Bensman, [[Bruno Canino]], Dmitri Ferschtman, Maciej Grzybowski, Paul Gulda, [[Krzysztof Jabłoński]], Krzysztof Jakowicz, [[Andrzej Jasiński]], [[Jadwiga Kotnowska]], [[Ralph Kirshbaum]], [[Mats Lidstroem]], Waldemar Malicki, Vladimir Mendelssohn, Tomasz Miczka, Bartłomiej Nizioł, [[Janusz Olejniczak]], Bruno Pasquier, Piotr Pławner, Ewa Pobłocka, Dominik Połoński, [[Niklas Sivelöv]], [[Jan Stanienda]], Tomasz Strahl, Piotr Szymyślik, Marek Toporowski. He performed or/and recorded with such ensembles as Dominant, Lutosławski, Royal, Stamic, Vanbrugh and Wieniawski string quartets. <br /><ref>[http://www.emimusic.pl/artysci,,string_quartets EMI Music Poland]</ref>

On the 15 CDs recorded by the [[Silesian String Quartet]] with Krzeszowiec as the first violinist, there are mainly pieces by [[Polish composers]] of the last 30 years. Chamber music of this period of Polish music is often performed by the Silesian String Quartet, as promoting [[contemporary music]] is one of the aims of the ensemble.<ref>[http://www.metro.co.uk/metrolife/292810-five-questions-for-szymon-krzeszowiec metro.co.uk - interview with Szymon Krzeszowiec]</ref> In 2002, the Silesian String Quartet won the prestigious « Orfeusz » Award, given by the [http://spa-m.pl/ Association of Polish Musicians], for having premiered Witold Szalonek’s Symphony of rituals on the « [[Warsaw Autumn]] » Festival.<br /><ref>[http://www.polishculture-nyc.org/index.cfm?itemcategory=30817&personDetailId=425 Polish Cultural Institute in New York]</ref>

=== Aristos Trio ===

While studying in such a cultural centre as Amsterdam, Szymon Krzeszowiec met Danish musicians [[Jakob Kullberg]] (cello) and Alexander Øllgaard (viola). Since 2004, they have performed as the Trio Aristos. In 2006, this ensemble won prestigious chamber music competitions in Copenhagen (Denmark) and Sondershausen (Germany). In Denmark, the Trio Aristos was contacted by the composer Per Nørgård, who asked the musicians to premiere some of his works.<ref>[http://www.dankultur.ee/eng/calendar/2006/ dankultur.ee]</ref>

==== Teaching ====
Since 1998, Szymon Krzeszowiec is a pedagogue of the Karol Szymanowski Academy of Music in Katowice. He teaches at the Karol Szymanowski Secondary Musical School as well. Annually since 2004, he has given chamber music master classes at the International Princess Daisy Chamber Arts Festival Ensemble. Often, Krzeszowiec gives violin master classes and works in juries of musical competitions.

==== Discography ====

{| class="wikitable"
|-
! Year !! Album details !! Recorded works !! Label
|-
| <small>2011</small> || <small>FRATRES<br />
Szymon Krzeszowiec - violin<br />
[[Niklas Sivelöv]] - piano</small><br />
|| <small><br />
<small>[[Igor Stravinski]] - Suite d’après les thèmes, fragments et morceaux de [[Giambattista Pergolesi]]<br />
[[Luigi Dallapiccola]] - Tartiniana Seconda<br />
[[Max Reger]] - Suite im alten Stil, op. 93<br />
[[Arvo Pärt]] - Fratres<br />
[[Alfred Schnittke]] - Suite im alten Stil, op. 80</small><br />
|| <small>[[Dux Records|DUX]] 0840</small>
|-
| <small>2010</small> || <small>ALEKSANDER TANSMAN<br />
TOMBEAU DE CHOPIN<br />
from [[trio (music)|trio]] to [[Octet (music)|octet]] vol.2<br />
Joanna Liberadzka - [[harp]]<br />
Elżbieta Mrożek-Loska - [[viola]]<br />
Krzysztof Firlus - [[double bass]]<br />
Adam Krzeszowiec - [[cello]]<br />
Jan Krzeszowiec - [[flute]]<br />
Roman Widaszek - [[clarinet]]<br />
[[Silesian String Quartet]]</small><br />
|| <small>[[Aleksander Tansman]]:<br />
Sextuor à cordes for 2 violins, 2 violas and 2 cellos<br />
Sonatina da camera for flute, violin, viola, cello and harp<br />
Tombeau de Chopin for string quintet<br />
Trois pieces for clarinet, harp and string quartet</small><br />
|| <small>Stowarzyszenie Promocji Kultury im. Aleksandra Tansmana<br />
www.tansman.lodz.pl</small><br />
|-
| <small>2010</small> || <small>STANISŁAW KRUPOWICZ - “TYLKO BEATRYCZE”</small><br />
<small>String quartets and computer</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <small>Stanisław Krupowicz:</small><br />
<small>Goodbye variations on a theme by Mozart</small><br />
<small>Tylko Beatrycze (Only Beatrice)</small><br />
<small>Prolongement</small><br />
 || <small>Musica Pro Bono Foundation</small><br />
<small>FMPB CD004</small><br />
|-
| <small>2010</small> || <small>RAFAŁ AUGUSTYN<br />
DO UT DES MUSIC FOR AND WITH QUARTET<br />
Agata Zubel - [[soprano]]<br />
Jan Krzeszowiec - [[flute]]<br />
[[Silesian String Quartet]]</small><br />
|| <small>Rafał Augustyn:<br />
String Quartet No.1<br />
String Quartet No.2 with flute<br />
Dedication for soprano & string quartet<br />
''Do ut des'' for string quartet<br />
''Grand jeté''. Quartet No.2½ with electronics</small><br />
|| <small>[http://www.cdaccord.com.pl www.cdaccord.com.pl]<br />
[http://www.universalmusic.pl www.universalmusic.pl]<br />
ACD 165-2</small><br />
|-
| <small>2009</small> || <small>KALEIDOSCOPE<br />
Polish [[contemporary music]] <br />
for violin solo<br />
Szymon Krzeszowiec - [[violin]]</small><br />
|| <small><br />
[[Krzysztof Meyer|K. Meyer]] - Sonata op. 36<br />
S. Moryto - Aria e Chorale<br />
[[Paweł Szymański|P. Szymański]] - Kaleidoscope for M.C.E<br />
[[Krzysztof Penderecki|K. Penderecki]] - Cadenza<br />
T. Wielecki - Przędzie się nić...II<br />
[[Eugeniusz Knapik|E. Knapik]] - Filo d’Arianna<br />
W. Szalonek - Chaconne-Fantaisie</small><br />
|| <small>DUX 0688</small>
|-
| <small>2009</small> || <small>STRING QUARTETS / POLAND ABROAD<br />
[[Silesian String Quartet]]</small><br />
|| <small>Joachim Mendelson (1897-1943):<br />
String Quartet no.1<br />
Roman Padlewski (1915-1944):<br />
String Quartet no.2<br />
[[Simon Laks]] (1901-1983):<br />
String Quartet no.5</small><br />
|| <small>[http://www.eda-records.com www.eda-records.com]<br />
EDA 34</small>
|-
| <small>2009</small> || <small>GET STRING</small><br />
<small>Danish contemporary string quartets</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <br />
<small>Jens Voigt-Lund: Circuitous, Mountains (1999)<br />
Morten Riis: getString (2009)<br />
Christian Winther Christensen: String Quartet (2002–03)<br />
Morten Riis: fromString (2009)<br />
Jexper Holmen: Intend/Ascend (2000/02)<br />
Morten Riis: useString (2009)<br />
Simon Steen-Andersen: String Quartet (1999)<br />
Morten Riis: toString (2009)<br />
Simon Christensen: Towards Nothingness (2008)<br />
Morten Riis: quitString (2009)</small><br />
 || <small>dacapo</small><br />
<small>CD 8.226530</small><br />
|-
| <small>2008</small> || <small>ANDRZEJ DZIADEK - chosen works</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <small>A. Dziadek:</small><br />
<small>String quartet no. 2</small><br />
 || <small>Polskie Radio Katowice</small><br />
<small>PRK CD086</small><br />
|-
| <small>2008</small> || <small>HELENA TULVE - LIJNEN</small><br />
<small>[[Silesian String Quartet]]</small><br />
<small>various artists</small><br />
 || <small>[[Helena Tulve|H. Tulve]]:</small><br />
<small>''nec ros, nec pluvia...'' for string quartet</small><br />
 || <small>ECM New Series</small><br />
<small>ECM 1955 476 6389</small><br />
<small>"Bestenliste der Deutschen Schallplattenkritik" 03/2008</small><br />
|-
| <small>2008</small> || <small>HENRYK MIKOŁAJ GÓRECKI - STRING QUARTETS</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <small>[[Henryk Mikołaj Górecki|H. M. Górecki]]:</small><br />
<small>''Already It Is Dusk'', music for string quartet (String quartet no. 1) op. 62</small><br />
<small>''Quasi una Fantasia'' (String quartet no. 2) op. 64</small><br />
<small>''...songs are sung'' (String quartet no. 3) op.67</small><br />
 || <small>[[EMI Music]] Poland</small><br />
<small>EMI 50999 2 36313 2 8</small><br />
|-
| <small>2008</small> || <small>ANDRZEJ KRZANOWSKI IN MEMORIAM</small><br />
<small>[[Silesian String Quartet]]</small><br />
<small>Agata Zubel - [[soprano]]</small><br />
 || <br />
<small>[[Andrzej Krzanowski|A. Krzanowski]] - Audition no. 6 for soprano and string quartet</small><br />
<small>W. Widłak - ”Sotto voce”, Five songs for mezzosoprano and string quartet</small><br />
<small>R. Augustyn - Dedication for soprano and string quartet</small><br />
<small>[[Andrzej Krzanowski|A. Krzanowski]] - String quartet no. 1, version B</small><br />
<small>A. Lasoń - Relief for Andrzej</small><br />
 || <small>Musica Pro Bono Foundation</small><br />
<small>FMPB CD003</small><br />
|-
| <small>2008</small> || <small>ALEKSANDER TANSMAN - OD TRIA DO OKTETU <br />
VOL. 1</small><br />
<small>Chamber works</small><br />
<small>[[Silesian String Quartet]]</small><br />
<small>Beata Bilińska - [[piano]]</small><br />
<small>Piotr Szymyślik - [[clarinet]]</small><br />
 || <small>[[Aleksander Tansman|A. Tansman]]:</small><br />
<small>Suite divertissement for piano quartet</small><br />
<small>Musica a cinque for piano and string quartet</small><br />
<small>Musique a six for clarinet, piano and strine quartet</small><br />
<small>Musique for clarinet and string quartet</small><br />
 || <small>[http://www.tansman.lodz.pl/about_assoc.php?lang=pl Stowarzyszenie Promocji Kultury im. A. Tansmana]</small><br />
<small>CD257</small><br />
|-
| <small>2007</small> || <small>ALEKSANDER LASOŃ</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <small>A. Lasoń:</small><br />
<small>String quartet no. 1</small><br />
<small>String quartet no. 3</small><br />
<small>String quartet no. 7</small> <br />
|| <small>Musica Pro Bono Foundation</small><br />
<small>FMPB CD001</small><br />
|-
| <small>2006</small> || <small>PAWEŁ SZYMAŃSKI<br />
CHAMBER WORKS</small><br />
<small>[[Silesian String Quartet]]</small><br />
<small>Krzysztof Jaguszewski - [[vibraphone]]</small><br />
<small>Roman Widaszek - [[clarinet]]</small><br />
 || <small>[[Paweł Szymański|P. Szymański]]:</small><br />
<small>Five pieces for string quartet</small><br />
<small>''Compartment 2, Car 7''  for string trio and vibraphone</small><br />
<small>Two pieces for string quartet</small><br />
<small>''Recalling a Serenade'' for clarinet and string quartet</small><br />
<small>''A Photo from the Birthday Party'' (The Silesian String Quartet with a Shadow of [[Bartók]])</small><br />
 || <small>[[EMI Music]] Poland</small><br />
<small>EMI 0946 3 84393 2 5</small><br />
|-
| <small>2006</small> || <small>ALEKSANDER LASOŃ</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <small>Aleksander Lasoń:</small><br />
<small>String quartet no. 5 ''Five and a half of a string quartet''</small><br />
<small>String quartet no. 6</small><br />
<small>20 for 4</small><br />
 || <small>Polskie Radio Katowice</small><br />
<small>PRK CD076</small><br />
|-
| <small>2005</small> || <small>REPUBLIQUE</small><br />
<small>[[Silesian String Quartet]]

<br />
CD of the Year 2005 - Award of the "Hi-fi Muzyka" Magazine<br />
CD nominated for the [[Fryderyk]] Award</small><br />
 || <small>songs composed by [[Grzegorz Ciechowski]],<br />
arranged by Stefan Sendecki</small><br />
 || <small>[[EMI Music]] Poland</small><br />
<small>EMI 0946 3 46038 2 9</small><br />
|-
| <small>2005</small> || <small>WITOLD LUTOSŁAWSKI / WITOLD SZALONEK</small><br />
<small>[[Silesian String Quartet]]</small><br />
<small>Michał Górczyński - [[bass clarinet]]</small><br />
 || <small>[[Witold Lutosławski|W. Lutosławski]]:</small><br />
<small>String quartet</small><br />
<small>W. Szalonek:</small><br />
<small>''Inside? - Outside?''  for bass clarinet and string quartet</small><br />
<small>''Chaconne-Fantaisie'' for violin solo</small><br />
 || <small>Polskie Radio Katowice</small><br />
<small>PRK CD069</small><br />
|-
| <small>2004</small> || <small>ERNEST CHAUSSON</small><br />
<small>[[Bruno Canino]] - [[piano]]</small><br />
<small>Piotr Pławner - [[violin]]</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <small>[[Ernest Chausson|E. Chausson]]:</small><br />
<small>Concerto in D major op. 21 for piano, violin and string quartet</small><br />
<small>String quartet in c minor op. 35</small><br />
 || <small>Polskie Radio Katowice</small><br />
<small>PRK CD062</small><br />
|-
| <small>2004</small> || <small>ANDRZEJ KRZANOWSKI / ANDRZEJ PANUFNIK</small><br />
<small>[[Silesian String Quartet]]</small><br /><small>Dominik Połoński - [[cello]]</small><br />
<small>Elżbieta Mrożek - [[viola]]</small><br />
 || <small>[[Andrzej Krzanowski|A. Krzanowski]]:</small><br />
<small>Relief no. 5 for cello solo</small><br />
<small>''Reminiscenza'' for string quartet, version B</small><br />
<small>String quartet</small><br />
<small>[[Andrzej Panufnik|A. Panufnik]]:</small><br />
<small>''Trans Of Thought'' for [[string sextet]]</small><br />
<small>''Song To The Virgin Mary'' for string sextet</small><br />
 || <small>Polskie Radio Katowice</small><br />
<small>PRK CD065</small><br />
|-
| <small>2003</small> || <small>ANDRZEJ PANUFNIK / ANDRZEJ KRZANOWSKI</small><br />
<small>[[Silesian String Quartet]]

CD nominated for the [[Fryderyk]] Award</small><br />
|| <small>[[Andrzej Panufnik|A. Panufnik]]:</small><br />
<small>String quartet no. 1</small><br />
<small>String quartet no. 2</small><br />
<small>String quartet no. 3</small><br />
<small>[[Andrzej Krzanowski|A. Krzanowski]]:</small><br />
<small>Relief IX ”Scottish” for string quartet and tape</small><br />
|| <small>Polskie Radio Katowice</small><br />
<small>PRK CD056</small><br />
|-
| <small>2003</small> || <small>WITOLD SZALONEK</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <small>Witold Szalonek:</small><br />
<small>''1+1+1+1'' per 1-4 strumenti ad arco</small><br />
<small>/version for [[violin]] and [[cello]]/</small><br />
<small>''Symphony of rituals'' for string quartet</small><br />
<small>''1+1+1+1'' per 1-4 strumenti ad arco</small><br />
<small>/version for string quartet/</small><br />
 || <small>Polskie Radio Katowice</small><br />
<small>PRK CD058</small><br />
|-
| <small>2003</small> || <small>MAX E. KELLER</small><br />
<small>[[Silesian String Quartet]]</small><br />
|| <small> [[Max E. Keller]]:<br />
2 String Quartet (1995)</small><br />
|| <small>Musiques Suisses/Grammont Portrait<br />
MGB CTS-M 84</small>
|-
| <small>2003</small> || <small>DANCES ...AND AFTER DANCES<br />
[[Silesian String Quartet]]

CD nominated for the [[Fryderyk]] Award</small><br />
|| <small>Dances:<br />
[[Joseph Haydn]] - String Quartet in D - minor Op. 103<br />
[[Alexander Glazunov|Alexander Glasunow]] - Valse from "Novelettes" op.15<br />
Anatoly Liadov - Mazurka<br />
[[Alexander Glazunov]] - Courante<br />
[[Anton Webern]] - Rondo<br />
[[Dmitri Shostakovich]] - Polka: Allegretto (1931)<br />
[[John Cage]] - Quodlibet<br />
[[Igor Stravinsky]] - Dance from "Trois Pieces"<br />
[[Astor Piazzolla]] - Four, for Tango<br />
...and after dances:<br />
[[Giacomo Puccini]] - Chrysanthemen<br />
[[Ludwig van Beethoven]] - Prelude and Fugue in C Major<br />
[[Nikolai Rimski-Korsakov]] - Fugue "Im Kloster"<br />
[[Hugo Wolf]] - Italian Serenade in G Major WW XV/3<br />
Anatoly Liadov - Fugue<br />
[[Franz Schubert]] - Quartet Movement in C Minor<br />
[[Krzysztof Penderecki]] - String Quartet No.2</small><br />
|| <small>C&P POLSKIE RADIO KATOWICE S.A<br />
PRK CD 054</small>
|-
| <small>2003</small> || <small>SALON BIELAJEWA</small><br />
<small>[[Silesian String Quartet]]</small><br />
 || <br />
<small>[[Alexander Glazunov|A. Glazunov]] - Waltz from "Novelettes" op.15</small><br />
<small>[[Alexander Glazunov|A. Glazunov]] - Preludio e Fuga</small><br />
<small>N. Arcybushev - Serenade</small><br />
<small>J. Wihtol - Menuet</small><br />
<small>M. d'Osten-Sacken - Berceuse</small><br />
<small>A. Liadov - Mazurka</small><br />
<small>N. Sokolov - Scherzo</small><br />
<small>A. Liadov - Sarabande</small><br />
<small>[[A. Borodin]] - Scherzo</small><br />
<small>[[Alexander Glazunov|A. Glazunov]] - Courante</small><br />
<small>A. Liadov - Fugue</small><br />
<small>N. Sokołow, [[Alexander Glazunov|A. Głazunow]], A. Liadov - Polka</small><br />
|| <small>[[Polskie Radio Katowice]]</small><br />
<small>PRK CD048</small><br />
|-
| <small>2001</small> || <small>JOHANNES BRAHMS: <br />
PIANO & VIOLIN SONATAS<br />
Szymon Krzeszowiec - [[violin]]<br />
Wojciech Świtała - [[piano]]</small><br />
||<small>[[Johannes Brahms]]:<br />
Sonata in G major op. 78<br />
Sonata in A major op. 100<br />
Sonata in d minor op. 108</small><br />
|| <small>[[Sony Classical Records|Sony Music Poland]]<br />
SK 87712<small>
|}

== References ==
{{reflist}}

{{DEFAULTSORT:Krzeszowiec, Szymon}}
[[Category:Polish classical violinists]]
[[Category:Male violinists]]
[[Category:21st-century classical violinists]]
[[Category:Living people]]
[[Category:1974 births]]