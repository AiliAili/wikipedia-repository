{{Infobox scientist
| name     =  Thomas P. Loughran, Jr., MD
| image    =  Thomas_P._Loughran,_Jr.jpg
| size     = 300x300px
| alt      =  Thomas P. Loughran, Jr., MD
| caption     =  Thomas P. Loughran, Jr., MD
| occupation  = [[Physician]]
| field      = {{plainlist|
* [[Cancer]]
* [[Medicine]]}}
| workplaces  = {{plainlist|
*[[University of Virginia School of Medicine]]
*[[Penn State Hershey Cancer Institute]]
*[[University of South Florida]]
*[[SUNY Health Science Center]]
*[[Fred Hutchinson Cancer Research Center]]}}
| alma_mater  = [[Hahnemann Medical School]]
| birth_date  =         <!--{{birth date |YYYY|MM|DD}}-->
| birth_place = 
| death_date  =         <!--{{death date and age |YYYY|MM|DD |YYYY|MM|DD}} (death date then birth date)-->
| death_place = 
| death_cause = 
| residence   = 
| citizenship = 
| nationality = American
| patrons     = 
| education   = 
| known_for   = 
| influences  = 
| influenced  = 
| awards      = 
| spouse      =         <!--(or | spouses = )-->
| partner     =         <!--(or | partners = )-->
| children    = 
}}

'''Thomas P. Loughran, Jr., MD''' is an American physician-scientist who specializes in [[cancer]] research and treatment. He became director of the [[University of Virginia Cancer Center]], F. Palmer Weber-Smithfield Foods Professor of Oncology Research and Professor of Medicine at the [[University of Virginia]] on August 15, 2013.<ref name="University of Virginia Health System Newsroom">{{cite web |url=http://uvahealth.com/about/news-room/archives/thomas-p.-loughran-jr.-md-appointed-director-of-uva-cancer-center/ |title=Thomas P. Loughran Jr., MD, appointed director of UVA Cancer Center |work=University of Virginia Health System Official Website |accessdate=2013-11-21}}</ref>
Between 2003 and 2013, Loughran served as the founding director of the [[Penn State Hershey Cancer Institute]] and professor of medicine at the [[Penn State College of Medicine]]. His previous appointments included program leader of hematologic malignancies at the [[H. Lee Moffitt Cancer Center & Research Institute]] at the University of South Florida, associate director of the Bone Marrow Transplant Program at [[State University of New York Upstate Medical University|SUNY Health Science Center]] and chief of hematology at the Syracuse Veteran’s Affairs Medical Center in Syracuse, N.Y.<ref name="Ursinus">{{cite web |url=http://issuu.com/ursinus/docs/magazinefall2013/21?e=1966424/5171267/ |title=Observe, lead, discover |work=Ursinus Magazine Fall 2013 |accessdate=2013-11-21}}</ref>
Loughran completed his fellowship in medical oncology in 1985 at the [[Fred Hutchinson Cancer Research Center]] in [[Seattle]], [[Washington (state)|Washington]] under direction of [[E. Donnall Thomas|Nobel Laureate E. Donnall Thomas]]. He remained on faculty there for seven years. Loughran earned his medical degree from [[Hahnemann Medical School]] in [[Philadelphia]] in 1979.<ref name="University of Virginia Health System Newsroom"/>

== LGL leukemia research ==

Loughran is internationally recognized for cancer research and treatment. His primary research interest is [[Large granular lymphocytic leukemia|large granular lymphocyte leukemia]] (LGL), a hematologic malignancy he discovered in the mid-1980s.<ref name="PennLive">{{cite web |url=http://www.pennlive.com/bodyandmind/index.ssf/2012/07/derry_twp_doctor_treats_rare_l.html |title=Hershey doctor's pioneering work with rare leukemia offers hope to patients worldwide |work=The Patriot-News|accessdate=2013-11-25}}</ref> He is considered the international expert on this form of leukemia.<ref>https://cancer.uvahealth.com/LGLLeBookNew.pdf</ref>
He has received continuous federal grant support for the past 26 years and currently is the principal investigator on two R01 grants and a P01 grant from the [[National Cancer Institute]], as well as a translational research grant from the [[Leukemia & Lymphoma Society]].
Loughran has published numerous articles in high impact peer-reviewed journals including [[The New England Journal of Medicine]],<ref name="New England Journal of Medicine">{{cite journal |title=Somatic STAT3 mutations in large granular lymphocytic leukemia. |work=New England Journal of Medicine |pmid=22591296 | doi=10.1056/NEJMoa1114885 |volume=366 |pmc=3693860 |author=Koskela HL, Eldfors S, Ellonen P, van Adrichem AJ, Kuusanmäki H, Andersson EI, Lagström S, Clemente MJ, Olson T, Jalkanen SE, Majumder MM, Almusa H, Edgren H, Lepistö M, Mattila P, Guinta K, Koistinen P, Kuittinen T, Penttinen K, Parsons A, Knowles J, Saarela J, Wennerberg K, Kallioniemi O, Porkka K, Loughran TP Jr, Heckman CA, Maciejewski JP, Mustjoki S |pages=1905-13}}</ref> [[Annals of Internal Medicine]],<ref name="Annals of Internal Medicine">{{cite journal |title=Polyarthritis and neutropenia associated with circulating large granular lymphocytes. |work=Annals of Internal Medicine |pmid=4026084 | volume=103 |author=Wallis WJ, Loughran TP Jr, Kadin ME, Clark EA, Starkebaum GA |pages=357–62 |doi=10.7326/0003-4819-103-3-357}}</ref> [[The Lancet]],<ref name="Lancet">{{cite journal |title=Serum reactivity to human T-cell leukaemia/lymphoma virus type I proteins in patients with large granular lymphocytic leukaemia. |work=Lancet |pmid=2881134 | volume=1 |author=Starkebaum G, Loughran TP Jr, Kalyanaraman VS, Kadin ME, Kidd PG, Singer JW, Ruscetti FW |pages=596-9}}</ref> [[Journal of Clinical Investigation]],<ref name="Journal of Clinical Investigation">{{cite journal |title=NKp46 identifies an NKT cell subset susceptible to leukemic transformation in mouse and human. |work=Journal of Clinical Investigation |pmid=21364281 | doi=10.1172/JCI43242 |volume=121 |pmc=3069763 |author=Yu J, Mitsui T, Wei M, Mao H, Butchar JP, Shah MV, Zhang J, Mishra A, Alvarez-Breckenridge C, Liu X, Liu S, Yokohama A, Trotta R, Marcucci G Jr, Benson DM, Loughran TP Jr, Tridandapani S, Caligiuri MA |pages=1456-70}}</ref> [[Journal of Clinical Oncology]],<ref name="Journal of Clinical Oncology">{{cite journal |title=Phase I combination trial of lenalidomide and azacitidine in patients with higher-risk myelodysplastic syndromes. |work=Journal of Clinical Oncology |pmid=20354132 | doi=10.1200/JCO.2009.26.0745 |volume=28 |pmc=2860439 |author=Sekeres MA, List AF, Cuthbertson D, Paquette R, Ganetzky R, Latham D, Paulic K, Afable M, Saba HI, Loughran TP Jr, Maciejewski JP |pages=2253-8}}</ref> and [[Blood (journal)|Blood]].<ref name="Blood">{{cite journal |title=STAT3 mutations indicate the presence of subclinical T-cell clones in a subset of aplastic anemia and myelodysplastic syndrome patients. |work=Blood. |pmid=23926297 | doi=10.1182/blood-2013-04-494930 |volume=122 |pmc=3790512 |author=Jerez A, Clemente MJ, Makishima H, Rajala H, Gómez-Seguí I, Olson T, McGraw K, Przychodzen B, Kulasekararaj A, Afable M, Husseinzadeh HD, Hosono N, LeBlanc F, Lagström S, Zhang D, Ellonen P, Tichelli A, Nissen C, Lichtin AE, Wodnar-Filipowicz A, Mufti GJ, List AF, Mustjoki S, Loughran TP Jr, Maciejewski JP |pages=2453-9}}</ref>

Loughran holds [[American Board of Medical Specialties|American Board certifications]] in [[internal medicine]] and [[medical oncology]]. In his clinical practice, he treats patients with bone marrow disorders and leukemia.<ref name="University of Virginia Find a Doctor">{{cite web |url=http://uvahealth.com/doctors/physicians/thomas-loughran |title=Thomas Loughran, MD Cancer Center|work=University of Virginia Health System Official Website |accessdate=2013-11-25}}</ref>

== References ==

{{reflist}}

== External links ==
* [http://uvahealth.com/doctors/physicians/thomas-loughran/ UVA Health System Doctor Profile]

{{DEFAULTSORT:Loughran, Thomas P., Jr.}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American oncologists]]
[[Category:Cancer researchers]]
[[Category:University of Virginia faculty]]
[[Category:Drexel University alumni]]