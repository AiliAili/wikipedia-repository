{{Infobox journal
| title = Journal for Early Modern Cultural Studies
| cover = [[File:Journal for Early Modern Cultural Studies.gif]]
| editor = Thomas DiPiero, Devoney Looser, Bruce Boehrer, Dan Vitkus 
| discipline = [[Cultural studies]]
| abbreviation = J. Early Mod. Cult. Stud.
| publisher = [[University of Pennsylvania Press]]
| country = United States
| frequency = Quarterly
| history = 2001–present
| openaccess =
| impact = 
| impact-year = 
| website = http://jemcs.pennpress.org/
| link1 = http://muse.jhu.edu/journals/journal_for_early_modern_cultural_studies/
| link1-name = Online access on [[Project MUSE]]
| ISSN = 1531-0485
| eISSN = 1553-3786
| JSTOR = 15310485
| OCLC = 47346092
| LCCN = 00213932
}}
The '''''Journal for Early Modern Cultural Studies''''' is a quarterly [[peer-reviewed]] [[academic journal]] and the official publication of the Group for Early Modern Cultural Studies.<ref>{{cite web |url=http://www.gemcs.org/WelcometoGEMCS.php |title=Group for Early Modern Cultural Studies}}</ref> It covers the cultural history of the period from the late fifteenth to the late nineteenth centuries. The journal was established in 2001 and has been published by the [[University of Pennsylvania Press]] since 2011. The journal was published biannually until 2012, when it became a quarterly publication. The [[editor-in-chief]] is [[Bruce Boehrer]].

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[MLA International Bibliography]].<ref>{{cite web|url=http://www.mla.org/bib_periodicals|title=MLA Directory of Periodicals}}</ref>

== References ==
{{reflist}}

== External links ==

* {{Official website|http://jemcs.pennpress.org/}}

[[Category:History journals]]
[[Category:Cultural journals]]
[[Category:Publications established in 2001]]
[[Category:Quarterly journals]]
[[Category:University of Pennsylvania Press academic journals]]


{{cultural-studies-stub}}
{{humanities-journal-stub}}