{{Infobox journal
| title = Irish Journal of Medical Science
| cover = [[File:IJMScover.jpg]]
| formernames = Dublin Journal of Medical & Chemical Science, Dublin Journal of Medical Science, Dublin Quarterly Journal of Medical Science
| discipline = [[Medicine]]
| editor = William P Tormey
| publisher = [[Springer Science+Business Media]] on behalf of the [[Royal Academy of Medicine in Ireland]]
| country = Ireland
| frequency = Quarterly
| history = 1832–present
| impact = 1.158
| impact-year = 2015
| website = http://www.springer.com/medicine/internal/journal/11845
| link1 = http://www.springerlink.com/openurl.asp?genre=issue&issn=0021-1265&issue=current
| link1-name = online access
| ISSN = 0021-1265
| eISSN = 1863-4362
| CODEN = IJMSA
| OCLC = 1588106
}}
The '''''Irish Journal of Medical Science''''' is a quarterly [[peer-reviewed]] [[medical journal]] that was established in 1832 by [[Robert Kane (chemist)|Robert Kane]] as the ''Dublin Journal of Medical & Chemical Science''. Besides Kane, it had distinguished [[editor-in-chief|editors]] like [[Robert James Graves]] and [[William Wilde]].<ref>Greta Jones, Elizabeth Malcolm: Medicine, disease and the state in Ireland, 1650-1940. Cork. Cork University Press. 1999 p. 97</ref> It is the official organ of the [[Royal Academy of Medicine in Ireland]] and published by [[Springer Science+Business Media]].

== History ==
[[File:Dublin Journal of Med Science Jan 1839.jpg|left|thumb|''Dublin Journal of Medical Science'', January 1839.]]
The journal was established in 1832 as the ''Dublin Journal of Medical & Chemical Science''.<ref>{{cite book|last=Boylan|first= Henry |year=1998|title=A Dictionary of Irish Biography, 3rd Edition|page=153|location=Dublin|publisher= Gill and MacMillan|isbn= 0-7171-2945-4}}</ref> It was then sequentially titled: 
*''Dublin Journal of Medical Science'' (until 1845) 
*''Dublin Quarterly Journal of Medical Science'' (from 1846 to 1871) 
*''Dublin Journal of Medical Science'' (until 1925)
In 1925 it obtained its current title and volume numbering was restarted at 1.

William Wilde became editor in 1845. Contributors included Dublin physicians [[Abraham Colles]] (1773–1840), [[William Stokes (physician)|William Stokes]] (1763–1845), [[Sir Philip Crampton]] (1777–1858), [[Thomas Hawkesworth Ledwich|Thomas Ledwich]] (1823–1858), [[Arthur Jacob]] (1790–1874), [[Robert Adams (physician)|Robert Adams]] (1791–1875), Stephen Myles MacSwiney (died 1890), [[Charles Cameron (physician)|Sir Charles Cameron]] (1830–1921) and [[Ephraim MacDowel Cosgrave]] (1847–1925).

[[James Little (physician)|James Little]] (1837–1916) was editor from 1869 to 1875; during his tenure, the journal changed from a quarterly to a monthly publication.<ref>{{cite web|title=The Dublin Journal of Medical and Chemical Science Catalogue |url=http://irserver.ucd.ie/dspace/bitstream/10197/2487/3/ResearchPaperDublinJournalMedicalChemicalScience.pdf|publisher=University College Dublin Library |accessdate=20 May 2011|author1=Mullen, John |author2=Wheelock, Harriet |page=4|format=PDF|year=2010}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[InfoTrac|Academic OneFile]], [[Chemical Abstracts Service]], [[CSA (database company)|CSA]], [[Current Contents]]/Clinical Medicine, [[EMBASE]], Health Reference Center Academic, [[IBIDS]], [[International Nuclear Information System|INIS Atomindex]], [[PubMed]]/[[MEDLINE]], [[Science Citation Index|Science Citation Index Expanded]], [[Scopus]], and [[Serials Solutions|Summon by Serial Solutions]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.573.<ref name=WoS>{{cite book |year=2014 |chapter=Irish Journal of Medical Science |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==See also==
* [[Medical Press and Circular|Dublin Medical Press]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.springer.com/medicine/internal/journal/11845}}
* [https://digital.ucd.ie/view/ivrla:29851 Index of the Dublin Journal of Medical Science 1832–1949. A UCD Digital Library Collection.]
* {{Cite book |title=Dublin Journal of Medical Science |date= |year=1843 |url=https://books.google.com/books?id=ynUBAAAAYAAJ&printsec=titlepage |publisher=[[Google Books]] |accessdate = 16 July 2009}}

{{DEFAULTSORT:Irish Journal of Medical Science}}
[[Category:Quarterly journals]]
[[Category:Publications established in 1832]]
[[Category:General medical journals]]
[[Category:English-language journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:1832 establishments in Ireland]]