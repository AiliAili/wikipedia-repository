An '''acknowledgment index''' is a method for [[Bibliographic index|indexing]] and analyzing acknowledgments in the [[scientific literature]] and, thus, quantifies the impact of [[acknowledgment (creative arts)|acknowledgments]]. Typically, a scholarly article has a section where the authors acknowledge entities such as funding, technical staff, colleagues, etc. that have contributed materials or knowledge or have influenced or inspired their work. Like a [[citation index]] it measures influences on scientific work, but in a different sense; it measures institutional and economic influences as well as informal influences of individual people, ideas, and artifacts.
Unlike the [[impact factor]], it does not produce a single overall metric, but analyses the components separately. However, the total number of acknowledgments to an acknowledged entity can be measured and so can the number of citations to the papers in which the acknowledgment appears. The ratio of this total number of citations to the total number of papers in which the acknowledge entity appears can be construed as the impact of that acknowledged entity.<ref>{{cite conference |doi=10.1145/1088622.1088627 |title=Automatic acknowledgement indexing: expanding the semantics of contribution in the CiteSeer digital library |book-title=Proceedings of the 3rd international conference on Knowledge capture |conference=K-CAP '05 |pages=19–26 |year=2005 |last1=Councill |first1=Isaac G. |last2=Giles |first2=C. Lee |authorlink2=Lee Giles |last3=Han |first3=Hui |last4=Manavoglu |first4=Eren |isbn=1-59593-163-5 |citeseerx=10.1.1.59.1661}}</ref><ref>{{cite journal |doi=10.1073/pnas.0407743101 |title=Who gets acknowledged: Measuring scientific contributions through automatic acknowledgment indexing |journal=[[Proceedings of the National Academy of Sciences of the United States of America|Proc. Natl. Acad. Sci. U.S.A.]] |volume=101 |issue=51 |pages=17599–17604 |date=December 15, 2004 |last1=Giles |first1=C. L. |authorlink1=Lee Giles |last2=Councill |first2=I. G. |bibcode=2004PNAS..10117599G |url=http://clgiles.ist.psu.edu/papers/PNAS-2004-Acknowledgements.pdf}}</ref>

The first automated acknowledgment indexing was created in the search engine and digital library, [[CiteSeer]].<ref>{{cite web |url=http://csxstatic.ist.psu.edu/about |title=About CiteSeerX}}</ref> However, that feature is no longer supported. A new acknowledgement extraction and indexing system for acknowledgement research is now available—AckSeer.<ref>{{cite web |url=http://ackseer.ist.psu.edu/about |title=About AckSeer}}</ref>

==See also==
*[[Citation impact]]
*[[CiteSeerX]]

==References==
{{Reflist}}

==External links==
* [https://web.archive.org/web/20120329020908/http://ackseer.ist.psu.edu:80/? AckSeer]

[[Category:Bibliometrics]]
[[Category:Reputation management]]


{{library-stub}}