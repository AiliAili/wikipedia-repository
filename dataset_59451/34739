{{Use dmy dates|date=May 2011}}
{{good article}}
{{Infobox military unit
|unit_name= 188th Infantry Brigade
|image=[[File:188InfantryBdeSSI.jpg|199px]]
|caption=188th Infantry Brigade [[shoulder sleeve insignia]]
|dates=24 June 1921 – 7 February 1946<br>13 February 1947 – 1 May 1959<br>25 October 1997 – 16 October 1999<br>1 December 2006 – Present
|country= {{flagicon|USA}} United States of America
|allegiance=
|branch= [[United States Army Reserve|Army Reserve]]
|type= [[Infantry]]
|role= Training
|size= Brigade
|command_structure= Division East, [[First United States Army]]
|garrison= [[Fort Stewart, Georgia]]
|garrison_label=
|equipment=
|equipment_label=
|nickname=
|patron=
|motto=''Procinctus'' (Ready for Battle)<ref name="TIOH"/>
|colors=
|colors_label=
|march=
|mascot=
|battles=[[World War II]]
|anniversaries=
|decorations=
|battle_honours=
<!-- Commanders -->
|current_commander=[[Colonel]] Matthew H. Fath|current_commander_label=Commander
|ceremonial_chief=
|ceremonial_chief_label=
|colonel_of_the_regiment=
|colonel_of_the_regiment_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:188InfantryBdeDUI.jpg|100px]]
|identification_symbol_label=[[Distinctive unit insignia]]
|identification_symbol_2=
|identification_symbol_2_label=
}}

The '''188th Infantry Brigade''' is an [[infantry]] training [[brigade]] of the [[United States Army]] based at [[Fort Stewart]], [[Georgia (U.S. state)|Georgia]]. It is a subordinate unit of the [[First United States Army]], Division East.

Active from 1921 to 1942 as part of the [[94th Infantry Division (United States)|94th Infantry Division]], the brigade was transformed into a reconnaissance troop during and after [[World War II]], supporting the 94th Division as it fought in Europe. Activated again from 1997 to 1999 and again in 2006, the unit gained responsibility for conducting training for other reserve component army units with a variety of purposes. Today, the unit is responsible for training selected [[United States Army Reserve]] and [[Army National Guard|National Guard]] units East of the [[Mississippi River]].

==Organization==
The brigade is subordinate to [[First Army Division East]] of the [[First United States Army]], one of several regional training brigades spread throughout the United States.<ref name="diveast"/>

The brigade consists of seven primary elements. The brigade's [[Headquarters and Headquarters Company]], along with its [[Special Troops Battalion]], provides support and oversight to the other six battalions in the brigade.<ref name="bdehist"/> The 1st Battalion, 306th Regiment trains primarily infantry units, while the 2nd Battalion, 306th Regiment trains [[field artillery]] units.<ref name="bdehist"/> The 1st Battalion, 347th Regiment provides training for [[combat support]] units, while the 3rd Battalion, 345th Regiment oversees [[combat service support]] units. The 2nd Battalion, 349th Regiment provides support training for the other battalions and large-scale training operations.<ref name="bdehist"/> The 3rd Battalion, 395th Regiment (recently realigned from [[First Army Division West]]) trains primarily armor/cavalry units.

== History ==
The 188th Infantry Brigade was first constituted on 24 June 1921 in the [[organized reserves]] at [[Boston, Massachusetts]].<ref name="lineage">{{cite web|url=http://www.history.army.mil/html/forcestruc/lineages/branches/div/188infbde.htm |title=Lineage and Honors Information: 188th Infantry Brigade |publisher=[[United States Army Center of Military History]] |accessdate=10 July 2009}}</ref> It was organized with two infantry regiments, the [[376th Infantry Regiment (United States)|376th Infantry Regiment]] and the [[419th Infantry Regiment (United States)|419th Infantry Regiment]].<ref>McGrath, p. 174.</ref> It was, in turn, assigned to the [[94th Infantry Division (United States)|94th Infantry Division]].<ref name="lineage"/>  From 1921 to 1942 the division remained on the [[Massachusetts]] organized reserve rolls, though the unit did not assemble except for regular weekend training.<ref name="bdehist">{{cite web|url=http://www.stewart.army.mil/tenants/188INF/home.asp |title=188th Infantry Brigade Homepage: History |publisher=188th Infantry Brigade Public Affairs Office |accessdate=10 July 2009}}</ref> In 1925, the brigade relocated to [[Worcester, Massachusetts]] and in 1940 it moved to [[Springfield, Massachusetts]].<ref name="lineage"/>

=== 94th Reconnaissance Troop ===
The 94th Infantry Division was mobilized for deployment for [[World War II]] on 15 September 1942 at [[Fort Custer Training Center|Fort Custer]], [[Michigan]]. During this mobilization the 188th Infantry Brigade went through a series of reorganizations. The 376th Infantry Regiment continued as part of the 94th Infantry Division and the remainder of the 188th Infantry Brigade became the 94th Reconnaissance Troop, still assigned to the 94th Infantry Division.<ref name="bdehist"/>

The 94th Infantry Division, with the troop in tow, landed on [[Utah Beach]] on 8 September 1944, 94 days after [[D-Day]]. It and moved into [[Brittany]] to assume responsibility for containing some 60,000 German troops besieged in the ports of [[Lorient]] and [[Saint-Nazaire]]. The 94th inflicted over 2,700 casualties on the German forces and took 566 prisoners before being relieved on 1 January 1945.<ref name="Almanac564">''Almanac'', p. 564.</ref>

Moving west, the troop followed the division as it took positions in the [[Saar (river)|Saar]]-[[Moselle]] Triangle, facing the [[Siegfried Switch]] Line on 7 January 1945, and shifted to the offensive on 14 January, seizing Tettingen and Butzdorf that day. The following day, the [[Nennigberg]]-[[Wies]] area was wrested from the German army, but heavy counterattacks followed and Butzdorf, Berg, and most of [[Nennig]] changed hands several times before being finally secured. On 20 January, an unsuccessful battalion attack against Orscholz, eastern terminus of the switch position, cost the division most of two companies. In early February, the division, with troop in tow, took the woods of Campholz and later [[Perl, Saarland|Sinz]].<ref name="Almanac564"/>

[[File:94th Regional Readiness Command SSI.svg|thumb|Insignia of the 94th Infantry Division, which the brigade was assigned to during World War II when it was a reconnaissance troop.]]
On 19 February 1945, the division launched a full-scale attack, storming Munzigen Ridge, the backbone of the Saar-Moselle Triangle, and captured all of its objectives. Moving forward, the 94th Infantry Division, along with the [[10th Armored Division (United States)|10th Armored Division]], secured the area from Orscholz to the confluence of the Saar and Moselle Rivers by 21 February 1945. Then, launching an attack across the Saar, it established and expanded a [[bridgehead]]. By 2 March 1945, the division stretched over a {{convert|10|mi|km|sing=on}} front, from Hocker Hill on the Saar through [[Zerf]], and [[Lampaden]] to [[Ollmuth]]. A heavy German attack near Lampaden achieved penetrations, but the line was shortly restored, and on 13 March, spearheading the [[XX Corps (United States)|XX Corps]], the 94th broke out of the bridgehead and drove to the [[Rhine River]], reaching it on 21 March. [[Ludwigshafen]] was taken on 24 March, with assistance from elements the [[12th Armored Division (United States)|12th Armored Division]].<ref name="Almanac564"/>

The 94th Infantry Division then moved by railroad and motor to the vicinity of [[Krefeld, Germany]], assuming responsibility for containing the west side of the Ruhr pocket from positions along the Rhine on 3 April. With the reduction of the pocket in mid-April, the 94th Infantry Division was assigned military government duties, first in the Krefeld and later in the Düsseldorf areas. Soldiers of the troop participated in this assignment.<ref name="Almanac564"/>

The 94th Infantry Division participated in four World War II campaigns on mainland Europe. The 94th Reconnaissance Troop supported the division throughout its push through Europe.<ref name="bdehist"/> Upon the end of fighting in Europe, the 94th Infantry Division began conducting occupation duty in Europe until it returned to the US at the end of 1945. The Troop was demobilized at [[Camp Kilmer, New Jersey]] and deactivated on 7 February 1946.<ref name="lineage"/>

The troop was reactivated on 13 February 1947 in [[Boston, Massachusetts]].<ref name="lineage"/> However, the troop did not see any significant actions during its post war years, still a part of the 94th Infantry Division, which itself was never called on for service. The troop was moved to [[Cohasset, Massachusetts]] in 1953, remaining there for another six years as an inactive reserve unit until 1959, when it was again disbanded and inactivated.<ref name="lineage"/>

=== Training brigade ===
On 24 October 1997, the 188th Infantry Brigade was reactivated again in the [[Active duty]] force at [[Fort Stewart, Georgia]].<ref name="lineage"/> It was a reflagging of the 4th Brigade, [[87th Division (United States)|87th Division]].<ref>McGrath, p. 218.</ref>  The brigade was reactivated to provide training support to [[Reserve Component]] units in the region.<ref name="GSO">{{cite web|url=http://www.globalsecurity.org/military/agency/army/188in-bde.htm |title=GlobalSecurity.org: 188th Infantry Brigade |work=GlobalSecurity.org |accessdate=11 July 2009| archiveurl= https://web.archive.org/web/20090807170642/http://www.globalsecurity.org/military/agency/army/188in-bde.htm| archivedate= 7 August 2009 <!--DASHBot-->| deadurl= no}}</ref>

As of 1998 there were 248 soldiers assigned to the Brigade in five training support battalions and the Brigade Headquarters. At that time, the 188th Infantry Brigade supported the training of over 5,000 National Guard and Reserve Component soldiers in infantry, armor, cavalry, aviation, artillery, communications, medical, maintenance, and supply units.<ref name="GSO"/>

The Brigade was composed entirely of active duty senior noncommissioned officers and officers who conducted training assistance and valuations for a variety of combat arms, [[combat support]] and [[combat service support]] units. A new building was constructed on Fort Stewart, Georgia for the Brigade and was occupied in August 1997. The [[First United States Army]], the Brigade's higher headquarters, also purchased new furniture and computers for the Brigade's use.<ref name="GSO"/> In 1999, the brigade was again reflagged as the 4th Brigade, 87th Division.<ref name="lineage"/>

=== Present day ===
The brigade was reactivated on 1 October 2006 as part of another consolidation of US Army training commands, again at Fort Stewart.<ref name="lineage"/> The division fell under the command of Division East of the First United States Army.<ref name="diveast">{{cite web|url=http://1aeast.army.mil/188inbde/ |title=First Army Division East Homepage |publisher=First Army Public Affairs Office |accessdate=11 July 2009}}</ref> During the consolidation, the brigade was given a larger area of responsibility, supporting the training of over 18,000 National Guard and Reserve Component soldiers in infantry, armor, cavalry, aviation, artillery, communications, medical, maintenance, and supply units both at and around the Fort Stewart area. The brigade grew to over 600 soldiers assigned to 6 training support battalions and the Headquarters.<ref name="bdehist"/>

On 15 January 2008, the brigade received a [[shoulder sleeve insignia]] and [[distinctive unit insignia]]. Both of these allude to the brigade's service to the 94th Infantry Division both during peacetime and as the 94th Reconnaissance Troop during and after World War II.<ref name="TIOH">{{cite web|url=http://www.tioh.hqda.pentagon.mil/Inf/188InfantryBrigade.htm |title=The Institute of Heraldry: 188th Infantry Brigade |publisher=[[The Institute of Heraldry]] |accessdate=11 July 2009 |archiveurl = https://web.archive.org/web/20080804110111/http://www.tioh.hqda.pentagon.mil/Inf/188InfantryBrigade.htm |archivedate = 4 August 2008}}</ref>

== Honors ==

=== Unit decorations ===
The brigade has never received a unit decoration from the United States military.<ref name="lineage"/>

=== Campaign streamers ===
{| class=wikitable
|- bgcolor="#efefef"
! Conflict
! Streamer
! Year(s)
|-
|| [[World War II]]
|| [[Invasion of Normandy|Northern France Campaign]]
|| 1944
|-
|| World War II
|| [[Rhineland Campaign]]
|| 1944–1945
|-
|| World War II
|| [[Ardennes-Alsace Campaign]]
|| 1944–1945
|-
|| World War II
|| [[Central Europe Campaign]] 
|| 1945
|}

== References ==
{{reflist}}

== Sources ==
*{{cite book|first=John J. |last=McGrath |title= The Brigade: A History: Its Organization and Employment in the US Army |year=2004 |publisher=Combat Studies Institute Press |isbn=978-1-4404-4915-4}}
*{{cite book |title= Army Almanac: A Book of Facts Concerning the Army of the United States |year=1959 |publisher=United States Government Printing Office |asin=B0006D8NKK}}

==External links==
*[http://1aeast.army.mil/188inbde/organization.html 188th Infantry Brigade Homepage]
*[http://www.stewart.army.mil/units/home.asp?u=188IN 188th Infantry Brigade @ Team Stewart]
* [http://www.history.army.mil/html/forcestruc/lineages/branches/div/188infbde.htm Lineage & Honors for 188th Infantry Brigade] at the [[United States Army Center of Military History]]

[[Category:Infantry brigades of the United States Army|188]]