{{Infobox college coach
| name = David Grace
| image =
| alt = 
| caption = 
| sport = [[Basketball]]
| current_title = Assistant coach
| current_team = [[UCLA Bruins men's basketball|UCLA]]
| current_conference = [[Pac-12 Conference|Pac-12]]
| birth_place =  [[Aberdeen, Maryland]]
| coach_years1 = 1999–2004
| coach_team1 =  [[Trevor G. Browne High School|Trevor G. Browne HS]] (assistant)
| coach_years2 = 2004–2006
| coach_team2 = [[South Mountain High School|South Mountain HS]]
| coach_years3 = 2006–2007
| coach_team3 = [[Sacramento State Hornets men's basketball|Sacramento State]] (assistant) 
| coach_years4 = 2007-2008
| coach_team4 = [[San Francisco Dons men's basketball|San Francisco]] (assistant)
| coach_years5 = 2008–2013
| coach_team5 = [[Oregon State Beavers men's basketball|Oregon State]] (assistant)
| coach_years6 = 2013–present
| coach_team6 = [[UCLA Bruins men's basketball|UCLA]] (assistant)
}}

'''David Grace''' is a NCAA Division 1 men's basketball coach, former high school and AAU head coach, and is retired from the United States Air Force.

==Military career==

Grace grew up in [[Aberdeen, Maryland]], while his father Gerald worked long hours as a mechanic and his mother worked as a beautician.  He began military life after his mother remarried a serviceman and then moved to a series of bases over the years.  He joined the United States Air Force at age 18 and served for 20 years.  For 16 years as a fuel specialist and accountant, Grace traveled between Air Force bases in Turkey, Germany, Spain, Saudi Arabia, Georgia, and Virginia before settling down in the Phoenix, Arizona area for the last few years of his enlistment.  

Jeff Eisenberg of Yahoo Sports said the importance of discipline, teamwork, and following the chain of command became ingrained in Grace and three months in Saudi Arabia during Operation Desert Storm also taught him to appreciate the difference between the life-threatening pressure of a combat zone and the day-to-day challenges of a job.  <ref>https://sports.yahoo.com/blogs/ncaab-the-dagger/ucla-assistant-david-grace-took-unlikely-path-top-185751864--ncaab.html</ref>
 
==Early AAU coaching career==

Grace began his coaching career in 1997 while he was stationed at Langley AFB, Virginia. when he started coaching with the Boo Williams [[Amateur Athletic Union]] (AAU) program in Hampton, Virginia. <ref>https://arizonapreps.com/david-grace-rising-to-the-top</ref>  After leaving Langley, Grace was stationed at Luke AFB near Phoenix and coached two AAU teams and a high school team.  "I've been very proud of him seeing the progress he's made from the time he began here as an assistant coach at a high school to becoming an assistant coach at OSU (Oregon State University)," said Ronald Goodwyn, 56th Fighter Wing equal opportunity director. "He has taken the Air Force core values and tied them into his coaching to prepare the athletes to be successful even after their life in basketball." <ref>http://www.luke.af.mil/News/ArticleDisplay/tabid/3070/Article/358818/airman-pursues-dream-of-becoming-first-d-1-coach.aspx</ref>

==Coaching career with the Magic (AAU) ==

After re-locating to Luke AFB in Arizona, he began coaching AAU teams with the Compton Magic.  He also co-founded and coached the Arizona Magic AAU program in the Phoenix, Arizona area, as an affiliate of the Compton Magic, that made it to the Elite Eight (of 338 AAU teams) of the 2004 Reebok Big Time Tournament in Las Vegas, Nevada.  That team that featured Milwaukee Bucks player [[Jerryd Bayless]] (Arizona), Kaleo Kina (Naval Academy), Darren Jordan (Oral Roberts), Ty Morrison (Creighton/Grand Canyon), and former Stanford All-Conference selection and European League professional Lawrence Hill (Le Havre, France). <ref>{{Cite web|url=https://www.rivals.com/content.asp?CID=813221|title=Rivals.com - The Magic gains national attention|website=www.rivals.com|access-date=2016-04-08}}</ref> That team finished the AAU season ranked 21st in the nation and was the highest ranked team ever from Arizona up until that time. <ref> http://www.cstv.com/sports/m-baskbl/stories/071207aas.html</ref>

==High School coaching career and a state championship==

Grace began his high school coaching career at Trevor Browne High School in Phoenix while at Luke AFB, and then retired and moved into coaching full-time.<ref>{{Cite web|url=http://www.luke.af.mil/News/ArticleDisplay/tabid/3070/Article/358818/airman-pursues-dream-of-becoming-first-d-1-coach.aspx|title=Airman pursues dream of becoming first D-1 coach|website=Luke Air Force Base|access-date=2016-04-06}}</ref>As an Assistant Varsity Coach he helped lead Trevor Browne to a State Final Four and the team finished ranked in the Top 40 in the nation by the Sporting News. <ref>http://www.cstv.com/sports/m-baskbl/stories/071207aas.html</ref>  He then became head coach at South Mountain High School in Phoenix, Arizona, for the 2004–05 season and won the 2005-06 Division 5A-2 State Championship with a win/loss record of 29–4, and was the Arizona Varsity and Arizona Informant Coach of the Year.<ref>{{Cite web|url=http://archive.azcentral.com/sports/preps/articles/20120131oregon-state-assistant-basketball-coach-david-grace.html|title=What's up with former South Mountain boys basketball coach David Grace|website=azcentral.com|access-date=2016-04-06}}</ref> <ref>{{Cite web|url=http://www.uclabruins.com/ViewArticle.dbml?ATCLID=207913802 |title=David Grace Biography |website=uclabruins.com |access-date=2016-04-16 |deadurl=yes |archiveurl=https://web.archive.org/web/20160602085913/http://www.uclabruins.com/ViewArticle.dbml?ATCLID=207913802 |archivedate=2016-06-02 |df= }}</ref>

==Early NCAA DI career==
Grace started his college coaching career at [[California State University, Sacramento]] in 2006 as an assistant coach after receiving a recommendation from Coach [[Lute Olson]] at the University of Arizona to Head Coach [[Jerome Jenkins]].<ref>{{Cite web|url=http://www.basketballinsiders.com/uclas-david-grace-ready-for-the-next-step/|title=UCLA’s David Grace Ready for the Next Step {{!}} Basketball Insiders {{!}} NBA Rumors And Basketball News|website=www.basketballinsiders.com|access-date=2016-04-06}}</ref>

After one year there he then became an assistant coach at the University of San Francisco in 2007-08 under Head Coach [[Jessie Evans (basketball)|Jessie Evans]] who had been an assistant coach under Lute Olson on his [[1996–97 Arizona Wildcats men's basketball team|1996-7 Arizona Wildcat's Men's Basketball]] National Championship team.<ref>{{Cite web|url=http://www.csnnw.com/article/osu-assistant-grace-leaves-ucla|title=OSU Assistant Grace Leaves for UCLA|website=CSN Northwest|access-date=2016-04-06}}</ref>

Grace was hired as an assistant coach of the [[Oregon State Beavers men's basketball]] team in 2009 by Head Coach [[Craig Robinson (basketball)|Craig Robinson]], the older brother of U.S. First Lady Michelle Obama, and served there in that role through the 2012–13 season.  His first major Beaver recruit was All-Conference Team selection Roberto Nelson, who became the 2012-13 Pac-12 Conference scoring champion (20.7 points per game), and is currently a European pro in Italy (Trieste).<ref>{{Cite web|url=http://www.sports-reference.com/cbb/players/roberto-nelson-1.html#all_leaderboard|title=Roberto Nelson|website=College Basketball at Sports-Reference.com|access-date=2016-04-08}}</ref>

==Current coaching position at UCLA==
Grace has completed 9 years of coaching in the Pac-12 Conference where he is one of the longest tenured coaches.  As of 2017, he is assistant coach at [[UCLA Bruins men's basketball|UCLA]] under Head Coach [[Steve Alford]], his fourth season with the Bruins.<ref>{{Cite web|url=https://sports.yahoo.com/blogs/ncaab-the-dagger/ucla-assistant-david-grace-took-unlikely-path-top-185751864--ncaab.html|title=UCLA assistant David Grace took unlikely path to coaching after serving 20 years in the Air Force|website=Yahoo Sports|access-date=2016-04-06}}</ref> Three of Grace's first four seasons at UCLA have ended with Sweet 16 appearances in the NCAA Tournament. <ref>{{Cite web|url=http://ftw.usatoday.com/2015/03/ucla-gonzaga-sweet-16-march-madness-david-grace|title=UCLA assistant coach says Bruins 'will step up' for game against Gonzaga|website=For The Win|language=en-US|access-date=2016-04-06}}</ref>  He was rated the seventh best recruiter in the USA and the highest ranked on the West Coast by ESPN's coaches survey for 2016. <ref>{{Cite web|url=http://espn.go.com/blog/ncbrecruiting/on-the-trail/insider/post?id=15109|title=Duke's Jeff Capel tops CBB recruiter rankings|website=ESPN.com|access-date=2016-04-06}}</ref>  

"David has been tremendous," Alford said. "I've been fortunate in my 23 years to not just hire people who understand basketball but who are also really good people, and that's what David is. He's a great guy and he's a tireless worker. He's always on the phone calling or texting recruits."  <ref>https://sports.yahoo.com/blogs/ncaab-the-dagger/ucla-assistant-david-grace-took-unlikely-path-top-185751864--ncaab.html</ref>

The 2016–17 [[UCLA Bruins men's basketball]] team includes new recruits [[Lonzo Ball]] (the Naismith National Prep Player of the Year), [[T. J. Leaf]], and [[Ike Anigbogu]], and was rated the fifth best recruiting class in the nation by ESPN. <ref>{{Cite web|url=http://insider.espn.go.com/college-sports/basketball/recruiting/classrankings|title=2016 Basketball Class Rankings|website=ESPN.com|access-date=2016-04-08}}</ref>   Grace had recruited Ball prior to Alford arriving at UCLA.  "They went up the court about four times, then stopped," Grace recalled. "I walked over to his head coach and said [Lonzo] has a scholarship offer from Oregon State. I knew he was that good."   <ref>http://abcnews.go.com/Sports/lonzo-beginning-ball-ucla-pipeline/story?id=45345917</ref>  

Columnist Clay Fowler of the Orange County Register wrote that these Bruins tied a program record with 28 regular season wins and spent nearly half the season in the top five of the AP poll, rising as high as No. 2.   UCLA finished its season as the highest-scoring team in the country (89.8 points per game), the nation’s leader in field-goal percentage (52.2), assists per game (21.4) and assist-to-turnover ratio (1.91). The Bruins’ peaked with a road victory at Arizona on Feb. 25 that earned UCLA the distinction as the only team to ever win games in Kentucky and Arizona’s home arenas in the same season. <ref>http://www.ocregister.com/articles/team-747597-likely-country.html</ref>

Lonzo Ball and T.J. Leaf both declared for the NBA draft after the conclusion of the 2016-17 season and are expected to be drafted in the first round.  <ref>http://www.cbssports.com/college-basketball/news/t-j-leaf-becomes-second-ucla-freshman-to-declare-for-2017-nba-draft/</ref> Grace's other former recruits and players who have played in the NBA as of 2016 include [[Kevon Looney]], [[Jerryd Bayless]], [[Zach LaVine]], [[Eric Moreland]], [[Jared Cunningham]], [[Kyle Anderson (basketball)|Kyle Anderson]], [[Jordan Adams]], [[Norman Powell]], and [[Lou Amundson|Lou Amundsen]].

== Personal life ==
Grace grew up in [[Aberdeen, MD|Aberdeen, Md.]], and played basketball and baseball at [[Aberdeen High School (Maryland)]].  He is of Native American descent (Cherokee) and has worked with the Nike N7 program for several years to honor Native Americans.  He earned a bachelor’s of science degree in management (human resources) from Park University and degrees in logistics and social services from the Air Force. He and his wife, Crystal, have six grown children: Troy, Terrell, Tierra, David II, Aubrey, and Andre, as well as godson Mekai Roquemore. His father Gerald was a high school basketball referee and baseball umpire in Maryland for 35 years.  <ref>{{cite web|url=http://www.uclabruins.com/ViewArticle.dbml?ATCLID%3D207913802 |title=Archived copy |accessdate=2016-04-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20160602085913/http://www.uclabruins.com/ViewArticle.dbml?ATCLID=207913802 |archivedate=2016-06-02 |df= }}</ref>

==References==
{{reflist}}

{{DEFAULTSORT:Grace, David}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:High school basketball coaches in the United States]]
[[Category:People from Aberdeen, Maryland]]
[[Category:Oregon State Beavers men's basketball coaches]]
[[Category:Sacramento State Hornets men's basketball coaches]]
[[Category:San Francisco Dons men's basketball coaches]]
[[Category:UCLA Bruins men's basketball coaches]]