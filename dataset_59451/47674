{{refimprove|date=March 2013}}
{{Infobox person
|name          = Ernst J Eichwald, MD
|image         = Eichwald Ernst 2007.jpg
|caption       = Pioneer in tissue transplantation.
Founder and editor of  the journal, Transplantation Bulletin and its successor, Transplantation, for more than 30 years.
|birth_date    = {{birth date|1913|12|13}}
|birth_place   = Hanover, Germany
|death_date    = {{death date and age|2007|12|23|1913|12|13}}
|death_place   = [[Salt Lake City, Utah]], U.S.
|spouse        = Jeane (Gary) Richardson<br>Irmingard "Sissy" Eichwald
|children      = Jenny (born 1943)<br>Paul (born 1947)<br>John (born 1951)<br>Stepson: Andre (born 1962)<br>Stepson: Daniel (born 1965)
|nationality   = American
}}

'''Ernst J Eichwald''' (1913–2007) was a [[pathologist]] known for his pioneering work in tissue transplantation and research on genetic factors that influence the rejection of [[organ transplantation]]. Dr. Eichwald, while studying cancer in the 1940s, described the male-specific antigen and helped establish the foundations of transplantation [[immunology]]. He organized the first International Transplantation Conference, sponsored by the [[National Institutes of Health]] (NIH), in Harriman, N.Y., in 1953; founded and edited the journal, Transplantation Bulletin and its successor, Transplantation, for more than 30 years; and chaired the Transplantation Committee of the [[National Academy of Sciences]] from 1955 to 1967. His research played an important role in the development of successful protocols for organ transplantation in humans.

==Early years==
Ernst Julius Eichwald was born December 13, 1913, in Hannover, Germany. He earned his medical degrees from the University of Freiburg in 1938 and from the University of Utah School of Medicine in 1953. He served briefly in the German Wehrmacht in 1938, then moved to the United States the same year to begin his pathology training, first in Covington, Kentucky, followed by Dayton, Ohio, and then in Boston, at Children’s Hospital. He subsequently worked as an Assistant in Pathology at [[Harvard Medical School]], followed by service in the U.S. Army from 1944 to 1946 as Head of Laboratory Service at the 120th General Hospital, New Guinea, Manila, Philippines, and Kyoto, Japan. Upon completing his military service, he returned to Boston as an Assistant Pathologist at Children’s Hospital before accepting a faculty position in Pathology at the [[University of Utah]] in 1948.

==Montana years==
He was recruited to Montana Deaconess Hospital in [[Great Falls, Montana]] in 1953 under an arrangement that provided resources to support his research program. He established the Laboratory for Experimental Medicine in 1956 that evolved into the McLaughlin Research Institute in Great Falls, an independent biomedical research institute that continues to thrive. He was a Professor of Microbiology at Montana State University, Bozeman, and Chairman of the Montana Chapter of the [[American Cancer Society]].

==Later years==
In 1967 he returned to Salt Lake City as Professor of Pathology and Surgery, and chaired the Pathology Department until his partial retirement in 1979. In Utah, the focus of his research began to change from transplanted tumors to transplantation of normal tissues. He was a member of the University of Utah Institutional Review Board which approved an expansion of the eligibility category for the Jarvik-7 [[artificial heart]] in 1982. In 1984, on the occasion of his 70th birthday and 30th year as editor, the Transplantation Society dedicated the “Eichwald Festschrift”<ref name="google">{{cite book|title=Eichwald Festschrift: Dedicated to Ernst J. Eichwald, M.D., on the Occasion of His 70th Birthday and 30th Year as Editor of Transplantation|author1=Eichwald, E.J.|author2=Transplantation Society|date=1984|publisher=Williams & Wilkins Company|url=https://books.google.com/books?id=xLasGwAACAAJ|accessdate=2015-02-04}}</ref> in his honor. He continued his laboratory work until 2003. PubMed<ref name="nih">{{cite web|url=http://www.ncbi.nlm.nih.gov/pubmed?term=%22Eichwald%20EJ%22&#91;Author|title="Eichwald EJ" Author - PubMed - NCBI|publisher=ncbi.nlm.nih.gov|accessdate=2015-02-04}}</ref> lists 112 publications.

==Family and personal life==
Dr. Eichwald had numerous interests outside of science, especially music. His passion for playing chamber music on the viola rivaled his zeal for research. He co-founded the Great Falls Symphony while in Montana. Preceded in death by his first wife, Jeane (Gary) Richardson, he was survived by his wife, Irmingard "Sissy"; daughter, Jenny, (Seattle); sons, Paul (Missoula) and John (Atlanta); stepsons Andre and Daniel (Salt Lake City) and grandchildren Paul, Morgan, Heather, Ian and Ryan.

== References ==
{{Reflist}}

== External links ==
* [http://onlinelibrary.wiley.com/doi/10.1111/j.1600-6143.2008.02184.x/pdf American Journal of Transplantation 2008; 8: 1071–1072]
* [http://www.path.utah.edu/news/ernst-eichwald Department of Pathology at the University of Utah]
* [http://www.montana.edu/mri/history.html McLaughlin Research Institute - History]

{{DEFAULTSORT:Eichwald, Ernst J}}
[[Category:1913 births]]
[[Category:2007 deaths]]
[[Category:Hanoverian emigrants to the United States]]
[[Category:American pathologists]]
[[Category:United States Army personnel]]
[[Category:American military personnel of World War II]]