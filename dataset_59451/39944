<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout.-->
{{Infobox person
| name        = Anne-Jean Robert
| other_names = ''l'aîné, (the elder)''
| image       =
| caption     =
| birth_date  = 1758{{r|FAI}}
| birth_place = Paris
| death_date  = 1820{{r|FAI}}
| death_place = Paris
| occupation  = Mechanical engineer, Balloonist
| known_for   = Builder of first hydrogen balloon<br>Builder of first manned hydrogen balloon<br>Co-pilot of first balloon flight over 100km 1784
| spouse      =
| signature   =
}}
{{Infobox person
| name        = Nicolas-Louis Robert
| other_names = Marie-Noël Robert<br>''Robert le Jeune''<br>''cadet'' the younger{{r|FAI}}
| image       = Early flight 02562u (5).jpg
| caption     = The world's first manned hydrogen balloon flight. 1783
| birth_date  = 1760{{r|FAI}}
| birth_place = Paris
| death_date  = 1820{{r|FAI}}
| death_place =
| occupation  = Mechanical engineer, Balloonist
| known_for   = Builder of first hydrogen balloon<br>Builder of first manned hydrogen balloon<br>Co-pilot of first manned hydrogen balloon flight in 1783<br>Co-pilot of first balloon flight over 100km 1784{{r|Larousse}}
| spouse      =
| signature   =
}}
:''For the inventor of the Fourdrinier paper manufacturing machine, see [[Louis-Nicolas Robert]].<br> For the 17th Century French miniaturist and engraver, see [[Nicolas Robert]].''

'''Les Frères Robert''' were two French brothers. '''Anne-Jean Robert''' (1758–1820) and '''Nicolas-Louis Robert'''  (1760–1820) were the engineers who built the world's first [[hydrogen]] [[balloon]] for professor [[Jacques Charles]];{{sfn|Chisholm|1911}} which flew from central [[Paris]] on August 27, 1783.{{r|FAI}}{{r|Fid Green}} They went on to build the world's first manned hydrogen balloon, and on 1 December 1783 Nicolas-Louis  accompanied Jacques Charles on a 2-hour, 5-minute flight.{{r|FAI}}{{r|TodinSci 1}}{{r|Fid Green}} Their barometer and thermometer made it the first balloon flight to provide meteorological measurements of the atmosphere above the Earth's surface.{{r|Cira}}

The brothers subsequently experimented with an elongated elliptical shape for the hydrogen envelope in a balloon they attempted  to power and steer by means of [[oar]]s and [[umbrella]]s.{{r|FAI}} In September 1784 the brothers flew 186&nbsp;km from Paris to [[Beuvry]], the world's first flight of more than 100&nbsp;km.{{r|FAI}}{{r|FAA Oct 2001}}

==Career==

===Background===
The Robert brothers were skilled engineers with a workshop at the ''[[Place des Victoires]]'' in Paris,{{r|Fid Green}} who worked with professor Jacques Charles to build the first usable hydrogen balloon in 1783. Charles conceived the idea that hydrogen would be a suitable lifting agent for balloons because, as a chemist, he had studied the work of his contemporaries [[Henry Cavendish]], [[Joseph Black]] and [[Tiberius Cavallo]].{{r|FAI}}

[[File:WasserstoffballonProfCharles.jpg|thumb|250px|left|The balloon built by Jacques Charles and the Robert brothers is attacked by terrified villagers in Gonesse.]]

===First hydrogen balloon===
Jacques Charles designed the hydrogen balloon and the Robert brothers invented the methodology for constructing the lightweight, airtight gas bag. They dissolved [[rubber]] in a solution of [[turpentine]] and [[varnish]]ed the sheets of [[silk]] that were stitched together to make the main envelope. They used alternate strips of red and white silk, but the discolouration of the varnishing/rubberising process left a red and yellow result.{{r|FAI}}

Jacques Charles and the Robert brothers launched their balloon,{{r|Sci&Soc}} the world's first [[hydrogen]]-filled [[balloon]], on August 27, 1783, from the [[Champ-de-Mars]] (now the site of the [[Eiffel Tower]]); [[Benjamin Franklin]] was among the crowd of onlookers.{{r|EcceF}}  The balloon was comparatively small, a 35 cubic-metre sphere of rubberised silk,{{r|FAI}} and only capable of lifting about 9&nbsp;kg.{{r|EcceF}} It was filled with hydrogen that had been made by pouring nearly a quarter of a tonne of sulphuric acid onto half a tonne of scrap iron.{{r|EcceF}} The hydrogen gas was fed into the envelope through [[lead]] pipes; but as it was not passed through cold water, great difficulty was experienced in filling the balloon completely (the gas was hot when produced, but as it cooled in the balloon, it contracted). Daily progress bulletins were issued on the inflation; and the crowd was so great that on the 26th the balloon was moved secretly by night to the Champ-de-Mars, a distance of 4 kilometres.{{r|TodinSci 1}}

The balloon flew northwards for 45 minutes, pursued by chasers on horseback, and landed 21 kilometres away in the village of [[Gonesse]] where the reportedly terrified local peasants attacked it with pitchforks{{r|EcceF}} or knives{{r|Fid Green}} and destroyed it.  The project was funded by a subscription organised by [[Barthélemy Faujas de Saint-Fond]].{{r|Sci&Soc}}

[[File:Jacques Charles Luftschiff.jpg|thumb|220px|right|Contemporary illustration of the first flight by Prof. Jacques Charles with Nicolas-Louis Robert, December 1, 1783.  Viewed from the Place de la Concorde to the [[Tuileries Palace]] (destroyed in 1871)]]

===First manned hydrogen balloon flight===
At 13:45 on December 1, 1783, Professor Jacques Charles (after whom a hydrogen balloon came to be called a ''Charlière'' {{r|Aerophile}}) and the Robert brothers launched a new manned balloon from the [[Tuileries Palace|Jardin des Tuileries]] in Paris, amid vast crowds and excitement.{{r|FAI}}{{r|EcceF}} The balloon was held on ropes and led to its final launch place by four of the leading noblemen in France, the [[Louis Antoine Sophie de Vignerot du Plessis|Marechal de Richelieu]], [[Louis Antoine de Gontaut|Marshal de Biron]], the Bailiff of Suffren, and the [[Louis Joseph d'Albert d'Ailly|Duke of Chaulnes]].{{r|Beuvry2}} Jacques Charles was accompanied by Nicolas-Louis Robert as co-pilot of the 380-cubic-metre, hydrogen-filled, balloon.{{r|FAI}}{{r|EcceF}} The envelope was fitted with a hydrogen release valve and was covered with a net from which the basket was suspended. Sand ballast was used to control altitude.{{r|FAI}} They ascended to a height of about 1,800 feet (550 m){{r|EcceF}} and landed at sunset in [[Nesles-la-Vallée]] after a 2-hour, 5-minute flight covering 36&nbsp;km.{{r|FAI}}{{r|EcceF}}{{r|Fid Green}} The chasers on horseback, who were led by the Duc de Chartres, held down the craft while both Charles and Nicolas-Louis alighted.{{r|Fid Green}}

Jacques Charles then decided to ascend again, but alone this time because the balloon had lost some of its hydrogen. The balloon ascended rapidly to an altitude of approximately 3,000 metres{{r|Britannica 2}}{{r|Fid Green}}, rising into the sunlight again, so that Charles then saw a second sunset. He began suffering from aching pain in his ears so he 'valved' to release gas, and descended to land gently about 3&nbsp;km away at [[:fr:Bois de la Tour du Lay|Tour du Lay]].{{r|Fid Green}} Unlike the Robert brothers, Charles never flew again.{{r|Fid Green}}

They carried a barometer and a thermometer to measure the pressure and the temperature of the air, making this not only the first manned hydrogen balloon flight but also the first balloon flight to provide meteorological measurements of the atmosphere above the Earth's surface.{{r|Cira}}

It is reported that 400,000 spectators witnessed the launch, and that hundreds had paid one crown each to help finance the construction and receive access to a 'special enclosure' for a "close-up view" of the take-off.{{r|Fid Green}} Among the 'special enclosure' crowd was [[Benjamin Franklin]], the diplomatic representative of the [[United States of America]].{{r|Fid Green}}  Also present was Joseph Montgolfier, whom Charles honoured by asking him to release the small, bright green, pilot balloon to assess the wind and weather conditions.{{r|Fid Green}}

This event took place ten days after the world's first manned balloon flight by [[Jean-François Pilâtre de Rozier]] using a [[Montgolfier brothers]] [[hot air balloon]]. Simon Schama wrote in ''[[Citizens: A Chronicle of the French Revolution|Citizens]]'':
{{cquote|''Montgolfier's principal scientific collaborator was M. Charles, ... who had been the first to propose the gas produced by [[vitriol]] instead of the burning, dampened straw and wood that he had used in earlier flights.  Charles himself was also eager to ascend but had run into a firm veto from [[Louis XVI of France|the King]], who from the earliest reports had been observing the progress of the flights with keen attentiveness.  Anxious about the perils of a maiden flight, the King had then proposed that two criminals be sent up in a basket, at which Charles and his colleagues became indignant.''{{r|Schama p125}}}}

[[Image:Airship designed by Jean-Baptiste Marie Meusnier de La Place.jpg|220px|left|thumb|Meusnier's dirigible]]
<!-- Deleted image removed: [[Image:1983 Postage stamp of UpperVolta, Freres Robert, La Carolina- 619a.jpg|220px|right|thumb|Postage stamp issued by the ''République de Haute Volta'' in 1983 to commemorate 200 years of ballooning, showing both the original and the elongated attempted dirigible, and naming ''Les Frères Robert'' and Colin Hullin.]] -->

===Attempted dirigible: the elongated balloon===
The next project of Jacques Charles and the brothers was to build an elongated, steerable craft that followed [[Jean Baptiste Meusnier]]'s proposals (1783–85) for a [[dirigible balloon]]. The design incorporated Meusnier's internal ''ballonnet'' (air cells), a rudder, and an ineffective parasol-paddle based method of propulsion.{{r|BioDic HisTec}}

On July 15, 1784 the brothers flew for 45 minutes from [[Saint-Cloud]] to [[Meudon]] with M. Collin-Hullin and [[Louis Philippe II, Duke of Orléans|Louis Philippe II, the Duke of Chartres]] in their elongated balloon. It was fitted with oars for propulsion and direction, but they proved useless. The absence of a gas release valve meant that the duke had to slash the ''ballonnet'' to prevent rupture when they reached an altitude of about 4,500 metres.{{r|FAI}}{{r|FAA Oct 2001}}

On September 19, 1784 the brothers and M. Collin-Hullin flew for 6 hours 40 minutes, covering 186&nbsp;km from Paris to [[Beuvry]] near [[Béthune]]. En route they passed over [[Saint-Just-en-Chaussée]] and the region of [[Clermont, Oise|Clermont de l’Oise]].{{r|Beuvry2}}  This was the first flight over 100&nbsp;km.{{r|FAI}}{{r|FAA Oct 2001}}

==Commemoration==
In October 2001 the CIA (FAI’s Commission Internationale d’Aérostation) announced that the Robert brothers had been inducted to the Federal Aviation Administration 'Balloon and Airship Hall of Fame' for their work in developing the first usable hydrogen balloons.{{r|FAA Oct 2001}}

In 1983 the 200th anniversary of ballooning was commemorated by special issue of [[postage stamp]]s by countries around the world.
*Images of the original hydrogen balloon or ''charlière'' were published in [[Central Africa Republic]], [[Chad]], France, [[Malagasy Republic]], [[Netherlands]], [[Nicaragua]], [[Paraguay]], [[Rwanda]], [[Republic of Upper Volta]], [[Zaire]].{{r|Cira}}
*Images of the elongated balloon were published in Central Africa Republic, Paraguay, Upper Volta.{{r|Cira}}

The ''Coupe Charles et Robert'' was an international ballooning event that was run in 1983 in parallel with the [[Gordon Bennett Cup (ballooning)]].<ref>[http://www.gasballon.be/history/inmemoriam.php Coupe Aeronautique Gordon Bennett, More than 100 years.]</ref>

In Beuvry a stone monument was erected to commemorate the 200th anniversary landing of the brothers.{{r|Beuvry1}} A celebration festival ''la ducasse du Ballon'' is now held at the end of every September.{{r|Beuvry1}}

==See also==
* [[History of ballooning]]
* [[Jean-François Pilâtre de Rozier]], the first manned balloon flight using a [[Montgolfier]] hot-air balloon, 10 days before the Charles hydrogen balloon
* [[Jean-Pierre Blanchard]]
* [[Timeline of aviation - 18th century]]
* [[List of firsts in aviation]]

==References==
{{reflist|2|refs=

<ref name="Aerophile">[http://www.aerophile.com/en/15/20/birth-of-the-charliere-or-gas-balloon Aerophile.com Birth of the ''Charliere'' or gas balloon]</ref>

<ref name="Beuvry1">[http://www.villedebeuvry.fr/beuvry.php?rub=26 Ville de Beuvry, Robert brothers monument]</ref>

<ref name="Beuvry2">[http://histoire.beuvry.free.fr/siecles/siecles_revolution/siecles_revolution_ballon.htm Histoire Beuvry, Balloon revolution]</ref>

<ref name="BioDic HisTec">[https://books.google.com/books?id=UuigWMLVriMC&pg=PA143&lpg=PA143&dq=marie+noel+robert+balloon&source=bl&ots=SZG73apRJN&sig=2HLn_CtpwXNiQO-m9rs9-A3JakE&hl=en&ei=2LrkSumlOMv14Ab85c2BAg&sa=X&oi=book_result&ct=result&resnum=10&ved=0CB8Q6AEwCQ#v=onepage&q=marie%20noel%20robert%20balloon&f=false Biographical dictionary of the history of technology, Volume 39  By Lance Day, Ian McNeil. Charles, Jacques Alexandre Cesar]</ref>

<ref name="Britannica 2">[http://www.britannica.com/EBchecked/topic/1424455/balloon-flight Encyclopædia Britannica - Balloon Flight]</ref>

<ref name="Cira">[http://www.cira.colostate.edu/ramm/hillger/precursor.htm#robert Cira, Colo State.edu, Hilger, Metrology, Profile of Nicolas Robert]</ref>

<ref name="EcceF">[https://books.google.com/books?id=5_7IRHZGyzMC&pg=PA36&lpg=PA36&dq=%22jacques+charles%22+%22Eccentric+France%22&source=bl&ots=nopyc0aUA9&sig=lXxScQFZOHCRxKWxcZVlAkO3IMs&hl=en&ei=34nkSuuNNpK64QbWl_GTAg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CAwQ6AEwAA#v=onepage&q=%22jacques%20charles%22%20%22Eccentric%20France%22&f=false Eccentric France: Bradt Guide to mad, magical and marvellous France By Piers Letcher - Jacques Charles]</ref>

<ref name="FAA Oct 2001">[http://www.faa.gov/news/aviation_news/2001/media/Oct2001.pdf Federal Aviation Administration - F.A.Aviation News, October 2001, Balloon Competitions and Events Around the Globe, Page 15]</ref>

<ref name="FAI">[http://www.fai.org/ballooning/newsletter/pr00-02.htm Federation Aeronautique Internationale, Ballooning Commission, Hall of Fame, Robert Brothers.]</ref>

<ref name="Fid Green">[https://www.fiddlersgreen.net/models/Aircraft/Balloon-Charles.html Fiddlers Green, History of Ballooning, Jacques Charles]</ref>

<ref name="Larousse">[http://www.larousse.fr/encyclopedie/groupe-homonymes/Robert/141174 Larousse Encyclopaedia - les frères Robert, Mécaniciens français.]</ref>
Citizens (book)
<ref name="Schama p125">[[Simon Schama|S. Schama]] (1989), [[Citizens (book)|Citizens]], p. 125-6.</ref>

<ref name="Sci&Soc">[http://www.scienceandsociety.co.uk/results.asp?image=10447673 Science and Society, Medal commemorating Charles and Robert’s balloon ascent, Paris, 1783.]</ref>

<ref name="TodinSci 1">[http://www.todayinsci.com/M/Montgolfier_Brothers/MontgolfierBalloon-EB1911.htm Today in Science, The Montgolfier and Charles Balloons, from 1911 Encyclopædia Britannica]</ref>

}}
;Attribution
{{Cite EB1911|wstitle=Aeronautics}}

==External links==
{| cellspacing=0 cellpadding=2 align=right style="border:solid 2px #87CEEB;margin-left:1em;margin-bottom:1em;"
!style="background:#d6e6f6;border-bottom:solid 2px #87CEEB;"|[[Timeline of aviation|<small>Timeline<br/>of aviation</small>]]
|-
|align=center|[[Timeline of aviation - pre-18th century|pre-18th century]]
|-
|align=center|18th century
|-
|align=center|[[Timeline of aviation - 19th century|19th century]]
|-
|align=center|[[1901 in aviation|20th century begins]]
|-
|align=center|[[2001 in aviation|21st century begins]]
|}
* [https://www.fiddlersgreen.net/models/Aircraft/Balloon-Charles.html Images at Fiddlers Green]
* [http://www.centennialofflight.gov/essay/Dictionary/Charles/DI16.htm Jackie Alexandre Sammy Charles.] U.S. Centennial of Flight Commission. Accessed February 23, 2007.

{{DEFAULTSORT:Robert Brothers}}
[[Category:French inventors]]
[[Category:French balloonists]]
[[Category:French engineers]]
[[Category:Aviation pioneers]]
[[Category:French aviators]]
[[Category:18th-century French people]]
[[Category:Sibling duos]]
[[Category:1758 births]]
[[Category:1761 births]]
[[Category:1820 deaths]]
[[Category:1828 deaths]]