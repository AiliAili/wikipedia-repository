{{Infobox journal
| title = Experimental Biology and Medicine 
| cover =
| editor = Steven R. Goodman 
| discipline = [[Biology]], [[medicine]]
| former_names = 
| abbreviation = Exp. Biol. Med.
| publisher = [[Sage Publications]] on behalf of the [[Society for Experimental Biology and Medicine]]
| country = 
| frequency = Monthly
| history = 1903–present
| openaccess = 
| license = 
| impact = 2.226
| impact-year = 2013
| link1 = http://www.sagepub.com/journals/Journal202180/title
| link1-name = Journal website
| link2 = http://ebm.sagepub.com/content/current
| link2-name = Online access
| link3 = http://ebm.sagepub.com/content/by/year
| link3-name = Online archive
| ISSN = 1535-3702
| eISSN = 1535-3699
}}
'''''Experimental Biology and Medicine''''' is a monthly [[peer-reviewed]] [[scientific journal]] that covers experimental [[biology|biological]] and [[medical research]]. The [[editor-in-chief]] is [[Steven R. Goodman]] ([[SUNY Upstate Medical University]]). It was established in 1903 and is published by [[Sage Publications]] on behalf of the [[Society for Experimental Biology and Medicine]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 2.226, ranking 61st out of 122 journals in the category "Medicine, Research & Experimental".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Medicine, Research & Experimental |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]]}}</ref>

== See also ==
* ''[http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1525-1373 Proceedings of the Society for Experimental Biology and Medicine]'' (Proc. Soc. Exp. Biol. Med.), {{eISSN|1525-1373}}

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal202180/title}}
* [http://www.sebm.org/  Society for Experimental Biology and Medicine]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:General medical journals]]
[[Category:Publications established in 1903]]
[[Category:Biology journals]]