[[File:The Great Stone Face.jpg|thumb|"The Great Stone Face" as it appeared in ''[[The Snow-Image, and Other Twice-Told Tales]]'']]
"'''The Great Stone Face'''" is a short story published by [[Nathaniel Hawthorne]] in 1850. The story reappeared in a full-length book, ''[[The Snow-Image, and Other Twice-Told Tales]]'', published by [[Ticknor and Fields|Ticknor, Reed & Fields]] in 1852. It has since been republished and anthologized many times.<ref name="Hawthorne">{{cite web |url=http://www.classicreader.com/book/726/1/ |title=The Great Stone Face: by Nathaniel Hawthorne |work=Classic Reader |accessdate=2013-05-26 |format=}}</ref>
==Plot summary==
Hawthorne sets the scene in a rural valley located in an unnamed U.S. state that resembles [[New Hampshire]]. A rock formation in a nearby [[mountain pass|notch]] is imagined, by many locals and visitors, to resemble the shape and features of a human face:

<blockquote>The Great Stone Face, then, was a work of Nature in her mood of majestic playfulness, formed on the perpendicular side of a mountain by some immense rocks, which had been thrown together in such a position as, when viewed at a proper distance, to precisely to resemble the features of the human countenance. It seemed as if an enormous giant, or a [[Titan (mythology)|Titan]], had sculptured his own likeness on the precipice. There was the broad arch of the forehead, a hundred feet in height. . . .<ref name="Hawthorne"/></blockquote>

The local [[folklore]] of the valley includes a prophecy, alleged to descend from the [[Native Americans of the United States|Native Americans]], that at some future date a native son would be born within sight of the notch whose features would resemble the Great Stone Face; and when this face was seen, those who would see him would recognize that he was "the greatest and noblest personage of his time."<ref name="Hawthorne"/> This prophecy inspires an innocent youngster of the valley, Ernest, who feels within himself the quest to help uncover this hero.

As time passes and Ernest grows to manhood, the story from the notch is bruited about the United States, and others are also inspired. Unlike Ernest, the hope of some of them is that they themselves would be the hero of the tale. One by one, they revisit the valley to seek public recognition and acknowledgment of this resemblance. The succession of would-be American heroes forms the body of Hawthorne's narrative. In succession, a merchant of immense wealth, a conquering general, a politician renowned for his skilled [[Eloquence|oratory]], and finally a brilliant writer return to the glen. After enjoying the brief plaudits of their admirers, the four men each reveal themselves to have [[character flaw]]s that prevent them from fulfilling the conditions of the prophecy. Each of them have slight flaws in their [[physiognomies]], recognized at once by the sensitive Ernest, that serve as foreshadows of their inability to live up to the expectations of their eager friends. 

During this string of disappointments, Ernest has become a spry but aged man. He has progressed from being a hill farmer to the position of local [[lay preacher]]. The writer, who (in contrast to the first three contestants) frankly acknowledges his failure to fulfill the prophecy, caps his visit to the notch by attending one of Ernest's impromptu sunset [[sermon]]s. By popular demand, the congregation has asked Ernest to deliver his sacred remarks from a site at the base of the notch where the worshipers can see the Great Stone Face high above.

Hawthorne describes the climax of Ernest's sermon:

<blockquote>At that moment, in sympathy with a thought which he was about to utter, the face of Ernest assumed a grandeur of expression, so imbued with benevolence, that the poet, by an irresistible impulse, threw his arms aloft and shouted, 'Behold!  Behold!  Ernest is himself the likeness of the Great Stone Face!'  Then all the people looked, and saw that what the deep-sighted poet said was true. The prophecy was fulfilled. But Ernest, having finished what he had to say, took the poet's arm, and walked slowly homeward, still hoping that some wiser and better man than himself would by and by appear, bearing a resemblance to the GREAT STONE FACE.<ref name="Hawthorne"/></blockquote>

==Composition and publication history==
Hawthorne likely began writing "The Great Stone Face" while living at 14 Mall Street in his native town of [[Salem, Massachusetts]], at the time when he was working at the Salem custom house.<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 283–284. ISBN 0-395-27602-0</ref> It was first published on January 24, 1850, in ''[[The National Era]]''.<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 292. ISBN 0-395-27602-0</ref> It was later collected in ''[[The Snow-Image, and Other Twice-Told Tales]]''.

==Critical commentary==
Patrick Hummel, park manager with the [[New Hampshire Division of Parks and Recreation]], in 2013 identified Hawthorne's short story "The Great Stone Face" as popularizing the real-life geological formation, the [[Old Man of the Mountain]], which looked down upon [[Franconia Notch]] until it crumbled into rubble in 2003.<ref name="Hummel">{{cite web |url=http://blog.nhstateparks.org/monadnock-weekly-report-05-03-13/ |title=Monadnock Weekly Report 05.03.13 |work=[[New Hampshire Division of Parks and Recreation]] |accessdate=2013-05-26 |format=}}</ref> The "Old Man" was a well-known [[tourist attraction]] in 1850, and Hawthorne's readers would have been familiar with it. The face of the New Hampshire-born politician and statesman [[Daniel Webster]] was often compared with that of the Old Man at that time, and Hummel asserts that the senator and the rock formation are still thought of together in common memory.<ref name="Hummel"/> Hawthorne certainly had Webster and his recent bid for the Presidency in mind when he wrote the story, though he is not named specifically but is instead nicknamed "Old Stony Phiz".<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 291. ISBN 0-395-27602-0</ref>

==See also==
* [[Physiognomy]]

==References==
{{reflist}}

==External links==
{{wikisource}}
* {{librivox book | title=The Great Stone Face | author=Nathaniel Hawthorne}}

{{Nathaniel Hawthorne}}
{{DEFAULTSORT:Great Stone Face}}
[[Category:1850 short stories]]
[[Category:Short stories by Nathaniel Hawthorne]]