{{other ships|HMS Hyacinth}}
{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Hyancinth 1915 AWM P01236.016.jpeg|300px]]
|Ship caption=HMS ''Hyancinth'' circa. 1915
}}
{{Infobox ship career
|Hide header=
|Ship country=United Kingdom
|Ship flag=[[File:Naval Ensign of the United Kingdom.svg|60px|Royal Navy Ensign]]
|Ship name=HMS ''Hyacinth''
|Ship ordered=
|Ship awarded=
|Ship builder=[[London and Glasgow Shipbuilding Company|London & Glasgow Shipbuilding]], [[Govan]]
|Ship laid down=21 January 1897
|Ship launched=27 October 1898
|Ship christened=
|Ship completed=3 September 1900
|Ship commissioned=
|Ship recommissioned=
|Ship decommissioned=August 1919
|Ship in service=
|Ship out of service=
|Ship reinstated=
|Ship fate=Sold for [[ship breaking|scrap]], 11 October 1923
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=[[Highflyer class cruiser|''Highflyer''-class]] [[protected cruiser]]
|Ship displacement={{convert|5650|LT|t}}
|Ship length=*{{convert|350|ft|m|abbr=on}} ([[Length between perpendiculars|p.p.]])
*{{convert|372|ft|m|abbr=on}} ([[Length overall|o/a]])
|Ship beam={{convert|54|ft|m|abbr=on|1}}
|Ship draught={{convert|21|ft|6|in|m|abbr=on|1}}
|Ship power=*{{convert|10000|ihp|kW|lk=in|abbr=on}}
*18 × [[Belleville boiler]]s
|Ship propulsion=*2 × Shafts
*2 × 4-cylinder [[Marine steam engine#Triple or multiple expansion|triple-expansion steam engines]]  
|Ship speed={{convert|20|kn|lk=in|abbr=on}}
|Ship range=
|Ship complement=470
|Ship armament=*11 × single [[QF 6 inch Mk I - III naval gun|QF {{convert|6|in|mm|abbr=on|0}} guns]]
*8 × single [[QF 12 pounder 12 cwt naval gun|QF 12-pounder 12 cwt guns]]<ref group=Note>"Cwt" is the abbreviation for [[hundredweight]], 12 cwt referring to the weight of the gun.</ref>
*6 × single [[QF 3 pounder Hotchkiss|QF 3-pounder]] [[Hotchkiss gun]]s
*2 × single [[British 18 inch torpedo|18 in (45 cm)]] [[torpedo tube]]s
|Ship armour=*[[Deck (ship)|Deck]]: {{convert|1.5|-|3|in|mm|0|abbr=on}}
*[[Gun shield]]s: {{convert|3|in|mm|0|abbr=on}}
*[[Conning tower]]: {{convert|6|in|mm|0|abbr=on}}
*Engine hatches: {{convert|5|in|mm|0|abbr=on}} 
|Ship notes=
}}
|}

'''HMS ''Hyacinth''''' was one of three [[Highflyer class cruiser|''Highflyer''-class]] [[protected cruiser]]s built for the [[Royal Navy]] in the 1890s. Initially assigned to the [[Channel Fleet]], she spent much of her early career as [[flagship]] for the [[East Indies Station]]. She was reduced to [[Reserve fleet|reserve]] in 1912 after a lengthy refit before becoming the flagship of the [[Cape of Good Hope Station]] in 1913. After the beginning of [[World War I]] in August 1914, she spent the first few months of the war escorting convoys around South Africa. In early 1915, she was deployed to German East Africa to [[blockade]] the German [[light cruiser]] [[SMS Königsberg (1905)|SMS ''Königsberg'']]. She destroyed a German [[blockade runner]] attempting to bring supplies through the blockade in April and sank a German merchant vessel in early 1916. ''Hyacinth'' remained on the Cape Station for the rest of the war and was [[Ship decommissioning|paid off]] in 1919, although she was not sold for [[ship breaking|scrap]] until 1923.

==Design and description==
[[File:HMS Hermes (1898) 6-inch guns.jpg|thumb|left|The two 6-inch guns on her [[sister ship]] ''Hermes''{{'}}s [[quarterdeck]]]]
''Hyacinth'' was designed to [[Displacement (ship)|displace]] {{convert|5650|LT|t}}. The ship had an [[length overall|overall length]] of {{convert|372|ft|m|1}}, a [[beam (nautical)|beam]] of {{convert|54|ft|m|1}} and a [[draft (ship)|draught]] of {{convert|29|ft|6|in|m|1}}. She was powered by two 4-cylinder [[Marine steam engine#Triple or multiple expansion|triple-expansion steam engines]], each driving one shaft, which produced a total of {{convert|10000|ihp|lk=in}} designed to give a maximum speed of {{convert|23|kn|lk=in}}. The engines were powered by 18 [[Belleville boiler]]s.<ref name=ck8>Chesneau & Kolesnik, p. 79</ref> She carried a maximum of {{convert|1125|LT|t}} of coal and her complement consisted of 470 officers and enlisted men.<ref name=f6>Friedman 2012, p. 336</ref>

Her main armament consisted of 11 [[quick-firing gun|quick-firing (QF)]] [[QF 6 inch Mk I - III naval gun|{{convert|6|in|mm|adj=on|0}} Mk I guns]].<ref>Friedman 2011, p. 87</ref> One gun was mounted on the [[forecastle]] and two others were positioned on the [[quarterdeck]]. The remaining eight guns were placed [[port and starboard]] [[amidships]].<ref>Friedman 2012, p. 171</ref> They had a maximum range of approximately {{convert|10000|yd}} with their {{convert|100|lb|adj=on}} shells.<ref>Friedman 2011, pp. 87–88</ref> Eight [[quick-firing gun|quick-firing (QF)]] [[QF 12 pounder 12 cwt naval gun|12-pounder 12 cwt guns]] were fitted for defence against [[torpedo boat]]s. One additional [[QF 12-pounder 8-cwt Mk I naval gun|12-pounder 8 cwt gun]] could be dismounted for service ashore.<ref name=f6/> ''Hyacinth'' also carried six [[QF 3-pounder Hotchkiss|3-pounder]] [[Hotchkiss gun]]s and two submerged [[British 18 inch torpedo|18-inch]] [[torpedo tube]]s.<ref name=ck8/>
 
The ship's protective [[deck (ship)|deck]] armour ranged in thickness from {{convert|1.5|to|3|in|mm|0}}. The engine hatches were protected by {{convert|5|in|adj=on|0}} of armour. The main guns were fitted with 3-inch [[gun shield]]s and the [[conning tower]] had armour 6 inches thick.<ref name=ck8/>

==Construction and service==
''Hyacinth'' was [[Keel|laid down]] by [[Fairfield Shipbuilding & Engineering]] at their shipyard in [[Govan]], [[Scotland]] on 27 January 1897 and [[Ship naming and launching|launched]] on 27 October 1898. She was completed on 7 December 1899<ref name=ck8/> She served with the Channel Fleet under Captain [[Douglas Gamble]] until she relieved her sister [[HMS Highflyer (1898)|''Highflyer'']] in 1903 as flagship of the East Indies Station. She was in reserve at [[Devonport Royal Dockyard]] in 1906 until she again became flagship of the East Indies Station in February 1907. She returned home in March 1911 for a refit at [[Chatham Royal Dockyard]] and was transferred to the reserve Third Fleet in February 1912. She recommissioned a year later for service as the flagship of the Cape of Good Hope Station, relieving her other sister, {{HMS|Hermes|1898|2}}.<ref name=gg6>Gardiner & Gray, p. 16</ref>

[[File:Bundesarchiv Bild 105-DOA3002, Deutsch-Ostafrika, Kreuzer Königsberg.jpg|thumb|left|''Königsberg'' in Dar es Salaam, 1914]]
Shortly before the beginning of the war, [[Rear-Admiral]] [[Herbert King-Hall]], commander of the Cape Station, was ordered to find and shadow SMS ''Königsberg'', based at [[Dar-es-Salaam]], [[German East Africa]]. Two of his ships, including ''Hyacinth'', spotted the German ship, but neither was fast enough to follow her. In early September she escorted the [[troopship]]s transporting the garrison of the [[Cape Colony]] home up to the Central Atlantic before returning to the Cape.<ref>Corbett, Vol. I, pp. 152, 264</ref> In November, King-Hall briefly transferred his flag to the [[armoured cruiser]] {{HMS|Minotaur|1906|2}} when his command was strengthened in anticipation of a battle with the [[German East Asia Squadron]] after its victory in the [[Battle of Coronel]]. ''Hyacinth'' hoisted his flag after ''Minotaur'' was ordered home as a result of the decisive victory over the German squadron in the [[Battle of the Falklands]] in early December. When the [[predreadnought battleship]] [[HMS Goliath (1898)|''Goliath'']] arrived later that month, he transferred his flag to her and ordered ''Hyacinth'' north to German East Africa. She arrived at the end of January 1915 and blockaded ''Königsberg'' in the [[Rufiji River|Rufiji delta]]. ''Goliath'' was ordered to the [[Dardanelles Campaign|Dardanelles]] on 25 March and the ship again became King-Hall's flagship.<ref>Corbett, Vol. II, pp. 234–35, 238–39</ref>

[[File:Raid hyacinth-rubens east africa1915-04-14.jpg|thumb|1915: ''Hyacinth'' meets the German auxiliary ship ''Rubens'' om the coast of German East Africa. ''Rubens'' escapes into Manza Bay.]]
On 14 April ''Hyacinth'' intercepted the captured British merchantman {{SS|Rubens}} making an attempt to deliver supplies to German East Africa. The cruiser spotted her bound for [[Tanga, Tanzania|Tanga]], but was not able to board and capture her when one engine broke down. ''Rubens'' [[Beaching (nautical)|beached]] herself out of sight in [[Manza Bay]], although ''Hyacinth'' set her afire. The fire was too hot for her cargo to be salvaged when ''Hyacinth''{{'}}s crew approached the stranded ship. The Germans, however, were able to salvage a great deal of her cargo after the fire had burnt out.<ref>Corbett, Vol. III, pp. 8–9; Newbolt, Vol. IV, p. 80</ref>

''Hyacinth'' remained on the Cape Station until the end of the war. On 23 March 1916 she sank the German merchant ship {{SS|Tabora}} in Dar-es-Salaam.<ref name=gg6/> In January 1917 she was stationed off [[Tanganyika]], where she served as the [[depot ship]] for the [[Royal Naval Air Service]]. On 6 January, Squadron Leader [[Edwin Moon]] was on a reconnaissance flight with Commander [[George Bridgeman, 4th Earl of Bradford#Family|Richard Bridgeman]] as observer, when they were forced to land with engine trouble and came down in a creek of the [[Rufiji River]] delta. Moon and Bridgeman wandered for days in the river delta before eventually building a makeshift raft which was swept out to sea. Bridgeman died of exposure but Moon was blown back to shore where he was taken into captivity. Moon was awarded a [[Medal bar|bar]] to his [[Distinguished Service Order]] for the display of "the greatest gallantry in attempting to save the life of his companion",<ref name="LG3395">{{LondonGazette|issue=30581|startpage=3395|date=15 March 1918|accessdate=31 May 2010}}</ref> together with the [[Royal Humane Society]]'s silver medal for his attempts to save Bridgeman's life and [[Légion d'honneur|The Legion of Honour]] – Croix de Chevalier.<ref name="HCC">{{cite web|title=Edwin Rowland Moon 1886 – 1920|url=http://www3.hants.gov.uk/centenary-of-flight/moon.htm|publisher=[[Hampshire County Council]]|accessdate=31 May 2010}}</ref> Bridgeman's body was recovered from the sea and is buried in [[Dar es Salaam]] [[Commonwealth War Graves Commission]] Cemetery.<ref>{{cite web|title=Casualty Details: Bridgeman, Richard Orlando Beaconsfield|url=http://www.cwgc.org/search/casualty_details.aspx?casualty=897905 |publisher=CWGC|accessdate=31 May 2010}}</ref> ''Hyacinth'' was paid off in August 1919 and sold for scrap on 11 October 1923.<ref>Colledge, p. 169</ref>

==Notes==
{{reflist|group=Note}}

==Footnotes==
{{Reflist|2}}

== Bibliography ==
* {{cite book|title=Conway's All the World's Fighting Ships 1860–1905|editor1-last=Chesneau|editor1-first=Roger|editor2-last=Kolesnik|editor2-first=Eugene M.|publisher=Conway Maritime Press|location=Greenwich|year=1979|isbn=0-8317-0302-4|lastauthoramp=y}}
*{{Colledge}}
* {{cite book|last=Corbett|first=Julian|authorlink=Julian Corbett|title=Naval Operations to the Battle of the Falklands|edition=2nd, reprint of the 1938|series=History of the Great War: Based on Official Documents|volume=I|publisher=Imperial War Museum and Battery Press|location=London and Nashville, Tennessee|isbn=0-89839-256-X}}
*{{cite book|last=Corbett|first=Julian|authorlink=Julian Corbett|title=Naval Operations|edition=reprint of the 1929 second|series=History of the Great War: Based on Official Documents|volume=II|year=1997|publisher=Imperial War Museum in association with the Battery Press|location=London and Nashille, Tennessee|isbn=1-870423-74-7}}
*{{cite book|last=Corbett|first=Julian|authorlink=Julian Corbett|title=Naval Operations|edition=reprint of the 1940 second|series=History of the Great War: Based on Official Documents|volume=III|year=1997|publisher=Imperial War Museum in association with the Battery Press|location=London and Nashille, Tennessee|isbn=1-870423-50-X}}
*{{cite book|last=Friedman|first=Norman|title=British Cruisers of the Victorian Era|year=2012|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|isbn=978-1-59114-068-9}}
* {{cite book|last=Friedman|first=Norman|title=Naval Weapons of World War One|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|year=2011|isbn=978-1-84832-100-7}}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=0-85177-245-5|lastauthoramp=y}}
*{{cite book|last=Goldrick|first=James|title=The King's Ships Were at Sea: The War in the North Sea August 1914–February 1915|year=1984|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=0-87021-334-2}}
* {{cite book|last=Newbolt|first=Henry|title=Naval Operations|edition=reprint of the 1928|series=History of the Great War Based on Official Documents|volume=IV|year=1996|publisher=Battery Press|location=Nashville, Tennessee|isbn=0-89839-253-5}}
*{{cite web|url=http://www.naval-history.net/OWShips-WW1-05-HMS_Hyacinth.htm|title=Transcript:  HMS HYACINTH - January 1914 to December 1916, Cape of Good Hope Station (Part 1 of 2)|work=Royal Navy Log Books of the World War 1 Era|publisher=Naval-History.net|accessdate=10 March 2014}}

==Further reading==
*{{cite magazine|title=The Boiler Trials of H.M.S. "Hyacinth" and H.M.S. "Minerva"|magazine=[[Engineering (magazine)|Engineering]]|date=30 August 1901|pages=283–285|url=http://www.gracesguide.co.uk/images/d/d2/Eg19010830.pdf#9}}

==External links==
*[http://www.worldwar1.co.uk/light-cruiser/hms-Highflyer.html Highflyer class in World War I]
*[http://www.historyofwar.org/articles/weapons_HMS_Hyacinth.html History of HMS Hyacinth]
*[http://www.worldnavalships.com/hermes_class1.htm#HMS%20Hyacinth HMS Hyacinth]

{{Highflyer class cruiser}}

{{DEFAULTSORT:Hyacinth (1898)}}
[[Category:Highflyer-class cruisers]]
[[Category:World War I cruisers of the United Kingdom]]
[[Category:1898 ships]]