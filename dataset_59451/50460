{{Infobox journal
| title = American Journal of Primatology
| cover =[[File:AmJPrimatolCover.jpg]]
| editor = Paul A. Garber
| discipline = [[Primatology]], [[biological anthropology]]
| abbreviation = Am. J. Primatol.
| publisher = [[John Wiley & Sons|Wiley-Liss]]
| country = United States
| frequency = Monthly
| history = 1981—present
| openaccess =
| license =
| impact = 2.221
| impact-year = 2011
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1098-2345
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1098-2345/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1098-2345/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 07113717
| LCCN = 81645893
| CODEN = AJPTDU
| ISSN = 0275-2565
| eISSN = 1098-2345
}}
The '''''American Journal of Primatology''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] and the official journal of the [[American Society of Primatologists]]. It was established in 1981 and covers all areas of [[primatology]], including the [[behavioral ecology]], [[conservation biology]], [[evolutionary biology]], life history, [[demography]], [[paleontology]], [[physiology]], [[endocrinology]], [[genetics]], [[molecular genetics]], and [[behavioral neuroscience|psychobiology]] of [[non-human primates]].<ref>[http://onlinelibrary.wiley.com/journal/10.1002/%28ISSN%291098-2345/homepage/ProductInformation.html Product Information] at wiley.com</ref> Besides its regular issues, the journal publishes a yearly supplementary issue detailing the program of the society's annual meetings. The [[editor-in-chief]] is [[Paul Garber]] ([[University of Illinois at Urbana–Champaign]]).  The types of papers published are: original research papers, review articles, book reviews, commentaries, and plenary addresses.

According to the ''[[Journal Citation Reports]]'', its 2011 [[impact factor]] is 2.221.<ref name=WoS>{{cite book |year=2012 |chapter=American Journal of Primatology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=|series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1098-2345 Official website]
* [http://www.asp.org/ American Society of Primatologists]
{{wikispecies|ISSN 0275-2565}}

[[Category:Primatology journals]]
[[Category:Wiley-Liss academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1981]]