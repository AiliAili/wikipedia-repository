{{Orphan|date=July 2013}}

{{Infobox military person
|name=Leonid Ivanov
|birth_date= 8 August 1909
|death_date= {{Death date and age|1941|6|27|1909|8|8}}
|birth_place=[[Kashira]], [[Moscow Governorate]], [[Russian Empire]] 
|image=Иванов Леонид Илларионович.jpg 
|caption= 
|nickname= 
|allegiance  = {{flag|Soviet Union}}
|serviceyears= 1932–1941
|rank        = [[Senior lieutenant]]
|commands    =
|unit        = [[147th Fighter Aviation Regiment]]
|battles     = [[Winter War]]<br>
[[World War II]]
*[[Operation Silver Fox]]
|awards      = {{Hero of the Soviet Union}} {{Order of Lenin}}<br>
[[Order of the Badge of Honour]]
|branch = [[Soviet Air Force]]|death_place = [[Afrikanda (rural locality)|Afrikanda]], [[Murmansk Oblast]], [[Soviet Union]]}}

'''Leonid Illarionovich Ivanov''' ([[Russian language|Russian]]: Леонид Илларионович Иванов; 8 August 1909 – 27 June 1941) was a [[Soviet Air Forces|Soviet Air Force]] [[senior lieutenant]] and posthumous [[Hero of the Soviet Union]]. Ivanov was posthumously awarded the title for reportedly shooting down two German aircraft in the first days of [[World War II]] and being killed in a ramming attack ([[Aerial ramming|taran]]) while defending his airfield from a  raid.<ref name=":0">{{Ruheroes|name = Leonid Ivanov|id = 80}}</ref>

==Early life==
Leonid Ivanov was born on 8 August 1909 in [[Kashira]]. After graduating from seven years of railway school, Ivanov took adult education courses while working as a laborer at the Kashira tram park. In the summer of 1930 he began working at the Krasny Proletary (Red Proletarian) Machine Tool Factory in Moscow as a [[Milling (machining)|milling machine]] operator. He took night school courses at the [[STANKIN]] Machine Tool Institute.<ref name=":0" />

In May 1932, Ivanov was drafted into the [[Red Army]].<ref name=":0" /> He graduated from the 7th [[Volgograd|Stalingrad]] Military Aviation School in 1935. Ivanov became a fighter pilot in the [[Leningrad Military District]] aviation wing  and later served in the [[Far East]] at [[Khabarovsk]]. He was transferred to the [[147th Fighter Aviation Regiment]] at [[Murmansk]] and fought in the [[Winter War]] between February and March 1940. In February 1941, he was awarded the [[Order of the Badge of Honour]].<ref name=":0" />

==World War II==
In June 1941 Ivanov was  commander of the 4th [[Squadron (aviation)|Squadron]] in the [[147th Fighter Aviation Regiment]] ([[:ru:147-й истребительный авиационный полк]]), [[1st Mixed Aviation Division]] ([[:ru:1-я смешанная авиационная дивизия]]), [[14th Army (Soviet Union)|14th Army]], which was equipped with [[Polikarpov I-153]]<ref>{{Cite web|url=http://niehorster.org/012_ussr/41_oob/leningrad/air.html|title=Niehorster.org|last=Niehorster|first=Leo|date=|website=World War II Armed Forces: Order of Battle and Organization|publisher=|access-date=2016-03-29}}</ref> and stationed on the [[Karelian Front]] (reportedly based at Murmashi) with the task of defending the harbours of [[Arkhangelsk]] and [[Murmansk]]. On 25 June, he and his wingman Lieutenant Filimonov damaged two [[Junkers Ju 88]]s over [[Kandalaksha]], and the next day he damaged a [[Heinkel He 111]] over the same region. On 26 June he shot down a [[Messerschmitt Bf 110]].  On 27 June he attacked seven [[Messerschmitt Bf 109]]s alone. Ivanov reportedly claimed three victories before he rammed a fourth. These losses cannot be verified in German sources.<ref>{{Cite web|url=http://surfcity.kund.dalnet.se/soviet_ivanov2.htm|title=Starshiy Leytenant Leonid Illarionovich Ivanov HSU|website=surfcity.kund.dalnet.se|access-date=2016-03-29}}</ref><ref name=":1">{{Cite web|url=http://aeroram.narod.ru/win/i/ivanov_li.htm|title=Иванов Леонид Илларионович|website=aeroram.narod.ru|language=Russian|trans-title=Ivanov Leonid Illarionovich|access-date=2016-03-29}}</ref>

Ivanov was buried in [[Afrikanda (rural locality)|Afrikanda]]. On 22 July 1941, he was posthumously awarded the title Hero of the Soviet Union and the Order of Lenin.<ref name=":0" /><ref name=":1" />

== Legacy ==
A fishing trawler was named after Ivanov.<ref name=":0" /><ref name=":1" />

==References==
{{Reflist}}

{{DEFAULTSORT:Ivanov, Leonid}}
[[Category:Soviet Air Force officers]]
[[Category:1909 births]]
[[Category:1941 deaths]]
[[Category:Soviet military personnel killed in World War II]]
[[Category:Aviators killed by being shot down]]
[[Category:Soviet World War II pilots]]
[[Category:Recipients of the Order of the Badge of Honour]]
[[Category:People from Kashira]]