{{Infobox journal
| title = International Journal of Clinical Practice
| cover = 
| former_names = Medical Bookman and Historian, Medical Illustrated, British Journal of Clinical Practice
| abbreviation = Int. J. Clin. Pract.
| discipline = [[Medicine]]
| editor = Leslie Citrome
| publisher = [[John Wiley & Sons]]
| country = 
| history = 1947-present
| frequency = Monthly
| openaccess = 
| license = 
| impact = 2.566
| impact-year = 2014
| ISSN = 1368-5031
| eISSN = 1742-1241
| CODEN = IJCPF9
| JSTOR = 
| LCCN = 
| OCLC = 37259930
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1742-1241
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1742-1241/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1742-1241/issues
| link2-name = Online archive
}}
The '''''International Journal of Clinical Practice''''' is a monthly [[peer-reviewed]] general [[medical journal]]. It was established in 1947 as the '''''Medical Bookman and Historian''''' and changed its name to '''''Medicine Illustrated''''' in 1949.<ref>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/97438 |title=Medicine Illustrated |publisher=NLM Catalog |accessdate=23 July 2015}}</ref> In 1956, its name was again changed, this time to the '''''British Journal of Clinical Practice'''''. The journal obtained its current name in 1997.<ref>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/0372546 |title=The British Journal of Clinical Practice |publisher=NLM Catalog |accessdate=23 July 2015}}</ref> The journal is published by [[John Wiley & Sons]] and the [[editor-in-chief]] is Leslie Citrome ([[New York Medical College]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.566, ranking it 35th out of 153 journals in the category "Medicine, General & Internal".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Medicine, General & Internal |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1742-1241}}

[[Category:General medical journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Publications established in 1947]]
[[Category:Monthly journals]]
[[Category:English-language journals]]