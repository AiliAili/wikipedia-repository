The '''Abertay Historical Society''' ('''AHS''')  is a [[historical society]] based in [[Dundee]], [[Scotland]]. It aims to promote interest in history, with a special focus on the history of [[Perthshire]], [[Angus, Scotland|Angus]] and [[Fife]]. The AHS runs a regular programme of public lectures on historical topics and also organises special events. The Society also publishes books, usually producing at least one publication per year.<ref name="Abertay home">{{cite web|title=Abertay Historical Society|url=http://www.abertay.org.uk/|publisher=Abertay Historical Society|accessdate=10 February 2012}}</ref>

==History==
The Society was founded at a meeting held at [[University of Dundee|University College, Dundee]] on 29 May 1947. The founders were the Principal of University College, Major-General [[Douglas Wimberley]], and the College's history lecturer, Dr Frederick T Wainwright. <ref name="About us">{{cite web|title=About us|url=http://www.abertay.org.uk/about-us.html|publisher=Abertay Historical Society|accessdate=10 February 2012}}</ref> It has been suggested that the Society was promoted as part of a process of developing 'town and gown links' in Dundee by Wimberley.<ref name="University">{{cite news|title=University Established|url=http://www.pressreader.com/uk/the-courier-advertiser-perth-and-perthshire-edition/20170202/282179355813920|accessdate=17 February 2017|work=The Courier & Advertiser (Perth & Perthshire Edition)|date=2 February 2017}}</ref> The AHS was set up with the goal of encouraging the study of local history in the [[River Tay|Abertay area]] (Perthshire, Angus and northern Fife).<ref name="About us"/>The Society pursued this aim by organising regular talks and lectures as well as other events such as excursions.<ref name="About us"/><ref name=SLH>{{cite journal|last=Baxter|first=Kenneth|title=Abertay Historical Society: 65th Birthday|journal=Scottish Local History|date=Autumn 2012|issue=84|pages=51–52}}</ref>

The Society formerly had an [[Archaeological]] Section which organised excavations in the local area and its own series of meetings. However, in 1993 the activities of this section came under the control of the Society’s Council.<ref name=Archives>{{cite web|title=MS 21 Abertay Historical Society|url=http://arccat.dundee.ac.uk/dserve.exe?&dsqIni=Dserve.ini&dsqApp=Archive&dsqCmd=show.tcl&dsqDb=Catalog&dsqPos=94&dsqSearch=((text)=%27abertay%20historical%20society%27)|work=Archives Services Online Catalogue|publisher=[[University of Dundee]]|accessdate=13 October 2014}}</ref> For a time in the 1970s the AHS also had an Industrial Archaeology Section, which mainly sought to record the region's industrial heritage.<ref name=Archives/>

Since 1953 the Society has published books on local history, the first of which was ''Dundee and the [[American Civil War]]'' by David Carrie. A number of notable historians and authors have written volumes published by the Society including Bruce Lenman, [[Andrew Murray Scott]] and [[Christopher Whatley]].<ref name=Courier>{{cite news|title=Rare Publications Will Be Available Online|newspaper=The Courier|date=13 December 2011}}</ref> The 51st AHS publication ''Ten Taysiders: Forgotten Figures from Dundee, Angus and Perthshire'' was published in 2011 and was edited by members of the Society's Council.<ref name="Ten Taysiders">{{cite news|title=New book tells the stories of 10 'forgotten' Taysiders|url=http://www.thecourier.co.uk/Community/Heritage-and-History/article/12686/new-book-tells-the-stories-of-10-forgotten-taysiders.html|accessdate=10 February 2012|newspaper=The Courier|date=9 April 2011}}</ref> In 2011 the AHS began to make copies of out-of-print titles available for free download from their website.<ref name=Courier/> The Society's publications are regularly cited in academic literature.<ref name=SLH/>

As well as running events, the AHS has undertaken various heritage activities. The Society has lobbied for the preservation of local buildings and historic sites, and played a key role in the creation of [[Dundee Civic Trust]] and [[Dundee Heritage Trust]].<ref name="About us"/> The Abertay Historical Society was also a benefactor of [[McManus Galleries|The McManus: Dundee's Art Gallery and Museum]] Fundraising Appeal which allowed the restoration and refurbishment of Dundee's main museum and galleries between 2006 and 2009.<ref name=McManus>{{cite web|title=Donations/Supporters. The McManus: Dundee's Art Gallery and Museum Fundraising Appeal|url=http://www.mcmanus.co.uk/content/home/donationssupporters|publisher=The McManus: Dundee's Art Gallery and Museum|accessdate=11 February 2012}}</ref>

A number of notable individuals have been associated with the Abertay Historical Society. These include Sir [[Francis Mudie]] who was President of the Society from 1963 to 1965 and also contributed to its publications.<ref name="Broughty Castle">{{cite book|last=Mudie|first=Sir Francis, Walker, David. M., and MacIvor, Iain|title=Broughty Castle and the Defence of the Tay|year=2010|publisher=Abertay Historical Society|location=Dundee|isbn=978-0-900019-47-0|pages=iv & 3}}</ref> Another key figure in the society's history was  S. G. Edgar Lythe, a founder member who went on to be Vice Principal of the [[University of Strathclyde]]. Lythe, who edited and wrote several of the Society's publications, was credited with moving the AHS away from simply organising events and towards actively encouraging research into local history.<ref name="SGE Lythe">{{cite news|last=Ogilvy|first=Graham|title=Prof Edgar Lythe|url=http://www.heraldscotland.com/sport/spl/aberdeen/prof-edgar-lythe-1.412865|accessdate=14 February 2012|newspaper=The Herald|date=13 February 1997}}</ref> The Dundee bibliophile and antiquarian Catherine Kinnear was a founder member of the Society and later went on to be its president.<ref name=Kinnear>{{cite web|title=MS 103 Kinnear Collection|url=http://arccat.dundee.ac.uk/dserve.exe?&dsqIni=Dserve.ini&dsqApp=Archive&dsqCmd=show.tcl&dsqDb=Catalog&dsqPos=504&dsqSearch=((text)=%27abertay%27)|website=Archive Services Online Catalogue|publisher=University of Dundee|accessdate=22 December 2015}}</ref>

==Activities==

The AHS has been described as "one of Scotland's most successful local history organisations".<ref name="SGE Lythe"/> It organises a regular programme of evening lectures, which are usually held in Discovery Point, Dundee, although other venues are also used from time to time. These lectures cover a wide range of topics relating to the local area.<ref name="Abertay home"/><ref name="About us"/><ref name="Contact us">{{cite web|title=Contact Us|url=http://www.abertay.org.uk/contact-us.html|publisher=Abertay Historical Society|accessdate=10 February 2012}}</ref> The Society also works with other Dundee-based groups to run the Dundee Afternoon Lectures series. The AHS is a registered charity.<ref name="About us"/>

Some events are held jointly with other local groups. For example, in 2010 the AHS joined with the Perth Strathspey and Reel Society to have an event at the A. K. Bell Library, [[Perth, Scotland|Perth]] entitled "The Fiddle Music of Perth" as part of the Perth 800 celebrations.<ref name="Fiddle Music">{{cite news|title=Perth800 takes a bow|url=http://www.perthshireadvertiser.co.uk/perthshire-news/local-news-perthshire/2010/04/16/perth800-takes-a-bow-73103-26250688/|accessdate=11 February 2012|newspaper=Perthshire Advertiser|date=16 April 2010}}</ref> In 2011 the AHS contributed to the Dundee Science Festival by organising a public lecture on the design and construction of the [[Bell Rock Lighthouse]].<ref name="Science Festival">{{cite web|title=Dundee Science Festival 2011|url=http://www.dundeesciencefestival.org/holding/DundeeScienceFestival.pdf|accessdate=14 February 2012|page=16}}</ref>

The Society is a partner in Great War Dundee, a project commemorating the [[First World War]], and its impact on [[Dundee]] set up to mark the centenary of the conflict.<ref name=GWDabout>{{cite web|title=About|url=http://www.greatwardundee.com/about/|website=Great War Dundee|accessdate=30 September 2015}}</ref><ref name=GWDpart>{{cite web|title=Partner Information|url=http://www.greatwardundee.com/about/partner-information/|website=Great War Dundee|accessdate=30 September 2015}}</ref>

==Office Bearers==

The Society is run by a council whose membership includes the President, General Secretary, other office bearers and various ordinary members.<ref>{{cite book|title=Session LXV  2011-2012 Syllabus of Meetings|year=2011|publisher=Abertay Historical Society}}</ref><ref name=Taysiders>{{cite book|last=Various|title=Ten Taysiders:Forgotten Figures from Dundee, Angus and Perthshire|year=2011|publisher=Abertay Historical Society|location=Dundee|isbn=978-0-900019-48-7|page=84}}</ref> In addition the [[Lord Provost of Dundee]], the Provost of [[Perth and Kinross]], the Provost of [[Angus, Scotland|Angus]], the Principal of the [[University of Dundee]] and the Principal of the [[University of St Andrews]] all serve as Honorary Presidents of the Society.<ref name=Taysiders/>

==Relationship with the University of Dundee==

As noted above the Abertay Historical Society was founded at what was then University College, Dundee by my members of that institution's staff. Since that time the AHS has maintained close links with the institution, which has since 1967 been known as the [[University of Dundee]]. The Principal of the University of Dundee is one five Honorary Presidents of the Society, and many of the Society's members and office-holders, including past presidents, have been University staff and students. Equally many of its publications have been authored by figures connected with the University and the AHS' lecture programme has offers University staff and students the chance to present their research to the local community.<ref name="About us"/><ref name=Archives/><ref name=Taysiders/><ref name=E-ARMMS2>{{cite web|title=Ten Taysiders book launch
|url=http://www.dundee.ac.uk/armms/e-armms02.htm#4|work=E-ARMMS newsletter 2|publisher=University of Dundee|accessdate=10 February 2012}}</ref><ref name=E-ARMMS7>{{cite web|title=Abertay Historical Society|url=http://www.dundee.ac.uk/armms/e-armms07.htm#6|work=E-ARMMS newsletter 7|publisher=University of Dundee|accessdate=10 February 2012}}</ref><ref name=Contact>{{cite journal|title=Ten Taysiders: Forgotten Figures|journal=Contact: the magazine of the University of Dundee|date=April 2011|page=25|url=http://www.dundee.ac.uk/pressoffice/contact/2011/april2011.pdf|accessdate=10 February 2012}}</ref><ref name="Contact April 12">{{cite journal|title=Historical Society Celebrates 65th Anniversary |journal=Contact |date=April 2012 |page=26 |url=http://www.dundee.ac.uk/pressoffice/contact/2012/april2012.pdf |accessdate=30 April 2012 |publisher=[[University of Dundee]] }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

An annual prize is awarded by the AHS to the undergraduate at the University of Dundee who produces the dissertation which makes the most significant contribution to local history.<ref name="About us"/> Some of these dissertations have been expanded and published by the AHS. For example, the AHS publication No. 44 ''Scottish Cowboys and the Dundee Investors'' by Claire E. Swan evolved from a dissertation which won the Abertay History Prize in 2003.<ref name=cowboys>{{cite book|last=Swan|first=Claire E.|title=Scottish Cowboys and the Dundee Investors|year=2004|publisher=Abertay Historical Society|location=Dundee|isbn=0-900019-40-9|page=iii}}</ref>  Julie Danskin, author of the Society's 54th publication is also a past winner of the Abertay History Prize.<ref name="Dundee's Own">{{cite book|last=Danskin|first=Julie|title=A City At War The 4th Black Watch - Dundee's Own|year=2013|publisher=Abertay Historical Society|location=Dundee|isbn=978-0-900019-51-7}}</ref>

The archives of the AHS are held by Archive Services at the University of Dundee.<ref name=Archives/><ref name=E-ARMMS7/>

Currently most of the Society's lectures are held at the University, usually in the Dalhousie Building.<ref name="Abertay home"/><ref>{{cite news|title=Abertay Historical Society delve into textile history|url=http://www.forfardispatch.co.uk/news/local-news/the-abertay-historical-society-delve-into-textile-history-1-2673956|accessdate=5 December 2015|work=Forfar Dispatch|date=2 December 2012}}</ref>

==Publications==

The first publication by the society was ''Dundee and the American Civil War'' which was published in 1953. An editorial note in the publication announced that it was hoped it was to 'be followed by others of similar
character, produced to a uniform size to simplify binding into convenient volumes'.<ref name=Carrie>{{cite book|last1=Carrie|first1=David C.|title=Dundee and the American Civil War|date=1953|publisher=Abertay Historical Society|location=Dundee|page=3|url=http://abertay.org.uk/images/docs/DundeeAmericanCivilWar.pdf|accessdate=24 December 2014}}</ref> It was also stated that while its publications would 'always have a strong local flavour the Society aims to
avoid undue parochialism and seeks to set its studies against the wide backcloth of general historical development'.<ref name=Carrie/>  As of December 2015 the Society has published a further 55 books relating to the history of Tayside and Fife.<ref name=Publications>{{cite web|title=Publications|url=http://www.abertay.org.uk/publications.html|publisher=Abertay Historical Society|accessdate=16 December 2015}}</ref><ref name=Rice>{{cite book|last1=Rice|first1=Catherine|title="All their Good Friends and Neighbours" The story of a vanished hamlet in Angus|date=2014|publisher=Abertay Historical Society|location=Dundee}}</ref> Members of the Society are sent a free copy of each new publication, while other copies are made available for sale either from the Society or via booksellers.<ref name="Abertay home"/> The publications have covered a wide number of topics and have varied in style. While most are monographs some titles have been collections of essays written by various authors. An example of the latter is the 2011 publication ''Ten Taysiders'' which featured biographical essays on ten historic figures from Dundee, Pethshire and Angus, such as [[Mary Ann Baxter]], [[Arthur Kinmond Bell|A.K. Bell]] and [[William Sharpey]] and which was written by various authors including Steve Connelly, Fiona Scharlau, Charles Waterston, Graham Lowe, John MacEwen, John Kerr, William Kenefick and Kenneth Baxter.<ref name="Ten Taysiders"/><ref name=Contact/>

The following books have been published by the AHS:<ref name=Publications/><ref name="Publications list">{{cite web|title=Abertay historical Society Publications|url=http://www.royalhistoricalsociety.org/abertayhistsoc.pdf|publisher=[[Royal Historical Society]]|accessdate=16 February 2012}}</ref><ref name="Three Dundonians">{{cite book|last=Lythe|first=S.G.E.|title=Three Dundonians|year=1968|publisher=Abertay Historical Society|location=Dundee|pages=24|url=http://www.abertay.org.uk/images/docs/three_dundonians.pdf|display-authors=etal}}</ref>

* No 1. Carrie, D.C., Dundee and the American Civil War, 1861-65 (1953)
* No 2. Turner, W.H.K., The Textile Industry of Arbroath since the early 18th century (1954)
* No 3. Walker, D.M., Architects and Architecture in Dundee, 1770-1914 (1955)
* No 4. Urquhart, E.A., Castle Huntly: its Development and History (1956)
* No 5. Lythe, S.G.E., Life and Labour in Dundee from the Reformation to the Civil War (1958)
* No 6. Carnie, R.H., Publishing in Perth before 1807(1960)
* No 7. Baxter, J.H., Dundee and the Reformation (1960)
* No 8. McNeill, W.A., Montrose before 1700: from original documents(1961)
* No 9. [[Francis Mudie|Mudie, Sir Francis]] & Walker, D. M., Mains Castle and the Grahams of Fintry (1964)
* No 10. Lythe, S.G.E., Courlays of Dundee: the Rise and Fall of a Scottish Shipbuilding Firm (1964)
* No 11. Wilson, E.M. (ed.) Aspects of antiquity  a miscellany by members of the Archaeological Section of the Abertay Historical Society (1966)<ref name="Aspects of antiquity">{{cite web|title=Aspects of antiquity  a miscellany by members of the Archaeological Section of the Abertay Historical Society |url=http://library.dundee.ac.uk/F/YPH2MDJFG8LPL3TLG4C922S6D8N5UN5SPPX65Y8VSSGFY7ELHS-25243?func=full-set-set&set_number=842754&set_entry=000045&format=999|work=University of Dundee Library Catalogue|publisher=University of Dundee|accessdate=16 February 2012}}</ref>
* No 12. Doughty, D.W., The Tullis Press, Cupar, 1803-49 (1967)
* No 13. Lythe, S.G.E., Ward, J.T. & Southgate, D.G., Three Dundonians (1968)
* No 14. Lenman, B., Lythe, C. & Gauldie, E.  Dundee and its Textile Industry, 1850-1914(1969)
* No 15. Mudie, Sir Francis, Walker, D. M. & McIvor, I., Broughty Castle and the Defence of the Tay (1970)
* No 16. Duncan, A.A.M. (ed.), Scots Antiquaries and Historians: Papers read at the Silver Jubilee Conference of the Abertay Historical Society, 15 April 1972 (1972)
* No 17. Carstairs, A. M. The Tayside Industrial population 1911-1951 (1974)<ref name="Tayside industrial population">{{cite web|title=The Tayside industrial population|url=http://library.dundee.ac.uk/F/YPH2MDJFG8LPL3TLG4C922S6D8N5UN5SPPX65Y8VSSGFY7ELHS-21008?func=full-set-set&set_number=842754&set_entry=000037&format=999|work=University of Dundee Library Catalogue|publisher=University of Dundee|accessdate=16 February 2012}}</ref>
* No 18. Walker, D.M., Architects and Architecture in Dundee, 1770-1914 (1977) - a reprint and expansion of publication number 3<ref name="Walker 77">{{cite web|title=Architects and Architecture in Dundee|url=http://library.dundee.ac.uk/F/YPH2MDJFG8LPL3TLG4C922S6D8N5UN5SPPX65Y8VSSGFY7ELHS-23850?func=full-set-set&set_number=842754&set_entry=000036&format=999|work=University of Dundee Library Catalogue|publisher=University of Dundee|accessdate=16 February 2012}}</ref>
* No 19. Smith, J.V., The Watt Institution, Dundee, 1824-49 (1978)
* No 20. Brown, W., Early Days in a Dundee Mill, 1819-1823: Extracts from the Diary of William Brown, an Early Dundee Spinner, edited by John Hume (1978)
* No 21. [[Archibald Richard Burdon Haldane|Haldane, A.R.B.]], The Great Fishmonger of the Tay: John Richardson of Perth & Pitfour 1760-1821  (1981)
* No 22. [[Christopher Whatley|Whatley, C.A.]], That Important and Necessary Article: the Salt Industry and its Trade in Fife and Tayside c. 1570-1850 (1984)
* No 23. Hay, E. R., Focus on fishing  Arbroath & Gourdon (1985)<ref name="Focus on fishing">{{cite web|title=The Focus on fishing  Arbroath & Gourdon |url=http://library.dundee.ac.uk/F/YPH2MDJFG8LPL3TLG4C922S6D8N5UN5SPPX65Y8VSSGFY7ELHS-19122?func=full-set-set&set_number=842754&set_entry=000031&format=999|work=University of Dundee Library Catalogue|publisher=University of Dundee|accessdate=16 February 2012}}</ref>
* No 24. Dingwall, C.H., Ardler: a Village History: the Planned Railway Village of Washington (1985)
* No 25. [[Charles W. J. Withers|Withers, C.W.J.]], Highland Communities in Dundee and Perth 1787-1891: a Study in the Social History of Migrant Highlanders (1986)
* No 26. Smith, A.M., The Three United Trades of Dundee: Masons, Wrights and Slaters (1987)
* No 27. Cameron, K.J., The Schoolmaster Engineer: Adam Anderson of Perth and St Andrews, c.1780-1846 (1988)<ref name="Cameron 1988">{{cite web|title=The Schoolmaster Engineer: Adam Anderson of Perth and St Andrews|url=http://library.dundee.ac.uk/F/YPH2MDJFG8LPL3TLG4C922S6D8N5UN5SPPX65Y8VSSGFY7ELHS-17658?func=full-set-set&set_number=842754&set_entry=000026&format=999|work=University of Dundee Library Catalogue|publisher=University of Dundee|accessdate=16 February 2012}}</ref>
* No 28. Gauldie, E., One Artful and Ambitious Individual: Alexander Riddoch (1989)
* No 29. Taylor, D.B., Circular Homesteads in North West Perthshire (1990)<ref name="Circular homesteads in north west Perthshire">{{cite web|title=Circular homesteads in north west Perthshire|url=http://library.dundee.ac.uk/F/YPH2MDJFG8LPL3TLG4C922S6D8N5UN5SPPX65Y8VSSGFY7ELHS-16524?func=full-set-set&set_number=842754&set_entry=000023&format=999|work=University of Dundee Library Catalogue|publisher=University of Dundee|accessdate=16 February 2012}}</ref>
* No 30. Torrie, E.P.D., Medieval Dundee: a Town and its People (1990)
* No 31. Jackson, G., Kinnear, K., The Trade and Shipping of Dundee, 1780-1850 (1991)
* No 32. Whatley, C.A., (ed.), The Remaking of Juteopolis: Dundee 1891-1991 (1992)
* No 33. [[Leah Leneman|Leneman, L.]], Martyrs in our Midst: Dundee, Perth and the Forcible Feeding of Suffragettes (1993)
* No 34. McCraw, I., The Fairs of Dundee (1994)
* No 35. Smith, A.M., The Nine Trades of Dundee(1995)
* No 36. Robertson, S., Young, P., Daughter of Atholl: [[Lady Evelyn Stewart Murray]], 1868-1940 (1996)
* No 37. St. John, M., The Demands of the People: Dundee Radicalism 1850-1870 (1997)
* No 38. Mathew, W.M., Keiller’s of Dundee: The Rise of the Marmalade Dynasty 1800-1879 (1998)
* No 39. Walsh, L., Patrons, Poverty and Profit: Organized Charity in Nineteenth-century Dundee (2000)
* No 40. Howe, S., [[William Low|William Low & Co.]]: a Family Business History (2000)
* No 41. McCraw, I., Victorian Dundee at Worship (2002)
* No 42. [[Andrew Murray Scott|Scott, A.M.]], Dundee’s Literary Lives: Vol. 1: Fifteenth to Nineteenth Century (2003)
* No 43. Scott, A.M., Dundee’s Literary Lives: Vol. 2: Twentieth Century(2003)
* No 44. Swan, C.E., Scottish Cowboys and the Dundee Investors (2004)
* No 45. Smith, A.M., The Guildry of Dundee: a History of the Merchant Guild of Dundee up to the 19th Century (2005)
* No 46. Verschuur, M., A Noble and Potent Lady: Katherine Campbell, Countess of Crawford (2006)
* No 47. Cameron, K.J., The Schoolmaster Engineer: Adam Anderson of Perth and St Andrews, c.1780-1846 (2007)
* No 48. Browne, S.F., Making the Vote Count: the Arbroath Women Citizens Association, 1931-1945 (2007)
* No 49. Petrie, A., The 1915 Rent Strikes: an East Coast Perspective (2008)
* No 50. Mudie, Sir Francis, Walker, D. M., McIvor, I., Broughty Castle and the Defence of the Tay (2010)
* No 51. Various, Ten Taysiders: Forgotten Figures from Dundee, Angus and Perthshire (with an introduction by [[Billy Kay (writer)|Billy Kay]]) (2011)
* No 52. Keracher, S., Dundee’s Two Intrepid Ladies: A Tour Round the World by [[David Coupar Thomson|D C Thomson’s]] Female Journalists in 1894 (2012)
* No 53. Davidson, F., [[Glen Clova]] through the Ages: a Short Guide to the History of an Angus Glen (2013)
* No 54. Danskin, J. S., A City at War The 4th [[Black Watch]] Dundee's Own (2013)
* No 55. Rice, Catherine, "All their Good Friends and Neighbours" The story of a vanished hamlet in Angus (2014)<ref name=Rice/>
* No 56. Jarron, Matthew, "Independent & Individualist" Art in Dundee 1867-1294 (2015)<ref name=Publications/>
* No 57. Dundas, Jean, & Orr, David, (eds.), "Quite Happy" The Diary of James Fyffe, Cattle Dealer 1836-1840 (2016)<ref name="Quitehappy">{{cite book|editor1-last=Dundas|editor1-first=Jean|editor2-last=Orr|editor2-first=David|title="Quite Happy" The Diary of James Fyffe, Cattle Dealer 1836-1840|date=2016|publisher=Abertay Historical Society|location=Dundee|isbn=978-0-900019-57-9}}</ref> 

The Society has undertaken a programme of making digital versions of some of its out of print publications available for free download from its website.<ref name=Downloads>{{cite web|title=Downloads|url=http://abertay.org.uk/downloads.html|publisher=Abertay Historical Society|accessdate=7 March 2015}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.abertay.org.uk/ Abertay Historical Society official website]
* [http://www.abertay.org.uk/downloads.html Abertay Historical Society publications available for free download]
* [https://web.archive.org/web/20150709015506/http://arccat.dundee.ac.uk/dserve.exe?&dsqIni=Dserve.ini&dsqApp=Archive&dsqCmd=show.tcl&dsqDb=Catalog&dsqPos=0&dsqSearch=((text)=%27ms%2021%27) Entry for the Abertay Historical Society at the University of Dundee Archive Services Online Catalogue]
*[http://twitter.com/AbertayHS Abertay Historical Society on [[Twitter]]]
*[http://www.facebook.com/AbertayHistoricalSociety Abertay Historical Society on [[Facebook]]]

{{Dundee}}

[[Category:Archaeology of the United Kingdom]]
[[Category:Archaeological organisations]]
[[Category:Historical societies of the United Kingdom]]
[[Category:Text societies]]
[[Category:Organizations established in 1947]]
[[Category:University of Dundee]]
[[Category:1947 establishments in Scotland]]
[[Category:Organisations based in Dundee]]
[[Category:Archaeology of Scotland]]
[[Category:Charities based in Scotland]]
[[Category:Culture in Dundee]]
[[Category:Clubs and societies in Dundee]]