{{Accounting}}
'''Generally Accepted Accounting Practice''' in the UK, or '''UK GAAP''', is the overall body of regulation establishing how company accounts must be prepared in the [[United Kingdom]].  This includes not only accounting standards, but also UK company law.

'''Generally accepted accounting practice''' is a [[statute|statutory]] term in the UK Taxes Acts.<ref>[http://webarchive.nationalarchives.gov.uk/20140109143644/http://www.hmrc.gov.uk/practitioners/clause50.htm Generally accepted accounting practice] defined on the [[HMRC]] website, as archived by the [[National Archive]]. Retrieved 27 January 2015.</ref> The abbreviation "UK GAAP" is also accepted as an abbreviation for the term used in other jurisdictions, '''Generally Accepted Accounting Principles''', or Generally Accepted Accounting Policies.<ref>[http://www.icaew.com/index.cfm/route/156255/icaew_ga/en/technical_and_business_topics/guides_and_publications/knowledge_guides/em_uk_accounting_standards_em#gaap Knowledge guide to UK Accounting Standards] on [[ICAEW]] website. Retrieved 6 December 2010</ref>

== History ==
Accounting standards derive from a number of sources.  The chief standard-setter is the [[Accounting Standards Board]] (ASB), which issues standards called ''Financial Reporting Standards'' (FRS).  The ASB is part of the [[Financial Reporting Council]], an independent regulator funded by a levy on listed companies,<ref>[http://www.frc.org.uk/about/funding.cfm Funding] on FRC website. Retrieved 6 December 2010</ref> and it replaced the Accounting Standards Committee (ASC), which was disbanded in 1990 following a number of criticisms of its work.  To the extent that the ASC's pronouncements, known as ''Statements of Standard Accounting Practice'' (SSAPs), have not been replaced by FRS, they remain in force.<ref>[http://www.icaew.com/index.cfm/route/156373/icaew_ga/en/Library/Links/Accounting_standards/UK/Sources_for_SSAPs Sources for SSAPs] on [[ICAEW]] website. Retrieved 6 December 2010</ref>

== Process for setting standards==
The ASB has a formal exposure process for proposed standards.  Early concepts are issued as ''Discussion Papers''.  These are released to the public and comments invited.  Where a new standard is to be proposed, a ''Financial Reporting Exposure Draft'' (FRED) is released for comment.  The standard in final form is only issued when comments have been incorporated or addressed.  This aims to address the criticisms levelled at the ASC, whose comment process was less rigorous.

Issues that require an immediate solution are considered by the ''Urgent Issues Task Force'' (UITF).  The UITF comprises a number of senior figures from industry and accounting firms.  It meets as necessary to consider pressing issues and issues ''Abstracts'' which become binding immediately.

== Legislation ==
The principal legislation governing reporting in the UK is laid down in the [[Companies Act 2006]], which incorporates the requirements of [[European law]].  The Companies Act sets out certain minimum reporting requirements for companies and, for example, requires limited companies to file their accounts with the [[Registrar of Companies]] who makes them available to the general public.

From 2005, this framework changed as a result of [[European law]] requiring that all listed [[Europe]]an companies report under [[International Financial Reporting Standards]] (IFRSs). In the UK, companies which are not listed have the option to report either under IFRSs or under UK GAAP.<ref>{{cite web |url=http://www.dti.gov.uk/bbf/financial-reporting/standards/page21457.html |title=International Standards (Accounting) |accessdate= 2010-12-06 |author= |date= |work= |publisher= [[Department of Trade and Industry (United Kingdom)|DTI]] |archiveurl=http://webarchive.nationalarchives.gov.uk/20070807130010/http://www.dti.gov.uk/bbf/financial-reporting/standards/page21457.html |archivedate=2007-08-07 }} </ref> Recently issued UK FRSs have, in any case replicated the wording of corresponding IFRSs, reducing the differences between the two sets of standards significantly. 

== New UK GAAP ==
A new financial reporting framework in the UK will be effective on 1 January 2015.

The UK's Financial Reporting Council (FRC) has published five standards which together form the basis of the new UK regime. The Financial Reporting Standard for Smaller Entities will continue to be available for those that qualify to use it and will remain fundamentally unaltered for the time being.

In March 2013 the [[Financial Reporting Council|FRC]] (now responsible for issuing accounting standards in the UK) issued FRS 102, The Financial Reporting Standard Applicable in the UK and [[Republic of Ireland]]. This followed the issue of FRS 100 Application of Financial Reporting Requirements and FRS 101 The Reduced Disclosure Framework in November 2012. Together these standards make up what is commonly being referred to by accountants as new UK GAAP, which takes mandatory effect for accounting periods commencing on or after 1 January 2015.<ref>{{cite web|url=http://www.newukgaap.co.uk/what-is-new-uk-gaap/impact-of-new-uk-gaap |title=International Standards (Accounting) |accessdate=2013-06-20 |author=Julia Penny |date= |work= |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20130622055244/http://www.newukgaap.co.uk:80/what-is-new-uk-gaap/impact-of-new-uk-gaap |archivedate=2013-06-22 |df= }} </ref>

FRS 102 replaces almost 3000 pages of current UK and Ireland GAAP with just over 300. The main purpose is to make reporting requirements proportionate to the size of the entity, and it also includes changes to disclosure, measurement, and recognition.<ref>{{cite web|url=http://sage-exchange.co.uk/news/new-uk-gaap-frs102 |title=New UK GAAP: FRS 102 |author=Sage UK |accessdate=2013-06-20}}</ref>

==See also==
*[[Generally Accepted Accounting Principles]]

==References==
{{Reflist}}

[[Category:Accounting in the United Kingdom]]
[[Category:Accounting principles by country|United Kingdom]]

[[he:עקרונות חשבונאיים מקובלים]]