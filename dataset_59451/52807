{{incomplete|date=January 2014}}
{{infobox bibliographic database
| title = Science Citation Index
| image = 
| caption = 
| producer = [[Thomson Reuters]]
| country = United States
| history = 2000-present
| languages = 
| providers = 
| cost = 
| disciplines = Science, medicine, and technology
| depth = 
| formats = 
| temporal = 
| geospatial = 
| number = 

| updates = 
| p_title = 
| p_dates = 
| ISSN = 0036-827X
| web = http://ip-science.thomsonreuters.com/cgi-bin/jrnlst/jloptions.cgi?PC=D
| titles = 
}}
The '''Science Citation Index''' ('''SCI''') is a [[citation index]] originally produced by the [[Institute for Scientific Information]] (ISI) and created by [[Eugene Garfield]]. It was officially launched in 1964. It is now owned by [[Clarivate Analytics]] (previously the Intellectual Property and Science business of [[Thomson Reuters]]).<ref name=dimension>
{{cite journal
|doi=10.1126/science.122.3159.108
|title=Citation Indexes for Science: A New Dimension in Documentation through Association of Ideas
|url=http://ije.oxfordjournals.org/content/35/5/1123.full
|format=Free web article download
|year=1955
|last1=Garfield
|first1=E.
|journal=Science
|volume=122
|issue=3159
|pages=108–11
|pmid=14385826|bibcode=1955Sci...122..108G
}}</ref><ref name=evolve>
{{cite journal
 |last = Garfield 
 |first = Eugene
 |doi=10.2436/20.1501.01.10
 |url=http://garfield.library.upenn.edu/papers/barcelona2007a.pdf
 |format=Free PDF download
 |title=The evolution of the Science Citation Index|doi-broken-date = 2017-01-16
 }} International microbiology '''10.'''1 (2010): 65-69.</ref><ref name=gOverview>
{{cite web
 | last = Garfield 
 | first = Eugene
 | authorlink =
 | coauthors =
 | title = Science Citation Index
 | work = Science Citation Index 1961
 | publisher = Garfield Library - UPenn
 | date = 1963
 | url = http://garfield.library.upenn.edu/papers/80.pdf
 | format = Free PDF download
 | doi =
 | accessdate = 2013-05-27}} 
* Originally published by the Institute of Scientific Information in 1964
* Other titles in this document are: What is a Citation Index? , How is the Citation Index Prepared? , How is the Citation Index Used? , Applications of the Science Citation Index , Source Coverage and Statistics , and a Glossary.</ref><ref name=history-cite-indexing>
{{cite web
 | title =History of Citation Indexing 
 | work =Needs of researchers create demand for citation indexing 
 | publisher =Thomson Ruters 
 | date =November 2010 
 | url =http://thomsonreuters.com/products_services/science/free/essays/history_of_citation_indexing/ 
 | format =Free HTML download 
 | accessdate =2010-11-04}}</ref> The larger version ('''Science Citation Index Expanded''') covers more than 8,500 notable and significant [[Scientific journal|journals]], across 150 disciplines, from 1900 to the present. These are alternatively described as the world's leading journals of [[science]] and [[technology]], because of a rigorous selection process.<ref name=Expanded>
{{cite web 
|url=https://www.thomsonreuters.com/en/products-services/scholarly-scientific-research/scholarly-search-and-discovery/science-citation-index-expanded.html 
|title=Science Citation Index Expanded 
|work= |accessdate=2017-01-17}}</ref><ref name=wetland>{{cite journal| doi= 10.1007/s12665-012-2193-y|title= The Top-cited Wetland Articles in Science Citation Index Expanded: characteristics and hotspots|url=http://dns2.asia.edu.tw/~ysho/YSHO-English/Publications/PDF/Env%20Ear%20Sci-Ma.pdf|date= December 2012| last1= Ma| first1= Jiupeng| last2= Fu| first2= Hui-Zhen| last3= Ho| first3= Yuh-Shan| journal= Environmental Earth Sciences|volume= 70|issue= 3|pages= 1039}} (Springer-Verlag)</ref><ref name=shan>
{{cite journal 
| doi= 10.1007/s11192-012-0837-z 
|title= The top-cited research works in the Science Citation Index Expanded 
|url= http://trend.asia.edu.tw/Publications/PDF/Scientometrics94,%201297.pdf 
| year= 2012 
| last1= Ho 
| first1= Yuh-Shan 
| journal= Scientometrics 
| volume= 94 
| issue= 3 
| page= 1297}}</ref>

The index is made available online through different platforms, such as the [[Web of Science]]<ref name=AtoZ>{{cite web |last=ISI Web of Knowledge platform |title =Available databases A to Z |publisher=Thomson Reuters |year=2010 |url=http://wokinfo.com/products_tools/products/ |format=Choose databases on method of discovery and analysis |accessdate=2010-06-24}}</ref><ref>[http://wokinfo.com/media/pdf/SSR1103443WoK5-2_web3.pdf Thomson Reuters Web of Knowledge. Thomson Reuters, 2013.]</ref> and SciSearch.<ref>{{cite web |url=http://library.dialog.com/bluesheets/html/bl0034.html |title=SCISEARCH - A CITED REFERENCE SCIENCE DATABASE |publisher=Library.dialog.com |date= |accessdate=2014-04-17}}</ref> (There are also CD and printed editions, covering a smaller number of journals). This database allows a researcher to identify which later articles have cited any particular earlier article, or have cited the articles of any particular author, or have been cited most frequently. Thomson Reuters also markets several subsets of this database, termed "Specialty Citation Indexes",<ref name=SpCI>
{{cite web 
|url=http://thomsonreuters.com/products_services/science/science_products/a-z/specialty_citation_indexes/ 
|title=Specialty Citation Indexes 
|work= |accessdate=2009-08-30}}</ref> 
such as the '''Neuroscience Citation Index'''<ref name=NCI>
{{cite web 
|url=http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MD 
|title=Journal Search - Science - |work= |accessdate=2009-08-30}}</ref> and the '''Chemistry Citation Index'''.<ref>{{cite web |url=http://science.thomsonreuters.com/cgi-bin/jrnlst/jloptions.cgi?PC=CD 
|title=Journal Search - Science - Thomson Reuters |accessdate=14 January 2011}}</ref>

==Chemistry Citation Index==
The Chemistry Citation Index was first introduced by Eugene Garfield, a chemist by training. His original "search examples were based on [his] experience as a chemist".<ref name=Garcci/> In 1992 an electronic and print form of the index was derived from a core of 330 chemistry journals, within which all areas were covered. Additional information was provided from articles selected from 4,000 other journals. All chemistry subdisciplines were covered: organic, inorganic, analytical, physical chemistry, polymer, computational, organometallic, materials chemistry, and electrochemistry.<ref name=Garcci>Garfield, Eugene. "[http://garfield.library.upenn.edu/essays/v15p007y1992-93.pdf New Chemistry Citation Index On CD-ROM Comes With Abstracts, Related Records, and Key-Words-Plus]." Current Contents 3 (1992): 5-9.</ref>

By 2002 the core journal coverage increased to 500 and related article coverage increased to 8,000 other journals.<ref>
[http://www.chinweb.com/cgi-bin/chemport/getfiler.cgi?ID=k4l7vyYF5FimYvScsOm3pxWVmEhBoH0ZuYgxjLdKBfqdmURDHLrjuVv78i16JLPX&VER=E Chemistry Citation Index]. Institute of Process Engineering of the Chinese Academy of Sciences. 2003.</ref> 

One 1980 study reported the overall citation indexing benefits for chemistry, examining the use of citations as a tool for the study of the sociology of chemistry and illustrating the use of citation data to "observe" chemistry subfields over time.<ref>
{{cite journal
|doi=10.1007/BF02016348
|title=Science citation index and chemistry
|year=1980
|last1=Dewitt
|first1=T. W.
|last2=Nicholson
|first2=R. S.
|last3=Wilson
|first3=M. K.
|journal=Scientometrics
|volume=2
|issue=4
|page=265}}</ref>

==See also==
* [[Arts and Humanities Citation Index]], which covers 1130 journals, beginning with 1975.
* [[Impact factor]]
* [[List of academic databases and search engines]]
* [[Social Sciences Citation Index]], which covers 1700 journals, beginning with 1956.

==References==
{{Reflist|30em}}

==Further reading==
*{{cite journal
|doi= 10.1002/aris.1440360102
|url= http://polaris.gseis.ucla.edu/cborgman/pubs/borgmanfurnerarist2002.pdf
|title=Scholarly Communication and Bibliometrics
|year= 2005
|last1= Borgman
|first1= Christine L.
|last2= Furner
|first2= Jonathan
|journal= Annual Review of Information Science and Technology
|volume= 36
|issue= 1 
|pages=3–72}}

*{{cite journal
|doi= 10.1002/asi.20677
|url= http://staff.aub.edu.lb/~lmeho/meho-yang-impact-of-data-sources.pdf
|format= Free PDF download
|title= Impact of data sources on citation counts and rankings of LIS faculty: Web of science versus scopus and google scholar
|year= 2007
|last1= Meho
|first1= Lokman I.
|last2= Yang
|first2= Kiduk
|journal= Journal of the American Society for Information Science and Technology
|volume= 58
|issue= 13
|page= 2105}}

*{{cite journal
|doi= 10.1002/asi.5090140304
|url= http://www.garfield.library.upenn.edu/essays/v6p492y1983.pdf
|format= Free PDF download
|title= New factors in the evaluation of scientific literature through citation indexing
|year= 1963
|last1= Garfield
|first1= E.
|last2= Sher
|first2= I. H.
|journal= American Documentation
|volume= 14
|issue= 3
|page= 195}}

*{{cite journal
|doi= 10.1038/227669a0
|url= http://www.garfield.library.upenn.edu/essays/V1p133y1962-73.pdf
|format= Free PDF download
|title= Citation Indexing for Studying Science
|year= 1970
|last1= Garfield
|first1= E.
|journal= Nature
|volume= 227
|issue= 5259
|pages= 669–71
|pmid= 4914589|bibcode= 1970Natur.227..669G
}}

*{{cite book
 | last =Garfield
 | first =Eugene 
 | authorlink =
 | title =Citation Indexing: Its Theory and Application in Science, Technology, and Humanities
 | publisher =Wiley-Interscience
 | series = Information Sciences Series
 | edition = 1st
 | origyear = 1979| year = 1983
 | location = New York
 | isbn =9780894950247}}

==External links==
* [http://scientific.thomson.com/products/wos/ Introduction to SCI]
* [http://science.thomsonreuters.com/mjl/ Master journal list]
* [https://en.wikibooks.org/wiki/Chemical_Information_Sources/Author_and_Citation_Searches Chemical Information Sources/ Author and Citation Searches]. on WikiBooks. 
* [http://scientific.thomson.com/tutorials/citedreference/crs1.htm Cited Reference Searching: An Introduction]. Thomson Reuters. 
* [http://www.chinweb.com/cgi-bin/chemport/getfiler.cgi?ID=k4l7vyYF5FimYvScsOm3pxWVmEhBoH0ZuYgxjLdKBfqdmURDHLrjuVv78i16JLPX&VER=E Chemistry Citation Index]. Chinweb.

{{Thomson Reuters}}

[[Category:Citation indices]]
[[Category:Online databases]]
[[Category:Thomson Reuters]]