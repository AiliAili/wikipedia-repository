{{Infobox airport
| name         = Voss Airport, Bømoen
| nativename   = {{smaller|Voss flyplass, Bømoen}}
| image        = Enbm from air summer2006.jpg
| IATA         = 
| ICAO         = ENBM
| type         = Public
| owner        = 
| operator     = 
| city-served  = [[Voss]], [[Norway]]
| location     = Bømoen, Voss
| elevation-f  = 300
| elevation-m  = 91
| metric-elev  = y
| coordinates  = {{coord|60|38|19|N|006|30|05|E|region:NO_type:airport}}
| latd =  60 | latm = 38 | lats = 19 | latNS = N
| longd= 006 | longm= 30 | longs= 05 | longEW= E
| coordinates_type       =
| coordinates_region     = NO
| coordinates_notitle    =
| pushpin_map            = Norway
| pushpin_relief         = yes
| pushpin_label_position =
| pushpin_label          = '''ENBM'''
| pushpin_map_alt        =
| pushpin_mapsize        = 300
| pushpin_image          =
| pushpin_map_caption    = Location in Norway
| website      = 
| metric-rwy   = y
| r1-number    = 09/27
| r1-length-m  = 1,000
| r1-length-f  = 3,281
| r1-surface   = Asphalt
| footnotes    = 
}}

'''Voss Airport, Bømoen''' ({{lang-no|Voss flyplass, Bømoen}}) {{airport codes||ENBM}} is a [[general aviation]] [[airport]] located on the closed [[Bømoen Base]] in [[Voss]], [[Norway]]. The airport consists of an asphalt {{convert|1000|m|sp=us|adj=on}} [[runway]] designated 09/27. The municipal airport is used by the helicopter operator [[Fonnafly]] as well as for [[hanggliding]], [[parachuting]] and [[sailplane]] activities.

The airport was built by the [[Norwegian Army]] in 1935. It was taken over by the [[Luftwaffe]] during [[World War II]], resulting in a new and longer runway being built. Since 1958 there has been civilian aviation at the airport. The annual [[Ekstremsportveko|Extreme Sport Festival]] has used the airport since 1998.

==History==
Bømoen Base was established by the Norwegian Army in 1899, replacing older bases in [[Vik]] and [[Lærdal]]. The [[Bergen Line]] was being constructed and by placing a base on the railway it would be possible to have quick access to [[Eastern Norway]]. This was considered important at the time because the main threat of war at the time was with Sweden.<ref>Hjelmeland: 108</ref> The airport was established in 1935,<ref>Gjerdåker: 69</ref> when the Norwegian Army Air Service built a small airfield, aligned southwest–northeast.<ref name=h109>Hjelmeland: 109</ref>

Bømoen was taken over by Luftwaffe with the [[German occupation of Norway]] on 9 April 1940. Work with expanding the airport commenced in May, and was conducted based on the need for defending [[Bergen]]. By 1941 work on the airport was stopped, as the [[Wehrmacht]] decided to instead build [[Herdla Airport]]. By then Bømoen had received a {{convert|1000|by|50|m|sp=us|adj=on}} runway with sections in concrete and others in wood.<ref name=h315>Hafsten: 315</ref> This work involved moving the runway and giving it an east–west alignment.<ref name=h109 /> The airport proved to have poor landing conditions and was after the opening of Herdla in diminutive use. No airborne units were ever stationed at Bømoen.<ref name=h315 />

Voss Flyklubb was established in 1958 with its home at Bømoen.<ref name=flyklubb>{{cite web |url=http://www.vossflyklubb.com/index.php?option=com_content&view=article&id=9&Itemid=16 |title=Om Voss Flykubb |publisher=Voss Flyklubb |date=28 December 2008 |language=Norwegian |accessdate=23 January 2015}}</ref> In 1960 it organized its first sailplane gathering there.<ref>Skorpen: 61</ref> Bømoen soon developed into an important center for sailplane flights, and attracted participants from throughout Hordaland and [[Valdres]].<ref>Skorpen: 62</ref> The high interest for sailplanes lasted through the 1960s. Thereafter [[Os Airport, Vaksinen]] reopened and the local recruitment stagnated.<ref>Skorpen: 65</ref> From the 1970s Os Aero Club took over the gatherings, organizing two annual gatherings, in Eastern and during the summer. These constituted the club's main training activities.<ref>Skorpen: 96</ref> The first edition of the Extreme Sport Week took place in 1998.<ref>{{cite web |url=http://ekstremsportveko.com/about/history/ |title=History |publisher=[[Ekstremsportveko|Extreme Sport Festival]] |accessdate=23 January 2015}}</ref>

The [[Norwegian Defence Estates Agency]] sold Bømoen Base in 2013 to the company Bømoen AS. During the negotiations the municipality attempted to secure purchasing only the airport, but no the rest of the base.<ref>{{cite news |title=Forsvarsdepartementet slår retrett på Bømoen |last=Tepstad |first=Rolf |work=[[Hordaland (newspaper)|Hordaland]] |date=13 March 2013 |page=3 |language=Norwegian}}</ref> At the time the municipality had an agreement which secured a lease until 2023. An agreement was struck whereby the municipality was granted a land lease until 2073 for {{convert|7.5|ha}} consisting of the runway, taxiways and the apron.<ref>{{cite news |title=Kommunen skal leiga, Bømoen AS eiga |last=Tepstad |first=Rolf |work=[[Hordaland (newspaper)|Hordaland]] |date=7 May 2013 |pages=2–3 |language=Norwegian}}</ref>

==Facilities==
Voss Airport, Bømoen is located on the premises of the Bømoen Base, about {{convert|4|km|sp=us}} east of [[Vossevangen]]. The aerodrome consists of an asphalt runway measuring {{convert|1000|by|30|m|sp=us}} and aligned 09/27 (east–west). The airport has a reference elevation of {{convert|91|m|sp=us}} [[above mean sea level]]. Air navigation consists of two [[VOR/DME]] beacons.<ref name=kommune>{{cite web |url=http://www.voss.kommune.no/artikkel.aspx?AId=1440&back=1&MId1=1&MId2=765 |title=Voss flyplass – Bømoen |publisher=Voss Municipality |accessdate=23 January 2015 |language=Norwegian}}</ref> Skydive Voss has a {{convert|800|m2|sp=us|adj=on}} club house at the airport, which in addition to course and technical facilities has accommodation for eighteen people.<ref name=skydive>{{cite web |url=http://www.skydivevoss.no/medlem/index.aspx?cat=144412&id=210974 |title=Klubbhuset |publisher=Skydive Voss |date=25 October 2014 |accessdate=23 January 2015 |language=Norwegian}}</ref>

[[Fonnafly]] has a base and hangar at Bømoen. The helicopter operates has an [[Eurocopter AS350]] stationed at the airport.<ref>{{cite web |url=http://www.fonnafly.no/nb/sted/voss-bomoen-47-88-00-13-13 |title=Voss, Bømoen |publisher=[[Fonnafly]] |accessdate=23 January 2015 |language=Norwegian}}</ref> Clubs based at the airport consist of Skydive Voss,<ref name=skydive /> and the aviation club Voss Flyklubb.<ref name=flyklubb /> Bergen Skydiving Club uses Bømoen during the summer.<ref>{{cite web |url=http://www.bfsk.no/medlem/joomla/index.php?option=com_content&task=view&id=107&Itemid=119 |title=Welcome to Bergen Skydiving Club |publisher=Bergen Skydiving Club |accessdate=23 January 2015}}</ref> Every June the Extreme Sport Festival is organized in Voss. The airport plays a central sole for several of these events, including parachuting and hanggliding.<ref>{{cite web |title=Sports |url=http://www.ekstremsportveko.com/sports |publisher=[[Ekstremsportveko|Extreme Sport Festival]] |accessdate=7 December 2013}}</ref>

The airport is limited to aircraft with a maximum take-off weight of {{convert|5.7|t}} as well as helicopters. Flight is only permitted during daylight with [[visual flight rules]]. Most parachuting and sailplane activity takes place during the period May through September. The runway closes during the winter during periods of snow.<ref name=kommune />

==Accidents and incidents==
A [[Cessna 172]] crashed on 27 June 1987, killing two and seriously injuring two. The accident took place during a failed abortion of an approach, causing the aircraft to crash into the forests near the airport.<ref>{{cite web |title=Flere årsaker til småflystyrt |date=4 March 1989 |page=10 |work=[[Aftenpost]] |agency=[[Norwegian News Agency]] |language=Norwegian}}</ref>

==References==
{{reflist|30em}}

==Bibliography==
{{commons category|Voss Airport, Bømoen}}
* {{cite book |last=Gjerdåker |first=Johannes |title=Levande landskap: Vossebygdene – Granvin – Nærøydalen |publisher=Voss Veksel- og Landmandsbank |location=Voss |year=1999 |isbn=8271282875 |url=http://www.nb.no/nbsok/nb/27e0e9bfee134faea19cd759b4525e72 |language=Norwegian}}
* {{cite book |last=Hafsten |first=Bjørn |last2=Larsstuvold |first2=Ulf |last3=Olsen |first3=Bjørn |last4=Stenersen |first4=Sten |title=Flyalarm: Luftkrigen over Norge 1939–1945 |year=1991 |publisher=Sem & Stenersen |location=Oslo |language=Norwegian |isbn=82-7046-058-3 |url=http://www.nb.no/nbsok/nb/824921f23d8278cf9650ee046c3209f8}}
* {{cite book |last=Hjelmeland |first=Britt-Elise |title=Landsverneplan for Forsvaret : verneplan for eiendommer, bygninger og anlegg : Katalog Sør- og Vestlandet, Trøndelag og Nord-Norge |year=2000 |publisher=Forsvarets bygningstjeneste |location=Oslo |language=Norwegian |url=http://www.nb.no/nbsok/nb/1c25831424cfb067345542ad86aa267a}}
* {{cite book |last=Skorpen |first=Lars |title=Ein flygeidé – OAK 50 år |publisher=Os Aero Klubb|year=1996 |location=Askøy |language=Norwegian}}

{{Airports in Norway}}
{{Portal bar|Aviation|World War II|Norway}}

{{DEFAULTSORT:Voss Airport, Bomoen}}
[[Category:Airports in Hordaland]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Royal Norwegian Air Force airfields]]
[[Category:Voss]]
[[Category:1935 establishments in Norway]]
[[Category:Airports established in 1935]]
[[Category:Military installations in Hordaland]]