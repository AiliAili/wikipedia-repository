{{redirect|MBPS|megabits or megabytes per second|data rate unit}}
The '''British Psychological Society''' is a [[professional body|representative body]] for [[psychologist]]s and [[psychology]] in the [[United Kingdom]]. Founded on 24 October 1901 at [[University College, London]] (UCL) as ''The Psychological Society'', the organisation initially admitted only recognised teachers in the field of psychology.  Its current name of ''The British Psychological Society''  was taken in 1906 to avoid confusion with another group named ''The Psychological Society''. Under the guidance of [[Charles Samuel Myers|Charles Myers]], membership was opened up to members of the medical profession in 1919. In 1941 the society was [[incorporation (business)|incorporated]].

In 2012 the BPS had 49,678 members and subscribers, in all fields of psychology, 18,342 of whom were Chartered Members. Its current President, for 2015-16, is Professor Jamie Hacker Hughes who is visiting professor of military psychological therapies at [[Anglia Ruskin University]], visiting professor of psychology at the [[University of Hertfordshire]] and director of the Veterans and Families Institute at Anglia Ruskin University.<ref>[http://www.bps.org.uk/what-we-do/bps/governance/trustees/trustees]</ref> The Society holds its Annual Conference, usually in May, in a different town or city each year. In recent years it has been held in [[Dublin]] (2008), [[Brighton]] (2009), [[Stratford-upon-Avon]] (2010), [[Glasgow]] (2011), [[London]] (2012), [[Harrogate]] (2013), [[Birmingham]] (2014). In 2015 the conference was held in [[Liverpool]].

The BPS is also a [[Charitable organization|Registered Charity]] and, along with advantages, this also imposes certain constraints on what the Society can and cannot do. For example, it cannot campaign on issues which are seen as party political. The BPS is not the statutory regulation body for Practitioner Psychologists in the UK which is the [[Health and Care Professions Council]].

==Chartership==

Following the receipt of a [[royal charter]] in 1965, the society became the keeper of the Register of Chartered Psychologists. The register was the means by which the Society could regulate the professional practice of psychology. Regulation included the awarding of practising certificates and the conduct of disciplinary proceedings. The register ceased to be when statutory regulation of psychologists began on 1 July 2009. The profession is now regulated by the [[Health and Care Professions Council]].

A member of the British Psychological Society (MBPS) who has achieved chartered status has the right to the letters "C.Psychol." after his or her name. The BPS is also licensed by the Science Council<ref>[http://www.sciencecouncil.org Science Council]</ref> to award Chartered Scientist<ref>[http://www.charteredscientist.org Chartered Scientist]</ref> status. The highest designation the Society can bestow is a [[Fellow (society)|Fellow]] of the British Psychological Society (FBPsS), showing a significant contribution to and understanding of the discipline.

== Mission ==
The Society aims to raise standards of training and practice in psychology, raise public awareness of psychology, and increase the influence of psychology practice in society. Specifically it has a number of key aims, as described below.

* Setting standards of training for psychologists at graduate and undergraduate levels.
* Providing information about psychology to the public.
* Providing support to its members via its membership networks and mandatory continuing professional development.
* Hosting conferences and events.
* Preparing policy statements.
* Publishing books, journals, the monthly magazine ''[[The Psychologist (publication)|The Psychologist]]'', the [http://bps-research-digest.blogspot.co.uk Research Digest] blog, including a free fortnightly research update, and various other publications (see below).
* Setting standards for [[psychological testing]].
* Maintaining a [http://hopc.bps.org.uk History of Psychology Centre].

== Journals ==
{{main cat|British Psychological Society academic journals}}
:''"{{no redirect|British Journal of Psychology}}" redirects here; it has been suggested that it be split into its own page.''
The BPS publishes 11 journals:

* ''[[British Journal of Clinical Psychology]]''
* ''[[British Journal of Developmental Psychology]]''
* ''[[British Journal of Educational Psychology]]''
* ''[[British Journal of Health Psychology]]''
* ''[[British Journal of Mathematical and Statistical Psychology]]''
* ''[[British Journal of Psychology]]''
* ''[[British Journal of Social Psychology]]''
* ''[[Journal of Neuropsychology]]''
* ''[[Journal of Occupational and Organizational Psychology]]''
* ''[[Legal and Criminological Psychology]]''
* ''[[Psychology and Psychotherapy: Theory, Research and Practice]]''

== Founders ==
The following were the ten founder members who first met on 24 October 1901 at  [[University College London]]:<ref>{{cite web|url=http://hopc.bps.org.uk/hopc/histres/bpshistory/founders/founders_home.cfm |title=Founder Members of the BPS |publisher=Hopc.bps.org.uk |date= |accessdate=2012-02-20}}</ref>

* [[Robert Armstrong-Jones]]
* [[Sophie Bryant]]
* [[W.R. Boyce Gibson]]
* [[Frank Noel Hales]]
* [[William McDougall (psychologist)|William McDougall]]
* [[Frederick Walker Mott]]
* [[William Halse Rivers Rivers]]
* [[Alexander Faulkner Shand]]
* [[William George Smith]]
* [[James Sully]]

== Presidents ==
The following have been Presidents of the Society<ref>{{cite web|url=http://hopc.bps.org.uk/histres/bpshistory/presidents$.cfm |title=Presidents of the BPS |publisher=Hopc.bps.org.uk |date= |accessdate=2012-02-20}}</ref>

{{columns-list|colwidth=18em|
* 1920-1923 [[Charles Samuel Myers]]
* 1923-1926 [[Charles Edward Spearman]]
* 1926-1929 [[Francis Aveling]]
* 1929-1932 [[Beatrice Edgell]]
* 1932-1935 [[John Carl Flügel]]
* 1935-1938 [[James Drever (1873)|James Drever Sr.]]
* 1938-1941 [[Albert William Wolters]]
* 1941-1943 [[Cyril Burt]]
* 1943-1944 [[Tom Hatherley Pear]]
* 1944-1945 [[Millais Culpin]]
* 1945-1946 [[Godfrey Thomson]]
* 1946-1947 [[Robert John Bartlett]]
* 1947-1948 [[Charles Wilfred Valentine]]
* 1948-1949 [[Stanley J.F. Philpott]]
* 1949-1950 [[Robert Henry Thouless]]
* 1950-1951 [[Frederic Charles Bartlett]]
* 1951-1952 [[William Brown (psychologist)|William Brown]]
* 1952-1953 [[Cecil Alec Mace]]
* 1953-1954 [[Arthur Rex Knight]]
* 1954-1955 [[Philip E. Vernon]]
* 1955-1956 [[Leslie S. Hearnshaw]]
* 1956-1957 [[Eric Benjamin Strauss]]
* 1957-1958 [[Alec Rodger]]
* 1958-1959 [[Magdalen D. Vernon]]
* 1959-1960 [[Frederick Viggers Smith]]
* 1960-1961 [[James Drever|James Drever Jr.]]
* 1961-1962 [[Edwin A. Peel]]
* 1962-1963 [[George C. Drew]]
* 1963-1964 [[Arthur Summerfield (psychologist)|Arthur Summerfield]]
* 1964-1965 [[Donald Eric Broadbent]]
* 1965-1966 [[George Westby]]
* 1966-1967 [[Grace Rawlings]]
* 1967-1968 [[George Seth]]
* 1968-1969 [[Boris Semeonoff]]
* 1969-1970 [[Robert John Audley]]
* 1970-1971 [[Harry Gwynne Jones]]
* 1971-1972 [[Harry Kay (psychologist)|Harry Kay]]
* 1972-1973 [[Max Hamilton]]
* 1973-1974 [[Brian Malzard Foss]]
* 1974-1975 [[Oliver Louis Zangwill]]
* 1975-1976 [[Jack Tizard]]
* 1976-1977 [[May Alison Davidson]]
* 1977-1978 [[Alan Douglas Benson Clarke]]
* 1978-1979 [[Philip Marcus Levy]]
* 1979-1980 [[Peter H. Venables]]
* 1980-1981 [[Kevin J. Connolly]]
* 1981-1982 [[Derek Ernest Blackman]]
* 1982-1983 [[Ralph R. Hetherington]]
* 1983-1984 [[Halla Beloff]]
* 1984-1985 [[Charles Ian Howarth]]
* 1985-1986 [[Robert Maclaughlin Farr]]
* 1986-1987 [[David Legge]]
* 1987-1988 [[Lea S. Pearson]]
* 1988-1989 [[Anthony John Chapman]]
* 1989-1990 [[Maurice Anthony Gale]]
* 1990-1991 [[Peter Edwin Morris]]
* 1991-1992 [[Fraser Norman Watts]]
* 1992-1993 [[Edgar Miller (psychologist)|Edgar Miller]]
* 1993-1994 [[Ann Mary Colley]]
* 1994-1995 [[Geoffrey Anthony Lindsay]]
* 1995-1996 [[Stephen Edward Newstead]]
* 1996-1997 [[Margaret Valerie McAllister]]
* 1997-1998 [[Christopher Noel Cullen]]
* 1998-1999 [[Ingrid Cecilia Lunt]]
* 1999-2000 [[Patricia Frankish]]
* 2000-2001 [[Tommy MacKay]]
* 2001-2002 [[Vicki Bruce]]
* 2002-2003 [[Graham Davey]]
* 2003-2004 [[Alexander Allan Innis Wedderburn (psychologist)|Alexander (Zander) Wedderburn]]
* 2004-2005 [[Ken Brown (psychologist)|Ken Brown]]
* 2005-2006 [[Graham Powell]]
* 2006-2007 [[Ray Miller (psychologist)|Ray Miller]]
* 2007-2008 [[Pam Maras]]
* 2008-2009 [[Elizabeth Campbell (psychologist)|Elizabeth Campbell]]
* 2009-2010 [[Sue Gardner (psychologist)|Sue Gardner]]
* 2010-2011 [[Gerry Mulhern]]
* 2011-2012 [[Carole Allan]]
* 2012-2013 [[Peter Banister]]
* 2013-2014 [[Richard Mallows]]
* 2014-2015 [[Dorothy Miell]] 
* 2015-2016 [[Jamie Hacker Hughes]]
* 2016-2017 [[Peter Kinderman]]
}}

== Honorary Members and Fellows ==

===Honorary members===
The following were Honorary Members of the Society:<ref>{{cite web|url=http://www.bps.org.uk/what-we-do/bps/history-psychology-centre/history-society/honorary-members/honorary-members|title=Honorary Members of the BPS |publisher=Hopc.bps.org.uk |date= |accessdate=2012-02-20}}</ref>

*1904 [[John Hughlings Jackson]]
*1905 [[Harald Høffding]], [[Sir Francis Galton]], [[William James]], [[Georg Elias Müller]], [[Théodule Armand Ribot]], [[Carl Stumpf]]
*1910 [[James Sully]]
*1911 [[Oswald Külpe]]
*1912 [[Franz Brentano]], [[James Ward (psychologist)|James Ward]]
*1926 [[Edward Claparède]], [[Sigmund Freud]], [[Gerardus Heymans]], [[Pierre Janet]], [[Henri Piéron]], [[Edward Lee Thorndike]], [[Edward Bradford Titchener]], [[Hendrik Zwaardemaker]]
*1927 [[Baron Albert Eduard Michotte van den Berck]]
*1928 [[Mary Whiton Calkins]]
*1932 [[James Rowland Angell]], [[James McKeen Cattell]], [[Sante de Sanctis]], [[William Stern (psychologist)|William Stern]]
*1934 [[Havelock Ellis]], [[Ernest Jones]], [[Felix Krueger]], [[William McDougall (psychologist)|William McDougall]], [[Conwy Lloyd Morgan]], [[Charles Samuel Myers]], [[Alexander Faulkner Shand]], [[Charles Edward Spearman]], [[George Frederick William Stout]]
*1937 [[Samuel Alexander]], [[Henry Head]], [[Charles Scott Sherrington]]
*1940 [[Georges Dumas]], [[Beatrice Edgell]], [[Kurt Koffka]], [[Carl Emil Seashore]]

In 1946 all surviving Honorary Members were made Honorary Fellows.

=== Honorary Fellows ===
The following have been or are still Honorary Fellows of the Society:<ref>{{cite web|url=http://hopc.bps.org.uk/hopc/histres/bpshistory/honfellows.cfm |title=Honorary Fellows of the BPS 1946-1969 |publisher=Hopc.bps.org.uk |date= |accessdate=2012-02-20 |deadurl=yes |archiveurl=https://web.archive.org/web/20150121104015/http://hopc.bps.org.uk/hopc/histres/bpshistory/honfellows.cfm |archivedate=2015-01-21 |df= }}</ref>

{{columns-list|colwidth=30em|
*1946 [[Carl Gustav Jung]], [[William Mitchell (philosopher)|Sir William Mitchell]]
*1950 [[Gordon Willard Allport]], [[Clark Leonard Hull]], [[David Katz (psychologist)|David Katz]], [[Wolfgang Köhler]], [[Karl Spencer Lashley]], [[Gardner Murphy]], [[Lewis Madison Terman]], [[Louis Leon Thurstone]]
*1952 [[Thomas Hunter (psychologist)|Thomas Hunter]]
*1954 [[Edgar Douglas Adrian]], [[Edward Chace Tolman]], [[Robert Sessions Woodworth]], [[Jean Piaget]], [[Edwin Garrigues Boring]], [[Cyril Burt]], [[Frederic Bartlett]], [[Donald Olding Hebb]], [[Ernst Kretschmer]]
*1955 [[Albert William Phillip Wolters]]
*1958 [[May Smith (psychologist)|May Smith]], [[Melanie Klein]], [[Agostino Gemelli]], [[Alexander Luria]], [[Tom Hatherley Pear]], [[Charles Wilfred Valentine]], [[Henry Tasman Lovell]]
*1959 [[Henry Cohen, 1st Baron Cohen of Birkenhead]]
*1960 [[Ernest Hilgard]], Roger Russell
*1961 [[Russell Brain, 1st Baron Brain]]
*1962 [[George Humphrey (psychologist)|George Humphrey]], [[B F Skinner]], [[Robert H Thouless]]
*1963 [[Otto Klineberg]], [[Robert John Bartlett]]
*1965 [[Anna Freud]], [[Cecil Alec Mace]]
*1966 [[Aubrey Lewis]], [[Robert Robertson Rusk]], [[Fred Schonell]]
*1967 [[Lionel Penrose]]
*1968 [[Neal E. Miller]], [[Erwin Stengel]]
*1970 [[Edward Glover (psychoanalyst)|Edward George Glover]], [[John Giffard, 3rd Earl of Halsbury]], [[Margaret Dorothea Vernon]]
*1972 [[Raymond Cattell|Raymond Bernard Cattell]], [[Harry Harlow]], [[Henry Murray]]
*1974 [[Michael Fordham]]
*1977 [[James J. Gibson]], [[Eleanor J Gibson]]
*1978 [[Michael Rutter]], [[Philip E. Vernon]]
*1979 [[Desmond Pond]]
*1981 [[Robert Hinde]]
*1982 [[Oliver Zangwill]]
*1984 [[Jerome Bruner]]
*1985 [[Noam Chomsky]]
*1986 [[Donald Broadbent]]
*1988 [[Herbert A. Simon]]
*1989 [[George Armitage Miller]]
*1990 [[Jack Davies (cricketer)|Jack Davies]]
*1991 [[Elizabeth Loftus]]
*1992 [[Michael Argyle (psychologist)|Michael Argyle]]
*1993 [[Margaret Donaldson]], [[Klaus Werner Wedell]]
*1994 [[Ulric Neisser]], [[Freda Newcombe|Freda Gladys Newcombe]]
*1995 [[Alan D. Baddeley]], [[Patrick Rabbitt]]
*1997 [[Victoria Bruce]], [[John Morton (scientist)|John Morton]], [[Peter B Warr]]
*1998 [[Heinz Rudolph Schaffer]]
*1999 [[Antony John Chapman]]
*2000 [[Richard L. Gregory]]
*2001 [[Maurice Anthony Gale]]
*2003 [[Miles Hewstone|Miles R.C. Hewstone]]
*2005 [[Andrew William Young]]
*2006 [[Uta Frith]], [[William Yule (psychologist)|William Yule]], [[Glynis Breakwell|Glynis M. Breakwell]]
*2007 [[Alan D.B. Clarke]], [[Anne M. Clarke]], [[Hannah Steinberg]]
*2008 [[David Victor Canter]]
*2009 [[David M. Clark]]
*2010 [[Raymond Henry Charles Bull]], [[Cary Cooper|Cary Lynn Cooper]]
*2011 [[James Orford]], [[John Weinman]], [[Marie Johnston (psychologist)|Marie Johnston]]
*2012 [[Dianne Berry]], [[David Farrington]], [[Glyn Humphreys]], [[Annette Karmiloff-Smith]], [[Peter Saville (psychologist)|Peter Saville]]
*2013 [[Saths Cooper]]
*2014 [[Dorothy Bishop (psychologist)|Dorothy Bishop]]
*2015
*2016 [[Erica Burman]], [[Wendy Hollway]]
}}

== The Research Digest ==
Since 2003 the BPS has published reports on new psychology research in the form of a free fortnightly email, and since 2005, also in the form of an online blog - both are referred to as the BPS Research Digest. As of 2014, the BPS states that the email has over 32,000 subscribers and the Digest blog attracts hundreds of thousands of page views a month. In 2010 the Research Digest blog won "best psychology blog" in the inaugural Research Blogging Awards. The Research Digest has been written and edited by psychologist Christian Jarrett since its inception [http://bps-research-digest.blogspot.co.uk/p/about.html]

== Member networks: Sections, Divisions and Branches ==
The British Psychological Society currently has ten Divisions and thirteen sections. Divisions and Sections differ in that the former are open to practitioners in a certain field of psychology, so professional and qualified psychologists only will be entitled to full membership of a Division, whereas the latter are interest groups comprising members of the BPS who are interested in a particular academic aspect of psychology.

The Divisions include the Division of Teachers and Researchers in Psychology, the Division of Health Psychology, the Division of Forensic Psychology, the Division of Child and Educational Psychology, the Scottish Division of Educational Psychology, the Division of Occupational Psychology, the Division of Counselling Psychology, the Division of Clinical Psychology and the [http://www.bps.org.uk/networks-and-communities/member-networks/division-neuropsychology Division of Neuropsychology].  The Division of Clinical Psychology is the largest Division within the BPS - it is subdivided into Faculties{{how many|date=December 2011}} - the largest of these is the Faculty for Children, Young People and Their Families.

The Sections include the Consciousness and Experiential Psychology Section, the Cognitive Psychology Section, the Developmental Psychology Section, the Psychology of Education Section, the History and Philosophy Section, the Psychology of Sexualities Section, the Psychobiology Section, the Psychotherapy Section, the Qualitative Methods Section, the Psychology of Women Section, the Social Psychology Section and the Transpersonal Psychology Section.

The term "Division" in the [[American Psychological Association]] does not have the same meaning as it does in the British Psychological Society, coming closer to what the British Psychological Society refers to as "Sections". Branches are for members in the same geographical region.

===Consciousness and Experiential Psychology===
The Consciousness and Experiential Psychology Section (CEP) is a Section of the British Psychological Society for those interested in the psychology of consciousness and experience. Initiated in 1994 by Jane Henry, [[Max Velmans]], John Pickering, Elizabeth Valentine and Richard Stevens, the Section promoted and supported the reincorporation of consciousness studies into mainstream psychology.<ref>{{cite web|url=http://www.issuu.com/thepsychologist/docs/bpsannualreview2009 |title=The British Psychological Society Annual Review 2009 |publisher=Issuu.com |date=2010-05-04 |accessdate=2012-02-20}}</ref> Official approval for CEP was announced in 1997 during the BPS Annual Conference. The Section’s mission is ‘to advance our understanding of [[consciousness]],<ref>Velmans, M. (2009) Understanding Consciousness (2nd Ed). London: Routledge/Psychology Press</ref>  to bring [[scientific research]] on consciousness closer to other traditions of inquiry into the nature of mind, and to explore how this research can be used to improve the [[quality of life]]’.<ref>{{cite web|url=http://www.bps.org.uk/cep |title=Consciousness and Experiential Section |publisher=BPS |date= |accessdate=2014-01-11}}</ref>  As of 2010 Susan Stuart is the Section Chair.  Every year in September the Consciousness and Experiential Psychology Section holds its annual conference,<ref>http://www.imprint.co.uk/pdf/17_11-12Conference%20Report_FINAL.pdf</ref> usually in [[Oxford]].

The Consciousness and Experiential Psychology Section is one of thirteen Sections of the BPS.<ref>{{cite web|url=http://www.bps.org.uk |title=BPS |publisher=BPS |date= |accessdate=2014-01-11}}</ref> This Section is for anyone interested in broad-based, rigorous academic exploration of consciousness and [[experience]]. The Section is an [[interest group]] comprising members of the BPS and also unaffiliated members.  In the modern era the Consciousness and Experiential Psychology Section was the first, and remains the only, Section of a nationally representative body of professional [[psychologist]]s devoted to the study of consciousness.<ref>[http://cep.bps.org.uk/ "Consciousness and Experiential Psychology Section" at bps.org.uk]</ref>

=== Psychology of Sexualities ===
The Psychology of Sexualities Section (PoS) is a Section of the British Psychological Society for psychologists whose work is relevant to [[lesbian]], [[gay]], [[Bisexuality|bisexual]], [[Transgender|trans]] and [[queer]] ([[LGBTQ]]) issues.<ref>{{Cite web|title = The Psychology of Sexualities Section {{!}} BPS|url = http://www.bps.org.uk/pos|website = www.bps.org.uk|accessdate = 2015-12-02}}</ref> The Section is open to all BPS members including both practitioner and academic psychologists. The Section was established in 1998, as the [[Homosexuality and psychology|Lesbian & Gay Psychology]] Section, after nearly a decade of campaigning and three rejected proposals (two for a Psychology of Lesbianism Section and one for a Lesbian & Gay Psychology Section).<ref>{{Cite journal|url = |title = The struggle to found the lesbian and gay psychology section|last = Wilkinson|first = S|date = 1999|journal = Lesbian & Gay Psychology Section Newsletter|doi = |pmid = |access-date = }}</ref> Founding members of the Section include [[Celia Kitzinger and Sue Wilkinson]]. In 2009, the Section changed its name to the Psychology of Sexualities Section in recognition that the work and interests of its members also applied to bisexuality, queer identities and [[Heterosexuality|heterosexualities]].<ref>{{Cite journal|url = |title = Editorial - The times they are a-changin'|last = das Nair|first = R|date = 2009|journal = Lesbian & Gay Psychology Review  |volume=10 |issue=1 |pages=2|doi = |pmid = |access-date = }}</ref> Although trans issues could more accurately be described as belonging to a psychology of gender, trans issues are typically included under the umbrella of lesbian, gay, bisexual, trans and queer (LGBTQ) psychology<ref>{{Cite book|title = Lesbian Gay Bisexual Trans and Queer psychology: An introduction|author = Clarke, V.|author2 = Ellis, SJ.|author3 = Peel, E.|author4 = Riggs, DW|last-author-amp = yes|publisher = Cambridge University Press|year = 2010|isbn = 978-0521700184|location = Cambridge|pages = }}</ref> and is therefore aligned with the Section’s remit.

The Section works with equivalent sections of other psychological organisations through the International Psychology Network for Lesbian, Gay, Bisexual, Trans and Intersex Issues (IPsyNET).<ref>{{Cite web|title = The International Psychology Network for Lesbian, Gay, Bisexual, Transgender and Intersex Issues (IPsyNet)|url = http://www.ipsynet.org|website = http://www.apa.org|accessdate = 2015-12-02}}</ref> Members of the Section have played an important role in drafting the BPS Guidelines and literature review for psychologists working therapeutically with sexual and gender minority clients;<ref>{{Cite web|url = http://www.bps.org.uk/sites/default/files/images/rep_92.pdf|title = Guidelines and literature review for psychologists working therapeutically with sexual and gender minority clients|date = 2012|accessdate = |website = |publisher = BPS|author = British Psychological Society}}</ref> Section members were also instrumental in drafting the Society’s Position Statement on Therapies attempting to Change Sexual Orientation;<ref>{{Cite web|url = http://www.bps.org.uk/system/files/images/therapies_attempting_to_change_sexual_orientation.pdf|title = Position Statement: Therapies Attempting to Change Sexual Orientation|date = 2012|accessdate = |website = |publisher = BPS|author = British Psychological Society}}</ref> a UK Consensus Statement on Conversion Therapy;<ref>{{Cite web|url = http://www.bps.org.uk/system/files/Public%20files/conversion_therapy_final_version.pdf|title = Conversion therapy: Consensus statement|date = 2014|accessdate = |website = BPS website|publisher = |author = UK Council for Psychotherapy|display-authors=etal}}</ref> and a Memorandum on Conversion Therapy in the UK.<ref>{{Cite web|url = http://www.bps.org.uk/system/files/Public%20files/Comms-media/mou-conversiontherapy.pdf|title = Memorandum of Understanding on Conversion Therapy in the UK|date = 2015|accessdate = |website = BPS website|publisher = }}</ref> The Section publishes Psychology of Sexualities Review (previously the Lesbian & Gay Psychology Review),<ref>{{Cite web|title = BPS Shop {{!}}  Psychology of Sexualities Review - Publication by Series - Publications|url = http://shop.bps.org.uk/publications/publication-by-series/psychology-of-sexualities-review.html|website = shop.bps.org.uk|accessdate = 2015-12-02}}</ref> organises events and training and awards prizes for achievement in the field.

== Statutory regulation ==

Following a number of scandals arising in the 1990s in the [[psychotherapy]] field, the UK government announced its intention to widen statutory regulation, to include ''inter alia'' psychologists.  The BPS was in favour of statutory regulation, but opposed the proposed regulator, the [[Health Professions Council]] (HPC), preferring the idea of a new Psychological Professions Council which would map quite closely onto its own responsibilities.  The government resisted this, however, and in June 2009, under the Health Care and Associated Professions (Miscellaneous Amendments) Order, regulation of most of the psychology professions passed to the HCPC, the renamed [[Health and Care Professions Council]].

== Society offices ==
The Society's main office is currently in [[Leicester]] in the United Kingdom. Before the transfer of registration and associated functions to the HPC, there were over 100 staff members at the Leicester office. There are also smaller regional offices in [[Belfast]], [[Cardiff]], [[Glasgow]] and [[London]]. The archives are deposited at the [[Wellcome Library]] in the [[Euston Road]], [[London]].<ref>{{cite web|title=British Psychological Society Archive|url=http://search.wellcomelibrary.org/iii/encore/record/C__Rb1972610__SCharles%20Myers__Orightresult__U__X3?lang=eng&suite=cobalt|website=Catalogue|publisher=Wellcome Library|accessdate=9 February 2016}}</ref>

== Logo ==
The British Psychological Society's logo is an image of the Greek mythical figure [[Psyche (mythology)|Psyche]], personification of the soul, holding a Victorian [[oil lamp]]. The use of her image is a reference to the origins of the word [[psychology]]. The lamp symbolises learning and is also a reference to the story of Psyche. [[Eros]] was in love with Psyche and would visit her at night, but had forbidden her from finding out his identity. She was persuaded by her jealous sisters to discover his identity by holding a lamp to his face as he slept. Psyche accidentally burnt him with oil from the lamp, and he awoke and flew away.<ref>Steinberg, H. (2001). A brief history of the Society logo. ''The Psychologist'', 14, 236–237. Download article via
{{cite web
 |url=http://hopc.bps.org.uk/hopc/histres/bpshistory/bpshistory_home.cfm 
 |title=Archived copy 
 |accessdate=2011-11-30 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120317130205/http://hopc.bps.org.uk:80/hopc/histres/bpshistory/bpshistory_home.cfm 
 |archivedate=2012-03-17 
 |df= 
}}</ref>

== See also ==
* [[Association for Psychological Science]]
* [[Association of Business Psychologists]]
* [[List of psychologists]]
* [[Spearman Medal]]

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.bps.org.uk/index.cfm}}
* [https://web.archive.org/web/20120317130205/http://hopc.bps.org.uk:80/hopc/histres/bpshistory/bpshistory_home.cfm History of the British Psychological Society]

[[Category:Organizations established in 1901]]
[[Category:Psychology organizations]]
[[Category:Learned societies of the United Kingdom]]
[[Category:Professional associations based in the United Kingdom]]
[[Category:History of mental health in the United Kingdom]]
[[Category:Charities based in Leicestershire]]
[[Category:1901 establishments in the United Kingdom]]
[[Category:British Psychological Society| ]]
[[Category:Psychology journals]]