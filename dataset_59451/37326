{{good article}}
{{Infobox royalty
| name          = Andrew Báthory
| title         = [[Cardinal-deacon]] of [[Sant'Adriano al Foro]]
| image         = File:Andreas Bathory, polsk biskop (1589-99), målad 1688-1703 - Skoklosters slott - 98170.tif
| caption       =
| succession    = [[List of bishops of Warmia|Prince-Bishop of Warmia]]
| reign         = 1589–1599
| coronation    =
| cor-type      =
| predecessor   =  [[Marcin Kromer]]
| successor     =  [[Piotr Tylicki]]
| regent         =
| succession1    =  [[Prince of Transylvania]]
| reign1         =  1599
| coronation1    =
| predecessor1   =  [[Sigismund Báthory]]
| successor1     =
| spouse         =
| issue          =
| house          = [[Báthory family]]
| house-type=
| father         = Andrew Báthory
| mother         = [[Margit Majláth]]
| birth_date  = 1562 or 1563
| birth_place =
| death_date  = 3 November {{Death year and age|1599|1562}}
| death_place = [[Csíkszentdomokos]], [[Principality of Transylvania (1570–1711)|Principality of Transylvania]]<br />(now Sândominic, Romania)
| burial_place = [[St. Michael's Cathedral, Alba Iulia|St. Michael's Catholic Cathedral]], [[Gyulafehérvár]], Principality of Transylvania)<br />(now Alba Iulia, Romania)
| religion =[[Roman Catholic]]
| signature=
}}

'''Andrew Báthory''' ({{lang-hu|Báthory András}}; {{lang-pl|Andrzej Batory}}; 1562 or 1563 – 3 November 1599) was the [[Cardinal-deacon]] of [[Sant'Adriano al Foro]] from 1584 to 1599, [[Prince-Bishopric of Warmia|Prince-Bishop of Warmia]] from 1589 to 1599, and [[Prince of Transylvania]] in 1599. His father was a brother of [[Stephen Báthory]], who ruled the [[Polish–Lithuanian Commonwealth]] from 1575. He was the childless Stephen Báthory's favorite nephew. He went to Poland at his uncle's invitation in 1578 and studied at the [[Jesuit]] college in [[Pułtusk]]. He became [[Canon (priest)|canon]] in the [[Chapter (religion)|Chapter]] of the [[Roman Catholic Archdiocese of Warmia|Roman Catholic Diocese of Warmia]] in 1581, and [[Provost (religion)|provost]] of the [[Church of the Holy Sepulchre, Miechów|Monastery of Miechów]] in 1583.

[[Pope Gregory XIII]] appointed Báthory cardinal during his visit to Rome in 1584. A year later, he was installed as [[coadjutor bishop]] of Warmia. He was in Rome again when Stephen Báthory died in 1586. Andrew was one of the candidates to succeed him in Poland and Lithuania, but [[Jan Zamoyski]], the [[Chancellor (Poland)|Chancellor of Poland]], convinced him to support another candidate, [[Sigismund III Vasa|Sigismund Vasa]], and to demonstrate the Báthorys' claim to the crown only through nominating his minor cousin, [[Sigismund Báthory]], [[Prince of Transylvania]]. After Sigismund Vasa was elected king in 1587, Báthory convinced his cousin's advisors to send reinforcements to Poland to fight against [[Maximilian III, Archduke of Austria|Maximilian of Habsburg]], who also claimed the throne. Báthory became Prince-Bishop of Warmia after the death of Bishop [[Marcin Kromer]] in 1589.

In the early 1590s, Andrew and his brother, [[Balthasar Báthory]], came into conflict with Sigismund Báthory over the presence of Jesuits in the predominantly Protestant Transylvania. Before long, Sigismund's plan to join the [[Holy League of Pope Clement VIII]] against the [[Ottoman Empire]] gave rise to new tensions, because the brothers sharply opposed the plan. Sigismund executed Balthasar and confiscated Andrew's estates in 1594. After the Ottomans defeated the army of the Holy League in a series of battles, Sigismund decided to [[Abdication|abdicate]]. He transferred Transylvania to the [[Holy Roman Emperor]], [[Rudolph II, Holy Roman Emperor|Rudolph II]], in 1598, but he returned a few months later.

Sigismund and Andrew were reconciled, and Sigismund renounced Transylvania in favor of Andrew in March 1599. Andrew was supported by Poland and the Ottoman Empire. Rudolph II persuaded [[Michael the Brave]], [[Voivode of Wallachia]], to invade Transylvania. Michael defeated Andrew's troops at the [[Battle of Sellenberk]] with the assistance of [[Székelys|Székely]] commoners, to whom he had promised to restore their freedom. Andrew wanted to flee to Poland, but Székely serfs captured and killed him.

== Early life ==

Andrew was the youngest of the four sons of András Báthory and [[Margit Majláth]].{{sfn|Horn|2002|pp=16, 245}}{{sfn|Szabó|2012|p=188}} His father was the marshal of the court of [[Isabella Jagiellon]] and her son, [[John Sigismund Zápolya]].{{sfn|Horn|2002|p=16}} Isabella and John Sigismund ruled the [[Eastern Hungarian Kingdom|eastern territories of the medieval Kingdom of Hungary]] as [[vassal]]s of the [[Ottoman Sultan]] [[Suleiman the Magnificent]].{{sfn|Barta|1994|pp=258–259}} The date of Andrew's birth is uncertain, because his actual age was kept secret in the early 1580s to promote his career in the [[Roman Catholic Church]].{{sfn|Horn|2002|pp=16–17}} He was most probably born in late 1562 or early 1563, according to historian Ildikó Horn.{{sfn|Horn|2002|p=16}} His father died around that time, implying that Andrew was a [[posthumous son]].{{sfn|Horn|2002|p=16}}

Andrew's mother married János Iffjú before the end 1563.{{sfn|Horn|2002|p=16}} Andrew and his youngest brother, [[Balthasar Báthory|Balthasar]], and their sisters, Anne and Catherine, lived for years at their stepfather's estate, Érmihályfalva (now [[Valea lui Mihai]] in Romania).{{sfn|Horn|2002|p=18}} Both his mother and her second husband were [[Evangelical-Lutheran Church in Hungary|Lutherans]], but Andrew remained Roman Catholic, like his father and uncles, [[Christopher Báthory|Christopher]] and [[Stephen Báthory]].{{sfn|Horn|2002|pp=18–19}} Christopher was Andrew's legal guardian, but his actual role in his education is uncertain.{{sfn|Horn|2002|pp=17–18}}

After John Sigismund died in 1571, the [[Diet of Transylvania]] elected Stephen Báthory [[voivode of Transylvania|voivode]] (or ruler) in 1571.{{sfn|Barta|1994|p=260}} Stephen adopted the title of [[prince of Transylvania]] after he was elected [[king of Poland]] and [[grand duke of Lithuania]] in 1575.{{sfn|Barta|1994|p=261}} The childless king decided to take charge of his nephew's education and ordered him to come to Poland in late 1578.{{sfn|Horn|2002|p=24}} Andrew studied at the [[Jesuit]] college in [[Pułtusk]].{{sfn|Szabó|2012|p=188}}{{sfn|Horn|2002|p=24}} His fellow-students were mainly young Protestant noblemen from [[Transylvania]] or [[Royal Hungary]], but Polish, Lithuanian, German and Italian noblemen and commoners were also chosen to study together with him.{{sfn|Horn|2002|p=29}} He could speak Latin, Italian, Polish and German by the end of his studies.{{sfn|Horn|2002|p=33}}

==Church career==

===Beginnings===

[[File:Emanuel van Meteren Historie ppn 051504510 MG 8754 stephanus bathor.tif|thumb|right|alt=A bearded middle-aged man wearing a hat|Andrew's uncle, [[Stephen Báthory]]]]

Andrew was the favorite nephew of Stephen Báthory who officially adopted him.{{sfn|Horn|2002|p=39}} [[Giovanni Andrea Caligari]], the [[papal nuncio]] (or ambassador) in the [[Polish–Lithuanian Commonwealth]], urged the king to persuade Andrew to start a church career.{{sfn|Horn|2002|p=27}} According to the Transylvanian historian, [[István Szamosközy]], Andrew had already been fascinated by altars, churches and tombs as a child.{{sfn|Horn|2002|p=26}} However, his Protestant mother and relatives wanted to dissuade Andrew.{{sfn|Horn|2002|p=26}} After recovering from an almost lethal [[pneumonia]] in spring 1581, Andrew accepted their advice.{{sfn|Horn|2002|pp=39–40, 42}} He was especially worried about [[Clerical celibacy (Catholic Church)|clerical celibacy]], because it prevented him from fathering legitimate children.{{sfn|Horn|2002|p=42}} He also hoped that his uncle would promote his secular career.{{sfn|Horn|2002|p=42}} His Jesuit tutors finally persuaded him to accept his uncle's decision.{{sfn|Horn|2002|p=43}}

Andrew was made [[Canon (priest)|canon]] in the [[Chapter (religion)|Chapter]] of the [[Roman Catholic Archdiocese of Warmia|Roman Catholic Diocese of Warmia]] on 16 September 1581.{{sfn|Horn|2002|p=49}} The canons and the elderly bishop, [[Marcin Kromer]], had been opposed to Andrew's appointment, but the new papal nuncio, [[Alberto Bolognetti]], the [[Chancellor (Poland)|chancellor of Poland]], [[Jan Zamoyski]], and Stephen Báthory persuaded one of the canons to [[abdicate]] in favor of Andrew.{{sfn|Horn|2002|p=49}} However, the canons, along with the Estates of [[Royal Prussia]], prevented Andrew's promotion to [[coadjutor bishop]] saying that he was too young.{{sfn|Horn|2002|p=50}} At the nuncio's initiative, Andrew was elected [[Provost (religion)|provost]] of the [[Church of the Holy Sepulchre, Miechów|Monastery of Miechów]] in spring 1583.{{sfn|Szabó|2012|p=188}}{{sfn|Horn|2002|p=58}}

===Cardinal===

Stephen Báthory sent Andrew to Rome to start negotiations about an alliance against the Ottoman Empire, and to secure Andrew's creation as [[Catholic Cardinal|cardinal]].{{sfn|Horn|2002|p=58}} Andrew and his retinue left Kraków for Italy on 10 September 1583.{{sfn|Horn|2002|p=64}} He visited [[Charles Borromeo]], [[archbishop of Milan]], who was a highly respected [[prelate]] and regarded as a living [[saint]] for his piety in his [[diocese]].{{sfn|Horn|2002|p=66}} After their meeting, Borromeo wrote a spiritual instruction to him.{{sfn|Horn|2002|p=66}} In Milan, Andrew also met [[Giovanni Botero]] who described him as an actual representative of [[Machiavellianism|anti-Machiavellianism]].{{sfn|Horn|2002|p=66}} A Protestant retainer of Andrew, [[Ferenc Forgách, Archbishop of Esztergom|Ferenc Forgách]], converted to Catholicism in the [[Basilica della Santa Casa]] in [[Loreto, Marche|Loreto]] on 21 November.{{sfn|Horn|2002|pp=68–69}}

Andrew (whom the ambassador of Venice mentioned as the "Polish prince") entered Rome on the [[Calendar of saints|feast]] of [[Andrew the Apostle]] (30 November).{{sfn|Horn|2002|p=69}} Five days later, [[Pope Gregory XIII]] received him and made him [[Protonotary apostolic|papal protonotary]].{{sfn|Horn|2002|p=70}} The pope, who could not persuade Stephen Báthory to join an anti-Ottoman coalition, failed to make Andrew cardinal.{{sfn|Horn|2002|p=77}} Following his uncle's advice, Andrew announced that he was to leave Rome.{{sfn|Horn|2002|p=77}} The pope appointed him [[cardinal-deacon]] of [[Sant'Adriano al Foro]] on 23 July 1584.{{sfn|Horn|2002|p=77}} On this occasion, [[Giovanni Pierluigi da Palestrina]] dedicated a [[motet]] to the [[Báthory family|Báthorys]].{{sfn|Granasztói|1981|p=408}} Three days later Andrew left Rome.{{sfn|Horn|2002|p=78}}

===Coadjutor bishop===

Andrew returned to Poland on 20 October 1584 and settled in Miechów.{{sfn|Horn|2002|pp=80–81}} He rebuilt the altar in the monastery and introduced new ceremonies (including a 40-hour prayer for the king and the kingdom).{{sfn|Horn|2002|p=82}} He was installed as coadjutor bishop at the see of the Bishopric of Warmia, [[Lidzbark Warmiński]], in early July 1585.{{sfn|Horn|2002|p=87}} He visited the nearby major towns, including [[Gdańsk]] and [[Malbork]].{{sfn|Horn|2002|p=87}}

Meanwhile, Stephen Báthory had decided to secure the [[Roman Catholic Archdiocese of Kraków|Bishopric of Kraków]] for his nephew, but the Holy See did not appoint Andrew coadjutor bishop of the Kraków See, because he was still too young.{{sfn|Horn|2002|pp=85–87}} The king wanted to invade Russia and sent Andrew to Rome to convince [[Pope Sixtus V]] to support his plan.{{sfn|Horn|2002|p=88}} Andrew left Poland on 22 March 1586.{{sfn|Horn|2002|p=90}} [[Samuel Zborowski]]'s brothers (he had been executed on Stephen Báthory's orders), wanted to capture and murder Andrew, but their plan failed because of a flood.{{sfn|Horn|2002|p=92}} Andrew came to Rome on 24 July.{{sfn|Horn|2002|p=92}} He informed the pope about Stephen Báthory's plan, asking financial support from the Holy See against Russia.{{sfn|Horn|2002|p=94}}

[[File:Polish-Lithuanian Commonwealth 1582.PNG|thumb|right|upright=1.5|alt=Administrative division of Poland and Lithuania|[[Polish–Lithuanian Commonwealth]] in 1582]]

Stephen Báthory died on 13 December 1586.{{sfn|Granasztói|1981|p=409}} Andrew left Rome and hurried back to Poland.{{sfn|Horn|2002|pp=96–97}} He inherited the domains of Gyalu, Nagyenyed and Örményes in Transylvania (now [[Gilău, Cluj|Gilău]], [[Aiud]] and [[Armeniș]] in Romania) from his uncle.{{sfn|Horn|2002|p=121}} Stephen Báthory stipulated that Gyalu should serve the reestablishment of the Roman Catholic bishopric in Transylvania.{{sfn|Horn|2002|p=136}}

Andrew, his brother Balthasar, and their cousin [[Sigismund Báthory]], prince of Transylvania, were among the sixteen candidates to the throne of Poland and Lithuania.{{sfn|Horn|2002|p=98}} Before long, [[Bartosz Paprocki]] published a pamphlet against "the kings from Hungary", accusing Stephen Báthory of suppressing the Polish nobles.{{sfn|Horn|2002|p=99}} Jan Zamoyski initially stood by Andrew, who was also supported by the pope and the sultan.{{sfn|Horn|2002|pp=103–104}} Andrew's opponents pillaged the monastery of Miechów.{{sfn|Horn|2002|p=98}} A group of noblemen warned Andrew to leave Poland in February 1587.{{sfn|Horn|2002|p=99}}

After realising that the Báthorys had little chance to seize the throne, Zamoyski decided to support another candidate, [[Sigismund III Vasa|Sigismund Vasa]].{{sfn|Horn|2002|p=100}} On Zamoyski's advice, the fourteen-year-old Sigismund Báthory was officially presented as the sole candidate from the family (which demonstrated the existence of the Báthorys' claim to the throne), but Andrew cooperated with Zamoyski on behalf of Sigismund Vasa.{{sfn|Horn|2002|pp=105–106}} Sigismund Vasa was elected king of Poland and grand duke of Lithuania on 19 August 1587.{{sfn|Horn|2002|p=110}} However, his opponents proclaimed [[Maximilian III, Archduke of Austria|Maximilian of Habsburg]] the ruler of the Commonwealth.{{sfn|Horn|2002|p=110}} Maximilian laid siege to Kraków and pillaged Miechów.{{sfn|Horn|2002|p=120}} Andrew persuaded Sigismund Báthory's advisors to send Transylvanian reinforcements to fight the invaders.{{sfn|Horn|2002|p=110}} Andrew was chosen to receive Sigismund Vasa in Kraków.{{sfn|Horn|2002|p=117}} He also attended the new king's coronation on 27 December 1587.{{sfn|Horn|2002|p=118}} The ''[[Sejm of the Polish–Lithuanian Commonwealth|Sejm]]'' (or general assembly) granted citizenship to both Andrew and Balthasar Báthory.{{sfn|Horn|2002|p=119}}

[[István Jósika]], István Bodoni, and other fellow-students of Andrew became important advisors of the young Sigismund Báthory in Transylvania.{{sfn|Horn|2002|p=126}} After the Diet of Transylvania expelled the Jesuits in December 1588,{{sfn|Barta|1994|p=293}} Pope Sixtus V excommunicated Sigismund.{{sfn|Horn|2002|p=133}} Andrew went to Transylvania and sent letters to Rome to achieve a reconciliation, emphasizing that the Jesuits' aggressive proselytizing policy had contributed to their unpopularity in the predominantly Protestant principality.{{sfn|Horn|2002|p=133}}

===Bishop of Warmia===

After Marcin Kromer died in early March 1589, Andrew became the [[Prince-Bishop]] of Warmia and a member of the [[Senate of Poland]].{{sfn|Horn|2002|p=140}} He supported the introduction of the teaching of philosophy and theology in the [[Collegium Hosianum]] in [[Braniewo]], because he wanted to develop it into a university.{{sfn|Horn|2002|p=145}} At the request of Sigismund III Vasa, Pope Sixtus made Andrew coadjutor bishop of [[Piotr Myszkowski (bishop)|Piotr Myszkowski]], the elderly [[bishop of Kraków]].{{sfn|Horn|2002|pp=139, 148}} However, the Habsburgs wanted to prevent Andrew from seizing the bishopric which was situated near Royal Hungary.{{sfn|Horn|2002|p=139}} The new papal nuncio, [[Annibale di Capua]] (who was their supporter) convinced the king to nominate [[Jerzy Radziwiłł (1556–1600)|Jerzy Radziwiłł]] to the see after Myszkowski died on 5 April 1591.{{sfn|Horn|2002|pp=148, 150–151}} Capua emphasized that Andrew had not been an [[Priesthood (Catholic Church)|ordained priest]].{{sfn|Horn|2002|pp=150–151}} Most Polish noblemen regarded the Lithuanian Radziwiłł's appointment to a Polish see unlawful, but [[Pope Gregory XIV]] confirmed the king's decision.{{sfn|Horn|2002|pp=151–152}}

[[File:Basilica of the Holy Sepulchre in Miechów 2014 P06.JPG|thumb|left|alt=A gilded altar with six marvel columns and several sculptures in the background|Altar in the [[Church of the Holy Sepulchre, Miechów|Church of the Holy Sepulchre]] in [[Miechów]]]]
[[File:Báthory Zsigmond 1596.jpg|thumb|left|alt=A young man with a moustache|Andrew's cousin, [[Sigismund Báthory]]]]

Sigismund III's confessor and court priest, who were Jesuits, supported Radziwiłł against Andrew.{{sfn|Horn|2002|pp=135, 150}} Thereafter, Andrew urged the Holy See to send [[Franciscan]] friars to Transylvania instead of the Jesuits (who were supported by Sigismund Báthory).{{sfn|Horn|2002|pp=135–136}} He also suggested that the pope should make a Franciscan friar bishop of Transylvania, with a see in [[Csíksomlyó]] (now Șumuleu Ciuc in Romania).{{sfn|Horn|2002|p=136}} He settled Catholic priests in four villages on his estates.{{sfn|Horn|2002|p=136}} The Holy See authorized him to set up [[deanery|deaneries]] in Transylvania, making him the actual head of the Catholic Church in the principality.{{sfn|Horn|2002|pp=136, 145}}

In August 1591, Maximilian of Habsburg, who still claimed Poland and Lithuania, sent his envoy to Andrew seeking Transylvanian support against Sigismund III.{{sfn|Horn|2002|p=160}} Andrew emphasized that being under Ottoman suzerainty, Transylvania could not openly support Maximilian, but he also promised that Sigismund Báthory would not prevent the mustering of Transylvanian soldiers to fight in Poland.{{sfn|Horn|2002|p=160}} In the same month, Sigismund Báthory's plan to enable the Jesuits to return to Transylvania gave rise to a serious family conflict, because both Andrew and Balthasar refused to support the prince at the Diet.{{sfn|Horn|2002|p=163}} According to contemporaneous gossips, Andrew and his two brothers decided to dethrone Sigismund, replacing him with Balthasar.{{sfn|Horn|2002|p=166}} [[Pope Clement VIII]] sent a papal nuncio, [[Attilio Amalteo]], to Transylvania to mediate a reconciliation.{{sfn|Horn|2002|p=168}}{{sfn|Ó hAnnracháin|2015|p=154}}

The Holy See also tried to reach a compromise on the Kraków bishopric, but Jan Zamoyski, who was in conflict with the king, persuaded Andrew to give up his claim in favor of Radziwiłł.{{sfn|Horn|2002|p=173}} Andrew withheld 20,000 [[Goldgulden|gulden]] from the royal tax of Warmia, saying that the king owed him 40,000 gulden.{{sfn|Horn|2002|p=173}} He started negotiations with [[John George, Elector of Brandenburg]], who promised him the hand of one of his daughters if he accepted the protection of Brandenburg over Warmia.{{sfn|Horn|2002|p=175}} Zamoyski, who had been with Sigismund III, also persuaded Andrew to make peace with the king in May 1593.{{sfn|Horn|2002|p=176}} Andrew renounced the see of Kraków in exchange for the [[Abbey of Czerwińsk nad Wisłą]].{{sfn|Horn|2002|p=177}}

The pope's new envoy, Alessandro Cumuleo, came to Transylvania to urge Sigismund Báthory to join the [[Holy League of Pope Clement VIII|Holy League]] that the pope had set up against the Ottomans.{{sfn|Horn|2002|p=180}} The prince was ready to join the alliance, but Andrew and Balthasar emphasized that Transylvania could not secede from the Ottoman Empire without the participation of Poland in the coalition.{{sfn|Horn|2002|pp=180–181}} Most of Sigismund's advisors supported their proposal, but his confessor, the Jesuit Alfonso Carillo, convinced him to continue the negotiations with the [[Holy Roman Emperor]], [[Rudolph II, Holy Roman Emperor|Rudolph II]].{{sfn|Horn|2002|p=181}} Carillo prevented Andrew from taking part in the negotiations in Prague.{{sfn|Horn|2002|p=182}} Before long, Andrew left Transylvania for Poland.{{sfn|Horn|2002|p=182}}

Sigismund Báthory's maternal uncle, [[Stephen Bocskai]], and other commanders of the Transylvanian army, persuaded the prince to get rid of those who did not support the Holy League.{{sfn|Barta|1994|p=294}}{{sfn|Felezeu|2009|p=30}} Balthasar Báthory and his allies were captured and murdered in late August 1594.{{sfn|Barta|1994|p=294}}{{sfn|Felezeu|2009|p=30}} Sigismund also confiscated Andrew's Transylvanian estates.{{sfn|Horn|2002|p=184}} The Diet confirmed the prince's acts, convicting Andrew and his brother, [[Stephen Báthory (1553–1601)|Stephen]], of treason.{{sfn|Horn|2002|pp=210–211}}

Andrew sent letters to the Holy See, describing his cousin as an immoral tyrant.{{sfn|Horn|2002|p=185}} He wanted to replace Sigismund with Stephen with the assistance of the pope, England and Poland, but they received no support.{{sfn|Horn|2002|pp=185–186}} Pope Clement VIII invited him to Rome, but he refused.{{sfn|Horn|2002|p=186}} At the pope's request, Sigismund Báthory allowed Andrew's mother and Stephen's wife, along with their children, to move to Poland.{{sfn|Horn|2002|p=187}} Zamoyski, who strongly opposed Sigismund Báthory's anti-Ottoman policy, supported Andrew and Stephen.{{sfn|Horn|2002|p=189}}

Andrew exchanged letters with [[Aaron the Tyrant]], [[Voivode of Moldavia]], who had been captured by Sigismund Báthory because of his attempts to make peace with Poland and the Ottoman Empire.{{sfn|Horn|2002|p=189}} The pope's special envoy, Martio Malacrida, tried to convince Andrew to accept the post of the ambassador of Poland in Rome, but Andrew again refused.{{sfn|Horn|2002|p=190}} He said, his homeland would require his presence, because Sigismund's anti-Ottoman policy could not be continued for long.{{sfn|Horn|2002|p=190}} Letters found on one of Stephen Báthory's retainers suggested that Andrew and his brother sent letters to the leaders of the [[Székelys|Székely]] commoners who had risen up against Sigismund Báthory in February 1596.{{sfn|Horn|2002|pp=191–192}} Sigismund III of Poland summoned Andrew and Stephen before the Senate, but Pope Clement VIII intervened on the brothers' behalf.{{sfn|Horn|2002|p=192}}

After the Ottomans defeated the armies of the Holy League in a series of battles in 1596, Transylvanian noblemen sent letters to Andrew, offering him the principality.{{sfn|Barta|1994|p=295}}{{sfn|Horn|2002|p=193}} After realizing that neither the pope nor the Polish king would support him against Sigismund Báthory, Andrew declared that he was ready to make peace with him.{{sfn|Horn|2002|p=194}} He returned to Warmia where he was ordained [[subdeacon]] on 4 January 1597.{{sfn|Horn|2002|p=194}} Andrew and Stephen even announced that they would not intervene in Transylvania.{{sfn|Horn|2002|p=195}} Their envoys also started discussions with Sigismund Báthory about the compensation for their expropriated Transylvanian estates.{{sfn|Horn|2002|p=197}}

Sigismund Báthory opened negotiations over the transfer of Transylvania to the Habsburgs, which worried both Poland and the Ottoman Empire.{{sfn|Horn|2002|p=198}} On 20 February 1598, Andrew offered the Bishopric of Warmia and his other Church offices in Poland to Sigismund, in exchange for Transylvania. {{sfn|Horn|2002|p=201}} However, Sigismund's envoys had already signed an agreement with Rudolph II.{{sfn|Horn|2002|p=198}} Sigismund left Transylvania, and Rudolph's commissioners took charge of the administration of the principality on 10 April.{{sfn|Horn|2002|p=201}} Before long, Sigismund changed his mind and returned to Transylvania with the assistance of Stephen Bocskai.{{sfn|Horn|2002|p=204}} Sigismund sent a Jesuit as his representative to Kraków and offered Transylvania to Andrew in November.{{sfn|Horn|2002|p=205}} Andrew accepted the offer and left Poland in disguise, without revealing the actual purpose of his travel to Transylvania.{{sfn|Horn|2002|p=206}} He met Sigismund in [[Kolozsvár]] (now Cluj-Napoca in Romania) in the middle of February 1599.{{sfn|Horn|2002|p=206}} Most contemporaries (including Szamosközy) believed that Andrew came to Transylvania to make peace with his cousin.{{sfn|Horn|2002|p=206}}

== Prince of Transylvania ==

Sigismund Báthory abdicated at the Diet in [[Medgyes]] (Mediaș, Romania) on 21 March and proposed Andrew his successor.{{sfn|Barta|1994|p=295}}{{sfn|Granasztói|1981|p=419}} After Andrew pledged that neither he nor his brother would take vengeance on those who had voted against them in 1594, the Diet elected him prince on 28 March.{{sfn|Horn|2002|p=211}}{{sfn|Felezeu|2009|p=80}} However, as Alfonso Carillo noted, the most influential noblemen remained opposed to Andrew, even if they did not dare to raise an objection openly.{{sfn|Horn|2002|p=211}}

[[File:Báthory András 1599. október 31.jpg|thumb|left|alt=A head of a bearded man with a wound on his forehead|Andrew's head]]

The new prince's principal supporters were the noblemen who had been forced into exile in 1594, but they were impoverished young men, without influence.{{sfn|Horn|2002|p=211}} Instead of them, Andrew had to choose his officials from among the Roman Catholic lords: he made István Bodoni ''[[ispán]]'' (or head) of [[Kolozs County]] and Gáspár Sibrik the commander of the cavalry.{{sfn|Horn|2002|p=212}} Andrew was even unable to get rid of his opponents.{{sfn|Horn|2002|p=212}} For instance, Gáspár Kornis remained a member of the royal council, although he had played a preeminent role in the execution of Andrew's brother in 1594.{{sfn|Horn|2002|p=212}}

Andrew wanted to secure his suzerainty over the rulers of [[Moldavia]] and [[Wallachia]].{{sfn|Horn|2002|p=216}} He initially wanted to replace [[Michael the Brave]], [[Voivode of Wallachia]], with his brother, Stephen, but Michael swore fealty to him on 26 June.{{sfn|Felezeu|2009|p=80}}{{sfn|Horn|2002|p=216}} Andrew's envoys signed an agreement with [[Ieremia Movilă]], Voivode of Moldavia, about the marriage of the voivode's daughter and Andrew's half-brother, János Iffjú, on 3 July.{{sfn|Horn|2002|p=216}} Andrew also started negotiations with the Ottoman Empire, demanding the confirmation of his hereditary rule and his suzerainty over Wallachia, and refusing the payment of the tribute for six years.{{sfn|Horn|2002|pp=217–218}}

After the marriage of Sigismund Báthory and [[Maria Christierna of Habsburg]] (who was a niece of Rudolph II) was declared invalid in August, Andrew proposed himself to her.{{sfn|Horn|2002|p=215}} However, Rudolph II had not acknowledged Andrew's rule in Transylvania and sent envoys to the leaders of the "[[Three Nations of Transylvania]]".{{sfn|Horn|2002|p=221}} He also urged Stephen Bocskai and Michael the Brave to invade Transylvania.{{sfn|Horn|2002|p=227}} Andrew summoned Stephen Bocskai to the Diet, charging him with treason.{{sfn|Granasztói|1981|p=420}} However, he did not believe the reports of Michael's preparations for an invasion and he was surprised when Michael broke into Transylvania in October.{{sfn|Horn|2002|p=229}}{{sfn|Felezeu|2009|p=81}} The Székely commoners joined the invaders, because Michael promised to restore their freedom.{{sfn|Horn|2002|p=230}} On 28 October, Michael routed the Transylvanian army in the [[Battle of Sellenberk]] after Andrew—who had never participated in a battle—fled from the battlefield in a panic.{{sfn|Felezeu|2009|p=82}}{{sfn|Horn|2002|pp=231, 233–234}} The Diet of Transylvania recognized Michael as Rudolph II's representative.{{sfn|Barta|1994|p=296}}

Andrew wanted to flee to Poland, but Székely serfs captured him on a mountain near [[Csíkszentdomokos]] (now Sândominic in Romania) on 3 November.{{sfn|Barta|1994|p=296}}{{sfn|Horn|2002|p=235}} He was struck to death with a [[shepherd's axe]].{{sfn|Horn|2002|p=235}} His head was cut off and taken to Michael the Brave, who sent it to the pope's envoy, [[Germanico Malaspina]].{{sfn|Horn|2002|p=236}} A Greek artist painted the head before it was sewn back on to the body at Michael's order.{{sfn|Horn|2002|p=236}} Andrew was ceremoniously buried in  [[St. Michael's Cathedral, Alba Iulia|St. Michael's Cathedral]] in Gyulafehérvár on 24 November.{{sfn|Horn|2002|p=236}}

== Ancestors ==

{{ahnentafel top|width=100%|Ancestors of Andrew Báthory{{sfn|Horn|2002|pp=13–14, 244–245}}{{sfn|Markó|2000|pp=232, 282, 289}}}}
<center>{{ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Andrew Báthory'''

|2= 2. András Báthory
|3= 3. [[Margit Majláth]]

|4= 4. [[Stephen VIII Báthory|Stephen Báthory]]
|5= 5. Katalin Thelegdy
|6= 6. Stephen Majláth
|7= 7. Katalin Nádasdy

|8= 8. Miklós Báthory
|9=
|10=
|11=
|12= 12. Mátyás Majláth
|13=
|14= 14. Ferenc Nádasdy
|15=

|16= 16. István Báthory
|17= 17. Dorottya Várdai
|18=
|19=
|20=
|21=
|22=
|23=
|24=
|25=
|26=
|27=
|28=
|29=
|30=
|31=
}}</center>
{{ahnentafel bottom}}

== References ==
{{reflist|30em}}

== Sources ==
{{Refbegin}}
* {{cite book |last=Barta |first=Gábor |editor1-last=Köpeczi |editor1-first=Béla |editor2-last=Barta |editor2-first=Gábor |editor3-last=Bóna |editor3-first=István |editor4-last=Makkai |editor4-first=László |editor5-last=Szász |editor5-first=Zoltán |editor6-last=Borus |editor6-first=Judit | title=History of Transylvania |publisher=Akadémiai Kiadó |year=1994 |pages=247–300 |chapter=The Emergence of the Principality and its First Crises (1526–1606) |isbn=963-05-6703-2 |ref=harv}}
* {{cite book |last=Felezeu |first=Călin |editor1-last=Pop |editor1-first=Ioan-Aurel |editor2-last=Nägler |editor2-first=Thomas |editor3-last=Magyari |editor3-first=András | title=The History of Transylvania, Vo. II (From 1541 to 1711) |publisher=Romanian Academy, Center for Transylvanian Studies |year=2009 |pages=15–73 |chapter=The International Political Background (1541–1699); The Legal Status of the Principality of Transylvania in Its Relations with the Ottoman Porte |isbn=973-7784-04-9 |ref=harv}}
* {{cite book |last=Granasztói |first=György |editor1-last=Benda |editor1-first=Kálmán |editor2-last=Péter |editor2-first=Katalin |title=Magyarország történeti kronológiája, II: 1526–1848 ''[Historical Chronology of Hungary, Volume I: 1526–1848]'' |publisher=Akadémiai Kiadó |year=1981 |pages=390–430 |chapter=A három részre szakadt ország és a török kiűzése (1557–1605) |isbn=963-05-2662-X |language=hu |ref=harv}}
* {{cite book |last=Ó hAnnracháin |first=Tadhg |year=2015 |title=Catholic Europe, 1592–1648: Centre and Peripheries |publisher=Oxford University Press |isbn=978-0-19-927272-3 |ref=harv}}
* {{cite book |last=Horn |first=Ildikó |year=2002 |title=Báthory András ''[Andrew Báthory]'' |publisher=Új Mandátum |isbn=963-9336-51-3 |language=hu |ref=harv}}
* {{cite book |last=Markó |first=László |year=2000 |title=A magyar állam főméltóságai Szent Istvántól napjainkig: Életrajzi Lexikon ''[Great Officers of State in Hungary from King Saint Stephen to Our Days: A Biographical Encyclopedia]'' |publisher=Magyar Könyklub |isbn=963-547-085-1 |language=hu |ref=harv}}
* {{cite book |last=Szabó |first=Péter Károly |editor1-last=Gujdár |editor1-first=Noémi |editor2-last=Szatmáry |editor2-first=Nóra |title=Magyar királyok nagykönyve: Uralkodóink, kormányzóink és az erdélyi fejedelmek életének és tetteinek képes története ''[Encyclopedia of the Kings of Hungary: An Illustrated History of the Life and Deeds of Our Monarchs, Regents and the Princes of Transylvania]'' |publisher=Reader's Digest |year=2012 |pages=188–189 |chapter=Báthory András |isbn=978-963-289-214-6 |language=hu |ref=harv}}
{{Refend}}

{{s-start}}
{{s-hou|[[Báthory|House of Báthory]]||1562/63|3 November|1599}}
{{s-rel|ca}}
{{s-bef|before=[[Marcin Kromer]]}}
{{s-ttl|title=[[List of bishops of Warmia|Prince-Bishop of Warmia]]|years=1589–1599}}
{{s-aft|after=[[Piotr Tylicki]]}}
|-
{{s-reg}}
{{s-bef|before=[[Sigismund Báthory]]}}
{{s-ttl|title=[[Prince of Transylvania]]|years=1599}}
{{s-vac|reason=[[Lands of the Hungarian Crown|Land of the Hungarian Crown]]|next=[[Sigismund Báthory]]}}
{{s-end}}

{{Authority control}}

{{DEFAULTSORT:Bathory, Andrew}}
[[Category:Monarchs of Transylvania]]
[[Category:Bishops of Warmia|Bathory]]
[[Category:Ecclesiastical senators of the Polish–Lithuanian Commonwealth]]
[[Category:1560s births]]
[[Category:1599 deaths]]
[[Category:Báthory family|Andrew]]
[[Category:Canons of Warmia]]
[[Category:Polish indigenes]]
[[Category:Polish Princes of the Holy Roman Empire]]
[[Category:Hungarian cardinals]]
[[Category:1590s in Romania]]
[[Category:16th-century Roman Catholic bishops]]
[[Category:16th-century Hungarian nobility]]