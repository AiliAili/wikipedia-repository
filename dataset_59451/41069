{{Orphan|date=September 2015}}

The right to be free from unreasonable [[search and seizure]] is well recognised by the international human rights community.<ref>International Convention on Civil and Political Rights, Art 17.</ref> Section 21 of the [[New Zealand Bill of Rights Act 1990]] (NZBoRA 1990) incorporates this right into [[Law of New Zealand|New Zealand law]], stating that: "Everyone has the right to be secure against unreasonable search or seizure, whether of the person, property, or correspondence or otherwise."

This right to be free from unreasonable [[search and seizure]] is primarily concerned with protecting the privacy interests of individuals against intrusions by the State. The [[Court of Appeal of New Zealand]]<ref>Williams v Attorney-General [2007] NZCA 52.</ref> and [[Supreme Court of New Zealand]]<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305, (2011) 25 CRNZ 326.</ref> have both recognised that the protection of privacy is core purpose of s21 NZBoRA 1990. Chief Justice Elias described the right protected as “the right to be let alone”<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305, (2011) 25 CRNZ 326, at [10].</ref> however it currently does not provide a full [[right to privacy]] of the kind in Article 17 of the [[International Convention on Civil and Political Rights]].<ref>Helu v Immigration and Protection Tribunal [2015] NZSC 28, at [118].</ref> Individuals subjected to a search or seizure that is unreasonable they may have a claim for a remedy, generally the exclusion of any evidence obtained by the search or seizure.

An analysis of whether an unreasonable search of seizure has occurred under section 21 is a two-step process of asking:

# Was there a search or seizure?
# Was the search or seizure unreasonable?

Unlike other rights protected under the NZBoRA 1990 there are no possible grounds for a reasonable limitation on section 21 under section 5 of the NZBoRA 1990 as if a search or seizure is unreasonable it is contradictory to say it could then be reasonably justified.<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305, (2011) 25 CRNZ 326, at [162].</ref>

==Search and Surveillance Act 2012==
While the NZBORA 1990 establishes the overall right to be free from unreasonable search and seizure the [[Search and Surveillance Act 2012]] provides the statutory framework for the practical application of the law in this area in New Zealand. Searches and seizures that do not comply with the requirements of the Act will be unlawful. The purpose section of the Act states that these requirements “recognise the importance of the rights and entitlements affirmed in other enactments including the New Zealand Bill of Rights Act 1990”.<ref>Search and Surveillance Act 2012, s5.</ref> The provisions of the Search and Surveillance Act 2012 accommodate the need to protect against unreasonable search and seizure. Therefore, cases involving possible unreasonable searches or seizures will involve significant consideration of whether the Search and Surveillance Act 2012 has been complied with.{{citation needed|date=May 2015}}

===Unlawfulness vs unreasonableness===
If a search or seizure is unlawful under the Search and Surveillance Act 2012 then it is also likely to be considered an unreasonable search or seizure. However, the concepts of unlawfulness and unreasonableness are independent and it is therefore possible that, for example, a lawful search can be unreasonable.<ref>R v Williams [2007] 3 NZLR 207, (2007) 23 CRNZ 1 (CA).</ref> Despite this, the courts have said that in terms of the most common remedy of exclusion of evidence an unlawful search should usually be considered an unreasonable search. Elias CJ went so far as to say that unlawful searches should automatically be unreasonable.<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305,(2011) 25 CRNZ 326, at [50].</ref> An exception is that where the search is only unlawful on minor or technical grounds it may still be reasonable.<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305,(2011) 25 CRNZ 326, at [174].</ref>

==Search==
The Search and Surveillance Act 2012 does not provide a definition for the term "search". While the courts have not formed a single definition, Elias CJ has said that police investigation which invades private space where individuals have an [[expectation of privacy]] constitutes a search.<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305,(2011) 25 CRNZ 326, at [10].</ref> McGrath J recognised “search” has a broad meaning in terms of section 21 of “an examination or investigation for the purpose of obtaining evidence”.<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305,(2011) 25 CRNZ 326, at [164].</ref> It is not settled whether a reasonable expectation of privacy must exist for there to be a search. In many cases there will be little difficulty in deciding that there has been a search as the Search and Surveillance Act 2012 specifically identifies and gives procedure for most common types of search. Common activities that are searches include:<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305,(2011) 25 CRNZ 326, at [165].</ref>

* Entry into and inspection of buildings, enclosed spaces or vehicles.
* Physical examination of a person.
* Taking bodily samples.<ref>R v Whimp [2007] NZCA 111.</ref>
* Internal examination of personal property (for example a bag or wallet).

===Visual observation===
An area of difficulty is whether simply visually observing something amounts to a search. If a police officer simply sees something relating to an offence in the course of their normal duty without any conscious effort to find it then there will generally not be a search. However, cases have shown that even where there is some conscious action there may not be a search as requesting an individual hold their hands out to be observed<ref>R v Yeung (HC Auckland CRI-2006-092-10945, 22 May 2009).</ref> or hold a bicycle up so the serial number could be seen<ref>Everitt v Attorney-General [2002] 1 NZLR 82.</ref> were both not seen to be searches.

===Covert surveillance===
The use of surveillance equipment and whether covert recording amounts to a search has been a significant issue for New Zealand courts. In ''Tararo v R'' (2010)<ref>[2010] NZSC 157, [2012] 1 NZLR 145, (2010) 25 CRNZ 317.</ref> the Supreme Court held that an undercover police officer video recording a drug transaction was not a search due to the low expectation of privacy. Covert surveillance of public areas will generally not be search due to the low expectation of privacy.<ref>Lorigan v R [2012] NZCA 264, at [22].</ref> In contrast, surveillance of private property will likely be a search due to the high privacy interests.<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305, (2011) 25 CRNZ 326.</ref> The Search and Surveillance Act 2012 now provides clear statutory guidance for when covert police surveillance can lawfully occur.<ref>Search and Surveillance Act 2012, Part 3, Subpart 1.</ref>

==Seizure==
There is no statutory definition of the term “seizure” in the Search and Surveillance Act 2012. In the Supreme Court Blanchard J defined seizure as simply the removing of something from the possession of someone else.<ref>Ngan v R [2007] NZSC 105, [2008] 2 NZLR 48, at [106].</ref> Seizure can involve the taking into custody of personal property including vehicles and clothing. It can also relate to detaining individuals in order to search or obtain evidence from them. Any seizure must be for a legitimate law enforcement purpose in order to be reasonable,<ref>Attorney-General v P F Sugrue Ltd [2004] 1 NZLR 207, (2003) 7 HRNZ 137.</ref> this will generally mean the seizure must be authorised by an empowering statute. Most cases in New Zealand under s21 have focused on unreasonable searches rather than seizures.

==Search warrants==
Searches by police and other law enforcement agencies must, in most cases, be carried out under a warrant in order to be lawful.<ref>Dotcom v Attorney-General [2014] NZSC 199, at [71].</ref> In order for a search warrant to be issued there must be:<ref>Search and Surveillance Act 2012, s6.</ref>

* “Reasonable grounds to suspect” the commission of an offence punishable by imprisonment, and 
* “Reasonable grounds to believe” evidence related to the offence will be found in the search location.

What amounts to “reasonable grounds” is not defined in the Search and Surveillance Act 2012, however a search warrant will be invalid only where the application for it was insufficient or it is so defective as to be likely to mislead the affected parties.<ref>Search and Surveillance Act 2012, s107.</ref> Part 4, subpart 3 of the Search and Surveillance Act 2012 sets out the requirements for lawfully carrying out a search once a warrant has been obtained. A search conducted without a search warrant or outside of the terms of a search warrant is highly likely to be unlawful and unreasonable.

Warrantless searches are lawful in certain circumstances under Part 2 of the Search and Surveillance Act 2012. For example, where they are incidental to an arrest<ref>Search and Surveillance Act 2012, s11 and Part 3, Subpart 4</ref> or drug offences.<ref>Search and Surveillance Act 2012, Part 2, Subpart 9</ref>

==Implied license==
Generally police officers must have statutory authorisation in order to enter on to private property. Police entry on to property outside of statutory powers may be lawful and reasonable where an implied license exists. An implied license permits an officer to “go to the door of private premises in order to make inquiry of an occupier for any reasonable purpose”.<ref>Tararo v R [2010] NZSC 157, [2012] 1 NZLR 145, (2010) 25 CRNZ 317, at [14]</ref> This is significant as a police officer may find evidence of an offence while on the property under the implied license. Finding this evidence will not be an unlawful search as a result of trespass as the officer is lawfully on the property, this was the scenario in ''Tararo v R'' (2010)<ref>NZSC 157, [2012] 1 NZLR 145, (2010) 25 CRNZ 317.</ref>

For this reason the scope of implied licenses is strictly limited in New Zealand. The Supreme Court has said an implied license is unlikely to exist if the purpose of the entry was not to make a reasonable inquiry or if it cannot be assumed that the occupier would have consented to the police entering had they known the particular purpose for the entry.<ref>Hamed v R [2011] NZSC 101, [2012] 2 NZLR 305, (2011) 25 CRNZ 326.</ref> An implied license can extend to allow entry into a private premises in certain circumstances but this will be in the minority of cases and there will generally be no right of access even where, for example, the door to the premises is open.<ref>R v Pou [2002] 3 NZLR 637, (2002) 19 CRNZ 563.</ref> An implied license can be revoked by the occupier through a clear unequivocal revocation, further, the existence of gates or signs may amount to a physical revocation.<ref>R v Ratima (1999) 17 CRNZ 227, (1999) 5 HRNZ 495.</ref>

==Consent to searches==
A search with no statutory authorisation may be lawful where consent to the search has been given. A search by consent will only be lawful if all of the following are satisfied:<ref>Search and Surveillance Act 2012, s94.</ref>

* The search is for one of the purposes given in s92 of the Search and Surveillance Act 2012, and
* The searching officer has advised the person giving consent of the reason for the proposed search and that they may either give or refuse consent,<ref>Search and Surveillance Act 2012, s93.</ref> and
* The person giving consent has legal authority to give that consent.

However, even if valid consent to a search has been given it does not automatically make the search reasonable. A lack of entitlement to conduct the search or the manner in which the search is conducted may still make the search unreasonable under s21.<ref>Cropp v Judicial Committee [2008] NZSC 46, [2008] 3 NZLR 774, at [18].</ref>

==Remedies==

===Exclusion of evidence===
The primary remedy for breaches of the right to be free from unreasonable search and search is the exclusion of evidence obtained by the search or seizure. This remedy is provided by s30 of the Evidence Act 2006 where evidence has been “improperly obtained”. Evidence is “improperly obtained” if it has been obtained in breach of s21 of the NZBoRA 1993 or the various regulations of the Search and Surveillance Act 2012.<ref>Evidence Act 2006, s30 (5).</ref> S30 requires the judge to conduct a balancing exercise to decide whether the exclusion of the evidence is a proportionate response to the impropriety or breach of s21 committed. If the judge decides exclusion is proportionate to the impropriety then the evidence must be excluded. This means that in some cases even where a search is unreasonable evidence obtained may still be admitted if excluding it would not be proportionate to the breach.

The factors that the judge will have regard to in the balancing process are given in s30 (3) of the Evidence Act 2006, these include:

* The nature of the breach of s21, in particular, whether it was deliberate, reckless, or done in bad faith.
* The nature and quality of the improperly obtained evidence.
* The seriousness of the offence with which the defendant is charged.
* Whether there were any other investigatory techniques available that would not have breached s21.
* Whether there are alternative remedies to exclusion of the evidence which can adequately provide redress to the defendant.
* Whether there was any urgency in obtaining the improperly obtained evidence.

A useful example of this process in action is ''[[Hamed & Ors v R]]'' (2011)<ref>[2011] NZSC 101, [2012] 2 NZLR 305, (2011) 25 CRNZ 326.</ref> where police unlawfully filmed activities on private land and parts of the evidence obtained were ruled admissible while other parts were inadmissible under the balancing process

===Damages===
In rare cases damages may also be available for breaches of s21 of the NZBoRA 1993 where the court wishes to highlight its disapproval of a breach of a fundamental right.<ref>Forrest v Attorney-General [2012] NZCA 125, [2012] NZAR 798. Van Essen v Attorney-General [2013] NZHC 917.</ref>

==References==
{{reflist|30em}}

{{ course assignment | course = Education Program:University of Canterbury/International Human Rights Law (Semester One 2015) | term = Spring 2015 }}

[[Category:Search and seizure case law]]
[[Category:New Zealand law]]