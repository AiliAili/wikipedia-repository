{{Infobox journal
| cover  = [[File:Journal AMS.jpg|200px]]
| editor = [[Brian Conrad]], [[Simon Donaldson]], [[Pavel Etingof]], [[Sergey Fomin]], [[Maryam Mirzakhani]], [[Assaf Naor]], [[Igor Rodnianski]]
| discipline = Pure and applied mathematics
| former_names = 
| abbreviation = J. Am. Math. Soc.
| publisher = [[American Mathematical Society]]
| country = United States
| frequency = Quarterly
| history = 1988-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.ams.org/publications/journals/journalsframework/jams
| link1  = http://www.ams.org/jourcgi/jrnl_toolbar_nav/jams_all
| link1-name = Online access and archive 
| link2  = 
| link2-name = 
| JSTOR  = 
| OCLC  =1088-6834
| LCCN  =88648217 
| CODEN  = 
| ISSN  =1088-6834 
| eISSN  =0894-0347
}}
The '''''Journal of the American Mathematical Society''''' ('''''JAMS'''''), is a quarterly [[peer review|peer-reviewed]] [[mathematical journal]] published by the [[American Mathematical Society]]. It was established in January 1988.

== Abstracting and indexing ==
This journal is abstracted and indexed in:<ref>[http://www.ams.org/publications/journals/journalsframework/aboutjams Indexing and archiving notes]. 2011. American Mathematical Society.</ref>
* [[Mathematical Reviews]] 
* [[Zentralblatt MATH]] 
* [[Science Citation Index]] 
* ISI Alerting Services 
* CompuMath Citation Index 
* [[Current Contents]]/Physical, Chemical & Earth Sciences.

== See also ==
* ''[[Bulletin of the American Mathematical Society]]''
* ''[[Memoirs of the American Mathematical Society]]''
* ''[[Notices of the American Mathematical Society]]''
* ''[[Proceedings of the American Mathematical Society]]''
* ''[[Transactions of the American Mathematical Society]]''

==References==
{{reflist}}

== External links ==
* {{Official website|http://www.ams.org/journals/jams}}

[[Category:American Mathematical Society academic journals]]
[[Category:Mathematics journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1988]]