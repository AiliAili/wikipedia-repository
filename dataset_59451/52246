{{Infobox journal
| title = Mechanisms of Development
| formernames = Mechanisms of Development', Cell Differentiation and Development, Cell Differentiation
| cover = [[File:Mechanisms of Development.gif]]
| editor = [[D. Wilkinson]]
| discipline = [[Developmental biology]]
| abbreviation = Mech. Dev.
| publisher = [[Elsevier]]
| country =
| frequency = Monthly
| history = 1972–present
| openaccess = [[Delayed open access journal|Delayed]], after 12 months
| license =
| impact = 2.041
| impact-year = 2015
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/506090/description
| link1 = http://www.sciencedirect.com/science/journal/09254773
| link1-name = Online archive
| link2 = http://www.elsevier.com/wps/find/journaldescription.cws_home/628039/description#description
| link2-name = ''Gene Expression Patterns''
| JSTOR =
| OCLC = 22927690
| LCCN = 92645804
| CODEN = MEDVE6
| ISSN = 0925-4773
| eISSN = 1872-6356
}}
'''''Mechanisms of Development''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering all aspects of [[developmental biology]]. It is the official journal of the [[International Society of Developmental Biologists]] and is published by [[Elsevier]]. The journal was established in 1972 as '''''Cell Differentiation''''' and was renamed '''''Cell Differentiation and Development''''' in 1988. It acquired its current name in December 1990. The [[editor-in-chief]] is [[D. Wilkinson]] ([[National Institute for Medical Research]]). A separate section of the journal, '''''Gene Expression Patterns''''', covers research on [[cloning]] and [[gene expression]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Elsevier BIOBASE]]
*[[Biological & Agricultural Index]]
*[[Biological Abstracts]]
*[[Biosis Previews]]
*[[Chemical Abstracts]]
*[[Current Awareness in Biological Sciences]]
*[[Current Contents]]/Life Sciences
*[[EMBASE]]
*[[EMBiology]]
*[[Genetics Abstracts]]
*[[MEDLINE]]
*[[PASCAL (database)|PASCAL]]
*[[FRANCIS]]
*[[Science Citation Index]]
*[[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.041,<ref>{{cite book |year=2016 |chapter=Mechanisms of Development |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> whereas ''Gene Expression Patterns'' has an impact factor of 1.485.<ref>{{cite book |year=2016 |chapter=Gene Expression Patterns |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/506090/description}} (''Mechanisms of Development'')
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/628039/description#description}} (''Gene Expression Patterns'')

[[Category:Publications established in 1972]]
[[Category:Monthly journals]]
[[Category:Developmental biology journals]]
[[Category:Elsevier academic journals]]
[[Category:English-language journals]]