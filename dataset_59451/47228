'''Coordinate descent''' is a [[derivative-free optimization]] [[algorithm]]. To find a [[local minimum]] of a function, one does [[line search]] along one [[Coordinate system|coordinate]] direction at the current point in each iteration. One uses different coordinate directions cyclically throughout the procedure.

==Description==

Coordinate descent is based on the idea that the minimization of a multivariable function <math>F(\mathbf{x})</math> can be achieved by minimizing it along one direction at a time, i.e., solving univariate (or at least much simpler) optimization problems in a loop.<ref name="wright">{{Cite journal |last=Wright |first=Stephen J. |title=Coordinate descent algorithms |journal=Mathematical Programming |volume=151 |issue=1 |year=2015 |pages=3–34 |arxiv=1502.04759 |doi=10.1007/s10107-015-0892-3}}</ref> In the simplest case of ''cyclic coordinate descent'', one cyclically iterates through the directions, one at a time, minimizing the objective function with respect to each coordinate direction at a time. That is, starting with initial variable values

: <math>\mathbf{x}^0 = (x^0_1, \ldots, x^0_n)</math>,

round <math>k+1</math> defines <math>\mathbf{x}^{k+1}</math> from <math>\mathbf{x}^k</math> by iteratively solving the single variable optimization problems

:<math>x^{k+1}_i = \underset{y\in\mathbb R}{\operatorname{arg\,min}}\; f(x^{k+1}_1, \dots, x^{k+1}_{i-1}, y, x^k_{i+1}, \dots, x^k_n)</math>

for each variable <math>x_i</math> of <math>\mathbf{x}</math>, for <math>i</math> from 1 to <math>n</math>.

Thus, one begins with an initial guess <math>\mathbf{x}^0</math> for a local minimum of <math>F</math>, and gets a sequence
<math>\mathbf{x}^0, \mathbf{x}^1, \mathbf{x}^2, \dots</math> iteratively.

By doing [[line search]] in each iteration, one automatically has

:<math>F(\mathbf{x}^0)\ge F(\mathbf{x}^1)\ge F(\mathbf{x}^2)\ge \dots.</math>

It can be shown that this sequence has similar convergence properties as steepest descent. No improvement after one cycle of [[line search]] along coordinate directions implies a stationary point is reached.

This process is illustrated below.

[[File:Coordinate descent.svg|center|500px]]

===Differentiable case===
In the case of a [[continuously differentiable]] function {{mvar|F}}, a coordinate descent algorithm can be [[pseudocode|sketched]] as:{{r|wright}}

<div style="margin-left: 35px; width: 600px">
{{framebox|blue}}
* Choose an initial parameter vector {{math|'''x'''}}.
* Until convergence is reached, or for some fixed number of iterations:
** Choose an index {{mvar|i}} from {{math|1}} to {{mvar|n}}.
** Choose a step size {{mvar|α}}. 
** Update {{mvar|x<sub>i</sub>}} to {{math|''x<sub>i</sub>'' − {{mvar|α}}{{sfrac|∂''F''|∂''x<sub>i</sub>''}}('''x''')}}.
{{frame-footer}}
</div>

The step size can be chosen in various ways, e.g., by solving for the exact minimizer of {{math|''f''(''x<sub>i</sub>'') {{=}} ''F''('''x''')}} (i.e., {{mvar|F}} with all variables but {{mvar|x<sub>i</sub>}} fixed), or by traditional line search criteria.{{r|wright}}

==Limitations==
Coordinate descent has two problems. One of them is having a non-[[Smoothness|smooth]] multivariable function. The following picture shows that coordinate descent iteration may get stuck at a non-[[stationary point]] if the level curves of a function are not smooth. Suppose that the algorithm is at the point {{math|(-2, -2)}}; then there are two axis-aligned directions it can consider for taking a step, indicated by the red arrows. However, every step along these two directions will increase the objective function's value (assuming a minimization problem), so the algorithm will not take any step, even though both steps together would bring the algorithm closer to the optimum. 

[[File:Nonsmooth coordinate descent.svg|center|500px]]The other problem is difficulty in parallelism. Since the nature of Coordinate Descent is to cycle through the directions and minimize the objective function with respect to each coordinate direction, Coordinate Descent is not an obvious candidate for massive parallelism. Recent research works have shown that massive parallelism is applicable to Coordinate Descent by relaxing the change of the objective function with respect to each coordinate direction. <ref>{{Cite journal|last=Zheng|first=J.|last2=Saquib|first2=S. S.|last3=Sauer|first3=K.|last4=Bouman|first4=C. A.|date=2000-10-01|title=Parallelizable Bayesian tomography algorithms with rapid, guaranteed convergence|url=http://ieeexplore.ieee.org/document/869186/|journal=IEEE Transactions on Image Processing|volume=9|issue=10|pages=1745–1759|doi=10.1109/83.869186|issn=1057-7149}}</ref><ref>{{Cite journal|last=Fessler|first=J. A.|last2=Ficaro|first2=E. P.|last3=Clinthorne|first3=N. H.|last4=Lange|first4=K.|date=1997-04-01|title=Grouped-coordinate ascent algorithms for penalized-likelihood transmission image reconstruction|url=http://ieeexplore.ieee.org/document/563662/|journal=IEEE Transactions on Medical Imaging|volume=16|issue=2|pages=166–175|doi=10.1109/42.563662|issn=0278-0062}}</ref><ref>{{Cite journal|last=Wang|first=Xiao|last2=Sabne|first2=Amit|last3=Kisner|first3=Sherman|last4=Raghunathan|first4=Anand|last5=Bouman|first5=Charles|last6=Midkiff|first6=Samuel|date=2016-01-01|title=High Performance Model Based Image Reconstruction|url=http://doi.acm.org/10.1145/2851141.2851163|journal=Proceedings of the 21st ACM SIGPLAN Symposium on Principles and Practice of Parallel Programming|series=PPoPP '16|location=New York, NY, USA|publisher=ACM|pages=2:1–2:12|doi=10.1145/2851141.2851163|isbn=9781450340922}}</ref>

==Applications==
Coordinate descent algorithms are popular with practitioners owing to their simplicity, but the same property has led optimization researchers to largely ignore them in favor of more interesting (complicated) methods.{{r|wright}} An early application of coordinate descent optimization was in the area of computed tomography<ref>{{cite journal|last1=Sauer|first1=Ken|last2=Bouman|first2=Charles|title=A Local Update Strategy for Iterative Reconstruction from Projections|journal=IEEE Trans. on Sig. Proc.|date=February 1993|volume=41|issue=2|page=534-548|doi=10.1109/78.193196|url=https://engineering.purdue.edu/~bouman/publications/orig-pdf/sp2.pdf}}</ref> where it has been found to have rapid convergence<ref>{{cite journal|last1=Yu|first1=Zhou|last2=Thibault|first2=Jean-Baptiste|last3=Bouman|first3=Charles|last4=Sauer|first4=Ken|last5=Hsieh|first5=Jiang|title=Fast Model-Based X-ray CT Reconstruction Using Spatially Non-Homogeneous ICD Optimization|journal=IEEE Trans. on Image Processing|date=January 2011|volume=20|issue=1|page=161-175|doi=10.1109/TIP.2010.2058811|url=https://engineering.purdue.edu/~bouman/publications/orig-pdf/tip28.pdf}}</ref> and was subsequently used for clinical multi-slice helical scan CT reconstruction.<ref>{{cite journal|last1=Thibault|first1=Jean-Baptiste|last2=Sauer|first2=Ken|last3=Bouman|first3=Charles|last4=Hsieh|first4=Jiang|title=A Three-Dimensional Statistical Approach to Improved Image Quality for Multi-Slice Helical CT|journal=Medical Physics|date=November 2007|volume=34|issue=11|page=4526-4544|doi=10.1118/1.2789499|url=https://engineering.purdue.edu/~bouman/publications/orig-pdf/medphys1.pdf}}</ref> Moreover, there has been increased interest in the use of coordinate descent with the advent of large-scale problems in [[machine learning]], where coordinate descent has been shown competitive to other methods when applied to such problems as training linear [[support vector machine]]s<ref>{{Cite book | last1 = Hsieh | first1 = C. J. | last2 = Chang | first2 = K. W. | last3 = Lin | first3 = C. J. | last4 = Keerthi | first4 = S. S. | last5 = Sundararajan | first5 = S. | doi = 10.1145/1390156.1390208 | chapter = A dual coordinate descent method for large-scale linear SVM | title = Proceedings of the 25th international conference on Machine learning - ICML '08 | pages = 408 | year = 2008 | isbn = 9781605582054 | pmid =  | pmc = | url = http://ntu.csie.org/~cjlin/papers/cddual.pdf}}</ref> (see [[LIBLINEAR]]) and [[non-negative matrix factorization]].<ref>{{Cite conference | last1 = Hsieh | first1 = C. J. | last2 = Dhillon | first2 = I. S. | doi = 10.1145/2020408.2020577 | title = Fast coordinate descent methods with variable selection for non-negative matrix factorization | conference = Proceedings of the 17th ACM SIGKDD international conference on Knowledge discovery and data mining - KDD '11 | pages = 
1064| year = 2011 | isbn = 9781450308137 | pmid =  | pmc = | url = http://www.cs.utexas.edu/~cjhsieh/nmf_kdd11.pdf}}</ref> They are attractive for problems where computing gradients is infeasible, perhaps because the data required to do so are distributed across computer networks.<ref>{{cite journal |last=Nesterov |first=Yurii |authorlink=Yurii Nesterov |title=Efficiency of coordinate descent methods on huge-scale optimization problems |journal=SIAM J. Optimization |volume=22 |issue=2 |year=2012 |pages=341–362 |url=http://www.ulouvain.be/cps/ucl/doc/core/documents/coredp2010_2web.pdf |doi=10.1137/100802001}}</ref>

==See also==
* [[Adaptive coordinate descent]]
* [[Conjugate gradient]]
* [[Gradient descent]]
* [[Line search]]
* [[Mathematical optimization]]
* [[Newton's method in optimization|Newton's method]]
* [[Stochastic gradient descent]] – uses one example at a time, rather than one coordinate

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

*{{Citation
 | last=Bezdek
 | first=J. C.
 | last2=Hathaway
 | first2=R. J.
 | last3=Howard
 | first3=R. E.
 | last4=Wilson
 | first4=C. A.
 | last5=Windham
 | first5=M. P.
 | year=1987
 | title=Local convergence analysis of a grouped variable version of coordinate descent
 | periodical=Journal of Optimization theory and applications
 | volume=54
 | issue=3
 | pages=471–477
 | doi=10.1007/BF00940196
 | publisher=Kluwer Academic/Plenum Publishers
}}
* Bertsekas, Dimitri P. (1999). ''Nonlinear Programming, Second Edition'' Athena Scientific, Belmont, Massachusetts. ISBN 1-886529-00-0.
* {{Citation
 | last=Canutescu
 | first=AA
 | last2=Dunbrack
 | first2=RL
 | title=Cyclic coordinate descent: A robotics algorithm for protein loop closure.
 | periodical=Protein science
 | year=2003
 | volume=12
 | issue=5
 | pages=963-72
 | pmid=12717019
 | doi=10.1110/ps.0242703
}}. 
*{{Citation
 | last=Luo
 | first=Zhiquan
 | last2=Tseng
 | first2=P.
 | year=1992
 | title=On the convergence of the coordinate descent method for convex differentiable minimization
 | periodical=Journal of Optimization theory and applications
 | volume=72
 | issue=1
 | pages=7–35
 | doi=10.1007/BF00939948
 | publisher=Kluwer Academic/Plenum Publishers
}}.
*{{Citation
 | last=Wu
 | first=TongTong
 | last2=Lange
 | first2=Kenneth
 | year=2008
 | title=Coordinate descent algorithms for Lasso penalized regression
 | periodical=The Annals of Applied Statistics
 | volume=2
 | issue=1
 | pages=224–244
 | doi=10.1214/07-AOAS147
 | publisher=Institute of Mathematical Statistics
}}.
*{{Citation
 | last=Richtarik
 | first=Peter
 | last2=Takac
 | first2=Martin
 | year=April 2011
 | title=Iteration complexity of randomized block-coordinate descent methods for minimizing a composite function
 | periodical=Mathematical Programming
 | doi=10.1007/s10107-012-0614-z
 | publisher=Springer
}}.
*{{Citation
 | last=Richtarik
 | first=Peter
 | last2=Takac
 | first2=Martin
 | year=December 2012
 | title=Parallel coordinate descent methods for big data optimization
 | periodical=arXiv:1212.0873
}}.

{{Optimization algorithms}}

[[Category:Gradient methods]]