{{refimprove|date=March 2011}}
[[File:Flag of the Antarctic Treaty.svg|thumb|right|Flag of the [[Antarctic Treaty]].]]

Prior to 2002 [[Antarctica]] had no official flag as the [[Condominium (international law)|condominium]] that governs the continent had not yet formally selected one even though a particular design was in widespread use. The consultative members of the [[Antarctic Treaty System]] officially adopted a flag and emblem in 2002, which is now the official symbol of the continent.<ref>{{cite web|url= http://www.ats.aq/devAS/info_measures_listitem.aspx?lang=e&id=289 | title = Decision 2 (2002) - ATCM XXV - CEP V, Warsaw}}</ref> Several unofficial designs have also been proposed.

== Proposed designs ==
Several sources have suggested various designs, including one by [[Graham Bartram]] and one by [[Whitney Smith]].

===Graham Bartram===
[[File:Flag of Antarctica.svg|thumb|right|The Graham Bartram design.]]
The [[Graham Bartram]] design uses the [[flag of the United Nations]] as its model. A plain white map of the continent on a blue background symbolizes neutrality (Bartram was well aware of the overlapping territorial claims of the [[United Kingdom]], [[Chile]], and [[Argentina]] when he designed the flag). This flag was actually flown on the Antarctic continent for the first time in 2002, when Ted Kaye (then editor of ''Raven'', the scholarly journal of the [[North American Vexillological Association]]) took several full-size flags in the Bartram design on an Antarctic cruise. The bases of Brazil, Ukraine, and the [[United Kingdom|UK]] all flew it from their flagpoles, making its raising "official". He presented a paper ("Flags Over Antarctica") which described the first flying of the Bartram design over Antarctica in Stockholm in 2003, at the 20th International Congress of [[Vexillology]]. It is perhaps the most popular flag for Antarctica, as seen by its prevalence on the [[Internet]].{{Citation needed|date=June 2014}}

===Whitney Smith===
[[File:Flag of Antarctica (Smith).svg|thumb|left|The Whitney Smith design.]]
The [[Whitney Smith]] design uses the high-visibility color [[orange (color)|orange]] as its background (it is the international rescue color, it contrasts the best against snow, and to avoid any confusion, is unlike almost any [[Gallery of sovereign-state flags|national flag on Earth]]). The emblem consists of several components. 'A' stands for Antarctica. The bottom segment of the globe represents Antarctica's "position" on Earth (according to the modern convention of drawing maps with [[north]] on top), while the two hands holding up the globe segment represent peaceful human use. The emblem is colored white to represent the snow and ice of Antarctica and is offset toward the [[hoist (flag)|hoist]] of the flag so as to maintain its integrity should the flag fray badly in the high winds prevalent upon the continent. However, there is no record of it ever being fabricated or used, despite being displayed in some atlases.{{citation needed|date=June 2012}}

==Territorial flags==
{{unreferenced section|date=January 2011}}
The nations of the Antarctic Treaty use their own national flags at their respective Antarctic research bases. Some nations however have their own flags for their Antarctic possessions.

===British Antarctic Territory===
{{main|Flag of the British Antarctic Territory|Coat of arms of the British Antarctic Territory}}
[[File:Flag of the British Antarctic Territory.svg|right|180px|thumb|British Antarctic Territory Flag]] The flag of the [[British Antarctic Territory]] is a plain [[White Ensign]] [[defaced (flag)|defaced]] by the Coat of Arms of the Territory. Other British territories in the Antarctic region are the [[Falkland Islands]] and [[South Georgia and the South Sandwich Islands]], which have their own flags (see [[Flag of the Falkland Islands]] and [[Flag of South Georgia and the South Sandwich Islands]]).
{{clear}}
{{anchor|French Southern and Antarctic Territories}}

===Adélie Land/French Southern and Antarctic Territories===
{{Expand section|date=February 2013}}[[File:Flag of the French Southern and Antarctic Lands.svg|right|180px|thumb|Flag of the Administrator of the French Southern and Antarctic Territories]] The flag of the Administrator (Commissioner) of the [[French Southern and Antarctic Territories]], which includes [[Adélie Land]], has the [[Flag of France|French tricolor]] in the [[canton (flag)|canton]] together with the Commissioner's arms. The coat of arms consists of five stars and the letters "TAAF" in a [[monogram]] (from the French name of the territory, ''Terres australes et antarctiques françaises''). The flag was adopted in 2007.<ref>[http://www.philateliedestaaf.fr/HTML/Timbres/loupe/loupe-PO/PO494.htm Philatelie des TAAF]</ref>
{{clear}}
{{anchor|Tierra del Fuego}}

===Argentine Antarctica/Tierra del Fuego===
{{Expand section|date=February 2013}}[[File:Flag of Tierra del Fuego province in Argentina.svg|right|180px|thumb|Tierra del Fuego flag]] The Argentine province of [[Tierra del Fuego (Argentina)|Tierra del Fuego]] includes [[Argentine Antarctica]] ([[25th meridian west|25 degrees W]] to [[74th meridian west|74 degrees W]]). The flag was adopted in 1999 as the result of a competition. It is a diagonal bicolor of sky blue and orange with an [[albatross]] in the center and the [[Southern Cross]] in the fly.
{{clear}}
{{anchor|Magallanes Region}}

===Chilean Antarctic Territory/Magallanes Region===
{{main|Flag of Magallanes}}
[[File:Flag of Magallanes, Chile.svg|right|180px|thumb|Magallanes Region flag]] The [[Antártica Chilena Province]] in the [[Magallanes Region]] includes the Chilean claim on the continent ([[53rd meridian west|53 degrees W]] to [[90th meridian west|90 degrees W]]). [[Puerto Williams]] is the capital of this province, which also includes the islands south of [[Tierra del Fuego]] and Cape Horn. The Magallanes Region's flag also has the [[Southern Cross]] appearing over a mountain range.
{{clear}}

===Ross Dependency===
[[File:Flag of the Ross Dependency (unofficial).svg|right|180px|thumb|Proposed Ross Dependency flag]]

Currently, only the [[New Zealand national flag]] serves in an official capacity in the Ross Dependency. The only other 'official' flag seen in photographs was the [[New Zealand Post]] flag to denote [[Scott Base]]'s post office.

[[Ross Dependency]] ([[New Zealand]]'s territorial claim in Antarctica) uses the [[Flag of New Zealand|New Zealand flag]], but [[vexillology|vexillologist]] [[James Dignan]]'s design concept was seen flying there at one time (when a friend of Dignan took it with him to [[Vanda Station]] in 1994). The New Zealand flag is the basis for his design, though with an 'Ice Blue' background representing the [[Ross Sea]], and the white horizontal bar at the bottom of the flag representing the [[Ross Ice Shelf]].{{Citation needed|date = July 2013}}
{{clear}}

===Other===
{{Gallery
|File:Flag of Australia.svg |[[Flag of Australia]], used by the [[Australian Antarctic Territory]]
|File:Flag of Norway.svg |[[Flag of Norway]], used by [[Queen Maud Land]] and [[Peter I Island]]
|File:Flag of New Zealand.svg |[[Flag of New Zealand]], used by [[Ross Dependency]]
|File:Flag of German Reich (1935–1945).svg |[[Flag of Nazi Germany]], used by the former German protectorate of [[New Swabia]]
}}

== References==

{{Reflist}}

== External links ==
*[http://www.crwflags.com/fotw/flags/aq!.html Antarctica — flag proposals]

{{Antarctica}}
{{Flags of Antarctica}}
{{Lists of flags}}

[[Category:Culture of Antarctica]]
[[Category:White Ensigns]]
[[Category:International flags|Antarctica]]
[[Category:Proposed flags|Antarctica]]
[[Category:Unofficial flags|Antarctica]]
[[Category:Flags of Antarctica| ]]