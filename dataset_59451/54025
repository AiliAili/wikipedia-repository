{{Use Australian English|date=January 2012}}

{{Infobox Union
| name = AUU
| affiliation = [[National Union of Students (Australia)|National Union of Students]]
| full_name = Adelaide University Union
| image = Adelaide_University_Union_Logo.png

| founded = 1895
| office = [[University of Adelaide]]
| people = President, Brodie Scott
General Manager, Gary Sutherland
| website = [https://www.auu.org.au/ www.auu.org.au]
| footnotes = 
}}

The '''Adelaide University Union''' (AUU) is a [[Students' union|student union]] at the [[University of Adelaide]], [[South Australia]]. It provides academic advocacy, welfare, and counselling services to students free of charge, funds the [[student newspaper]] ''[[On Dit]],'' and owns a number of commercial operations on campus. It also oversees the ''Student Representative Council'' (SRC), an organisationally separate body responsible for [[Student activism|student political representation]].

The AUU was founded in 1895 and as of 1971 is recognised as a [[statutory corporation]] under the legislation governing the University of Adelaide.

==Formal relationship with the University of Adelaide==
As of 2008, the Union relies on the University of Adelaide for the majority of its funding. This is a result of a funding agreement with the University.<ref>[[Adelaide University Union#Impact of VSU]]</ref>

The ultimate existence of the Union, and its relationship with the university, is governed by the ''University of Adelaide Act 1971''. This Act of the South Australian Parliament gives the [[University of Adelaide]] Council certain powers over the Union. The Union cannot alter its constitution or rules, or charge a membership fee, without the agreement of University Council. Furthermore, the Union is bound to provide University Council with its financial reports and budget for the coming calendar year prior to 1 December.<ref>Ihttp://www.adelaide.edu.au/policies/232/?dsn=policy.document;field=data;id=228;m=view University of Adelaide Act 1971</ref>

In 2009, the [[Adelaide University Sports Association]], previously an affiliate of the Union, began the process of disaffiliation from the Union, having secured a separate funding agreement with the University.<ref>http://www.theblacks.com.au/images/stories/draft%20constitution%20august%202009%20website%20copy.doc{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }} Sports Association Constitution proposed Aug 2009. No contains reference to Adelaide University Union</ref>

==Student governance==
[[File:Adelaide University Union Building 1930.jpg|right|thumb|Adelaide University Union Building 1930]] The Union is governed by a board of management. The board consists of 10 ordinary members, who are not also permanent staff of the Union, five of whom are elected annually on two year terms by the students of the University. The Board then elects several of its members to positions within the Union, such as Union President, Vice President, Student Media Committee Chair and Clubs Committee Chair.<ref>http://auu.org.au/Common/ContentWM.aspx?CID=130</ref>  Elections are held annually in September, with the Board-elect and officer bearers taking their positions on 1 December.

In 2008 the Union Board disaffiliated the now defunct [[Students' Association of the University of Adelaide]] (SAUA), which was replaced by the current Student Representative Council (SRC).

Notable past presidents include former South Australian Premier [[John Bannon]], former South Australia Attorney-General [[Chris Sumner]], Australia's first female Prime Minister [[Julia Gillard]] (1981–1982)<ref>{{Cite news |url=http://blogs.theage.com.au/thirddegree/archives/2010/07/rise_of_the_campus_pollies.html |title=Third Degree: Rise of the campus pollies |first=Erica |last=Cervini |date=16 July 2010 |publisher=The Age |location=Melbourne}}</ref><ref name="CrikeyList">{{Cite web |url=http://www.crikey.com.au/2010/10/01/crikey-list-which-mps-were-involved-in-student-politics |title=Crikey List: which MPs were involved in student politics? |publisher=Crikey.com.au |first=Andrew |last=Crook |date=1 October 2010 |accessdate=24 February 2011 }}</ref> and former South Australian Supreme Court Judges [[Elliott Johnston]] and [[Samuel Jacobs (judge)|Samuel Jacobs]]. The current Union President is Brodie Scott.

==Services and Activities==
The Union provides a range of activities as well as support services for Adelaide University students. This includes welfare and academic support services provided by Student Care Inc. The Union also provides Employment and Volunteering Services, support to over 150 student-run Clubs and Societies, and regular social events. The Union also continues to publish the student newspaper [[On Dit]], the third oldest student newspaper in Australia,<ref>http://www.union.adelaide.edu.au/student/representation/students.html</ref> as well as the longest-running Student Radio in the country, broadcasting six hours a week on Radio Adelaide.<ref>http://auu.org.au/Common/ContentWM.aspx?CID=100</ref>

==O'Week==
The Union coordinates the activities, events and entertainment for Adelaide University [[Orientation week|O'Week]].<ref>http://www.adelaide.edu.au/student/new/oweek/timetbl/auu.html</ref> O'Week is the week before lectures begin. During O'Week the Union coordinates a variety of events centering on activities, free stuff, entertainment, competitions and a huge area for clubs on the Math Lawns near the Union House and the Braggs building.

==Impact of VSU==
The post-[[Voluntary Student Unionism]] (VSU) period contained significant financial difficulties for the Union. Previously funded by compulsory fees paid by all students, the introduction of voluntary unionism resulted in a sharp drop in income for the Union. This resulted in grave financial difficulties. In late 2007 the Union handed control of Union House and the vast majority of the Union's commercial services to the University of Adelaide.<ref>[http://www.adelaide.edu.au/news/news23541.html UniBar set for an upgrade in 2008<!-- Bot generated title -->]</ref> This was in return for the University agreeing to fund the Union for a period of ten years. The University of Adelaide paid $1.2 million to the Union in the first year of this funding agreement, with future funding to be determined on a year-to-year basis. This gives the University final control over the size of the Union budget in any given year.

==References==
{{Reflist}}

==External links==
* [http://www.auu.org.au Adelaide University Union - Official site]

{{NUS}}
{{Use dmy dates|date=June 2013}}

[[Category:Students' unions in Australia]]