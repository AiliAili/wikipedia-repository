{{Use dmy dates|date=February 2015}}
{{Use South African English|date=February 2015}}
{{Infobox military award
| name             = Union of South Africa Commemoration Medal
| image            = [[File:Union of South Africa Commemoration Medal.jpg|300px]]
| caption          = 
| awarded_by       = [[George V|the Monarch of the United Kingdom and the British Dominions, and Emperor of India]]
| country          = {{flagicon|South Africa|1910}} [[Union of South Africa]]
| type             = Commemorative medal
| eligibility      = Military and civil leaders
| for              = Commemoration of the opening of the first Union Parliament
| campaign         = 
| status           = 
| description      = 
| motto            = 
| clasps           = 
| post-nominals    = 
| established      = 1910
| first_award      = 1910
| last_award       = 1911
| total            = 580
| posthumous       = 
| recipients       = 
| precedence_label = South African order of wear
| higher           = [[File:Flag of the United Kingdom.svg|25px]] [[Air Efficiency Award]]
| same             = 
| lower            = [[File:Flag of South Africa 1928-1994.svg|25px]] [[South African Medal for War Services]]
| related          = 
| image2           = [[File:Union of South Africa Commemoration Medal (ribbon).png|x29px]]
| caption2         = Ribbon Bar
}}

The '''Union of South Africa Commemoration Medal''' is a military and civil commemorative medal which was awarded to commemorate the opening of the first Union Parliament by the [[Duke of Connaught and Strathearn]] in 1910. It may be considered as the first of many independence medals which were instituted throughout the Commonwealth during the 20th century.<ref name="McCreery">{{cite book|last=McCreery|first=Christopher|authorlink=Christopher McCreery|title=Commemorative medals of the Queen's reign in Canada, 1952-2012|publisher=Dundurn Press|location=Toronto|isbn=9781459707566|pages=31, 40, 60}}</ref><ref name="SACA">{{cite book | title=South African Civil Awards | publisher=South African National Museum of Military History | author=Monick, S | year=1990 | pages=75–76}}</ref><ref name="UnionCivil">[http://www.geocities.ws/militaf/civ10.htm South African Medal Website - Civil - Union of South Africa] (Accessed 1 May 2015)</ref>

==Institution==
The Union of South Africa Commemoration Medal was instituted by [[George V|King George V]] in terms of Royal Warrant of 3 October 1910, published in Government Gazette no. 59 dated 29 November 1910. The medal was struck to commemorate the opening of the first Parliament of the [[Union of South Africa]] by the Duke of Connaught and it was awarded to military and civilian leaders who took part in the ceremonies to establish the Union of South Africa from the unification of the former Colonies of the [[Cape Colony|Cape of Good Hope]], [[Colony of Natal|Natal]], [[Transvaal Colony|Transvaal]] and the [[Orange River Colony]].<ref name="SACA"/><ref>{{cite book|title=The Medal Collector|year=1921|publisher=Dodd Mead and Company|location=New York|page=249}}</ref> 

==Award criteria==
The medal was awarded to members of the military and naval forces as well as certain civilian leaders who took a prominent part in the proceedings, and only on the recommendation of the Principal Secretary of State for the Colonies. A roll was to be kept and was to be closed six months after the date of the warrant, after which no award could be made except with Royal Approval in very special circumstances.<ref name="SACA"/><ref name="UnionCivil"/>

==Order of wear==
In the order of wear prescribed by the British [[Central Chancery of the Orders of Knighthood]], the Union of South Africa Commemoration Medal takes precedence after the [[Northern Ireland Prison Service Medal]] and before the [[Indian Independence Medal]].<ref name="London Gazette">{{londongazette|issue=56878|supp=y|startpage=3353|date=17 March 2003}}</ref>

===South Africa===
{{main|South African military decorations order of wear#Order of wear}}
With effect from 6 April 1952, when a new South African set of decorations and medals was instituted to replace the British awards used to date, the older British decorations and medals which were applicable to South Africa continued to be worn in the same order of precedence but, with the exception of the [[Victoria Cross]], took precedence after all South African decorations and medals awarded to South Africans on or after that date. Of the official British medals which were applicable to South Africans, the Union of South Africa Commemoration Medal takes precedence as shown.<ref name="London Gazette"/><ref name="Notice1982">Government Notice no. 1982 of 1 October 1954 - ''Order of Precedence of Orders, Decorations and Medals'', published in the Government Gazette of 1 October 1954.</ref><ref name="Gazette 27376">Republic of South Africa Government Gazette Vol. 477, no. 27376, Pretoria, 11 March 2005, {{OCLC|72827981}}</ref>

[[File:Ribbon - Air Efficiency Award.png|x37px|Air Efficiency Award]] [[File:Union of South Africa Commemoration Medal (ribbon).png|x37px|Union of South Africa Commemoration Medal]] [[File:Ribbon - South African Medal for War Services.png|x37px|South African Medal for War Services]] 
* Preceded by the [[Air Efficiency Award]].
* Succeeded by the [[South African Medal for War Services]].

==Description==
The medal, which was awarded unnamed, was struck in silver and is 36 millimetres in diameter with a raised rim.<ref name="McCreery"/><ref name="SACA"/><ref name="UnionCivil"/>

;Obverse
The obverse bears the effigy of King George V, facing left and surrounded by the legend: "GEORGIVS V BRITT:OMN:REX ET IND:IMP:". The medal is attached with a swiveling claw suspension affixed to the medal by means of a pin through the upper edge of the medal. The suspender is fitted with a large ring.<ref name="McCreery"/><ref name="SACA"/><ref name="UnionCivil"/>

;Reverse
The reverse depicts the god Mercury, as the god of commerce and prosperity, forging links of a chain on an anvil, with four links each bearing the name of a Province of the Union of South Africa, "CAPE OF GOOD HOPE", "NATAL", "TRANSVAAL" and "ORANGE RIVER". The year "1910" underneath is entwined by an olive branch. Around the circumference and encircling the image is the inscription "TO COMMEMORATE THE UNION OF SOUTH AFRICA".<ref name="SACA"/><ref name="UnionCivil"/><ref name="greenwich">{{cite web|title=Union of South Africa Commemoration Medal|url=http://collections.rmg.co.uk/collections/objects/207956.html|publisher=Royal Museums Greenwich|accessdate=19 June 2013}}</ref>

;Ribbon
The ribbon is 38 millimetres wide, with two 10 millimetres wide orange bands separated by an 18 millimetres wide dark blue band.<ref name="UnionCivil"/><ref name="greenwich"/>

==Recipients==
The Roll shows that altogether 580 medals were awarded. Of these, 21 went to members of the Royal Navy, 52 to the British Army, 41 to South African forces, 465 to civilians of which 31 were women, and one to a member of foreign services.<ref name="SACA"/><ref name="UnionCivil"/>

==References==
{{Reflist|30em}}
{{South African military decorations and medals}}
{{Orders, decorations, and medals of the United Kingdom}}

[[Category:Civil awards and decorations of South Africa|406]]
[[Category:Civil awards and decorations of the United Kingdom]]
[[Category:Military decorations and medals of South Africa]]
[[Category:Military decorations and medals of South Africa in order of precedence|418]]
[[Category:Military decorations and medals of South Africa pre-1952]]
[[Category:Military awards and decorations of the United Kingdom]]