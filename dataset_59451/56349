The '''''Comparative Labor Law and Policy Journal''''' is a [[law review|law journal]] which publishes articles in the field of comparative and transnational labor and employment law.

The journal was founded in 1976 at the [[University of Pennsylvania Law School]] as the ''Comparative Labor Law Journal''. In 1997, the journal moved to the [[University of Illinois at Urbana-Champaign|University of Illinois]] and rechristened the ''Comparative Labor Law and Policy Journal'' to widen its scope.

The journal publishes comparative analysis articles on labor law, employment policy, [[labor economics]], worker migration, and [[social security]] issues.  Many articles focus on legal systems in [[developing country|developing countries]] or [[post-colonial]] nations with emerging or new legal systems.

The target audience for the journal comprises academics, practicing attorneys, policy makers, students, workers and labor movement officials and activists.  The journal's stated policy is to make the publication readable and of practical value to officials in developing countries.

The journal is published quarterly by the [[UIUC College of Law|University of Illinois College of Law]] and the [[International Society for Labor Law and Social Security]].

==External links==
*[http://www.law.uiuc.edu/publications/cll&pj/ Comparative Labor Law and Policy Journal Web site]
*[http://www.law.uiuc.edu/ University of Illinois College of Law Web site]
*[http://www.ilo.org/public/english/dialogue/ifpdial/isllss/91.htm International Society for Labor Law and Social Security]

{{University of Illinois at Urbana-Champaign campus}}

[[Category:Labour law journals]]
[[Category:Publications established in 1976]]
[[Category:University of Illinois at Urbana–Champaign]]
[[Category:United States labor law]]


{{law-journal-stub}}