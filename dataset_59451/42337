__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name           = Bernard 82
|image          = Bernard 82-B.jpg
|caption        = Bernard 82-B.3 (ca 1934)
}}{{Infobox Aircraft Type
|type           = Long-range [[bomber]]
|national origin= [[France]]
|manufacturer   = [[Société des Avions Bernard]]
|designer       =
|developed from = [[Bernard 80 GR]]
|first flight   = 11 December 1933
|number built   = 2
}}
|}
The '''Bernard 82''' was a [[France|French]] single-engined long-range [[monoplane]] [[bomber]] designed and built by [[Société des Avions Bernard]]. Only two prototypes were built and the type did not enter production.<ref name="orbis"/>

==Design and development==
The Bernard 82 was developed from the long-range [[Bernard 80 GR]], which had been designed to set long-distance-flight records.<ref name="orbis" /> The all-metal Bernard 82 was a three-seat long-range bomber, known at the time as a ''bombardier de represaillies'' or reprisal bomber.<ref name="orbis"/> The cantilever mid-wing monoplane was powered by an {{convert|860|hp|kW|0|abbr=on}} [[Hispano-Suiza 12Ybrs]] inline piston engine.<ref name="orbis"/> The first prototype flew from [[Paris–Le Bourget Airport|Le Bourget]] on 11 December 1933, and in March 1932 was joined by the second prototype.<ref name="orbis"/> Flight testing showed the twin lateral radiators to be inadequate; they were replaced by front-mounted radiators. Landing-gear deficiencies were the most persistent difficulty encountered during testing.<ref name="orbis"/> The retractable landing gear regularly failed; resulting in wheels-up landings. The problem was never resolved, and testing was halted in mid-1935; the production contract for ten aircraft was cancelled.<ref name="orbis"/>

In August 1936 the second prototype was re-engined with a {{convert|650|hp|kW|0|abbr=on}} [[CLM Lille 6AS]], a licence-built [[Junkers Jumo]] diesel engine.<ref name="orbis"/> The diesel-powered aircraft, redesignated '''Bernard 86''',  was entered into the 1936 Paris-Saigon-Paris air race. The race was run in September, but the Bernard was not ready by then, so it was not used. There were no further flight tests; the units were scrapped.<ref name="orbis"/>

==Specifications==
{{aerospecs|ref=<ref name="orbis"/><small>The [[Illustrated Encyclopedia of Aircraft]]</small>
|met or eng?=met
|crew=3
|capacity=
|length m=17.98
|length ft=58
|length in=11¾
|span m=27.10
|span ft=88
|span in=11
|height m=
|height ft=
|height in=
|wing area sqm=90.00
|wing area sqft=968.75
|empty weight kg=2823
|empty weight lb=6224
|gross weight kg=5083
|gross weight lb=11206
|eng1 number=1
|eng1 type=[[Hispano-Suiza 12Ybrs]] inline piston engine
|eng1 kw=641
|eng1 hp=860
|max speed kmh=317
|max speed mph=197
|cruise speed kmh=260
|cruise speed mph=162
|range km=2800
|range miles=1740
|ceiling m=
|ceiling ft=
|climb rate ms=
|climb rate ftmin=
|armament1=Single or twin 7.7mm (0.303in) dorsal machine-guns 
|armament2=4×200kg (441b) and 2×100kg (220lb) bombs (internal)
}}

==See also==
{{Commons category|Bernard 82}}
{{aircontent|
|related=
|similar aircraft=*[[Tupolev DB-1]]
|lists=
|see also=
}}

==References==

===Notes===
{{reflist|refs=<ref name=orbis>{{cite book|title=The [[Illustrated Encyclopedia of Aircraft]] (Part Work 1982-1985)|year=1985|publisher=Orbis Publishing|page=638}}</ref>}}

===Bibliography===

<!-- ==External links== -->
{{Bernard aircraft}}

[[Category:French bomber aircraft 1930–1939]]
[[Category:Bernard aircraft|082]]