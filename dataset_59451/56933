{{Infobox journal
| title = Exemplaria
| cover =
| discipline = [[Middle Ages]] and [[Early Modern Period]]
| abbreviation = Exemplaria
| editor = Anke Bernau, Noah Guynn, Patricia Clare Ingham, Elizabeth Scala
| publisher = [[Taylor & Francis]]
| country = 
| frequency = Quarterly
| history = 1989-present
| ISSN = 1041-2573
| eISSN = 1753-3074
| OCLC = 716182556
| LCCN = 90649992
| website = http://www.maneyonline.com/loi/exm
| link1 = http://www.maneyonline.com/toc/exm/current
| link1-name = Online access
}}
'''''Exemplaria''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering  the [[Middle Ages]] and the [[Early modern period]]. It was established in 1989 and is published by [[Taylor & Francis]]. The [[editors-in-chief]] are Anke Bernau ([[University of Manchester]]), Noah Guynn ([[University of California, Davis]]), Patricia Clare Ingham ([[Indiana University]]), and Elizabeth Scala ([[University of Texas at Austin]]). The book review editor is Robert Mills ([[University College London]]).

== Reception ==
In 2006 ''[[The Times Literary Supplement]]'' stated that ''Exemplaria'' "breaks into new territory, while never compromising on scholarly quality".<ref>{{cite news |last=Bildhauer |first=Bettina |date=10 March 2006 |title=Medieval Studies |newspaper=[[The Times Literary Supplement]] |location=London |url=http://www.the-tls.co.uk/tls/reviews/politics_and_social_studies/article714177.ece}}{{subscription required}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Annual Bibliography of English Language and Literature]]
* [[Arts and Humanities Citation Index]]
* [[British Humanities Index]]
* [[Current Contents]]/Arts & Humanities
* [[EBSCO Information Services|EBSCO databases]]
* [[Modern Language Association|Modern Language Association Bibliographies]]
* [[Scopus]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.maneyonline.com/loi/exm}}

[[Category:English-language journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1989]]