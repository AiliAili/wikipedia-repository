{{copy edit|date=September 2016}}
{{good article}}
{{Use American English|date=December 2016}}
{{Infobox military conflict
| image       = [[File:St-louis-attack.jpg|300px]]
| caption     = Mural of the attack in the [[Missouri State Capitol]].
| conflict    = Battle of St. Louis
| partof      = the [[Anglo-Spanish War of 1779&ndash;1783|Anglo-Spanish War]]
| date        = May 25, 1780
| place       = [[St. Louis]], [[Louisiana (New Spain)|Spanish Louisiana]] (present-day [[Missouri]], US) and [[Cahokia, Illinois|Cahokia]], [[Illinois Country]], [[Virginia]] (present-day [[Illinois]], US)
| coordinates = {{coord|38|37|27.4|N|90|11|21.2|W|display=title}}
| result      = Spanish victory
| combatant1  = {{flag|Spain|1748}}
| combatant2  = {{flagcountry|Kingdom of Great Britain}}<br>[[Indigenous people of the Americas|Indian nations]]<ref name=VR44>Van Ravenswaay, p. 44</ref>
*[[Sioux]]<ref name=P40/>
*[[Chippewa (tribe)|Chippewa]]
*[[Ho-Chunk|Winnebago]]
*[[Fox (tribe)|Fox]]
*[[Sac (tribe)|Sac]]
*[[Ottawa (tribe)|Ottawa]]
*[[Mascouten]]
*[[Kickapoo people|Kickapoo]]
*[[Potawatomi]]
| commander1  = [[Fernando de Leyba]]
| commander2  = [[Emanuel Hesse]]<br>[[Matchekewis]]<br>[[Wapasha I|Wapasha]]
| strength1   = 29 regulars<br>291 militia<ref name=VR44/>
| strength2   = 750–1,500 fur traders and Indians<ref name=VR44/>
| casualties1 = 50–100 killed, captured, wounded (mostly civilians)<ref name=VR46/>
| casualties2 = 4 killed <br>4 wounded<ref name=VR45>Van Ravenswaay, p. 45</ref>
| campaignbox =
{{Campaignbox Anglo-Spanish War (1779)}} 
}}

The '''Battle of St. Louis''' (''San Luis in Spanish),'' also known as the '''Battle of Fort San Carlos,''' was an unsuccessful attack led by the [[Kingdom of Great Britain|British]] on [[St. Louis]] (a French settlement in [[Spanish Louisiana]], founded on the West Bank of the [[Mississippi]] after the [[Treaty of Paris (1763)]]) on May 26, 1780 during the [[Anglo-Spanish War of 1779&ndash;1783|Anglo-Spanish War]]. A former British militia commander led a force of primarily [[Indigenous people of the Americas|Indians]] and attacked the settlement. [[Fernando de Leyba]], the Lieutenant Governor of [[Spanish Louisiana]], led the local militia to fortify the town as best as they could and successfully withstood the attack.

On the opposite bank of the Mississippi, a second simultaneous attack on the nearby former British colonial outpost of [[Cahokia, Illinois|Cahokia]] occupied by [[Patriot (American Revolution)|Patriot]] [[Virginia|Virginians]] was also repulsed. The retreating Indians destroyed the crops and took captive civilians outside the protected area. The British failed to defend their side of the river and thus, effectively ended any attempts to gain control of the [[Mississippi River]] during the war.

==Background==
[[File:BattleOfStLouis.png|thumb|Detail of a 1778 map depicting the middle reaches of the Mississippi River, annotated to show locations of interest for this battle]]
{{main article|Western theater of the American Revolutionary War|Spain in the American Revolutionary War}}

The [[Spain|Spanish]] entered the [[American Revolutionary War]] in 1779. The [[Great Britain|British]] military planners in [[London]] wanted to secure the corridor of the [[Mississippi River]] against both Spanish and [[Patriot (American Revolution)|Patriot]] activity. Their plans included expeditions from [[West Florida]] to take [[New Orleans]] and other Spanish targets, including several expeditions to gain control of targets in the [[Upper Mississippi River|Upper Mississippi]], including the small town of [[St. Louis, Missouri|St. Louis]]. The expedition from [[West Florida]] never got off the ground, because [[Bernardo de Gálvez]], the Governor of [[Spanish Louisiana]], moved rapidly to gain control of British outposts on the Lower Mississippi and threatened action against West Florida's principal outposts of [[Mobile, Alabama|Mobile]] and [[Pensacola, Florida|Pensacola]].<ref name=N279/><ref>Nester, p. 273</ref>

===British expedition===

[[Patrick Sinclair]], the military governor at [[Fort Michilimackinac]], organized the British expeditions from the north in present-day [[Michigan]]. Beginning in February 1780, he directed traders to circulate through their territories and recruit interested tribes for an expedition against St. Louis. Sinclair offered the fur traders the opportunity to control the fur trade in the upper parts of [[Spanish Louisiana]] as an incentive to participate.<ref name=N279>Nester, p. 279</ref>

Most of the force gathered at [[Prairie du Chien, Wisconsin|Prairie du Chien]], where [[Emanuel Hesse]], a former militia captain turned fur trader took command. The force numbered about two dozen fur traders and an estimated 750 to 1,000 Indians when it left Prairie du Chien on May 2.<ref name=P40>Primm, p. 40</ref> 200 [[Sioux]] warriors led by [[Wapasha I|Wapasha]] made up the largest contingency of the force, with additional sizable companies from the [[Chippewa (tribe)|Chippewa]], [[Menominee]], and [[Ho-Chunk|Winnebago]] and smaller numbers from other nations.<ref name=P40/> The Chippewa chief, [[Matchekewis]], was given overall command of the native forces. When the force reached [[Rock Island, Illinois|Rock Island]], they were joined by about 250 men from the [[Sac (tribe)|Sac]] and [[Fox (tribe)|Fox]] nations. These warriors were somewhat reluctant to attack St. Louis, but Hesse gave them large gifts to secure their participation in the venture.<ref>Nester, p. 280</ref>  The diversity within the expedition led to some animosity among the tribes. The Chippewa and Sioux, in particular, had a history of conflict with each other. However, Wapasha and Matchekewis promoted unity during the expedition.<ref>{{cite web|url=http://www.biographi.ca/009004-119.01-e.php?&id_nbr=2702|title=Biography of Wahpasha|publisher=Dictionary of Canadian Biography Online|accessdate=2013-01-09}}</ref>

===Spanish and American defenses===

The village of St. Louis was primarily a trading hub on the Mississippi River that was governed by Lieutenant Governor [[Fernando de Leyba]], a captain in the [[Spanish Army]], but it was also the administrative capital of [[Illinois Country|Upper Spanish Louisiana]]. [[Fernando de Leyba|Leyba]] was warned by a fur trader in late March 1780 that the British were planning an attack on St. Louis and the nearby American post at [[Cahokia, Illinois|Cahokia]]. He began developing plans for the village's defense. He had an inexperienced militia force of 168 dispersed around the surrounding countryside<ref name="P40" /> and only 29 [[regular army]] soldiers of the ''Fijo de Luisiana'' Colonial Regiment.<ref>{{cite web|url=http://www.cmhg.gc.ca/cmh/page-318-eng.asp|title=Conflict in the Far North and South|publisher=Canadian Military Heritage|accessdate=2010-10-17}}</ref>

[[Fernando de Leyba|Leyba]] developed a grand plan of defense that included the construction of four stone towers. Without funds or the time to get them from New Orleans, [[Fernando de Leyba|Leyba]] asked the villagers to contribute funds and labor to the construction of these [[fortification]]s and paid for some of the work from his private funds.<ref name=VR43>Van Ravenswaay, p. 43</ref>  By mid-May, a single round tower had been built that was about {{convert|30|ft|m}} in diameter and 30 to 40 feet tall. The tower, dubbed [[Fort San Carlos (St. Louis)|Fort San Carlos]], provided a commanding view of the surrounding countryside. As there did not appear to be sufficient time to build more towers, trenches were dug between the tower and the river to the north and south of the village.  Three four-pounder and two six-pounder cannons from Fort Don Carlos were mounted in the tower, and other cannons were emplaced at each end of the line of trenches.<ref name=P43>Primm, p.43</ref>  Fort Don Carlos had been constructed earlier in 1767 on the south bank of the Missouri river, near its mouth, just 15 miles north of the village of St. Louis.<ref name=P21>Primm, p.21</ref>
With a force of only 197 men, 168 of whom were inexperienced militia, it was highly probable that the opposing British and Indian combined force of 1,000 would overwhelm the Fort San Carlos. However, [[Fernando de Leyba|Leyba]] appealed to Francois Valle, a 70-year-old French inhabitant, who was located 60 miles to the South of the fort at the site of the French Colonial Valles Mines. Valle sent his two sons and 151 well-trained and equipped French militiamen and thus tipped the scale in favor of the defenders. By Royal Decree on April 1, 1782, King Carlos III of Spain, conferred upon Francois Valle the rank of lieutenant in the regular Spanish army thus making him a Spanish don. (citation: Colonial Ste. Genevieve: An Adventure on the Mississippi Frontier written by Carl J. Ekberg, Patrice Pr; 2 Sub Edition, March 1996).

Valle also aided greatly in the Battle of Fort San Carlos because he gave the defenders of both forts a major tactical advantage by supplying them with genuine lead (instead of pebbles or stones) from his mines for musket balls and cannon balls. (Getting hit with a pebble or stone did not compare to the damage and knockdown power of a 52-caliber rifle ball at 100 feet. (See [[France in the American Revolutionary War]].))

As a result of his contributions, Francois Valle was called the "Defender of St. Louis".<ref>http://vallemines.com/OfHistoricalNote/DefenderOfStLouis.aspx{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

On May 15, [[Fernando de Leyba|Leyba]] was visited by [[John Montgomery (pioneer)|John Montgomery]], the American commander at Cahokia, who proposed a joint Spanish and American force to counter Hesse's expedition, an idea that never reached fruition. On May 23, [[Fernando de Leyba|Leyba]]<nowiki/>s scouts reported that Hesse's force landed their canoes only {{convert|14|mi|km}} away and were coming overland.<ref name="VR44" />

==Battle==
[[File:SaintLouis1790s.jpg|left|thumb|A sketch of Saint Louis made in the 1790s by [[Victor Collot]]]]

On May 25, Hesse sent out scouting parties to determine the situation at St. Louis. These parties were unable to get close to the village due to the presence of workers (including women and children) in the fields outside the village.<ref name=P41>Primm, p. 41</ref> The next day, Hesse sent [[Jean-Marie Ducharme]] and 300 Indians across the river to attack Cahokia, while the remainder arrived around 1:00 pm near St. Louis. A warning shot was fired from the tower when they came into view. The Sioux and Winnebagoes led the way, followed by the Sac, Fox and fur traders, including Hesse, in the rear. [[Fernando de Leyba|Leyba]] directed the defense from the tower and opened fire on the approaching enemy from the trenches and tower when they came in range. On the first volley, most of the Sac and Fox fell back, apparently unwilling to fight, leaving many of the other participants suspicious of their motives in joining the expedition and complaining of their "treachery".<ref name=P42>Primm, p. 42</ref>

Wapasha and the Sioux persisted for several hours in attempts to draw the Spanish defenders out. They went as far as brutally killing some captives they had taken in the fields. Although this angered some of the townspeople, Lebya refused to grant permission to the militia to make a [[sortie]]. The attackers eventually withdrew and headed north, destroying crops, livestock, and buildings as they went.<ref name=P42/>

On the other side of the river, Ducharme's attack on Cahokia was easily repulsed. [[George Rogers Clark]] arrived timely to lead Cahokia's defense. Clark's reputation as a frontier fighter made the Indian force reluctant to pursue the attack.<ref name=VR46>Van Ravenswaay, p. 46</ref>

==Aftermath==
[[File:Fort-san-carlos.jpg|right|thumb|Diorama in the Missouri State Capitol showing Fort San Carlos and the attack]]

The village of 700 inhabitants lost between 50 and 100 killed, wounded and captured. Virtually all casualties were civilians.<ref name=VR46/> A year later, the Spaniards from St. Louis raided [[Fort St. Joseph (Niles)|Fort St. Joseph]] and brought the captured British flag back to St. Louis.<ref name=P45>Primm, p. 45</ref>

Fernando de Leyba died the following month. He was the subject of local criticism because he never formally recognized the efforts made by the citizenry in the town's defense.<ref name=VR47>Van Ravenswaay, p. 47</ref>  [[Charles III of Spain|King Charles]] did not know he had died and promoted him to lieutenant colonel because of his valor in action.<ref name=P44>Primm, p. 44</ref>

==Legacy==

The site where Fort San Carlos stood is at the corner of Fourth and Walnut Street in St. Louis. A local organization commemorates the event by reading out the names of the 21 people who lost their lives during the battle.<ref>{{cite news|url=http://www.westendword.com/NC/0/934.html|title=Group fights for recognition of little-known 1780 Battle of St. Louis|publisher=West End Word|last=Rice|first=Patricia|accessdate=2010-10-17}}</ref>  The battle is also remembered in a mural and diorama located in the [[Missouri State Capitol]] (pictured).

==Notes==
{{Reflist|2}}

==References==
* Gilman, Carolyn.  "L'Anneé du Coup: The Battle of St. Louis, 1780 Part 2," ''Missouri Historical Review'' (2009) 103#4 pp: 195-211
*{{cite book|title=Lion of the Valley: St. Louis, Missouri, 1764–1980|first=James Neal|last=Primm|isbn=978-1-883982-24-9|oclc=245700335|year=1998|publisher=Missouri History Museum|ref=Primm|location=St. Louis, MO}}
*{{cite book|title=The Frontier War for American Independence|first=William R|last=Nester|isbn=978-0-8117-0077-1|publisher=Stackpole Books|year=2004|ref=Nester|oclc=260092836|location=Mechanicsburg, PA}}
*{{cite book|last=Van Ravenswaay|first=Charles|title=Saint Louis: an Informal History of the City and its People, 1764–1865|ref=VR | isbn=978-0-252-01915-9 | year=1991 | publisher=Missouri History Museum|location=St. Louis, MO|oclc=24431022}}

{{DEFAULTSORT:Battle Of St. Louis}}
[[Category:Battles of the Anglo-Spanish War (1779–83)|Saint Louis]]
[[Category:Battles involving Spain|Saint Louis]]
[[Category:Conflicts in 1780]]
[[Category:Battles involving Great Britain|Saint Louis]]
[[Category:Colonial Louisiana]]
[[Category:History of St. Louis]]
[[Category:Missouri in the American Revolution]]
[[Category:1780 in New Spain]]
[[Category:1780 in the United States]]
[[Category:May 1780 events]]