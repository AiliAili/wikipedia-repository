{{for|the South African rugby player|Adolf Malan}}
{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name= Adolph Gysbert (Sailor) Malan
|image= Group_Captain_A_G_Malan_WWII_IWM_CH_12661.jpg
|image_size= 150
|caption= Group Captain Sailor Malan c. 1945
|birth_date= {{birth date|1910|03|24|df=yes}}
|death_date= {{death date and age|1963|09|17|1910|03|24|df=yes}}
|birth_place= [[Wellington, Western Cape|Wellington]], [[Cape Colony]]
|death_place=
|placeofburial=
|nickname= Sailor
|allegiance= [[United Kingdom]]
|branch= [[Royal Air Force]]
|serviceyears= 1935–1946
|rank= [[Group Captain]]
|commands= [[No. 74 Squadron RAF]]<br />[[No. 19 Wing RAF]]<br/>[[No. 145 Wing RAF|No. 145 (Free French) Fighter Wing]]
|unit=
|battles= [[World War II]]
* [[Battle of Barking Creek]]
* [[Battle of Dunkirk]]
* [[Battle of Britain]]
* [[Operation Overlord]]
|awards= [[Distinguished Service Order]] & [[Medal bar|Bar]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] & Bar<br/>[[Croix de guerre (Belgium)|Croix de guerre]] (Belgium)<br/>[[Czechoslovak War Cross 1939–1945|War Cross]] (Czechoslovakia)<br/>[[Legion of Honour]] (France)<br/>[[Croix de guerre 1939–1945 (France)|Croix de guerre]] (France)
|laterwork=
}}

'''Adolph Gysbert Malan''' [[Distinguished Service Order|DSO]] & [[Medal bar|Bar]], [[Distinguished Flying Cross (United Kingdom)|DFC]] & Bar (24 March 1910&nbsp;– 17 September 1963), better known as '''Sailor Malan''', was a South African fighter pilot in the [[Royal Air Force]] (RAF), who led [[No. 74 Squadron RAF]] during the height of the [[Battle of Britain]]. Under his leadership No. 74 Squadron became one of the RAF's best units. Malan scored 27 kills, seven shared destroyed, three probably destroyed and 16 damaged.<ref>{{cite book|last1=Price|first1=Alfred|title=Spitfire Mark V aces, 1941–45|date=1997|publisher=Osprey Aerospace|location=London|isbn=978-1-85532-635-4|page=65}}</ref>

Malan survived the war to become involved in the [[Internal resistance to South African apartheid|anti-apartheid movement]] in his country. His younger brother, George F. Malan, was killed flying with No. 72 Squadron RAF as a Spitfire pilot in Tunisia, in early 1943.<ref>{{cite book|last1=Oxspring|first1=Bobby|title=Spitfire Command|date=1987|publisher=Grafton|location=London|isbn=0-586-07068-0|pages=161, 166}}</ref>

==Early life==
Malan was born in [[Wellington, Western Cape]], then part of the [[Cape Colony]]. He joined the South African Training Ship ''[[General Botha]]'' in 1924 or 1925 as a naval cadet (cadet number 168), and on 5 January 1928 engaged as an officer cadet (seaman's discharge number R42512) aboard the ''Landsdown Castle''<ref>CR1 record card, R42512, record group BT348, The National Archives (London)</ref> of the [[Union-Castle Line]] of the [[International Mercantile Marine Co.]] which later earned him the nickname of "Sailor" amongst his pilot colleagues.

==Royal Air Force==
In 1935 the RAF started the rapid expansion of its pilot corps, and Malan was one of the people who joined up. He learned to fly in the [[Tiger Moth (plane)|Tiger Moth]] at an elementary flying school near [[Bristol]], flying for the first time on 6 January 1936. Commissioned an acting pilot officer on 2 March,<ref>{{London Gazette |issue=34265 |startpage=1742 |date=17 March 1936 }}</ref> he completed training by the end of the year, and was sent to join 74 Squadron on 20 December 1936. He was confirmed as a [[pilot officer]] on 6 January 1937,<ref>{{London Gazette |issue=34380 |startpage=1750 |date=16 March 1937 }}</ref> and was appointed to acting flight commander of "A" Flight, flying [[Spitfire]]s, in August. He was promoted to acting [[flying officer]] on 20 May 1938<ref>{{London Gazette |issue=34544 |startpage=5418 |date=23 August 1938 }}</ref> and promoted to substantive flying officer on 6 July.<ref>{{London Gazette |issue=34542 |startpage=5293 |date=16 August 1938 }}</ref> He received another promotion to acting [[flight lieutenant]] on 2 March 1939,<ref>{{London Gazette |issue=34611 |startpage=2097 |date=2 March 1939 }}</ref> six months before the outbreak of war.

==Second World War==

===Battle of Barking Creek===
No. 74 Squadron saw its first action only 15 hours after war was declared, sent to intercept a bomber raid that turned out to be returning RAF planes. On 6 September 1939, "A" Flight was scrambled to intercept a suspected enemy radar track and ran into the Hurricanes of [[No. 56 Squadron RAF]]. Believing 56 to be the enemy, Malan ordered an attack. Paddy Byrne and [[John Freeborn]] downed two RAF aircraft, killing one officer, Montague Hulton-Harrop, in this [[friendly fire]] incident, which became known as the [[Battle of Barking Creek]]. At the subsequent [[courts-martial]], Malan denied responsibility for the attack.<ref name="nasson">{{cite journal|first= Bill |last=Nasson | title = A flying Springbok of wartime British skies: A.G. ʻSailorʼ Malan| journal= Kronos |issue=35 |pages=71–97|publisher=University of Western Cape|date=2009| url = http://repository.uwc.ac.za/xmlui/bitstream/handle/10566/101/NassonSailor2009.pdf?sequence=1| accessdate = 26 August 2010}}</ref> He testified for the prosecution against his own pilots stating that Freeborn had been irresponsible, impetuous, and had not taken proper heed of vital communications.<ref name="nasson"/> This prompted Freeborn's counsel, [[Patrick Hastings|Sir Patrick Hastings]] to call Malan a bare-faced liar.<ref>{{cite book|first=Bob|last=Cossey|title=A Tiger's Tale: The Story of Battle of Britain Fighter Ace Wg. Cdr. John Connell Freeborn|publisher=J & KH Publishing|year=2002 |isbn= 978-1-900511-64-3|pages= 64–66}}</ref> Hastings was assisted in defending the pilots by [[Roger Bushell]], the London barrister and RAF Auxiliary pilot who later led the Great Escape from [[Stalag Luft III]]. The court ruled the entire incident was an unfortunate error and acquitted both pilots.<ref>{{cite book|first=Stephen|last=Bungay|title=The Most Dangerous Enemy: A history of the Battle of Britain|publisher=Aurum Press|year= 2001|page=67|isbn=978-1-84513-481-5}}</ref>

===Dunkirk===
Events soon overtook the squadron. After fierce fighting over Dunkirk during [[Operation Dynamo|the evacuation of Dunkirk]] on 28 June 1940, Malan was awarded the [[Distinguished Flying Cross (UK)|Distinguished Flying Cross]] having achieved five 'kills'. During this battle he first exhibited his fearless and implacable fighting spirit. In one incident he was able to coolly change the light bulb in his gunsight while in combat and then quickly return to the fray. During the night of 19/20 June Malan flew a night sortie in bright moonlight and shot down two [[Heinkel He-111]] bombers, a then unique feat for which a bar to his DFC was awarded. On 6 July, he was promoted to the substantive rank of flight lieutenant.<ref>{{London Gazette |issue=34915 |startpage=4811 |date=6 August 1940}}</ref>

Malan and his senior pilots also decided to abandon the "vic" formation used by the RAF, and turned to a looser formation (the "[[finger-four]]") similar to the four aircraft ''Schwarm'' the ''[[Luftwaffe]]'' had developed during the Spanish Civil War. Legend has it that on 28 July he met [[Werner Mölders]] in combat, damaging his plane and wounding him, but failing to bring him down. Recent research has suggested however that Mölders was wounded in a fight with [[No. 41 Squadron RAF]].{{fact|date=March 2017}}

===Squadron leader of No. 74 Squadron===
On 8 August, Malan was given command of 74 Squadron and promoted to acting [[squadron leader]]. This was at the height of the [[Battle of Britain]]. Three days later, on 11 August, action started at 7&nbsp;am when 74 was sent to intercept a raid near [[Dover, England|Dover]], but this was followed by another three raids, lasting all day. At the end of the day, 74 had claimed to have shot down 38 aircraft, and was known from then on as "Sailor's August the Eleventh". Malan himself simply commented, "''thus ended a very successful morning of combat''." He received a bar to his DFC on 13 August.<ref>{{London Gazette |issue=34920 |startpage=4939 |date=13 August 1940 }}</ref>

On the ground, Malan was remembered as an inveterate gambler and often owed his subordinates money. Malan was older than most of his charges and although sociable and relaxed off-duty, he spent most of his time with his wife and family living near Biggin Hill. He would soon develop a routine of flying the first sortie of the day and then handing the squadron to a subordinate while he stayed on the ground to do paperwork. Despite frosty relations after the Battle of Barking Creek he would often give command of the squadron to [[John Freeborn]] (himself an [[Flying ace|ace]] of note), showing Malan's ability to keep the personal and professional separate.

Malan commanded 74 Squadron with strict discipline and did not suffer fools gladly, and could be high-handed with sergeant pilots (many non-commissioned pilots were joining the RAF at this time). He could also be reluctant to hand out decorations, and he had a strict yardstick by which he would make recommendations for medals: six kills confirmed for a DFC, twelve for a bar to the DFC; eighteen for a DSO.

[[File:Sailor Malan, colour oil painting by Cuthbert Orde.JPG|thumb|right|Sailor Malan, colour oil painting by Cuthbert Orde]]On 29 December 1941 Malan was added to the select list of airmen who had sat for one of [[Cuthbert Orde]]'s iconic charcoal portraits. He also had the far rarer honour of having Orde paint a full colour painting.<ref>{{citation| author = RAF Museum| title = Battle of Britain| url = http://navigator.rafmuseum.org/results.do;jsessionid=7737A08DF483E2E4A45DB574929022DD?view=detail&db=object&pageSize=1&id=7603| accessdate =4 November 2010}}</ref>

===Wing commander – Biggin Hill===
On 24 December, Malan received the [[Distinguished Service Order]], and on 22 July 1941, a [[Medal bar|bar]] to the Order. On 10 March 1941 he was appointed as one of the first [[Wing (military unit)|wing]] leaders for the offensive operations that spring and summer, leading the Biggin Hill Wing until mid August, when he was rested from operations.  He finished his active fighter career in 1941 with 27 kills destroyed, 7 shared destroyed and 2 unconfirmed, 3 probables and 16 damaged, at the time the RAF's leading ace, and one of the highest scoring pilots to have served wholly with Fighter Command during World War II. He was transferred to the reserve as a squadron leader on 6 January 1942.<ref>{{London Gazette |issue=36524 |supp=yes |startpage=2339 |date=23 May 1944}}</ref>

After tours to the USA and the [[RAF Sutton Bridge|Central Gunnery School]], Malan was promoted to temporary [[Wing commander (rank)|wing commander]] on 1 September 1942<ref>{{London Gazette |issue=35725 |supp=yes |startpage=4258 |date=1 October 1942}}</ref> and became station commander at [[Biggin Hill]], receiving a promotion to war substantive wing commander on 1 July 1943.<ref>{{London Gazette |issue=36157 |startpage=3927 |date=3 September 1943 |supp=yes}}</ref> Malan remained keen to fly on operations, often ignoring standing orders for station commanders not to risk getting shot down. In October 1943 he became officer commanding [[No. 19 Wing RAF|19 Fighter Wing]], [[RAF Second Tactical Air Force]], then commander of the 145 (Free French) Fighter Wing in time for D-day, leading a section of the wing over the beaches during the late afternoon.

[[File:Malan and RAF officers D Day IWM CL 29.jpg|thumb|Malan and RAF officers on D-Day]]

==Rules of air fighting==
Although not an instinctive, gifted pilot Malan was an exceptional shot and a very aggressive air fighter, and above all a superb tactician who instilled the methods and techniques he had honed in 1940 into successive generations of young fighter pilots who followed him.

Malan developed a set of simple rules for fighter pilots, to be disseminated throughout [[RAF Fighter Command]], which eventually could be found tacked to the wall of most airbases:

'''TEN OF MY RULES FOR AIR FIGHTING'''

# Wait until you see the whites of his eyes. Fire short bursts of one to two seconds only when your sights are definitely "ON".
# Whilst shooting think of nothing else, brace the whole of your body: have both hands on the stick: concentrate on your ring sight.
# Always keep a sharp lookout. "Keep your finger out".
# Height gives you the initiative.
# Always turn and face the attack.
# Make your decisions promptly. It is better to act quickly even though your tactics are not the best.
# Never fly straight and level for more than 30 seconds in the combat area.
# When diving to attack always leave a proportion of your formation above to act as a top guard.
# ''INITIATIVE'', ''AGGRESSION'', ''AIR DISCIPLINE'', and ''TEAMWORK'' are words that MEAN something in Air Fighting.
# Go in quickly – Punch hard – Get out!

==Later life==
On 5 April 1946, Malan resigned his RAF commission, retaining the rank of [[group captain]]<ref>{{London Gazette |issue=37526 |startpage=1795 |date=9 April 1946 |supp=yes}}</ref> and returned to South Africa where he joined the ''[[Torch Commando]]'' a joint project of the anti-fascist ex-servicemen's organisation, the ''[[Springbok Legion]]'' and the ''War Veterans Action committee''. Sailor Malan became the president of that new organization. In Malan's words, it was "established to oppose the police state, abuse of state power, censorship, racism, the removal of the coloured vote and other oppressive manifestations of the creeping [[fascism]] of the National Party regime".{{cite quote|date=March 2017}}

Amongst the leading members of the Springbok Legion were many ex-servicemen who would later join the [[African National Congress]] and its armed wing, ''Umkhonto we Sizwe'' under the leadership of [[Nelson Mandela]]. Amongst these were [[Joe Slovo]], Jack Hodgson, Wolfie Kodesh, Brian Bunting and Fred Carneson. After the National Party came to power and began to implement its policies, many{{who|date=March 2017}} found the Springbok Legion, founded in 1941, to be too left orientated and too radical. In 1950 members of the Springbok Legion began to work with other more liberal organizations and even the United Party official opposition, to find new ways to mobilise protest support against a string of Apartheid laws.

In 1951 the Springbok Legion, formed a protest group together with the War Veterans Action Committee, to appeal to a broader base of ex-servicemen, which they called the 'Torch Commando', as a tactic to fight the National Party's plans to [[Coloured vote constitutional crisis|remove Cape's "coloured" voters from the roll]]. [[Harry Schwarz]], an ex-serviceman and later a leading figure in the anti-apartheid movement was one of the founders of the organization. The Torch Commando fought a battle for more than five years, and at its height had 250,000 members.  The government was so alarmed by the number of judges, public servants and military officers joining the organisation that a new law was passed to ban anyone in public service or the military from joining. The National Party tried to purge the memory of the Springbok Legion, Torch Commando and of Sailor Malan from history because there was a fear that young Afrikaners in particular might want to emulate Malan.

At its largest Torch Commando protest rally, the Springbok Legion attracted 75,000 people. In a speech at a rally outside City Hall in Johannesburg, Malan made reference to the ideals for which the Second World War was fought: "The strength of this gathering is evidence that the men and women who fought in the war for freedom still cherish what they fought for. We are determined not to be denied the fruits of that victory."{{cite quote|date=March 2017}}

Malan died in 1963 from [[Parkinson's Disease]], at the time a rare and essentially mysterious malady. A considerable sum of money was raised in his name to further study the disease, a fund that continues to this day. 

The Nationalist government of the day (several of whom (including Balthazar John Vorster Prime Minister of South Africa 1966-78 were openly war-time Nazi sympathizers and members of the Ossewabrandwag) controversially denied Malan a State funeral in 1963. This active snub further entrenched increasing post-independent anti-British sentiment within South Africa after 1948 and outraged many South African WW2 veterans who had regarded Malan as a South African WW2 hero and as a patriot (9). The perception that the Nationalists acted more favorably to another South African Sidney Robie Liebbrandt (deceased 1966 had allegedly spied for Germany, sentenced to death for high treason and later pardoned and whom the Nazis had landed by boat off the South African coast in 1941), only added salt to the wound left by the refusal of a State funeral for Malan.  

He was survived by his wife, Lynda, son Jonathan, and daughter Valerie.

In the 1969 war film [[Battle of Britain (film)|''Battle of Britain'']], the [[Robert Shaw (British actor)|Robert Shaw]] character 'Squadron Leader Skipper' was explicitly based on Malan, as recounted by director [[Guy Hamilton]] in the documentary 'A Film for the Few', which was included with the 2004 Special Edition DVD release. At one point early in the film, Skipper gives advanced [[air combat maneuvering|air combat manoeuvring]] training to an inexperienced pilot, and angrily barks "Never fly straight and level for more than 30 seconds in the combat area!"{{cite quote|date=March 2017}} —quoting one of Malan's rules.

==See also==
* [[List of top World War II aces]]
* [[List of World War II aces from South Africa]]
* [[Huguenots in South Africa]], for the history of French surnames (like Malan) in South Africa.

==References==
{{Reflist|30em}}

'''Bibliography'''
{{refbegin}}
* Franks, Norman L.R. ''Sky Tiger The Story of Sailor Malan''. Crecy, Manchester, UK. 1994. ISBN 978-0-907579-83-0.
* Walker, Oliver '' Sailor Malan''. Casssell & Co Ltd. 1953.
* Price, Dr. Alfred. ''Spitfire Mark V Aces, 1941–45''. Botley, Oxford, UK: Osprey Publishing Ltd., 1997. ISBN 978-1-85532-635-4.
{{refend}}

==External links==
* [http://acesofww2.com/safrica/aces/malan/ Sailor Malan at acesofww2.com]
* [http://www.scielo.org.za/scielo.php?pid=S0259-01902009000100004&script=sci_arttext A flying Springbok of wartime British skies: A.G. 'Sailor' Malan – Bill Nasson]
* [http://zar.co.za/sailor.htm ZAR.co.za Biography]

{{Commons category|Adolph Malan}}

{{Authority control}}

{{DEFAULTSORT:Malan, Adolph}}
[[Category:1910 births]]
[[Category:1963 deaths]]
[[Category:People from the Cape Winelands District Municipality]]
[[Category:Afrikaner people]]
[[Category:South African people of Huguenot descent]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:South African military personnel]]
[[Category:South African World War II flying aces]]
[[Category:Recipients of the Distinguished Flying Cross and Bar (United Kingdom)]]
[[Category:British World War II flying aces]]
[[Category:The Few]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Deaths from Parkinson's disease]]
[[Category:Anti-apartheid activists]]