{{Infobox company
|name = Coronation Fund Managers
|logo = File:Coronation-fund-managers-logo.jpg
|type = Public Company
|traded_as = {{JSE|CML}}<ref>http://fin.jse.co.za/findata/sharesearchresult.asp?searchtype=string&searchtext=CML&y</ref>
|sector = [[Asset management]]
|foundation = Cape Town, {{start date |1993}}
|location_city =Cape Town
|location_coutry =South Africa
|area_served = South Africa, Ireland, United Kingdom and Namibia
|key_people = {{ubl |Anton Pillay (Chief Executive Officer) |Karl Leinberger (Chief Investment Officer) }}
|products = Institutional segregated and pooled products across asset classes and geographies, Retail unit trust products, Retirement products, Hedge funds
|revenue = R4 774 million (2014)
|website = {{url |http:www.coronation.com}}
}}

'''Coronation Fund Managers''' is a South-African third-party [[Fund manager|fund management company]], headquartered in [[Cape Town]].<ref>http://www.coronation.com/doc-1-sc-2-5689/dc-2-3884</ref>  The company has locations in all South African major centers and offices in, [[Ireland]], [[United Kingdom]] and in [[Namibia]] where it is represented by Namibia Asset Management a strategic partner. As of September 2014 the company had [[Assets under management]] of R588 billion.

== History ==
Coronation Fund Managers commenced operations in Cape Town in 1993, with 15 staff and no assets under management.<ref name="financialmail.co.za">http://free.financialmail.co.za/report05/coronation05/index.html</ref>

In 1996 Coronation's [[unit trust]] business, Coronation Management Company,<ref>http://www.btimes.co.za/98/top100/top20.htm</ref> was launched. It also launched South Africa’s first international fund of [[hedge fund]]s: Coronation Global Equity Fund.

In 1997, Coronation launched the country’s first retail international fund of hedge funds, the Coronation International Active Fund of Funds, and was instrumental in the formation of Namibia Asset Management and Namibia Unit Trust Managers.{{citation needed|date=July 2012}}

In 1998 Coronation Fund Managers established in [[Dublin]], Ireland and in 1999 opened an office in [[London]]. This was also the year in which Coronation pioneered the launch of [[absolute return]] products to the<ref name="financialmail.co.za"/> South African market.

In June 2003, Coronation Fund Managers was listed separately on the [[Johannesburg Stock Exchange]] (JSE) after it was unbundled from Coronation Holdings with a total staff complement of 133.<ref name="financialmail.co.za"/> It became the first{{citation needed|date=July 2012}} [[Asset management company|asset manager]] in South Africa to gain Global Investment Performance Standards (GIPSTM) compliance.{{clarify|date=July 2012}}

Since its formation, Coronation has developed a number of black-controlled enterprises in the asset management industry. This includes Namibia Asset Management and Namibia Unit Trust Managers in Namibia and African Harvest in 1997, Kagiso Asset Management in 2002, and in 2005 launched Coronation Fund Managers<ref>http://free.financialmail.co.za/report05/coronation05/bfundman.htm</ref> Botswana.

In 2007, Coronation became a signatory to the [[Principles for Responsible Investment|United Nations Principles of Responsible Investments]]<ref>http://www.docstoc.com/docs/72488838/Investing-for-the-long-term</ref> and began formulating their global emerging markets (GEM) product offering. Coronation’s Africa unit was launched in 2008.<ref>http://www.africa-fm.com/article/coronation-long-term-performance-everything</ref>

== Business activities ==
Coronation is an investment-led, owner-managed business specialising in emerging market. It is an active manager with a long-term valuation-driven investment approach.

Product range: Emerging Market Equity; Global Equity, Fixed Interest and Multi-Asset; South African Equity, Fixed Interest and Multi-Asset; African Equity; Retirement Products and Hedge Funds.

==Regulators==
Coronation engages with the following regulatory bodies in the various jurisdictions in which they operate on an ongoing basis:

* [[Financial Services Board (South Africa)]]
* Financial Conduct Authority (UK)
* Central Bank of Ireland (Ireland)
* Securities and Exchange Commission (USA)

==Stewardship==
Coronation was an early signatory to the United Nations Principles for Responsible Investing in 2007. The business was also an early adopter of the Code for Responsible Investing in South Africa (2012) and the UK Stewardship Code (2010) [10]. The business incorporates environmental, social and governance (ESG) factors into its investment decision-making process in a way that is fully consistent with their long-term investment horizon. 

Coronation believes companies cannot achieve sustainable economic success while neglecting their social and environmental responsibilities. Social responsibility has the potential to increase the quality of a company’s earnings stream and consequently its long-term investment potential. For Coronation, ESG issues form an intrinsic part of the mosaic for any investment case. Poor ESG performance may not necessarily exclude investing in a company, but it does force them to carefully consider the issues and engage management on these factors.

Coronation furthermore recognises the importance of focusing its attention and time on those ESG issues where it has expertise, and that can have the most meaningful impact on a company’s long-term prospects. To this end, Coronation spends large amounts of time studying the corporate governance of the companies they invest in. Coronation engages extensively with management and the board where needed.

==Transformation==
As a South African company, Coronation is committed to [[Broad-Based Black Economic Empowerment]] (B-BBEE). Coronation’s approach to transformation is based on the ideal that people are their greatest asset. This is reflected in their choice of a B-BBEE partner, the Imvula Trust, of which the beneficiaries comprise all black staff. Imvula was created in April 2005<ref>{{cite web|url=http://mg.co.za/article/2005-01-26-coronation-announces-bee-funding|title=Coronation announces BEE funding|author=Staff Reporter|work=The M&G Online}}</ref> and on 28 February 2013 acquired a direct ownership of 10% in the listed Coronation business.<ref>{{cite web|url=http://www.sharenet.co.za/free/sens/disp_news.phtml?tdate=20130228160000&seq=74|title=CML  - Coronation Fund Managers Limited - Implementation Of The Imvula Transformation Transaction - 28/02/2013|work=sharenet.co.za}}</ref>

In terms of their procurement processes, Coronation applies strict consideration rules in order to create the opportunity for enhanced economic activity.<ref>	http://www.coronation.com/Assets/Global/AnnualReports/2014/2014-Integrated-Annual-Report.pdf</ref>

== References ==
{{reflist}}

== External links ==
* [http://www.coronation.com/ Coronation Fund Managers Homepage]

[[Category:Financial services companies of South Africa]]
[[Category:Investment management companies of South Africa]]
[[Category:Companies based in Cape Town]]