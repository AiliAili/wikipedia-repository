{{technical|date=July 2015}}
'''Equilibrium partitioning Sediment Benchmarks (ESBs)''' are a type of [[Aquatic toxicology#Significance to regulatory world|Sediment Quality Guideline (SQG)]] derived by the USEPA for the protection of [[benthic]] organisms. ESBs are based on the bioavailable concentration of [[contaminants]] in [[sediment]]s rather than the dry-weight concentration. It has been demonstrated that sediment concentrations on a dry-weight basis often do not predict biological effects.<ref name="USEPA ESB">United States Environmental Protection Agency. 2003. Technical basis for the derivation of equilibrium partitioning sediment benchmarks (ESBs) for the protection of benthic organisms: [[Nonionic]] organics. EPA-600-R-02-014. Office of Research and Development. Washington, DC. (draft)</ref> Interstitial water concentrations, however, predict biological effects much better. This is true because the chemical present in the interstitial water (or pore water) is the uncomplexed/free phase of the chemical that is bioavailable and toxic to benthic organisms. Other phases of the chemical are bound to sediment particles like organic carbon (OC) or acid volatile sulfides (AVS) and are not bioavailable. Thus the interstitial water concentration is important to consider for effects to benthic organisms.

Equilibrium partitioning (EqP) predicts the bioavailable concentration of a chemical under given sediment conditions by using partition coefficients. The bioavailable concentration in [[interstitial fluid|interstitial]] water can then be compared to an established water-based effect concentration and used to predict the likelihood of adverse effects. When a chemical exceeds the ESB, an adverse biological effect may occur, and when concentrations are below or equal to an ESB, biological effects are unlikely to occur.

The USEPA's Office of Research and Development (ORD) has published ESBs for approximately 65 pollutants or classes of pollutants including 34 [[Polycyclic aromatic hydrocarbon|PAHs]],<ref name="PAHs">U.S. Environmental Protection Agency. 2003. Procedures for the derivation of equilibrium partitioning sediment benchmarks (ESBs) for the protection of benthic organisms: PAH mixtures. EPA 600/R-02/013. Technical Report, Washington, DC.</ref> metal mixtures (e.g., cadmium, chromium, copper, nickel, lead, silver, and zinc),<ref name="Metals">U.S. Environmental Protection Agency. 2005. Procedures for the derivation of equilibrium partitioning sediment benchmarks (ESBs) for the protection of benthic organisms:Metal mixtures. EPA 600/R-02/011. Technical Report, Washington, DC.</ref> and pesticides [[dieldrin]]<ref name="Dieldrin">U.S. Environmental Protection Agency. 2003. Procedures for the derivation of equilibrium partitioning sediment benchmarks (ESBs) for the protection of benthic organisms: Dieldrin. EPA 600/R-02/010. Technical Report, Washington, DC.</ref> and [[endrin]]<ref name="Endrin">U.S. Environmental Protection Agency. 2003. Procedures for the derivation of equilibrium partitioning sediment benchmarks (ESBs) for the protection of benthic organisms: Endrin. EPA 600-R-02-009. Technical Report. Office of Research and Development, Washington, DC.</ref>

== Theory ==
[[Partition equilibrium|Equilibrium partitioning theory (EqP)]] forms the basis for ESBs developed to account for bioavailability of contaminants in sediments. The concentration of interstitial water reflects the chemical's activity and is a surrogate for bioavailability.<ref name="DiToro">Di Toro DM, Zarba CS, Hansen DJ, Berry WJ, Swartz RC, Cowan CE, Pavlou SP, Allen HE, Thomas NA, Paquin PR. 1991. Technical basis for the equilibrium partitioning method for establishing sediment quality criteria. Environ Toxicol Chem 11:1541–1583.</ref> EqP theory holds that a non-ionic chemical in sediment partitions between sediment OC, interstitial water and benthic organisms. For cationic metals, the chemical also partitions onto sediment AVS, as well as sedimentary OC. At equilibrium, if the partition coefficients are known along with the concentration in any one of the phases, then the concentrations of the other phases can be predicted.

Using EqP theory, the bioavailable concentration of chemicals is predicted and then related to an established toxic effect concentration such as the final chronic value (FCV), which is used to derive water quality criteria (WQC) in the US.<ref>Stephan CE, Mount DI, Hansen DJ, Gentile GH, Chapman GA, Brungs WA. 1985. Guidelines for deriving numerical national water quality criteria for the protection of aquatic organisms and their uses. PB85– 227049. Technical Report. National Technical Information Service, Springfield, VA, USA.</ref> Other values besides the FCV may also be used when an FCV is not available. Using water-based effects concentration when predicting toxic effects in benthos has been supported by studies showing similar effects occur in pelagic and benthic organisms.<ref name="DiToro"/>

=== Nonionic organic chemicals ===

Experiments have demonstrated that toxicological effects of benthic organisms are not only correlated to interstitial water concentrations, but also to sediment concentrations when expressed on a microgram chemical/gram OC basis.<ref name="USEPA ESB"/> This is because hydrophobic chemicals like PAHs tend to be bound to OC in sediment. The OC-normalized concentration of sediment concentrations is easily calculated by measuring total OC in sediments synoptically with dry weight concentrations. Thus, the freely dissolved interstitial water concentration of nonionic organic chemicals can be predicted using the K<sub>OC</sub>, the organic carbon-water partition coefficient, which is a constant for each chemical:

K<sub>OC</sub> = C<sub>OC</sub>/C<sub>''d''</sub>

For nonionic organic contaminants, the K<sub>OC</sub> can be determined based on the octanol-water partition coefficient (K<sub>OW</sub>). C<sub>OC</sub> is the organic carbon normalized sediment concentration (micrograms per kilogram OC) and C<sub>''d''</sub> is the dissolved interstitial water concentration (milligrams per liter).

The equation above can be rearranged to:

C<sub>OC</sub> = K<sub>OC</sub>C<sub>''d''</sub>.

Using 1/1000 to convert C<sub>OC</sub> in kilograms OC to grams OC and substituting a known water effects concentrations (e.g. FCV), the ESB (micrograms per gram OC) is calculated as:

ESB = K<sub>OC</sub>FCV(1/1000)

The calculated ESB is site-specific.

=== Cationic metals ===

The equation for the bioavailable concentration for cationic metals including cadmium, copper, nickel, lead, silver and zinc that incorporates the AVS phase is as follows:

C<sub>''d''</sub> = (SEM-AVS) / (ƒ<sub>OC</sub>K<sub>OC</sub>)

SEM = simultaneously extract metals

== Government Agency Use ==

=== USEPA ===

The USEPA's Office of Research and Development (ORD) has published ESBs for approximately 65 pollutants or classes of pollutants. Five documents describing the derivation of 34 [[Polycyclic aromatic hydrocarbon|PAHs]],<ref name="PAHs"/> nonionic organics,<ref>U.S. Environmental Protection Agency. 2012. Equilibrium Partitioning Sediment Benchmarks (ESBs) for the Protection of Benthic Organisms: Procedures for the Determination of the Freely Dissolved Interstitial Water Concentrations of Nonionic Organics. EPA 600/R-02/012. Technical Report, Washington, DC.</ref> metal mixtures (e.g., cadmium, chromium, copper, nickel, lead, silver, and zinc),<ref name="Metals"/> and pesticides [[dieldrin]]<ref name="Dieldrin"/> and [[endrin]]<ref name="Endrin"/> are available on the USEPA's SQG Technical Resources website.<ref>USEPA. (2012, July 21). Technical Resources-Guidelines. Water: Contaminated Sediments. Retrieved May 29, 2015, from http://water.epa.gov/polwaste/sediments/cs/guidelines.cfm</ref> The USEPA recommends using the ESB approach for the above-mentioned classes of chemicals as well as other nonionic chemicals with hydrophobic chemicals with logK<sub>OW</sub> > 2.00 and sediments containing TOC ≥ 0.2% dry weight (ƒ<sub>OC</sub> = 0.002). The USEPA provides technical guidance for their use, however there are no legally binding requirements.

== Application ==
The purpose of SQGs are to provide a relatively inexpensive line of evidence (a chemical concentration) to predict adverse toxicological effects. Ideally they are used in conjunction with biological and toxicity assessments to provide an overall estimation of risk from contaminated sediments.

ESBs have been applied to Manufactured Gas Plant Sites where PAHs are a chemical of concern. The ESB approach is based on the additivity of ESBs for the 34 PAHs. Because many historical studies and current studies do not analyze for 34 PAHs, uncertainty factors are employed to calculated ESB for total 34 PAHs.<ref name="PAHs"/> However, it is recommended that all 34 PAHs be analyzed for best results. The site-specific ESB can then be used to determine which sediments should be remediated, and if further toxicity testing should be employed. As mentioned before, it is best to use ESBs as one line of evidence in conjunction with other studies.

== Comparison to other SQGs ==
ESBs are just one of multiple SQGs that have been developed for assessment of contaminated sediments. ESBs are different from other SQGs because they are mechanistically derived using chemical and physical properties. Other SQGs have been derived empirically using databases of synoptically collected sediment chemistry and biological effects. Other SQGs include the apparent effects threshold (AET),<ref>Barrick R, Becker S, Brown L, Beller H, Pastorok R. 1988. ''Sediment Quality Values Refinement: 1988 Update and Evaluation of Puget Sound AET'' Vol 1. PTI Environmental Services, Bellevue, WA, USA. https://fortress.wa.gov/ecy/publications/summarypages/0609094.html</ref> [[Effects range low and effects range median|effects range low/effects range median (ERL/ERM)]],<ref>Long E.R., L.G. Morgan. The Potential for Biological Effects of Sediment-Sorbed Contaminants Tested in the National Status and Trends Program. NOAA Technical Memorandum NOS OMA 52. National Oceanic and Atmospheric Administration. Seattle, Washington. 1990.</ref> threshold effects level/probable effect level (TEL/PEL),<ref>MacDonald DD, Ingersoll CG, Berger T. 2000. Development and evaluation of consensus-based sediment quality guidelines for freshwater ecosystems. Arch Environ Contam Toxicol 39:20–31.</ref> and a  logistical model.<ref>Field LJ, MacDonald D, Norton SB, Severn CG, Ingersoll CG. 1999. Evaluating sediment chemistry and toxicity data using logistic regression modeling. Environ Toxicol Chem 18:1311–1322.</ref>

== Limitations ==
ESBs do not predict bioaccumulation or trophic transfer to wildlife and humans, which are important considerations in ecological risk assessment. Bioaccumulative chemicals like polychlorinated biphenls (PCBs) and mercury often affect upper trophic level organisms more seriously than benthic organisms. ESBs, however, are specifically designed for the protection of benthic organisms. As a consequence, the broader ecological risks of bioaccumulative chemicals are not accounted for in the ESB approach.

A second limitation is that the ESB approach assumes that nonionic organic contaminants are associated with the organic carbon portion of the sediment. Recent studies have shown that some organic contaminants are associated with another form of carbon called "black carbon".<ref>Gustafsson O, Haghseta F, Chan C, MacFarlane J, Gschwend PM. 1997. Quantification of the dilute sedimentary soot phase: Implications for PAH speciation and bioavailability. Environ Sci Technol 31:203–209.</ref> If black carbon constitutes a large fraction of sediment and is not accounted for because only TOC measurements are in the ESB calculation, then ESBs may be overprotective.

A final limitations is that ESBs do not always consider the antagonistic, additive or synergistic effects of sediment contaminants. For the specific cases of metal mixtures and individual PAHs, ESBs do take into account additive effects.

==References==
{{reflist}}



[[Category:Aquatic ecology]]
[[Category:Water pollution]]