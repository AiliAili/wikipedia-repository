{{italic title}}
'''''Les Sœurs Vatard''''' (English: ''The Vatard Sisters'') is a [[novel]] by the French writer [[Joris-Karl Huysmans]], first published in 1879. It was the author's second novel. His first, ''Marthe'' (1876), had earned the praise of [[Émile Zola]] and Huysmans had come to be associated with the older author and his [[Naturalism (literature)|Naturalist]] school of fiction. ''Les Sœurs Vatard'' shows the clear influence of Naturalism, being a realistic depiction of working-class life based on meticulous documentation. 

The novel tells the story of two sisters who work in a [[Paris]]ian book-binding factory. Huysmans had inherited such a factory from his mother on her death in 1876, allowing him to observe the lives of his employees at close quarters. The plot concentrates on the unhappy love affairs of the sisters of the title, Céline and Désirée. Céline lives with an artist called Cyprien Tibaille - whose caustic remarks reflect Huysmans' own opinions - before leaving him for a man who beats her. Her more timid sister's relationship with the ineffectual Auguste also comes to nothing. However, the author's main focus is not the plot but vivid descriptions of the milieu in which the characters live. 

''Les Sœurs Vatard'' was published by Charpentier on 26 February 1879. To Huysmans' surprise, the book sold moderately well. It received a glowing notice from Zola and was attacked by the more conservative critics who hated the typical bleakness of Naturalism. [[Gustave Flaubert]], one of Huysmans' heroes, was impressed by what he called "a powerful and outstanding work", although he criticised the book for its lack of focus, use of slang and its promotion of Naturalist concepts of artistic taste.

==Sources==
* Huysmans ''Romans'' Volume One (Bouquins, Robert Laffont, 2005)
* [[Robert Baldick]]: ''The Life of J.-K. Huysmans'' (originally published by Oxford University Press, 1955; revised by Brendan King, Dedalus Press, 2006)
* Huysmans: ''The Vatard Sisters'' translated by Brendan King (Dedalus Books, 2012). Includes introduction and notes.

==External links==
*[https://archive.org/details/lessoeursvatard00huys Full French text]

{{Joris-Karl Huysmans}}

{{DEFAULTSORT:Soeurs Vatard}}
[[Category:1879 novels]]
[[Category:Novels by Joris-Karl Huysmans]]
[[Category:Novels set in Paris]]