{{Use dmy dates|date=March 2012}}
{{Infobox football match
| title                   = 2002 FA Community Shield
| image                   = [[File:2002 FA Community Shield programme.jpg|frameless|upright]]
| caption                 = The [[Football programme|match programme]] cover
| team1                   = [[Arsenal F.C.|Arsenal]]
| team1score              = 1
| team2                   = [[Liverpool F.C.|Liverpool]]
| team2score              = 0
| date                    = 11 August 2002
| stadium                 = [[Millennium Stadium]]
| city                    = [[Cardiff]]
| referee                 = [[Alan Wiley]] ([[Staffordshire Football Association|Staffordshire]])
| man_of_the_match1a      = [[Martin Keown]] (Arsenal)<ref name="rep"/>
| attendance              = 67,337
| weather                 = Mostly cloudy<br />Light rain showers<br />{{convert|16|°C|°F}}<ref>{{cite web|title=History for Cardiff-Wales, United Kingdom|url=http://www.wunderground.com/history/airport/EGFF/2002/8/11/DailyHistory.html?req_city=NA&req_state=NA&req_statename=NA|work=Weather Underground|accessdate=26 January 2013}}</ref>
| previous                = [[2001 FA Charity Shield|2001]]
| next                    = [[2003 FA Community Shield|2003]]
}}

The '''2002 FA Community Shield''' was the 80th [[FA Community Shield]], an annual English [[association football|football]] match played between the winners of the previous season's [[Premier League]] and [[FA Cup]]. It was the first to be contested following the renaming of the competition, formerly titled the FA Charity Shield. The match was contested by [[Arsenal F.C.|Arsenal]], who won a [[2001–02 FA Premier League|league]] and [[2001-02 FA Cup|FA Cup]] [[Double (association football)|double]] the previous season, and [[Liverpool F.C.|Liverpool]], who finished runners-up in the league. It was held at Cardiff's [[Millennium Stadium]], on 11 August 2002. Arsenal won the match by one goal to nil, watched by a crowd of 67,337.

This was Arsenal's 16th Shield appearance and Liverpool's 20th. Arsenal was without several of their first choice players in midfield, who were absent through injury; this prompted a shuffle in the team which saw striker [[Sylvain Wiltord]] positioned on the [[Left winger (football)#Left Wing .28LW.29 and Right Wing .28RW.29|left wing]]. For Liverpool, defender [[Markus Babbel]] was named as a substitute ater a lengthy period out of the side through illness. New signing [[El Hadji Diouf]] started in a creative role, behind strikers [[Michael Owen]] and [[Emile Heskey]] to begin with.

The only goal of the match came in the second half; Arsenal substitute [[Gilberto Silva]] on his debut collected a pass from [[Dennis Bergkamp]] and struck the ball through goalkeeper [[Jerzy Dudek]]'s legs. Arsenal manager [[Arsène Wenger]] praised the match-winner in his post-match interview, while opposing manager [[Gérard Houllier]] felt the result showed that his team needed more game time and attention to passing, in order to improve. The result meant Arsenal was the first team to win the Shield outright 11 times, while it marked Liverpool's first defeat at the Millennium Stadium.

==Background==
Founded in 1908 as a successor to the [[Sheriff of London Charity Shield]],<ref>{{cite news |title=Abandonment of the Sheriff Shield |page=11 |newspaper=The Observer |location=London |date=19 April 1908}}</ref> the [[FA Community Shield|FA Charity Shield]] began as a contest between the respective champions of the [[The Football League|Football League]] and [[Southern Football League|Southern League]], although in 1913, it was played between an Amateurs XI and a Professionals XI.<ref>{{cite news |title=The F.A. Charity Shield |page=10 |newspaper=The Times |location=London |date=7 October 1913}}</ref> In 1921, it was played by the league champions of the top division and [[FA Cup]] winners for the first time.<ref>{{cite news |first=Peter |last=Ferguson |title=The FA Community Shield history |url=http://www.mcfc.co.uk/news/team-news/archive/2011/august/community-shield-2011/the-shield-from-the-beginning |work=mcfc.co.uk |publisher=Manchester City FC |date=4 August 2011 |accessdate=21 April 2014 }}</ref>{{#tag:ref|The [[Premier League]] replaced the [[Football League First Division]] at the top of the [[English football league system|English football pyramid]] after its inception in 1992.<ref>{{cite news |title=Continental or the full English? |url=https://www.theguardian.com/football/2001/dec/02/sport.sportfeatures |first=Alex |last=Fynn |newspaper=The Observer |location=London |date=2 December 2001 |accessdate=3 July 2014}}</ref>|group=lower-alpha}} [[Wembley Stadium (1923)|Wembley Stadium]] acted as the host of the Shield from 1974.<ref name="faru">{{cite web|title=The FA Community Shield history |url=http://www.thefa.com/fa-community-shield/more/history |publisher=The Football Association |accessdate=3 July 2013 |archiveurl=http://www.webcitation.org/6Hthw8Bhx?url=http%3A%2F%2Fwww.thefa.com%2Ffa-community-shield%2Fmore%2Fhistory |archivedate=6 July 2013 |deadurl=no |df=dmy }}</ref> Cardiff's [[Millennium Stadium]] was hosting the Shield for the second time; it took over as the venue for the event while [[Wembley Stadium]] underwent a six-year renovation between 2001 and 2006.<ref name="faru"/>

In February 2002, the trophy was renamed the FA Community Shield. The then-FA marketing director [[Paul Barber (soccer administrator)|Paul Barber]] noted this was in order to reward the work of those contributing to the game, who go unnoticed: "The most important thing is that the many good causes that have benefited from the shield in the past will continue to benefit from the Community Shield in the future."<ref>{{cite news |title=Football Association rename Charity Shield |url=http://www.highbeam.com/doc/1P1-51003923.html |publisher=Associated Press Worldstream |date=9 February 2002}} {{subscription required}}</ref> The FA also intended to prevent any interference made by the [[Charity Commission for England and Wales|Charity Commission]] into where money raised by the game should go.<ref>{{cite news |title=Football: A revamp for Shield |page=59 |newspaper=The Mirror |location=London |date=9 February 2002}}</ref> In April 2002, [[McDonald's]] was announced as the main sponsor of the Community Shield, in a four-year, £28 million pound contract.<ref>{{cite news |first1=David |last1=Bond |first2=Ken |last2=Dyer |title=Football: Double deal joy for FA |url=http://www.standard.co.uk/sport/double-sponsorship-deal-joy-for-fa-6317643.html |newspaper=London Evening Standard |date=11 April 2002 |accessdate=23 July 2013}}</ref> The deal included the company making an "invest[ment] in 8,000 new community-based coaches for young players".<ref>{{cite news |title=McDonalds sign up for community initiative |page=20 |newspaper=Derby Evening Telegraph |date=11 April 2002}}</ref>

[[Arsenal F.C.|Arsenal]] qualified for the 2002 FA Community Shield as the holders of the FA Cup, beating [[Chelsea F.C.|Chelsea]] 2–0 in the [[2002 FA Cup Final]]. The team later won the [[2001–02 FA Premier League]] and completed the [[Double (association football)#England|domestic double]].<ref name="doo">{{cite news |title=New shield for new season |url=http://news.bbc.co.uk/sport1/hi/football/2173566.stm |publisher=BBC Sport |date=8 August 2002 |accessdate=8 July 2013}}</ref> The other Community Shield place went to league runners-up [[Liverpool F.C.|Liverpool]], who secured second place on the final day of the season.<ref name="doo"/><ref>{{cite news |title=Rampant Reds sink Ipswich |url=http://news.bbc.co.uk/sport1/hi/football/eng_prem/1978521.stm |publisher=BBC Sport |date=11 May 2002 |accessdate=26 January 2013}}</ref> The game marked Arsenal's 16th Charity Shield appearance and first since [[1999 FA Charity Shield|1999]], where they beat Manchester United by two goals to one.<ref>{{cite web |first=James |last=Ross |title=List of FA Charity/Community Shield Matches |url=http://www.rsssf.com/tablese/engsupcuphist.html |publisher=Rec. Sport. Soccer Statistics Foundation (RSSSF) |date=15 August 2013 |accessdate=4 August 2014}}</ref><ref>{{cite news |first=David |last=Lacey |title=Arsenal play their troubles away |url=https://www.theguardian.com/football/1999/aug/02/newsstory.sport4 |newspaper=The Guardian |location=London |date=2 August 1999 |accessdate=23 July 2013}}</ref> By contrast this was Liverpool's 20th, who incidentally played United and won by exactly the same scoreline to become holders of the trophy.<ref>{{cite news |title=Liverpool edge out Man Utd |url=http://news.bbc.co.uk/sport1/hi/football/1487037.stm |publisher=BBC Sport |date=12 August 2001 |accessdate=21 March 2013}}</ref> The last meeting between the two teams was in the FA Cup; a goal by [[Dennis Bergkamp]] ensured Arsenal progressed into the fifth round of the competition.<ref>{{cite news |title=Arsenal v Liverpool head-to-head record |url=http://www.lfconline.com/head_to_head/arsenal/vs/liverpool/index.shtml |work=LFC Online |publisher=Digital Sports Group |accessdate=8 July 2013}}</ref><ref>{{cite news |title=Sweet revenge for Arsenal |url=http://news.bbc.co.uk/sport1/hi/football/fa_cup/1780997.stm |publisher=BBC Sport |date=27 January 2002 |accessdate=26 January 2013}}</ref>

==Pre-match==
In spite of the game's traditional friendly feel, Liverpool manager [[Gérard Houllier]] highlighted in pre-match press conference the importance of winning the Shield: "It is very important to me. This match is more than a friendly. It represents the start of the season. The Community Shield is like a final. There is a trophy at stake and we are the holders."<ref>{{cite news |title=Houllier holds no charity |page=88 |newspaper=[[The Daily Telegraph (Australia)|The Daily Telegraph]] |location=Sydney |first=Bill |last=Barclay |date=10 August 2002}}</ref> [[Arsène Wenger]] wanted to use the match as "the final preparation game for the championship" and commented that Liverpool, amongst four other teams (Manchester United, [[Newcastle United F.C.|Newcastle United]], Chelsea and [[Leeds United A.F.C.|Leeds United]]) would challenge Arsenal in the league.<ref>{{cite news |title=Community Shield: Wenger's winning way with words |url=http://www.telegraph.co.uk/sport/football/teams/arsenal/3032445/Community-Shield-Wengers-winning-way-with-words.html |work=The Daily Telegraph |location=London |date=10 August 2002 |accessdate=21 March 2013}}</ref>

==Match==
Arsenal was without injured trio [[Robert Pirès]], [[Freddie Ljungberg]] and [[Giovanni van Bronckhorst]] and lined up in their traditional [[Formation (association football)#4–4–2|4–4–2]] formation; striker [[Sylvain Wiltord]] was accommodated to the left wing, with Bergkamp playing off main striker [[Thierry Henry]].<ref name="rep">{{cite news |title=Arsenal add to silvaware |page=B12 |newspaper=The Times |location=London |first=Matt |last=Dickinson |date=12 August 2002}}</ref><ref name="pre">{{cite news |title=Confident Vieira puts his money on Arsenal |page=29 |newspaper=The Times |location=London |first=Matt |last=Dickinson |date=10 August 2002}}</ref> After a lengthy illness, Liverpool defender [[Markus Babbel]] returned to the squad and was named as a substitute. Houllier deployed a [[Formation (association football)#4.E2.80.931.E2.80.933.E2.80.932|4–3–1–2]] formation with [[Steven Gerrard]], [[John Arne Riise]] and [[Dietmar Hamann]] as a midfield three and new signing, [[El Hadji Diouf]] positioned in a [[Hole (association football)|free role]] to begin with and as the game went on, moved to the right wing to stretch Arsenal's defence.<ref name="rep"/>

===Summary===
[[File:Gilberto silva crop.png|thumb|Arsenal midfielder [[Gilberto Silva]] scored the only goal of the game, on his debut.]]
Arsenal won the toss and kicked off.<ref name="ded">{{cite news |title=Clockwatch: Arsenal 1–0 Liverpool |url=http://news.bbc.co.uk/sport1/hi/football/2183671.stm |publisher=BBC Sport |date=11 August 2002 |accessdate=22 March 2013}}</ref> Within the first three minutes they fashioned their first opening: Wiltord found space outside the Liverpool box, yet failed to give the ball to Henry, as the through ball was cleared.<ref name="ded"/> Gerrard was booked for a tackle on Vieira in the sixth minute, which left the Frenchman seeking medical attention and moments after, he caught Henry late with a similar lunge; Gerrard was given the benefit of the doubt by the referee.<ref name="ded"/> Bergkamp volleyed the ball "from close range" which was blocked by defender [[Abel Xavier]] and two of his efforts were saved in quick succession by [[Jerzy Dudek]] after the 20-minute mark.<ref name="ded"/> Liverpool managed their first shot on target before the half-hour: a shot from [[Emile Heskey]] which hit goalkeeper [[David Seaman]]'s chest.<ref name="guar">{{cite news |first=Richard |last=Williams |title=Arsenal show undimmed taste for titles as Silva shines |url=https://www.theguardian.com/football/2002/aug/12/match.sport4 |newspaper=The Guardian |page=A2 |location=London |date=12 August 2002 |accessdate=23 July 2013}}</ref> After a quiet period towards the end of the first half, [[Ray Parlour]] went close to giving Arsenal the lead, through a long-range shot that missed the post by fractions.<ref name="ded"/> A move from Arsenal soon after concluded with Wiltord firing the ball straight at Dudek, having been under pressure by [[Djimi Traoré]].<ref name="ded"/>

Midfielder [[Gilberto Silva]] made his debut in the second half, coming on in place of fellow Brazilian [[Edu Gaspar|Edu]]. In the 47th minute, Henry went close to scoring his first goal at the Millennium Stadium; he collected a pass from Bergkamp and used his pace to get past the Liverpool defence.<ref name="ded"/> His shot caught Dudek and rebounded off the post.<ref name="rep"/> Henry created further chances in the 51st and 54th minutes, forcing more saves from the Liverpool goalkeeper.<ref name="ded"/> Owen was unable to reach the ball from a corner and Liverpool continued to pressure Arsenal, with Gerrard regaining control of the midfield and making a substitution which saw [[Danny Murphy (footballer born 1977)|Danny Murphy]] come on for Hamann.<ref name="ded"/> In spite of this, it was Arsenal who scored the opening goal, in the 68th minute. A move by Liverpool, which had broken down, allowed Gilberto to pass the ball out to [[Ashley Cole]], who in turn fed Bergkamp.<ref name="guar"/> His cut-back met the Brazilian's left foot "with a shot that squeezed through Dudek's legs."<ref name="guar"/> Diouf went down in the penalty box four minutes after, which provoked a reaction by Vieira, who believed he had dived. Both managers made numerous changes in the final 15 minutes, which included [[Milan Baroš]] replacing Heskey and midfielder [[Kolo Touré]] on for Bergkamp.<ref name="ded"/> The final notable chance went to Liverpool a minute before additional time: [[Sol Campbell]] was caught out of position in Arsenal's half, but Diouf's shot went over the goal bar.<ref name="ded"/>

===Details===
{{Football box
|date = 11 August 2002
|time = 14:00 [[Western European Summer Time|BST]]
|team1 = [[Arsenal F.C.|Arsenal]]
|score = 1–0
|report = [https://archive.is/20040621224728/http://www.thefa.com/TheFACup/TheFACommunityShield/NewsAndFeatures/Postings/2002/08/21438.htm Report]<br /><ref>{{cite news |title=Slick Arsenal win Shield |url=http://news.bbc.co.uk/sport1/hi/football/2182875.stm |publisher=BBC Sport |date=11 August 2002 |accessdate=23 July 2013}}</ref>
|team2 = [[Liverpool F.C.|Liverpool]]
|goals1 = [[Gilberto Silva|Gilberto]] {{goal|69}}
|goals2 =
|stadium = [[Millennium Stadium]], [[Cardiff]]
|attendance = 67,337
|referee = [[Alan Wiley]] ([[Staffordshire Football Association|Staffordshire]])<ref name="officials">{{cite news |title=Community Shield match details |url=http://www.thefa.com/TheFACup/TheFACommunityShield/NewsAndFeatures/Postings/2002/07/19698.htm |archive-url=https://archive.is/20040615195846/http://www.thefa.com/TheFACup/TheFACommunityShield/NewsAndFeatures/Postings/2002/07/19698.htm |dead-url=yes |archive-date=15 June 2004 |publisher=The Football Association |date=29 July 2002 |accessdate=23 July 2013 }}</ref>
}}

{| width=92%
|-
|{{Football kit
 | pattern_la = _arsenal0203h
 | pattern_b  = _arsenalh0204
 | pattern_ra = _arsenal0203h
 | pattern_sh = _arsenal0203h
 | pattern_so = _arsenal0203h
 | leftarm    = FFFFFF
 | body       = DD0000
 | rightarm   = FFFFFF
 | shorts     = FFFFFF
 | socks      = FFFFFF
 | title      = Arsenal
}}
|{{Football kit
 | pattern_la = _liverpool0203a
 | pattern_b  = _liverpool0203a
 | pattern_ra = _liverpool0203a
 | pattern_sh = _liverpool0203a
 | pattern_so = _liverpool0203a
 | leftarm    = CCCCCC
 | body       = 000000
 | rightarm   = CCCCCC
 | shorts     = CCCCCC
 | socks      = 000000
 | title      = Liverpool
}}
|}

{| width="100%"
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0"
|colspan="4"|
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagicon|ENG}} [[David Seaman]]
|-
|RB ||'''12'''||{{flagicon|CMR}} [[Lauren Etame Mayer|Lauren]]
|-
|CB ||'''5'''||{{flagicon|ENG}} [[Martin Keown]]
|-
|CB ||'''23'''||{{flagicon|ENG}} [[Sol Campbell]]
|-
|LB ||'''3''' ||{{flagicon|ENG}} [[Ashley Cole]]
|-
|RM ||'''15'''||{{flagicon|ENG}} [[Ray Parlour]]
|-
|CM ||'''4'''||{{flagicon|FRA}} [[Patrick Vieira]]
|-
|CM ||'''17'''||{{flagicon|BRA}} [[Edu Gaspar|Edu]] || || {{suboff|45}}
|-
|LM ||'''11'''||{{flagicon|FRA}} [[Sylvain Wiltord]] || {{yel|24}}
|-
|CF ||'''10'''||{{flagicon|NED}} [[Dennis Bergkamp]] || || {{suboff|85}}
|-
|CF ||'''14'''||{{flagicon|FRA}} [[Thierry Henry]] || {{yel|44}}
|-
|colspan=3|'''Substitutes:'''
|-
|GK ||'''13'''||{{flagicon|ENG}} [[Stuart Taylor (footballer born 1980)|Stuart Taylor]]
|-
|DF ||'''18'''||{{flagicon|FRA}} [[Pascal Cygan]]
|-
|DF ||'''20'''||{{flagicon|ENG}} [[Matthew Upson]]
|-
|DF ||'''22'''||{{flagicon|UKR}} [[Oleh Luzhnyi|Oleg Luzhny]]
|-
|MF ||'''28'''||{{flagicon|CIV}} [[Kolo Touré]] || || {{subon|85}}
|-
|MF ||'''19'''||{{flagicon|BRA}} [[Gilberto Silva]] || || {{subon|45}}
|-
|FW ||'''30'''||{{flagicon|FRA}} [[Jérémie Aliadière]]
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagicon|FRA}} [[Arsène Wenger]]
|-
|}
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0" align=center
|colspan="4"|
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagicon|POL}} [[Jerzy Dudek]]
|-
|RB ||'''3''' ||{{flagicon|POR}} [[Abel Xavier]] || || {{suboff|78}}
|-
|CB ||'''4'''||{{flagicon|FIN}} [[Sami Hyypiä]]
|-
|CB ||'''2'''||{{flagicon|SUI}} [[Stéphane Henchoz]]
|-
|LB ||'''30'''||{{flagicon|MLI}} [[Djimi Traoré]] || || {{suboff|88}}
|-
|RM ||'''9'''||{{flagicon|SEN}} [[El Hadji Diouf]]
|-
|CM ||'''16'''||{{flagicon|GER}} [[Dietmar Hamann]] || || {{suboff|67}}
|-
|CM ||'''17'''||{{flagicon|ENG}} [[Steven Gerrard]] || {{yel|6}}
|-
|LM ||'''18'''||{{flagicon|NOR}} [[John Arne Riise]]
|-
|CF ||'''8'''||{{flagicon|ENG}} [[Emile Heskey]] || || {{suboff|74}}
|-
|CF ||'''10'''||{{flagicon|ENG}} [[Michael Owen]] || || {{suboff|85}}
|-
|colspan=3|'''Substitutes:'''
|-
|GK ||'''22'''||{{flagicon|ENG}} [[Chris Kirkland]]
|-
|DF ||'''6''' ||{{flagicon|GER}} [[Markus Babbel]] || || {{subon|78}}
|-
|DF ||'''23'''||{{flagicon|ENG}} [[Jamie Carragher]]
|-
|MF ||'''28'''||{{flagicon|FRA}} [[Bruno Cheyrou]] || || {{subon|88}}
|-
|MF ||'''7'''||{{flagicon|CZE}} [[Vladimír Šmicer]] || || {{subon|85}}
|-
|MF ||'''13'''||{{flagicon|ENG}} [[Danny Murphy (footballer born 1977)|Danny Murphy]] || || {{subon|67}} || {{yel|82}}
|-
|FW ||'''5'''||{{flagicon|CZE}} [[Milan Baroš]] || || {{subon|74}}
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagicon|FRA}} [[Gérard Houllier]]
|}
|}
{| width=82% style="font-size: 90%"
| width=50% valign=top|
;Man of the match
*[[Martin Keown]] (Arsenal)<ref name="rep"/>

;Match officials
*Assistant referees:
**Nigel Miller ([[Durham County Football Association|Durham]])<ref name="officials"/>
**Trevor Kettle ([[Royal Air Force Football Association|Royal Air Force]])<ref name="officials"/>
*Fourth official: [[Rob Styles]] ([[Hampshire Football Association|Hampshire]])<ref name="officials"/>
| style="width:50%; vertical-align:top"|
'''Match rules'''<ref>{{cite web |title=Rules of the Football Association Community Shield |url=http://www.thefa.com/~/media/978792AF8BBE4927AA4583437A7DF882.ashx/FA%20Community%20Shield%20Rules.pdf |format=PDF |publisher=The Football Association |accessdate=4 August 2014}}</ref>
*90 minutes.
*Penalty shoot-out if scores level after 90 minutes.
*Seven named substitutes.
*Maximum of six substitutions.
|}

===Statistics===
{| class="wikitable plainrowheaders" style="text-align:center"
|-
! scope="col" |Statistic<ref name="guar"/>
! scope="col" style="width:70px"| Arsenal
! scope="col" style="width:70px;"| Liverpool
|-
!scope=row|Goals scored
| 1
| 0
|-
!scope=row|Possession
| 56%
| 44%
|-
!scope=row|Shots on target
| 9
| 1
|-
!scope=row|Shots off target
| 3
| 6
|-
!scope=row|Corner kicks
| 5
| 7
|-
!scope=row|Offsides
| 3
| 1
|-
!scope=row|Yellow cards
| 4
| 2
|-
!scope=row|Red cards
| 0
| 0
|}

==Post-match==
{{Quote box |width=25em |quoted=true |bgcolor=#FFFFF0 |salign=left |quote=He's an intelligent boy, makes things simple and gets into the box at the right time.|source=[[Arsène Wenger]] on [[Gilberto Silva]]<ref name="reaction">{{cite news |title=Wenger delighted with win |url=http://news.bbc.co.uk/sport1/hi/football/2187002.stm |publisher=BBC Sport |date=12 August 2002 |accessdate=25 January 2013}}</ref>}}
The result marked the first time that Liverpool had lost at the Millennium Stadium. Wenger used his post-match news interview to praise goalscorer Gilberto Silva and asserted that the Brazilian would be a "very strong player" for the team, in the incoming future.<ref name="reaction"/> He noted that Dudek had "kept [Liverpool] in the game for a long time" and was critical of Gerrard, who he felt was fortunate not to have been sent off for a "reckless and dangerous" challenge on Vieira.<ref>{{cite news |title=Silva adds to Arsenal's riches |url=http://www.telegraph.co.uk/sport/football/teams/liverpool/3032553/Silva-adds-to-Arsenals-riches.html |newspaper=The Daily Telegraph |location=London |first=Henry |last=Winter |date=11 August 2002 |accessdate=25 January 2013}}</ref><ref name="bham">{{cite news |title=Football: Wenger sees red at 'reckless' Gerrard as Gunners triumph |page=18 |newspaper=Birmingham Post |date=12 August 2002}}</ref> Wenger believed Arsenal's experience put the team in good stead for the upcoming season: "There are two attitudes to being champions. One is to get respect, another is to get kicked more and to face more aggression as people want to beat you more. We are quite used to that though as we have had some big fights in the past four or five years."<ref name="reaction"/>

Houllier argued that his team paid Arsenal "too much respect" in the match and blamed his full-backs Xavier and Traoré for not providing width, adding: "We need to have more flow in our game, more safety in our passing construction".<ref name="bham"/><ref>{{cite news |title=Wenger fury over 'reckless' Gerrard |page=80 |newspaper=Daily Express |location=London |first=David |last=Hytner |date=12 August 2002}}</ref> Gerrard was unrepentant for tackling Vieira and stated he was trying to "stamp [his] authority" on the match.<ref name="echo">{{cite news |title=Football: We want revenge |pages=39–40 |newspaper=Liverpool Echo |first=Chris |last=Bascombe |date=12 August 2002}}</ref>  He accepted defeat and was optimistic Liverpool had the means to win the Premier League: "We'd rather they beat us in this game and then we go on to pick up the title. That's what we're really looking at. Even though they got the better of us yesterday, it doesn't mean they'll do better than us over the whole season."<ref name="echo"/> Diouf insisted he should have earned a penalty in the second half, given he felt there was "definite contact" from a challenge in the penalty box: "It was a penalty, definitely, but the referee is the master on the pitch and he didn't give it. I know the Arsenal players think I dived, but I didn't."<ref>{{cite news |title=Wenger: Gerrard should have gone |url=http://www1.skysports.com/football/news/11669/2242749/Wenger-Gerrard-should-have-gone |publisher=Sky Sports |first=Paul |last=Higham |date=12 August 2002 |accessdate=25 January 2013}}</ref>

==See also==
*[[2002–03 FA Premier League]]
*[[2002–03 FA Cup]]

==Notes==
{{reflist|group=lower-alpha}}

==References==
{{reflist|30em}}

{{FA Community Shield}}
{{Arsenal F.C. matches}}
{{Liverpool F.C. matches}}
{{2002–03 in English football}}
{{good article}}

[[Category:FA Community Shield|2002]]
[[Category:2002–03 in Welsh football|Fa Community Shield]]
[[Category:Arsenal F.C. matches|Charity Shield 2002]]
[[Category:Liverpool F.C. matches|Charity Shield 2002]]
[[Category:2002–03 in English football|Comm]]
[[Category:21st century in Cardiff]]