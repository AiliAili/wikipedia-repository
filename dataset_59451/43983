<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, guidelines. -->
{|{{Infobox aircraft begin
 |name= F414
 |image= File:General Electric F414 AEDC 93-206711 USAF.jpg
 |caption= 
}}
{{Infobox aircraft engine
 |type= [[Turbofan]]
 |national origin= [[United States]]
 |manufacturer= [[GE Aviation|General Electric]] 
 |first run= May 20, {{Avyear|1993}}<ref>"GE marks F414 progress; endurance tests near" (1993).  ''Aviation Week and Space Technology''. Vol. 139, No. 1; p. 31</ref>
 |major applications= [[Boeing F/A-18E/F Super Hornet]]<br />[[Saab JAS 39 Gripen#Gripen NG and recent developments|Saab Gripen Demo/NG]]<br />[[HAL Tejas|HAL Tejas Mk 2]]
 |number built=
 |program cost= 
 |unit cost= 
 |developed from= [[General Electric F404]]
 |developed into= 
 |variants with their own articles= 
}}
|}

The '''General Electric F414''' is an [[Afterburner|afterburning]] [[turbofan]] engine in the 22,000-[[pound-force|pound]] (98 [[kilonewton|kN]]) thrust class produced by [[GE Aviation]].  The F414 originated from GE's widely used [[General Electric F404|F404]] turbofan from the [[McDonnell Douglas F/A-18 Hornet]], enlarged and improved for use in the [[Boeing F/A-18E/F Super Hornet]]. The engine was developed from the F412 non-afterburning turbofan planned for the [[McDonnell Douglas A-12 Avenger II|A-12 Avenger II]], before it was canceled.

==Design and development==

===Origins===
GE evolved the F404 into the F412-GE-400 non-afterburning turbofan for the [[McDonnell Douglas A-12 Avenger II]]. After the cancellation of the A-12, the research was directed toward an engine for the [[F/A-18E/F Super Hornet]]. GE successfully pitched the F414 as a low-risk derivative of the F404, rather than a riskier new engine. The F414 engine was originally envisioned as not using any materials or processes not used in the F404, and was designed to fit in the same footprint as the F404.<ref name="f414_CDR">"Confident GE heads to F414 CDR next month" (1994). ''Aerospace Daily''. Vol 169, No. 34; p. 270.</ref>

The F414 uses the core of the F412 and its [[FADEC|full-authority digital engine control]] (FADEC), alongside the low-pressure system from the [[General Electric YF120|YF120]] engine developed for the [[Advanced Tactical Fighter]] competition. One of the major differences between the F404 and the F414 is the fan section. The fan of the F414 is larger than that of the [[General Electric F404|F404]], but smaller than the fan for the F412.<ref>"GE wins F-18E/F study" (1991). ''Flight International''. 4 September 1991.</ref> The larger fan section increases airflow mass by 16% and is {{convert|5|in|cm|sigfig=2}} longer. To keep the engine in the F404's footprint, the afterburner section was shortened by {{convert|4|in|cm|sigfig=2|abbr=on}} and the [[combustor]] shortened by {{convert|1|in|cm|sigfig=2|abbr=on}}. Another change from the F404 is the fact that the first three stages of the high-pressure compressor are [[blisk]]s rather than dovetailed blades, saving {{convert|50|lb|kg|sigfig=2}} in weight.<ref name="f414_CDR"/> Furthermore, the FADEC guided F414 uses a fuel actuated "[[Hydraulic drive system|fueldraulic]]" system to manipulate the [[Propelling nozzle|convergent-divergent nozzle]] in the afterburner section, which is a system which uses fuel from the aircraft's fuel system as hydraulic fluid, rather than relying on a separate hydraulic system with its associated fluid reservoir ("fueldraulic" systems have become popular in applications such as this, and even in [[Rolls-Royce LiftSystem|more major systems]] in some cases).<ref name="f414_comptest">Kandebo, Stanley (1992). "GE Component Test Program to Reduce Risk in F414 Engine Development". ''Aviation Week and Space Technology''. Vol. 136, No. 26; p. 64.</ref>

===Further development===
The F414 continues to be improved, both through internal GE efforts and federally funded development programs.  By 2006 GE  had tested an Enhanced Durability Engine (EDE) with an advanced core.  The EDE engine provided a 15% thrust increase or longer life without the thrust increase. It has a six-stage high-pressure compressor (down from 7 stages in the standard F414) and an advanced high-pressure turbine.<ref name= "f414_improve">[http://www.geae.com/aboutgeae/presscenter/military/military_20060717b.html "GE F110 and F404/F414 Fighter Engines Expand Capability and Global Presence"].  GE Aviation, July 17, 2006</ref> The new compressor should be about 3% more efficient. The new high-pressure turbine uses new materials and a new way of delivering cooling air to the blades. These changes should increase the turbine temperature capability by about 150&nbsp;°F (83&nbsp;°C).<ref name ="f414ede">Kandebo, Stanley W. "Enhanced F414 Readies for Tests" (2004). ''Aviation Week and Space Technology''. Vol. 160, No. 1; p. 58.</ref>  The EDE is designed to have better [[foreign object damage]] resistance, and a reduced fuel burn rate.<ref name= "More Powerful Engine">Norris, Guy. [http://www.aviationweek.com/aw/generic/story_channel.jsp?channel=mro&id=news/ge5149.xml "GE Eyes More Powerful Engine For Super Hornets, Growlers"]. ''Aviation Week'', 14 May 2009.</ref><ref name="thrust upgrade">Trimble, Stephen. [http://www.flightglobal.com/articles/2009/05/12/326376/boeings-super-hornet-seeks-export-sale-to-launch-20-thrust.html "Boeing's Super Hornet seeks export sale to launch 20% thrust upgrade"]. ''Flight International'', 12 May 2009.</ref>

The EDE program continued with the testing of an advanced two stage blade-disk ([[blisk]]) fan.  The first advanced fan was produced using traditional methods, but future blisk fans will be made using translational [[friction welding]] with the goal of reducing manufacturing costs.<ref name= "f414ede" /> GE touts that this latest variant yields either a 20% increase in thrust or threefold increase in hot-section durability over the current F414.<ref name= "f414_improve" />  This version is called the Enhanced Performance Engine (EPE) and was partially funded through the federal [[Integrated High Performance Turbine Engine Technology]] (or IHPTET) program.<ref name="More Powerful Engine" /><ref>{{Cite web |url= http://www.geae.com/aboutgeae/presscenter/military/military_20061212.html |title= F414 Growth Demonstrator Engine Completes Testing |publisher= GE |type= press release |date= 12 December 2006 |accessdate= 13 Aug 2009}}</ref>

Other possible F414 improvements include efforts to reduce engine noise by using either mechanical or fluidic [[Chevron (aeronautics)|chevrons]] and efforts to reduce emissions with a new trapped vortex combustor.<ref name="f414ede" /> Chevrons would reduce engine noise by inducing mixing between the cooler, slower bypass air and the hotter, faster core exhaust air. Mechanical chevrons would come in the form of triangular cutouts (or extensions) at the end of the nozzle, resulting in a "sharktooth" pattern. Fluidic chevrons would operate by injecting differential air flows around the exhaust to achieve the same ends as the mechanical variety. A new combustor would likely aim to reduce emissions by burning a higher percentage of the [[oxygen]], thereby reducing the amount of oxygen available to bond with [[nitrogen]] forming the pollutant [[Nitrogen oxide#NOx|NO<sub>x</sub>]].

As of 2009, the F414-EDE was being developed and tested, under a United States Navy contract for a reduced [[Specific fuel consumption (thrust)|specific fuel consumption]] (SFC) demonstrator engine.<ref>[http://www.geae.com/aboutgeae/presscenter/military/military_20090615.html "New Orders, Tech Insertions Mark Increased GE Fighter Engine Presence"]. GE Press Release. June 15, 2009. Retrieved 13 Aug 2009.</ref><ref>{{Cite web |url= https://www.fbo.gov/spg/DON/NAVAIR/N00019/N00019-09-G-0009/listing.html |title= Recovery: Specific Fuel Consumption Reduction Demonstration |year= 2009 |publisher= Federal Business Opportunities |id= Solicitation Number: N00019-09-G-0009 |accessdate= 13 August 2009}}</ref> In addition, General Electric has tested F414 engines equipped with a second low-pressure turbine stage made from ceramic matrix composites (CMC). The F414 represents the first successful use of a CMC in a rotating engine part. The tests proved CMCs are strong enough to endure the heat and rotational stress inside the turbine. The advantage CMC offers is a weight one third that of metal alloy and the ability to operate without cooling air, making the engine more aerodynamically efficient and fuel efficient.  The new turbine is not yet ready for a production aircraft, however, as further design changes are needed to make it more robust.<ref>Norris, Guy. "CMCs advance", Aviation Week & Space Technology, February 2–15, 2015, p. 28.</ref>

Over 1,000 F414 engines have been delivered and the engine family has totaled over 1 million flight hours by 2010.<ref>[http://www.geae.com/aboutgeae/presscenter/military/military_20100719.html "Proven Experience, Program Upgrades Spark GE F110 and F404/414 Popularity"]. GE Aviation, July 19, 2010.</ref>

==Variants==
[[File:Four Super Hornets.jpg|right|thumb|F/A-18 Super Hornets, powered by the F414-GE-400]]

;F414-GE-400: Flies in the Boeing [[F/A-18E/F Super Hornet]].  Also proposed for the unbuilt naval F-117N variant of the [[F-117 Nighthawk]].<ref>Morrocco, John (1994). "Lockheed returns to Navy with new F-117N design".  ''Aviation Week and Space Technology''. Vol. 140, No. 10; p. 26.</ref>
;F414-EDE: "Enhanced Durability Engine" or "EDE", includes an improved high-pressure turbine (HPT) and high-pressure compressor (HPC). The HPT is redesigned to withstand slightly higher temperatures and includes aerodynamic changes. The HPC has been redesigned to 6 stages, down from 7. These changes aimed at reducing SFC by 2% and component durability three times higher.<ref>[http://www.flightglobal.com/news/articles/ge-bids-for-enhanced-f414-ede-funding-by-2003-129821/ "GE bids for enhanced F414 EDE funding by 2003"]. ''Flight International''. 8–14 May 2001, p. 26.</ref>
;F414-EPE:  "Enhanced Performance Engine" or "EPE", includes a new core and a redesigned fan and compressor.  Offers up to a 20 percent thrust boost, increasing it to 26,400 pounds (120&nbsp;kN), giving an almost 11:1 thrust/weight ratio.<ref>Sweetman, Bill. [http://www.aviationweek.com/aw/blogs/defense/index.jsp?plckController=Blog&plckBlogPage=BlogViewPost&newspaperUserId=27ec4a53-dcc8-42d0-bd3a-01329aef79a7&plckPostId=Blog%3a27ec4a53-dcc8-42d0-bd3a-01329aef79a7Post%3a98ca66c5-16cc-44a7-bfa3-45d1436e826f&plckScript=blogScript&plckElementId=blogDest "GE Brings Good Things To Hornet, Gripen"]. ''Aviation Week'' Ares Blog, 21 April 2011.</ref>
;F414M: Used by the [[EADS Mako/HEAT]]. Derated thrust to 12,500&nbsp;lbf (55.6&nbsp;kN) dry and 16,850&nbsp;lbf (75&nbsp;kN) wet.<ref>"Military training: Phase III". ''Flight International''. 15 July 2003. p. 40.</ref>  Proposed for international versions of the Korean [[KAI T-50 Golden Eagle|T-50]] series of trainers and fighter aircraft, but later superseded by a new offer with a standard F414.<ref name= "f414_improve" /><ref name= fgengine>{{Cite web |url= http://www.flightglobal.com/articles/2011/05/24/357148/lockheed-ponders-t-50-re-engining-for-t-x-programme.html |title= Lockheed ponders T-50 re-engining for T-X programme |work= Flight global |date= May 24, 2011 }}</ref>
;F414G: Produced for the [[Saab JAS 39 Gripen]] Demonstrator.  Slightly modified for use in a single engine Gripen, instead of a twin-engine aircraft like the F/A-18. With it, the Gripen Demonstrator reached Mach 1.2 in [[supercruise]] (without afterburner).<ref>{{Cite web |last= Hoyle |first= Craig |url= http://www.flightglobal.com/articles/2009/01/22/321428/saab-celebrates-supercruise-test-success-for-gripen.html |title= Saab celebrates 'supercruise' test success for Gripen Demo |publisher= [[Flight International]] |work= Flight global |date= 22 January 2009}}.</ref>
;F414BJ: Proposed for the Dassault Falcon SSBJ.  Would produce around {{convert|12000|lbf|kN|abbr=on}} of thrust without use of afterburner.<ref>"Dassault officials say three-engine SST would have a 4 000-mile range" (1998). ''The Weekly of Business Aviation''. Vol. 66, No. 22; p. 239.</ref><ref>{{Cite web |last= Warwick |first= Graham |url= http://www.flightglobal.com/pdfarchive/view/1998/1998%20-%202374.html |title= Big-jet business |publisher= Flight International |date= 8 September 1998 |work= Flight global}}</ref>
;F414-GE-INS6: India's [[Aeronautical Development Agency]] selected the F414-GE-INS6 to power the [[HAL Tejas]] Light Combat Aircraft (LCA) Mk II for the Indian Air Force.  India ordered 99 engines in October 2010.  It produces more thrust than previous versions, and features a Full Authority Digital Electronic Control ([[FADEC]]) system.<ref name="hoyle" />  The engines are to be delivered by 2013.<ref>{{Cite web|url=http://www.tejas.gov.in/history/genesis.html |title=Tejas, India’s Light Combat Aircraft, History |publisher=tejas.gov.in |deadurl=yes |archiveurl=http://www.webcitation.org/6QkIEvgRo?url=http://www.tejas.gov.in/history/genesis.html |archivedate=2014-07-01 |df= }}</ref>
;F414-GE-39E: New version of the F414G for the Saab JAS-39E/F Gripen.<ref>{{Cite web |url= http://www.geaviation.com/military/engines/f414/ |title= Military engines, The F414 Engine |publisher= GE aviation}}</ref>

==Applications==
* [[Boeing F/A-18E/F Super Hornet]]
* [[EADS Mako/HEAT]]
* [[Saab JAS 39 Gripen#Further developments|Saab Gripen Demo/NG]]
* [[HAL Tejas#Planned production variants|HAL Tejas Mark II]]<ref name="hoyle">Hoyle, Craig. [http://www.flightglobal.com/articles/2010/10/01/348059/india-picks-ges-f414-for-tejas-mkii-fighter.html "India picks GE's F414 for Tejas MkII fighter"]. ''Flight International'', 1 October 2010.</ref>

==Specifications (F414-400)==
{{jetspecs
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]].
     Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully-formatted line with <li>. -->
|type=Afterburning [[turbofan]] 
|ref=General Electric<ref name= "specs">{{Cite web |url= http://www.geae.com/engines/military/comparison_turbofan.html |title= Comparison Chart — Military Turbofans |publisher= GE Aviation}}</ref>
|length=154 in (391 cm)
|diameter=35 in (89 cm)
|weight= 2,445 lb (1,110 kg) max weight<ref>{{Cite web |title= Fighter aircraft engines, F414 GE 400 |url= http://www.deagel.com/Fighter-Aircraft-Engines/F414-GE-400_a001733001.aspx |publisher= Dégel}}</ref> 
|compressor=[[Axial compressor]] with 3 fan and 7 compressor stages
|combustion=annular
|turbine=1 low-pressure and 1 high-pressure stage
|fueltype=
|oilsystem=
|power= 
|thrust=<br>
* {{convert|13,000|lbf|kN|abbr=on|lk=on|sigfig=3}} military thrust
* {{convert|22,000|lbf|kN|abbr=on|sigfig=3}} with afterburner 
|compression=30:1
|aircon=
|turbinetemp=
|fuelcon=
|specfuelcon=
|power/weight=
|thrust/weight=9:1<ref>{{Cite web |title= Military aircraft systems, F414 |publisher= Global security |url= http://www.globalsecurity.org/military/systems/aircraft/systems/f414.htm}}</ref>
}}
*air mass flow: 77.1 kg/s

==See also==
{{Aircontent
|see also=<!-- other related articles that have not already linked: -->

|related=<!-- designs which were developed into or from this aircraft: -->
*[[General Electric F404]]
*[[General Electric YF120]]
|similar aircraft=<!-- aircraft that are of similar role, era, and capability as this design: -->

|lists=<!-- relevant lists that this aircraft appears in: -->
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[Eurojet EJ200]]
*[[GTRE GTX-35VS Kaveri]]
*[[Guizhou WS-13]]
*[[Klimov RD-93]]
*[[Snecma M88]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{Reflist|2}}

==External links==
{{commons category}}
* [http://www.geae.com/engines/military/f414/ GEAE F414/F414M page]
* [http://www.globalsecurity.org/military/systems/aircraft/systems/f414.htm F414 page on GlobalSecurity.org]

{{GE aeroengines}}
{{USAF gas turbine engines}}

[[Category:Low-bypass turbofan engines]]
[[Category:General Electric aircraft engines|F414]]
[[Category:Turbofan engines 1990–1999]]